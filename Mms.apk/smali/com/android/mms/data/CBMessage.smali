.class public Lcom/android/mms/data/CBMessage;
.super Ljava/lang/Object;
.source "CBMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/data/CBMessage$Cache;
    }
.end annotation


# static fields
.field private static final ALL_CHANNEL_PROJECTION:[Ljava/lang/String;

.field private static final ALL_MESSAGE_PROJECTION:[Ljava/lang/String;

.field private static final ALL_MESSAGE_URI:Landroid/net/Uri;

.field private static final BODY:I = 0x3

.field private static final CHANNEL_ID:I = 0x2

.field private static final CHANNEL_URI_SLOT_0:Landroid/net/Uri;

.field private static final CHANNEL_URI_SLOT_1:Landroid/net/Uri;

.field private static final CHANNEL_URI_SLOT_2:Landroid/net/Uri;

.field private static final CHANNEL_URI_SLOT_3:Landroid/net/Uri;

.field private static final DATE:I = 0x5

.field private static final DEBUG:Z = false

.field private static final ID:I = 0x0

.field private static final MESSAGE_URI:Landroid/net/Uri;

.field private static final READ:I = 0x4

.field private static final SIM_ID:I = 0x1

.field private static final TAG:Ljava/lang/String; = "CB/message"

.field private static final THREAD_ID:I = 0x6

.field public static final THREAD_MESSAGE_URI:Landroid/net/Uri;

.field private static sLoadingMessages:Z

.field private static sReadContentValues:Landroid/content/ContentValues;


# instance fields
.field private mAddressId:I

.field private mBody:Ljava/lang/String;

.field private mChannelId:I

.field private mChannelName:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mDate:J

.field private mId:J

.field private mRead:I

.field private mSimId:I

.field private mThreadId:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SmsCb;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/mms/data/CBMessage;->MESSAGE_URI:Landroid/net/Uri;

    sget-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SmsCb;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_URI:Landroid/net/Uri;

    const-string v0, "content://cb/messages/#"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/CBMessage;->THREAD_MESSAGE_URI:Landroid/net/Uri;

    const-string v0, "content://cb/channel"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_0:Landroid/net/Uri;

    const-string v0, "content://cb/channel1"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_1:Landroid/net/Uri;

    const-string v0, "content://cb/channel2"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_2:Landroid/net/Uri;

    const-string v0, "content://cb/channel3"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_3:Landroid/net/Uri;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sim_id"

    aput-object v1, v0, v4

    const-string v1, "channel_id"

    aput-object v1, v0, v5

    const-string v1, "body"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "read"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_PROJECTION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "number"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/mms/data/CBMessage;->ALL_CHANNEL_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/data/CBMessage;->mContext:Landroid/content/Context;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/mms/data/CBMessage;->mId:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;JZ)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/data/CBMessage;->mContext:Landroid/content/Context;

    invoke-direct {p0, p2, p3, p4}, Lcom/android/mms/data/CBMessage;->loadFromId(JZ)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/mms/data/CBMessage;->mId:J

    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/data/CBMessage;->mContext:Landroid/content/Context;

    invoke-static {p1, p0, p2, p3}, Lcom/android/mms/data/CBMessage;->fillFromCursor(Landroid/content/Context;Lcom/android/mms/data/CBMessage;Landroid/database/Cursor;Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/data/CBMessage;)I
    .locals 1
    .param p0    # Lcom/android/mms/data/CBMessage;

    iget v0, p0, Lcom/android/mms/data/CBMessage;->mThreadId:I

    return v0
.end method

.method static synthetic access$100(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/mms/data/CBMessage;->cacheAllMessages(Landroid/content/Context;)V

    return-void
.end method

.method private buildReadContentValues()V
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/android/mms/data/CBMessage;->sReadContentValues:Landroid/content/ContentValues;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    sput-object v0, Lcom/android/mms/data/CBMessage;->sReadContentValues:Landroid/content/ContentValues;

    sget-object v0, Lcom/android/mms/data/CBMessage;->sReadContentValues:Landroid/content/ContentValues;

    const-string v1, "read"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method private static cacheAllMessages(Landroid/content/Context;)V
    .locals 12
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string v0, "Mms:app"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[CBMessages] cacheAllThreads"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/mms/LogTag;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-static {}, Lcom/android/mms/data/CBMessage$Cache;->getInstance()Lcom/android/mms/data/CBMessage$Cache;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/mms/data/CBMessage;->sLoadingMessages:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/mms/data/CBMessage;->sLoadingMessages:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_4

    const-wide/16 v9, 0x0

    :goto_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/android/mms/data/CBMessage$Cache;->getInstance()Lcom/android/mms/data/CBMessage$Cache;

    move-result-object v1

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v9, v10}, Lcom/android/mms/data/CBMessage$Cache;->get(J)Lcom/android/mms/data/CBMessage;

    move-result-object v8

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-nez v8, :cond_3

    :try_start_3
    new-instance v8, Lcom/android/mms/data/CBMessage;

    const/4 v0, 0x1

    invoke-direct {v8, p0, v6, v0}, Lcom/android/mms/data/CBMessage;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {}, Lcom/android/mms/data/CBMessage$Cache;->getInstance()Lcom/android/mms/data/CBMessage$Cache;

    move-result-object v1

    monitor-enter v1
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-static {v8}, Lcom/android/mms/data/CBMessage$Cache;->put(Lcom/android/mms/data/CBMessage;)V

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_0
    move-exception v7

    :try_start_7
    const-string v0, "Tried to add duplicate CBMessages to Cache"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/mms/LogTag;->error(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    invoke-static {}, Lcom/android/mms/data/CBMessage$Cache;->getInstance()Lcom/android/mms/data/CBMessage$Cache;

    move-result-object v1

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_8
    sput-boolean v2, Lcom/android/mms/data/CBMessage;->sLoadingMessages:Z

    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    throw v0

    :catchall_2
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v0

    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :try_start_b
    throw v0

    :cond_3
    const/4 v0, 0x1

    invoke-static {p0, v8, v6, v0}, Lcom/android/mms/data/CBMessage;->fillFromCursor(Landroid/content/Context;Lcom/android/mms/data/CBMessage;Landroid/database/Cursor;Z)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_1

    :cond_4
    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    invoke-static {}, Lcom/android/mms/data/CBMessage$Cache;->getInstance()Lcom/android/mms/data/CBMessage$Cache;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_c
    sput-boolean v0, Lcom/android/mms/data/CBMessage;->sLoadingMessages:Z

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    invoke-static {v11}, Lcom/android/mms/data/CBMessage$Cache;->keepOnly(Ljava/util/Set;)V

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    :try_start_d
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v0

    :catchall_5
    move-exception v0

    :try_start_e
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v0
.end method

.method public static cleanup(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    return-void
.end method

.method public static createNew(Landroid/content/Context;)Lcom/android/mms/data/CBMessage;
    .locals 1
    .param p0    # Landroid/content/Context;

    new-instance v0, Lcom/android/mms/data/CBMessage;

    invoke-direct {v0, p0}, Lcom/android/mms/data/CBMessage;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static fillFromCursor(Landroid/content/Context;Lcom/android/mms/data/CBMessage;Landroid/database/Cursor;Z)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mms/data/CBMessage;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Z

    monitor-enter p1

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/android/mms/data/CBMessage;->mId:J

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p1, Lcom/android/mms/data/CBMessage;->mSimId:I

    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p1, Lcom/android/mms/data/CBMessage;->mChannelId:I

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/mms/data/CBMessage;->mBody:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p1, Lcom/android/mms/data/CBMessage;->mRead:I

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/android/mms/data/CBMessage;->mDate:J

    const/4 v0, 0x6

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p1, Lcom/android/mms/data/CBMessage;->mThreadId:I

    invoke-virtual {p1}, Lcom/android/mms/data/CBMessage;->getChannelName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/mms/data/CBMessage;->mChannelName:Ljava/lang/String;

    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static from(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/CBMessage;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/Cursor;

    new-instance v0, Lcom/android/mms/data/CBMessage;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/android/mms/data/CBMessage;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    return-object v0
.end method

.method public static declared-synchronized getCBChannelName(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    const-class v0, Lcom/android/mms/data/CBMessage;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, Lcom/android/mms/data/CBMessage;->getCBChannelNameGemini(II)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized getCBChannelNameGemini(II)Ljava/lang/String;
    .locals 10
    .param p0    # I
    .param p1    # I

    const-class v8, Lcom/android/mms/data/CBMessage;

    monitor-enter v8

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    :try_start_0
    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid soltId: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/android/mms/LogTag;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0062

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    :goto_0
    monitor-exit v8

    return-object v0

    :pswitch_0
    :try_start_1
    sget-object v1, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_0:Landroid/net/Uri;

    :goto_1
    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/mms/data/CBMessage;->ALL_CHANNEL_PROJECTION:[Ljava/lang/String;

    const-string v3, "number = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    if-eqz v6, :cond_1

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0062

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    if-eqz v6, :cond_0

    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    :pswitch_1
    :try_start_4
    sget-object v1, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_1:Landroid/net/Uri;

    goto :goto_1

    :pswitch_2
    sget-object v1, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_2:Landroid/net/Uri;

    goto :goto_1

    :pswitch_3
    sget-object v1, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_3:Landroid/net/Uri;

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/android/mms/data/CBMessage;->CHANNEL_URI_SLOT_0:Landroid/net/Uri;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :cond_3
    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    if-eqz v6, :cond_0

    :try_start_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getUri(J)Landroid/net/Uri;
    .locals 1
    .param p0    # J

    sget-object v0, Lcom/android/mms/data/CBMessage;->MESSAGE_URI:Landroid/net/Uri;

    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/data/CBMessage$1;

    invoke-direct {v1, p0}, Lcom/android/mms/data/CBMessage$1;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private loadFromId(JZ)Z
    .locals 8
    .param p1    # J
    .param p3    # Z

    const/4 v4, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/mms/data/CBMessage;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/data/CBMessage;->mContext:Landroid/content/Context;

    invoke-static {v0, p0, v6, p3}, Lcom/android/mms/data/CBMessage;->fillFromCursor(Landroid/content/Context;Lcom/android/mms/data/CBMessage;Landroid/database/Cursor;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadFromThreadId: Can\'t find thread ID "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/mms/LogTag;->error(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v7

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static loadingMessages()Z
    .locals 2

    invoke-static {}, Lcom/android/mms/data/CBMessage$Cache;->getInstance()Lcom/android/mms/data/CBMessage$Cache;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/mms/data/CBMessage;->sLoadingMessages:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static startDelete(Landroid/content/AsyncQueryHandler;IZJ)V
    .locals 6
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Z
    .param p3    # J

    const/4 v2, 0x0

    sget-object v0, Lcom/android/mms/data/CBMessage;->MESSAGE_URI:Landroid/net/Uri;

    invoke-static {v0, p3, p4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object v0, p0

    move v1, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public static startDeleteAll(Landroid/content/AsyncQueryHandler;IZ)V
    .locals 6
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Z

    const/4 v2, 0x0

    sget-object v3, Lcom/android/mms/data/CBMessage;->MESSAGE_URI:Landroid/net/Uri;

    move-object v0, p0

    move v1, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public static startQueryForAll(Landroid/content/AsyncQueryHandler;I)V
    .locals 8
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    sget-object v3, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_PROJECTION:[Ljava/lang/String;

    const-string v7, "date DESC"

    move-object v0, p0

    move v1, p1

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static startQueryForThreadId(Landroid/content/AsyncQueryHandler;JI)V
    .locals 8
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # J
    .param p3    # I

    const/4 v2, 0x0

    invoke-virtual {p0, p3}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    sget-object v0, Lcom/android/mms/data/CBMessage;->THREAD_MESSAGE_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/mms/data/CBMessage;->ALL_MESSAGE_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move v1, p3

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized clearThreadId()V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "Mms:app"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "clearThreadId old threadId was: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/mms/data/CBMessage;->mId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " now zero"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/mms/LogTag;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-wide v0, p0, Lcom/android/mms/data/CBMessage;->mId:J

    invoke-static {v0, v1}, Lcom/android/mms/data/CBMessage$Cache;->remove(J)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/mms/data/CBMessage;->mId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1    # Ljava/lang/Object;

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/android/mms/data/CBMessage;

    move-object v2, v0

    iget v4, p0, Lcom/android/mms/data/CBMessage;->mChannelId:I

    iget v5, v2, Lcom/android/mms/data/CBMessage;->mChannelId:I

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/android/mms/data/CBMessage;->mBody:Ljava/lang/String;

    iget-object v5, v2, Lcom/android/mms/data/CBMessage;->mBody:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-wide v4, p0, Lcom/android/mms/data/CBMessage;->mDate:J

    iget-wide v6, v2, Lcom/android/mms/data/CBMessage;->mDate:J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    :goto_0
    monitor-exit p0

    return v3

    :catch_0
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getBody()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/data/CBMessage;->mBody:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getChannelId()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/mms/data/CBMessage;->mChannelId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getChannelName()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/data/CBMessage;->mChannelName:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/mms/data/CBMessage;->mChannelId:I

    iget v1, p0, Lcom/android/mms/data/CBMessage;->mSimId:I

    invoke-static {v0, v1}, Lcom/android/mms/data/CBMessage;->getCBChannelNameGemini(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/data/CBMessage;->mChannelName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/android/mms/data/CBMessage;->mChannelName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDate()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/android/mms/data/CBMessage;->mDate:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDisplayName()Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/mms/data/CBMessage;->getChannelName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/data/CBMessage;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0062

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mms/data/CBMessage;->getChannelId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getMessageId()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/android/mms/data/CBMessage;->mId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/android/mms/data/CBMessage;->mSimId:I

    return v0
.end method

.method public declared-synchronized getThreadId()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/mms/data/CBMessage;->mThreadId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUri()Landroid/net/Uri;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/mms/data/CBMessage;->mThreadId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/android/mms/data/CBMessage;->MESSAGE_URI:Landroid/net/Uri;

    iget v1, p0, Lcom/android/mms/data/CBMessage;->mThreadId:I

    int-to-long v1, v1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hashCode()I
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/mms/data/CBMessage;->mBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/mms/data/CBMessage;->mDate:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/mms/data/CBMessage;->mChannelId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setChannelName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/android/mms/data/CBMessage;->mChannelName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
