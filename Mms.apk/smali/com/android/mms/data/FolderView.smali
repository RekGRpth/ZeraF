.class public Lcom/android/mms/data/FolderView;
.super Ljava/lang/Object;
.source "FolderView.java"


# static fields
.field private static final ADDRESS:I = 0x2

.field private static final ATTACHMENT:I = 0x8

.field private static final BOXTYPE:I = 0xb

.field private static final DATE:I = 0x4

.field private static final DRAFTFOLDER_URI:Landroid/net/Uri;

.field private static final ID:I = 0x0

.field private static final INBOXFOLDER_URI:Landroid/net/Uri;

.field private static final LOCKED:I = 0xd

.field private static final M_TYPE:I = 0x9

.field private static final OUTBOXFOLDER_URI:Landroid/net/Uri;

.field private static final READ:I = 0x5

.field private static final SENDBOXFOLDER_URI:Landroid/net/Uri;

.field private static final SILENT_SELECTION:Ljava/lang/String; = "(ThreadSettings.NOTIFICATION_ENABLE = 0) OR (ThreadSettings.MUTE > 0 and ThreadSettings.MUTE_START > 0)"

.field private static final SIM_ID:I = 0xa

.field private static final STATUS:I = 0x7

.field private static final SUBJECT:I = 0x3

.field private static final SUB_CS:I = 0xc

.field private static final TAG:Ljava/lang/String; = "FolderView"

.field private static final THREAD_ID:I = 0x1

.field private static final THREAD_URI_FOR_RECEIPIENTS:Landroid/net/Uri;

.field private static final TYPE:I = 0x6

.field private static mMuteCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/mms/util/MuteCache;",
            ">;"
        }
    .end annotation
.end field

.field private static sContext:Landroid/content/Context;


# instance fields
.field private mBoxType:I

.field private mDate:J

.field private mHasAttachment:Z

.field private mHasError:Z

.field private mHasUnreadMessages:Z

.field private mId:I

.field private mIsMute:Z

.field private mLocked:Z

.field private mRecipientString:Lcom/android/mms/data/ContactList;

.field private mSimId:I

.field private mStatus:I

.field private mSubject:Ljava/lang/String;

.field private mThreadId:J

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Landroid/provider/Telephony$Threads;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/FolderView;->THREAD_URI_FOR_RECEIPIENTS:Landroid/net/Uri;

    const-string v0, "content://mms-sms/draftbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/FolderView;->DRAFTFOLDER_URI:Landroid/net/Uri;

    const-string v0, "content://mms-sms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/FolderView;->INBOXFOLDER_URI:Landroid/net/Uri;

    const-string v0, "content://mms-sms/outbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/FolderView;->OUTBOXFOLDER_URI:Landroid/net/Uri;

    const-string v0, "content://mms-sms/sentbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/data/FolderView;->SENDBOXFOLDER_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sput-object p1, Lcom/android/mms/data/FolderView;->sContext:Landroid/content/Context;

    new-instance v0, Lcom/android/mms/data/ContactList;

    invoke-direct {v0}, Lcom/android/mms/data/ContactList;-><init>()V

    iput-object v0, p0, Lcom/android/mms/data/FolderView;->mRecipientString:Lcom/android/mms/data/ContactList;

    return-void
.end method

.method static synthetic access$000()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/mms/data/FolderView;->DRAFTFOLDER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/mms/data/FolderView;->INBOXFOLDER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/mms/data/FolderView;->markFailedSmsSeen(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$300(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/mms/data/FolderView;->markOutboxMmsSeen(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/mms/data/FolderView;->OUTBOXFOLDER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$500()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/mms/data/FolderView;->SENDBOXFOLDER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method private static fillFromCursor(Landroid/content/Context;Lcom/android/mms/data/FolderView;Landroid/database/Cursor;Z)V
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mms/data/FolderView;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Z

    monitor-enter p1

    const/4 v1, 0x0

    :try_start_0
    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p1, Lcom/android/mms/data/FolderView;->mId:I

    const/4 v1, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p1, Lcom/android/mms/data/FolderView;->mThreadId:J

    const/4 v1, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p1, Lcom/android/mms/data/FolderView;->mDate:J

    const/4 v1, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p1, Lcom/android/mms/data/FolderView;->mHasUnreadMessages:Z

    const/4 v1, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/android/mms/data/FolderView;->mSubject:Ljava/lang/String;

    const/4 v1, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p1, Lcom/android/mms/data/FolderView;->mType:I

    const/16 v1, 0xb

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p1, Lcom/android/mms/data/FolderView;->mBoxType:I

    const/4 v1, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p1, Lcom/android/mms/data/FolderView;->mStatus:I

    iget v1, p1, Lcom/android/mms/data/FolderView;->mBoxType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/android/mms/data/FolderView;->mStatus:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_5

    :cond_0
    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p1, Lcom/android/mms/data/FolderView;->mHasError:Z

    const/16 v1, 0x8

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p1, Lcom/android/mms/data/FolderView;->mHasAttachment:Z

    const/16 v1, 0xa

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p1, Lcom/android/mms/data/FolderView;->mSimId:I

    const/16 v1, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_7

    const/4 v1, 0x1

    :goto_3
    iput-boolean v1, p1, Lcom/android/mms/data/FolderView;->mLocked:Z

    iget v1, p1, Lcom/android/mms/data/FolderView;->mBoxType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_b

    iget-wide v1, p1, Lcom/android/mms/data/FolderView;->mThreadId:J

    invoke-static {v1, v2}, Lcom/android/mms/util/MuteCache;->getMuteEntry(J)Lcom/android/mms/util/MuteEntry;

    move-result-object v1

    if-nez v1, :cond_9

    invoke-static {p0}, Lcom/android/mms/ui/MessageUtils;->checkAppSettingsNeedNotify(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    :goto_4
    iput-boolean v1, p1, Lcom/android/mms/data/FolderView;->mIsMute:Z

    const-string v1, "FolderView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fview.mIsMute =  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p1, Lcom/android/mms/data/FolderView;->mIsMute:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget v1, p1, Lcom/android/mms/data/FolderView;->mType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    iget v1, p1, Lcom/android/mms/data/FolderView;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget v1, p1, Lcom/android/mms/data/FolderView;->mBoxType:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    :cond_1
    iget v1, p1, Lcom/android/mms/data/FolderView;->mType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_c

    :cond_2
    move/from16 v0, p3

    invoke-static {v9, v0}, Lcom/android/mms/data/ContactList;->getByIds(Ljava/lang/String;Z)Lcom/android/mms/data/ContactList;

    move-result-object v10

    :goto_6
    iget v1, p1, Lcom/android/mms/data/FolderView;->mType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget-object v1, p1, Lcom/android/mms/data/FolderView;->mSubject:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    new-instance v11, Lcom/google/android/mms/pdu/EncodedStringValue;

    const/16 v1, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iget-object v2, p1, Lcom/android/mms/data/FolderView;->mSubject:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/mms/pdu/PduPersister;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v11, v1, v2}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(I[B)V

    invoke-virtual {v11}, Lcom/google/android/mms/pdu/EncodedStringValue;->getString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/android/mms/data/FolderView;->mSubject:Ljava/lang/String;

    :cond_3
    :goto_7
    monitor-enter p1

    :try_start_1
    iput-object v10, p1, Lcom/android/mms/data/FolderView;->mRecipientString:Lcom/android/mms/data/ContactList;

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const-string v1, "FolderView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mRecipientString"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/mms/data/FolderView;->mRecipientString:Lcom/android/mms/data/ContactList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_9
    :try_start_2
    iget-wide v2, p1, Lcom/android/mms/data/FolderView;->mThreadId:J

    invoke-static {}, Lcom/android/mms/util/MuteCache;->getInstance()Lcom/android/mms/util/MuteCache;

    move-result-object v1

    iget-wide v4, p1, Lcom/android/mms/data/FolderView;->mThreadId:J

    invoke-virtual {v1, v4, v5}, Lcom/android/mms/util/MuteCache;->getMute(J)J

    move-result-wide v4

    invoke-static {}, Lcom/android/mms/util/MuteCache;->getInstance()Lcom/android/mms/util/MuteCache;

    move-result-object v1

    iget-wide v6, p1, Lcom/android/mms/data/FolderView;->mThreadId:J

    invoke-virtual {v1, v6, v7}, Lcom/android/mms/util/MuteCache;->getMuteStart(J)J

    move-result-wide v6

    invoke-static {}, Lcom/android/mms/util/MuteCache;->getInstance()Lcom/android/mms/util/MuteCache;

    move-result-object v1

    iget-wide v12, p1, Lcom/android/mms/data/FolderView;->mThreadId:J

    invoke-virtual {v1, v12, v13}, Lcom/android/mms/util/MuteCache;->getMuteEnable(J)Z

    move-result v8

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/android/mms/ui/MessageUtils;->checkNeedNotifyForFolderMode(Landroid/content/Context;JJJZ)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    :goto_8
    iput-boolean v1, p1, Lcom/android/mms/data/FolderView;->mIsMute:Z

    goto/16 :goto_5

    :catchall_0
    move-exception v1

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_a
    const/4 v1, 0x0

    goto :goto_8

    :cond_b
    const/4 v1, 0x0

    :try_start_3
    iput-boolean v1, p1, Lcom/android/mms/data/FolderView;->mIsMute:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_5

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v9, v1, v2}, Lcom/android/mms/data/ContactList;->getByNumbers(Ljava/lang/String;ZZ)Lcom/android/mms/data/ContactList;

    move-result-object v10

    const-string v1, "FolderView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recipients "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Ljava/util/AbstractCollection;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_d
    const v1, 0x7f0b0221

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/android/mms/data/FolderView;->mSubject:Ljava/lang/String;

    goto/16 :goto_7

    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method public static from(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/FolderView;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/Cursor;

    new-instance v0, Lcom/android/mms/data/FolderView;

    invoke-direct {v0, p0}, Lcom/android/mms/data/FolderView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/android/mms/data/FolderView;->fillFromCursor(Landroid/content/Context;Lcom/android/mms/data/FolderView;Landroid/database/Cursor;Z)V

    return-object v0
.end method

.method public static markFailedSmsMmsSeen(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-string v0, "FolderView"

    const-string v1, "markFailedSmsMmsRead"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/data/FolderView$5;

    invoke-direct {v1, p0}, Lcom/android/mms/data/FolderView$5;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private static markFailedSmsSeen(Landroid/content/Context;)V
    .locals 6
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v0, "seen"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "type = 4 or type = 6 or type = 5"

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Landroid/database/sqlite/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private static markOutboxMmsSeen(Landroid/content/Context;)V
    .locals 6
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v0, "seen"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "msg_box = 4"

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Landroid/database/sqlite/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static startDeleteBoxMessage(Landroid/content/AsyncQueryHandler;ILandroid/net/Uri;Ljava/lang/String;)V
    .locals 6
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;

    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public static startQueryForDraftboxView(Landroid/content/AsyncQueryHandler;I)V
    .locals 5
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v1, p1

    move-object v0, p0

    const-string v2, "FolderView"

    const-string v3, "startQueryForDraftboxView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/android/mms/data/FolderView$1;

    invoke-direct {v2, v0, v1}, Lcom/android/mms/data/FolderView$1;-><init>(Landroid/content/AsyncQueryHandler;I)V

    const-wide/16 v3, 0xa

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static startQueryForDraftboxView(Landroid/content/AsyncQueryHandler;II)V
    .locals 6
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v2, p1

    move v0, p2

    move-object v1, p0

    const-string v3, "FolderView"

    const-string v4, "startQueryForDraftboxView"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/android/mms/data/FolderView$2;

    invoke-direct {v3, v1, v2}, Lcom/android/mms/data/FolderView$2;-><init>(Landroid/content/AsyncQueryHandler;I)V

    int-to-long v4, p2

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static startQueryForInboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V
    .locals 6
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v2, p1

    move-object v1, p0

    move-object v0, p2

    const-string v3, "FolderView"

    const-string v4, "startQueryForInboxView"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/android/mms/data/FolderView$3;

    invoke-direct {v3, v1, v2, v0}, Lcom/android/mms/data/FolderView$3;-><init>(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static startQueryForInboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;I)V
    .locals 7
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v3, p1

    move-object v2, p0

    move v1, p3

    move-object v0, p2

    const-string v4, "FolderView"

    const-string v5, "startQueryForInboxView"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/android/mms/data/FolderView$4;

    invoke-direct {v4, v2, v3, v0}, Lcom/android/mms/data/FolderView$4;-><init>(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    int-to-long v5, v1

    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static startQueryForOutBoxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V
    .locals 6
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v2, p1

    move-object v1, p0

    move-object v0, p2

    const-string v3, "FolderView"

    const-string v4, "startQueryForOutBoxView"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/android/mms/data/FolderView$6;

    invoke-direct {v3, v1, v2, v0}, Lcom/android/mms/data/FolderView$6;-><init>(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static startQueryForOutBoxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;I)V
    .locals 7
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v3, p1

    move-object v2, p0

    move-object v0, p2

    move v1, p3

    const-string v4, "FolderView"

    const-string v5, "startQueryForOutBoxView"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/android/mms/data/FolderView$7;

    invoke-direct {v4, v2, v3, v0}, Lcom/android/mms/data/FolderView$7;-><init>(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    int-to-long v5, v1

    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static startQueryForSentboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V
    .locals 6
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v2, p1

    move-object v1, p0

    move-object v0, p2

    const-string v3, "FolderView"

    const-string v4, "startQueryForSentboxView"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/android/mms/data/FolderView$8;

    invoke-direct {v3, v1, v2, v0}, Lcom/android/mms/data/FolderView$8;-><init>(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static startQueryForSentboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;I)V
    .locals 7
    .param p0    # Landroid/content/AsyncQueryHandler;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p0, p1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    move v3, p1

    move-object v2, p0

    move-object v0, p2

    move v1, p3

    const-string v4, "FolderView"

    const-string v5, "startQueryForSentboxView"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/android/mms/data/FolderView$9;

    invoke-direct {v4, v2, v3, v0}, Lcom/android/mms/data/FolderView$9;-><init>(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    int-to-long v5, p3

    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public declared-synchronized getmDate()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/android/mms/data/FolderView;->mDate:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmHasAttachment()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/mms/data/FolderView;->mHasAttachment:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmId()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/mms/data/FolderView;->mId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmRead()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/mms/data/FolderView;->mHasUnreadMessages:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmRecipientString()Lcom/android/mms/data/ContactList;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/data/FolderView;->mRecipientString:Lcom/android/mms/data/ContactList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmSimId()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/mms/data/FolderView;->mSimId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmStatus()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/mms/data/FolderView;->mStatus:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmSubject()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/data/FolderView;->mSubject:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getmType()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/mms/data/FolderView;->mType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasError()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/mms/data/FolderView;->mHasError:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isLocked()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/mms/data/FolderView;->mLocked:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isMute()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/mms/data/FolderView;->mIsMute:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
