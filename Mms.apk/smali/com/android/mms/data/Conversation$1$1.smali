.class Lcom/android/mms/data/Conversation$1$1;
.super Ljava/lang/Object;
.source "Conversation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/data/Conversation$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/data/Conversation$1;

.field final synthetic val$allNeedUpdateCount:I

.field final synthetic val$conv:Lcom/android/mms/data/Conversation;


# direct methods
.method constructor <init>(Lcom/android/mms/data/Conversation$1;ILcom/android/mms/data/Conversation;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/data/Conversation$1$1;->this$1:Lcom/android/mms/data/Conversation$1;

    iput p2, p0, Lcom/android/mms/data/Conversation$1$1;->val$allNeedUpdateCount:I

    iput-object p3, p0, Lcom/android/mms/data/Conversation$1$1;->val$conv:Lcom/android/mms/data/Conversation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v10, 0x0

    const/16 v1, 0x64

    iget v2, p0, Lcom/android/mms/data/Conversation$1$1;->val$allNeedUpdateCount:I

    const/16 v3, 0x64

    if-lt v2, v3, :cond_0

    :goto_0
    if-lez v1, :cond_1

    iget-object v2, p0, Lcom/android/mms/data/Conversation$1$1;->this$1:Lcom/android/mms/data/Conversation$1;

    iget-object v2, v2, Lcom/android/mms/data/Conversation$1;->this$0:Lcom/android/mms/data/Conversation;

    invoke-static {v2}, Lcom/android/mms/data/Conversation;->access$100(Lcom/android/mms/data/Conversation;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/data/Conversation$1$1;->this$1:Lcom/android/mms/data/Conversation$1;

    iget-object v3, v3, Lcom/android/mms/data/Conversation$1;->val$threadUri:Landroid/net/Uri;

    invoke-static {}, Lcom/android/mms/data/Conversation;->access$300()Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "(read=0 OR seen=0) and _id in (select _id from sms where (read=0 OR seen=0) and thread_id=? order by _id limit 0,100)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/mms/data/Conversation$1$1;->this$1:Lcom/android/mms/data/Conversation$1;

    iget-object v8, v8, Lcom/android/mms/data/Conversation$1;->this$0:Lcom/android/mms/data/Conversation;

    invoke-static {v8}, Lcom/android/mms/data/Conversation;->access$400(Lcom/android/mms/data/Conversation;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const-string v2, "Mms/conv"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "markAsRead-updateThread: updateCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    monitor-enter p0

    const-wide/16 v2, 0xc8

    :try_start_0
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "Mms/conv"

    const-string v3, "InterruptedException"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/android/mms/data/Conversation$1$1;->this$1:Lcom/android/mms/data/Conversation$1;

    iget-object v2, v2, Lcom/android/mms/data/Conversation$1;->this$0:Lcom/android/mms/data/Conversation;

    invoke-static {v2}, Lcom/android/mms/data/Conversation;->access$100(Lcom/android/mms/data/Conversation;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/data/Conversation$1$1;->this$1:Lcom/android/mms/data/Conversation$1;

    iget-object v3, v3, Lcom/android/mms/data/Conversation$1;->val$threadUri:Landroid/net/Uri;

    invoke-static {}, Lcom/android/mms/data/Conversation;->access$300()Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "(read=0 OR seen=0)"

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const-string v2, "Mms/conv"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "markAsRead-updateThread: updateCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/android/mms/data/Conversation$1$1;->val$conv:Lcom/android/mms/data/Conversation;

    invoke-virtual {v2, v10}, Lcom/android/mms/data/Conversation;->setIsDoingMarkAsRead(Z)V

    iget-object v2, p0, Lcom/android/mms/data/Conversation$1$1;->this$1:Lcom/android/mms/data/Conversation$1;

    iget-object v2, v2, Lcom/android/mms/data/Conversation$1;->this$0:Lcom/android/mms/data/Conversation;

    invoke-static {v2}, Lcom/android/mms/data/Conversation;->access$100(Lcom/android/mms/data/Conversation;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mms/transaction/MessagingNotification;->blockingUpdateAllNotifications(Landroid/content/Context;)V

    return-void
.end method
