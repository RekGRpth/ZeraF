.class final Lcom/mediatek/ipmsg/util/IpMessageUtils$5;
.super Ljava/lang/Object;
.source "IpMessageUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/ipmsg/util/IpMessageUtils;->showActivateOrEnableIpMessageServiceDialog(Landroid/app/Activity;Ljava/util/List;IILandroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activityContext:Landroid/app/Activity;

.field final synthetic val$ipMsgHandler:Landroid/os/Handler;

.field final synthetic val$mode:I

.field final synthetic val$simId:I

.field final synthetic val$simInfoList:Ljava/util/List;


# direct methods
.method constructor <init>(IILandroid/app/Activity;Ljava/util/List;Landroid/os/Handler;)V
    .locals 0

    iput p1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$mode:I

    iput p2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    iput-object p3, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    iput-object p4, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simInfoList:Ljava/util/List;

    iput-object p5, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$ipMsgHandler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/16 v3, 0x94

    const/4 v2, -0x1

    const/4 v4, 0x1

    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$mode:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    iget v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    invoke-static {v1, v4, v2}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->access$200(Landroid/app/Activity;II)V

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    invoke-static {v1, v4, v2}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->access$200(Landroid/app/Activity;II)V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    iget-object v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simInfoList:Ljava/util/List;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$ipMsgHandler:Landroid/os/Handler;

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->access$300(Landroid/app/Activity;Ljava/util/List;ILandroid/os/Handler;)V

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ServiceManager;->enableIpService(I)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    iget-object v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    invoke-static {v2}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$ipMsgHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$ipMsgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->enableIpService()V

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    iget-object v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    invoke-static {v2}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_5
    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simId:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$activityContext:Landroid/app/Activity;

    iget-object v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$simInfoList:Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$5;->val$ipMsgHandler:Landroid/os/Handler;

    invoke-static {v1, v2, v4, v3}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->access$300(Landroid/app/Activity;Ljava/util/List;ILandroid/os/Handler;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
