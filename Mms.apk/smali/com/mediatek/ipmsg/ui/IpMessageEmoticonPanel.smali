.class public Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
.super Lcom/android/mms/ui/EmoticonPanel;
.source "IpMessageEmoticonPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;,
        Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/IpMessageEmoticonPanel"


# instance fields
.field private mAdIndex:I

.field private mAdName:[Ljava/lang/String;

.field private mAdTab:Landroid/widget/RadioButton;

.field private mColumnArray:[I

.field private mContext:Landroid/content/Context;

.field private mDelEmoticon:Landroid/widget/Button;

.field private mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

.field private mDotFirst:Landroid/widget/RadioButton;

.field private mDotForth:Landroid/widget/RadioButton;

.field private mDotSec:Landroid/widget/RadioButton;

.field private mDotThird:Landroid/widget/RadioButton;

.field private mDynamicIndex:I

.field private mDynamicName:[Ljava/lang/String;

.field private mDynamicTab:Landroid/widget/RadioButton;

.field private mEmoticonName:[Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mLargeIndex:I

.field private mLargeName:[Ljava/lang/String;

.field private mLargeTab:Landroid/widget/RadioButton;

.field private mListener:Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

.field private mNeedQuickDelete:Z

.field private mNormalIndex:I

.field private mNormalTab:Landroid/widget/RadioButton;

.field private mObject:Ljava/lang/Object;

.field private mOrientation:I

.field private mPreview:Lcom/mediatek/ipmsg/ui/EmoticonPreview;

.field private mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

.field private mSharePanelMain:Landroid/widget/LinearLayout;

.field private mXmIndex:I

.field private mXmName:[Ljava/lang/String;

.field private mXmTab:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/mms/ui/EmoticonPanel;-><init>(Landroid/content/Context;)V

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mObject:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNeedQuickDelete:Z

    iput-object p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/EmoticonPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    iput v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mObject:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNeedQuickDelete:Z

    iput-object p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mListener:Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V
    .locals 0
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->startDelEmoticon()V

    return-void
.end method

.method static synthetic access$1002(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)I
    .locals 0
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    return p1
.end method

.method static synthetic access$1102(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)I
    .locals 0
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getEmoticonName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Lcom/mediatek/ipmsg/ui/EmoticonPreview;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mPreview:Lcom/mediatek/ipmsg/ui/EmoticonPreview;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getLargeName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)I
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getPreviewIcon(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mObject:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-boolean v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNeedQuickDelete:Z

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V
    .locals 0
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->stopDelEmoticon()V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)I
    .locals 0
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalIndex:I

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$802(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)I
    .locals 0
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    return p1
.end method

.method static synthetic access$902(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;I)I
    .locals 0
    .param p0    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    return p1
.end method

.method private addAdPage(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040001

    iget-object v7, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f0004

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v9, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    :goto_1
    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getAdIconArray(I)[I

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;[I)V

    invoke-direct {p0, v4, v1, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->updateGridView(Landroid/view/View;Landroid/widget/GridView;Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v9

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1
.end method

.method private addAdPanel()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v2, v5, :cond_3

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateDynamicPageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addAdPage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    iput v6, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    :cond_2
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$7;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$7;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    :goto_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setDefaultScreen(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_2
    return-void

    :cond_3
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateDynamicPageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_4

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addAdPage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$8;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$8;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    if-ne v2, v5, :cond_6

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_6
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    if-ne v2, v6, :cond_7

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2
.end method

.method private addDynamicPage(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f04001f

    iget-object v7, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f0086

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v9, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    :goto_1
    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getDynamicIconArray(I)[I

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;[I)V

    invoke-direct {p0, v4, v1, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->updateGridView(Landroid/view/View;Landroid/widget/GridView;Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v9

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1
.end method

.method private addDynamicPanel()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v2, v5, :cond_3

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateDynamicPageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addDynamicPage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    iput v6, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    :cond_2
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$5;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$5;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    :goto_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setDefaultScreen(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_2
    return-void

    :cond_3
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateDynamicPageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_4

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addDynamicPage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$6;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$6;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    if-ne v2, v5, :cond_6

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_6
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    if-ne v2, v6, :cond_7

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2
.end method

.method private addLargePage(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040034

    iget-object v7, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f00c7

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v9, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    :goto_1
    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getLargeIconArray(I)[I

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;[I)V

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v5, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$12;

    invoke-direct {v5, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$12;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v1, v5}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v9

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1
.end method

.method private addLargePanel()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v2, v5, :cond_3

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateLargePageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addLargePage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    iput v6, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    :cond_2
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$3;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$3;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    :goto_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setDefaultScreen(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_2
    return-void

    :cond_3
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateLargePageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_4

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addLargePage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$4;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$4;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    if-ne v2, v5, :cond_6

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_6
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    if-ne v2, v6, :cond_7

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2
.end method

.method private addNormalPage(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f04004a

    iget-object v7, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f0139

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v9, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    :goto_1
    new-instance v0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getNormalIconArray(I)[I

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;-><init>(Lcom/android/mms/ui/EmoticonPanel;[I)V

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v5, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$11;

    invoke-direct {v5, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$11;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v1, v5}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v9

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1
.end method

.method private addNormalPanel()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setDefaultScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateNormalPageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addNormalPage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$2;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$2;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    return-void
.end method

.method private addXmPage(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040078

    iget-object v7, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f018f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v9, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    :goto_1
    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->getXmIconArray(I)[I

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;[I)V

    invoke-direct {p0, v4, v1, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->updateGridView(Landroid/view/View;Landroid/widget/GridView;Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v9

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1
.end method

.method private addXmPanel()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v2, v5, :cond_3

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateDynamicPageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addXmPage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    iput v6, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    :cond_2
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$9;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$9;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    :goto_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setDefaultScreen(I)V

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_2
    return-void

    :cond_3
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    invoke-direct {p0, v2}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->calculateDynamicPageCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_4

    invoke-direct {p0, v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addXmPage(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v3, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$10;

    invoke-direct {v3, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$10;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    if-ne v2, v5, :cond_6

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_6
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    if-ne v2, v6, :cond_7

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2
.end method

.method private calculateDynamicPageCount(I)I
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    mul-int/lit8 v1, v3, 0x2

    :goto_0
    sget-object v3, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->DYNAMIC_ICON_ARR:[I

    array-length v2, v3

    div-int v0, v2, v1

    mul-int v3, v0, v1

    if-le v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v1, v3, v4

    goto :goto_0
.end method

.method private calculateLargePageCount(I)I
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    mul-int/lit8 v1, v3, 0x2

    :goto_0
    sget-object v3, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->LARGE_ICON_ARR:[I

    array-length v2, v3

    div-int v0, v2, v1

    mul-int v3, v0, v1

    if-le v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v1, v3, v4

    goto :goto_0
.end method

.method private calculateNormalPageCount(I)I
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    mul-int/lit8 v1, v3, 0x4

    :goto_0
    sget-object v3, Lcom/android/mms/util/MessageConsts;->emoticonIdList:[I

    array-length v2, v3

    div-int v0, v2, v1

    mul-int v3, v0, v1

    if-le v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v3, v3, v4

    mul-int/lit8 v1, v3, 0x2

    goto :goto_0
.end method

.method private getAdIconArray(I)[I
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    sget-object v4, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->AD_PNG_ICON_ARR:[I

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    mul-int/lit8 v3, v5, 0x2

    :goto_0
    new-array v0, v3, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    mul-int v5, p1, v3

    add-int v2, v5, v1

    array-length v5, v4

    if-lt v2, v5, :cond_2

    :cond_0
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v3, v5, v6

    goto :goto_0

    :cond_2
    mul-int v5, p1, v3

    add-int/2addr v5, v1

    aget v5, v4, v5

    aput v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getDynamicIconArray(I)[I
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    sget-object v4, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->DYNAMIC_PNG_ICON_ARR:[I

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    mul-int/lit8 v3, v5, 0x2

    :goto_0
    new-array v0, v3, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    mul-int v5, p1, v3

    add-int v2, v5, v1

    array-length v5, v4

    if-lt v2, v5, :cond_2

    :cond_0
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v3, v5, v6

    goto :goto_0

    :cond_2
    mul-int v5, p1, v3

    add-int/2addr v5, v1

    aget v5, v4, v5

    aput v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getEmoticonName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    mul-int/lit8 v1, v3, 0x4

    :goto_0
    const/16 v3, 0x14

    if-lt p1, v3, :cond_2

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v3, v3, v4

    mul-int/lit8 v1, v3, 0x2

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalIndex:I

    mul-int/2addr v3, v1

    add-int v0, p1, v3

    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mEmoticonName:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mEmoticonName:[Ljava/lang/String;

    aget-object v2, v2, v0

    goto :goto_1
.end method

.method private getLargeIconArray(I)[I
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    sget-object v4, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->LARGE_ICON_ARR:[I

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    mul-int/lit8 v3, v5, 0x2

    :goto_0
    new-array v0, v3, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    mul-int v5, p1, v3

    add-int v2, v5, v1

    array-length v5, v4

    if-lt v2, v5, :cond_2

    :cond_0
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v3, v5, v6

    goto :goto_0

    :cond_2
    mul-int v5, p1, v3

    add-int/2addr v5, v1

    aget v5, v4, v5

    aput v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getLargeName(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    iget v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-int/lit8 v0, v1, 0x2

    :goto_0
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    if-lt p1, v0, :cond_1

    const-string v1, ""

    :goto_1
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v0, v1, v2

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeName:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeIndex:I

    mul-int/2addr v2, v0

    add-int/2addr v2, p1

    aget-object v1, v1, v2

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    if-lt p1, v0, :cond_3

    const-string v1, ""

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicName:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    mul-int/2addr v2, v0

    add-int/2addr v2, p1

    aget-object v1, v1, v2

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_6

    if-lt p1, v0, :cond_5

    const-string v1, ""

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdName:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    mul-int/2addr v2, v0

    add-int/2addr v2, p1

    aget-object v1, v1, v2

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_8

    if-lt p1, v0, :cond_7

    const-string v1, ""

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmName:[Ljava/lang/String;

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    mul-int/2addr v2, v0

    add-int/2addr v2, p1

    aget-object v1, v1, v2

    goto :goto_1

    :cond_8
    const-string v1, ""

    goto :goto_1
.end method

.method private getNormalIconArray(I)[I
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    sget-object v4, Lcom/android/mms/util/MessageConsts;->emoticonIdList:[I

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    mul-int/lit8 v3, v5, 0x4

    :goto_0
    new-array v0, v3, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    mul-int v5, p1, v3

    add-int v2, v5, v1

    array-length v5, v4

    if-lt v2, v5, :cond_2

    :cond_0
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v5, v5, v6

    mul-int/lit8 v3, v5, 0x2

    goto :goto_0

    :cond_2
    mul-int v5, p1, v3

    add-int/2addr v5, v1

    aget v5, v4, v5

    aput v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getPreviewIcon(I)I
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v2, v2, v1

    mul-int/lit8 v0, v2, 0x2

    :goto_1
    if-ge p1, v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v1, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->DYNAMIC_ICON_ARR:[I

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicIndex:I

    mul-int/2addr v2, v0

    add-int/2addr v2, p1

    aget v1, v1, v2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v0, v2, v3

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->AD_ICON_ARR:[I

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdIndex:I

    mul-int/2addr v2, v0

    add-int/2addr v2, p1

    aget v1, v1, v2

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->XM_ICON_ARR:[I

    iget v2, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmIndex:I

    mul-int/2addr v2, v0

    add-int/2addr v2, p1

    aget v1, v1, v2

    goto :goto_0
.end method

.method private getXmIconArray(I)[I
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    sget-object v4, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->XM_PNG_ICON_ARR:[I

    iget v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mOrientation:I

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    mul-int/lit8 v3, v5, 0x2

    :goto_0
    new-array v0, v3, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    mul-int v5, p1, v3

    add-int v2, v5, v1

    array-length v5, v4

    if-lt v2, v5, :cond_2

    :cond_0
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mColumnArray:[I

    aget v3, v5, v6

    goto :goto_0

    :cond_2
    mul-int v5, p1, v3

    add-int/2addr v5, v1

    aget v5, v4, v5

    aput v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private startDelEmoticon()V
    .locals 2

    const-string v0, "Mms/IpMessageEmoticonPanel"

    const-string v1, "Delete one emoticon."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->stopDelEmoticon()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNeedQuickDelete:Z

    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$1;)V

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private stopDelEmoticon()V
    .locals 2

    const-string v0, "Mms/IpMessageEmoticonPanel"

    const-string v1, "Stop quick delete."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNeedQuickDelete:Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    invoke-virtual {v0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;->stopThread()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticonThread:Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$DelEmoticonThread;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateGridView(Landroid/view/View;Landroid/widget/GridView;Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/widget/GridView;
    .param p3    # Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$LargeEmoticonAdapter;

    invoke-virtual {p2, p3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$13;

    invoke-direct {v0, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$13;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {p2, v0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$14;

    invoke-direct {v0, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$14;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {p2, v0}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    new-instance v0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$15;

    invoke-direct {v0, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$15;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0f00a5

    if-ne v1, v0, :cond_2

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addNormalPanel()V

    goto :goto_0

    :cond_2
    const v1, 0x7f0f00a9

    if-ne v1, v0, :cond_3

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addLargePanel()V

    goto :goto_0

    :cond_3
    const v1, 0x7f0f00a8

    if-ne v1, v0, :cond_4

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addDynamicPanel()V

    goto :goto_0

    :cond_4
    const v1, 0x7f0f00a6

    if-ne v1, v0, :cond_5

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addXmPanel()V

    goto/16 :goto_0

    :cond_5
    const v1, 0x7f0f00a7

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addAdPanel()V

    goto/16 :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    const v0, 0x7f0f0099

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/LevelControlLayout;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iput-object p0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    const v0, 0x7f0f009b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    const v0, 0x7f0f009c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    const v0, 0x7f0f009d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    const v0, 0x7f0f009e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    const v0, 0x7f0f00a5

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    const v0, 0x7f0f00a9

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    const v0, 0x7f0f00a6

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    const v0, 0x7f0f00a7

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mEmoticonName:[Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeName:[Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicName:[Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdName:[Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmName:[Ljava/lang/String;

    const v0, 0x7f0f00a4

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticon:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDelEmoticon:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$1;

    invoke-direct {v1, p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel$1;-><init>(Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/mediatek/ipmsg/ui/EmoticonPreview;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mPreview:Lcom/mediatek/ipmsg/ui/EmoticonPreview;

    invoke-virtual {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->resetShareItem()V

    return-void
.end method

.method public recycleView()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    return-void
.end method

.method public resetShareItem()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mNormalTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addNormalPanel()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v0}, Lcom/android/mms/ui/LevelControlLayout;->autoRecovery()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mLargeTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addLargePanel()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mDynamicTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addDynamicPanel()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mAdTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addAdPanel()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mXmTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->addXmPanel()V

    goto :goto_0
.end method

.method public setEditEmoticonListener(Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

    iput-object p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mListener:Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/ipmsg/ui/IpMessageEmoticonPanel;->mHandler:Landroid/os/Handler;

    return-void
.end method
