.class public Lcom/mediatek/mms/ipmessage/ContactManager;
.super Landroid/content/ContextWrapper;
.source "ContactManager.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "Mms/ipmsgContactManagerImpl"


# instance fields
.field public mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ipmessage/ContactManager;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/ContactManager;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public addContactToSpamList([I)Z
    .locals 2
    .param p1    # [I

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "addContactToSpamList called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public deleteContactFromSpamList([I)Z
    .locals 2
    .param p1    # [I

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "deleteContactFromSpamList called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getAvatarByNumber(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getAvatarByNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAvatarByThreadId(J)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # J

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getAvatarByThreadId called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getContactIdByNumber(Ljava/lang/String;)S
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getContactIdByNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getNameByNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getNameByNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    return-object v0
.end method

.method public getNameByThreadId(J)Ljava/lang/String;
    .locals 2
    .param p1    # J

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getNameByThreadId called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    return-object v0
.end method

.method public getNumberByEngineId(S)Ljava/lang/String;
    .locals 2
    .param p1    # S

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getNumberByEngineId called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    return-object v0
.end method

.method public getNumberByMessageId(J)Ljava/lang/String;
    .locals 2
    .param p1    # J

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getNumberByMessageId called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    return-object v0
.end method

.method public getNumberByThreadId(J)Ljava/lang/String;
    .locals 2
    .param p1    # J

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getNumberByThreadId called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    return-object v0
.end method

.method public getOnlineTimeByNumber(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getOnlineTimeByNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getSignatureByNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getSignatureByNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    return-object v0
.end method

.method public getStatusByNumber(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getStatusByNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getTypeByNumber(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "getTypeByNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isIpMessageNumber(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/ipmsgContactManagerImpl"

    const-string v1, "isIpMessageNumber called"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method
