.class public Lcom/mediatek/mms/ipmessage/ChatManager;
.super Landroid/content/ContextWrapper;
.source "ChatManager.java"


# instance fields
.field public mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ipmessage/ChatManager;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/ChatManager;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public deleteDraftMessageInThread(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public enterChatMode(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public exitFromChatMode(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public getIpMessageCountOfTypeInThread(JI)I
    .locals 1
    .param p1    # J
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public handleInviteDlg(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public handleInviteDlgLater(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public needShowInviteDlg(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public needShowReminderDlg(J)I
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public needShowSwitchAcctDlg(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public saveChatHistory([J)V
    .locals 0
    .param p1    # [J

    return-void
.end method

.method public sendChatMode(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    return-void
.end method
