.class public final Lcom/mediatek/mms/ipmessage/IpMessageConsts;
.super Ljava/lang/Object;
.source "IpMessageConsts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$drawable;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$array;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$string;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$RemoteActivities;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ResourceId;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$FeatureId;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ReminderType;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$IpMessageServiceId;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$SpecialSimId;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ContactStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$SelectContactType;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ContactType;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$IpMessageSendMode;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$MessageProtocol;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$IpMessageCategory;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$IpMessageMediaTypeFlag;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$IpMessageType;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$RestoreMsgStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$BackupMsgStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$SetProfileResult;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$DownloadAttachStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$HandleIpMessageAction;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$IpMessageStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ActivationStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$SaveHistroy;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ConnectionStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$SimInfoChanged;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ImStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$ServiceStatus;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$UpdateGroup;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$RefreshGroupList;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$RefreshContactList;,
        Lcom/mediatek/mms/ipmessage/IpMessageConsts$NewMessageAction;
    }
.end annotation


# static fields
.field public static final ACTION_DEL_IP_MSG_DONE:Ljava/lang/String; = "com.mediatek.mms.ipmessage.delIpMsgDone"

.field public static final ACTION_GROUP_NOTIFICATION_CLICKED:Ljava/lang/String; = "com.mediatek.mms.ipmessage.group_notification_clicked"

.field public static final ACTION_SERVICE_READY:Ljava/lang/String; = "com.mediatek.mms.ipmessage.service.ready"

.field public static final ACTION_UPGRADE:Ljava/lang/String; = "com.mediatek.mms.ipmessage.upgrade"

.field public static final AD_ICON_ARR:[I

.field public static final AD_PNG_ICON_ARR:[I

.field public static final DYNAMIC_ICON_ARR:[I

.field public static final DYNAMIC_PNG_ICON_ARR:[I

.field public static final GROUP_START:Ljava/lang/String; = "7---"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IPMESSAGE_NOTIFICATION:Ljava/lang/String; = "ipmessage_notification"

.field public static final LARGE_ICON_ARR:[I

.field public static final NUMBER:Ljava/lang/String; = "number"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final XM_ICON_ARR:[I

.field public static final XM_PNG_ICON_ARR:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x18

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->LARGE_ICON_ARR:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->DYNAMIC_ICON_ARR:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->DYNAMIC_PNG_ICON_ARR:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->AD_ICON_ARR:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->AD_PNG_ICON_ARR:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->XM_ICON_ARR:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/mediatek/mms/ipmessage/IpMessageConsts;->XM_PNG_ICON_ARR:[I

    return-void

    :array_0
    .array-data 4
        0x65
        0x66
        0x67
        0x68
        0x69
        0x6a
        0x6b
        0x6c
        0x6d
        0x6e
        0x6f
        0x70
        0x71
        0x72
        0x73
        0x74
        0x75
        0x76
        0x77
        0x78
        0x79
        0x7a
        0x7b
        0x7c
    .end array-data

    :array_1
    .array-data 4
        0xc9
        0xca
        0xcb
        0xcc
        0xcd
        0xce
        0xcf
        0xd0
        0xd1
        0xd2
        0xd3
        0xd4
        0xd5
        0xd6
        0xd7
        0xd8
        0xd9
        0xda
        0xdb
        0xdc
        0xdd
        0xde
        0xdf
        0xe0
    .end array-data

    :array_2
    .array-data 4
        0x12d
        0x12e
        0x12f
        0x130
        0x131
        0x132
        0x133
        0x134
        0x135
        0x136
        0x137
        0x138
        0x139
        0x13a
        0x13b
        0x13c
        0x13d
        0x13e
        0x13f
        0x140
        0x141
        0x142
        0x143
        0x144
    .end array-data

    :array_3
    .array-data 4
        0x191
        0x192
        0x193
        0x194
        0x195
        0x196
        0x197
        0x198
        0x199
        0x19a
        0x19b
        0x19c
        0x19d
        0x19e
        0x19f
        0x1a0
        0x1a1
        0x1a2
        0x1a3
        0x1a4
        0x1a5
        0x1a6
        0x1a7
        0x1a8
    .end array-data

    :array_4
    .array-data 4
        0x1f5
        0x1f6
        0x1f7
        0x1f8
        0x1f9
        0x1fa
        0x1fb
        0x1fc
        0x1fd
        0x1fe
        0x1ff
        0x200
        0x201
        0x202
        0x203
        0x204
        0x205
        0x206
        0x207
        0x208
        0x209
        0x20a
        0x20b
        0x20c
    .end array-data

    :array_5
    .array-data 4
        0x259
        0x25a
        0x25b
        0x25c
        0x25d
        0x25e
        0x25f
        0x260
        0x261
        0x262
        0x263
        0x264
        0x265
        0x266
        0x267
        0x268
        0x269
        0x26a
        0x26b
        0x26c
        0x26d
        0x26e
        0x26f
        0x270
    .end array-data

    :array_6
    .array-data 4
        0x2bd
        0x2be
        0x2bf
        0x2c0
        0x2c1
        0x2c2
        0x2c3
        0x2c4
        0x2c5
        0x2c6
        0x2c7
        0x2c8
        0x2c9
        0x2ca
        0x2cb
        0x2cc
        0x2cd
        0x2ce
        0x2cf
        0x2d0
        0x2d1
        0x2d2
        0x2d3
        0x2d4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
