.class public Lcom/mediatek/mms/ipmessage/message/IpImageMessage;
.super Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;
.source "IpImageMessage.java"


# instance fields
.field private mCaption:Ljava/lang/String;

.field private mHeightInPixel:I

.field private mThumbPath:Ljava/lang/String;

.field private mWidthInPixel:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getCaption()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mCaption:Ljava/lang/String;

    return-object v0
.end method

.method public getHeightInPixel()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mHeightInPixel:I

    return v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mThumbPath:Ljava/lang/String;

    return-object v0
.end method

.method public getWidthInPixel()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mWidthInPixel:I

    return v0
.end method

.method public setCaption(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mCaption:Ljava/lang/String;

    return-void
.end method

.method public setHeightInPixel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mHeightInPixel:I

    return-void
.end method

.method public setThumbPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mThumbPath:Ljava/lang/String;

    return-void
.end method

.method public setWidthInPixel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpImageMessage;->mWidthInPixel:I

    return-void
.end method
