.class public Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;
.super Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;
.source "IpLocationMessage.java"


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mLatitude:D

.field private mLongitude:D

.field private mThumbPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mLongitude:D

    return-wide v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mThumbPath:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mAddress:Ljava/lang/String;

    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1    # D

    iput-wide p1, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mLatitude:D

    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1    # D

    iput-wide p1, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mLongitude:D

    return-void
.end method

.method public setThumbPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpLocationMessage;->mThumbPath:Ljava/lang/String;

    return-void
.end method
