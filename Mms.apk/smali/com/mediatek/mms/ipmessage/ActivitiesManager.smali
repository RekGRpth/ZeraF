.class public Lcom/mediatek/mms/ipmessage/ActivitiesManager;
.super Landroid/content/ContextWrapper;
.source "ActivitiesManager.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "Mms/ActivityManagerImpl"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ipmessage/ActivitiesManager;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/ActivitiesManager;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public startRemoteActivity(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "Mms/ActivityManagerImpl"

    const-string v1, "ActivityManagerImpl start remote activity called."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
