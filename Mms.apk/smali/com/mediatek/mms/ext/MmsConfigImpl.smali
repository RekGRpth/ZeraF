.class public Lcom/mediatek/mms/ext/MmsConfigImpl;
.super Ljava/lang/Object;
.source "MmsConfigImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsConfig;


# static fields
.field private static final DEFAULTRETRYSCHEME:[I

.field private static final TAG:Ljava/lang/String; = "Mms/MmsConfigImpl"

.field private static sHttpSocketTimeout:I

.field private static sMaxTextLimit:I

.field private static sMmsRecipientLimit:I

.field private static sSmsToMmsTextThreshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    sput v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sSmsToMmsTextThreshold:I

    const/16 v0, 0x800

    sput v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMaxTextLimit:I

    const/16 v0, 0x14

    sput v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMmsRecipientLimit:I

    const v0, 0xea60

    sput v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sHttpSocketTimeout:I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->DEFAULTRETRYSCHEME:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0xea60
        0x493e0
        0x927c0
        0x1b7740
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public appendExtraQueryParameterForConversationDeleteAll(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p1    # Landroid/net/Uri;

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "appendExtraQueryParameterForConversationDeleteAll; null "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public getCapturePictureIntent()Landroid/content/Intent;
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "get capture picture intent: null"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHttpSocketTimeout()I
    .locals 3

    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get default socket timeout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/mms/ext/MmsConfigImpl;->sHttpSocketTimeout:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sHttpSocketTimeout:I

    return v0
.end method

.method public getMaxTextLimit()I
    .locals 3

    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get MaxTextLimit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMaxTextLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMaxTextLimit:I

    return v0
.end method

.method public getMmsRecipientLimit()I
    .locals 3

    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RecipientLimit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMmsRecipientLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMmsRecipientLimit:I

    return v0
.end method

.method public getMmsRetryPromptIndex()I
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "getMmsRetryPromptIndex"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public getMmsRetryScheme()[I
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "getMmsRetryScheme"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->DEFAULTRETRYSCHEME:[I

    return-object v0
.end method

.method public getSmsToMmsTextThreshold()I
    .locals 3

    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get SmsToMmsTextThreshold: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/mms/ext/MmsConfigImpl;->sSmsToMmsTextThreshold:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/mediatek/mms/ext/MmsConfigImpl;->sSmsToMmsTextThreshold:I

    return v0
.end method

.method public getUAProf(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set default UAProf is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public isAllowRetryForPermanentFail()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "setSoSndTimeout"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableAdjustFontSize()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Enable adjust font size"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isEnableDialogForUrl()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable ForwardWithSender "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableFolderMode()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable FolderMode "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableForwardWithSender()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable ForwardWithSender "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableMultiSmsSaveLocation()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable MultiSmsSaveLocation "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isEnableReportAllowed()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable ReportAllowed "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableSIMLongSmsConcatenate()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Enable concatenate long sms in sim card status"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isEnableSIMSmsForSetting()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Enable display storage status "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isEnableSmsEncodingType()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable Sms Encoding type "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableSmsValidityPeriod()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Enable sms validity period"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableStorageFullToast()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable StorageFullToast "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableStorageStatusDisp()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Disable display storage status "

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isNeedExitComposerAfterForward()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "isNeedExitComposerAfterForward: true"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isRetainRetryIndexWhenInCall()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "isIncreaseRetryIndexWhenInCall: false"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isSendExpiredResIfNotificationExpired()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "isSendExpiredResIfNotificationExpired: true"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isShowDraftIcon()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "isShowDraftIcon: false"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isShowUrlDialog()Z
    .locals 2

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "Enable show dialog when open browser: false"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public printMmsMemStat(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "printMmsMemStat"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setExtendUrlSpan(Landroid/widget/TextView;)V
    .locals 2
    .param p1    # Landroid/widget/TextView;

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "setExtendUrlSpan"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setHttpSocketTimeout(I)V
    .locals 3
    .param p1    # I

    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set default socket timeout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sput p1, Lcom/mediatek/mms/ext/MmsConfigImpl;->sHttpSocketTimeout:I

    return-void
.end method

.method public setMaxTextLimit(I)V
    .locals 3
    .param p1    # I

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    sput p1, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMaxTextLimit:I

    :cond_0
    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set MaxTextLimit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMaxTextLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setMmsRecipientLimit(I)V
    .locals 3
    .param p1    # I

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    sput p1, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMmsRecipientLimit:I

    :cond_0
    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set RecipientLimit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/mms/ext/MmsConfigImpl;->sMmsRecipientLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSmsToMmsTextThreshold(I)V
    .locals 3
    .param p1    # I

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    sput p1, Lcom/mediatek/mms/ext/MmsConfigImpl;->sSmsToMmsTextThreshold:I

    :cond_0
    const-string v0, "Mms/MmsConfigImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set SmsToMmsTextThreshold: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/mms/ext/MmsConfigImpl;->sSmsToMmsTextThreshold:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSoSndTimeout(Lorg/apache/http/params/HttpParams;)V
    .locals 2
    .param p1    # Lorg/apache/http/params/HttpParams;

    const-string v0, "Mms/MmsConfigImpl"

    const-string v1, "setSoSndTimeout"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
