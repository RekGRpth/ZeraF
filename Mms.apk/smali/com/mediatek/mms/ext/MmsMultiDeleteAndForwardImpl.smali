.class public Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;
.super Landroid/content/ContextWrapper;
.source "MmsMultiDeleteAndForwardImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsMultiDeleteAndForwardImpl"


# instance fields
.field private mBodyandAddressItem:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;",
            ">;"
        }
    .end annotation
.end field

.field private mHost:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mHost:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;

    return-void
.end method


# virtual methods
.method public clearBodyandAddressList()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    return-void
.end method

.method public getAddress(J)Ljava/lang/String;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;

    iget-object v0, v0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;->mAddress:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBody(J)Ljava/lang/String;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;

    iget-object v0, v0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;->mBody:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getHost()Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mHost:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;

    return-object v0
.end method

.method public init(Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;)V
    .locals 0
    .param p1    # Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;

    iput-object p1, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mHost:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;

    return-void
.end method

.method public initBodyandAddress()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    return-void
.end method

.method public onMultiforwardItemSelected()Z
    .locals 2

    const-string v0, "Mms/MmsMultiDeleteAndForwardImpl"

    const-string v1, "onMultiforwardItemSelected  "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->getHost()Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;->prepareToForwardMessage()V

    const/4 v0, 0x1

    return v0
.end method

.method public setBodyandAddress(Landroid/database/Cursor;IILjava/lang/String;J)V
    .locals 6
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # J

    const-string v3, "mms"

    invoke-virtual {p4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Mms/MmsMultiDeleteAndForwardImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initListMap mAddress = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "mBody"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl$BodyandAddress;-><init>(Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;->mBodyandAddressItem:Ljava/util/Map;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setForwardMenuEnabled(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method
