.class public Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationManagerPlus;
.super Ljava/lang/Object;
.source "EncapsulatedNotificationManagerPlus.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancel(Landroid/content/Context;I)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-static {p0, p1}, Lcom/mediatek/notification/NotificationManagerPlus;->cancel(Landroid/content/Context;I)V

    return-void
.end method

.method public static notify(ILcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;)V
    .locals 1
    .param p0    # I
    .param p1    # Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, p1, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlus:Lcom/mediatek/notification/NotificationPlus;

    invoke-static {p0, v0}, Lcom/mediatek/notification/NotificationManagerPlus;->notify(ILcom/mediatek/notification/NotificationPlus;)V

    return-void
.end method
