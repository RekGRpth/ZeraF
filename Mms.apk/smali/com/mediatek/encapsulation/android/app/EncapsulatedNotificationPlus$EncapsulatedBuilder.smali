.class public Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
.super Ljava/lang/Object;
.source "EncapsulatedNotificationPlus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EncapsulatedBuilder"
.end annotation


# instance fields
.field private mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    invoke-direct {v0, p1}, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    return-void
.end method


# virtual methods
.method public create()Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    return-object v0
.end method

.method public setCancelable(Z)Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlusBuilder:Lcom/mediatek/notification/NotificationPlus$Builder;

    invoke-virtual {v0, p1}, Lcom/mediatek/notification/NotificationPlus$Builder;->setCancelable(Z)Lcom/mediatek/notification/NotificationPlus$Builder;

    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlusBuilder:Lcom/mediatek/notification/NotificationPlus$Builder;

    invoke-virtual {v0, p1}, Lcom/mediatek/notification/NotificationPlus$Builder;->setMessage(Ljava/lang/String;)Lcom/mediatek/notification/NotificationPlus$Builder;

    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlusBuilder:Lcom/mediatek/notification/NotificationPlus$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/notification/NotificationPlus$Builder;->setNegativeButton(Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/mediatek/notification/NotificationPlus$Builder;

    return-object p0
.end method

.method public setNeutralButton(Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlusBuilder:Lcom/mediatek/notification/NotificationPlus$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/notification/NotificationPlus$Builder;->setNeutralButton(Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/mediatek/notification/NotificationPlus$Builder;

    return-object p0
.end method

.method public setOnCancelListener(Landroid/app/PendingIntent;)Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
    .locals 1
    .param p1    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlusBuilder:Lcom/mediatek/notification/NotificationPlus$Builder;

    invoke-virtual {v0, p1}, Lcom/mediatek/notification/NotificationPlus$Builder;->setOnCancelListener(Landroid/app/PendingIntent;)Lcom/mediatek/notification/NotificationPlus$Builder;

    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlusBuilder:Lcom/mediatek/notification/NotificationPlus$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/notification/NotificationPlus$Builder;->setPositiveButton(Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/mediatek/notification/NotificationPlus$Builder;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus$EncapsulatedBuilder;->mEncapsulatedNotification:Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;

    iget-object v0, v0, Lcom/mediatek/encapsulation/android/app/EncapsulatedNotificationPlus;->mNotificationPlusBuilder:Lcom/mediatek/notification/NotificationPlus$Builder;

    invoke-virtual {v0, p1}, Lcom/mediatek/notification/NotificationPlus$Builder;->setTitle(Ljava/lang/String;)Lcom/mediatek/notification/NotificationPlus$Builder;

    return-object p0
.end method
