.class public interface abstract Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone;
.super Ljava/lang/Object;
.source "EncapsulatedPhone.java"

# interfaces
.implements Lcom/android/internal/telephony/Phone;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$1;,
        Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;,
        Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;
    }
.end annotation


# static fields
.field public static final APN_ALREADY_ACTIVE:I = 0x0

.field public static final APN_ALREADY_INACTIVE:I = 0x4

.field public static final APN_REQUEST_FAILED:I = 0x3

.field public static final APN_REQUEST_FAILED_DUE_TO_RADIO_OFF:I = 0x62

.field public static final APN_REQUEST_STARTED:I = 0x1

.field public static final APN_TYPE_ALL:Ljava/lang/String; = "*"

.field public static final APN_TYPE_CMMAIL:Ljava/lang/String; = "cmmail"

.field public static final APN_TYPE_DISABLE_ONGOING:I = 0x64

.field public static final APN_TYPE_DM:Ljava/lang/String; = "dm"

.field public static final APN_TYPE_MMS:Ljava/lang/String; = "mms"

.field public static final APN_TYPE_NET:Ljava/lang/String; = "net"

.field public static final APN_TYPE_NOT_AVAILABLE:I = 0x2

.field public static final APN_TYPE_NOT_AVAILABLE_DUE_TO_RECORDS_NOT_LOADED:I = 0x63

.field public static final APN_TYPE_TETHERING:Ljava/lang/String; = "tethering"

.field public static final APN_TYPE_WAP:Ljava/lang/String; = "wap"

.field public static final DATA_APN_TYPE_KEY:Ljava/lang/String; = "apnType"

.field public static final DISABLE_DATA_CONNECTIVITY_INVALID_SIM_ID:I = 0x5

.field public static final DISABLE_DATA_CONNECTIVITY_INVALID_STATE:I = 0x8

.field public static final DISABLE_DATA_CONNECTIVITY_STARTED:I = 0x6

.field public static final DISABLE_DATA_CONNECTIVITY_SUCCESS:I = 0x7

.field public static final DISCONNECT_DATA_FLAG:Ljava/lang/String; = "disconnectPdpFlag"

.field public static final ENABLE_DATA_CONNECTIVITY_FAILED_THIS_SIM_STILL_DETACHING:I = 0x3

.field public static final ENABLE_DATA_CONNECTIVITY_INVALID_SIM_ID:I = 0x0

.field public static final ENABLE_DATA_CONNECTIVITY_INVALID_STATE:I = 0x4

.field public static final ENABLE_DATA_CONNECTIVITY_STARTED:I = 0x1

.field public static final ENABLE_DATA_CONNECTIVITY_SUCCESS:I = 0x2

.field public static final FEATURE_ENABLE_CMMAIL:Ljava/lang/String; = "enableCMMAIL"

.field public static final FEATURE_ENABLE_DM:Ljava/lang/String; = "enableDM"

.field public static final FEATURE_ENABLE_NET:Ljava/lang/String; = "enableNET"

.field public static final FEATURE_ENABLE_WAP:Ljava/lang/String; = "enableWAP"

.field public static final GEMINI_DEFAULT_SIM_MODE:Ljava/lang/String; = "persist.radio.default_sim_mode"

.field public static final GEMINI_DEFAULT_SIM_PROP:Ljava/lang/String; = "persist.radio.default_sim"

.field public static final GEMINI_GPRS_TRANSFER_TYPE:Ljava/lang/String; = "gemini.gprs.transfer.type"

.field public static final GEMINI_SIM_1:I = 0x0

.field public static final GEMINI_SIM_2:I = 0x1

.field public static final GEMINI_SIM_3:I = 0x2

.field public static final GEMINI_SIM_4:I = 0x3

.field public static final GEMINI_SIM_ID_KEY:Ljava/lang/String; = "simId"

.field public static final GEMINI_SIM_NUM:I

.field public static final GEMINI_SIP_CALL:I = -0x1

.field public static final IS_VT_CALL:Ljava/lang/String; = "isVtCall"

.field public static final MULTI_SIM_ID_KEY:Ljava/lang/String; = "simid"

.field public static final NT_MODE_GEMINI:I = 0xc

.field public static final PHONE_TYPE_GEMINI:I = 0x4

.field public static final REASON_GPRS_ATTACHED_TIMEOUT:Ljava/lang/String; = "gprsAttachedTimeout"

.field public static final REASON_NO_SUCH_PDP:Ljava/lang/String; = "noSuchPdp"

.field public static final REASON_ON_RADIO_AVAILABLE:Ljava/lang/String; = "onRadioAvailable"

.field public static final REASON_ON_RECORDS_LOADED:Ljava/lang/String; = "onRecordsLoaded"

.field public static final REASON_PDP_NOT_ACTIVE:Ljava/lang/String; = "pdpNotActive"

.field public static final REASON_POLL_STATE_DONE:Ljava/lang/String; = "pollStateDone"

.field public static final SIM_INDICATOR_ABSENT:I = 0x0

.field public static final SIM_INDICATOR_CONNECTED:I = 0x7

.field public static final SIM_INDICATOR_INVALID:I = 0x3

.field public static final SIM_INDICATOR_LOCKED:I = 0x2

.field public static final SIM_INDICATOR_NORMAL:I = 0x5

.field public static final SIM_INDICATOR_RADIOOFF:I = 0x1

.field public static final SIM_INDICATOR_ROAMING:I = 0x6

.field public static final SIM_INDICATOR_ROAMINGCONNECTED:I = 0x8

.field public static final SIM_INDICATOR_SEARCHING:I = 0x4

.field public static final SIM_INDICATOR_UNKNOWN:I = -0x1

.field public static final STATE_KEY:Ljava/lang/String; = "state"

.field public static final TOTAL_SIM_COLOR_COUNT:I = 0x4

.field public static final UIM_STATUS_CARD_INSERTED:I = 0x1

.field public static final UIM_STATUS_NO_CARD_INSERTED:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lcom/android/internal/telephony/PhoneConstants;->GEMINI_SIM_NUM:I

    sput v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone;->GEMINI_SIM_NUM:I

    return-void
.end method


# virtual methods
.method public abstract changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract doSimAuthentication(Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract doUSimAuthentication(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract get3GCapabilitySIM()I
.end method

.method public abstract getAccumulatedCallMeter(Landroid/os/Message;)V
.end method

.method public abstract getAccumulatedCallMeterMaximum(Landroid/os/Message;)V
.end method

.method public abstract getActiveApnType()Ljava/lang/String;
.end method

.method public abstract getApnForType(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getCurrentCallMeter(Landroid/os/Message;)V
.end method

.method public abstract getDnsServers(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public abstract getEfRatBalancing()I
.end method

.method public abstract getEfRatBalancing(I)I
.end method

.method public abstract getFacilityLock(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract getIccServiceStatus(Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;)Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;
.end method

.method public abstract getLastCallFailCause()I
.end method

.method public abstract getMobileRevisionAndIMEI(ILandroid/os/Message;)V
.end method

.method public abstract getMySimId()I
.end method

.method public abstract getPOLCapability(Landroid/os/Message;)V
.end method

.method public abstract getPdpContextList(Landroid/os/Message;)V
.end method

.method public abstract getPpuAndCurrency(Landroid/os/Message;)V
.end method

.method public abstract getPreferedOperatorList(Landroid/os/Message;)V
.end method

.method public abstract getSN()Ljava/lang/String;
.end method

.method public abstract getSimIndicateState()I
.end method

.method public abstract getSpNameInEfSpn()Ljava/lang/String;
.end method

.method public abstract getSpNameInEfSpn(I)Ljava/lang/String;
.end method

.method public abstract getVtCallForwardingOption(ILandroid/os/Message;)V
.end method

.method public abstract getVtCallWaiting(Landroid/os/Message;)V
.end method

.method public abstract getVtFacilityLock(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract hangupActiveCall()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public abstract hangupAll()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public abstract hangupAllEx()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public abstract isCspPlmnEnabled(I)Z
.end method

.method public abstract isIccCardProviderAsMvno()Z
.end method

.method public abstract isIccCardProviderAsMvno(I)Z
.end method

.method public abstract isOperatorMvnoForImsi()Ljava/lang/String;
.end method

.method public abstract isOperatorMvnoForImsi(I)Ljava/lang/String;
.end method

.method public abstract isSimInsert()Z
.end method

.method public abstract notifySimMissingStatus(Z)V
.end method

.method public abstract queryCellBroadcastSmsActivation(Landroid/os/Message;)V
.end method

.method public abstract refreshSpnDisplay()V
.end method

.method public abstract registerForCrssSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V
.end method

.method public abstract registerForNeighboringInfo(Landroid/os/Handler;ILjava/lang/Object;)V
.end method

.method public abstract registerForNetworkInfo(Landroid/os/Handler;ILjava/lang/Object;)V
.end method

.method public abstract registerForSpeechInfo(Landroid/os/Handler;ILjava/lang/Object;)V
.end method

.method public abstract registerForVtReplaceDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V
.end method

.method public abstract registerForVtRingInfo(Landroid/os/Handler;ILjava/lang/Object;)V
.end method

.method public abstract registerForVtStatusInfo(Landroid/os/Handler;ILjava/lang/Object;)V
.end method

.method public abstract resetAccumulatedCallMeter(Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract sendBTSIMProfile(IILjava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract set3GCapabilitySIM(I)Z
.end method

.method public abstract setAccumulatedCallMeterMaximum(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract setCRO(ILandroid/os/Message;)V
.end method

.method public abstract setCellBroadcastSmsConfig([Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;[Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;Landroid/os/Message;)V
.end method

.method public abstract setFacilityLock(Ljava/lang/String;ZLjava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract setGprsTransferType(ILandroid/os/Message;)V
.end method

.method public abstract setPOLEntry(Lcom/android/internal/telephony/gsm/NetworkInfoWithAcT;Landroid/os/Message;)V
.end method

.method public abstract setPpuAndCurrency(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract setRadioPower(ZZ)V
.end method

.method public abstract setRadioPowerOn()V
.end method

.method public abstract setVtCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V
.end method

.method public abstract setVtCallWaiting(ZLandroid/os/Message;)V
.end method

.method public abstract setVtFacilityLock(Ljava/lang/String;ZLjava/lang/String;Landroid/os/Message;)V
.end method

.method public abstract unregisterForCrssSuppServiceNotification(Landroid/os/Handler;)V
.end method

.method public abstract unregisterForNeighboringInfo(Landroid/os/Handler;)V
.end method

.method public abstract unregisterForNetworkInfo(Landroid/os/Handler;)V
.end method

.method public abstract unregisterForSpeechInfo(Landroid/os/Handler;)V
.end method

.method public abstract unregisterForVtReplaceDisconnect(Landroid/os/Handler;)V
.end method

.method public abstract unregisterForVtRingInfo(Landroid/os/Handler;)V
.end method

.method public abstract unregisterForVtStatusInfo(Landroid/os/Handler;)V
.end method

.method public abstract updateSimIndicateState()V
.end method

.method public abstract voiceAccept()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public abstract vtDial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method

.method public abstract vtDial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation
.end method
