.class public Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;
.super Ljava/lang/Object;
.source "EncapsulatedR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "drawable"
.end annotation


# static fields
.field public static final drm_green_lock:I

.field public static final drm_red_lock:I

.field public static final sim_background_locked:I

.field public static final sim_connected:I

.field public static final sim_invalid:I

.field public static final sim_locked:I

.field public static final sim_radio_off:I

.field public static final sim_roaming:I

.field public static final sim_roaming_connected:I

.field public static final sim_searching:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x2020040

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->drm_red_lock:I

    const v0, 0x202003f

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->drm_green_lock:I

    const v0, 0x20200ff

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_locked:I

    const v0, 0x2020112

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_radio_off:I

    const v0, 0x20200f8

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_invalid:I

    const v0, 0x2020119

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_searching:I

    const v0, 0x2020117

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_roaming:I

    const v0, 0x20200ef

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_connected:I

    const v0, 0x2020118

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_roaming_connected:I

    const v0, 0x7f020156

    sput v0, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->sim_background_locked:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
