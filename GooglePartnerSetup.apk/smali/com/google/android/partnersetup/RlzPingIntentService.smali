.class public Lcom/google/android/partnersetup/RlzPingIntentService;
.super Landroid/app/IntentService;
.source "RlzPingIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;,
        Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;,
        Lcom/google/android/partnersetup/RlzPingIntentService$MyBinder;
    }
.end annotation


# instance fields
.field private findAppsInNewThread:Z

.field private mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

.field private final mBinder:Landroid/os/IBinder;

.field private mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

.field private mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "GooglePartnerSetup"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->findAppsInNewThread:Z

    new-instance v0, Lcom/google/android/partnersetup/RlzPingIntentService$MyBinder;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/RlzPingIntentService$MyBinder;-><init>(Lcom/google/android/partnersetup/RlzPingIntentService;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mBinder:Landroid/os/IBinder;

    return-void
.end method

.method public static startRlzPingService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-class v1, Lcom/google/android/partnersetup/RlzPingIntentService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0, p1, p0}, Lcom/google/android/partnersetup/RlzPingIntentService;->onHandleIntentWithContext(Landroid/content/Intent;Landroid/content/Context;)V

    return-void
.end method

.method onHandleIntentWithContext(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 10

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    const-string v0, "GooglePartnerSetup"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GooglePartnerSetup"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/partnersetup/RlzPreferences;

    invoke-direct {v0, p2}, Lcom/google/android/partnersetup/RlzPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    :cond_1
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;

    iget-object v1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-direct {v0, p2, v1}, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;-><init>(Landroid/content/Context;Lcom/google/android/partnersetup/RlzPreferencesInterface;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "status"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Lcom/google/android/partnersetup/RlzProtocol$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-array v2, v8, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v9

    sget-object v1, Lcom/google/android/partnersetup/RlzProtocol$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    if-lez v1, :cond_4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/partnersetup/RlzPingService;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    invoke-interface {v0}, Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;->reschedulePing()V

    goto :goto_0

    :cond_5
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x80

    invoke-virtual {v1, v2, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_3

    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "com.google.android.partnersetup.RLZ_ACCESS_POINT"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "com.google.android.partnersetup.RLZ_ACCESS_POINT"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v6, v8, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v6, v9

    sget-object v5, Lcom/google/android/partnersetup/RlzProtocol$Apps;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "app_name=?"

    new-array v8, v8, [Ljava/lang/String;

    aput-object v1, v8, v9

    move-object v4, v0

    move-object v9, v3

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v0, v1}, Lcom/google/android/partnersetup/RlzPingService;->addApplicationInstallEvent(Landroid/content/ContentResolver;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    invoke-interface {v0}, Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;->scheduleEventPing()V

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_7
    const-string v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;

    invoke-direct {v0, p2}, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    :cond_8
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    invoke-interface {v0}, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v0}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->isActivationPingPrepared()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/partnersetup/RlzPingIntentService$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/partnersetup/RlzPingIntentService$1;-><init>(Lcom/google/android/partnersetup/RlzPingIntentService;Landroid/content/Context;)V

    iget-boolean v1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->findAppsInNewThread:Z

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    invoke-interface {v0}, Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;->scheduleEventPing()V

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v0, v8}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->setActivationPingPrepared(Z)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v0}, Ljava/lang/Thread;->run()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method setAccountManager(Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;)V
    .locals 0
    .param p1    # Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    return-void
.end method

.method setFindAppsInNewThreadFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->findAppsInNewThread:Z

    return-void
.end method

.method setPingScheduler(Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;)V
    .locals 0
    .param p1    # Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    return-void
.end method

.method setPreferences(Lcom/google/android/partnersetup/RlzPreferencesInterface;)V
    .locals 0
    .param p1    # Lcom/google/android/partnersetup/RlzPreferencesInterface;

    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    return-void
.end method
