.class public Lcom/google/android/partnersetup/RlzDebugActivity;
.super Landroid/app/Activity;
.source "RlzDebugActivity.java"


# instance fields
.field private current_oem_mode:I

.field private mACAP:Landroid/widget/TextView;

.field private mActivated:Landroid/widget/TextView;

.field private mBackoff:Landroid/widget/TextView;

.field private mBrandCode:Landroid/widget/TextView;

.field private mDelayAfterEvent:Landroid/widget/TextView;

.field private mEnabled:Landroid/widget/TextView;

.field private mGUID:Landroid/widget/TextView;

.field private mInitialDelay:Landroid/widget/TextView;

.field private mMaxRetryInterval:Landroid/widget/TextView;

.field private mMaxTableSize:Landroid/widget/TextView;

.field private mOEM:Landroid/widget/RadioGroup;

.field private mPID:Landroid/widget/TextView;

.field private mPingInterval:Landroid/widget/TextView;

.field private mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

.field private mWakeTime:Landroid/widget/TextView;

.field private unknownProperty:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/partnersetup/RlzDebugActivity;I)I
    .locals 0
    .param p0    # Lcom/google/android/partnersetup/RlzDebugActivity;
    .param p1    # I

    iput p1, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->current_oem_mode:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/partnersetup/RlzDebugActivity;)Lcom/google/android/partnersetup/RlzPreferences;
    .locals 1
    .param p0    # Lcom/google/android/partnersetup/RlzDebugActivity;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    return-object v0
.end method

.method private getUnknownPropertyString()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->unknownProperty:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/partnersetup/RlzDebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->unknownProperty:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->unknownProperty:Ljava/lang/String;

    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f020000

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->setContentView(I)V

    new-instance v0, Lcom/google/android/partnersetup/RlzPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/RlzPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mBrandCode:Landroid/widget/TextView;

    const v0, 0x7f050001

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mGUID:Landroid/widget/TextView;

    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mPID:Landroid/widget/TextView;

    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mActivated:Landroid/widget/TextView;

    const v0, 0x7f050004

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mACAP:Landroid/widget/TextView;

    const v0, 0x7f050005

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mWakeTime:Landroid/widget/TextView;

    const v0, 0x7f050006

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mBackoff:Landroid/widget/TextView;

    const v0, 0x7f05000d

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mOEM:Landroid/widget/RadioGroup;

    iget-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mOEM:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/google/android/partnersetup/RlzDebugActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/partnersetup/RlzDebugActivity$1;-><init>(Lcom/google/android/partnersetup/RlzDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    const v0, 0x7f050007

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mEnabled:Landroid/widget/TextView;

    const v0, 0x7f050008

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mMaxTableSize:Landroid/widget/TextView;

    const v0, 0x7f050009

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mPingInterval:Landroid/widget/TextView;

    const v0, 0x7f05000a

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mInitialDelay:Landroid/widget/TextView;

    const v0, 0x7f05000b

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mMaxRetryInterval:Landroid/widget/TextView;

    const v0, 0x7f05000c

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mDelayAfterEvent:Landroid/widget/TextView;

    const v0, 0x7f050010

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/partnersetup/RlzDebugActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/partnersetup/RlzDebugActivity$2;-><init>(Lcom/google/android/partnersetup/RlzDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f050011

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/partnersetup/RlzDebugActivity$3;

    invoke-direct {v1, p0}, Lcom/google/android/partnersetup/RlzDebugActivity$3;-><init>(Lcom/google/android/partnersetup/RlzDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f050012

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/partnersetup/RlzDebugActivity$4;

    invoke-direct {v1, p0}, Lcom/google/android/partnersetup/RlzDebugActivity$4;-><init>(Lcom/google/android/partnersetup/RlzDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f050013

    invoke-virtual {p0, v0}, Lcom/google/android/partnersetup/RlzDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/partnersetup/RlzDebugActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/partnersetup/RlzDebugActivity$5;-><init>(Lcom/google/android/partnersetup/RlzDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onResume()V
    .locals 9

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mBrandCode:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->getBrandCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v7}, Lcom/google/android/partnersetup/RlzPreferences;->getDeviceGUID()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/partnersetup/RlzDebugActivity;->getUnknownPropertyString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mGUID:Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v7}, Lcom/google/android/partnersetup/RlzPreferences;->getDevicePID()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/partnersetup/RlzDebugActivity;->getUnknownPropertyString()Ljava/lang/String;

    move-result-object v4

    :cond_1
    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mPID:Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mActivated:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->isActivationPingPrepared()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mACAP:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/partnersetup/RlzDebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/partnersetup/RlzAcap;->generateAcap(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v7}, Lcom/google/android/partnersetup/RlzPreferences;->getAlarmWakeTime()J

    move-result-wide v5

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mWakeTime:Landroid/widget/TextView;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v7}, Lcom/google/android/partnersetup/RlzPreferences;->getBackoffTime()I

    move-result v0

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mBackoff:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v7}, Lcom/google/android/partnersetup/RlzPreferences;->isOEMModeEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    const v7, 0x7f05000e

    :goto_0
    iput v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->current_oem_mode:I

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mOEM:Landroid/widget/RadioGroup;

    iget v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->current_oem_mode:I

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mEnabled:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->isRlzEnabledGservices()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mMaxTableSize:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->getMaxTableSize()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mPingInterval:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->getPingInterval()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mInitialDelay:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->getInitialDelay()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mMaxRetryInterval:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->getMaxRetryInterval()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mDelayAfterEvent:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/partnersetup/RlzDebugActivity;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferences;

    invoke-virtual {v8}, Lcom/google/android/partnersetup/RlzPreferences;->getDelayAfterEvent()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    const v7, 0x7f05000f

    goto :goto_0
.end method
