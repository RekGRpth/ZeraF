.class Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;
.super Ljava/lang/Object;
.source "MASFConnection.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->makeRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;JJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
        "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

.field final synthetic val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

.field final synthetic val$hardMaxAgeMs:J

.field final synthetic val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

.field final synthetic val$requestCacheBytes:[B


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$requestCacheBytes:[B

    iput-wide p4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$hardMaxAgeMs:J

    iput-object p6, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 7
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$requestCacheBytes:[B

    iget-wide v3, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$hardMaxAgeMs:J

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    # invokes: Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->tryGenieCache(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->access$000(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-virtual {v0, v1, v6, v2}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->successInUiThread(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->failureInUiThread(Ljava/lang/Exception;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    goto :goto_0
.end method

.method public progress(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-interface {v0, p1}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->progress(I)V

    return-void
.end method

.method public success(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    if-eqz p2, :cond_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->successInUiThread(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    check-cast p2, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;->success(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;)V

    return-void
.end method
