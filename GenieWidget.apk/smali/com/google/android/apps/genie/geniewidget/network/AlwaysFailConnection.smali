.class public Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;
.super Ljava/lang/Object;
.source "AlwaysFailConnection.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;


# instance fields
.field private fail:Z

.field private fakeNews:Z

.field private fakeWeather:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeNews:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeWeather:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fail:Z

    const-string v0, "Genie"

    const-string v1, "Dummy network configured to ALWAYS FAIL."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fail:Z

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeNews:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeWeather:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fail:Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeNews:Z

    iput-boolean p2, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeWeather:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->generateFakeData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fail:Z

    return v0
.end method

.method private execute(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;-><init>(Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method

.method private generateFakeData()Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;",
            ">;"
        }
    .end annotation

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeNews:Z

    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    const-string v3, "h"

    const-string v4, "Headlines"

    const-string v5, "This is a really long headline designed to take up all Three lines and test the padding at the bottom of the widget"

    const-string v6, "fake"

    const-string v7, "thumb"

    const-string v8, "guardian.co.uk"

    const-wide/16 v9, 0x0

    const-string v11, ""

    const-string v12, "Fake snippet"

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    const-string v3, "h"

    const-string v4, "Headlines"

    const-string v5, "Short headline"

    const-string v6, "fake"

    const-string v7, "thumb"

    const-string v8, "guardian.co.uk"

    const-wide/16 v9, 0x0

    const-string v11, ""

    const-string v12, "Fake snippet"

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fakeWeather:Z

    if-eqz v2, :cond_1

    new-instance v17, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    const-string v2, "london"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const-string v7, "today"

    const-string v8, "fake condition"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, ""

    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    invoke-direct/range {v2 .. v16}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->setCurrentCondition(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v18
.end method


# virtual methods
.method public getRawFeed(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;JJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # J
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "JJ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p6}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->execute(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    return-void
.end method

.method public getUrl(Ljava/lang/String;JZLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JZ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Dummy network: always fails"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {p5, v0}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->failure(Ljava/lang/Exception;)V

    return-void
.end method

.method public logClick(Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;

    return-void
.end method
