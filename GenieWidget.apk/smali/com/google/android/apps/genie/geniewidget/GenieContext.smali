.class public interface abstract Lcom/google/android/apps/genie/geniewidget/GenieContext;
.super Ljava/lang/Object;
.source "GenieContext.java"


# virtual methods
.method public abstract cancelScheduledModelUpdate()V
.end method

.method public abstract formatMillis(J)Ljava/lang/String;
.end method

.method public abstract getBooleanPreference(IZ)Z
.end method

.method public abstract getContentResolver()Landroid/content/ContentResolver;
.end method

.method public abstract getCustomTopicPreference()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLastKnownLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
.end method

.method public abstract getLongPreference(IJ)J
.end method

.method public abstract getNewsTopicPreference()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getString(I)Ljava/lang/String;
.end method

.method public abstract getStringPreference(ILjava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getWeatherIconResource(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)I
.end method

.method public abstract logClick([BII)V
.end method

.method public abstract recordRefreshTime()V
.end method

.method public abstract redrawWidget(Ljava/lang/String;)V
.end method

.method public abstract refreshDataModel()V
.end method

.method public abstract registerConnectivityIntent(I)V
.end method

.method public abstract requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;J)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
            ">;J)V"
        }
    .end annotation
.end method

.method public abstract requestWidgetRefresh()V
.end method

.method public abstract resetScheduleFalloff()V
.end method

.method public abstract runOnUIThread(Ljava/lang/Runnable;)V
.end method

.method public abstract scheduleModelUpdate(Z)V
.end method

.method public abstract setCustomTopicPreference(Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setLongPreference(IJ)V
.end method

.method public abstract setNewsTopicPreference(Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract stopLocationMonitor()V
.end method

.method public abstract unregisterConnectivityIntent()V
.end method

.method public abstract updateContentProvider(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;J)V
.end method
