.class Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;
.super Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;
.source "HtmlParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithXmlEncoding(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;)V

    return-void
.end method


# virtual methods
.method public onAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;)Z
    .locals 8
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v7, -0x1

    const/16 v6, 0x20

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    const/16 v6, 0x3d

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write(I)V

    const-string v6, "encoding"

    invoke-virtual {p3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;->bypassAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    :cond_0
    :goto_0
    return v5

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    const/16 v6, 0x27

    if-ne v1, v6, :cond_2

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v6, "\'"

    invoke-direct {v4, p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write(I)V

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;

    invoke-direct {v0, v4, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->pipe()I

    move-result v1

    :goto_2
    if-eq v1, v7, :cond_5

    if-lez v1, :cond_4

    invoke-virtual {v2, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->pipe()I

    move-result v1

    goto :goto_2

    :cond_2
    const/16 v6, 0x22

    if-ne v1, v6, :cond_3

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v6, "\""

    invoke-direct {v4, p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    :cond_3
    new-instance v4, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v6, " \t\r\n>"

    invoke-direct {v4, p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    invoke-static {v6, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$200(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->reset()V

    neg-int v6, v1

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    invoke-static {v6, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$200(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->getEndCharacter()I

    move-result v1

    if-eq v1, v7, :cond_6

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$302(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/lang/String;)Ljava/lang/String;

    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->getEndCharacter()I

    move-result v1

    const/16 v6, 0x3e

    if-ne v1, v6, :cond_0

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    const/4 v5, 0x0

    goto/16 :goto_0
.end method
