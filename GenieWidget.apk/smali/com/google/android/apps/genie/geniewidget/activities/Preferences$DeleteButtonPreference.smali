.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;
.super Landroid/preference/Preference;
.source "Preferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteButtonPreference"
.end annotation


# instance fields
.field private final parent:Landroid/preference/PreferenceCategory;

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

.field private final topic:Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/content/Context;Landroid/preference/PreferenceCategory;Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/preference/PreferenceCategory;
    .param p4    # Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v0, 0x7f040010

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->setWidgetLayoutResource(I)V

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->parent:Landroid/preference/PreferenceCategory;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->topic:Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;)Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->topic:Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;)Landroid/preference/PreferenceCategory;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->parent:Landroid/preference/PreferenceCategory;

    return-object v0
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v1, 0x7f0b0037

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
