.class Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;
.super Ljava/lang/Object;
.source "GenieLocationProvider.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/utils/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationMonitor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
        ">;"
    }
.end annotation


# instance fields
.field private current:I

.field private listener:Landroid/location/LocationListener;

.field private final providers:[Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

.field private final timeouts:[J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "network"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->providers:[Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->timeouts:[J

    return-void

    :array_0
    .array-data 8
        0xea60
        0x493e0
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;-><init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized failure(Ljava/lang/Exception;)V
    .locals 5
    .param p1    # Ljava/lang/Exception;

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->providers:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->timeouts:[J

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    aget-wide v1, v1, v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->providers:[Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    aget-object v3, v3, v4

    # invokes: Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;
    invoke-static {v0, p0, v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->access$200(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized start()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->timeouts:[J

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    aget-wide v1, v1, v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->providers:[Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->current:I

    aget-object v3, v3, v4

    # invokes: Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;
    invoke-static {v0, p0, v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->access$200(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    # invokes: Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->getLocationManager()Landroid/location/LocationManager;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->access$400(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized success(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;-><init>(Z)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    # getter for: Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->ctx:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->access$300(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->listener:Landroid/location/LocationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->success(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V

    return-void
.end method
