.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/utils/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpNewsTopics(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
        "<",
        "Ljava/util/LinkedHashSet",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

.field final synthetic val$context:Landroid/app/Activity;

.field final synthetic val$model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/app/Activity;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->val$context:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->val$model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;

    const-string v0, "Genie"

    const-string v1, "Failed to retrieve news topics"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/LinkedHashSet;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->success(Ljava/util/LinkedHashSet;)V

    return-void
.end method

.method public success(Ljava/util/LinkedHashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->allNewsSections:Ljava/util/Set;
    invoke-static {v0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$702(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Ljava/util/Set;)Ljava/util/Set;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->val$context:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;Ljava/util/LinkedHashSet;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
