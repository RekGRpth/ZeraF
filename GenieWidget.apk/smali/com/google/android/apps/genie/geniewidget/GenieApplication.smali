.class public Lcom/google/android/apps/genie/geniewidget/GenieApplication;
.super Landroid/app/Application;
.source "GenieApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;,
        Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;
    }
.end annotation


# static fields
.field private static context:Landroid/content/Context;

.field private static currentLocation:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

.field private static geniePreferences:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

.field private static network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;


# instance fields
.field private alarm:Landroid/app/AlarmManager;

.field private genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

.field private locationProvider:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

.field private miniWidget:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

.field private miniWidgetController:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

.field private miniWidgetDataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

.field private miniWidgetModel:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private miniWidgetView:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;

.field private newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

.field private final postToUI:Landroid/os/Handler;

.field private prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

.field private systemServicesMonitor:Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->postToUI:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->currentLocation:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    sput-object p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->currentLocation:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-object p0
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidget:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Landroid/app/AlarmManager;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->alarm:Landroid/app/AlarmManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->postToUI:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->systemServicesMonitor:Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->locationProvider:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    return-object v0
.end method

.method private ensureScheduledUpdate()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v3, 0x7f0c0006

    const-string v4, "10800"

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v0, v2, v4

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v3, 0x7f0c000c

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getLongPreference(IJ)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->scheduleModelUpdate(Z)V

    :cond_0
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->context:Landroid/content/Context;

    return-object v0
.end method

.method public static getGeniePrefs()Lcom/google/android/apps/genie/geniewidget/GeniePreferences;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->geniePreferences:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    return-object v0
.end method

.method public static getLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->currentLocation:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-object v0
.end method

.method public static getNetwork()Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    return-object v0
.end method

.method public static getPrefs()Landroid/content/SharedPreferences;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static setLocation(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    sput-object p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->currentLocation:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-void
.end method


# virtual methods
.method public formatMillis(J)Ljava/lang/String;
    .locals 1
    .param p1    # J

    const/16 v0, 0x11

    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    return-object v0
.end method

.method public getLastKnownLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->locationProvider:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->getLastKnownLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->currentLocation:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->currentLocation:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-object v0
.end method

.method public getMiniWidgetController()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetController:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    return-object v0
.end method

.method public getMiniWidgetDataService()Lcom/google/android/apps/genie/geniewidget/services/DataService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetDataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    return-object v0
.end method

.method public getMiniWidgetManager()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidget:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    return-object v0
.end method

.method public getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetModel:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    return-object v0
.end method

.method public getMiniWidgetView()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetView:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;

    return-object v0
.end method

.method public getNewsImageCache()Lcom/google/android/apps/genie/geniewidget/utils/FileCache;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    return-object v0
.end method

.method public getPrefetchHelper()Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    return-object v0
.end method

.method public getWeatherIconResource(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    const/16 v2, 0x2f

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ".png"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "SIZE"

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "drawable"

    const-string v4, "com.google.android.apps.genie.geniewidget"

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 10

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    sput-object p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/network/NetworkConnectionFactory;->create(Landroid/content/Context;)Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;

    const-string v0, "news_image_cache"

    invoke-direct {v6, p0, v0}, Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    const/16 v1, 0x64

    invoke-direct {v0, v6, v1}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;I)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetModel:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/services/DataService;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetModel:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/services/DataService;-><init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Lcom/google/android/apps/genie/geniewidget/GenieContext;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetDataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetModel:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidget:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetModel:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    sget-object v4, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;Lcom/google/android/apps/genie/geniewidget/utils/FileCache;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetController:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidgetView:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->alarm:Landroid/app/AlarmManager;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->geniePreferences:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->locationProvider:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->systemServicesMonitor:Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;

    return-void
.end method

.method public onDeviceStartup()V
    .locals 13

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v10, 0x7f0c000c

    const-wide/16 v11, -0x1

    invoke-interface {v9, v10, v11, v12}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getLongPreference(IJ)J

    move-result-wide v1

    const-wide/16 v9, -0x1

    cmp-long v9, v1, v9

    if-nez v9, :cond_1

    const-string v9, "Genie"

    const-string v10, "ignoring BOOT_COMPLETE since app has never been started"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v9}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->cancelScheduledModelUpdate()V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v9

    const v10, 0x7f0c0005

    invoke-virtual {p0, v10}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v10, 0x7f0c000b

    const-wide/16 v11, 0x0

    invoke-interface {v9, v10, v11, v12}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getLongPreference(IJ)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v10, 0x7f0c0006

    const-string v11, "10800"

    invoke-interface {v9, v10, v11}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    mul-long v7, v9, v11

    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-eqz v9, :cond_2

    if-eqz v0, :cond_3

    add-long v9, v3, v7

    cmp-long v9, v9, v5

    if-gez v9, :cond_3

    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v9}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->resetScheduleFalloff()V

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const/4 v10, 0x1

    invoke-interface {v9, v10}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->scheduleModelUpdate(Z)V

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->scheduleModelUpdate(Z)V

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 0

    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    return-void
.end method

.method public onTerminate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void
.end method

.method public onWidgetUpdate()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->ensureScheduledUpdate()V

    return-void
.end method

.method public requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;J)V
    .locals 2
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
            ">;J)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->locationProvider:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/GenieApplication$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$1;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieApplication;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;J)V

    return-void
.end method

.method public requestRedraw(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public updateContentProvider(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;J)V
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
    .param p3    # J

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieApplication;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;J)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
