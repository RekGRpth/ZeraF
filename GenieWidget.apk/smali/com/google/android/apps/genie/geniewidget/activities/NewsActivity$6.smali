.class Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;
.super Ljava/lang/Object;
.source "NewsActivity.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$802(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$900(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0c002c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressControl:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$100(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public invalidate()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->refreshNewsViews()V

    goto :goto_0
.end method

.method public progress(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;I)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->postToUI:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1200(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;ILcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public success()V
    .locals 6

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getTabIds()Ljava/util/HashSet;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->newsTopicsHaveChanged(Ljava/util/HashSet;)Z
    invoke-static {v3, v1}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1400(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Ljava/util/HashSet;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->buildUI()V
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1500(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingWeather:Z
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1600(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->refreshWeatherView()V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingWeather:Z
    invoke-static {v3, v5}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1602(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingNewsTopics:Z
    invoke-static {v3, v5}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1102(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->contentFrame:Landroid/widget/FrameLayout;
    invoke-static {v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1700(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/FrameLayout;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->showTabView(Landroid/widget/FrameLayout;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z
    invoke-static {v3, v5}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$802(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1800(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;
    invoke-static {v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1800(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->setCurrentTabNumber(I)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;
    invoke-static {v3, v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1802(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    const v4, 0x7f050003

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$2;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$000(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
