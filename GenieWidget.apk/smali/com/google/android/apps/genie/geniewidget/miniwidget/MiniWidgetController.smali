.class public Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
.super Ljava/lang/Object;
.source "MiniWidgetController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;
    }
.end annotation


# static fields
.field private static final log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

.field private final genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

.field private lastRequest:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

.field private final model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private final network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

.field private final networkCallback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;"
        }
    .end annotation
.end field

.field private final newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

.field private final progressObservers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;",
            ">;"
        }
    .end annotation
.end field

.field private refreshThreadHash:I

.field private requestInFlight:Z

.field private requestLatch:Ljava/util/concurrent/CountDownLatch;

.field private retryNumber:I

.field private shouldReschedule:Z

.field private final widgetConfig:Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->getLogger()Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;Lcom/google/android/apps/genie/geniewidget/utils/FileCache;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .param p4    # Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;
    .param p5    # Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/utils/Clock$SystemClock;

    invoke-direct {v1}, Lcom/google/android/apps/genie/geniewidget/utils/Clock$SystemClock;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestInFlight:Z

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;-><init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->networkCallback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->appContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    const v1, 0x7f0c000a

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    invoke-direct {v1, v0}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->widgetConfig:Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    iput-object p5, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->internalRefreshData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/utils/FileCache;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->getNewsImageUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/utils/Clock;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyFailure(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->setRequestInFlight(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->maybeRetry()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyProgress(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifySuccess()V

    return-void
.end method

.method private acquireLocationAndRefresh(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;ZZ)V
    .locals 5
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Z
    .param p3    # Z

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getLastKnownLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->setLocation(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->internalRefreshData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;

    invoke-direct {v2, p0, p1, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;-><init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V

    const-wide/16 v3, 0x2710

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;J)V

    goto :goto_0
.end method

.method private buildRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v5, 0x7f0c0004

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->userAllowLocation()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->setFakeLocation(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getTypeFilter()Ljava/util/ArrayList;

    move-result-object v4

    const/16 v5, 0xf

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v4}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getNewsTopicPreference()Ljava/util/List;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v4}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getCustomTopicPreference()Ljava/util/List;

    move-result-object v0

    if-eqz v2, :cond_1

    invoke-virtual {p1, v2}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->setNewsTopics(Ljava/util/List;)V

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->setCustomTopics(Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method private getNewsImageUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "content://com.google.android.apps.genie.geniewidget.newsimage"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "news_image"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized internalRefreshData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V
    .locals 8
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Z

    const-wide/16 v4, 0x0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->userAllowLocation()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->removeTypeFilter(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->LOADING:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->setState(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;)V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyLoading()V

    if-eqz p2, :cond_1

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->lastRequest:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    const-wide/16 v2, 0x0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->networkCallback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;->getRawFeed(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;JJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getRefreshIntervalMillis()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private maybeRetry()V
    .locals 12

    const/4 v5, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->retryNumber:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->appContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/ConnectivityManager;

    invoke-virtual {v10}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-virtual {v11}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->appContext:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->retryNumber:I

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    move v4, v1

    :goto_0
    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->retryNumber:I

    add-int/lit8 v7, v2, 0x1

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->refreshThreadHash:I

    move v2, v1

    move v3, v1

    move v6, v5

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZZILjava/lang/String;I)V

    :goto_1
    return-void

    :cond_0
    move v4, v5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->retryNumber:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->registerConnectivityIntent(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->scheduleModelUpdate(Z)V

    goto :goto_1
.end method

.method private declared-synchronized notifyAcquiringLocation()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;->ACQUIRING_LOCATION:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;->progress(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->requestWidgetRefresh()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifyFailure(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    invoke-interface {v0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;->failure(Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->requestWidgetRefresh()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifyLoading()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;->LOADING:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;->progress(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->requestWidgetRefresh()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private notifyNewsImageUpdated()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$4;-><init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->runOnUIThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private declared-synchronized notifyProgress(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;->LOADING:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;

    invoke-interface {v0, v2, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;->progress(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifySuccess()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;->success()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->requestWidgetRefresh()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private declared-synchronized setRequestInFlight(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestInFlight:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private userAllowLocation()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v2, 0x7f0c0003

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getBooleanPreference(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->appContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/util/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public fetchNewsImage(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/util/concurrent/CountDownLatch;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/NewsItem;
    .param p2    # Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getGuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v6}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->getNewsImageUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->setThumbnailUri(Landroid/net/Uri;)V

    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->network:Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;

    invoke-direct {v5, p0, p2, v6, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;-><init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Ljava/util/concurrent/CountDownLatch;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)V

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;->getUrl(Ljava/lang/String;JZLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    goto :goto_0
.end method

.method public getLastRequest()Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->lastRequest:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    return-object v0
.end method

.method public getWidgetConfig()Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->widgetConfig:Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    return-object v0
.end method

.method public declared-synchronized isRequestInFlight()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestInFlight:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loadNewsImages()V
    .locals 4

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    if-eqz v3, :cond_1

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->fetchNewsImage(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->commit()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyNewsImageUpdated()V

    :cond_1
    return-void

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public logClick([BII)V
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->logClick([BII)V

    return-void
.end method

.method public declared-synchronized registerProgressObserver(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public removeStaleNews()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/utils/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getLastUpdateTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    const-wide/32 v2, 0xa4cb800

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->resetNews()V

    :cond_0
    return-void
.end method

.method public declared-synchronized requestData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;ZZZLjava/util/concurrent/CountDownLatch;II)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Ljava/util/concurrent/CountDownLatch;
    .param p6    # I
    .param p7    # I

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->isRequestInFlight()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->resetDataLocale()V

    iput p6, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->retryNumber:I

    iput-object p5, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestLatch:Ljava/util/concurrent/CountDownLatch;

    iput p7, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->refreshThreadHash:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->buildRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)V

    iput-boolean p4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->shouldReschedule:Z

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->userAllowLocation()Z

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->setRequestInFlight(Z)V

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyAcquiringLocation()V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->acquireLocationAndRefresh(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;ZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_1
    :try_start_2
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->internalRefreshData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized unregisterProgressObserver(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->progressObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
