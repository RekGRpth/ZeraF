.class public Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
.super Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;
.source "NewsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$7;,
        Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$AnimationAdapter;
    }
.end annotation


# instance fields
.field private contentFrame:Landroid/widget/FrameLayout;

.field private ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

.field private dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private newsItemClickListener:Landroid/view/View$OnClickListener;

.field private placeHolder:Landroid/widget/FrameLayout;

.field private final postToUI:Landroid/os/Handler;

.field private prefListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private preferencesChanged:Z

.field private preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

.field private progressBar:Landroid/widget/ProgressBar;

.field private progressControl:Landroid/view/View;

.field private progressMessage:Landroid/widget/TextView;

.field private final progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

.field private progressPanel:Landroid/view/View;

.field private refreshOngoing:Z

.field private updatingNewsTopics:Z

.field private updatingWeather:Z

.field private viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

.field private viewMediatorTab:Ljava/lang/Integer;

.field private weatherSettingRequestListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->postToUI:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingNewsTopics:Z

    iput-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingWeather:Z

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressControl:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingNewsTopics:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingNewsTopics:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->postToUI:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Ljava/util/HashSet;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Ljava/util/HashSet;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->newsTopicsHaveChanged(Ljava/util/HashSet;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->buildUI()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingWeather:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingWeather:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->contentFrame:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/NewsItem;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->launchNewsContentActivity(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->showWeatherSettingView()V

    return-void
.end method

.method static synthetic access$502(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesChanged:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;ZZZZZ)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->startRefresh(ZZZZZ)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method private buildUI()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->placeHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->contentFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->newsItemClickListener:Landroid/view/View$OnClickListener;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->weatherSettingRequestListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->placeHolder:Landroid/widget/FrameLayout;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;-><init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/widget/FrameLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    return-void
.end method

.method public static isNetworkAvailable(Landroid/content/Context;)Z
    .locals 8
    .param p0    # Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v3

    move-object v0, v3

    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v6

    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v6, v7, :cond_0

    const/4 v6, 0x1

    :goto_1
    return v6

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private launchNewsContentActivity(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/lang/String;)V
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "url-string"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "guid-string"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getGuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "title"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Referer"

    const-string v3, "http://news.google.com/"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.android.browser.headers"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private launchPreferenceActivity()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGeniePrefs()Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->newPreferencesSnapshot()Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    iput-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesChanged:Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private newsTopicsHaveChanged(Ljava/util/HashSet;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getCurrentNewsTopics()Ljava/util/List;

    move-result-object v0

    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    iget-object v4, v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private prepareWidgetLaunch()Z
    .locals 9

    const/4 v8, -0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "android.intent.action.MAIN"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v1, v6, v7

    const-string v6, "com.google.android.apps.genie.EVENT_ID"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    const-string v6, "com.google.android.apps.genie.INDEX"

    invoke-virtual {v4, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    const/16 v7, 0xb

    invoke-virtual {v6, v2, v3, v7}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->logClick([BII)V

    const-string v6, "com.google.android.apps.genie.weather"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->WEATHER:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->setClickedWidgetItemType(Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;I)V

    const/4 v5, 0x1

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->NEWS:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    invoke-virtual {v6, v7, v3}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->setClickedWidgetItemType(Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;I)V

    const/4 v5, 0x1

    goto :goto_0
.end method

.method private refreshData(ZZZZZZ)V
    .locals 8
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z

    if-nez p5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->startRefresh(ZZZZZ)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f050002

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;ZZZZZ)V

    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private restoreState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v6, "tab"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-gez v6, :cond_1

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    :cond_1
    const-string v6, "lat"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    const-string v6, "lng"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    const-string v6, "acc"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;-><init>(DDF)V

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->setLocation(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V

    goto :goto_0
.end method

.method private showWeatherSettingView()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGeniePrefs()Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->newPreferencesSnapshot()Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    iput-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesChanged:Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startRefresh(ZZZZZ)V
    .locals 0
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z

    invoke-static/range {p0 .. p5}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZ)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v3, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesChanged:Z

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesChanged:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferredTopicsChanged()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->customTopicsChanged()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingNewsTopics:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressMessage:Landroid/widget/TextView;

    const v2, 0x7f0c0019

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move-object v0, p0

    move v2, v1

    move v4, v1

    move v5, v1

    move v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshData(ZZZZZZ)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->prefetchPagesChanged()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGeniePrefs()Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->isPrefetchPagesEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->triggerPrefetch(Landroid/content/Context;)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->useMyLocationChanged()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->weatherHasBeenUpdated()Z

    move-result v0

    if-nez v0, :cond_5

    iput-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingWeather:Z

    move-object v4, p0

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v3

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshData(ZZZZZZ)V

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->triggerPurge(Landroid/content/Context;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->preferencesSnapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->useCelsiusChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->refreshWeatherView()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->requestWidgetRefresh()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->restoreState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetController()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetDataService()Lcom/google/android/apps/genie/geniewidget/services/DataService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    const v0, 0x7f04000c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->setContentView(I)V

    const v0, 0x7f0b002f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->contentFrame:Landroid/widget/FrameLayout;

    const v0, 0x7f0b0030

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->placeHolder:Landroid/widget/FrameLayout;

    const v0, 0x7f0b0032

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressMessage:Landroid/widget/TextView;

    const v0, 0x7f0b0033

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressBar:Landroid/widget/ProgressBar;

    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressControl:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressControl:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$2;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->newsItemClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$3;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->weatherSettingRequestListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$4;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->prefListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->prefListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->buildUI()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onDestroy()V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->prefListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/Menu;

    const/4 v2, 0x1

    if-eqz p2, :cond_2

    const v1, 0x7f0b0071

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_1
    return v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v2

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v6, 0x0

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    const v0, 0x7f0b0071

    if-ne v0, v7, :cond_0

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshData(ZZZZZZ)V

    :goto_0
    return v1

    :cond_0
    const v0, 0x7f0b0072

    if-ne v0, v7, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->launchPreferenceActivity()V

    goto :goto_0

    :cond_1
    move v1, v6

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onPause()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->restoreState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 7

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasAppLocaleChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Genie"

    const-string v2, "LANGUAGE CHANGE"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->resetAppLocale()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->buildUI()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->prepareWidgetLaunch()Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->removeStaleNews()V

    iput-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshOngoing:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->contentFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->showTabView(Landroid/widget/FrameLayout;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->setCurrentTabNumber(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediatorTab:Ljava/lang/Integer;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->requestWidgetRefresh()V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressMessage:Landroid/widget/TextView;

    const v2, 0x7f0c001a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move-object v0, p0

    move v2, v1

    move v4, v3

    move v5, v3

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshData(ZZZZZZ)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "tab"

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getCurrentTabNumber()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "lat"

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getLat()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    const-string v1, "lng"

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getLng()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    const-string v1, "acc"

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getAccuracy()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->registerProgressObserver(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->unregisterProgressObserver(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;)V

    return-void
.end method
