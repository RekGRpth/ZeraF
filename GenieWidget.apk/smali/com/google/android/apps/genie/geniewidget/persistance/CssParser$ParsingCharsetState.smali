.class final enum Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;
.super Ljava/lang/Enum;
.source "CssParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ParsingCharsetState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

.field public static final enum CHARSET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

.field public static final enum WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    const-string v1, "WHITE_SPACES_BEFORE_TOKEN"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    const-string v1, "CHARSET"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->CHARSET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->CHARSET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    return-object v0
.end method
