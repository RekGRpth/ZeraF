.class public Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;
.super Ljava/lang/Object;
.source "PrefetchHelper.java"


# static fields
.field private static NO_MEDIA_FILTER:Ljava/io/FilenameFilter;


# instance fields
.field private cacheRoot:Ljava/io/File;

.field private cancelled:Z

.field private final fetcher:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;

.field private fs:Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;

.field private ready:Z

.field private final rootDirectory:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper$1;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->NO_MEDIA_FILTER:Ljava/io/FilenameFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ready:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cancelled:Z

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->rootDirectory:Ljava/io/File;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->fetcher:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ensureCacheDirectory()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ready:Z

    return-void
.end method

.method private deleteDirectory(Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v0, v1

    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->deleteDirectory(Ljava/io/File;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v2

    return v2
.end method

.method private ensureCacheDirectory()Z
    .locals 6

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->rootDirectory:Ljava/io/File;

    const-string v5, "news-content-cache"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    invoke-direct {v3, v4}, Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;-><init>(Ljava/io/File;)V

    iput-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->fs:Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    const-string v4, ".nomedia"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->deleteDirectory(Ljava/io/File;)Z

    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cancelled:Z

    return-void
.end method

.method public cleanup(Ljava/util/ArrayList;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;)I"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-direct {v0, v9}, Ljava/util/HashSet;-><init>(I)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getGuid()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    if-nez v9, :cond_2

    const/4 v2, 0x0

    :goto_1
    if-nez v2, :cond_3

    const/4 v3, 0x0

    :cond_1
    return v3

    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    sget-object v10, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->NO_MEDIA_FILTER:Ljava/io/FilenameFilter;

    invoke-virtual {v9, v10}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    move-object v1, v2

    array-length v6, v1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v6, :cond_1

    aget-object v4, v1, v5

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-direct {p0, v4}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->deleteDirectory(Ljava/io/File;)Z

    move-result v8

    add-int/lit8 v3, v3, 0x1

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public getCacheLocation(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v2, Ljava/io/File;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    invoke-direct {v2, v5, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v5

    const-string v6, "precache.idx"

    invoke-direct {v3, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->fs:Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;

    new-instance v6, Ljava/io/File;

    const-string v7, "precache.idx"

    invoke-direct {v6, v2, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;->read(Ljava/io/File;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->deserialize([B)Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->getStatus()Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->READY:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    if-ne v5, v6, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->getLocalCache()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->getLocalCache()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ready:Z

    return v0
.end method

.method public prefetch(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ensureCacheDirectory()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ready:Z

    invoke-virtual {p0, p2}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->getCacheLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    new-instance v3, Ljava/io/File;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    invoke-direct {v3, v7, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    new-instance v4, Ljava/io/File;

    const-string v7, ".nomedia"

    invoke-direct {v4, v3, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;

    invoke-direct {v1, p2}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->fetcher:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, p1, v8, p3}, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->retrieve(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    move-result-object v5

    iget-boolean v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cancelled:Z

    if-nez v7, :cond_0

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;->SUCCEESSFUL:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    if-ne v5, v7, :cond_2

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->READY:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->setStatus(Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->fetcher:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->getCacheName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->setCacheFile(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->fs:Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;

    new-instance v7, Ljava/io/File;

    const-string v8, "precache.idx"

    invoke-direct {v7, v3, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->serialize()[B

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;->write(Ljava/io/File;[B)V

    const/4 v6, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->deleteDirectory(Ljava/io/File;)Z

    goto :goto_0

    :cond_2
    sget-object v7, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->FAILED:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    invoke-virtual {v1, v7}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->setStatus(Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;)V

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->fs:Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;

    new-instance v8, Ljava/io/File;

    const-string v9, "precache.idx"

    invoke-direct {v8, v3, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->serialize()[B

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/genie/geniewidget/utils/DefaultFileSystemProvider;->write(Ljava/io/File;[B)V

    goto :goto_0
.end method

.method public purge()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cacheRoot:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->deleteDirectory(Ljava/io/File;)Z

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ensureCacheDirectory()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->ready:Z

    return-void
.end method
