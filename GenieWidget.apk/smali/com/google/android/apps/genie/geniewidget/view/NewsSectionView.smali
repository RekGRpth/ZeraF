.class public Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;
.super Landroid/widget/LinearLayout;
.source "NewsSectionView.java"


# instance fields
.field private newsItemViewsByGuid:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;",
            ">;"
        }
    .end annotation
.end field

.field private selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->newsItemViewsByGuid:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->newsItemViewsByGuid:Ljava/util/Hashtable;

    return-void
.end method

.method public static createNewsSectionView(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p2    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;"
        }
    .end annotation

    const/4 v4, 0x1

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04000d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->refreshData(Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->buildDrawingCache()V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->setAnimationCacheEnabled(Z)V

    return-object v0
.end method

.method private refreshData(Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 9
    .param p2    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    const/4 v7, -0x1

    const/4 v8, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_2

    :cond_0
    new-instance v4, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v6, 0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0c0022

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v6, 0x32

    invoke-virtual {v4, v8, v6, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->addView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->invalidate()V

    return-void

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->createNewsItemView(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    move-result-object v5

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v5, v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->setIndex(I)V

    invoke-virtual {v5, p2}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->newsItemViewsByGuid:Ljava/util/Hashtable;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getGuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->addView(Landroid/view/View;)V

    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public getNewsItemView(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->newsItemViewsByGuid:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->getIndex()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->getHeight()I

    move-result v0

    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    mul-int v4, v1, v0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->getWidth()I

    move-result v5

    add-int/lit8 v6, v1, 0x1

    mul-int/2addr v6, v0

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, p0, v2, v4}, Landroid/view/ViewParent;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    invoke-interface {v3, p0, v4}, Landroid/view/ViewParent;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    :cond_0
    return-void
.end method

.method public setSelectedNewsItem(Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->selectedView:Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    return-void
.end method
