.class public Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
.super Ljava/lang/Object;
.source "GeniePayload.java"


# instance fields
.field private final isCached:Z

.field private final payload:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;",
            ">;"
        }
    .end annotation
.end field

.field private final timeStamp:J


# direct methods
.method public constructor <init>(Ljava/util/List;ZJ)V
    .locals 0
    .param p2    # Z
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;",
            ">;ZJ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->payload:Ljava/util/List;

    iput-boolean p2, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->isCached:Z

    iput-wide p3, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->timeStamp:J

    return-void
.end method


# virtual methods
.method public getPayload()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->payload:Ljava/util/List;

    return-object v0
.end method

.method public getTimeStampMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->timeStamp:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GeniePayload: isCached: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->isCached:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " payload: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->payload:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->payload:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "[null]"

    goto :goto_0
.end method
