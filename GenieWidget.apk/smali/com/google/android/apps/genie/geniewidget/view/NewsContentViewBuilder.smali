.class public Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;
.super Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
.source "NewsContentViewBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder",
        "<",
        "Landroid/webkit/WebView;",
        ">;"
    }
.end annotation


# instance fields
.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private requestedNewsGuid:Ljava/lang/String;

.field private requestedNewsUrl:Ljava/lang/String;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentView;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/view/NewsContentView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->webView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    return-void
.end method


# virtual methods
.method public buildView()Landroid/webkit/WebView;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestedNewsGuid:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->resourceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->webView:Landroid/webkit/WebView;

    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefetchHelper()Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestedNewsGuid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->getCacheLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "file://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestedNewsGuid:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->resourceName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->webView:Landroid/webkit/WebView;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->webView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestedNewsUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestedNewsGuid:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->resourceName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->webView:Landroid/webkit/WebView;

    goto :goto_0
.end method

.method public bridge synthetic buildView()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->buildView()Landroid/webkit/WebView;

    move-result-object v0

    return-object v0
.end method

.method protected isStale()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public requestNews(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestedNewsGuid:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestedNewsUrl:Ljava/lang/String;

    return-void
.end method
