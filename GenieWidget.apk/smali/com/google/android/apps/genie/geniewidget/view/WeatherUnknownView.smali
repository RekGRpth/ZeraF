.class public Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;
.super Landroid/widget/RelativeLayout;
.source "WeatherUnknownView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static createWeatherUnknowView(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;Landroid/view/View$OnClickListener;)Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;
    .param p2    # Landroid/view/View$OnClickListener;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040016

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;->init(Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private init(Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;
    .param p2    # Landroid/view/View$OnClickListener;

    const v2, 0x7f0b005f

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0b005e

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;->NETWORK_FAILURE:Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;

    if-ne v2, p1, :cond_0

    const v2, 0x7f0c003a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const v2, 0x7f0c0039

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f0c003b

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/high16 v0, 0x40000000

    const/16 v1, 0x16a

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void
.end method
