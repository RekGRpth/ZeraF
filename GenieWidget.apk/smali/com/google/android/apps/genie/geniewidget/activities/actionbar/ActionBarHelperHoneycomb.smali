.class public Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperHoneycomb;
.super Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;
.source "ActionBarHelperHoneycomb.java"


# instance fields
.field private mOptionsMenu:Landroid/view/Menu;

.field private final mRefreshIndeterminateProgressView:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;-><init>(Landroid/app/Activity;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperHoneycomb;->mRefreshIndeterminateProgressView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperHoneycomb;->mOptionsMenu:Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
