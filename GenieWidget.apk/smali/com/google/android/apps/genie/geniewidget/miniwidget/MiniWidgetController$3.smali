.class Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;
.super Ljava/lang/Object;
.source "MiniWidgetController.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->fetchNewsImage(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/util/concurrent/CountDownLatch;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
        "<",
        "Ljava/lang/String;",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

.field final synthetic val$completeLatch:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$guid:Ljava/lang/String;

.field final synthetic val$story:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Ljava/util/concurrent/CountDownLatch;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$completeLatch:Ljava/util/concurrent/CountDownLatch;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$guid:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$story:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$completeLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public progress(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->success(Ljava/lang/String;[B)V

    return-void
.end method

.method public success(Ljava/lang/String;[B)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$1000(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_1

    array-length v1, p2

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$1000(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$guid:Ljava/lang/String;

    invoke-virtual {v1, v2, p2}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->put(Ljava/lang/String;[B)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$guid:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->getNewsImageUri(Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v1, v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$1100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$story:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->setThumbnailUri(Landroid/net/Uri;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$completeLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->newsImageCache:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$1000(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$guid:Ljava/lang/String;

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->put(Ljava/lang/String;[B)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$3;->val$story:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->setThumbnailUri(Landroid/net/Uri;)V

    goto :goto_0
.end method
