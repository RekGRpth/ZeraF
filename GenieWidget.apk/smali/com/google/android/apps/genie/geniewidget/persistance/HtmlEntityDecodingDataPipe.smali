.class Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;
.super Ljava/lang/Object;
.source "HtmlEntityDecodingDataPipe.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;


# static fields
.field private static entityMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

.field private entity:Ljava/lang/String;

.field private extraChar:I

.field private input:Ljava/io/InputStream;

.field private nextChar:I

.field private output:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&quot;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&amp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&apos;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lt;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&gt;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3e

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&nbsp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&iexcl;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&cent;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&pound;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&curren;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&yen;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&brvbar;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sect;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&uml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&copy;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xa9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ordf;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xaa

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&laquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xab

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&not;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xac

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&shy;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xad

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&reg;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xae

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&macr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xaf

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&deg;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&plusmn;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sup2;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sup3;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&acute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&micro;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&para;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&middot;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&cedil;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sup1;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xb9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ordm;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xba

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&raquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xbb

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&frac14;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xbc

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&frac12;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xbd

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&frac34;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xbe

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&iquest;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xbf

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Agrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Aacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Acirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Atilde;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Auml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Aring;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&AElig;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ccedil;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Egrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Eacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xc9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ecirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xca

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Euml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xcb

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Igrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xcc

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Iacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xcd

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Icirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xce

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Iuml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xcf

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ETH;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ntilde;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ograve;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Oacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ocirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Otilde;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ouml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&times;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Oslash;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ugrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xd9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Uacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xda

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Ucirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xdb

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Uuml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xdc

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Yacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xdd

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&THORN;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xde

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&szlig;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xdf

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&agrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&aacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&acirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&atilde;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&auml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&aring;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&aelig;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ccedil;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&egrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&eacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xe9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ecirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xea

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&euml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xeb

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&igrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xec

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&iacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xed

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&icirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xee

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&iuml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xef

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&eth;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ntilde;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ograve;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&oacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ocirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&otilde;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ouml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&divide;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&oslash;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ugrave;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xf9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&uacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xfa

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ucirc;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xfb

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&uuml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xfc

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&yacute;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xfd

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&thorn;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xfe

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&yuml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0xff

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&OElig;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x152

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&oelig;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x153

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Scaron;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x160

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&scaron;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x161

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Yuml;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x178

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&fnof;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x192

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&circ;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2c6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&tilde;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2dc

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Alpha;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x391

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Beta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x392

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Gamma;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x393

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Delta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x394

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Epsilon;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x395

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Zeta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x396

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Eta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x397

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Theta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x398

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Iota;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x399

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Kappa;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x39a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Lambda;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x39b

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Mu;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x39c

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Nu;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x39d

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Xi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x39e

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Omicron;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x39f

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Pi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Rho;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Sigma;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Tau;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Upsilon;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Phi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Chi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Psi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Omega;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3a9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&alpha;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&beta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&gamma;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&delta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&epsilon;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&zeta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&eta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&theta;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&iota;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3b9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&kappa;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3ba

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lambda;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3bb

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&mu;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3bc

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&nu;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3bd

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&xi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3be

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&omicron;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3bf

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&pi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rho;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sigmaf;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sigma;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&tau;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&upsilon;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&phi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&chi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c7

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&psi;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c8

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&omega;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3c9

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&thetasym;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3d1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&upsih;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3d2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&piv;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x3d6

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ensp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2002

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&emsp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2003

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&thinsp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2009

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&zwnj;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x200c

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&zwj;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x200d

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lrm;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x200e

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rlm;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x200f

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ndash;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2013

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&mdash;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2014

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lsquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2018

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rsquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2019

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sbquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x201a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ldquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x201c

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rdquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x201d

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&bdquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x201e

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&dagger;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2020

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Dagger;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2021

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&bull;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2022

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&hellip;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2026

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&permil;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2030

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&prime;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2032

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&Prime;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2033

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lsaquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2039

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rsaquo;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x203a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&oline;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x203e

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&frasl;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2044

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&euro;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x20ac

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&image;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2111

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&weierp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2118

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&real;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x211c

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&trade;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2122

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&alefsym;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2135

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&larr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2190

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&uarr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2191

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rarr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2192

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&darr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2193

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&harr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2194

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&crarr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x21b5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lArr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x21d0

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&uArr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x21d1

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rArr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x21d2

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&dArr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x21d3

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&hArr;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x21d4

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&forall;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2200

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&part;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2202

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&exist;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2203

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&empty;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2205

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&nabla;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2207

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&isin;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2208

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&notin;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2209

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ni;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x220b

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&prod;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x220f

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sum;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2211

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&minus;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2212

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lowast;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2217

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&radic;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x221a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&prop;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x221d

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&infin;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x221e

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ang;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2220

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&and;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2227

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&or;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2228

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&cap;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2229

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&cup;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x222a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&int;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x222b

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&there4;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2234

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sim;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x223c

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&cong;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2245

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&asymp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2248

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ne;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2260

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&equiv;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2261

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&le;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2264

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&ge;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2265

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sub;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2282

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sup;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2283

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&nsub;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2284

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sube;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2286

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&supe;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2287

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&oplus;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2295

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&otimes;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2297

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&perp;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x22a5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&sdot;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x22c5

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lceil;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2308

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rceil;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2309

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lfloor;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x230a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rfloor;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x230b

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&lang;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2329

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&rang;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x232a

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&loz;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x25ca

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&spades;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2660

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&clubs;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2663

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&hearts;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2665

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    const-string v1, "&diams;"

    new-instance v2, Ljava/lang/Character;

    const/16 v3, 0x2666

    invoke-direct {v2, v3}, Ljava/lang/Character;-><init>(C)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->nextChar:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->extraChar:I

    return-void
.end method

.method private decodeEntity(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entityMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v1

    goto :goto_0
.end method

.method private getNextChar()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, -0x1

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->extraChar:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->extraChar:I

    neg-int v1, v3

    iput v5, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->extraChar:I

    :goto_0
    return v1

    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->popFront()B

    move-result v0

    :goto_1
    const/16 v3, 0x26

    if-eq v0, v3, :cond_2

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v4, v0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    if-eq v0, v5, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v4, v0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    const/16 v3, 0x23

    if-ne v0, v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->parseUnicode()I

    move-result v2

    :goto_2
    if-eq v2, v5, :cond_7

    move v1, v2

    goto :goto_0

    :cond_3
    const/16 v3, 0x61

    if-lt v0, v3, :cond_4

    const/16 v3, 0x7a

    if-le v0, v3, :cond_5

    :cond_4
    const/16 v3, 0x41

    if-lt v0, v3, :cond_6

    const/16 v3, 0x5a

    if-gt v0, v3, :cond_6

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->parseEntity()I

    move-result v2

    goto :goto_2

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->popFromTheBuffer()I

    move-result v1

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->popFromTheBuffer()I

    move-result v1

    goto :goto_0
.end method

.method private parseDecimal(I)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v5, 0x39

    const/16 v4, 0x30

    const/4 v1, -0x1

    if-lt p1, v4, :cond_2

    if-gt p1, v5, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-lt p1, v4, :cond_0

    if-gt p1, v5, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v2, p1, -0x30

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v3, p1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result p1

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->toByteArray()[B

    move-result-object v4

    const-string v5, "ISO-8859-1"

    invoke-direct {v3, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->clear()V

    :goto_2
    const/16 v2, 0x3b

    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v2, p1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    :cond_1
    return v0

    :catch_0
    move-exception v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private parseEntity()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, -0x1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    :goto_0
    if-eq v0, v5, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->capacity()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2

    const/16 v1, 0x61

    if-lt v0, v1, :cond_0

    const/16 v1, 0x7a

    if-le v0, v1, :cond_1

    :cond_0
    const/16 v1, 0x41

    if-lt v0, v1, :cond_2

    const/16 v1, 0x5a

    if-gt v0, v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->toByteArray()[B

    move-result-object v3

    const-string v4, "ISO-8859-1"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->decodeEntity(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_4

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;

    :goto_2
    const/16 v2, 0x3b

    if-eq v0, v2, :cond_3

    if-eq v0, v5, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v0, v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    :cond_3
    return v1

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->clear()V

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private parseHex(I)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v7, 0x46

    const/16 v6, 0x41

    const/16 v5, 0x39

    const/16 v4, 0x30

    const/4 v1, -0x1

    if-lt p1, v4, :cond_0

    if-le p1, v5, :cond_2

    :cond_0
    const/16 v0, 0x61

    if-lt p1, v0, :cond_1

    const/16 v0, 0x66

    if-le p1, v0, :cond_2

    :cond_1
    if-lt p1, v6, :cond_8

    if-gt p1, v7, :cond_8

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-lt p1, v4, :cond_3

    if-le p1, v5, :cond_5

    :cond_3
    const/16 v2, 0x61

    if-lt p1, v2, :cond_4

    const/16 v2, 0x66

    if-le p1, v2, :cond_5

    :cond_4
    if-lt p1, v6, :cond_6

    if-gt p1, v7, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v2

    const/16 v3, 0x9

    if-ge v2, v3, :cond_6

    :cond_5
    sparse-switch p1, :sswitch_data_0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v3, p1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result p1

    goto :goto_0

    :sswitch_0
    mul-int/lit8 v0, v0, 0x10

    add-int/lit8 v2, p1, -0x30

    add-int/2addr v0, v2

    goto :goto_1

    :sswitch_1
    mul-int/lit8 v0, v0, 0x10

    add-int/lit8 v2, p1, -0x41

    add-int/lit8 v2, v2, 0xa

    add-int/2addr v0, v2

    goto :goto_1

    :sswitch_2
    mul-int/lit8 v0, v0, 0x10

    add-int/lit8 v2, p1, -0x61

    add-int/lit8 v2, v2, 0xa

    add-int/2addr v0, v2

    goto :goto_1

    :cond_6
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->toByteArray()[B

    move-result-object v4

    const-string v5, "ISO-8859-1"

    invoke-direct {v3, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->clear()V

    :goto_3
    const/16 v2, 0x3b

    if-eq p1, v2, :cond_7

    if-eq p1, v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v2, p1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    :cond_7
    return v0

    :catch_0
    move-exception v2

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
        0x37 -> :sswitch_0
        0x38 -> :sswitch_0
        0x39 -> :sswitch_0
        0x41 -> :sswitch_1
        0x42 -> :sswitch_1
        0x43 -> :sswitch_1
        0x44 -> :sswitch_1
        0x45 -> :sswitch_1
        0x46 -> :sswitch_1
        0x61 -> :sswitch_2
        0x62 -> :sswitch_2
        0x63 -> :sswitch_2
        0x64 -> :sswitch_2
        0x65 -> :sswitch_2
        0x66 -> :sswitch_2
    .end sparse-switch
.end method

.method private parseUnicode()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x30

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    if-eq v0, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v3, v0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    :cond_0
    const/16 v2, 0x78

    if-ne v0, v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    if-eq v0, v4, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->parseHex(I)I

    move-result v1

    :goto_0
    const/16 v2, 0x7f

    if-ge v1, v2, :cond_4

    move v2, v1

    :goto_1
    return v2

    :cond_2
    :goto_2
    if-ne v0, v4, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->input:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_2

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->parseDecimal(I)I

    move-result v1

    goto :goto_0

    :cond_4
    const v2, 0xffff

    if-ge v1, v2, :cond_5

    neg-int v2, v1

    goto :goto_1

    :cond_5
    const/high16 v2, 0x10000

    sub-int/2addr v1, v2

    and-int/lit16 v2, v1, 0x3ff

    const v3, 0xdc00

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->extraChar:I

    shr-int/lit8 v2, v1, 0xa

    const v3, 0xd800

    add-int/2addr v2, v3

    neg-int v2, v2

    goto :goto_1
.end method

.method private popFromTheBuffer()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->popFront()B

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public peek()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->nextChar:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->getNextChar()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->nextChar:I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->nextChar:I

    return v0
.end method

.method public pipe()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->peek()I

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;

    const-string v3, "ISO-8859-1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->entity:Ljava/lang/String;

    :cond_0
    :goto_1
    iput v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->nextChar:I

    return v0

    :cond_1
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->peek()I

    move-result v0

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->nextChar:I

    return v0
.end method

.method public write(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public write([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public writeEscaped(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const v7, 0xd800

    const/4 v2, -0x1

    const/4 v0, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v1, v2, :cond_0

    sub-int/2addr v1, v7

    shl-int/lit8 v1, v1, 0xa

    const v4, 0xdc00

    sub-int/2addr v3, v4

    add-int/2addr v1, v3

    const/high16 v3, 0x10000

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "&#x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ";"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/io/OutputStream;->write([B)V

    move v1, v2

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sparse-switch v3, :sswitch_data_0

    const/16 v4, 0x20

    if-lt v3, v4, :cond_1

    const/16 v4, 0x7f

    if-gt v3, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    invoke-virtual {v4, v3}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    :sswitch_0
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    const-string v4, "&amp;"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    :sswitch_1
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    const-string v4, "&apos;"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    :sswitch_2
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    const-string v4, "&quot;"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    :sswitch_3
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    const-string v4, "&lt;"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    :sswitch_4
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    const-string v4, "&gt;"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    :cond_1
    if-lt v3, v7, :cond_2

    const v4, 0xdbff

    if-gt v3, v4, :cond_2

    move v1, v3

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->output:Ljava/io/OutputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "&#x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ";"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    :cond_3
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_2
        0x26 -> :sswitch_0
        0x27 -> :sswitch_1
        0x3c -> :sswitch_3
        0x3e -> :sswitch_4
    .end sparse-switch
.end method
