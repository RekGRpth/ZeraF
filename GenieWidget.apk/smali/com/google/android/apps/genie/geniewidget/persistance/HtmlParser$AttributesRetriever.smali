.class Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;
.super Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;
.source "HtmlParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AttributesRetriever"
.end annotation


# instance fields
.field private attributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private interestingTags:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private postpondingTags:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->attributes:Ljava/util/HashMap;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->interestingTags:Ljava/util/HashSet;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->postpondingTags:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->attributes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public onAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;)Z
    .locals 11
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v10, -0x1

    const/4 v6, 0x1

    const/4 v4, 0x1

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->postpondingTags:Ljava/util/HashSet;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->postpondingTags:Ljava/util/HashSet;

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    move v4, v6

    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    const/16 v8, 0x20

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write([B)V

    const/16 v8, 0x3d

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write(I)V

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->interestingTags:Ljava/util/HashSet;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->interestingTags:Ljava/util/HashSet;

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    :cond_2
    if-eqz v4, :cond_5

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->bypassAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    :cond_3
    :goto_1
    return v6

    :cond_4
    move v4, v7

    goto :goto_0

    :cond_5
    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    const/16 v8, 0x27

    if-ne v1, v8, :cond_7

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v8, "\'"

    invoke-direct {v5, p1, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    if-eqz v4, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write(I)V

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;

    invoke-direct {v0, v5, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    if-eqz v4, :cond_a

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->pipe()I

    move-result v1

    :goto_3
    if-eq v1, v10, :cond_d

    if-lez v1, :cond_b

    invoke-virtual {v2, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :goto_4
    if-eqz v4, :cond_c

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->pipe()I

    move-result v1

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    goto :goto_2

    :cond_7
    const/16 v8, 0x22

    if-ne v1, v8, :cond_9

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v8, "\""

    invoke-direct {v5, p1, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    if-eqz v4, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write(I)V

    goto :goto_2

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    goto :goto_2

    :cond_9
    new-instance v5, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v8, " \t\r\n>"

    invoke-direct {v5, p1, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->read()I

    move-result v1

    goto :goto_3

    :cond_b
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    invoke-static {v8, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$200(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->reset()V

    neg-int v8, v1

    invoke-static {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_c
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->read()I

    move-result v1

    goto :goto_3

    :cond_d
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    invoke-static {v8, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$200(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->getEndCharacter()I

    move-result v1

    if-eq v1, v10, :cond_e

    if-eqz v4, :cond_e

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->attributes:Ljava/util/HashMap;

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_f
    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->getEndCharacter()I

    move-result v1

    const/16 v8, 0x3e

    if-ne v1, v8, :cond_3

    if-eqz v4, :cond_10

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_10
    move v6, v7

    goto/16 :goto_1
.end method
