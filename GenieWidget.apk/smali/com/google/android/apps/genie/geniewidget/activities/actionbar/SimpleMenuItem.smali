.class public Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;
.super Ljava/lang/Object;
.source "SimpleMenuItem.java"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private mEnabled:Z

.field private mIconDrawable:Landroid/graphics/drawable/Drawable;

.field private mIconResId:I

.field private final mId:I

.field private mMenu:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;

.field private final mOrder:I

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleCondensed:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;IILjava/lang/CharSequence;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconResId:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mEnabled:Z

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mMenu:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mId:I

    iput p3, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mOrder:I

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public collapseActionView()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public expandActionView()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getGroupId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconResId:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mMenu:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mId:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getOrder()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mOrder:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public hasSubMenu()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCheckable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mEnabled:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/view/ActionProvider;

    return-object p0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 0
    .param p1    # I

    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/view/View;

    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .param p1    # C

    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mEnabled:Z

    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconResId:I

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconResId:I

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/content/Intent;

    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .param p1    # C

    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/view/MenuItem$OnActionExpandListener;

    return-object p0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0
    .param p1    # C
    .param p2    # C

    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mMenu:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    return-object p0
.end method
