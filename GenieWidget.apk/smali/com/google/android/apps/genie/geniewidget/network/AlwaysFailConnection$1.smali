.class Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;
.super Ljava/util/TimerTask;
.source "AlwaysFailConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->execute(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

.field final synthetic val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

.field final synthetic val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    # invokes: Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->generateFakeData()Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->access$000(Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->this$0:Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    # getter for: Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->fail:Z
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;->access$100(Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;-><init>(Ljava/util/List;ZJ)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->success(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Dummy network: always fails."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->failure(Ljava/lang/Exception;)V

    goto :goto_0
.end method
