.class Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$3;
.super Ljava/lang/Object;
.source "ViewMediator.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->showTabView(Landroid/widget/FrameLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$3;->this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$3;->this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getIndex()I

    move-result v2

    # invokes: Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->buildPage(I)V
    invoke-static {v1, v2}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->access$000(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;I)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$3;->this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    # getter for: Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->access$200(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->showPage(I)V

    return-void
.end method
