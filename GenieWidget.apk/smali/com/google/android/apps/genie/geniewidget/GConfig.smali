.class public Lcom/google/android/apps/genie/geniewidget/GConfig;
.super Ljava/lang/Object;
.source "GConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/GConfig$Server;,
        Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;
    }
.end annotation


# static fields
.field public static final BACKEND:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

.field public static MASF_SERVICE:Ljava/lang/String;

.field public static MASF_URL:Ljava/lang/String;

.field public static NETWORK:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

.field public static NEWS_TOPICS_URL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->REAL:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->NETWORK:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$Server;->PROD:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->BACKEND:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->BACKEND:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$Server;->PROD:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    if-ne v0, v1, :cond_1

    const-string v0, "http://www.google.com/m/appreq"

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_URL:Ljava/lang/String;

    const-string v0, "g:gne"

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_SERVICE:Ljava/lang/String;

    const-string v0, "http://www.google.com/m/gne/newsTopics?hl="

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->NEWS_TOPICS_URL:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->BACKEND:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$Server;->DEV:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    if-ne v0, v1, :cond_2

    const-string v0, "http://jmt24.google.com/dev/r?sky=tchma"

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_URL:Ljava/lang/String;

    const-string v0, "g:gne-dev"

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_SERVICE:Ljava/lang/String;

    const-string v0, "http://jmt24.google.com/gne/newsTopics?hl="

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->NEWS_TOPICS_URL:Ljava/lang/String;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->BACKEND:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$Server;->ENG:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    if-ne v0, v1, :cond_0

    const-string v0, "http://mattlewis.lon.corp.google.com:6000"

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_URL:Ljava/lang/String;

    const-string v0, "g:gne-dev"

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_SERVICE:Ljava/lang/String;

    const-string v0, "http://jmt24.google.com/gne/newsTopics?hl="

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->NEWS_TOPICS_URL:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
