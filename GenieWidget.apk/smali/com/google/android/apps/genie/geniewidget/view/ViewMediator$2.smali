.class Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$2;
.super Ljava/lang/Object;
.source "ViewMediator.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->initViewsContainer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$2;->this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageShown(Lcom/google/android/apps/genie/geniewidget/ui/PageView;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getPageNumber()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$2;->this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getTabView()Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getTabCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getTab(I)Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$2;->this$0:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    # getter for: Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->access$100(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->getCurrentView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->setSelectedTab(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    :cond_0
    return-void
.end method
