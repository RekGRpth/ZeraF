.class public Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;
.super Landroid/content/ContentProvider;
.source "WeatherProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CURRENT_WEATHER_URI:Landroid/net/Uri;

.field private static final DAILY_FIELDS_FAHRENHEIT:Ljava/lang/String;

.field public static final DAILY_WEATHER_URI:Landroid/net/Uri;

.field private static final HOURLY_FIELDS_FAHRENHEIT:Ljava/lang/String;

.field public static final HOURLY_WEATHER_URI:Landroid/net/Uri;

.field private static log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

.field private static uriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private dbOpener:Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->getLogger()Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    const-string v0, "content://com.google.android.apps.genie.geniewidget.weather/weather"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "current"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->CURRENT_WEATHER_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "daily"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_WEATHER_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hourly"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->HOURLY_WEATHER_URI:Landroid/net/Uri;

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/current"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/daily"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/hourly"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/current/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/daily/#"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/hourly/#"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/current/#/*"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/daily/#/*"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.genie.geniewidget.weather"

    const-string v2, "weather/hourly/#/*"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id, fakeLocation, location, timestamp, begins, ends, pointInTime, description, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "temperature"

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->convertToFahrenheit(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "highTemperature"

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->convertToFahrenheit(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "lowTemperature"

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->convertToFahrenheit(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "chancePrecipitation, wind, humidity, sunrise, sunset, iconUrl, iconResId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id, fakeLocation, location, timestamp, begins, ends, description, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "temperature"

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->convertToFahrenheit(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "chancePrecipitation, wind, humidity, iconUrl, iconResId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->HOURLY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/apps/genie/geniewidget/utils/Logger;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    return-object v0
.end method

.method private static convertToFahrenheit(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "round(1.8 * "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " + 32) as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static deleteStale(Lcom/google/android/apps/genie/geniewidget/GenieContext;Ljava/lang/String;J)V
    .locals 4
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_WEATHER_URI:Landroid/net/Uri;

    const-string v3, "location = ? OR ends < ?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->HOURLY_WEATHER_URI:Landroid/net/Uri;

    const-string v3, "location = ? OR ends < ?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private getUpdated(Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 5
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "SELECT MAX(timestamp) FROM tblDailyWeather"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static updateContent(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;J)V
    .locals 10
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;
    .param p3    # J

    const/4 v2, 0x0

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/util/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v6

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/util/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v7

    new-array v9, v2, [Landroid/content/ContentValues;

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v4

    const-wide/32 v2, 0x240c8400

    sub-long v2, p3, v2

    invoke-static {p0, v5, v2, v3}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->deleteStale(Lcom/google/android/apps/genie/geniewidget/GenieContext;Ljava/lang/String;J)V

    invoke-virtual {p2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v1

    move-object v0, p0

    move-wide v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherAdapter;->forecastToContentValues(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;JLjava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getForecasts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-object v0, p0

    move-wide v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherAdapter;->forecastToContentValues(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;JLjava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/content/ContentValues;

    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_WEATHER_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v9}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getHourlyData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-object v0, p0

    move-wide v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherAdapter;->forecastToContentValues(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;JLjava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/content/ContentValues;

    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->HOURLY_WEATHER_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v9}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->dbOpener:Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t delete uri "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v1, "tblDailyWeather"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    :goto_0
    return v1

    :pswitch_1
    const-string v1, "tblHourlyWeather"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->dbOpener:Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "fakeLocation"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "fakeLocation"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to insert uri "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v1, "tblDailyWeather"

    const-string v2, ""

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->CONTENT_URI:Landroid/net/Uri;

    return-object v1

    :pswitch_1
    const-string v1, "tblHourlyWeather"

    const-string v2, ""

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 2

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;-><init>(Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->dbOpener:Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 18
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection not allowed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-eqz p4, :cond_1

    move-object/from16 v0, p4

    array-length v1, v0

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection args not allowed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No timestamp in uri "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v1

    const v2, 0x7f0c0009

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/utils/LocaleUtils;->prefersCelsius(Ljava/util/Locale;)Z

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getBooleanPreference(IZ)Z

    move-result v12

    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v1

    const v2, 0x7f0c0003

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getBooleanPreference(IZ)Z

    move-result v16

    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v1

    const v2, 0x7f0c0004

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->dbOpener:Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->getUpdated(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v1

    invoke-static {v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    sub-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-gez v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZ)V

    :cond_3
    sget-object v1, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->uriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t query for URI "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v14, "tblDailyWeather"

    if-eqz v16, :cond_4

    const-string v17, "fakeLocation IS NULL AND begins <= ? AND ends >= ? ORDER BY timestamp DESC LIMIT 1"

    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v15, v7, v1

    const/4 v1, 0x1

    aput-object v15, v7, v1

    :goto_0
    if-eqz v12, :cond_5

    const-string v10, "*"

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :cond_4
    const-string v17, "fakeLocation = ? AND begins <= ? AND ends >= ? ORDER BY timestamp DESC LIMIT 1"

    const/4 v1, 0x3

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v9, v7, v1

    const/4 v1, 0x1

    aput-object v15, v7, v1

    const/4 v1, 0x2

    aput-object v15, v7, v1

    goto :goto_0

    :cond_5
    sget-object v10, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    goto :goto_1

    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_6

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No fake location"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    const-string v14, "tblDailyWeather"

    const-string v17, "fakeLocation = ? AND begins <= ? AND ends >= ? ORDER BY timestamp DESC LIMIT 1"

    const/4 v1, 0x3

    new-array v7, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v7, v2

    const/4 v1, 0x1

    aput-object v15, v7, v1

    const/4 v1, 0x2

    aput-object v15, v7, v1

    if-eqz v12, :cond_7

    const-string v10, "*"

    goto :goto_1

    :cond_7
    sget-object v10, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    const-string v14, "tblDailyWeather"

    const-string v17, "fakeLocation IS NULL AND ends >= ?"

    const/4 v1, 0x1

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v15, v7, v1

    if-eqz v12, :cond_8

    const-string v10, "*"

    goto/16 :goto_1

    :cond_8
    sget-object v10, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_9

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No fake location"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    const-string v14, "tblDailyWeather"

    const-string v17, "fakeLocation = ? AND ends >= ?"

    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v7, v2

    const/4 v1, 0x1

    aput-object v15, v7, v1

    if-eqz v12, :cond_a

    const-string v10, "*"

    goto/16 :goto_1

    :cond_a
    sget-object v10, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->DAILY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_4
    const-string v14, "tblHourlyWeather"

    const-string v17, "fakeLocation IS NULL AND ends >= ?"

    const/4 v1, 0x1

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v15, v7, v1

    if-eqz v12, :cond_b

    const-string v10, "*"

    goto/16 :goto_1

    :cond_b
    sget-object v10, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->HOURLY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_c

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No fake location"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_c
    const-string v14, "tblHourlyWeather"

    const-string v17, "fakeLocation = ? AND ends >= ?"

    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v7, v2

    const/4 v1, 0x1

    aput-object v15, v7, v1

    if-eqz v12, :cond_d

    const-string v10, "*"

    goto/16 :goto_1

    :cond_d
    sget-object v10, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->HOURLY_FIELDS_FAHRENHEIT:Ljava/lang/String;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
