.class Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;
.super Ljava/lang/Thread;
.source "CachedUrlFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->fetchAsync(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

.field final synthetic val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->this$0:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->this$0:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->val$url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->fetch(Ljava/lang/String;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    invoke-interface {v2, v1}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->success(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    invoke-interface {v2, v0}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->failure(Ljava/lang/Exception;)V

    goto :goto_0
.end method
