.class Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;
.super Ljava/lang/Object;
.source "FixedCapacityByteQueue.java"


# instance fields
.field private buffer:[B

.field private head:I

.field private size:I

.field private tail:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->tail:I

    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    return-void
.end method


# virtual methods
.method public capacity()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v0, v0

    return v0
.end method

.method public clear()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->tail:I

    return-void
.end method

.method public get(I)B
    .locals 2
    .param p1    # I

    if-ltz p1, :cond_0

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    if-lt p1, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    :cond_1
    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    add-int v0, v1, p1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v1, v1

    if-lt v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    aget-byte v1, v1, v0

    return v1
.end method

.method public popFront()B
    .locals 3

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    aget-byte v0, v1, v2

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v2, v2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    :cond_1
    return v0
.end method

.method public pushBack(B)V
    .locals 2
    .param p1    # B

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v1, v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->tail:I

    aput-byte p1, v0, v1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->tail:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->tail:I

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->tail:I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v1, v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->tail:I

    :cond_1
    return-void
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    return v0
.end method

.method public toByteArray([BII)V
    .locals 5
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->head:I

    const/4 v0, 0x0

    move v2, v1

    move v3, p2

    :goto_0
    if-ge v0, p3, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v4, v4

    if-ge v0, v4, :cond_1

    add-int/lit8 p2, v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    add-int/lit8 v1, v2, 0x1

    aget-byte v4, v4, v2

    aput-byte v4, p1, v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->buffer:[B

    array-length v4, v4

    if-ne v1, v4, :cond_0

    const/4 v1, 0x0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v3, p2

    goto :goto_0

    :cond_1
    return-void
.end method

.method public toByteArray()[B
    .locals 3

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    new-array v0, v1, [B

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->toByteArray([BII)V

    return-object v0
.end method
