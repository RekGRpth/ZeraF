.class public Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;
.super Ljava/lang/Object;
.source "CssParser.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/persistance/ContentParser;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$1;,
        Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;,
        Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;,
        Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;
    }
.end annotation


# instance fields
.field private charsetName:Ljava/lang/String;

.field private resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    return-void
.end method

.method private appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p1    # Ljava/io/ByteArrayOutputStream;
    .param p2    # Ljava/lang/StringBuilder;
    .param p3    # I

    if-lez p3, :cond_0

    invoke-virtual {p1, p3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->reset()V

    neg-int v0, p3

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/io/ByteArrayOutputStream;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v2, "ISO-8859-1"

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, ""

    goto :goto_0
.end method

.method private expectToken(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;Ljava/lang/String;)Z
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    move v3, v2

    :goto_0
    const/4 v5, -0x1

    if-eq v0, v5, :cond_1

    array-length v5, v1

    if-ge v3, v5, :cond_1

    add-int/lit8 v2, v3, 0x1

    aget-byte v5, v1, v3

    if-eq v0, v5, :cond_0

    :goto_1
    return v4

    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    move v3, v2

    goto :goto_0

    :cond_1
    const/16 v5, 0x20

    if-eq v0, v5, :cond_2

    const/16 v5, 0x9

    if-eq v0, v5, :cond_2

    const/16 v5, 0xd

    if-eq v0, v5, :cond_2

    const/16 v5, 0xa

    if-eq v0, v5, :cond_2

    const/16 v5, 0x2f

    if-ne v0, v5, :cond_3

    :cond_2
    const/4 v4, 0x1

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private parseCharset(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v5, 0x2f

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$persistance$CssParser$ParsingCharsetState:[I

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    goto :goto_0

    :pswitch_0
    sparse-switch v0, :sswitch_data_0

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->CHARSET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :sswitch_0
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_1
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {p1, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto :goto_1

    :sswitch_2
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;->CHARSET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCharsetState;

    goto :goto_1

    :pswitch_1
    sparse-switch v0, :sswitch_data_1

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :sswitch_3
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    :cond_1
    :goto_2
    return-void

    :sswitch_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    goto :goto_2

    :sswitch_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {p1, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x22 -> :sswitch_2
        0x27 -> :sswitch_2
        0x2f -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x22 -> :sswitch_3
        0x27 -> :sswitch_3
        0x2f -> :sswitch_5
        0x3b -> :sswitch_4
    .end sparse-switch
.end method

.method private parseUrl(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0x2a

    const/16 v8, 0x2f

    const/4 v11, 0x1

    const/16 v10, 0x22

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->BEFORE_URL:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v6, -0x1

    if-eq v0, v6, :cond_1

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$persistance$CssParser$ParsingUrlState:[I

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    goto :goto_0

    :pswitch_0
    sparse-switch v0, :sswitch_data_0

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_1

    :sswitch_0
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_1
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_1

    :sswitch_2
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_1

    :sswitch_3
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v1, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_1

    :sswitch_4
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_1

    :pswitch_1
    sparse-switch v0, :sswitch_data_1

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    :goto_2
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto :goto_1

    :sswitch_5
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    :cond_1
    :goto_3
    return-void

    :sswitch_6
    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_2

    :pswitch_2
    sparse-switch v0, :sswitch_data_2

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    :goto_4
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_7
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto :goto_3

    :sswitch_8
    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_4

    :pswitch_3
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    sparse-switch v0, :sswitch_data_3

    if-lez v4, :cond_2

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_e

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    :goto_5
    const/4 v6, 0x6

    if-ne v4, v6, :cond_0

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_f

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    :goto_6
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v6

    const/16 v7, 0x20

    if-ne v6, v7, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_9
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v3, v3, 0x4

    add-int/lit8 v6, v0, -0x30

    add-int/2addr v3, v6

    goto :goto_5

    :sswitch_a
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v3, v3, 0x4

    add-int/lit8 v6, v0, 0xa

    add-int/lit8 v6, v6, -0x41

    add-int/2addr v3, v6

    goto :goto_5

    :sswitch_b
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v3, v3, 0x4

    add-int/lit8 v6, v0, 0xa

    add-int/lit8 v6, v6, -0x61

    add-int/2addr v3, v6

    goto :goto_5

    :sswitch_c
    if-lez v4, :cond_3

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_4

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    :goto_8
    goto :goto_5

    :cond_3
    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_4
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_8

    :sswitch_d
    if-nez v4, :cond_6

    const-string v6, "\""

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_5

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    :goto_9
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto :goto_5

    :cond_5
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_9

    :cond_6
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_7

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto/16 :goto_3

    :cond_7
    const-string v6, "\""

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_8

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    :goto_a
    goto :goto_9

    :cond_8
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_a

    :sswitch_e
    if-nez v4, :cond_a

    const-string v6, "\'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_9

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    :goto_b
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto/16 :goto_5

    :cond_9
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_b

    :cond_a
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_b

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto/16 :goto_3

    :cond_b
    const-string v6, "\'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    if-ne v5, v6, :cond_c

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    :goto_c
    goto :goto_b

    :cond_c
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto :goto_c

    :sswitch_f
    if-lez v4, :cond_d

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto/16 :goto_5

    :cond_e
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_5

    :cond_f
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_6

    :pswitch_4
    sparse-switch v0, :sswitch_data_4

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    goto/16 :goto_1

    :sswitch_10
    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto/16 :goto_3

    :sswitch_11
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    if-eq v0, v9, :cond_10

    if-ne v0, v8, :cond_11

    :cond_10
    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    goto/16 :goto_3

    :cond_11
    invoke-virtual {v1, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto/16 :goto_1

    :pswitch_5
    sparse-switch v0, :sswitch_data_5

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_12
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_13
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    if-eq v0, v9, :cond_12

    if-ne v0, v8, :cond_13

    :cond_12
    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    goto/16 :goto_3

    :cond_13
    invoke-virtual {v1, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :pswitch_6
    sparse-switch v0, :sswitch_data_6

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_14
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_15
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    if-eq v0, v9, :cond_14

    if-ne v0, v8, :cond_15

    :cond_14
    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    goto/16 :goto_3

    :cond_15
    invoke-virtual {v1, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :pswitch_7
    sparse-switch v0, :sswitch_data_7

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_16
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    goto/16 :goto_1

    :sswitch_17
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    if-eq v0, v9, :cond_16

    if-ne v0, v8, :cond_17

    :cond_16
    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    goto/16 :goto_3

    :cond_17
    invoke-virtual {v1, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_18
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :pswitch_8
    sparse-switch v0, :sswitch_data_8

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_19
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_1a
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :sswitch_1b
    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    goto/16 :goto_1

    :pswitch_9
    packed-switch v0, :pswitch_data_1

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    goto/16 :goto_1

    :pswitch_a
    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9, v11}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write([B)V

    invoke-interface {p1, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    invoke-interface {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x22 -> :sswitch_2
        0x27 -> :sswitch_1
        0x2f -> :sswitch_3
        0x75 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x27 -> :sswitch_5
        0x5c -> :sswitch_6
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_7
        0x5c -> :sswitch_8
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x20 -> :sswitch_c
        0x22 -> :sswitch_d
        0x27 -> :sswitch_e
        0x30 -> :sswitch_9
        0x31 -> :sswitch_9
        0x32 -> :sswitch_9
        0x33 -> :sswitch_9
        0x34 -> :sswitch_9
        0x35 -> :sswitch_9
        0x36 -> :sswitch_9
        0x37 -> :sswitch_9
        0x38 -> :sswitch_9
        0x39 -> :sswitch_9
        0x41 -> :sswitch_a
        0x42 -> :sswitch_a
        0x43 -> :sswitch_a
        0x44 -> :sswitch_a
        0x45 -> :sswitch_a
        0x46 -> :sswitch_a
        0x5c -> :sswitch_f
        0x61 -> :sswitch_b
        0x62 -> :sswitch_b
        0x63 -> :sswitch_b
        0x64 -> :sswitch_b
        0x65 -> :sswitch_b
        0x66 -> :sswitch_b
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x9 -> :sswitch_10
        0xa -> :sswitch_10
        0xd -> :sswitch_10
        0x20 -> :sswitch_10
        0x2f -> :sswitch_11
        0x3b -> :sswitch_10
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x2f -> :sswitch_13
        0x72 -> :sswitch_12
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0x2f -> :sswitch_15
        0x6c -> :sswitch_14
    .end sparse-switch

    :sswitch_data_7
    .sparse-switch
        0x9 -> :sswitch_16
        0xa -> :sswitch_16
        0xd -> :sswitch_16
        0x20 -> :sswitch_16
        0x28 -> :sswitch_18
        0x2f -> :sswitch_17
    .end sparse-switch

    :sswitch_data_8
    .sparse-switch
        0x9 -> :sswitch_19
        0xa -> :sswitch_19
        0xd -> :sswitch_19
        0x20 -> :sswitch_19
        0x22 -> :sswitch_1b
        0x27 -> :sswitch_1a
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x29
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public process(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/InputStream;
    .param p4    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;

    invoke-direct {v2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v6

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    :goto_0
    const/4 v0, -0x1

    if-eq v6, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$persistance$CssParser$ParsingCssState:[I

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v6

    goto :goto_0

    :pswitch_0
    sparse-switch v6, :sswitch_data_0

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    :cond_1
    :goto_2
    :sswitch_0
    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_1
    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->AT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    goto :goto_2

    :sswitch_2
    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x2f

    invoke-interface {v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto :goto_2

    :pswitch_1
    packed-switch v6, :pswitch_data_1

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :pswitch_2
    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->url:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->charsetName:Ljava/lang/String;

    const/16 v5, 0x7d

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;-><init>(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->process()V

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    goto :goto_1

    :pswitch_3
    sparse-switch v6, :sswitch_data_1

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_3
    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->IMPORT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    goto :goto_1

    :sswitch_4
    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->CHARSET_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    goto :goto_1

    :pswitch_4
    const-string v0, "import"

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->expectToken(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->parseUrl(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)V

    :cond_2
    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->PIPE_TILL_SEMI:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    goto :goto_1

    :pswitch_5
    const-string v0, "charset"

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->expectToken(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->parseCharset(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)V

    :cond_3
    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->PIPE_TILL_SEMI:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    goto :goto_1

    :pswitch_6
    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    move-result v0

    const/16 v1, 0x3b

    if-ne v0, v1, :cond_0

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    goto :goto_1

    :cond_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x2f -> :sswitch_2
        0x40 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x7b
        :pswitch_2
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_4
        0x69 -> :sswitch_3
    .end sparse-switch
.end method
