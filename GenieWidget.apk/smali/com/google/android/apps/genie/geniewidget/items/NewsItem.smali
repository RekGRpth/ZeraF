.class public Lcom/google/android/apps/genie/geniewidget/items/NewsItem;
.super Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;
.source "NewsItem.java"


# instance fields
.field private final category:Ljava/lang/String;

.field private final guid:Ljava/lang/String;

.field private final linkUrl:Ljava/lang/String;

.field private final snippet:Ljava/lang/String;

.field private final source:Ljava/lang/String;

.field private thumbnailUri:Landroid/net/Uri;

.field private final thumbnailUrl:Ljava/lang/String;

.field private final timestamp:J

.field private final title:Ljava/lang/String;

.field private final topicId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # J
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->NEWS:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;-><init>(Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;)V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->topicId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->category:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->title:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->linkUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->thumbnailUrl:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->source:Ljava/lang/String;

    iput-wide p7, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->timestamp:J

    iput-object p9, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->guid:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->snippet:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCategoryTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->linkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->thumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getThumbnailUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->thumbnailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->timestamp:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->topicId:Ljava/lang/String;

    return-object v0
.end method

.method public setThumbnailUri(Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->thumbnailUri:Landroid/net/Uri;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTopicId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
