.class public Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
.super Ljava/lang/Object;
.source "ViewMediator.java"


# instance fields
.field private final context:Landroid/app/Activity;

.field private newsContentViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;

.field private final newsItemClickListener:Landroid/view/View$OnClickListener;

.field private final newsSectionViewBuilders:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final pagePlaceHolder:Landroid/widget/FrameLayout;

.field private tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

.field private viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

.field private final weatherSettingRequestListener:Landroid/view/View$OnClickListener;

.field private final weatherTabName:Ljava/lang/String;

.field private weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/widget/FrameLayout;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View$OnClickListener;
    .param p3    # Landroid/view/View$OnClickListener;
    .param p4    # Landroid/widget/FrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->context:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsItemClickListener:Landroid/view/View$OnClickListener;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherSettingRequestListener:Landroid/view/View$OnClickListener;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsSectionViewBuilders:Ljava/util/Hashtable;

    const v0, 0x7f0c0030

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherTabName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->pagePlaceHolder:Landroid/widget/FrameLayout;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->buildPage(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    return-object v0
.end method

.method private addResourceView(Landroid/widget/FrameLayout;Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)V
    .locals 2
    .param p1    # Landroid/widget/FrameLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/widget/FrameLayout;",
            "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder",
            "<TT;>;)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getView(Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->buildFailedView()Landroid/view/View;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->removeAllViews()V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->onRequestViewShown(Z)V

    :cond_1
    return-void
.end method

.method private buildPage(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getViewBuilder(I)Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getViewsContainer()Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getPage(I)Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->addResourceView(Landroid/widget/FrameLayout;Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)V

    return-void
.end method

.method private buildRelationship(Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getQuickAccessor()Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;->setRelative(Ljava/lang/Object;)V

    return-void
.end method

.method private getNewsSectionViewBuilder(Ljava/lang/String;I)Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsSectionViewBuilders:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->context:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsItemClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;-><init>(Landroid/app/Activity;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsSectionViewBuilders:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private getView(Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder",
            "<TT;>;)TT;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->getCurrentView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->buildView()Ljava/lang/Object;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private getViewBuilder(I)Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getTabView()Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getTab(I)Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getQuickAccessor()Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;->getRelative()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getExtra()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getViewBuilder(Ljava/lang/String;I)Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->buildRelationship(Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    :cond_0
    return-object v1
.end method

.method private getViewBuilder(Ljava/lang/String;I)Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherTabName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getWeatherViewBuilder(I)Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getNewsSectionViewBuilder(Ljava/lang/String;I)Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;

    move-result-object v0

    goto :goto_0
.end method

.method private getViewsContainer()Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->initViewsContainer()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    return-object v0
.end method

.method private getWeatherViewBuilder(I)Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->context:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherSettingRequestListener:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;-><init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;I)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    return-object v0
.end method

.method private initViewsContainer()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->context:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04000b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->getCurrentView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getTabCount()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->setTotalPages(I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$1;-><init>(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->setPageProvider(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$2;-><init>(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->setPageEventObserver(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->pagePlaceHolder:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getCurrentTabNumber()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getCurrentPage()I

    move-result v0

    goto :goto_0
.end method

.method public getTabIds()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->getCurrentView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getTabIds()Ljava/util/HashSet;

    move-result-object v1

    goto :goto_0
.end method

.method public getTabView()Lcom/google/android/apps/genie/geniewidget/ui/TabView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getView(Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    return-object v0
.end method

.method public refreshNewsViews()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsSectionViewBuilders:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->isStale()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->invalidate()V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->getPageNumber()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->buildPage(I)V

    goto :goto_0
.end method

.method public refreshWeatherView()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    const-wide/16 v4, -0x1

    iput-wide v4, v3, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->curViewTimestamp:J

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->getCurrentView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getTabCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->getViewBuilder(I)Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;

    move-result-object v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getPage(I)Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->removeAllViews()V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->weatherViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->addResourceView(Landroid/widget/FrameLayout;Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setCurrentTabNumber(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->viewsContainer:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->showPage(I)V

    return-void
.end method

.method public declared-synchronized showNewsView(Landroid/widget/FrameLayout;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/widget/FrameLayout;
    .param p2    # Landroid/view/View;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsContentViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->context:Landroid/app/Activity;

    invoke-direct {v0, v1, p2}, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsContentViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsContentViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;->requestNews(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->newsContentViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/NewsContentViewBuilder;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->addResourceView(Landroid/widget/FrameLayout;Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public showTabView(Landroid/widget/FrameLayout;)V
    .locals 3
    .param p1    # Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->context:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator$3;-><init>(Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;-><init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->tabViewBuilder:Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->addResourceView(Landroid/widget/FrameLayout;Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;)V

    return-void
.end method
