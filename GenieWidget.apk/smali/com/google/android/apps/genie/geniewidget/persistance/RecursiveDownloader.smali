.class public Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;
.super Ljava/lang/Object;
.source "RecursiveDownloader.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;
    }
.end annotation


# instance fields
.field abort:Z

.field absolutePath:Z

.field private converter:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;

.field crossSite:Z

.field private downloaded:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private downloader:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;

.field limit:I

.field private queue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;",
            ">;"
        }
    .end annotation
.end field

.field private serial:I

.field private tempDir:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->serial:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->abort:Z

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloader:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->converter:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;

    return-void
.end method

.method private static getFinalPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 14
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/4 v1, 0x0

    const/4 v7, 0x0

    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v8, Ljava/net/URL;

    move-object/from16 v0, p2

    invoke-direct {v8, v2, v0}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_3

    if-nez p4, :cond_0

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v7, v8

    move-object v1, v2

    :goto_0
    return-object v9

    :catch_0
    move-exception v3

    :goto_1
    move-object/from16 v9, p2

    goto :goto_0

    :cond_0
    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->limit:I

    if-gtz v9, :cond_1

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v7, v8

    move-object v1, v2

    goto :goto_0

    :cond_1
    iget-boolean v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->crossSite:Z

    if-nez v9, :cond_4

    const/16 v9, 0x3a

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    if-lez v9, :cond_4

    const/16 v9, 0x3f

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_2

    const/16 v9, 0x3a

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/16 v10, 0x3f

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    if-ge v9, v10, :cond_4

    :cond_2
    iget-boolean v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->absolutePath:Z

    if-eqz v9, :cond_3

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    :cond_3
    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v7, v8

    move-object v1, v2

    goto :goto_0

    :cond_4
    iget-boolean v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->absolutePath:Z

    if-nez v9, :cond_5

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_5

    const/4 v9, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x2f

    if-ne v9, v10, :cond_5

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v7, v8

    move-object v1, v2

    goto :goto_0

    :cond_5
    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloaded:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloaded:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    move-object v7, v8

    move-object v1, v2

    goto/16 :goto_0

    :cond_6
    const/4 v5, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "res"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->serial:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->serial:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :try_start_2
    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloader:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->tempDir:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, p1, v11, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v5

    iget-object v9, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->encoding:Ljava/lang/String;

    if-nez v9, :cond_7

    move-object/from16 v0, p3

    iput-object v0, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->encoding:Ljava/lang/String;

    :cond_7
    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->queue:Ljava/util/LinkedList;

    new-instance v10, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;

    iget-object v11, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->url:Ljava/lang/String;

    iget-object v12, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->path:Ljava/lang/String;

    iget-object v13, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->encoding:Ljava/lang/String;

    invoke-direct {v10, p0, v11, v12, v13}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    new-instance v9, Ljava/io/File;

    iget-object v10, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->path:Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloaded:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->url:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloaded:Ljava/util/HashMap;

    iget-object v10, v5, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->url:Ljava/lang/String;

    invoke-virtual {v9, v10, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->limit:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->limit:I

    move-object v7, v8

    move-object v1, v2

    move-object v9, v4

    goto/16 :goto_0

    :catch_1
    move-exception v3

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v7, v8

    move-object v1, v2

    goto/16 :goto_0

    :catch_2
    move-exception v3

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v7, v8

    move-object v1, v2

    goto/16 :goto_0

    :catch_3
    move-exception v3

    move-object v1, v2

    goto/16 :goto_1
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z
    .param p6    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/URISyntaxException;
        }
    .end annotation

    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->serial:I

    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->limit:I

    move/from16 v0, p6

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->crossSite:Z

    move/from16 v0, p5

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->absolutePath:Z

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->queue:Ljava/util/LinkedList;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloaded:Ljava/util/HashMap;

    new-instance v1, Ljava/io/File;

    const-string v2, "_temp"

    invoke-direct {v1, p2, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->tempDir:Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->tempDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->downloader:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->tempDir:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-interface {v1, p1, v2, v3, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;

    move-result-object v11

    iget-object v1, v11, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->encoding:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "ISO-8859-1"

    iput-object v1, v11, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->encoding:Ljava/lang/String;

    :cond_0
    iget-object v1, v11, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->path:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->getFinalPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->converter:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;

    iget-object v2, v11, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->url:Ljava/lang/String;

    iget-object v3, v11, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->path:Ljava/lang/String;

    iget-object v5, v11, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->encoding:Ljava/lang/String;

    move-object v6, p0

    invoke-interface/range {v1 .. v6}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;->convert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->queue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->abort:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->queue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->converter:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;

    invoke-virtual {v12}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->getFinalPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->getEncoding()Ljava/lang/String;

    move-result-object v9

    move-object v10, p0

    invoke-interface/range {v5 .. v10}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;->convert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->tempDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    return-object v4
.end method
