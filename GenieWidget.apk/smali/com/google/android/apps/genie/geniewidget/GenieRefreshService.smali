.class public Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;
.super Landroid/app/Service;
.source "GenieRefreshService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;
    }
.end annotation


# instance fields
.field private ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;

.field private wifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Landroid/content/Intent;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;
    .param p1    # Landroid/content/Intent;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startRefreshThread(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/net/wifi/WifiManager$WifiLock;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    return-object v0
.end method

.method private static buildIntent(Landroid/content/Context;ZZZZZZILjava/lang/String;I)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "requestNews"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "requestWeather"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "forceLocationFix"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "ignoreMASFcache"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "isScheduled"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "retryNumber"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "shouldUpdate"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "refreshThreadHash"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p8, :cond_0

    const-string v1, "fakeLocation"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method private parseIntent(Landroid/content/Intent;)Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;-><init>()V

    const-string v1, "requestNews"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->addTypeFilter(I)V

    :cond_0
    const-string v1, "requestWeather"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->addTypeFilter(I)V

    :cond_1
    const-string v1, "fakeLocation"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "fakeLocation"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->setFakeLocation(Ljava/lang/String;)V

    :cond_2
    return-object v0
.end method

.method private releaseAllLocks()V
    .locals 1

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private startRefreshThread(Landroid/content/Intent;Z)V
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # Z

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->parseIntent(Landroid/content/Intent;)Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    move-result-object v2

    const-string v0, "forceLocationFix"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v0, "ignoreMASFcache"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v0, "isScheduled"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    const-string v0, "retryNumber"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const-string v0, "com.google.android.apps.genie.PREFETCH_ACTION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->unregisterConnectivityIntent()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;ZZZZZI)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->start()V

    return-void
.end method

.method public static startService(Landroid/content/Context;ZZZZZ)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v7, v6

    move v9, v6

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZZILjava/lang/String;I)V

    return-void
.end method

.method public static startService(Landroid/content/Context;ZZZZZZ)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p6

    move v6, p5

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZZILjava/lang/String;I)V

    return-void
.end method

.method public static startService(Landroid/content/Context;ZZZZZZILjava/lang/String;I)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .param p8    # Ljava/lang/String;
    .param p9    # I

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz p6, :cond_1

    const v3, 0x7f0c0005

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static/range {p0 .. p9}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->buildIntent(Landroid/content/Context;ZZZZZZILjava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static triggerPrefetch(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.genie.PREFETCH_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static triggerPurge(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.genie.PURGE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetController()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "wifi"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    const-string v2, "Genie"

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "Genie"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->releaseAllLocks()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->stopLocationMonitor()V

    return-void
.end method

.method public declared-synchronized onStart(Landroid/content/Intent;I)V
    .locals 8
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string v6, "com.google.android.apps.genie.PURGE_ACTION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isComplete()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isPrefetching()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->cancelPrefetch()V

    :cond_2
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$1;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)V

    new-instance v5, Ljava/lang/Thread;

    invoke-direct {v5, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :cond_3
    :try_start_2
    const-string v6, "shouldUpdate"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v6, "refreshThreadHash"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isComplete()Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isWaitingForWifi()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->cancelRequest()V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->shouldUpdateScheduledRefresh()Z

    move-result v6

    or-int/2addr v2, v6

    :cond_4
    :goto_1
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startRefreshThread(Landroid/content/Intent;Z)V

    goto :goto_0

    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isPrefetching()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->cancelPrefetch()V

    goto :goto_1

    :cond_6
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v6

    if-ne v1, v6, :cond_0

    move-object v3, p1

    move v4, v2

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;

    invoke-direct {v6, p0, v3, v4}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Landroid/content/Intent;Z)V

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->start()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
