.class public Lmaps/p/s;
.super Landroid/view/TextureView;

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field private static final a:Lmaps/p/at;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;

.field private c:Lmaps/p/ao;

.field private d:Lmaps/af/v;

.field private e:Z

.field private f:Lmaps/p/e;

.field private g:Lmaps/p/ab;

.field private h:Lmaps/p/v;

.field private i:Lmaps/p/as;

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/p/at;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/p/at;-><init>(Lmaps/p/h;)V

    sput-object v0, Lmaps/p/s;->a:Lmaps/p/at;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/p/s;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lmaps/p/s;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/p/s;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lmaps/p/s;->a()V

    return-void
.end method

.method static synthetic a(Lmaps/p/s;)I
    .locals 1

    iget v0, p0, Lmaps/p/s;->k:I

    return v0
.end method

.method private a()V
    .locals 0

    invoke-virtual {p0, p0}, Lmaps/p/s;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method static synthetic b(Lmaps/p/s;)Lmaps/p/e;
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->f:Lmaps/p/e;

    return-object v0
.end method

.method static synthetic c(Lmaps/p/s;)Lmaps/p/ab;
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->g:Lmaps/p/ab;

    return-object v0
.end method

.method static synthetic d(Lmaps/p/s;)Lmaps/p/v;
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->h:Lmaps/p/v;

    return-object v0
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic e(Lmaps/p/s;)Lmaps/p/as;
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->i:Lmaps/p/as;

    return-object v0
.end method

.method static synthetic f(Lmaps/p/s;)I
    .locals 1

    iget v0, p0, Lmaps/p/s;->j:I

    return v0
.end method

.method static synthetic g(Lmaps/p/s;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/p/s;->l:Z

    return v0
.end method

.method static synthetic h(Lmaps/p/s;)Lmaps/af/v;
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->d:Lmaps/af/v;

    return-object v0
.end method

.method static synthetic k()Lmaps/p/at;
    .locals 1

    sget-object v0, Lmaps/p/s;->a:Lmaps/p/at;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    invoke-direct {p0}, Lmaps/p/s;->d()V

    iput p1, p0, Lmaps/p/s;->k:I

    return-void
.end method

.method public a(Lmaps/af/v;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/p/s;->d()V

    iget-object v0, p0, Lmaps/p/s;->f:Lmaps/p/e;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/p/i;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lmaps/p/i;-><init>(Lmaps/p/s;Z)V

    iput-object v0, p0, Lmaps/p/s;->f:Lmaps/p/e;

    :cond_0
    iget-object v0, p0, Lmaps/p/s;->g:Lmaps/p/ab;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/p/ah;

    invoke-direct {v0, p0, v2}, Lmaps/p/ah;-><init>(Lmaps/p/s;Lmaps/p/h;)V

    iput-object v0, p0, Lmaps/p/s;->g:Lmaps/p/ab;

    :cond_1
    iget-object v0, p0, Lmaps/p/s;->h:Lmaps/p/v;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/p/an;

    invoke-direct {v0, v2}, Lmaps/p/an;-><init>(Lmaps/p/h;)V

    iput-object v0, p0, Lmaps/p/s;->h:Lmaps/p/v;

    :cond_2
    iput-object p1, p0, Lmaps/p/s;->d:Lmaps/af/v;

    new-instance v0, Lmaps/p/ao;

    iget-object v1, p0, Lmaps/p/s;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Lmaps/p/ao;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->start()V

    return-void
.end method

.method public a(Lmaps/p/as;)V
    .locals 0

    iput-object p1, p0, Lmaps/p/s;->i:Lmaps/p/as;

    return-void
.end method

.method public a(Lmaps/p/e;)V
    .locals 0

    invoke-direct {p0}, Lmaps/p/s;->d()V

    iput-object p1, p0, Lmaps/p/s;->f:Lmaps/p/e;

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->f()V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0, p1}, Lmaps/p/ao;->a(I)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/p/s;->l:Z

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->g()V

    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public h(Z)V
    .locals 1

    iput-boolean p1, p0, Lmaps/p/s;->m:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lmaps/p/s;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->h()V

    :cond_0
    return-void
.end method

.method public j_()V
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->c()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lmaps/p/s;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/p/s;->d:Lmaps/af/v;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->b()I

    move-result v0

    :goto_0
    new-instance v2, Lmaps/p/ao;

    iget-object v3, p0, Lmaps/p/s;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lmaps/p/ao;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v1, v0}, Lmaps/p/ao;->a(I)V

    :cond_1
    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/s;->e:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lmaps/p/s;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->h()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/s;->e:Z

    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->d()V

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0, p2, p3}, Lmaps/p/ao;->a(II)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0}, Lmaps/p/ao;->e()V

    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lmaps/p/s;->c:Lmaps/p/ao;

    invoke-virtual {v0, p2, p3}, Lmaps/p/ao;->a(II)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method
