.class public Lmaps/p/au;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/af/f;


# static fields
.field static final a:I


# instance fields
.field final b:Lmaps/p/am;

.field protected c:Lmaps/p/ai;

.field private d:I

.field private e:J

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:J

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lmaps/bm/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    :goto_0
    sput v0, Lmaps/p/au;->a:I

    return-void

    :cond_0
    const/16 v0, 0x14

    goto :goto_0
.end method

.method public constructor <init>(Lmaps/p/am;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/au;->n:Z

    sget v0, Lmaps/p/au;->a:I

    invoke-virtual {p0, v0}, Lmaps/p/au;->c(I)V

    iput-object p1, p0, Lmaps/p/au;->b:Lmaps/p/am;

    return-void
.end method

.method private j()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lmaps/p/au;->k:I

    iget v1, p0, Lmaps/p/au;->j:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lmaps/p/au;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3e6b8520

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    iget v0, p0, Lmaps/p/au;->f:I

    int-to-float v0, v0

    const v1, 0x3f8ccccd

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lmaps/p/au;->c(I)V

    :cond_0
    :goto_0
    iput v2, p0, Lmaps/p/au;->l:I

    iput v2, p0, Lmaps/p/au;->j:I

    iput v2, p0, Lmaps/p/au;->k:I

    return-void

    :cond_1
    const v1, 0x3ebd70a4

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lmaps/p/au;->f:I

    int-to-float v0, v0

    const v1, 0x3f666666

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lmaps/p/au;->c(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lmaps/p/au;->d:I

    invoke-virtual {p0}, Lmaps/p/au;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/p/au;->e:J

    return-void
.end method

.method public a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0, p1, p2}, Lmaps/p/ai;->a(J)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/p/au;->g:Z

    return-void
.end method

.method public a(ZZ)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lmaps/p/au;->c(Z)V

    :cond_0
    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_1

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0}, Lmaps/p/ai;->e()V

    :cond_1
    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0}, Lmaps/p/ai;->f()V

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)Z
    .locals 2

    iget v0, p0, Lmaps/p/au;->d:I

    add-int/2addr v0, p1

    iget v1, p0, Lmaps/p/au;->d:I

    if-eqz v1, :cond_0

    const v1, 0x88b8

    if-gt v0, v1, :cond_1

    :cond_0
    iput v0, p0, Lmaps/p/au;->d:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 10

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/p/au;->d()J

    move-result-wide v2

    iget-wide v4, p0, Lmaps/p/au;->e:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit8 v3, v0, 0x5

    iget-boolean v0, p0, Lmaps/p/au;->g:Z

    if-eqz v0, :cond_4

    sget v0, Lmaps/p/au;->a:I

    :goto_0
    iget-boolean v2, p0, Lmaps/p/au;->h:Z

    if-eqz v2, :cond_0

    add-int/lit16 v0, v0, 0x1f4

    :cond_0
    const/16 v2, 0xf

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0}, Lmaps/p/ai;->d()I

    move-result v0

    iget-boolean v1, p0, Lmaps/p/au;->i:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lmaps/p/au;->g:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v1}, Lmaps/p/ai;->f()V

    move v1, v2

    :goto_1
    iget-object v2, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {p0, v1}, Lmaps/p/au;->d(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lmaps/p/ai;->a(I)V

    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-boolean v2, Lmaps/ae/h;->A:Z

    if-eqz v2, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lmaps/p/au;->m:J

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-eqz v2, :cond_1

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v2

    iget-wide v6, p0, Lmaps/p/au;->m:J

    sub-long v6, v4, v6

    long-to-int v6, v6

    add-int/2addr v0, v3

    iget-boolean v7, p0, Lmaps/p/au;->g:Z

    invoke-virtual {v2, v6, v0, v7}, Lmaps/p/f;->a(IIZ)V

    :cond_1
    iput-wide v4, p0, Lmaps/p/au;->m:J

    :cond_2
    iget-boolean v0, p0, Lmaps/p/au;->h:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lmaps/p/au;->g:Z

    if-nez v0, :cond_3

    iget v0, p0, Lmaps/p/au;->j:I

    add-int/2addr v0, v3

    iput v0, p0, Lmaps/p/au;->j:I

    iget v0, p0, Lmaps/p/au;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/p/au;->k:I

    iget v0, p0, Lmaps/p/au;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/p/au;->l:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lmaps/p/au;->j()V

    :cond_3
    return-void

    :cond_4
    iget v0, p0, Lmaps/p/au;->f:I

    goto :goto_0

    :cond_5
    :try_start_1
    iget-object v1, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v1}, Lmaps/p/ai;->g()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v1, v4, v6

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v1}, Lmaps/p/ai;->g()J

    move-result-wide v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v1, v4

    long-to-int v2, v1

    move v1, v2

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v1}, Lmaps/p/ai;->e()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/p/au;->i:Z

    move v1, v2

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move v1, v2

    goto :goto_1

    :cond_8
    move v0, v1

    move v1, v2

    goto :goto_2
.end method

.method public b(I)V
    .locals 1

    iget v0, p0, Lmaps/p/au;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lmaps/p/au;->d:I

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/p/au;->h:Z

    return-void
.end method

.method public declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/au;->i:Z

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0}, Lmaps/p/ai;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c(I)V
    .locals 1

    sget v0, Lmaps/p/au;->a:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lmaps/p/au;->f:I

    return-void
.end method

.method public declared-synchronized c(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/p/au;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d(I)I
    .locals 0

    return p1
.end method

.method d()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0}, Lmaps/p/ai;->b()V

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/p/au;->b:Lmaps/p/am;

    invoke-interface {v0}, Lmaps/p/am;->j_()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lmaps/p/ai;

    invoke-direct {v0, p0}, Lmaps/p/ai;-><init>(Lmaps/p/au;)V

    iput-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0}, Lmaps/p/ai;->start()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    invoke-virtual {v0}, Lmaps/p/ai;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized h()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/p/au;->n:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/p/au;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/p/au;->c:Lmaps/p/ai;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lmaps/p/ai;->a(Lmaps/p/ai;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
