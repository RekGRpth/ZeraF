.class public Lmaps/p/f;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmaps/p/f;


# instance fields
.field private b:I

.field private c:Ljava/util/List;

.field private d:I

.field private e:Lmaps/p/x;

.field private f:Lmaps/p/x;

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:Lmaps/p/a;

.field private l:I

.field private m:I

.field private n:Ljava/lang/Object;

.field private o:J

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/p/f;

    invoke-direct {v0}, Lmaps/p/f;-><init>()V

    sput-object v0, Lmaps/p/f;->a:Lmaps/p/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/16 v3, 0x708

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/p/f;->b:I

    const/4 v1, 0x1

    iput v1, p0, Lmaps/p/f;->d:I

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/p/f;->k:Lmaps/p/a;

    iput v0, p0, Lmaps/p/f;->l:I

    iput v0, p0, Lmaps/p/f;->m:I

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lmaps/p/f;->n:Ljava/lang/Object;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lmaps/p/f;->o:J

    iput-boolean v0, p0, Lmaps/p/f;->p:Z

    invoke-static {v3}, Lmaps/f/fd;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lmaps/p/f;->c:Ljava/util/List;

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lmaps/p/f;->c:Ljava/util/List;

    new-instance v2, Lmaps/p/x;

    invoke-direct {v2}, Lmaps/p/x;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/p/f;->c:Ljava/util/List;

    iget v1, p0, Lmaps/p/f;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/x;

    iput-object v0, p0, Lmaps/p/f;->f:Lmaps/p/x;

    iget-object v0, p0, Lmaps/p/f;->c:Ljava/util/List;

    iget v1, p0, Lmaps/p/f;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/x;

    iput-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    return-void
.end method

.method static synthetic a(Lmaps/p/f;)J
    .locals 2

    iget-wide v0, p0, Lmaps/p/f;->o:J

    return-wide v0
.end method

.method public static a()Lmaps/p/f;
    .locals 1

    sget-object v0, Lmaps/p/f;->a:Lmaps/p/f;

    return-object v0
.end method

.method static synthetic b(Lmaps/p/f;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/p/f;->p:Z

    return v0
.end method

.method static synthetic c(Lmaps/p/f;)Lmaps/p/x;
    .locals 1

    iget-object v0, p0, Lmaps/p/f;->f:Lmaps/p/x;

    return-object v0
.end method


# virtual methods
.method public a(IIZ)V
    .locals 2

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    iput p1, v0, Lmaps/p/x;->i:I

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    iput p2, v0, Lmaps/p/x;->j:I

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    iput-boolean p3, v0, Lmaps/p/x;->k:Z

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    invoke-virtual {v0}, Lmaps/p/x;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/p/f;->o:J

    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    const-wide/32 v1, 0xf4240

    div-long v1, p1, v1

    long-to-int v1, v1

    iput v1, v0, Lmaps/p/x;->d:I

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    iput-object v0, p0, Lmaps/p/f;->f:Lmaps/p/x;

    iget-object v1, p0, Lmaps/p/f;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/p/f;->f:Lmaps/p/x;

    iget v2, p0, Lmaps/p/f;->l:I

    iput v2, v0, Lmaps/p/x;->g:I

    iget-object v0, p0, Lmaps/p/f;->f:Lmaps/p/x;

    iget v2, p0, Lmaps/p/f;->m:I

    iput v2, v0, Lmaps/p/x;->h:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/p/f;->l:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/p/f;->m:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lmaps/l/al;I)V
    .locals 2

    iget-object v1, p0, Lmaps/p/f;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/p/f;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/p/f;->l:I

    iget v0, p0, Lmaps/p/f;->m:I

    add-int/2addr v0, p2

    iput v0, p0, Lmaps/p/f;->m:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lmaps/p/f;->c:Ljava/util/List;

    iget v1, p0, Lmaps/p/f;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/x;

    iput-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    invoke-virtual {v0}, Lmaps/p/x;->a()V

    iget v0, p0, Lmaps/p/f;->d:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit16 v0, v0, 0x708

    iput v0, p0, Lmaps/p/f;->d:I

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    iget v1, p0, Lmaps/p/f;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/p/f;->b:I

    iput v1, v0, Lmaps/p/x;->a:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/p/f;->g:J

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/p/f;->h:J

    return-void
.end method

.method public c()V
    .locals 5

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lmaps/p/f;->g:J

    sub-long/2addr v1, v3

    long-to-int v1, v1

    iput v1, v0, Lmaps/p/x;->b:I

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lmaps/p/f;->h:J

    sub-long/2addr v1, v3

    long-to-int v1, v1

    iput v1, v0, Lmaps/p/x;->c:I

    return-void
.end method

.method public d()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/p/f;->i:J

    return-void
.end method

.method public e()V
    .locals 6

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    iget v1, v0, Lmaps/p/x;->e:I

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lmaps/p/f;->i:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Lmaps/p/x;->e:I

    return-void
.end method

.method public f()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/p/f;->j:J

    return-void
.end method

.method public g()V
    .locals 5

    iget-object v0, p0, Lmaps/p/f;->e:Lmaps/p/x;

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lmaps/p/f;->j:J

    sub-long/2addr v1, v3

    long-to-int v1, v1

    iput v1, v0, Lmaps/p/x;->f:I

    return-void
.end method

.method public h()Lmaps/p/a;
    .locals 1

    iget-object v0, p0, Lmaps/p/f;->k:Lmaps/p/a;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/p/a;

    invoke-direct {v0, p0}, Lmaps/p/a;-><init>(Lmaps/p/f;)V

    iput-object v0, p0, Lmaps/p/f;->k:Lmaps/p/a;

    :cond_0
    iget-object v0, p0, Lmaps/p/f;->k:Lmaps/p/a;

    return-object v0
.end method
