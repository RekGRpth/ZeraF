.class public Lmaps/p/a;
.super Lmaps/u/c;


# instance fields
.field final synthetic a:Lmaps/p/f;

.field private b:[I

.field private c:[I

.field private d:I

.field private e:Ljava/nio/FloatBuffer;

.field private f:[F


# direct methods
.method public constructor <init>(Lmaps/p/f;)V
    .locals 2

    const/4 v0, 0x0

    iput-object p1, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-direct {p0}, Lmaps/u/c;-><init>()V

    iput-object v0, p0, Lmaps/p/a;->b:[I

    iput-object v0, p0, Lmaps/p/a;->c:[I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/p/a;->d:I

    const/16 v0, 0x118

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/p/a;->e:Ljava/nio/FloatBuffer;

    return-void
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;)V
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/16 v2, 0x1701

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p2}, Lmaps/bq/d;->l()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Lmaps/bq/d;->m()I

    move-result v3

    int-to-float v4, v3

    const/high16 v5, -0x40800000

    const/high16 v6, 0x3f800000

    move v3, v1

    invoke-interface/range {v0 .. v6}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    return-void
.end method

.method private d(Lmaps/cr/c;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1701

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/p/a;->b:[I

    iput-object v0, p0, Lmaps/p/a;->c:[I

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 8

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lmaps/cr/c;->D()V

    invoke-direct {p0, p1, p2}, Lmaps/p/a;->a(Lmaps/cr/c;Lmaps/bq/d;)V

    invoke-interface {v7}, Ljavax/microedition/khronos/opengles/GL11;->glPushMatrix()V

    sget-boolean v0, Lmaps/ae/h;->B:Z

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v2}, Lmaps/p/f;->a(Lmaps/p/f;)J

    move-result-wide v2

    const-wide/16 v4, 0xc8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lmaps/p/a;->c:[I

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lmaps/bq/d;->l()I

    move-result v0

    invoke-virtual {p2}, Lmaps/bq/d;->m()I

    move-result v1

    div-int/lit8 v2, v0, 0xc

    const/16 v3, 0x24

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x3

    int-to-float v5, v2

    aput v5, v3, v4

    const/4 v4, 0x4

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x5

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x6

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x7

    int-to-float v5, v1

    aput v5, v3, v4

    const/16 v4, 0x8

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x9

    int-to-float v5, v2

    aput v5, v3, v4

    const/16 v4, 0xa

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0xb

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0xc

    int-to-float v5, v2

    aput v5, v3, v4

    const/16 v4, 0xd

    int-to-float v5, v1

    aput v5, v3, v4

    const/16 v4, 0xe

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0xf

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x10

    int-to-float v5, v1

    aput v5, v3, v4

    const/16 v4, 0x11

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x12

    sub-int v5, v0, v2

    int-to-float v5, v5

    aput v5, v3, v4

    const/16 v4, 0x13

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x14

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x15

    int-to-float v5, v0

    aput v5, v3, v4

    const/16 v4, 0x16

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x17

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x18

    sub-int v5, v0, v2

    int-to-float v5, v5

    aput v5, v3, v4

    const/16 v4, 0x19

    int-to-float v5, v1

    aput v5, v3, v4

    const/16 v4, 0x1a

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x1b

    int-to-float v5, v0

    aput v5, v3, v4

    const/16 v4, 0x1c

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x1d

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x1e

    int-to-float v5, v0

    aput v5, v3, v4

    const/16 v4, 0x1f

    int-to-float v5, v1

    aput v5, v3, v4

    const/16 v4, 0x20

    const/4 v5, 0x0

    aput v5, v3, v4

    const/16 v4, 0x21

    sub-int/2addr v0, v2

    int-to-float v0, v0

    aput v0, v3, v4

    const/16 v0, 0x22

    int-to-float v1, v1

    aput v1, v3, v0

    const/16 v0, 0x23

    const/4 v1, 0x0

    aput v1, v3, v0

    const/16 v0, 0x90

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lmaps/p/a;->c:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/p/a;->c:[I

    const/4 v3, 0x0

    invoke-interface {v7, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    const v1, 0x8892

    iget-object v2, p0, Lmaps/p/a;->c:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-interface {v7, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    const v1, 0x8892

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    const v3, 0x88e4

    invoke-interface {v7, v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTranslatef(FFF)V

    const v0, 0x8892

    iget-object v1, p0, Lmaps/p/a;->c:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    invoke-virtual {p1}, Lmaps/cr/c;->t()V

    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-interface {v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBlendFunc(II)V

    const v0, 0x3f2e147b

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    const v3, 0x3f4ccccd

    invoke-interface {v7, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_2

    const/4 v4, 0x0

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/16 v3, 0x1406

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    :goto_0
    const/4 v0, 0x4

    const/4 v1, 0x0

    const/16 v2, 0xc

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_1

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/s/a;->b(I)V

    :cond_1
    iget-object v0, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v0}, Lmaps/p/f;->b(Lmaps/p/f;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/p/a;->b:[I

    if-nez v0, :cond_5

    const v0, 0x18ad8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    const/16 v0, 0x46

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lmaps/p/a;->f:[F

    const/4 v0, 0x0

    :goto_1
    const/16 v2, 0x168

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lmaps/p/a;->f:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x3

    const/16 v1, 0x1406

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-interface {v7, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x46

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/p/a;->b:[I

    const/4 v0, 0x1

    iget-object v2, p0, Lmaps/p/a;->b:[I

    const/4 v3, 0x0

    invoke-interface {v7, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    const v0, 0x8892

    iget-object v2, p0, Lmaps/p/a;->b:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-interface {v7, v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    const v0, 0x8892

    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    const v3, 0x88e4

    invoke-interface {v7, v0, v2, v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    :goto_2
    invoke-virtual {p2}, Lmaps/bq/d;->l()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    const/high16 v1, 0x43340000

    sub-float/2addr v0, v1

    invoke-virtual {p2}, Lmaps/bq/d;->m()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40400000

    div-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTranslatef(FFF)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p2}, Lmaps/bq/d;->m()I

    move-result v1

    div-int/lit8 v1, v1, 0x64

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/cr/c;->x()V

    invoke-virtual {p1}, Lmaps/cr/c;->t()V

    invoke-virtual {p1}, Lmaps/cr/c;->p()V

    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-interface {v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBlendFunc(II)V

    iget v0, p0, Lmaps/p/a;->d:I

    rsub-int v0, v0, 0x168

    int-to-float v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTranslatef(FFF)V

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_6

    const/4 v4, 0x0

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/16 v3, 0x1406

    const/16 v5, 0x1c

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x2

    const/4 v2, 0x4

    const/16 v3, 0x1406

    const/16 v5, 0x1c

    const/16 v6, 0xc

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    :goto_3
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lmaps/p/a;->d:I

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v2, v2, 0xa

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    iget v0, p0, Lmaps/p/a;->d:I

    rsub-int v0, v0, 0x168

    neg-int v0, v0

    iget v1, p0, Lmaps/p/a;->d:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTranslatef(FFF)V

    const/4 v0, 0x1

    iget v1, p0, Lmaps/p/a;->d:I

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0xa

    iget v2, p0, Lmaps/p/a;->d:I

    rsub-int v2, v2, 0x168

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0xa

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    iget v0, p0, Lmaps/p/a;->d:I

    int-to-float v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTranslatef(FFF)V

    const/high16 v0, 0x40400000

    invoke-interface {v7, v0}, Ljavax/microedition/khronos/opengles/GL11;->glLineWidth(F)V

    const/4 v0, 0x1

    const/16 v1, 0xe10

    const/16 v2, 0xa

    invoke-interface {v7, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    const/high16 v0, 0x3f800000

    invoke-interface {v7, v0}, Ljavax/microedition/khronos/opengles/GL11;->glLineWidth(F)V

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_4

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/s/a;->b(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/s/a;->b(I)V

    :cond_4
    const v0, 0x8892

    const/4 v1, 0x0

    invoke-interface {v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    invoke-direct {p0, p1}, Lmaps/p/a;->d(Lmaps/cr/c;)V

    invoke-virtual {p1}, Lmaps/cr/c;->F()V

    invoke-interface {v7}, Ljavax/microedition/khronos/opengles/GL11;->glPopMatrix()V

    iget v0, p0, Lmaps/p/a;->d:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, Lmaps/p/a;->d:I

    return-void

    :cond_5
    const v0, 0x8892

    iget-object v1, p0, Lmaps/p/a;->b:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/4 v1, 0x0

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/4 v1, 0x7

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x8

    iget-object v2, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v2}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v2

    iget v2, v2, Lmaps/p/x;->e:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0xe

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0xf

    iget-object v2, p0, Lmaps/p/a;->f:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x15

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x16

    iget-object v2, p0, Lmaps/p/a;->f:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    iget-object v3, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v3}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v3

    iget v3, v3, Lmaps/p/x;->f:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x1c

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x1d

    iget-object v2, p0, Lmaps/p/a;->f:[F

    const/16 v3, 0x16

    aget v2, v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x23

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x24

    iget-object v2, p0, Lmaps/p/a;->f:[F

    const/16 v3, 0x16

    aget v2, v2, v3

    iget-object v3, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v3}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v3

    iget v3, v3, Lmaps/p/x;->b:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v3}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v3

    iget v3, v3, Lmaps/p/x;->e:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v3}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v3

    iget v3, v3, Lmaps/p/x;->f:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x2a

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x2b

    iget-object v2, p0, Lmaps/p/a;->f:[F

    const/16 v3, 0x24

    aget v2, v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x31

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x32

    iget-object v2, p0, Lmaps/p/a;->f:[F

    const/16 v3, 0x24

    aget v2, v2, v3

    iget-object v3, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v3}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v3

    iget v3, v3, Lmaps/p/x;->d:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x38

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x3f

    iget v2, p0, Lmaps/p/a;->d:I

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->f:[F

    const/16 v1, 0x40

    iget-object v2, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v2}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v2

    iget v2, v2, Lmaps/p/x;->b:I

    iget-object v3, p0, Lmaps/p/a;->a:Lmaps/p/f;

    invoke-static {v3}, Lmaps/p/f;->c(Lmaps/p/f;)Lmaps/p/x;

    move-result-object v3

    iget v3, v3, Lmaps/p/x;->c:I

    sub-int/2addr v2, v3

    neg-int v2, v2

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/p/a;->e:Ljava/nio/FloatBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/p/a;->e:Ljava/nio/FloatBuffer;

    iget-object v1, p0, Lmaps/p/a;->f:[F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    iget-object v0, p0, Lmaps/p/a;->e:Ljava/nio/FloatBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const v0, 0x8892

    iget v1, p0, Lmaps/p/a;->d:I

    mul-int/lit8 v1, v1, 0x46

    add-int/lit8 v1, v1, 0x0

    mul-int/lit8 v1, v1, 0x4

    iget-object v2, p0, Lmaps/p/a;->e:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    iget-object v3, p0, Lmaps/p/a;->e:Ljava/nio/FloatBuffer;

    invoke-interface {v7, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x3

    const/16 v1, 0x1406

    const/16 v2, 0x1c

    const/4 v3, 0x0

    invoke-interface {v7, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    const/4 v0, 0x4

    const/16 v1, 0x1406

    const/16 v2, 0x1c

    const/16 v3, 0xc

    invoke-interface {v7, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glColorPointer(IIII)V

    goto/16 :goto_3

    :array_0
    .array-data 4
        -0x40800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x3f800000
        0x0
        0x3f800000
        0x3f000000
        -0x40800000
        0x0
        0x0
        0x3f800000
        0x0
        0x3f800000
        0x3f000000
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x43b40000
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x0
        0x41800000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x43b40000
        0x41800000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x0
        0x42000000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x43b40000
        0x42000000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x0
        -0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x43b40000
        -0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x0
        -0x3f000000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
        0x43b40000
        -0x3f000000
        0x0
        0x0
        0x0
        0x0
        0x3e800000
    .end array-data
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const v1, 0x8892

    iget-object v2, p0, Lmaps/p/a;->b:[I

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/p/a;->b:[I

    return-void
.end method
