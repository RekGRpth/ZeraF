.class Lmaps/p/q;
.super Ljava/lang/Thread;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Ljava/util/ArrayList;

.field private q:Z

.field private r:Lmaps/p/al;

.field private s:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/p/q;->p:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lmaps/p/q;->q:Z

    iput v2, p0, Lmaps/p/q;->k:I

    iput v2, p0, Lmaps/p/q;->l:I

    iput-boolean v1, p0, Lmaps/p/q;->n:Z

    iput v1, p0, Lmaps/p/q;->m:I

    iput-object p1, p0, Lmaps/p/q;->s:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic a(Lmaps/p/q;)Ljava/lang/ref/WeakReference;
    .locals 1

    iget-object v0, p0, Lmaps/p/q;->s:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic a(Lmaps/p/q;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmaps/p/q;->b:Z

    return p1
.end method

.method private k()V
    .locals 1

    iget-boolean v0, p0, Lmaps/p/q;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/q;->i:Z

    iget-object v0, p0, Lmaps/p/q;->r:Lmaps/p/al;

    invoke-virtual {v0}, Lmaps/p/al;->e()V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 1

    iget-boolean v0, p0, Lmaps/p/q;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/q;->r:Lmaps/p/al;

    invoke-virtual {v0}, Lmaps/p/al;->f()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/q;->h:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/p/aa;->c(Lmaps/p/q;)V

    :cond_0
    return-void
.end method

.method private m()V
    .locals 20

    new-instance v2, Lmaps/p/al;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/p/q;->s:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lmaps/p/al;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/p/q;->r:Lmaps/p/al;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->h:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->i:Z

    const/4 v4, 0x0

    const/4 v13, 0x0

    const/4 v2, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    move-object v15, v4

    move v4, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v2

    move/from16 v19, v3

    move-object v3, v5

    move v5, v7

    move/from16 v7, v19

    :goto_0
    :try_start_0
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v16

    monitor-enter v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->a:Z

    if-eqz v2, :cond_0

    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v3

    monitor-enter v3

    :try_start_2
    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->k()V

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->l()V

    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_0
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->p:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    move v3, v7

    move v7, v5

    move-object v5, v2

    move v2, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v4

    :goto_2
    monitor-exit v16
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v5, :cond_11

    :try_start_4
    invoke-interface {v5}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const/4 v5, 0x0

    move v4, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v2

    move/from16 v19, v3

    move-object v3, v5

    move v5, v7

    move/from16 v7, v19

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lmaps/p/q;->d:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lmaps/p/q;->c:Z

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v14, v0, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->c:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lmaps/p/q;->c:Z

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lmaps/p/q;->d:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    move v14, v2

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->j:Z

    if-eqz v2, :cond_2

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->k()V

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->l()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->j:Z

    const/4 v6, 0x1

    :cond_2
    if-eqz v10, :cond_3

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->k()V

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->l()V

    const/4 v10, 0x0

    :cond_3
    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->i:Z

    if-eqz v2, :cond_4

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->k()V

    :cond_4
    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->h:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/p/w;

    if-nez v2, :cond_d

    const/4 v2, 0x0

    :goto_4
    if-eqz v2, :cond_5

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/p/aa;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->l()V

    :cond_6
    if-eqz v14, :cond_7

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/p/aa;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->r:Lmaps/p/al;

    invoke-virtual {v2}, Lmaps/p/al;->f()V

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->e:Z

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->g:Z

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->i:Z

    if-eqz v2, :cond_8

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->k()V

    :cond_8
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->g:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->f:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->e:Z

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->g:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->g:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :cond_a
    if-eqz v7, :cond_b

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->o:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :cond_b
    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->n()Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->h:Z

    if-nez v2, :cond_c

    if-eqz v6, :cond_e

    const/4 v6, 0x0

    :cond_c
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->h:Z

    if-eqz v2, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/p/q;->i:Z

    if-nez v2, :cond_1c

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->i:Z

    const/4 v12, 0x1

    const/4 v11, 0x1

    const/4 v9, 0x1

    move v2, v9

    move v9, v11

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lmaps/p/q;->i:Z

    if-eqz v11, :cond_f

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lmaps/p/q;->q:Z

    if-eqz v11, :cond_1b

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Lmaps/p/q;->k:I

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/p/q;->l:I

    const/4 v5, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lmaps/p/q;->q:Z

    :goto_7
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lmaps/p/q;->n:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->notifyAll()V

    move v12, v9

    move v9, v5

    move-object v5, v3

    move v3, v7

    move v7, v4

    move/from16 v19, v2

    move v2, v11

    move v11, v10

    move v10, v8

    move v8, v6

    move/from16 v6, v19

    goto/16 :goto_2

    :cond_d
    invoke-static {v2}, Lmaps/p/w;->g(Lmaps/p/w;)Z

    move-result v2

    goto/16 :goto_4

    :cond_e
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lmaps/p/aa;->b(Lmaps/p/q;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v2

    if-eqz v2, :cond_c

    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->r:Lmaps/p/al;

    invoke-virtual {v2}, Lmaps/p/al;->a()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    const/4 v2, 0x1

    :try_start_7
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->h:Z

    const/4 v13, 0x1

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    goto :goto_5

    :catchall_1
    move-exception v2

    monitor-exit v16
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception v2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v3

    monitor-enter v3

    :try_start_9
    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->k()V

    invoke-direct/range {p0 .. p0}, Lmaps/p/q;->l()V

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    throw v2

    :catch_0
    move-exception v2

    :try_start_a
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lmaps/p/aa;->c(Lmaps/p/q;)V

    throw v2

    :cond_f
    move v11, v9

    move v9, v2

    :cond_10
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    :cond_11
    if-eqz v2, :cond_1a

    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/p/q;->r:Lmaps/p/al;

    invoke-virtual {v4}, Lmaps/p/al;->b()Z

    move-result v4

    if-nez v4, :cond_12

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v4

    monitor-enter v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    const/4 v14, 0x1

    :try_start_c
    move-object/from16 v0, p0

    iput-boolean v14, v0, Lmaps/p/q;->f:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v4

    move v4, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v2

    move/from16 v19, v3

    move-object v3, v5

    move v5, v7

    move/from16 v7, v19

    goto/16 :goto_0

    :catchall_3
    move-exception v2

    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v2

    :cond_12
    const/4 v2, 0x0

    move v4, v2

    :goto_8
    if-eqz v12, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->r:Lmaps/p/al;

    invoke-virtual {v2}, Lmaps/p/al;->c()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    check-cast v2, Ljavax/microedition/khronos/opengles/GL10;

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v12

    invoke-virtual {v12, v2}, Lmaps/p/aa;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v12, 0x0

    move-object v14, v2

    :goto_9
    if-eqz v13, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/p/w;

    if-eqz v2, :cond_13

    invoke-static {v2}, Lmaps/p/w;->h(Lmaps/p/w;)Lmaps/p/b;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/p/q;->r:Lmaps/p/al;

    iget-object v13, v13, Lmaps/p/al;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v2, v14, v13}, Lmaps/p/b;->a(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    :cond_13
    const/4 v13, 0x0

    :cond_14
    if-eqz v10, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/p/w;

    if-eqz v2, :cond_15

    invoke-static {v2}, Lmaps/p/w;->h(Lmaps/p/w;)Lmaps/p/b;

    move-result-object v2

    invoke-interface {v2, v14, v7, v6}, Lmaps/p/b;->a(Ljavax/microedition/khronos/opengles/GL10;II)V

    :cond_15
    const/4 v10, 0x0

    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/p/w;

    if-eqz v2, :cond_17

    invoke-static {v2}, Lmaps/p/w;->h(Lmaps/p/w;)Lmaps/p/b;

    move-result-object v2

    invoke-interface {v2, v14}, Lmaps/p/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_17
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/p/q;->r:Lmaps/p/al;

    invoke-virtual {v2}, Lmaps/p/al;->d()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    const-string v17, "GLThread"

    const-string v18, "eglSwapBuffers"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v2}, Lmaps/p/al;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v17

    monitor-enter v17
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    const/4 v2, 0x1

    :try_start_e
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/p/q;->f:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v17
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :goto_a
    :sswitch_0
    :try_start_f
    sget-boolean v2, Lmaps/ae/h;->A:Z

    if-eqz v2, :cond_18

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v17

    sub-long v15, v17, v15

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Lmaps/p/f;->a(J)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :cond_18
    if-eqz v9, :cond_1e

    const/4 v2, 0x1

    :goto_b
    move-object v3, v5

    move-object v15, v14

    move v5, v7

    move v7, v2

    move/from16 v19, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v4

    move v4, v6

    move/from16 v6, v19

    goto/16 :goto_0

    :sswitch_1
    const/4 v11, 0x1

    goto :goto_a

    :catchall_4
    move-exception v2

    :try_start_10
    monitor-exit v17
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    throw v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    :catchall_5
    move-exception v2

    :try_start_12
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    throw v2

    :cond_19
    move-object v14, v15

    goto/16 :goto_9

    :cond_1a
    move v4, v2

    goto/16 :goto_8

    :cond_1b
    move v11, v12

    move/from16 v19, v5

    move v5, v8

    move v8, v2

    move v2, v4

    move/from16 v4, v19

    goto/16 :goto_7

    :cond_1c
    move v2, v9

    move v9, v11

    goto/16 :goto_6

    :cond_1d
    move v14, v2

    goto/16 :goto_3

    :cond_1e
    move v2, v3

    goto :goto_b

    nop

    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method

.method private n()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lmaps/p/q;->d:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lmaps/p/q;->e:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lmaps/p/q;->f:Z

    if-nez v1, :cond_1

    iget v1, p0, Lmaps/p/q;->k:I

    if-lez v1, :cond_1

    iget v1, p0, Lmaps/p/q;->l:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lmaps/p/q;->n:Z

    if-nez v1, :cond_0

    iget v1, p0, Lmaps/p/q;->m:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lmaps/p/q;->m:I

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(II)V
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lmaps/p/q;->k:I

    iput p2, p0, Lmaps/p/q;->l:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/q;->q:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/q;->n:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/q;->o:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/p/q;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/p/q;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/p/q;->o:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/p/q;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/p/q;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/p/q;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/p/q;->i:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/p/q;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/p/q;->m:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/q;->n:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/q;->e:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/p/q;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/p/q;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public e()V
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/q;->e:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/p/q;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/p/q;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public f()V
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/q;->c:Z

    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/p/u;

    invoke-direct {v0, p0}, Lmaps/p/u;-><init>(Lmaps/p/q;)V

    invoke-virtual {p0, v0}, Lmaps/p/q;->a(Ljava/lang/Runnable;)V

    :cond_0
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/p/q;->b:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lmaps/p/q;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public g()V
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/q;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/q;->n:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/p/q;->o:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/p/q;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/p/q;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/p/q;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public h()V
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/p/q;->a:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/p/q;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public i()Z
    .locals 2

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/p/q;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public j()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/p/q;->j:Z

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method

.method public run()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GLThread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/p/q;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/p/q;->setName(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lmaps/p/q;->m()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/p/aa;->a(Lmaps/p/q;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/p/aa;->a(Lmaps/p/q;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_1
    const-string v1, "MAP"

    invoke-static {v1, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v1

    invoke-virtual {v1, p0}, Lmaps/p/aa;->a(Lmaps/p/q;)V

    throw v0

    :catch_2
    move-exception v0

    :try_start_2
    const-string v1, "MAP"

    invoke-static {v1, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {}, Lmaps/p/w;->r()Lmaps/p/aa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/p/aa;->a(Lmaps/p/q;)V

    goto :goto_0
.end method
