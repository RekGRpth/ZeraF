.class Lmaps/p/t;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/HashMap;

.field private b:[Lmaps/p/t;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(III)I
    .locals 2

    shr-int v0, p1, p3

    and-int/lit8 v0, v0, 0x1

    shr-int v1, p2, p3

    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method

.method public a(Lmaps/o/c;)Lmaps/p/ap;
    .locals 1

    iget-object v0, p0, Lmaps/p/t;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/p/t;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/ap;

    goto :goto_0
.end method

.method public a(I)Lmaps/p/t;
    .locals 1

    iget-object v0, p0, Lmaps/p/t;->b:[Lmaps/p/t;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lmaps/p/t;->b:[Lmaps/p/t;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public a(IIILmaps/o/c;Lmaps/p/ap;)V
    .locals 6

    if-gtz p3, :cond_1

    iget-object v0, p0, Lmaps/p/t;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/p/t;->a:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lmaps/p/t;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p4, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_1
    add-int/lit8 v3, p3, -0x1

    invoke-virtual {p0, p1, p2, v3}, Lmaps/p/t;->a(III)I

    move-result v1

    iget-object v0, p0, Lmaps/p/t;->b:[Lmaps/p/t;

    if-nez v0, :cond_2

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/p/t;

    iput-object v0, p0, Lmaps/p/t;->b:[Lmaps/p/t;

    :cond_2
    iget-object v0, p0, Lmaps/p/t;->b:[Lmaps/p/t;

    aget-object v0, v0, v1

    if-nez v0, :cond_3

    new-instance v0, Lmaps/p/t;

    invoke-direct {v0}, Lmaps/p/t;-><init>()V

    iget-object v2, p0, Lmaps/p/t;->b:[Lmaps/p/t;

    aput-object v0, v2, v1

    :cond_3
    move v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lmaps/p/t;->a(IIILmaps/o/c;Lmaps/p/ap;)V

    goto :goto_0
.end method
