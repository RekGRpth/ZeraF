.class public Lmaps/q/ar;
.super Lmaps/q/aj;

# interfaces
.implements Lmaps/q/bg;


# instance fields
.field private i:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lmaps/q/ag;

    invoke-direct {p0, v0}, Lmaps/q/aj;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/ar;->i:[F

    return-void
.end method

.method public constructor <init>(II)V
    .locals 4

    const/high16 v3, 0x437f0000

    const/4 v0, 0x4

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, Lmaps/q/ar;-><init>(I[F)V

    return-void
.end method

.method public constructor <init>(I[F)V
    .locals 3

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-static {p1}, Lmaps/q/ar;->b(I)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/q/aj;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/ar;->i:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/q/ar;->i:[F

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    invoke-static {p2, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method private static b(I)Ljava/lang/Class;
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid blend mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-class v0, Lmaps/q/o;

    :goto_0
    return-object v0

    :pswitch_1
    const-class v0, Lmaps/q/ag;

    goto :goto_0

    :pswitch_2
    const-class v0, Lmaps/q/q;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(FFFF)V
    .locals 2

    iget-boolean v0, p0, Lmaps/q/ar;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/q/ar;->i:[F

    if-nez v0, :cond_1

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/q/ar;->i:[F

    :cond_1
    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    return-void
.end method

.method public a(I)V
    .locals 4

    const/high16 v3, 0x437f0000

    iget-boolean v0, p0, Lmaps/q/ar;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/q/ar;->i:[F

    if-nez v0, :cond_1

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/q/ar;->i:[F

    :cond_1
    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    const/4 v1, 0x3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    return-void
.end method

.method protected a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Lmaps/q/aj;->a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V

    sget-object v0, Lmaps/q/ap;->b:Lmaps/q/ap;

    invoke-virtual {p1, v0, p4}, Lmaps/q/ba;->b(Lmaps/q/ap;I)Lmaps/q/z;

    move-result-object v0

    check-cast v0, Lmaps/q/au;

    iget-object v1, p0, Lmaps/q/ar;->g:Lmaps/q/bh;

    check-cast v1, Lmaps/q/w;

    iget v1, v1, Lmaps/q/w;->b:I

    const/4 v2, 0x1

    iget-object v0, v0, Lmaps/q/au;->h:Lmaps/q/az;

    iget-object v0, v0, Lmaps/q/az;->a:[F

    invoke-static {v1, v2, v3, v0, v3}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    iget-object v0, p0, Lmaps/q/ar;->i:[F

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lmaps/q/ar;->i:[F

    invoke-static {v0, v1, v3}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    :cond_0
    return-void
.end method
