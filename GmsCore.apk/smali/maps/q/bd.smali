.class public Lmaps/q/bd;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/i;


# static fields
.field private static f:Z


# instance fields
.field a:Ljava/util/List;

.field private b:[Ljava/util/Set;

.field private c:I

.field private d:I

.field private e:Ljava/lang/Object;

.field private g:Ljava/util/Map;

.field private h:Ljava/util/Map;

.field private i:Ljava/util/PriorityQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/q/bd;->f:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Set;

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, Lmaps/q/bd;->b:[Ljava/util/Set;

    iput v3, p0, Lmaps/q/bd;->c:I

    iput v2, p0, Lmaps/q/bd;->d:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/q/bd;->e:Ljava/lang/Object;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-static {}, Lmaps/f/fd;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/bd;->a:Ljava/util/List;

    new-instance v0, Ljava/util/PriorityQueue;

    new-instance v1, Lmaps/q/at;

    invoke-direct {v1, p0}, Lmaps/q/at;-><init>(Lmaps/q/bd;)V

    invoke-direct {v0, v2, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lmaps/q/bd;->i:Ljava/util/PriorityQueue;

    return-void
.end method

.method private a(Lmaps/q/x;I)I
    .locals 2

    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    if-ge v0, p2, :cond_1

    :goto_1
    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return p2

    :cond_0
    move v0, p2

    goto :goto_0

    :cond_1
    move p2, v0

    goto :goto_1
.end method

.method static synthetic a(Lmaps/q/bd;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    return-object v0
.end method

.method private a(Ljava/util/Set;)V
    .locals 5

    invoke-static {p1}, Lmaps/f/fd;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    iget-object v3, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lmaps/q/x;

    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    add-int/lit8 v4, v3, 0x1

    invoke-direct {p0, v0, v4}, Lmaps/q/bd;->a(Lmaps/q/x;I)I

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    iget-object v2, p0, Lmaps/q/bd;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    return-void
.end method

.method static b()V
    .locals 2

    sget-boolean v0, Lmaps/q/bd;->f:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to update live data from outside a Behavior"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/q/bd;->e:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v0, p0, Lmaps/q/bd;->c:I

    iput v0, p0, Lmaps/q/bd;->d:I

    iget v0, p0, Lmaps/q/bd;->c:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x2

    iput v0, p0, Lmaps/q/bd;->c:I

    iget-object v0, p0, Lmaps/q/bd;->b:[Ljava/util/Set;

    iget v3, p0, Lmaps/q/bd;->d:I

    aget-object v0, v0, v3

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lmaps/q/bd;->b:[Ljava/util/Set;

    iget v2, p0, Lmaps/q/bd;->c:I

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lmaps/q/bd;->a(Ljava/util/Set;)V

    iget-object v0, p0, Lmaps/q/bd;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v2, p0, Lmaps/q/bd;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    move v2, v0

    iget-object v0, p0, Lmaps/q/bd;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    iget-object v3, p0, Lmaps/q/bd;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Lmaps/q/x;->a()V

    iget-object v0, p0, Lmaps/q/bd;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v2, :cond_0

    move v2, v0

    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lmaps/q/bd;->f:Z

    :try_start_1
    iget-object v0, p0, Lmaps/q/bd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    invoke-interface {v0, p0}, Lmaps/q/x;->b(Lmaps/q/i;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    sput-boolean v1, Lmaps/q/bd;->f:Z

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    iget-object v2, p0, Lmaps/q/bd;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_3
    sput-boolean v1, Lmaps/q/bd;->f:Z

    iget-object v0, p0, Lmaps/q/bd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/q/bd;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method a(Lmaps/q/x;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/q/bd;->f:Z

    :try_start_0
    invoke-interface {p1, p0}, Lmaps/q/x;->a(Lmaps/q/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sput-boolean v1, Lmaps/q/bd;->f:Z

    return-void

    :catchall_0
    move-exception v0

    sput-boolean v1, Lmaps/q/bd;->f:Z

    throw v0
.end method

.method public a(Lmaps/q/x;Lmaps/q/c;)V
    .locals 5

    invoke-static {p1, p2}, Lmaps/ap/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmaps/q/as;->a:[I

    iget-object v1, p2, Lmaps/q/c;->a:Lmaps/q/t;

    invoke-virtual {v1}, Lmaps/q/t;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unimplemented WakeUpCondition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, p0, Lmaps/q/bd;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/q/bd;->b:[Ljava/util/Set;

    iget v2, p0, Lmaps/q/bd;->d:I

    aget-object v0, v0, v2

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_1
    iget-object v1, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    monitor-enter v1

    :try_start_1
    check-cast p2, Lmaps/q/d;

    invoke-virtual {p2}, Lmaps/q/d;->a()Lmaps/q/x;

    move-result-object v0

    iget-object v2, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :cond_0
    :try_start_2
    iget-object v2, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    const/4 v3, 0x1

    new-array v3, v3, [Lmaps/q/x;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v3}, Lmaps/f/a;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method b(Lmaps/q/x;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/q/bd;->f:Z

    :try_start_0
    iget-object v1, p0, Lmaps/q/bd;->e:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/q/bd;->b:[Ljava/util/Set;

    iget v2, p0, Lmaps/q/bd;->d:I

    aget-object v0, v0, v2

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v1, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    iget-object v3, p0, Lmaps/q/bd;->g:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    sput-boolean v4, Lmaps/q/bd;->f:Z

    throw v0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_0
    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    sput-boolean v4, Lmaps/q/bd;->f:Z

    return-void
.end method
