.class public Lmaps/q/k;
.super Ljava/lang/Object;


# instance fields
.field final a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/q/k;->a:[F

    invoke-virtual {p0}, Lmaps/q/k;->a()Lmaps/q/k;

    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0

    invoke-direct {p0}, Lmaps/q/k;-><init>()V

    invoke-virtual {p0, p1, p2, p3}, Lmaps/q/k;->a(FFF)Lmaps/q/k;

    return-void
.end method

.method public static a([F)Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x10

    if-ge v0, v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget v3, p0, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    rem-int/lit8 v2, v0, 0x4

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/q/k;
    .locals 2

    iget-object v0, p0, Lmaps/q/k;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    return-object p0
.end method

.method public a(F)Lmaps/q/k;
    .locals 2

    iget-object v0, p0, Lmaps/q/k;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p1, p1}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    return-object p0
.end method

.method public a(FFF)Lmaps/q/k;
    .locals 2

    iget-object v0, p0, Lmaps/q/k;->a:[F

    const/16 v1, 0xc

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/q/k;->a:[F

    const/16 v1, 0xd

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/q/k;->a:[F

    const/16 v1, 0xe

    aput p3, v0, v1

    return-object p0
.end method

.method public a(Lmaps/q/bf;F)Lmaps/q/k;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/q/k;->a:[F

    iget-object v2, p1, Lmaps/q/bf;->a:[F

    aget v3, v2, v1

    iget-object v2, p1, Lmaps/q/bf;->a:[F

    const/4 v4, 0x1

    aget v4, v2, v4

    iget-object v2, p1, Lmaps/q/bf;->a:[F

    const/4 v5, 0x2

    aget v5, v2, v5

    move v2, p2

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    return-object p0
.end method

.method public a(Lmaps/q/k;)Lmaps/q/k;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p1, Lmaps/q/k;->a:[F

    iget-object v1, p0, Lmaps/q/k;->a:[F

    const/16 v2, 0x10

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p0
.end method

.method public b(FFF)Lmaps/q/k;
    .locals 2

    iget-object v0, p0, Lmaps/q/k;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    return-object p0
.end method

.method public c(FFF)Lmaps/q/k;
    .locals 2

    iget-object v0, p0, Lmaps/q/k;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/q/k;->a:[F

    invoke-static {v0}, Lmaps/q/k;->a([F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
