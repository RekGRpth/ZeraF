.class public Lmaps/q/ae;
.super Lmaps/q/al;


# instance fields
.field private final d:[Lmaps/q/ai;


# direct methods
.method public constructor <init>(I)V
    .locals 3

    invoke-direct {p0, p1}, Lmaps/q/al;-><init>(I)V

    const/16 v0, 0x16

    new-array v0, v0, [Lmaps/q/ai;

    iput-object v0, p0, Lmaps/q/ae;->d:[Lmaps/q/ai;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/q/ae;->d:[Lmaps/q/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lmaps/q/ae;->d:[Lmaps/q/ai;

    new-instance v2, Lmaps/q/ai;

    invoke-direct {v2, v0, p0}, Lmaps/q/ai;-><init>(ILmaps/q/al;)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method a(Lmaps/q/ad;)V
    .locals 4

    iget-object v0, p0, Lmaps/q/ae;->a:Lmaps/q/h;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/q/ae;->a:Lmaps/q/h;

    invoke-virtual {v0, p1, p0}, Lmaps/q/h;->a(Lmaps/q/ad;Lmaps/q/al;)V

    iget-object v1, p0, Lmaps/q/ae;->d:[Lmaps/q/ai;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lmaps/q/ai;->a(Lmaps/q/ad;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/q/ae;->a:Lmaps/q/h;

    invoke-virtual {v0, p1, p0}, Lmaps/q/h;->b(Lmaps/q/ad;Lmaps/q/al;)V

    goto :goto_0
.end method

.method a(Lmaps/q/ba;)V
    .locals 2

    check-cast p1, Lmaps/q/aa;

    iget-object v0, p0, Lmaps/q/ae;->d:[Lmaps/q/ai;

    iget v1, p1, Lmaps/q/aa;->b:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lmaps/q/ai;->a(Lmaps/q/aa;)V

    return-void
.end method

.method b(Lmaps/q/ad;)V
    .locals 4

    iget-object v1, p0, Lmaps/q/ae;->d:[Lmaps/q/ai;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lmaps/q/ai;->b(Lmaps/q/ad;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method b(Lmaps/q/ba;)V
    .locals 2

    check-cast p1, Lmaps/q/aa;

    iget-object v0, p0, Lmaps/q/ae;->d:[Lmaps/q/ai;

    iget v1, p1, Lmaps/q/aa;->b:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lmaps/q/ai;->b(Lmaps/q/aa;)V

    return-void
.end method
