.class public Lmaps/q/ad;
.super Ljava/lang/Object;


# static fields
.field private static final j:Ljava/util/List;

.field private static final k:Ljava/util/List;

.field private static n:Ljava/lang/String;


# instance fields
.field a:I

.field b:I

.field private c:[Lmaps/q/z;

.field private d:Lmaps/q/a;

.field private e:Lmaps/q/bd;

.field private f:[Lmaps/q/al;

.field private g:Ljava/util/Set;

.field private h:Ljava/util/Set;

.field private final i:Lmaps/q/y;

.field private l:Lmaps/q/an;

.field private m:I

.field private o:Lmaps/q/b;

.field private p:Lmaps/q/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lmaps/q/ad;->j:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lmaps/q/ad;->k:Ljava/util/List;

    const/4 v0, 0x0

    sput-object v0, Lmaps/q/ad;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/q/al;

    const/4 v1, 0x0

    new-instance v2, Lmaps/q/bi;

    invoke-direct {v2, v4}, Lmaps/q/bi;-><init>(I)V

    aput-object v2, v0, v1

    new-instance v1, Lmaps/q/bi;

    invoke-direct {v1, v3}, Lmaps/q/bi;-><init>(I)V

    aput-object v1, v0, v3

    new-instance v1, Lmaps/q/bi;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lmaps/q/bi;-><init>(I)V

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, Lmaps/q/ad;-><init>([Lmaps/q/al;)V

    return-void
.end method

.method public constructor <init>([Lmaps/q/al;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lmaps/q/z;->a:I

    new-array v0, v0, [Lmaps/q/z;

    iput-object v0, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    new-instance v0, Lmaps/q/a;

    invoke-direct {v0}, Lmaps/q/a;-><init>()V

    iput-object v0, p0, Lmaps/q/ad;->d:Lmaps/q/a;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/q/al;

    iput-object v0, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/q/ad;->g:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/q/ad;->h:Ljava/util/Set;

    new-instance v0, Lmaps/q/y;

    invoke-direct {v0, p0}, Lmaps/q/y;-><init>(Lmaps/q/ad;)V

    iput-object v0, p0, Lmaps/q/ad;->i:Lmaps/q/y;

    iput v1, p0, Lmaps/q/ad;->a:I

    iput-object v2, p0, Lmaps/q/ad;->l:Lmaps/q/an;

    iput v1, p0, Lmaps/q/ad;->m:I

    iput-object v2, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    iput-object v2, p0, Lmaps/q/ad;->p:Lmaps/q/r;

    new-instance v0, Lmaps/q/bd;

    invoke-direct {v0}, Lmaps/q/bd;-><init>()V

    iput-object v0, p0, Lmaps/q/ad;->e:Lmaps/q/bd;

    iget-object v2, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    if-eqz v4, :cond_0

    iget-object v4, v4, Lmaps/q/al;->a:Lmaps/q/h;

    sget-object v5, Lmaps/q/f;->b:Lmaps/q/f;

    invoke-virtual {v4, p0, v5}, Lmaps/q/h;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lmaps/q/ad;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/z;

    if-eqz v0, :cond_3

    sget-object v3, Lmaps/q/f;->b:Lmaps/q/f;

    invoke-virtual {v0, p0, v3}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_4
    sget-object v0, Lmaps/q/ad;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/r;

    if-eqz v0, :cond_6

    sget-object v3, Lmaps/q/f;->b:Lmaps/q/f;

    invoke-virtual {v0, p0, v3}, Lmaps/q/r;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    goto :goto_2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_7
    array-length v2, p1

    move v0, v1

    :goto_3
    if-ge v0, v2, :cond_8

    aget-object v1, p1, v0

    iget-object v3, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    iget v4, v1, Lmaps/q/al;->b:I

    aput-object v1, v3, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": glError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Ljava/lang/Thread;->dumpStack()V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/System;->exit(I)V

    new-instance v1, Lmaps/q/e;

    invoke-direct {v1, v0}, Lmaps/q/e;-><init>(I)V

    throw v1

    :cond_1
    return-void
.end method

.method public static a(Lmaps/q/r;)V
    .locals 2

    sget-object v0, Lmaps/q/ad;->k:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    invoke-interface {v0}, Lmaps/q/b;->a()V

    :cond_0
    invoke-virtual {p0}, Lmaps/q/ad;->b()V

    iget-object v0, p0, Lmaps/q/ad;->e:Lmaps/q/bd;

    invoke-virtual {v0}, Lmaps/q/bd;->a()V

    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    invoke-interface {v0}, Lmaps/q/b;->b()V

    :cond_1
    iget-object v0, p0, Lmaps/q/ad;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/av;

    invoke-virtual {v0}, Lmaps/q/av;->b()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/q/ad;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/h;

    invoke-virtual {v0}, Lmaps/q/h;->c()V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    if-eqz v3, :cond_4

    invoke-virtual {v3, p0}, Lmaps/q/al;->a(Lmaps/q/ad;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lmaps/q/ad;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/h;

    invoke-virtual {v0}, Lmaps/q/h;->d()V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lmaps/q/ad;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/av;

    invoke-virtual {v0}, Lmaps/q/av;->e()V

    goto :goto_4

    :cond_7
    iget v0, p0, Lmaps/q/ad;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/q/ad;->m:I

    return-void
.end method

.method a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/q/af;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/q/ad;->i:Lmaps/q/y;

    invoke-virtual {v0}, Lmaps/q/y;->a()V

    iget-object v2, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    if-eqz v4, :cond_0

    invoke-virtual {v4, p0}, Lmaps/q/al;->b(Lmaps/q/ad;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    sget v2, Lmaps/q/z;->a:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v2, 0xd57

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    aget v0, v0, v1

    iput v0, p0, Lmaps/q/ad;->a:I

    const-wide/high16 v0, 0x4000000000000000L

    iget v2, p0, Lmaps/q/ad;->a:I

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lmaps/q/ad;->b:I

    sget-object v0, Lmaps/q/ad;->n:Ljava/lang/String;

    if-nez v0, :cond_3

    const/16 v0, 0x1f03

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/q/ad;->n:Ljava/lang/String;

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_3

    const-string v0, "EntityRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GL_EXTENSIONS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lmaps/q/ad;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method public a(Lmaps/q/b;)V
    .locals 0

    iput-object p1, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    return-void
.end method

.method public a(Lmaps/q/ba;)V
    .locals 3

    iget-object v0, p0, Lmaps/q/ad;->d:Lmaps/q/a;

    new-instance v1, Lmaps/q/am;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lmaps/q/am;-><init>(Lmaps/q/ba;Z)V

    invoke-static {v0, v1}, Lmaps/q/a;->a(Lmaps/q/a;Lmaps/q/am;)V

    return-void
.end method

.method public a(Lmaps/q/h;)V
    .locals 3

    iget-object v0, p0, Lmaps/q/ad;->d:Lmaps/q/a;

    new-instance v1, Lmaps/q/am;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lmaps/q/am;-><init>(Lmaps/q/h;Z)V

    invoke-static {v0, v1}, Lmaps/q/a;->a(Lmaps/q/a;Lmaps/q/am;)V

    return-void
.end method

.method public a(Lmaps/q/x;)V
    .locals 3

    iget-object v0, p0, Lmaps/q/ad;->d:Lmaps/q/a;

    new-instance v1, Lmaps/q/am;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lmaps/q/am;-><init>(Lmaps/q/x;Z)V

    invoke-static {v0, v1}, Lmaps/q/a;->a(Lmaps/q/a;Lmaps/q/am;)V

    return-void
.end method

.method a([Lmaps/q/z;Lmaps/q/ba;Lmaps/q/al;)V
    .locals 6

    const/4 v2, 0x0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget v0, p3, Lmaps/q/al;->b:I

    invoke-virtual {p2, v0}, Lmaps/q/ba;->c(I)Lmaps/q/r;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    :goto_1
    sget v0, Lmaps/q/z;->b:I

    :goto_2
    sget v3, Lmaps/q/z;->a:I

    if-ge v0, v3, :cond_6

    aget-object v3, p1, v0

    if-nez v3, :cond_4

    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v3, v3, v0

    aget-object v4, p1, v0

    invoke-virtual {v3, p0, v4}, Lmaps/q/z;->b(Lmaps/q/ad;Lmaps/q/z;)V

    const-string v3, "applyEntityStates"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in postDraw of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aput-object v2, v3, v0

    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_3

    const-string v1, "APPLY_ENTITY_STATES_GET_NEW_VERTEX_DATA"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v1, v2

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v3, v3, v0

    if-nez v3, :cond_5

    aget-object v3, p1, v0

    iget-object v4, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v4, v4, v0

    invoke-virtual {v3, p0, v4}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/z;)V

    const-string v3, "applyEntityStates"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in preDraw of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v4, p1, v0

    aput-object v4, v3, v0

    goto :goto_3

    :cond_5
    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v3, v3, v0

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v3, v3, v0

    aget-object v4, p1, v0

    invoke-virtual {v3, p0, v4}, Lmaps/q/z;->b(Lmaps/q/ad;Lmaps/q/z;)V

    const-string v3, "applyEntityStates"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in postDraw of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    aget-object v3, p1, v0

    iget-object v4, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v4, v4, v0

    invoke-virtual {v3, p0, v4}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/z;)V

    const-string v3, "applyEntityStates"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in preDraw of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v4, p1, v0

    aput-object v4, v3, v0

    goto/16 :goto_3

    :cond_6
    sget-object v0, Lmaps/q/ap;->d:Lmaps/q/ap;

    invoke-virtual {v0}, Lmaps/q/ap;->a()I

    move-result v0

    aget-object v0, p1, v0

    check-cast v0, Lmaps/q/aj;

    invoke-virtual {p3}, Lmaps/q/al;->a()Lmaps/q/h;

    move-result-object v2

    iget-object v3, p2, Lmaps/q/ba;->g:[Lmaps/q/k;

    iget v4, p3, Lmaps/q/al;->b:I

    aget-object v3, v3, v4

    iget v4, p3, Lmaps/q/al;->b:I

    invoke-virtual {v0, p2, v2, v3, v4}, Lmaps/q/aj;->a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V

    const-string v0, "applyEntityStates"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in copyDataToGl of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lmaps/q/ap;->d:Lmaps/q/ap;

    invoke-virtual {v3}, Lmaps/q/ap;->a()I

    move-result v3

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_8

    iget-object v0, p0, Lmaps/q/ad;->p:Lmaps/q/r;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lmaps/q/ad;->p:Lmaps/q/r;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/q/ad;->p:Lmaps/q/r;

    invoke-virtual {v0, p0, v1}, Lmaps/q/r;->b(Lmaps/q/ad;Lmaps/q/r;)V

    :cond_7
    iget-object v0, p0, Lmaps/q/ad;->p:Lmaps/q/r;

    invoke-virtual {v1, p0, v0}, Lmaps/q/r;->a(Lmaps/q/ad;Lmaps/q/r;)V

    :cond_8
    :try_start_1
    iget v0, p3, Lmaps/q/al;->b:I

    invoke-virtual {p2, v0}, Lmaps/q/ba;->c(I)Lmaps/q/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/ad;->p:Lmaps/q/r;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_0

    const-string v1, "APPLY_ENTITY_STATES"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method b()V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/q/ad;->d:Lmaps/q/a;

    invoke-static {v0}, Lmaps/q/a;->a(Lmaps/q/a;)Lmaps/q/am;

    move-result-object v2

    if-eqz v2, :cond_6

    :try_start_0
    iget v0, v2, Lmaps/q/am;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v0

    sget-object v3, Lmaps/q/f;->c:Lmaps/q/f;

    invoke-virtual {v0, p0, v3}, Lmaps/q/ba;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/q/ba;->b()B

    move-result v3

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_2

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_1

    iget-object v4, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/q/al;->a(Lmaps/q/ba;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/q/ba;->a()Lmaps/q/aw;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v2

    invoke-interface {v0, v2}, Lmaps/q/b;->a(Lmaps/q/ba;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v2, Lmaps/ae/h;->h:Z

    if-eqz v2, :cond_0

    const-string v2, "EntityRenderer"

    const-string v3, "applyPending failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v0

    sget-object v3, Lmaps/q/f;->c:Lmaps/q/f;

    invoke-virtual {v0, p0, v3}, Lmaps/q/ba;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/q/ba;->b()B

    move-result v3

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_4

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_3

    iget-object v4, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/q/al;->b(Lmaps/q/ba;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/q/ba;->a()Lmaps/q/aw;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/q/ad;->o:Lmaps/q/b;

    invoke-virtual {v2}, Lmaps/q/am;->a()Lmaps/q/ba;

    move-result-object v2

    invoke-interface {v0, v2}, Lmaps/q/b;->b(Lmaps/q/ba;)V

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {v2}, Lmaps/q/am;->b()Lmaps/q/h;

    move-result-object v0

    sget-object v3, Lmaps/q/f;->c:Lmaps/q/f;

    invoke-virtual {v0, p0, v3}, Lmaps/q/h;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    iget-object v0, p0, Lmaps/q/ad;->g:Ljava/util/Set;

    invoke-virtual {v2}, Lmaps/q/am;->b()Lmaps/q/h;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/q/h;->b()Lmaps/q/av;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/q/ad;->h:Ljava/util/Set;

    invoke-virtual {v2}, Lmaps/q/am;->b()Lmaps/q/h;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lmaps/q/am;->b()Lmaps/q/h;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/q/h;->e()B

    move-result v3

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_0

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_5

    iget-object v4, p0, Lmaps/q/ad;->f:[Lmaps/q/al;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lmaps/q/am;->b()Lmaps/q/h;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/q/al;->a(Lmaps/q/h;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Remove camera not implemented"

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_4
    iget-object v0, p0, Lmaps/q/ad;->e:Lmaps/q/bd;

    invoke-virtual {v2}, Lmaps/q/am;->c()Lmaps/q/x;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/q/bd;->a(Lmaps/q/x;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lmaps/q/ad;->e:Lmaps/q/bd;

    invoke-virtual {v2}, Lmaps/q/am;->c()Lmaps/q/x;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/q/bd;->b(Lmaps/q/x;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(Lmaps/q/ba;)V
    .locals 3

    iget-object v0, p0, Lmaps/q/ad;->d:Lmaps/q/a;

    new-instance v1, Lmaps/q/am;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lmaps/q/am;-><init>(Lmaps/q/ba;Z)V

    invoke-static {v0, v1}, Lmaps/q/a;->a(Lmaps/q/a;Lmaps/q/am;)V

    return-void
.end method

.method public b(Lmaps/q/x;)V
    .locals 3

    iget-object v0, p0, Lmaps/q/ad;->d:Lmaps/q/a;

    new-instance v1, Lmaps/q/am;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lmaps/q/am;-><init>(Lmaps/q/x;Z)V

    invoke-static {v0, v1}, Lmaps/q/a;->a(Lmaps/q/a;Lmaps/q/am;)V

    return-void
.end method

.method public c()Lmaps/q/j;
    .locals 1

    iget-object v0, p0, Lmaps/q/ad;->i:Lmaps/q/y;

    return-object v0
.end method

.method d()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    sget v1, Lmaps/q/z;->a:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0, v2}, Lmaps/q/z;->b(Lmaps/q/ad;Lmaps/q/z;)V

    iget-object v1, p0, Lmaps/q/ad;->c:[Lmaps/q/z;

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
