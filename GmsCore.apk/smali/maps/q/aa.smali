.class public Lmaps/q/aa;
.super Lmaps/q/ba;


# instance fields
.field final a:I

.field final b:I

.field private final c:Lmaps/t/ax;

.field private d:[F

.field private final k:I


# direct methods
.method public constructor <init>(ILmaps/t/ah;)V
    .locals 1

    invoke-direct {p0}, Lmaps/q/ba;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/q/aa;->d:[F

    iput p1, p0, Lmaps/q/aa;->a:I

    invoke-virtual {p2}, Lmaps/t/ah;->c()I

    move-result v0

    iput v0, p0, Lmaps/q/aa;->b:I

    invoke-virtual {p2}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/aa;->c:Lmaps/t/ax;

    const/16 v0, 0x8

    iput v0, p0, Lmaps/q/aa;->k:I

    return-void
.end method


# virtual methods
.method a(Lmaps/q/ad;Lmaps/q/al;)V
    .locals 6

    const/4 v5, 0x1

    iget-boolean v0, p0, Lmaps/q/aa;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p2, Lmaps/q/al;->a:Lmaps/q/h;

    iget v0, v0, Lmaps/q/h;->b:I

    iget-object v1, p0, Lmaps/q/aa;->h:[I

    iget v2, p2, Lmaps/q/al;->b:I

    aget v1, v1, v2

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p2, Lmaps/q/al;->a:Lmaps/q/h;

    check-cast v0, Lmaps/bq/d;

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/q/aa;->c:Lmaps/t/ax;

    invoke-virtual {v2}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v2

    iget-object v3, p0, Lmaps/q/aa;->c:Lmaps/t/ax;

    invoke-virtual {v3}, Lmaps/t/ax;->g()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lmaps/q/aa;->d:[F

    invoke-static {v1, v0, v2, v3, v4}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    iget-object v0, p0, Lmaps/q/aa;->e:Lmaps/q/k;

    invoke-virtual {v0}, Lmaps/q/k;->a()Lmaps/q/k;

    iget-object v0, p0, Lmaps/q/aa;->e:Lmaps/q/k;

    iget-object v1, p0, Lmaps/q/aa;->d:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lmaps/q/aa;->d:[F

    aget v2, v2, v5

    iget-object v3, p0, Lmaps/q/aa;->d:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lmaps/q/k;->a(FFF)Lmaps/q/k;

    iget-object v0, p0, Lmaps/q/aa;->e:Lmaps/q/k;

    iget-object v1, p0, Lmaps/q/aa;->d:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lmaps/q/k;->a(F)Lmaps/q/k;

    iput-boolean v5, p0, Lmaps/q/aa;->f:Z

    :cond_1
    invoke-super {p0, p1, p2}, Lmaps/q/ba;->a(Lmaps/q/ad;Lmaps/q/al;)V

    return-void
.end method

.method public a(Lmaps/q/k;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lmaps/q/r;)V
    .locals 1

    iget v0, p0, Lmaps/q/aa;->k:I

    invoke-super {p0, p1, v0}, Lmaps/q/ba;->a(Lmaps/q/r;I)V

    return-void
.end method

.method public a(Lmaps/q/r;I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use setVertexData(VertexData v) instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lmaps/q/z;)V
    .locals 1

    iget v0, p0, Lmaps/q/aa;->k:I

    invoke-super {p0, p1, v0}, Lmaps/q/ba;->a(Lmaps/q/z;I)V

    return-void
.end method

.method public a(Lmaps/q/z;I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use setState(EntityState e) instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
