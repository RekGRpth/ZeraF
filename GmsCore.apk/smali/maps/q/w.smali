.class public Lmaps/q/w;
.super Lmaps/q/bh;


# instance fields
.field protected a:I

.field protected b:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/q/bh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 2

    invoke-super {p0, p1}, Lmaps/q/bh;->a(I)V

    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/q/w;->a:I

    const-string v0, "ShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/q/w;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sTexture0 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/q/w;->a:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    const-string v0, "ShaderState"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "uTextureMatrix"

    invoke-virtual {p0, p1, v0}, Lmaps/q/w;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/q/w;->b:I

    return-void
.end method
