.class public Lmaps/bk/f;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:Z

.field private c:I

.field private final d:Ljava/util/LinkedHashSet;

.field private final e:Ljava/util/LinkedHashSet;

.field private f:Ljava/util/Iterator;

.field private final g:Lmaps/bk/i;

.field private final h:Ljava/util/LinkedList;

.field private i:Lmaps/cq/a;

.field private j:Lmaps/t/bx;

.field private k:J

.field private l:J

.field private m:B

.field private n:Z

.field private o:Ljava/util/Map;

.field private p:Ljava/util/Set;

.field private q:Lmaps/bk/a;


# direct methods
.method public constructor <init>(IIZZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/f;->e:Ljava/util/LinkedHashSet;

    invoke-static {}, Lmaps/f/fd;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/f;->h:Ljava/util/LinkedList;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/bk/f;->k:J

    const/4 v0, 0x4

    iput-byte v0, p0, Lmaps/bk/f;->m:B

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/f;->o:Ljava/util/Map;

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/f;->p:Ljava/util/Set;

    iput p1, p0, Lmaps/bk/f;->a:I

    new-instance v0, Lmaps/bk/i;

    invoke-direct {v0, p0, p4}, Lmaps/bk/i;-><init>(Lmaps/bk/f;Z)V

    iput-object v0, p0, Lmaps/bk/f;->g:Lmaps/bk/i;

    iput-boolean p3, p0, Lmaps/bk/f;->b:Z

    iput p2, p0, Lmaps/bk/f;->c:I

    return-void
.end method

.method static synthetic a(Lmaps/bk/f;)Ljava/util/LinkedHashSet;
    .locals 1

    iget-object v0, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    return-object v0
.end method

.method static synthetic b(Lmaps/bk/f;)J
    .locals 2

    invoke-direct {p0}, Lmaps/bk/f;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method private declared-synchronized b(Lmaps/bk/a;Z)Lmaps/bk/a;
    .locals 8

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lmaps/bk/f;->a(Lmaps/bk/a;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-wide v1, p1, Lmaps/bk/a;->c:J

    iget-byte v5, p0, Lmaps/bk/f;->m:B

    if-nez v5, :cond_2

    iget-object v5, p0, Lmaps/bk/f;->q:Lmaps/bk/a;

    iget-wide v5, v5, Lmaps/bk/a;->c:J

    cmp-long v5, v1, v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    iput-byte v5, p0, Lmaps/bk/f;->m:B

    invoke-direct {p0}, Lmaps/bk/f;->e()V

    move p2, v4

    :cond_2
    if-eqz p2, :cond_3

    iget-object v5, p0, Lmaps/bk/f;->q:Lmaps/bk/a;

    if-eq p1, v5, :cond_3

    iget-object v5, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    iget-object v6, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    iget-object v5, p0, Lmaps/bk/f;->p:Ljava/util/Set;

    iget-object v6, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-byte v5, p0, Lmaps/bk/f;->m:B

    if-ne v5, v3, :cond_6

    iget-wide v5, p0, Lmaps/bk/f;->k:J

    cmp-long v5, v1, v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lmaps/bk/f;->p:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    iget-object v6, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v6}, Ljava/util/LinkedHashSet;->size()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lmaps/bk/f;->c:I

    if-ge v5, v6, :cond_4

    iget-object v5, p0, Lmaps/bk/f;->f:Ljava/util/Iterator;

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v1, Lmaps/bk/a;

    iget-object v0, p0, Lmaps/bk/f;->f:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-direct {p0}, Lmaps/bk/f;->d()J

    move-result-wide v2

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-direct {v1, v0, v2, v3, v4}, Lmaps/bk/a;-><init>(Lmaps/t/ah;JZ)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v5, p0, Lmaps/bk/f;->n:Z

    if-eqz v5, :cond_5

    const/4 v1, 0x4

    iput-byte v1, p0, Lmaps/bk/f;->m:B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    const/4 v5, 0x2

    :try_start_2
    iput-byte v5, p0, Lmaps/bk/f;->m:B

    iget-object v5, p0, Lmaps/bk/f;->g:Lmaps/bk/i;

    invoke-virtual {v5}, Lmaps/bk/i;->a()V

    :cond_6
    iget-object v5, p0, Lmaps/bk/f;->p:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    iget v6, p0, Lmaps/bk/f;->c:I

    if-ge v5, v6, :cond_7

    :goto_1
    if-nez v3, :cond_8

    const/4 v1, 0x4

    iput-byte v1, p0, Lmaps/bk/f;->m:B

    goto/16 :goto_0

    :cond_7
    move v3, v4

    goto :goto_1

    :cond_8
    iget-byte v4, p0, Lmaps/bk/f;->m:B

    if-ne v4, v7, :cond_a

    iget-object v1, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    iget-wide v1, p1, Lmaps/bk/a;->c:J

    iget-wide v4, p0, Lmaps/bk/f;->k:J

    cmp-long v1, v1, v4

    if-nez v1, :cond_0

    if-eqz v3, :cond_0

    iget-object v0, p0, Lmaps/bk/f;->g:Lmaps/bk/i;

    invoke-virtual {v0, p2}, Lmaps/bk/i;->a(Z)Lmaps/bk/a;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    iget-wide v1, p0, Lmaps/bk/f;->k:J

    const/4 v4, 0x3

    iput-byte v4, p0, Lmaps/bk/f;->m:B

    :cond_a
    iget-byte v4, p0, Lmaps/bk/f;->m:B

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    iget-wide v4, p0, Lmaps/bk/f;->k:J

    cmp-long v1, v1, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/bk/f;->h:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    if-eqz v3, :cond_b

    iget-object v0, p0, Lmaps/bk/f;->h:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    new-instance v2, Lmaps/bk/a;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/t/ah;

    invoke-direct {p0}, Lmaps/bk/f;->d()J

    move-result-wide v3

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lmaps/bk/a;-><init>(Lmaps/t/ah;JZ)V

    move-object v0, v2

    goto/16 :goto_0

    :cond_b
    const/4 v1, 0x4

    iput-byte v1, p0, Lmaps/bk/f;->m:B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method static synthetic c(Lmaps/bk/f;)I
    .locals 1

    iget v0, p0, Lmaps/bk/f;->a:I

    return v0
.end method

.method private d()J
    .locals 4

    iget-wide v0, p0, Lmaps/bk/f;->k:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/bk/f;->k:J

    return-wide v0
.end method

.method static synthetic d(Lmaps/bk/f;)Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/bk/f;->j:Lmaps/t/bx;

    return-object v0
.end method

.method static synthetic e(Lmaps/bk/f;)Lmaps/cq/a;
    .locals 1

    iget-object v0, p0, Lmaps/bk/f;->i:Lmaps/cq/a;

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 7

    const/4 v6, -0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bk/f;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/bk/f;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lmaps/bk/f;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-boolean v0, p0, Lmaps/bk/f;->b:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v2

    iget-object v0, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/bk/f;->i:Lmaps/cq/a;

    iget v1, p0, Lmaps/bk/f;->a:I

    iget-object v3, p0, Lmaps/bk/f;->j:Lmaps/t/bx;

    const/16 v4, 0x8

    iget-object v5, p0, Lmaps/bk/f;->e:Ljava/util/LinkedHashSet;

    invoke-static/range {v0 .. v5}, Lmaps/bu/d;->a(Lmaps/cq/a;ILjava/util/Collection;Lmaps/t/bx;ILjava/util/LinkedHashSet;)Ljava/util/LinkedHashSet;

    iget-boolean v0, p0, Lmaps/bk/f;->n:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iget-object v1, p0, Lmaps/bk/f;->e:Ljava/util/LinkedHashSet;

    iget-object v2, p0, Lmaps/bk/f;->i:Lmaps/cq/a;

    iget-object v3, p0, Lmaps/bk/f;->j:Lmaps/t/bx;

    invoke-interface {v2, v0, v3}, Lmaps/cq/a;->a(ILmaps/t/bx;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iget-object v0, p0, Lmaps/bk/f;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/f;->f:Ljava/util/Iterator;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    :try_start_2
    iget-object v0, p0, Lmaps/bk/f;->e:Ljava/util/LinkedHashSet;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    iget-object v2, p0, Lmaps/bk/f;->h:Ljava/util/LinkedList;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/bk/f;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v6

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    if-ne v1, v6, :cond_4

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    :goto_3
    move v1, v0

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    if-ne v1, v0, :cond_5

    move v0, v1

    goto :goto_3

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public declared-synchronized a(Lmaps/bk/a;Z)Lmaps/bk/a;
    .locals 2

    monitor-enter p0

    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2}, Lmaps/bk/f;->b(Lmaps/bk/a;Z)Lmaps/bk/a;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 p2, 0x0

    iget-object v0, p0, Lmaps/bk/f;->o:Ljava/util/Map;

    iget-object v1, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bk/a;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lmaps/bk/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lmaps/bk/a;->b:Z

    if-nez v0, :cond_0

    :cond_1
    iget-object v0, p0, Lmaps/bk/f;->o:Ljava/util/Map;

    iget-object v1, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p1, Lmaps/bk/a;->b:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/bk/f;->p:Ljava/util/Set;

    iget-object v1, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/bk/f;->n:Z

    if-nez v0, :cond_0

    const/4 v0, 0x4

    iput-byte v0, p0, Lmaps/bk/f;->m:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lmaps/cq/a;Lmaps/t/bx;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V
    .locals 5

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-byte v0, p0, Lmaps/bk/f;->m:B

    invoke-direct {p0}, Lmaps/bk/f;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/bk/f;->l:J

    iput-object p1, p0, Lmaps/bk/f;->i:Lmaps/cq/a;

    iput-object p2, p0, Lmaps/bk/f;->j:Lmaps/t/bx;

    new-instance v0, Lmaps/bk/a;

    sget-object v1, Lmaps/an/y;->h:Lmaps/t/ah;

    invoke-direct {p0}, Lmaps/bk/f;->d()J

    move-result-wide v2

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/bk/a;-><init>(Lmaps/t/ah;JZ)V

    iput-object v0, p0, Lmaps/bk/f;->q:Lmaps/bk/a;

    iget-object v0, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lmaps/bk/f;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p3}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/bk/f;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    iget-object v2, p0, Lmaps/bk/f;->h:Ljava/util/LinkedList;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-eqz p5, :cond_1

    :try_start_1
    invoke-interface {p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    iget-object v2, p0, Lmaps/bk/f;->h:Ljava/util/LinkedList;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iput-boolean p6, p0, Lmaps/bk/f;->n:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lmaps/bk/a;)Z
    .locals 4

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-wide v0, p1, Lmaps/bk/a;->c:J

    iget-wide v2, p0, Lmaps/bk/f;->l:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lmaps/bk/f;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Lmaps/bk/a;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bk/f;->q:Lmaps/bk/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lmaps/bk/f;->m:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
