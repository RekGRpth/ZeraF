.class Lmaps/bk/i;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lmaps/bk/f;

.field private b:Z

.field private c:I

.field private d:Lmaps/t/ah;

.field private e:Ljava/util/LinkedHashSet;

.field private f:Ljava/util/LinkedHashSet;

.field private g:Ljava/util/Iterator;


# direct methods
.method public constructor <init>(Lmaps/bk/f;Z)V
    .locals 1

    iput-object p1, p0, Lmaps/bk/i;->a:Lmaps/bk/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/bk/i;->c:I

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/i;->e:Ljava/util/LinkedHashSet;

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/i;->f:Ljava/util/LinkedHashSet;

    iput-boolean p2, p0, Lmaps/bk/i;->b:Z

    return-void
.end method

.method private b(Z)V
    .locals 3

    iget v0, p0, Lmaps/bk/i;->c:I

    iget-object v1, p0, Lmaps/bk/i;->a:Lmaps/bk/f;

    invoke-static {v1}, Lmaps/bk/f;->c(Lmaps/bk/f;)I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lmaps/bk/i;->a:Lmaps/bk/f;

    invoke-static {v0}, Lmaps/bk/f;->e(Lmaps/bk/f;)Lmaps/cq/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/bk/i;->d:Lmaps/t/ah;

    iget-object v2, p0, Lmaps/bk/i;->a:Lmaps/bk/f;

    invoke-static {v2}, Lmaps/bk/f;->d(Lmaps/bk/f;)Lmaps/t/bx;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lmaps/cq/a;->a(Lmaps/t/ah;Lmaps/t/bx;)Lmaps/t/ah;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/bk/i;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lmaps/bk/i;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bk/i;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/bk/i;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bk/i;->c:I

    iget-object v0, p0, Lmaps/bk/i;->e:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lmaps/bk/i;->f:Ljava/util/LinkedHashSet;

    iput-object v1, p0, Lmaps/bk/i;->e:Ljava/util/LinkedHashSet;

    iput-object v0, p0, Lmaps/bk/i;->f:Ljava/util/LinkedHashSet;

    iget-object v0, p0, Lmaps/bk/i;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lmaps/bk/i;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/i;->g:Ljava/util/Iterator;

    goto :goto_0
.end method


# virtual methods
.method public a(Z)Lmaps/bk/a;
    .locals 5

    iget-object v0, p0, Lmaps/bk/i;->d:Lmaps/t/ah;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lmaps/bk/i;->b(Z)V

    :cond_0
    iget-object v0, p0, Lmaps/bk/i;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lmaps/bk/i;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    iput-object v0, p0, Lmaps/bk/i;->d:Lmaps/t/ah;

    new-instance v1, Lmaps/bk/a;

    iget-object v2, p0, Lmaps/bk/i;->d:Lmaps/t/ah;

    iget-object v0, p0, Lmaps/bk/i;->a:Lmaps/bk/f;

    invoke-static {v0}, Lmaps/bk/f;->b(Lmaps/bk/f;)J

    move-result-wide v3

    iget-boolean v0, p0, Lmaps/bk/i;->b:Z

    if-nez v0, :cond_2

    iget v0, p0, Lmaps/bk/i;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v1, v2, v3, v4, v0}, Lmaps/bk/a;-><init>(Lmaps/t/ah;JZ)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lmaps/bk/i;->c:I

    iget-object v0, p0, Lmaps/bk/i;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lmaps/bk/i;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/bk/i;->d:Lmaps/t/ah;

    iget-object v0, p0, Lmaps/bk/i;->e:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lmaps/bk/i;->a:Lmaps/bk/f;

    invoke-static {v1}, Lmaps/bk/f;->a(Lmaps/bk/f;)Ljava/util/LinkedHashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/bk/i;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/i;->g:Ljava/util/Iterator;

    return-void
.end method
