.class public final Lmaps/au/f;
.super Ljava/lang/Object;


# instance fields
.field public final a:D

.field public final b:D

.field public final c:D


# direct methods
.method public constructor <init>(DDD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lmaps/au/f;->a:D

    iput-wide p3, p0, Lmaps/au/f;->b:D

    iput-wide p5, p0, Lmaps/au/f;->c:D

    return-void
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/au/f;
    .locals 9

    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    new-instance v0, Lmaps/au/f;

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    mul-double/2addr v1, v7

    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    mul-double/2addr v3, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, Lmaps/au/f;-><init>(DDD)V

    return-object v0
.end method

.method public static a(Lmaps/au/f;Lmaps/au/f;)Lmaps/au/f;
    .locals 11

    const-wide/high16 v9, 0x4000000000000000L

    new-instance v0, Lmaps/au/f;

    iget-wide v1, p0, Lmaps/au/f;->a:D

    iget-wide v3, p1, Lmaps/au/f;->a:D

    add-double/2addr v1, v3

    div-double/2addr v1, v9

    iget-wide v3, p0, Lmaps/au/f;->b:D

    iget-wide v5, p1, Lmaps/au/f;->b:D

    add-double/2addr v3, v5

    div-double/2addr v3, v9

    iget-wide v5, p0, Lmaps/au/f;->c:D

    iget-wide v7, p1, Lmaps/au/f;->c:D

    add-double/2addr v5, v7

    div-double/2addr v5, v9

    invoke-direct/range {v0 .. v6}, Lmaps/au/f;-><init>(DDD)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/android/gms/maps/model/LatLng;
    .locals 10

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lmaps/au/f;->a:D

    cmpl-double v2, v2, v0

    if-nez v2, :cond_0

    iget-wide v2, p0, Lmaps/au/f;->b:D

    cmpl-double v2, v2, v0

    if-nez v2, :cond_0

    iget-wide v2, p0, Lmaps/au/f;->c:D

    cmpl-double v2, v2, v0

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/ArithmeticException;

    invoke-direct {v0}, Ljava/lang/ArithmeticException;-><init>()V

    throw v0

    :cond_0
    iget-wide v2, p0, Lmaps/au/f;->c:D

    iget-wide v4, p0, Lmaps/au/f;->a:D

    iget-wide v6, p0, Lmaps/au/f;->a:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lmaps/au/f;->b:D

    iget-wide v8, p0, Lmaps/au/f;->b:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    iget-wide v4, p0, Lmaps/au/f;->b:D

    cmpl-double v4, v4, v0

    if-nez v4, :cond_1

    iget-wide v4, p0, Lmaps/au/f;->a:D

    cmpl-double v4, v4, v0

    if-nez v4, :cond_1

    :goto_0
    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    invoke-direct {v4, v2, v3, v0, v1}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v4

    :cond_1
    iget-wide v0, p0, Lmaps/au/f;->b:D

    iget-wide v4, p0, Lmaps/au/f;->a:D

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    goto :goto_0
.end method
