.class public final Lmaps/au/e;
.super Ljava/lang/Object;


# static fields
.field static final synthetic a:Z

.field private static final d:Lmaps/au/e;


# instance fields
.field private final b:Ljava/lang/Thread;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lmaps/au/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/au/e;->a:Z

    new-instance v0, Lmaps/au/e;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "Not on the main thread"

    invoke-direct {v0, v1, v2}, Lmaps/au/e;-><init>(Ljava/lang/Thread;Ljava/lang/String;)V

    sput-object v0, Lmaps/au/e;->d:Lmaps/au/e;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/Thread;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/au/e;->b:Ljava/lang/Thread;

    iput-object p2, p0, Lmaps/au/e;->c:Ljava/lang/String;

    return-void
.end method

.method public static a()Lmaps/au/e;
    .locals 1

    sget-object v0, Lmaps/au/e;->d:Lmaps/au/e;

    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lmaps/au/e;->b:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lmaps/au/e;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    sget-boolean v0, Lmaps/au/e;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lmaps/au/e;->b:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lmaps/au/e;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    return-void
.end method
