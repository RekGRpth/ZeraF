.class Lmaps/y/ag;
.super Lmaps/y/ay;


# instance fields
.field private a:I

.field private b:Z

.field private c:F

.field private d:J

.field private final e:I


# direct methods
.method public constructor <init>(Lmaps/y/am;I)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/y/ay;-><init>(Lmaps/y/am;)V

    iput p2, p0, Lmaps/y/ag;->e:I

    return-void
.end method

.method static a(JJIZ)F
    .locals 3

    const/high16 v2, 0x3f800000

    sub-long v0, p2, p0

    long-to-float v0, v0

    int-to-float v1, p4

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    if-eqz p5, :cond_0

    :goto_0
    return v0

    :cond_0
    sub-float v0, v2, v0

    goto :goto_0
.end method

.method static a(JJFZ)J
    .locals 2

    if-eqz p5, :cond_0

    :goto_0
    long-to-float v0, p2

    mul-float/2addr v0, p4

    float-to-int v0, v0

    int-to-long v0, v0

    sub-long v0, p0, v0

    return-wide v0

    :cond_0
    const/high16 v0, 0x3f800000

    sub-float p4, v0, p4

    goto :goto_0
.end method


# virtual methods
.method a(ZJ)V
    .locals 6

    iget-boolean v0, p0, Lmaps/y/ag;->b:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lmaps/y/ag;->b:Z

    iget v0, p0, Lmaps/y/ag;->e:I

    int-to-long v2, v0

    iget v4, p0, Lmaps/y/ag;->c:F

    iget-boolean v5, p0, Lmaps/y/ag;->b:Z

    move-wide v0, p2

    invoke-static/range {v0 .. v5}, Lmaps/y/ag;->a(JJFZ)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/y/ag;->d:J

    iget-wide v0, p0, Lmaps/y/ag;->d:J

    iget-wide v2, p0, Lmaps/y/ag;->d:J

    iget v4, p0, Lmaps/y/ag;->e:I

    iget-boolean v5, p0, Lmaps/y/ag;->b:Z

    invoke-static/range {v0 .. v5}, Lmaps/y/ag;->a(JJIZ)F

    move-result v0

    iput v0, p0, Lmaps/y/ag;->c:F

    :cond_0
    return-void
.end method

.method a(J)Z
    .locals 6

    iget-wide v0, p0, Lmaps/y/ag;->d:J

    iget v4, p0, Lmaps/y/ag;->e:I

    iget-boolean v5, p0, Lmaps/y/ag;->b:Z

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lmaps/y/ag;->a(JJIZ)F

    move-result v0

    iget-boolean v1, p0, Lmaps/y/ag;->b:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lmaps/y/ag;->c:F

    const/high16 v2, 0x3f800000

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lmaps/y/ag;->b:Z

    if-nez v1, :cond_2

    iget v1, p0, Lmaps/y/ag;->c:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    :cond_1
    iput v0, p0, Lmaps/y/ag;->c:F

    iget v0, p0, Lmaps/y/ag;->c:F

    iget v1, p0, Lmaps/y/ag;->a:I

    invoke-static {v0, v1}, Lmaps/s/k;->a(FI)I

    move-result v0

    invoke-super {p0, v0}, Lmaps/y/ay;->b(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lmaps/y/ag;->a:I

    return-void
.end method

.method public declared-synchronized b(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lmaps/y/ag;->a(ZJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/y/ag;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lmaps/y/ag;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/y/ag;->a(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
