.class public Lmaps/y/an;
.super Lmaps/y/ba;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:Lmaps/ac/c;

.field private final f:I


# direct methods
.method public constructor <init>(ILmaps/y/f;)V
    .locals 1

    invoke-direct {p0, p2}, Lmaps/y/ba;-><init>(Lmaps/y/f;)V

    iput p1, p0, Lmaps/y/an;->f:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/y/an;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lmaps/y/f;)V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lmaps/y/an;-><init>(ILmaps/y/f;)V

    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lmaps/y/an;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/y/an;->b:Lmaps/ac/c;

    invoke-virtual {v0}, Lmaps/ac/c;->d()Lmaps/ac/f;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lmaps/ac/f;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lmaps/ac/f;->a()Lmaps/l/u;

    move-result-object v1

    iget v2, p0, Lmaps/y/an;->f:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {v1}, Lmaps/l/u;->u()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    iget v2, p0, Lmaps/y/an;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, Lmaps/l/u;->u()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v2, p0, Lmaps/y/an;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/util/List;FFLmaps/t/bx;Lmaps/bq/d;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/an;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    instance-of v2, v0, Lmaps/y/bh;

    if-eqz v2, :cond_0

    check-cast v0, Lmaps/y/bh;

    invoke-interface {v0, p2, p3, p4, p5}, Lmaps/y/bh;->c(FFLmaps/t/bx;Lmaps/bq/d;)I

    move-result v2

    if-ge v2, p6, :cond_0

    new-instance v3, Lmaps/y/d;

    invoke-direct {v3, v0, p0, v2}, Lmaps/y/d;-><init>(Lmaps/y/bh;Lmaps/y/ba;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public a(Lmaps/ac/c;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/an;->b:Lmaps/ac/c;

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    iget-object v0, p0, Lmaps/y/an;->b:Lmaps/ac/c;

    if-eqz v0, :cond_0

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    sget-object v1, Lmaps/af/q;->f:Lmaps/af/q;

    if-eq v0, v1, :cond_0

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    sget-object v1, Lmaps/af/q;->e:Lmaps/af/q;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/j/d;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/j/d;

    sget-object v1, Lmaps/j/d;->g:Lmaps/t/ah;

    invoke-virtual {v0, v1}, Lmaps/j/d;->a(Lmaps/t/ah;)V

    :cond_2
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/y/an;->e()V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/y/an;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lmaps/y/an;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    invoke-virtual {p1}, Lmaps/cr/c;->D()V

    invoke-virtual {v0, p1, p2, p3}, Lmaps/l/u;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    invoke-virtual {p1}, Lmaps/cr/c;->F()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/j/d;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/j/d;

    sget-object v1, Lmaps/j/d;->f:Lmaps/t/ah;

    invoke-virtual {v0, v1}, Lmaps/j/d;->a(Lmaps/t/ah;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()Lmaps/y/am;
    .locals 2

    iget v0, p0, Lmaps/y/an;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lmaps/y/am;->p:Lmaps/y/am;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lmaps/y/am;->l:Lmaps/y/am;

    goto :goto_0
.end method
