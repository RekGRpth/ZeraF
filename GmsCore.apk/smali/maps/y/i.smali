.class public Lmaps/y/i;
.super Lmaps/y/ab;

# interfaces
.implements Lmaps/af/n;
.implements Lmaps/b/c;
.implements Lmaps/b/g;
.implements Lmaps/b/y;


# instance fields
.field private volatile d:Z

.field private volatile e:Z

.field private final f:Lmaps/an/i;

.field private final g:Lmaps/b/r;

.field private final h:Lmaps/b/s;

.field private i:Lmaps/bu/a;

.field private final j:Lmaps/y/ag;

.field private final k:Ljava/util/Set;

.field private volatile l:Ljava/util/Set;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Ljava/util/List;

.field private final p:Lmaps/p/z;


# direct methods
.method protected constructor <init>(Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;ILandroid/content/Context;Lmaps/b/r;)V
    .locals 20

    sget-object v5, Lmaps/o/c;->n:Lmaps/o/c;

    new-instance v7, Lmaps/y/bq;

    move-object/from16 v0, p2

    move/from16 v1, p3

    move-object/from16 v2, p8

    move-object/from16 v3, p9

    invoke-direct {v7, v0, v1, v2, v3}, Lmaps/y/bq;-><init>(Lmaps/cq/b;ILandroid/content/Context;Lmaps/b/r;)V

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object/from16 v4, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    move/from16 v13, p7

    invoke-direct/range {v4 .. v19}, Lmaps/y/ab;-><init>(Lmaps/o/c;Lmaps/bk/b;Lmaps/cq/b;IIILmaps/y/am;IIZZZZZZ)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lmaps/y/i;->d:Z

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lmaps/y/i;->e:Z

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->l:Ljava/util/Set;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->m:Ljava/util/Map;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->n:Ljava/util/Map;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->o:Ljava/util/List;

    new-instance v4, Lmaps/y/ac;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lmaps/y/ac;-><init>(Lmaps/y/i;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->p:Lmaps/p/z;

    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->f:Lmaps/an/i;

    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lmaps/y/i;->g:Lmaps/b/r;

    new-instance v4, Lmaps/b/s;

    invoke-direct {v4}, Lmaps/b/s;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->h:Lmaps/b/s;

    new-instance v4, Lmaps/y/ag;

    sget-object v5, Lmaps/y/am;->d:Lmaps/y/am;

    const/16 v6, 0x12c

    invoke-direct {v4, v5, v6}, Lmaps/y/ag;-><init>(Lmaps/y/am;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lmaps/y/i;->j:Lmaps/y/ag;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/i;->j:Lmaps/y/ag;

    const v5, -0x7fafafb0

    invoke-virtual {v4, v5}, Lmaps/y/ag;->b(I)V

    return-void
.end method

.method static synthetic a(Lmaps/y/i;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/y/i;->l:Ljava/util/Set;

    return-object v0
.end method

.method private a(Lmaps/t/bg;)Lmaps/ac/g;
    .locals 1

    iget-object v0, p0, Lmaps/y/i;->c:Lmaps/ac/g;

    invoke-static {v0, p1}, Lmaps/ac/b;->a(Lmaps/ac/g;Ljava/lang/Object;)Lmaps/ac/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/t/e;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/b/r;->a(Lmaps/t/bg;)Lmaps/t/bb;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/b/r;->c(Lmaps/t/bg;)Lmaps/t/bb;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v2, v1, v0}, Lmaps/b/r;->b(Lmaps/t/bb;Lmaps/t/bb;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {v2}, Lmaps/bu/a;->c()V

    iget-object v2, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v2, v1, v0}, Lmaps/b/r;->a(Lmaps/t/bb;Lmaps/t/bb;)V

    iget-object v2, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v2, v0, v5, v5, v5}, Lmaps/b/r;->a(Lmaps/t/bg;ZZZ)Lmaps/b/z;

    move-result-object v2

    const/4 v0, 0x0

    iget-object v3, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v1}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v4

    invoke-virtual {v3, v4, v5, v5, v0}, Lmaps/b/r;->a(Lmaps/t/bg;ZZZ)Lmaps/b/z;

    move-result-object v3

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    iget-object v4, p0, Lmaps/y/i;->k:Ljava/util/Set;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/z;

    invoke-virtual {v0}, Lmaps/b/z;->e()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lmaps/b/z;->b()F

    move-result v0

    invoke-virtual {v3}, Lmaps/b/z;->b()F

    move-result v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Lmaps/b/z;->a(I)V

    const/16 v0, 0x18

    invoke-virtual {v3, v0}, Lmaps/b/z;->a(I)V

    :goto_2
    iget-object v0, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lmaps/bu/a;->a(Lmaps/t/bg;Lmaps/t/bb;)V

    goto/16 :goto_0

    :cond_3
    const/16 v0, 0x14

    :try_start_2
    invoke-virtual {v2, v0}, Lmaps/b/z;->a(I)V

    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Lmaps/b/z;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private c(Lmaps/bq/d;)V
    .locals 6

    iget-object v0, p0, Lmaps/y/i;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/y/i;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p1}, Lmaps/bq/d;->t()F

    move-result v0

    const/high16 v1, 0x41880000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lmaps/y/i;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v1

    sget-object v3, Lmaps/t/cm;->d:Lmaps/t/cm;

    invoke-virtual {v1, v3}, Lmaps/t/ah;->a(Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v1

    check-cast v1, Lmaps/t/cu;

    invoke-virtual {v1}, Lmaps/t/cu;->b()Lmaps/t/bg;

    move-result-object v3

    iget-object v1, p0, Lmaps/y/i;->n:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/g;

    if-nez v1, :cond_1

    invoke-direct {p0, v3}, Lmaps/y/i;->a(Lmaps/t/bg;)Lmaps/ac/g;

    move-result-object v1

    iget-object v4, p0, Lmaps/y/i;->n:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v3, p0, Lmaps/y/i;->h:Lmaps/b/s;

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v3, v0}, Lmaps/b/s;->a(Lmaps/t/ah;)Lmaps/b/k;

    move-result-object v3

    if-eqz v3, :cond_0

    sget-object v0, Lmaps/b/s;->a:Lmaps/b/k;

    if-eq v3, v0, :cond_0

    iget-object v0, p0, Lmaps/y/i;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/d;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/t/d;

    const/4 v4, 0x1

    new-array v4, v4, [Lmaps/t/av;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-direct {v0, v4}, Lmaps/t/d;-><init>([Lmaps/t/av;)V

    iget-object v3, p0, Lmaps/y/i;->m:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lmaps/t/d;->a(Lmaps/t/av;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private d(Lmaps/bq/d;)V
    .locals 3

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/bq/d;->t()F

    move-result v0

    const v1, 0x416e6666

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {v0, p1}, Lmaps/bu/a;->c(Lmaps/bq/d;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v1, v0}, Lmaps/b/r;->a(Ljava/util/Set;)V

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {v0, p1}, Lmaps/bu/a;->a(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    iget-object v2, p0, Lmaps/y/i;->h:Lmaps/b/s;

    invoke-virtual {v2, v0}, Lmaps/b/s;->a(Lmaps/t/ah;)Lmaps/b/k;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/b/r;->a(Ljava/util/Set;)V

    goto :goto_0
.end method

.method private e(Lmaps/bq/d;)V
    .locals 3

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->t()F

    move-result v1

    const/high16 v2, 0x41880000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {v0, p1}, Lmaps/bu/a;->e(Lmaps/bq/d;)Lmaps/t/bg;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v1, v0}, Lmaps/b/r;->d(Lmaps/t/bg;)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/b/r;->c()Lmaps/t/e;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/y/i;->j:Lmaps/y/ag;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmaps/t/bi;->g()I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lmaps/y/ag;->b(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v1, v0}, Lmaps/b/r;->b(Lmaps/t/e;)Lmaps/t/bi;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Lmaps/t/u;Lmaps/p/ac;Ljava/util/Set;)I
    .locals 5

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/b/r;->g()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/y/i;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v1

    sget-object v4, Lmaps/t/cm;->d:Lmaps/t/cm;

    invoke-virtual {v1, v4}, Lmaps/t/ah;->a(Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v1

    check-cast v1, Lmaps/t/cu;

    invoke-virtual {v1}, Lmaps/t/cu;->b()Lmaps/t/bg;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p1, :cond_1

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v4

    invoke-virtual {p1, v4}, Lmaps/t/u;->b(Lmaps/t/an;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_1
    invoke-direct {p0, v1}, Lmaps/y/i;->a(Lmaps/t/bg;)Lmaps/ac/g;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/l/al;->a(Lmaps/ac/g;)V

    invoke-interface {v0, p2}, Lmaps/l/al;->a(Lmaps/p/ac;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Lmaps/bq/d;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {v0, p1}, Lmaps/bu/a;->b(Lmaps/bq/d;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;)V
    .locals 2

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/b/r;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/y/i;->a(Lmaps/t/bg;)Lmaps/ac/g;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/y/i;->m:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public a(Lmaps/b/r;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/i;->d:Z

    iput-boolean v0, p0, Lmaps/y/i;->e:Z

    invoke-direct {p0}, Lmaps/y/i;->t()V

    invoke-virtual {p1}, Lmaps/b/r;->c()Lmaps/t/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/t/e;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lmaps/t/cf;->a:Lmaps/ap/i;

    invoke-static {v0, v1}, Lmaps/f/fa;->a(Ljava/lang/Iterable;Lmaps/ap/i;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/bd;->a(Ljava/lang/Iterable;)Lmaps/f/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/i;->l:Ljava/util/Set;

    :goto_0
    invoke-virtual {p0}, Lmaps/y/i;->h()V

    return-void

    :cond_0
    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/i;->l:Ljava/util/Set;

    goto :goto_0
.end method

.method public a(Lmaps/b/r;Lmaps/t/e;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/i;->d:Z

    iput-boolean v0, p0, Lmaps/y/i;->e:Z

    invoke-direct {p0}, Lmaps/y/i;->t()V

    invoke-direct {p0, p2}, Lmaps/y/i;->a(Lmaps/t/e;)V

    invoke-virtual {p0}, Lmaps/y/i;->h()V

    return-void
.end method

.method protected a(Lmaps/cq/a;)V
    .locals 1

    move-object v0, p1

    check-cast v0, Lmaps/bu/a;

    iput-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-super {p0, p1}, Lmaps/y/ab;->a(Lmaps/cq/a;)V

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 2

    invoke-interface {p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/av;->b()Lmaps/p/j;

    move-result-object v0

    sget-object v1, Lmaps/p/j;->d:Lmaps/p/j;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lmaps/y/i;->j:Lmaps/y/ag;

    invoke-virtual {v0}, Lmaps/y/ag;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/i;->j:Lmaps/y/ag;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/ag;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/bq/d;->t()F

    move-result v0

    const v1, 0x416e6666

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    invoke-super {p0, p1, p2, p3}, Lmaps/y/ab;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/i;->b:Z

    goto :goto_0
.end method

.method public a(Lmaps/t/ah;Lmaps/b/k;)V
    .locals 0

    invoke-virtual {p0}, Lmaps/y/i;->h()V

    return-void
.end method

.method public a(Ljava/util/List;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/y/i;->b(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Lmaps/y/i;->e(Lmaps/bq/d;)V

    invoke-direct {p0, p1}, Lmaps/y/i;->d(Lmaps/bq/d;)V

    iget-object v1, p0, Lmaps/y/i;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    invoke-virtual {p1}, Lmaps/bq/d;->t()F

    move-result v1

    const v2, 0x416e6666

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/i;->d:Z

    invoke-super {p0, p1, p2}, Lmaps/y/ab;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    move-result v0

    invoke-direct {p0, p1}, Lmaps/y/i;->c(Lmaps/bq/d;)V

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Lmaps/y/i;->d:Z

    goto :goto_0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lmaps/y/ab;->a_(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0, v1}, Lmaps/b/r;->d(Lmaps/t/bg;)V

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0, v1}, Lmaps/b/r;->a(Ljava/util/Set;)V

    return-void
.end method

.method public b(Lmaps/bq/d;)I
    .locals 5

    const/4 v1, 0x2

    const/4 v0, 0x0

    iget-object v2, p0, Lmaps/y/i;->j:Lmaps/y/ag;

    invoke-virtual {v2}, Lmaps/y/ag;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    iget-object v2, p0, Lmaps/y/i;->k:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    monitor-exit v2

    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/z;

    invoke-virtual {v0}, Lmaps/b/z;->f()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/i;->e:Z

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {v1}, Lmaps/bu/a;->c()V

    iget-object v1, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v1}, Lmaps/b/r;->b()V

    invoke-virtual {p0}, Lmaps/y/i;->h()V

    :cond_4
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lmaps/b/r;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/i;->e:Z

    return-void
.end method

.method b(Ljava/util/List;)Z
    .locals 10

    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lmaps/y/i;->e:Z

    if-eqz v0, :cond_8

    iput-boolean v3, p0, Lmaps/y/i;->e:Z

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/y/i;->j:Lmaps/y/ag;

    invoke-virtual {v0}, Lmaps/y/ag;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/p/j;->d:Lmaps/p/j;

    invoke-virtual {p0, v0}, Lmaps/y/i;->a(Lmaps/p/j;)Lmaps/p/av;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v5

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v1}, Lmaps/b/r;->i()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lmaps/y/i;->k:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v6, p0, Lmaps/y/i;->k:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/z;

    invoke-virtual {v0}, Lmaps/b/z;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lmaps/p/j;->h:Lmaps/p/j;

    new-array v7, v2, [Lmaps/p/z;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lmaps/y/i;->a(Lmaps/p/j;[Lmaps/p/z;)Lmaps/p/av;

    move-result-object v1

    :goto_1
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lmaps/b/z;->b()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_4

    invoke-virtual {v0}, Lmaps/b/z;->d()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lmaps/b/z;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    invoke-virtual {v0}, Lmaps/b/z;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_3

    sget-object v1, Lmaps/p/j;->f:Lmaps/p/j;

    invoke-static {v0}, Lmaps/f/ef;->a(Ljava/lang/Object;)Lmaps/f/ef;

    move-result-object v7

    iget-object v8, p0, Lmaps/y/i;->p:Lmaps/p/z;

    invoke-static {v8}, Lmaps/f/ef;->a(Ljava/lang/Object;)Lmaps/f/ef;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lmaps/y/i;->a(Lmaps/p/j;Ljava/util/Collection;Ljava/util/Collection;)Lmaps/p/av;

    move-result-object v1

    goto :goto_1

    :cond_3
    sget-object v1, Lmaps/p/j;->c:Lmaps/p/j;

    invoke-static {v0}, Lmaps/f/ef;->a(Ljava/lang/Object;)Lmaps/f/ef;

    move-result-object v7

    iget-object v8, p0, Lmaps/y/i;->p:Lmaps/p/z;

    invoke-static {v8}, Lmaps/f/ef;->a(Ljava/lang/Object;)Lmaps/f/ef;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lmaps/y/i;->a(Lmaps/p/j;Ljava/util/Collection;Ljava/util/Collection;)Lmaps/p/av;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lmaps/b/z;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_1

    invoke-virtual {v0}, Lmaps/b/z;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lmaps/b/z;->g()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    sget-object v1, Lmaps/p/j;->g:Lmaps/p/j;

    new-array v7, v2, [Lmaps/p/z;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lmaps/y/i;->a(Lmaps/p/j;[Lmaps/p/z;)Lmaps/p/av;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lmaps/p/j;->b:Lmaps/p/j;

    new-array v1, v2, [Lmaps/p/z;

    new-instance v6, Lmaps/y/aq;

    invoke-direct {v6, v4}, Lmaps/y/aq;-><init>(Ljava/util/Set;)V

    aput-object v6, v1, v3

    invoke-virtual {p0, v0, v1}, Lmaps/y/i;->a(Lmaps/p/j;[Lmaps/p/z;)Lmaps/p/av;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lmaps/p/j;->e:Lmaps/p/j;

    new-array v1, v2, [Lmaps/p/z;

    new-instance v4, Lmaps/y/aq;

    invoke-direct {v4, v5}, Lmaps/y/aq;-><init>(Ljava/util/Set;)V

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Lmaps/y/i;->a(Lmaps/p/j;[Lmaps/p/z;)Lmaps/p/av;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    move v0, v2

    :goto_2
    return v0

    :cond_8
    move v0, v3

    goto :goto_2
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0, p0}, Lmaps/b/r;->a(Lmaps/b/y;)V

    iget-object v0, p0, Lmaps/y/i;->h:Lmaps/b/s;

    invoke-virtual {v0, p0}, Lmaps/b/s;->a(Lmaps/b/c;)V

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/b/r;->j()Lmaps/b/u;

    move-result-object v0

    invoke-interface {v0, p0}, Lmaps/b/u;->a(Lmaps/b/g;)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0, p0}, Lmaps/b/r;->b(Lmaps/b/y;)V

    iget-object v0, p0, Lmaps/y/i;->h:Lmaps/b/s;

    invoke-virtual {v0, p0}, Lmaps/b/s;->b(Lmaps/b/c;)V

    iget-object v0, p0, Lmaps/y/i;->g:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/b/r;->j()Lmaps/b/u;

    move-result-object v0

    invoke-interface {v0, p0}, Lmaps/b/u;->b(Lmaps/b/g;)V

    return-void
.end method

.method public g()Lmaps/af/n;
    .locals 0

    return-object p0
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/y/i;->i:Lmaps/bu/a;

    invoke-virtual {v0}, Lmaps/bu/a;->b()V

    iget-object v0, p0, Lmaps/y/i;->a:Lmaps/af/f;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/af/f;->a(ZZ)V

    goto :goto_0
.end method

.method public i()Lmaps/bq/a;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public j()V
    .locals 0

    invoke-virtual {p0}, Lmaps/y/i;->h()V

    return-void
.end method
