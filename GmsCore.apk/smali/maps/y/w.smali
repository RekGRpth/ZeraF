.class Lmaps/y/w;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field final a:Lmaps/q/k;

.field final b:Ljava/util/List;

.field c:Lmaps/q/i;

.field d:Lmaps/bq/d;

.field e:Lmaps/t/bx;

.field f:F

.field final g:Lmaps/w/o;

.field h:Z

.field final i:Lmaps/t/aj;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/q/k;

    invoke-direct {v0}, Lmaps/q/k;-><init>()V

    iput-object v0, p0, Lmaps/y/w;->a:Lmaps/q/k;

    new-instance v0, Lmaps/w/i;

    invoke-direct {v0}, Lmaps/w/i;-><init>()V

    iput-object v0, p0, Lmaps/y/w;->g:Lmaps/w/o;

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/y/w;->i:Lmaps/t/aj;

    iput-object p1, p0, Lmaps/y/w;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method declared-synchronized a(Lmaps/bq/d;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/w;->d:Lmaps/bq/d;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/w;->h:Z

    iget-object v0, p0, Lmaps/y/w;->c:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/w;->c:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/q/i;)V
    .locals 1

    iput-object p1, p0, Lmaps/y/w;->c:Lmaps/q/i;

    sget-object v0, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {p1, p0, v0}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    return-void
.end method

.method declared-synchronized a(Lmaps/t/bx;I)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lmaps/y/w;->g:Lmaps/w/o;

    new-instance v2, Lmaps/t/aj;

    invoke-direct {v2, p1, v0, p2}, Lmaps/t/aj;-><init>(Lmaps/t/bx;FI)V

    invoke-interface {v1, v2}, Lmaps/w/o;->b(Lmaps/t/aj;)V

    iget-object v0, p0, Lmaps/y/w;->c:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/w;->c:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/q/i;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/w;->g:Lmaps/w/o;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lmaps/w/o;->a(J)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/w;->h:Z

    :cond_0
    iget-boolean v0, p0, Lmaps/y/w;->h:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/w;->h:Z

    iget-object v0, p0, Lmaps/y/w;->g:Lmaps/w/o;

    iget-object v1, p0, Lmaps/y/w;->i:Lmaps/t/aj;

    invoke-interface {v0, v1}, Lmaps/w/o;->a(Lmaps/t/aj;)Z

    iget-object v0, p0, Lmaps/y/w;->i:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/w;->e:Lmaps/t/bx;

    iget-object v0, p0, Lmaps/y/w;->i:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/y/w;->e:Lmaps/t/bx;

    invoke-static {v0, v1}, Lmaps/y/aw;->a(ILmaps/t/bx;)F

    move-result v0

    iput v0, p0, Lmaps/y/w;->f:F

    iget-object v0, p0, Lmaps/y/w;->d:Lmaps/bq/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/w;->e:Lmaps/t/bx;

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/y/w;->d:Lmaps/bq/d;

    iget-object v3, p0, Lmaps/y/w;->e:Lmaps/t/bx;

    iget v4, p0, Lmaps/y/w;->f:F

    invoke-static {v1, v2, v3, v4, v0}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    iget-object v1, p0, Lmaps/y/w;->a:Lmaps/q/k;

    invoke-virtual {v1}, Lmaps/q/k;->a()Lmaps/q/k;

    iget-object v1, p0, Lmaps/y/w;->a:Lmaps/q/k;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x2

    aget v4, v0, v4

    invoke-virtual {v1, v2, v3, v4}, Lmaps/q/k;->a(FFF)Lmaps/q/k;

    iget-object v1, p0, Lmaps/y/w;->a:Lmaps/q/k;

    const/4 v2, 0x3

    aget v0, v0, v2

    invoke-virtual {v1, v0}, Lmaps/q/k;->a(F)Lmaps/q/k;

    iget-object v0, p0, Lmaps/y/w;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ba;

    iget-object v2, p0, Lmaps/y/w;->a:Lmaps/q/k;

    invoke-interface {v0, v2}, Lmaps/q/be;->a(Lmaps/q/k;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/y/w;->c:Lmaps/q/i;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/w;->c:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-void
.end method
