.class public Lmaps/y/bo;
.super Lmaps/p/s;

# interfaces
.implements Lmaps/p/am;
.implements Lmaps/y/aa;


# instance fields
.field protected a:Lmaps/y/q;

.field private final b:Lmaps/y/bj;

.field private final c:Landroid/content/res/Resources;

.field private d:Lmaps/af/v;

.field private e:Lmaps/d/v;

.field private f:Lmaps/y/r;

.field private g:Lmaps/y/at;

.field private h:Lmaps/y/bf;

.field private i:Lmaps/y/bc;

.field private j:Z

.field private k:Lmaps/bq/a;

.field private l:J

.field private m:Z

.field private n:Lmaps/p/au;

.field private o:Lmaps/q/ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 2

    invoke-direct {p0, p1}, Lmaps/p/s;-><init>(Landroid/content/Context;)V

    new-instance v0, Lmaps/y/bj;

    invoke-direct {v0}, Lmaps/y/bj;-><init>()V

    iput-object v0, p0, Lmaps/y/bo;->b:Lmaps/y/bj;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lmaps/y/bo;->l:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/bo;->m:Z

    iput-object p2, p0, Lmaps/y/bo;->c:Landroid/content/res/Resources;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmaps/y/bo;->a(Lmaps/q/ad;)V

    return-void
.end method

.method private a(Lmaps/q/ad;)V
    .locals 13

    iput-object p1, p0, Lmaps/y/bo;->o:Lmaps/q/ad;

    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/y/bo;->b(Z)V

    :cond_0
    new-instance v0, Lmaps/y/bf;

    invoke-direct {v0, p0}, Lmaps/y/bf;-><init>(Lmaps/y/aa;)V

    iput-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    new-instance v0, Lmaps/d/v;

    invoke-direct {v0}, Lmaps/d/v;-><init>()V

    iput-object v0, p0, Lmaps/y/bo;->e:Lmaps/d/v;

    iget-object v0, p0, Lmaps/y/bo;->e:Lmaps/d/v;

    invoke-virtual {p0}, Lmaps/y/bo;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0, v1, v2}, Lmaps/d/v;->a(Landroid/content/Context;Lmaps/d/m;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/y/bo;->setFocusable(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/y/bo;->setClickable(Z)V

    iget-object v0, p0, Lmaps/y/bo;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v10, v0, Landroid/util/DisplayMetrics;->density:F

    const/16 v11, 0x100

    const/16 v12, 0x100

    new-instance v0, Lmaps/p/au;

    invoke-direct {v0, p0}, Lmaps/p/au;-><init>(Lmaps/p/am;)V

    iput-object v0, p0, Lmaps/y/bo;->n:Lmaps/p/au;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v7

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/16 v6, 0x8

    invoke-static {}, Lmaps/bm/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    const/16 v2, 0x8

    const/16 v3, 0x8

    new-instance v0, Lmaps/y/ai;

    invoke-direct/range {v0 .. v6}, Lmaps/y/ai;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v1, 0x5

    const/4 v2, 0x6

    const/4 v3, 0x5

    new-instance v0, Lmaps/y/ai;

    invoke-direct/range {v0 .. v6}, Lmaps/y/ai;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x0

    new-instance v0, Lmaps/y/ai;

    invoke-direct/range {v0 .. v6}, Lmaps/y/ai;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/ai;

    sget-boolean v2, Lmaps/ae/h;->t:Z

    invoke-virtual {v0, v2}, Lmaps/y/ai;->a(Z)V

    goto :goto_0

    :cond_2
    new-instance v1, Lmaps/y/bg;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lmaps/y/ai;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/y/ai;

    invoke-direct {v1, v0}, Lmaps/y/bg;-><init>([Lmaps/y/ai;)V

    invoke-virtual {p0, v1}, Lmaps/y/bo;->a(Lmaps/p/e;)V

    const/4 v6, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_5

    if-nez p1, :cond_8

    new-instance v9, Lmaps/q/ad;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/q/al;

    const/4 v1, 0x0

    new-instance v2, Lmaps/q/bi;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lmaps/q/bi;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lmaps/q/bi;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lmaps/q/bi;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lmaps/q/bi;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lmaps/q/bi;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lmaps/q/ae;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Lmaps/q/ae;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lmaps/q/u;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lmaps/q/u;-><init>(I)V

    aput-object v2, v0, v1

    invoke-direct {v9, v0}, Lmaps/q/ad;-><init>([Lmaps/q/al;)V

    :goto_1
    new-instance v6, Lmaps/q/g;

    invoke-direct {v6, v9}, Lmaps/q/g;-><init>(Lmaps/q/ad;)V

    const/16 v0, 0x10

    new-array v8, v0, [F

    const/4 v0, 0x0

    invoke-static {v8, v0}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    new-instance v0, Lmaps/bq/d;

    sget-object v1, Lmaps/bq/d;->d:Lmaps/bq/a;

    const/4 v5, 0x0

    const/16 v7, 0x1c

    move v2, v11

    move v3, v12

    move v4, v10

    invoke-direct/range {v0 .. v9}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIFLjava/lang/Thread;Lmaps/q/av;I[FLmaps/q/ad;)V

    new-instance v1, Lmaps/q/k;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    invoke-direct {v1, v2, v3, v4}, Lmaps/q/k;-><init>(FFF)V

    invoke-virtual {v0, v1}, Lmaps/bq/d;->a(Lmaps/q/k;)V

    sget-boolean v1, Lmaps/ae/h;->C:Z

    if-eqz v1, :cond_3

    new-instance v1, Lmaps/q/ax;

    invoke-direct {v1, v0}, Lmaps/q/ax;-><init>(Lmaps/q/h;)V

    invoke-virtual {v9, v1}, Lmaps/q/ad;->a(Lmaps/q/b;)V

    :cond_3
    invoke-virtual {v9, v0}, Lmaps/q/ad;->a(Lmaps/q/h;)V

    move-object v3, v0

    move-object v5, v9

    :goto_2
    invoke-virtual {p0}, Lmaps/y/bo;->s()Lmaps/o/c;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/bo;->c:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lmaps/y/ab;->a(Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/ab;

    move-result-object v4

    new-instance v0, Lmaps/af/v;

    iget-object v1, p0, Lmaps/y/bo;->n:Lmaps/p/au;

    iget-object v2, p0, Lmaps/y/bo;->c:Landroid/content/res/Resources;

    invoke-direct/range {v0 .. v6}, Lmaps/af/v;-><init>(Lmaps/p/au;Landroid/content/res/Resources;Lmaps/bq/d;Lmaps/y/ab;Lmaps/q/ad;Lmaps/q/g;)V

    iput-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lmaps/y/bo;->a(I)V

    :cond_4
    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lmaps/ae/h;->T:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lmaps/ae/h;->S:Z

    if-nez v0, :cond_6

    :goto_3
    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {p0, v0}, Lmaps/y/bo;->a(Lmaps/af/v;)V

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-boolean v0, Lmaps/ae/h;->U:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/y/bo;->b(I)V

    :goto_4
    return-void

    :cond_5
    const/4 v9, 0x0

    new-instance v0, Lmaps/bq/d;

    sget-object v1, Lmaps/bq/d;->d:Lmaps/bq/a;

    const/4 v5, 0x0

    move v2, v11

    move v3, v12

    move v4, v10

    invoke-direct/range {v0 .. v5}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIFLjava/lang/Thread;)V

    move-object v3, v0

    move-object v5, v9

    goto :goto_2

    :cond_6
    new-instance v0, Lmaps/y/c;

    invoke-direct {v0, p0}, Lmaps/y/c;-><init>(Lmaps/y/bo;)V

    invoke-virtual {p0, v0}, Lmaps/y/bo;->a(Lmaps/p/as;)V

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/y/bo;->b(I)V

    goto :goto_4

    :cond_8
    move-object v9, p1

    goto/16 :goto_1
.end method


# virtual methods
.method public a(Lmaps/y/am;)Lmaps/y/k;
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->a(Lmaps/y/am;)Lmaps/y/k;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Lmaps/y/x;
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->b(Z)Lmaps/y/x;

    move-result-object v0

    return-object v0
.end method

.method public a(FF)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    invoke-interface {v0, p0, p1, p2}, Lmaps/y/r;->a(Lmaps/y/bo;FF)V

    :cond_0
    return-void
.end method

.method public a(FFF)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    invoke-interface {v0, p0, p1, p2, p3}, Lmaps/y/r;->a(Lmaps/y/bo;FFF)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/af/e;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->a(Lmaps/af/e;)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/af/q;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->b(Lmaps/af/q;)V

    return-void
.end method

.method public a(Lmaps/cf/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->a(Lmaps/cf/a;)V

    return-void
.end method

.method public a(Lmaps/y/ab;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->a(Lmaps/y/ab;)V

    return-void
.end method

.method public a(Lmaps/y/bc;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->a(Lmaps/y/bc;)V

    return-void
.end method

.method public a(Lmaps/y/bh;Lmaps/y/ah;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/bo;->j:Z

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1, p2}, Lmaps/af/v;->a(Lmaps/y/bh;Lmaps/y/ah;)V

    return-void
.end method

.method public a(Lmaps/y/q;)V
    .locals 2

    iput-object p1, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    iget-object v1, p0, Lmaps/y/bo;->n:Lmaps/p/au;

    invoke-virtual {v0, v1}, Lmaps/y/q;->a(Lmaps/af/f;)V

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, v1}, Lmaps/y/q;->a(Lmaps/ax/c;)V

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    iget-object v1, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v0, v1}, Lmaps/af/v;->a(Lmaps/af/n;)V

    return-void
.end method

.method public a(Lmaps/y/r;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    return-void
.end method

.method public a(ZZ)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->n:Lmaps/p/au;

    invoke-virtual {v0, p1, p2}, Lmaps/p/au;->a(ZZ)V

    return-void
.end method

.method public a(Landroid/view/MotionEvent;FF)Z
    .locals 10

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->j()Lmaps/u/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/u/a;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v0, v4, v5, v1}, Lmaps/u/a;->b(FFLmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/y/bo;->q()V

    move v0, v3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->f()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_1
    if-ltz v4, :cond_4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    invoke-virtual {v0}, Lmaps/y/bc;->c()Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez v1, :cond_2

    new-instance v1, Lmaps/bq/d;

    iget-object v6, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v6}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v6

    invoke-virtual {p0}, Lmaps/y/bo;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lmaps/y/bo;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lmaps/y/bo;->r()F

    move-result v9

    invoke-direct {v1, v6, v7, v8, v9}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {v0, v6, v7, v1}, Lmaps/y/bc;->b(FFLmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lmaps/y/bo;->q()V

    move v0, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public b()V
    .locals 2

    invoke-virtual {p0}, Lmaps/y/bo;->i()V

    iget-object v0, p0, Lmaps/y/bo;->n:Lmaps/p/au;

    invoke-virtual {v0}, Lmaps/p/au;->g()V

    invoke-super {p0}, Lmaps/p/s;->b()V

    invoke-static {}, Lmaps/ca/a;->a()Lmaps/ca/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v1}, Lmaps/af/v;->l()Lmaps/cr/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ca/a;->b(Lmaps/cr/c;)V

    :cond_0
    return-void
.end method

.method public b(Lmaps/y/bc;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->b(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/y/bo;->i:Lmaps/y/bc;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lmaps/y/bo;->i()V

    :cond_0
    return-void
.end method

.method public b(FF)Z
    .locals 5

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/bq/d;

    iget-object v1, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v1}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/y/bo;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lmaps/y/bo;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lmaps/y/bo;->r()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    invoke-virtual {v0, p1, p2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v2}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v1, v0}, Lmaps/y/f;->a_(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    invoke-super {p0}, Lmaps/p/s;->c()V

    iget-object v0, p0, Lmaps/y/bo;->n:Lmaps/p/au;

    invoke-virtual {v0}, Lmaps/p/au;->f()V

    return-void
.end method

.method public c(FF)V
    .locals 10

    const/4 v7, 0x1

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Lmaps/bq/d;

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/y/bo;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lmaps/y/bo;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lmaps/y/bo;->r()F

    move-result v3

    invoke-direct {v4, v0, v1, v2, v3}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    invoke-virtual {v4, p1, p2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v3

    const/4 v0, 0x0

    iget-boolean v2, p0, Lmaps/y/bo;->j:Z

    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v1}, Lmaps/af/v;->j()Lmaps/u/a;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->j()Lmaps/u/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v3, v4}, Lmaps/u/a;->a(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    :cond_1
    if-nez v0, :cond_2

    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v1}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v3, v4}, Lmaps/y/f;->a(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    :cond_2
    iget-object v1, p0, Lmaps/y/bo;->g:Lmaps/y/at;

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/y/bo;->g:Lmaps/y/at;

    invoke-interface {v0, p0, v3}, Lmaps/y/at;->a(Lmaps/y/bo;Lmaps/t/bx;)Z

    move-result v0

    :cond_3
    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v1}, Lmaps/af/v;->f()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v1, v1, -0x1

    move v6, v0

    :goto_1
    if-nez v6, :cond_5

    if-ltz v1, :cond_5

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    invoke-virtual {v0}, Lmaps/y/bc;->f_()Z

    move-result v9

    if-eqz v9, :cond_4

    check-cast v0, Lmaps/y/ba;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v6

    :goto_2
    add-int/lit8 v1, v1, -0x1

    move v6, v0

    goto :goto_1

    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v9, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v9}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v9

    if-eq v0, v9, :cond_a

    invoke-virtual {v0, p1, p2, v3, v4}, Lmaps/y/bc;->a(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v7

    goto :goto_2

    :cond_5
    if-nez v6, :cond_8

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    if-eqz v2, :cond_6

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/bo;->k:Lmaps/bq/a;

    invoke-virtual {v0, v1}, Lmaps/bq/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    invoke-virtual {p0, v7}, Lmaps/y/bo;->i(Z)V

    :cond_7
    iget-object v0, p0, Lmaps/y/bo;->b:Lmaps/y/bj;

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lmaps/y/bj;->a(FFLmaps/t/bx;Lmaps/bq/d;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v6, v7

    :cond_8
    if-nez v6, :cond_9

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    invoke-interface {v0, p0, v3}, Lmaps/y/r;->a(Lmaps/y/bo;Lmaps/t/bx;)V

    :cond_9
    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bo;->k:Lmaps/bq/a;

    invoke-virtual {p0}, Lmaps/y/bo;->j_()V

    goto/16 :goto_0

    :cond_a
    move v0, v6

    goto :goto_2
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0, p1}, Lmaps/y/bf;->a(Z)V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->c()V

    return-void
.end method

.method public d(FF)V
    .locals 7

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Lmaps/bq/d;

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/y/bo;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lmaps/y/bo;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lmaps/y/bo;->r()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    invoke-virtual {v3, p1, p2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v4

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v1}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v4, v3}, Lmaps/y/f;->d(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    :cond_1
    iget-object v1, p0, Lmaps/y/bo;->g:Lmaps/y/at;

    if-eqz v1, :cond_5

    if-nez v0, :cond_5

    iget-object v0, p0, Lmaps/y/bo;->g:Lmaps/y/at;

    invoke-interface {v0, p0, v4}, Lmaps/y/at;->b(Lmaps/y/bo;Lmaps/t/bx;)Z

    move-result v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->f()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    iget-object v6, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v6}, Lmaps/af/v;->i()Lmaps/y/f;

    move-result-object v6

    if-eq v0, v6, :cond_4

    invoke-virtual {v0, p1, p2, v4, v3}, Lmaps/y/bc;->d(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_2
    if-nez v1, :cond_3

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/bo;->f:Lmaps/y/r;

    invoke-interface {v0, p0, v4}, Lmaps/y/r;->b(Lmaps/y/bo;Lmaps/t/bx;)V

    :cond_3
    invoke-virtual {p0}, Lmaps/y/bo;->j_()V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0, p1}, Lmaps/y/bf;->b(Z)V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/p/s;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lmaps/y/bo;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/bo;->m:Z

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->d()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/y/bo;->l:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-object v2, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmaps/af/v;->c(Z)V

    :goto_0
    iput-wide v0, p0, Lmaps/y/bo;->l:J

    return-void

    :cond_0
    iget-object v2, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmaps/af/v;->c(Z)V

    goto :goto_0
.end method

.method public e(FF)V
    .locals 5

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Lmaps/bq/d;

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v0}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/y/bo;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lmaps/y/bo;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lmaps/y/bo;->r()F

    move-result v4

    invoke-direct {v2, v0, v1, v3, v4}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->f()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    invoke-virtual {v0, p1, p2, v2}, Lmaps/y/bc;->a(FFLmaps/bq/d;)Z

    move-result v4

    if-eqz v4, :cond_2

    iput-object v0, p0, Lmaps/y/bo;->i:Lmaps/y/bc;

    invoke-virtual {p0}, Lmaps/y/bo;->j_()V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1
.end method

.method public e(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0, p1}, Lmaps/y/bf;->c(Z)V

    return-void
.end method

.method public f()Lmaps/y/q;
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    return-object v0
.end method

.method public f(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0, p1}, Lmaps/y/bf;->d(Z)V

    return-void
.end method

.method public f(FF)Z
    .locals 10

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    if-nez v0, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->f()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v2}, Lmaps/af/v;->j()Lmaps/u/a;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/u/a;->c()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2, p1, p2, v1, v1}, Lmaps/u/a;->b(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lmaps/y/bo;->q()V

    move v0, v4

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    move v5, v0

    move-object v2, v1

    :goto_1
    if-ltz v5, :cond_4

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    invoke-virtual {v0}, Lmaps/y/bc;->c()Z

    move-result v7

    if-eqz v7, :cond_3

    if-nez v2, :cond_2

    new-instance v2, Lmaps/bq/d;

    iget-object v1, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v1}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/y/bo;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lmaps/y/bo;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lmaps/y/bo;->r()F

    move-result v9

    invoke-direct {v2, v1, v7, v8, v9}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    invoke-virtual {v2, p1, p2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, p1, p2, v1, v2}, Lmaps/y/bc;->b(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v4

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public g(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0, p1}, Lmaps/af/v;->d(Z)V

    return-void
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->c:Landroid/content/res/Resources;

    return-object v0
.end method

.method public h()Lmaps/bq/d;
    .locals 5

    new-instance v0, Lmaps/bq/d;

    iget-object v1, p0, Lmaps/y/bo;->a:Lmaps/y/q;

    invoke-virtual {v1}, Lmaps/y/q;->b()Lmaps/bq/a;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/y/bo;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lmaps/y/bo;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lmaps/y/bo;->r()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIF)V

    return-object v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->i:Lmaps/y/bc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bo;->i:Lmaps/y/bc;

    invoke-virtual {v0}, Lmaps/y/bc;->a_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/bo;->i:Lmaps/y/bc;

    invoke-virtual {p0}, Lmaps/y/bo;->j_()V

    :cond_0
    return-void
.end method

.method public i(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->b:Lmaps/y/bj;

    invoke-virtual {v0, p1}, Lmaps/y/bj;->a(Z)V

    return-void
.end method

.method public isOpaque()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public j()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/bo;->j:Z

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    invoke-virtual {v0}, Lmaps/af/v;->h()V

    return-void
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0}, Lmaps/y/bf;->a()Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0}, Lmaps/y/bf;->b()Z

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0}, Lmaps/y/bf;->c()Z

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->h:Lmaps/y/bf;

    invoke-virtual {v0}, Lmaps/y/bf;->d()Z

    move-result v0

    return v0
.end method

.method public onFinishInflate()V
    .locals 0

    invoke-super {p0}, Lmaps/p/s;->onFinishInflate()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lmaps/y/bo;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/y/bo;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bo;->e:Lmaps/d/v;

    invoke-virtual {v0, p1}, Lmaps/d/v;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    invoke-super {p0, p1}, Lmaps/p/s;->onWindowFocusChanged(Z)V

    iget-object v1, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lmaps/af/v;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Lmaps/af/v;
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->d:Lmaps/af/v;

    return-object v0
.end method

.method public q()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lmaps/y/bo;->a(ZZ)V

    return-void
.end method

.method public r()F
    .locals 1

    iget-object v0, p0, Lmaps/y/bo;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method public s()Lmaps/o/c;
    .locals 1

    sget-object v0, Lmaps/o/c;->a:Lmaps/o/c;

    return-object v0
.end method
