.class Lmaps/y/ao;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field private final a:Lmaps/q/k;

.field private final b:Lmaps/q/ba;

.field private final c:Lmaps/q/bg;

.field private d:Z

.field private final e:[F

.field private f:F

.field private g:Z

.field private final h:I

.field private i:Lmaps/q/i;

.field private final j:Lmaps/w/p;

.field private k:Lmaps/w/q;

.field private l:F

.field private volatile m:Z


# direct methods
.method public constructor <init>(Lmaps/q/ba;Lmaps/q/bg;I)V
    .locals 8

    const-wide/16 v1, 0x0

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/q/k;

    invoke-direct {v0}, Lmaps/q/k;-><init>()V

    iput-object v0, p0, Lmaps/y/ao;->a:Lmaps/q/k;

    iput-boolean v7, p0, Lmaps/y/ao;->d:Z

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/y/ao;->e:[F

    iput v6, p0, Lmaps/y/ao;->f:F

    new-instance v0, Lmaps/w/p;

    sget-object v5, Lmaps/w/q;->b:Lmaps/w/q;

    move-wide v3, v1

    invoke-direct/range {v0 .. v5}, Lmaps/w/p;-><init>(JJLmaps/w/q;)V

    iput-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    sget-object v0, Lmaps/w/q;->b:Lmaps/w/q;

    iput-object v0, p0, Lmaps/y/ao;->k:Lmaps/w/q;

    iput v6, p0, Lmaps/y/ao;->l:F

    iput-boolean v7, p0, Lmaps/y/ao;->m:Z

    iput-object p1, p0, Lmaps/y/ao;->b:Lmaps/q/ba;

    iput-object p2, p0, Lmaps/y/ao;->c:Lmaps/q/bg;

    iput p3, p0, Lmaps/y/ao;->h:I

    return-void
.end method

.method private a([F[F)Z
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    aget v2, p1, v1

    aget v3, p2, v1

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(IILmaps/w/q;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/ao;->k:Lmaps/w/q;

    if-eq v0, p3, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/ao;->m:Z

    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    invoke-virtual {v0}, Lmaps/w/p;->reset()V

    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p3, v1, v2}, Lmaps/w/p;->a(Lmaps/w/q;FF)V

    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lmaps/w/p;->setDuration(J)V

    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lmaps/w/p;->setStartOffset(J)V

    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    invoke-virtual {v0}, Lmaps/w/p;->start()V

    iput-object p3, p0, Lmaps/y/ao;->k:Lmaps/w/q;

    iget-object v0, p0, Lmaps/y/ao;->i:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/q/i;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/ao;->i:Lmaps/q/i;

    sget-object v0, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {p1, p0, v0}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/y/ao;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a([FF)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lmaps/y/ao;->f:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/ao;->e:[F

    invoke-direct {p0, p1, v0}, Lmaps/y/ao;->a([F[F)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/y/ao;->d:Z

    iget-object v0, p0, Lmaps/y/ao;->a:Lmaps/q/k;

    invoke-virtual {v0}, Lmaps/q/k;->a()Lmaps/q/k;

    iget-object v0, p0, Lmaps/y/ao;->a:Lmaps/q/k;

    const/4 v1, 0x0

    aget v1, p1, v1

    const/4 v2, 0x1

    aget v2, p1, v2

    const/4 v3, 0x2

    aget v3, p1, v3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/q/k;->a(FFF)Lmaps/q/k;

    iget-object v0, p0, Lmaps/y/ao;->a:Lmaps/q/k;

    invoke-virtual {v0, p2}, Lmaps/q/k;->a(F)Lmaps/q/k;

    iget-object v0, p0, Lmaps/y/ao;->i:Lmaps/q/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/ao;->i:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    iget-object v0, p0, Lmaps/y/ao;->e:[F

    const/4 v1, 0x3

    invoke-static {p1, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput p2, p0, Lmaps/y/ao;->f:F

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/q/i;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/y/ao;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/ao;->b:Lmaps/q/ba;

    iget-object v1, p0, Lmaps/y/ao;->a:Lmaps/q/k;

    invoke-virtual {v0, v1}, Lmaps/q/ba;->a(Lmaps/q/k;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/ao;->d:Z

    :cond_0
    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    invoke-virtual {v0}, Lmaps/w/p;->hasEnded()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/y/ao;->m:Z

    iget-boolean v0, p0, Lmaps/y/ao;->m:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/w/p;->b(J)F

    move-result v0

    iput v0, p0, Lmaps/y/ao;->l:F

    :cond_1
    iget-boolean v0, p0, Lmaps/y/ao;->g:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/ao;->c:Lmaps/q/bg;

    iget v1, p0, Lmaps/y/ao;->h:I

    invoke-interface {v0, v1}, Lmaps/q/bg;->a(I)V

    :goto_0
    iget-object v0, p0, Lmaps/y/ao;->j:Lmaps/w/p;

    invoke-virtual {v0}, Lmaps/w/p;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/y/ao;->i:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    iget-object v0, p0, Lmaps/y/ao;->c:Lmaps/q/bg;

    iget v1, p0, Lmaps/y/ao;->l:F

    iget v2, p0, Lmaps/y/ao;->l:F

    iget v3, p0, Lmaps/y/ao;->l:F

    iget v4, p0, Lmaps/y/ao;->l:F

    invoke-interface {v0, v1, v2, v3, v4}, Lmaps/q/bg;->a(FFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/ao;->m:Z

    return v0
.end method
