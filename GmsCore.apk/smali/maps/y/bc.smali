.class public abstract Lmaps/y/bc;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/t;


# instance fields
.field private a:Lmaps/y/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a(Ljavax/microedition/khronos/opengles/GL10;I)V
    .locals 5

    const v4, 0xff00

    shr-int/lit8 v0, p1, 0x10

    and-int/2addr v0, v4

    shr-int/lit8 v1, p1, 0x8

    and-int/2addr v1, v4

    and-int v2, p1, v4

    shl-int/lit8 v3, p1, 0x8

    and-int/2addr v3, v4

    invoke-interface {p0, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method


# virtual methods
.method protected a(Lmaps/p/j;)Lmaps/p/av;
    .locals 2

    new-instance v0, Lmaps/p/av;

    const/4 v1, 0x0

    new-array v1, v1, [Lmaps/p/z;

    invoke-direct {v0, p0, p1, v1}, Lmaps/p/av;-><init>(Lmaps/y/bc;Lmaps/p/j;[Lmaps/p/z;)V

    return-object v0
.end method

.method protected a(Lmaps/p/j;Ljava/util/Collection;Ljava/util/Collection;)Lmaps/p/av;
    .locals 1

    new-instance v0, Lmaps/p/av;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/p/av;-><init>(Lmaps/y/bc;Lmaps/p/j;Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v0
.end method

.method protected varargs a(Lmaps/p/j;[Lmaps/p/z;)Lmaps/p/av;
    .locals 1

    new-instance v0, Lmaps/p/av;

    invoke-direct {v0, p0, p1, p2}, Lmaps/p/av;-><init>(Lmaps/y/bc;Lmaps/p/j;[Lmaps/p/z;)V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/y/bc;->c(Lmaps/cr/c;)V

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 0

    return-void
.end method

.method public abstract a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
.end method

.method public a(Lmaps/y/p;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/bc;->a:Lmaps/y/p;

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(FFLmaps/bq/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/util/List;)Z
    .locals 1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/y/bc;->b_()Lmaps/p/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/y/bc;->a(Lmaps/p/j;)Lmaps/p/av;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a_()V
    .locals 0

    return-void
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public a_(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract b()Lmaps/y/am;
.end method

.method public b(Lmaps/cr/c;)V
    .locals 3

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xxx GLOverlay["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "].initializeGL2()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public b(FFLmaps/bq/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected b_()Lmaps/p/j;
    .locals 1

    sget-object v0, Lmaps/p/j;->i:Lmaps/p/j;

    return-object v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e_()V
    .locals 0

    return-void
.end method

.method protected f()V
    .locals 1

    iget-object v0, p0, Lmaps/y/bc;->a:Lmaps/y/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bc;->a:Lmaps/y/p;

    invoke-interface {v0, p0}, Lmaps/y/p;->a(Lmaps/y/bc;)V

    :cond_0
    return-void
.end method

.method public f_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public g()Lmaps/af/n;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
