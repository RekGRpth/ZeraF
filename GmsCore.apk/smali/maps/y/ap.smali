.class Lmaps/y/ap;
.super Lmaps/ax/a;


# instance fields
.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:I

.field private final f:J

.field private final g:Lmaps/ax/b;

.field private h:Lmaps/ax/d;


# direct methods
.method protected constructor <init>(Lmaps/bq/a;Lmaps/ax/b;FFFI)V
    .locals 2

    invoke-direct {p0, p1}, Lmaps/ax/a;-><init>(Lmaps/bq/a;)V

    iput-object p2, p0, Lmaps/y/ap;->g:Lmaps/ax/b;

    iput p3, p0, Lmaps/y/ap;->b:F

    iput p4, p0, Lmaps/y/ap;->c:F

    iput p5, p0, Lmaps/y/ap;->d:F

    iput p6, p0, Lmaps/y/ap;->e:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/y/ap;->f:J

    return-void
.end method


# virtual methods
.method public a(Lmaps/bq/d;)Lmaps/bq/b;
    .locals 7

    iget-object v0, p0, Lmaps/y/ap;->a:Lmaps/bq/a;

    iget-object v2, p0, Lmaps/y/ap;->g:Lmaps/ax/b;

    iget v3, p0, Lmaps/y/ap;->b:F

    iget v4, p0, Lmaps/y/ap;->c:F

    iget v5, p0, Lmaps/y/ap;->d:F

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lmaps/y/q;->a(Lmaps/bq/a;Lmaps/bq/d;Lmaps/ax/b;FFF)Lmaps/bq/a;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v3, p0, Lmaps/y/ap;->f:J

    sub-long/2addr v0, v3

    long-to-int v3, v0

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v0, Lmaps/y/ad;

    iget-object v1, p0, Lmaps/y/ap;->a:Lmaps/bq/a;

    iget v6, p0, Lmaps/y/ap;->e:I

    sub-int v3, v6, v3

    invoke-direct/range {v0 .. v5}, Lmaps/y/ad;-><init>(Lmaps/bq/a;Lmaps/bq/b;IZF)V

    iput-object v0, p0, Lmaps/y/ap;->h:Lmaps/ax/d;

    iget-object v0, p0, Lmaps/y/ap;->h:Lmaps/ax/d;

    invoke-interface {v0, p1}, Lmaps/ax/d;->a(Lmaps/bq/d;)Lmaps/bq/b;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lmaps/y/ap;->h:Lmaps/ax/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/y/ap;->h:Lmaps/ax/d;

    invoke-interface {v0}, Lmaps/ax/d;->c()I

    move-result v0

    goto :goto_0
.end method
