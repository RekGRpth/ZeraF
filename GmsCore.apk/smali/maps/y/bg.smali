.class public Lmaps/y/bg;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/p/e;


# instance fields
.field private a:[I

.field private final b:[Lmaps/y/ai;


# direct methods
.method public constructor <init>([Lmaps/y/ai;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/y/bg;->a:[I

    iput-object p1, p0, Lmaps/y/bg;->b:[Lmaps/y/ai;

    return-void
.end method

.method private a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 2

    iget-object v0, p0, Lmaps/y/bg;->a:[I

    invoke-interface {p1, p2, p3, p4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bg;->a:[I

    const/4 v1, 0x0

    aget p5, v0, v1

    :cond_0
    return p5
.end method

.method private a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;Lmaps/y/ai;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 11

    const/4 v5, 0x0

    array-length v7, p3

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_1

    aget-object v3, p3, v6

    const/16 v4, 0x3025

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmaps/y/bg;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    const/16 v4, 0x3026

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmaps/y/bg;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    invoke-static {p4}, Lmaps/y/ai;->a(Lmaps/y/ai;)I

    move-result v1

    if-lt v8, v1, :cond_0

    invoke-static {p4}, Lmaps/y/ai;->b(Lmaps/y/ai;)I

    move-result v1

    if-lt v0, v1, :cond_0

    const/16 v4, 0x3024

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmaps/y/bg;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    const/16 v4, 0x3023

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmaps/y/bg;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    const/16 v4, 0x3022

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmaps/y/bg;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v10

    const/16 v4, 0x3021

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmaps/y/bg;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    invoke-static {p4}, Lmaps/y/ai;->c(Lmaps/y/ai;)I

    move-result v1

    if-ne v8, v1, :cond_0

    invoke-static {p4}, Lmaps/y/ai;->d(Lmaps/y/ai;)I

    move-result v1

    if-ne v9, v1, :cond_0

    invoke-static {p4}, Lmaps/y/ai;->e(Lmaps/y/ai;)I

    move-result v1

    if-ne v10, v1, :cond_0

    invoke-static {p4}, Lmaps/y/ai;->f(Lmaps/y/ai;)I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_1
    return-object v3

    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 13

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    new-array v5, v6, [I

    new-instance v0, Lmaps/y/ai;

    iget-object v1, p0, Lmaps/y/bg;->b:[Lmaps/y/ai;

    aget-object v1, v1, v4

    invoke-direct {v0, v1}, Lmaps/y/ai;-><init>(Lmaps/y/ai;)V

    invoke-virtual {v0, v6}, Lmaps/y/ai;->a(Z)V

    invoke-virtual {v0}, Lmaps/y/ai;->a()[I

    move-result-object v2

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    aget v0, v5, v4

    if-lez v0, :cond_1

    move v0, v6

    :goto_0
    invoke-static {v0}, Lmaps/bm/b;->a(Z)V

    move v12, v4

    move-object v6, v3

    :goto_1
    iget-object v0, p0, Lmaps/y/bg;->b:[Lmaps/y/ai;

    aget-object v0, v0, v12

    invoke-virtual {v0}, Lmaps/y/ai;->a()[I

    move-result-object v2

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v4

    goto :goto_0

    :cond_2
    aget v10, v5, v4

    if-lez v10, :cond_6

    new-array v9, v10, [Ljavax/microedition/khronos/egl/EGLConfig;

    move-object v6, p1

    move-object v7, p2

    move-object v8, v2

    move-object v11, v5

    invoke-interface/range {v6 .. v11}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig#2 failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lmaps/y/bg;->b:[Lmaps/y/ai;

    aget-object v0, v0, v12

    invoke-direct {p0, p1, p2, v9, v0}, Lmaps/y/bg;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;Lmaps/y/ai;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v1

    :goto_2
    if-nez v1, :cond_4

    add-int/lit8 v0, v12, 0x1

    iget-object v2, p0, Lmaps/y/bg;->b:[Lmaps/y/ai;

    array-length v2, v2

    if-lt v0, v2, :cond_7

    :cond_4
    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No config chosen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v1

    :cond_6
    move-object v1, v6

    goto :goto_2

    :cond_7
    move v12, v0

    move-object v6, v1

    goto :goto_1
.end method
