.class public Lmaps/y/be;
.super Lmaps/y/bc;


# static fields
.field private static final a:F

.field private static final b:F


# instance fields
.field private c:Lmaps/t/bx;

.field private d:I

.field private e:Lmaps/af/q;

.field private f:Z

.field private final g:Lmaps/al/o;

.field private final h:Lmaps/al/a;

.field private final i:Lmaps/al/a;

.field private final j:Lmaps/t/bx;

.field private final k:Lmaps/t/bx;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide v0, 0x3fb657184ae74487L

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/y/be;->a:F

    const-wide v0, 0x3faacee9f37bebd6L

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/y/be;->b:F

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x6

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lmaps/y/be;->d:I

    sget-object v0, Lmaps/af/q;->f:Lmaps/af/q;

    iput-object v0, p0, Lmaps/y/be;->e:Lmaps/af/q;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/y/be;->j:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/y/be;->k:Lmaps/t/bx;

    new-instance v0, Lmaps/al/o;

    invoke-direct {v0, v2}, Lmaps/al/o;-><init>(I)V

    iput-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    new-instance v0, Lmaps/al/a;

    invoke-direct {v0, v2}, Lmaps/al/a;-><init>(I)V

    iput-object v0, p0, Lmaps/y/be;->h:Lmaps/al/a;

    sget-object v0, Lmaps/af/q;->a:Lmaps/af/q;

    invoke-static {v0}, Lmaps/af/v;->a(Lmaps/af/q;)[I

    move-result-object v0

    invoke-static {v0}, Lmaps/y/be;->a([I)I

    move-result v0

    iget-object v1, p0, Lmaps/y/be;->h:Lmaps/al/a;

    invoke-virtual {v1, v0, v3}, Lmaps/al/a;->a(II)V

    iget-object v1, p0, Lmaps/y/be;->h:Lmaps/al/a;

    or-int/lit16 v0, v0, 0xff

    invoke-virtual {v1, v0, v4}, Lmaps/al/a;->a(II)V

    new-instance v0, Lmaps/al/a;

    invoke-direct {v0, v2}, Lmaps/al/a;-><init>(I)V

    iput-object v0, p0, Lmaps/y/be;->i:Lmaps/al/a;

    sget-object v0, Lmaps/af/q;->c:Lmaps/af/q;

    invoke-static {v0}, Lmaps/af/v;->a(Lmaps/af/q;)[I

    move-result-object v0

    invoke-static {v0}, Lmaps/y/be;->a([I)I

    move-result v0

    iget-object v1, p0, Lmaps/y/be;->i:Lmaps/al/a;

    invoke-virtual {v1, v0, v3}, Lmaps/al/a;->a(II)V

    iget-object v1, p0, Lmaps/y/be;->i:Lmaps/al/a;

    or-int/lit16 v0, v0, 0xff

    invoke-virtual {v1, v0, v4}, Lmaps/al/a;->a(II)V

    return-void
.end method

.method private static a([I)I
    .locals 3

    const v2, 0xff00

    const/4 v0, 0x0

    aget v0, p0, v0

    and-int/2addr v0, v2

    shl-int/lit8 v0, v0, 0x10

    const/4 v1, 0x1

    aget v1, p0, v1

    and-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    const/4 v1, 0x2

    aget v1, p0, v1

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/q;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->a(Lmaps/cr/c;)V

    sget-object v0, Lmaps/af/q;->f:Lmaps/af/q;

    if-eq p3, v0, :cond_0

    sget-object v0, Lmaps/af/q;->e:Lmaps/af/q;

    if-eq p3, v0, :cond_0

    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->h()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Lmaps/bq/d;->c(F)F

    move-result v1

    sget-object v0, Lmaps/af/q;->b:Lmaps/af/q;

    if-ne p3, v0, :cond_2

    const/high16 v0, 0x40400000

    :goto_1
    sub-float v0, v1, v0

    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v1

    invoke-virtual {p2}, Lmaps/bq/d;->o()F

    move-result v2

    const/high16 v3, 0x3f000000

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    cmpg-float v1, v1, v0

    if-lez v1, :cond_0

    invoke-virtual {p2}, Lmaps/bq/d;->p()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lmaps/y/be;->d:I

    invoke-virtual {p2, v0}, Lmaps/bq/d;->d(F)Lmaps/t/u;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/u;->c()Lmaps/t/an;

    move-result-object v0

    check-cast v0, Lmaps/t/ak;

    invoke-virtual {v0}, Lmaps/t/ak;->e()Lmaps/t/bx;

    move-result-object v2

    iput-object v2, p0, Lmaps/y/be;->c:Lmaps/t/bx;

    iget-object v2, p0, Lmaps/y/be;->j:Lmaps/t/bx;

    invoke-virtual {v2, v4, v4}, Lmaps/t/bx;->d(II)V

    invoke-virtual {v0}, Lmaps/t/ak;->d()Lmaps/t/bx;

    move-result-object v0

    iget-object v2, p0, Lmaps/y/be;->c:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/y/be;->k:Lmaps/t/bx;

    invoke-static {v0, v2, v3}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    iget-object v2, p0, Lmaps/y/be;->j:Lmaps/t/bx;

    iget v3, p0, Lmaps/y/be;->d:I

    invoke-virtual {v0, v2, v3}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    iget-object v2, p0, Lmaps/y/be;->k:Lmaps/t/bx;

    iget v3, p0, Lmaps/y/be;->d:I

    invoke-virtual {v0, v2, v3}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v0, p0, Lmaps/y/be;->c:Lmaps/t/bx;

    invoke-virtual {p2}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v2

    sget-object v0, Lmaps/af/q;->b:Lmaps/af/q;

    if-ne p3, v0, :cond_3

    sget v0, Lmaps/y/be;->b:F

    :goto_2
    mul-float/2addr v0, v2

    float-to-int v0, v0

    iget-object v2, p0, Lmaps/y/be;->j:Lmaps/t/bx;

    invoke-virtual {v2, v0}, Lmaps/t/bx;->b(I)V

    iget-object v2, p0, Lmaps/y/be;->k:Lmaps/t/bx;

    invoke-virtual {v2, v0}, Lmaps/t/bx;->b(I)V

    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    iget-object v2, p0, Lmaps/y/be;->j:Lmaps/t/bx;

    iget v3, p0, Lmaps/y/be;->d:I

    invoke-virtual {v0, v2, v3}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    iget-object v2, p0, Lmaps/y/be;->k:Lmaps/t/bx;

    iget v3, p0, Lmaps/y/be;->d:I

    invoke-virtual {v0, v2, v3}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v0, p0, Lmaps/y/be;->j:Lmaps/t/bx;

    invoke-virtual {v0, v1}, Lmaps/t/bx;->b(I)V

    iget-object v0, p0, Lmaps/y/be;->k:Lmaps/t/bx;

    invoke-virtual {v0, v1}, Lmaps/t/bx;->b(I)V

    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    iget-object v1, p0, Lmaps/y/be;->j:Lmaps/t/bx;

    iget v2, p0, Lmaps/y/be;->d:I

    invoke-virtual {v0, v1, v2}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    iget-object v1, p0, Lmaps/y/be;->k:Lmaps/t/bx;

    iget v2, p0, Lmaps/y/be;->d:I

    invoke-virtual {v0, v1, v2}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    goto/16 :goto_0

    :cond_2
    const/high16 v0, 0x40a00000

    goto/16 :goto_1

    :cond_3
    sget v0, Lmaps/y/be;->a:F

    goto :goto_2
.end method

.method private a(Lmaps/af/q;)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/y/be;->f:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/y/be;->e:Lmaps/af/q;

    if-eq p1, v1, :cond_1

    :cond_0
    iput-object p1, p0, Lmaps/y/be;->e:Lmaps/af/q;

    iput-boolean v0, p0, Lmaps/y/be;->f:Z

    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/y/be;->a(Lmaps/af/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lmaps/y/be;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/q;)V

    :cond_0
    iget-object v0, p0, Lmaps/y/be;->g:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->c()I

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/y/be;->c:Lmaps/t/bx;

    iget v2, p0, Lmaps/y/be;->d:I

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    invoke-virtual {p1}, Lmaps/cr/c;->p()V

    invoke-virtual {p1}, Lmaps/cr/c;->v()V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v1, p0, Lmaps/y/be;->g:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v1

    sget-object v2, Lmaps/af/q;->c:Lmaps/af/q;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lmaps/y/be;->i:Lmaps/al/a;

    invoke-virtual {v1, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    :goto_1
    const/4 v1, 0x5

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/y/be;->g:Lmaps/al/o;

    invoke-virtual {v3}, Lmaps/al/o;->c()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lmaps/y/be;->h:Lmaps/al/a;

    invoke-virtual {v1, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    goto :goto_1
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/be;->f:Z

    return v0
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->s:Lmaps/y/am;

    return-object v0
.end method
