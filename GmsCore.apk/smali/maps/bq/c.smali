.class public Lmaps/bq/c;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field a:Lmaps/q/i;

.field b:Lmaps/q/ah;

.field c:Lmaps/q/c;

.field final synthetic d:Lmaps/bq/d;


# direct methods
.method public constructor <init>(Lmaps/bq/d;)V
    .locals 0

    iput-object p1, p0, Lmaps/bq/c;->d:Lmaps/bq/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method a(F)V
    .locals 2

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_0
    return-void
.end method

.method a(IIF)V
    .locals 2

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_0
    return-void
.end method

.method a(Lmaps/bq/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/q/i;)V
    .locals 2

    iput-object p1, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/bq/c;->b:Lmaps/q/ah;

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/q/d;

    iget-object v1, p0, Lmaps/bq/c;->b:Lmaps/q/ah;

    invoke-direct {v0, v1}, Lmaps/q/d;-><init>(Lmaps/q/x;)V

    iput-object v0, p0, Lmaps/bq/c;->c:Lmaps/q/c;

    iget-object v0, p0, Lmaps/bq/c;->c:Lmaps/q/c;

    invoke-interface {p1, p0, v0}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_0
    return-void
.end method

.method a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/c;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    :cond_0
    return-void
.end method

.method public declared-synchronized b(Lmaps/q/i;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bq/c;->b:Lmaps/q/ah;

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/bq/a;

    iget-object v1, p0, Lmaps/bq/c;->b:Lmaps/q/ah;

    invoke-interface {v1}, Lmaps/q/ah;->b()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/bq/c;->d:Lmaps/bq/d;

    invoke-virtual {v2}, Lmaps/bq/d;->s()F

    move-result v2

    iget-object v3, p0, Lmaps/bq/c;->d:Lmaps/bq/d;

    invoke-virtual {v3}, Lmaps/bq/d;->r()F

    move-result v3

    iget-object v4, p0, Lmaps/bq/c;->d:Lmaps/bq/d;

    invoke-virtual {v4}, Lmaps/bq/d;->q()F

    move-result v4

    iget-object v5, p0, Lmaps/bq/c;->d:Lmaps/bq/d;

    invoke-virtual {v5}, Lmaps/bq/d;->j()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    iget-object v1, p0, Lmaps/bq/c;->d:Lmaps/bq/d;

    invoke-virtual {v1, v0}, Lmaps/bq/d;->a(Lmaps/bq/a;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/bq/c;->c:Lmaps/q/c;

    invoke-interface {p1, p0, v0}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
