.class public Lmaps/w/t;
.super Lmaps/w/f;


# instance fields
.field private final e:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;[Ljava/lang/Object;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lmaps/w/f;-><init>(Landroid/view/animation/Interpolator;)V

    array-length v0, p2

    const/4 v3, 0x2

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    iput-object p2, p0, Lmaps/w/t;->e:[Ljava/lang/Object;

    aget-object v0, p2, v2

    iput-object v0, p0, Lmaps/w/t;->a:Ljava/lang/Object;

    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p2, v0

    iput-object v0, p0, Lmaps/w/t;->b:Ljava/lang/Object;

    aget-object v0, p2, v2

    iput-object v0, p0, Lmaps/w/t;->c:Ljava/lang/Object;

    iput-boolean v1, p0, Lmaps/w/t;->d:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method protected b(J)V
    .locals 2

    invoke-virtual {p0, p1, p2}, Lmaps/w/t;->c(J)F

    move-result v0

    iget-object v1, p0, Lmaps/w/t;->e:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lmaps/w/t;->e:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lmaps/w/t;->e:[Ljava/lang/Object;

    aget-object v0, v1, v0

    iput-object v0, p0, Lmaps/w/t;->c:Ljava/lang/Object;

    return-void
.end method

.method protected b(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lmaps/w/t;->a:Ljava/lang/Object;

    return-void
.end method

.method protected c(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lmaps/w/t;->b:Ljava/lang/Object;

    return-void
.end method

.method protected d(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lmaps/w/t;->c:Ljava/lang/Object;

    return-void
.end method
