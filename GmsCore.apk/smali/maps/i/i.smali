.class Lmaps/i/i;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/b/y;


# instance fields
.field final synthetic a:Lmaps/i/t;


# direct methods
.method constructor <init>(Lmaps/i/t;)V
    .locals 0

    iput-object p1, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/b/r;)V
    .locals 2

    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-static {v0}, Lmaps/i/t;->a(Lmaps/i/t;)Lmaps/ao/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-virtual {v0}, Lmaps/i/t;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-static {v0}, Lmaps/i/t;->b(Lmaps/i/t;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lmaps/i/g;

    invoke-direct {v1, p0}, Lmaps/i/g;-><init>(Lmaps/i/i;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lmaps/b/r;Lmaps/t/e;)V
    .locals 2

    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-static {v0}, Lmaps/i/t;->a(Lmaps/i/t;)Lmaps/ao/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-virtual {v0}, Lmaps/i/t;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-static {v0}, Lmaps/i/t;->b(Lmaps/i/t;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lmaps/i/f;

    invoke-direct {v1, p0, p2}, Lmaps/i/f;-><init>(Lmaps/i/i;Lmaps/t/e;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b(Lmaps/b/r;)V
    .locals 2

    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-static {v0}, Lmaps/i/t;->a(Lmaps/i/t;)Lmaps/ao/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-virtual {v0}, Lmaps/i/t;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/i/i;->a:Lmaps/i/t;

    invoke-static {v0}, Lmaps/i/t;->b(Lmaps/i/t;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lmaps/i/e;

    invoke-direct {v1, p0}, Lmaps/i/e;-><init>(Lmaps/i/i;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
