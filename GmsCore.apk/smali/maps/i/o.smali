.class public final Lmaps/i/o;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/i/v;


# instance fields
.field private final a:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/i/o;->a:Landroid/os/Handler;

    return-void
.end method

.method public static a()Lmaps/i/o;
    .locals 2

    new-instance v0, Lmaps/i/o;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, Lmaps/i/o;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method

.method public static a(Landroid/os/Handler;)Lmaps/i/o;
    .locals 1

    new-instance v0, Lmaps/i/o;

    invoke-direct {v0, p0}, Lmaps/i/o;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lmaps/i/o;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .locals 1

    iget-object v0, p0, Lmaps/i/o;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lmaps/i/o;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
