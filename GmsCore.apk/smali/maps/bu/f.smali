.class public abstract Lmaps/bu/f;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/cq/a;


# instance fields
.field protected final a:Lmaps/o/c;

.field protected final b:Lmaps/o/l;

.field protected final c:Lmaps/p/k;


# direct methods
.method public constructor <init>(Lmaps/o/c;Lmaps/o/l;)V
    .locals 1

    invoke-static {}, Lmaps/be/b;->c()Lmaps/p/k;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lmaps/bu/f;-><init>(Lmaps/o/c;Lmaps/p/k;Lmaps/o/l;)V

    return-void
.end method

.method protected constructor <init>(Lmaps/o/c;Lmaps/p/k;Lmaps/o/l;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/bu/f;->a:Lmaps/o/c;

    iput-object p2, p0, Lmaps/bu/f;->c:Lmaps/p/k;

    iput-object p3, p0, Lmaps/bu/f;->b:Lmaps/o/l;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Null zoom table"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lmaps/t/bx;)F
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/bu/f;->b(Lmaps/t/bx;)Lmaps/p/ap;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/ap;->a()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public a(ILmaps/t/bx;)Ljava/util/List;
    .locals 9

    const/4 v1, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p2}, Lmaps/bu/f;->b(Lmaps/t/bx;)Lmaps/p/ap;

    move-result-object v5

    move v3, v1

    :goto_0
    if-gt v3, p1, :cond_3

    invoke-virtual {v5, v3}, Lmaps/p/ap;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    shl-int v6, v0, v3

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_0

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_2

    new-instance v7, Lmaps/t/ah;

    iget-object v8, p0, Lmaps/bu/f;->b:Lmaps/o/l;

    invoke-interface {v8}, Lmaps/o/l;->a()Lmaps/t/al;

    move-result-object v8

    invoke-direct {v7, v3, v2, v0, v8}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    return-object v4
.end method

.method public a(Lmaps/t/ah;Lmaps/t/bx;)Lmaps/t/ah;
    .locals 2

    invoke-virtual {p0, p2}, Lmaps/bu/f;->b(Lmaps/t/bx;)Lmaps/p/ap;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/p/ap;->a(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, v0}, Lmaps/t/ah;->a(I)Lmaps/t/ah;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lmaps/t/ah;Lmaps/t/bx;)Ljava/util/List;
    .locals 9

    const/4 v2, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p2}, Lmaps/bu/f;->b(Lmaps/t/bx;)Lmaps/p/ap;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v3

    invoke-virtual {v1, v3}, Lmaps/p/ap;->b(I)I

    move-result v4

    if-gez v4, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v1

    sub-int v5, v4, v1

    const/4 v1, 0x1

    shl-int v6, v1, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_0

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_2

    invoke-virtual {p1}, Lmaps/t/ah;->d()I

    move-result v7

    shl-int/2addr v7, v5

    add-int/2addr v7, v1

    invoke-virtual {p1}, Lmaps/t/ah;->e()I

    move-result v8

    shl-int/2addr v8, v5

    add-int/2addr v8, v3

    invoke-virtual {p1, v4, v7, v8}, Lmaps/t/ah;->a(III)Lmaps/t/ah;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0
.end method

.method protected b(Lmaps/t/bx;)Lmaps/p/ap;
    .locals 2

    iget-object v0, p0, Lmaps/bu/f;->c:Lmaps/p/k;

    iget-object v1, p0, Lmaps/bu/f;->a:Lmaps/o/c;

    invoke-virtual {v0, p1, v1}, Lmaps/p/k;->a(Lmaps/t/bx;Lmaps/o/c;)Lmaps/p/ap;

    move-result-object v0

    return-object v0
.end method
