.class public Lmaps/bu/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/cq/a;


# instance fields
.field private final a:Lmaps/cq/a;

.field private final b:Lmaps/b/u;

.field private final c:Lmaps/an/i;

.field private volatile d:Z

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:Ljava/util/Set;

.field private h:Ljava/util/Set;

.field private i:Lmaps/t/bg;

.field private j:Lmaps/t/cu;

.field private final k:Lmaps/be/i;

.field private final l:Lmaps/b/r;

.field private m:J


# direct methods
.method public constructor <init>(Lmaps/cq/a;Lmaps/b/u;Lmaps/an/i;ILmaps/b/r;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bu/a;->d:Z

    iput-object v1, p0, Lmaps/bu/a;->i:Lmaps/t/bg;

    iput-object v1, p0, Lmaps/bu/a;->j:Lmaps/t/cu;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/bu/a;->m:J

    iput-object p1, p0, Lmaps/bu/a;->a:Lmaps/cq/a;

    iput-object p2, p0, Lmaps/bu/a;->b:Lmaps/b/u;

    iput-object p3, p0, Lmaps/bu/a;->c:Lmaps/an/i;

    new-instance v0, Lmaps/be/i;

    invoke-direct {v0, p4}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/bu/a;->k:Lmaps/be/i;

    iput-object p5, p0, Lmaps/bu/a;->l:Lmaps/b/r;

    return-void
.end method

.method private a(Ljava/util/List;Lmaps/bq/d;)Lmaps/t/ah;
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v2

    invoke-virtual {p2}, Lmaps/bq/d;->i()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/t/ax;->a(Lmaps/t/bx;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lmaps/bq/d;)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bu/a;->a:Lmaps/cq/a;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Lmaps/cq/a;->a(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/bu/a;->d:Z

    if-nez v1, :cond_0

    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bu/a;->e:Ljava/util/List;

    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-wide v1, v0, Lmaps/bu/a;->m:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    move-object/from16 v0, p0

    iput-wide v1, v0, Lmaps/bu/a;->m:J

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/bu/a;->d:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bu/a;->l:Lmaps/b/r;

    invoke-virtual {v1}, Lmaps/b/r;->c()Lmaps/t/e;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v7}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v1

    move-object v3, v1

    :goto_1
    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v8

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v9

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v10

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/ah;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bu/a;->k:Lmaps/be/i;

    invoke-virtual {v2, v1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bu/a;->b:Lmaps/b/u;

    invoke-interface {v2, v1}, Lmaps/b/u;->a(Lmaps/t/ah;)Ljava/util/Collection;

    move-result-object v2

    sget-object v4, Lmaps/b/u;->a:Ljava/util/Collection;

    if-eq v2, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/bu/a;->k:Lmaps/be/i;

    invoke-virtual {v4, v1, v2}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_2
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/b/x;

    invoke-virtual {v2}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v4

    invoke-interface {v10, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/bu/a;->l:Lmaps/b/r;

    invoke-virtual {v2}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/b/r;->b(Lmaps/t/bg;)Lmaps/t/cu;

    move-result-object v4

    if-eqz v4, :cond_4

    new-instance v5, Lmaps/t/al;

    invoke-direct {v5}, Lmaps/t/al;-><init>()V

    invoke-virtual {v5, v4}, Lmaps/t/al;->a(Lmaps/t/ao;)V

    invoke-virtual {v1, v5}, Lmaps/t/ah;->a(Lmaps/t/al;)Lmaps/t/ah;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    monitor-enter p0

    :try_start_0
    invoke-virtual {v2}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/bu/a;->i:Lmaps/t/bg;

    invoke-virtual {v5, v13}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v5, Lmaps/t/al;

    invoke-direct {v5}, Lmaps/t/al;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/bu/a;->j:Lmaps/t/cu;

    invoke-virtual {v5, v13}, Lmaps/t/al;->a(Lmaps/t/ao;)V

    invoke-virtual {v1, v5}, Lmaps/t/ah;->a(Lmaps/t/al;)Lmaps/t/ah;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v2, v3}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v4}, Lmaps/t/cu;->c()Lmaps/t/bb;

    move-result-object v2

    invoke-virtual {v7, v2}, Lmaps/t/e;->b(Lmaps/t/bb;)I

    move-result v13

    const/4 v2, -0x1

    if-eq v13, v2, :cond_3

    invoke-virtual {v7}, Lmaps/t/e;->b()Ljava/util/List;

    move-result-object v14

    add-int/lit8 v2, v13, -0x1

    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v4, v13, 0x1

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v15

    move v5, v2

    :goto_2
    if-ge v5, v15, :cond_3

    if-ne v5, v13, :cond_7

    :goto_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    move-object v3, v1

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_7
    new-instance v4, Lmaps/t/al;

    invoke-direct {v4}, Lmaps/t/al;-><init>()V

    invoke-interface {v14, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/t/bi;

    invoke-virtual {v2}, Lmaps/t/bi;->a()Lmaps/t/bb;

    move-result-object v2

    invoke-static {v2}, Lmaps/t/cu;->a(Lmaps/t/bb;)Lmaps/t/cu;

    move-result-object v2

    invoke-virtual {v4, v2}, Lmaps/t/al;->a(Lmaps/t/ao;)V

    invoke-virtual {v1, v4}, Lmaps/t/ah;->a(Lmaps/t/al;)Lmaps/t/ah;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lmaps/bu/a;->a(Lmaps/t/ah;Lmaps/t/bx;)Lmaps/t/ah;

    move-result-object v4

    if-nez v4, :cond_9

    :goto_4
    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    move-object/from16 v0, p0

    iput-object v6, v0, Lmaps/bu/a;->e:Ljava/util/List;

    invoke-static {v8}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/bu/a;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v9, v0, Lmaps/bu/a;->g:Ljava/util/Set;

    move-object/from16 v0, p0

    iput-object v10, v0, Lmaps/bu/a;->h:Ljava/util/Set;

    goto/16 :goto_0

    :cond_9
    move-object v2, v4

    goto :goto_4
.end method


# virtual methods
.method public a(Lmaps/t/bx;)F
    .locals 1

    iget-object v0, p0, Lmaps/bu/a;->a:Lmaps/cq/a;

    invoke-interface {v0, p1}, Lmaps/cq/a;->a(Lmaps/t/bx;)F

    move-result v0

    return v0
.end method

.method public a()J
    .locals 2

    iget-wide v0, p0, Lmaps/bu/a;->m:J

    return-wide v0
.end method

.method public a(ILmaps/t/bx;)Ljava/util/List;
    .locals 1

    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/bq/d;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/bu/a;->f(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/bu/a;->f:Ljava/util/List;

    return-object v0
.end method

.method public a(Lmaps/t/ah;Lmaps/t/bx;)Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/bu/a;->a:Lmaps/cq/a;

    invoke-interface {v0, p1, p2}, Lmaps/cq/a;->a(Lmaps/t/ah;Lmaps/t/bx;)Lmaps/t/ah;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a(Lmaps/t/bg;Lmaps/t/bb;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/bu/a;->i:Lmaps/t/bg;

    new-instance v0, Lmaps/t/be;

    invoke-direct {v0}, Lmaps/t/be;-><init>()V

    invoke-virtual {v0, p2}, Lmaps/t/be;->a(Lmaps/t/bb;)Lmaps/t/be;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/be;->a()Lmaps/t/cu;

    move-result-object v0

    iput-object v0, p0, Lmaps/bu/a;->j:Lmaps/t/cu;

    invoke-virtual {p0}, Lmaps/bu/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lmaps/bq/d;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/bu/a;->f(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/bu/a;->g:Ljava/util/Set;

    return-object v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/bu/a;->d:Z

    return-void
.end method

.method public c(Lmaps/bq/d;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/bu/a;->f(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/bu/a;->h:Ljava/util/Set;

    return-object v0
.end method

.method public declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lmaps/bu/a;->i:Lmaps/t/bg;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/bu/a;->j:Lmaps/t/cu;

    invoke-virtual {p0}, Lmaps/bu/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(Lmaps/bq/d;)D
    .locals 8

    const-wide/high16 v6, 0x4033000000000000L

    const-wide/high16 v0, 0x4020000000000000L

    invoke-virtual {p1}, Lmaps/bq/d;->t()F

    move-result v2

    float-to-double v2, v2

    cmpl-double v4, v2, v6

    if-lez v4, :cond_0

    const-wide/high16 v4, 0x4000000000000000L

    sub-double/2addr v2, v6

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    :cond_0
    return-wide v0
.end method

.method public e(Lmaps/bq/d;)Lmaps/t/bg;
    .locals 4

    invoke-direct {p0, p1}, Lmaps/bu/a;->f(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/bu/a;->e:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lmaps/bu/a;->a(Ljava/util/List;Lmaps/bq/d;)Lmaps/t/ah;

    move-result-object v0

    iget-object v1, p0, Lmaps/bu/a;->k:Lmaps/be/i;

    invoke-virtual {v1, v0}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/bq/d;->i()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p0, p1}, Lmaps/bu/a;->d(Lmaps/bq/d;)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lmaps/b/x;->a(Ljava/util/Collection;Lmaps/t/bx;D)Lmaps/t/bg;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
