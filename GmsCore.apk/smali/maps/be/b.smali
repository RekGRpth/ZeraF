.class public final Lmaps/be/b;
.super Ljava/lang/Object;


# static fields
.field private static a:Z

.field private static volatile b:Z

.field private static volatile c:Z

.field private static volatile d:Lmaps/be/o;

.field private static volatile e:Lmaps/p/k;

.field private static volatile f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/be/o;

    new-instance v1, Lmaps/bb/c;

    sget-object v2, Lmaps/c/ft;->i:Lmaps/bb/d;

    invoke-direct {v1, v2}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-direct {v0, v1}, Lmaps/be/o;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/be/b;->d:Lmaps/be/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()Lmaps/be/o;
    .locals 2

    const-class v0, Lmaps/be/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/be/b;->d:Lmaps/be/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic a(Lmaps/be/o;)Lmaps/be/o;
    .locals 0

    sput-object p0, Lmaps/be/b;->d:Lmaps/be/o;

    return-object p0
.end method

.method static synthetic a(Lmaps/p/k;)Lmaps/p/k;
    .locals 0

    sput-object p0, Lmaps/be/b;->e:Lmaps/p/k;

    return-object p0
.end method

.method static synthetic a(Landroid/content/Context;Lmaps/ak/n;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lmaps/be/b;->b(Landroid/content/Context;Lmaps/ak/n;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lmaps/ak/n;Ljava/lang/Runnable;Z)V
    .locals 6

    sget-boolean v0, Lmaps/be/b;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lmaps/be/b;->a:Z

    new-instance v0, Lmaps/be/m;

    const-string v1, "ParameterManagerLoad"

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lmaps/be/m;-><init>(Ljava/lang/String;Landroid/content/Context;Lmaps/ak/n;Ljava/lang/Runnable;Z)V

    invoke-virtual {v0}, Lmaps/be/m;->start()V

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lmaps/be/b;->f:Z

    return p0
.end method

.method private static declared-synchronized b(Landroid/content/Context;Lmaps/ak/n;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v4, 0x0

    const-class v7, Lmaps/be/b;

    monitor-enter v7

    :try_start_0
    const-string v0, "ParameterManager.initializeInternal"

    invoke-static {v0}, Lmaps/ah/a;->a(Ljava/lang/String;)V

    sget-object v0, Lmaps/c/ft;->d:Lmaps/bb/d;

    invoke-static {p0, p3, v0}, Lmaps/bt/a;->a(Landroid/content/Context;Ljava/lang/String;Lmaps/bb/d;)Lmaps/bb/c;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    const/4 v2, 0x1

    sput-boolean v2, Lmaps/be/b;->c:Z

    :goto_0
    new-instance v2, Lmaps/be/o;

    invoke-direct {v2, v0}, Lmaps/be/o;-><init>(Lmaps/bb/c;)V

    sput-object v2, Lmaps/be/b;->d:Lmaps/be/o;

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lmaps/bb/c;->e(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_1
    sget-object v0, Lmaps/c/ft;->d:Lmaps/bb/d;

    invoke-static {p0, p4, v0}, Lmaps/bt/a;->a(Landroid/content/Context;Ljava/lang/String;Lmaps/bb/d;)Lmaps/bb/c;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lmaps/bb/c;->i(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v1

    invoke-static {v1}, Lmaps/p/k;->a(Lmaps/bb/c;)Lmaps/p/k;

    move-result-object v1

    sput-object v1, Lmaps/be/b;->e:Lmaps/p/k;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bb/c;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bb/c;->e(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    :cond_0
    const-class v0, Lmaps/be/b;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_1
    if-eqz p1, :cond_4

    sget-object v0, Lmaps/be/b;->e:Lmaps/p/k;

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lmaps/ak/n;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    :cond_2
    const/4 v0, 0x1

    sput-boolean v0, Lmaps/be/b;->f:Z

    const-class v0, Lmaps/be/b;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    new-instance v0, Lmaps/be/e;

    invoke-direct {v0, p1, p2}, Lmaps/be/e;-><init>(Lmaps/ak/n;Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Lmaps/ak/n;->a(Lmaps/ak/i;)V

    new-instance v0, Lmaps/be/k;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lmaps/be/k;-><init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lmaps/be/m;)V

    invoke-virtual {p1, v0}, Lmaps/ak/n;->a(Lmaps/ak/j;)V

    :cond_4
    const-string v0, "ParameterManager.initializeInternal"

    invoke-static {v0}, Lmaps/ah/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v7

    return-void

    :cond_5
    :try_start_1
    new-instance v1, Lmaps/bb/c;

    sget-object v0, Lmaps/c/ft;->d:Lmaps/bb/d;

    invoke-direct {v1, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    new-instance v0, Lmaps/bb/c;

    sget-object v2, Lmaps/c/ft;->i:Lmaps/bb/d;

    invoke-direct {v0, v2}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_6
    move-object v2, v4

    goto :goto_1
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lmaps/be/b;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/be/b;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Z)Z
    .locals 0

    sput-boolean p0, Lmaps/be/b;->b:Z

    return p0
.end method

.method public static declared-synchronized c()Lmaps/p/k;
    .locals 2

    const-class v1, Lmaps/be/b;

    monitor-enter v1

    :goto_0
    :try_start_0
    sget-object v0, Lmaps/be/b;->e:Lmaps/p/k;

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/be/b;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    const-class v0, Lmaps/be/b;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    sget-object v0, Lmaps/be/b;->e:Lmaps/p/k;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d()Z
    .locals 1

    sget-object v0, Lmaps/be/b;->e:Lmaps/p/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/be/b;->f:Z

    sput-boolean v0, Lmaps/be/b;->a:Z

    sput-boolean v0, Lmaps/be/b;->b:Z

    sput-boolean v0, Lmaps/be/b;->c:Z

    return-void
.end method

.method static synthetic f()Lmaps/p/k;
    .locals 1

    sget-object v0, Lmaps/be/b;->e:Lmaps/p/k;

    return-object v0
.end method
