.class public Lmaps/j/d;
.super Lmaps/j/a;


# static fields
.field public static final f:Lmaps/t/ah;

.field public static final g:Lmaps/t/ah;


# instance fields
.field private h:Ljava/util/Map;

.field private i:Ljava/util/Map;

.field private j:Lmaps/j/l;

.field private k:Lmaps/j/l;

.field private l:Lmaps/t/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, -0x2

    new-instance v0, Lmaps/t/ah;

    invoke-direct {v0, v2, v2, v2}, Lmaps/t/ah;-><init>(III)V

    sput-object v0, Lmaps/j/d;->f:Lmaps/t/ah;

    new-instance v0, Lmaps/t/ah;

    invoke-direct {v0, v1, v1, v1}, Lmaps/t/ah;-><init>(III)V

    sput-object v0, Lmaps/j/d;->g:Lmaps/t/ah;

    return-void
.end method

.method public constructor <init>(Ljavax/microedition/khronos/opengles/GL;)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/j/a;-><init>(Ljavax/microedition/khronos/opengles/GL;)V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/j/d;->h:Ljava/util/Map;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/j/d;->i:Ljava/util/Map;

    sget-object v0, Lmaps/j/d;->f:Lmaps/t/ah;

    iput-object v0, p0, Lmaps/j/d;->l:Lmaps/t/ah;

    return-void
.end method

.method private declared-synchronized a(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/j/d;->i:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to delete non-existent buffer id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(ILmaps/j/l;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/j/d;->i:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/j/l;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buffer id allocated twice: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lmaps/j/l;->a:Ljava/lang/Throwable;

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized b(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/j/d;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to delete non-existent texture id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized b(ILmaps/j/l;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/j/d;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/j/l;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Texture id allocated twice: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lmaps/j/l;->a:Ljava/lang/Throwable;

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public a()V
    .locals 28

    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_0

    const-string v2, "GLLeakCheckWrapper"

    const-string v3, "dumping GL memory statistics"

    invoke-static {v2, v3}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    invoke-static {}, Lmaps/f/cs;->d()Ljava/util/TreeMap;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/j/d;->h:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/j/l;

    iget-object v3, v2, Lmaps/j/l;->d:Lmaps/t/ah;

    invoke-interface {v10, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v14, v2, Lmaps/j/l;->d:Lmaps/t/ah;

    invoke-interface {v10, v14, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/j/d;->i:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/j/l;

    iget-object v3, v2, Lmaps/j/l;->d:Lmaps/t/ah;

    invoke-interface {v10, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-nez v3, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v14, v2, Lmaps/j/l;->d:Lmaps/t/ah;

    invoke-interface {v10, v14, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_5
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    const-wide/16 v10, 0x0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/j/l;

    iget-wide v0, v3, Lmaps/j/l;->b:J

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    sget-object v18, Lmaps/j/d;->f:Lmaps/t/ah;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_6

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Allocated "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-wide v0, v3, Lmaps/j/l;->c:J

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " bytes for general use "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " seconds ago"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    iget-object v0, v3, Lmaps/j/l;->a:Ljava/lang/Throwable;

    move-object/from16 v17, v0

    invoke-static/range {v16 .. v17}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-wide v0, v3, Lmaps/j/l;->c:J

    move-wide/from16 v16, v0

    add-long v6, v6, v16

    move-wide/from16 v20, v10

    move-wide/from16 v22, v4

    move-wide/from16 v3, v20

    move-wide/from16 v24, v6

    move-wide/from16 v5, v22

    move-wide/from16 v26, v8

    move-wide/from16 v9, v26

    move-wide/from16 v7, v24

    :goto_4
    move-wide/from16 v20, v3

    move-wide/from16 v22, v5

    move-wide/from16 v4, v22

    move-wide/from16 v24, v7

    move-wide/from16 v6, v24

    move-wide/from16 v26, v9

    move-wide/from16 v8, v26

    move-wide/from16 v10, v20

    goto :goto_3

    :cond_6
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    sget-object v18, Lmaps/j/d;->g:Lmaps/t/ah;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_7

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Allocated "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-wide v0, v3, Lmaps/j/l;->c:J

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " bytes for labels "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " seconds ago"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    iget-object v0, v3, Lmaps/j/l;->a:Ljava/lang/Throwable;

    move-object/from16 v17, v0

    invoke-static/range {v16 .. v17}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-wide v0, v3, Lmaps/j/l;->c:J

    move-wide/from16 v16, v0

    add-long v4, v4, v16

    move-wide/from16 v20, v10

    move-wide/from16 v22, v4

    move-wide/from16 v3, v20

    move-wide/from16 v24, v6

    move-wide/from16 v5, v22

    move-wide/from16 v26, v8

    move-wide/from16 v9, v26

    move-wide/from16 v7, v24

    goto :goto_4

    :cond_7
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Allocated "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-wide v0, v3, Lmaps/j/l;->c:J

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " bytes for tile"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " seconds ago"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    iget-object v0, v3, Lmaps/j/l;->a:Ljava/lang/Throwable;

    move-object/from16 v17, v0

    invoke-static/range {v16 .. v17}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-wide v0, v3, Lmaps/j/l;->c:J

    move-wide/from16 v16, v0

    add-long v10, v10, v16

    iget-wide v0, v3, Lmaps/j/l;->c:J

    move-wide/from16 v16, v0

    add-long v8, v8, v16

    move-wide/from16 v20, v10

    move-wide/from16 v22, v4

    move-wide/from16 v3, v20

    move-wide/from16 v24, v6

    move-wide/from16 v5, v22

    move-wide/from16 v26, v8

    move-wide/from16 v9, v26

    move-wide/from16 v7, v24

    goto/16 :goto_4

    :cond_8
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    sget-object v15, Lmaps/j/d;->f:Lmaps/t/ah;

    if-eq v3, v15, :cond_5

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    sget-object v15, Lmaps/j/d;->g:Lmaps/t/ah;

    if-eq v3, v15, :cond_5

    sget-boolean v3, Lmaps/ae/h;->j:Z

    if-eqz v3, :cond_5

    const-string v3, "GLLeakCheckWrapper"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Tile "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v15, " total size: "

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_a

    const-string v2, "GLLeakCheckWrapper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bytes used in TileOverlays: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_b

    const-string v2, "GLLeakCheckWrapper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bytes used for labels: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_c

    const-string v2, "GLLeakCheckWrapper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bytes used for general use: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    return-void
.end method

.method public a(J)V
    .locals 2

    iget-object v0, p0, Lmaps/j/d;->j:Lmaps/j/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/j/d;->j:Lmaps/j/l;

    iput-wide p1, v0, Lmaps/j/l;->c:J

    iget-object v0, p0, Lmaps/j/d;->j:Lmaps/j/l;

    iget-object v1, p0, Lmaps/j/d;->l:Lmaps/t/ah;

    iput-object v1, v0, Lmaps/j/l;->d:Lmaps/t/ah;

    :cond_0
    return-void
.end method

.method public a(Lmaps/t/ah;)V
    .locals 0

    iput-object p1, p0, Lmaps/j/d;->l:Lmaps/t/ah;

    return-void
.end method

.method public b()Z
    .locals 3

    iget-object v0, p0, Lmaps/j/d;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lmaps/j/d;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_3

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_1

    const-string v0, "GLLeakCheckWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkLeaks called with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/j/d;->h:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " allocated textures and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/j/d;->i:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " allocated buffers."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " See below for stacks where they were allocated."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lmaps/j/d;->a()V

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_2

    const-string v0, "GLLeakCheckWrapper"

    const-string v1, "checkLeaks FAILED"

    invoke-static {v0, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_4

    const-string v0, "GLLeakCheckWrapper"

    const-string v1, "checkLeaks PASSED"

    invoke-static {v0, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public glBindBuffer(II)V
    .locals 2

    iget-object v0, p0, Lmaps/j/d;->c:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, p1, p2}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v0, p0, Lmaps/j/d;->i:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/j/l;

    iput-object v0, p0, Lmaps/j/d;->k:Lmaps/j/l;

    return-void
.end method

.method public glBindTexture(II)V
    .locals 2

    iget-object v0, p0, Lmaps/j/d;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v0, p1, p2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    const/16 v0, 0xde1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lmaps/j/d;->h:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/j/l;

    iput-object v0, p0, Lmaps/j/d;->j:Lmaps/j/l;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/j/d;->j:Lmaps/j/l;

    goto :goto_0
.end method

.method public glBufferData(IILjava/nio/Buffer;I)V
    .locals 3

    iget-object v0, p0, Lmaps/j/d;->c:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    iget-object v0, p0, Lmaps/j/d;->k:Lmaps/j/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/j/d;->k:Lmaps/j/l;

    int-to-long v1, p2

    iput-wide v1, v0, Lmaps/j/l;->c:J

    iget-object v0, p0, Lmaps/j/d;->k:Lmaps/j/l;

    iget-object v1, p0, Lmaps/j/d;->l:Lmaps/t/ah;

    iput-object v1, v0, Lmaps/j/l;->d:Lmaps/t/ah;

    :cond_0
    return-void
.end method

.method public glDeleteBuffers(ILjava/nio/IntBuffer;)V
    .locals 3

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->position()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->get()I

    move-result v2

    invoke-direct {p0, v2}, Lmaps/j/d;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    invoke-super {p0, p1, p2}, Lmaps/j/a;->glDeleteBuffers(ILjava/nio/IntBuffer;)V

    return-void
.end method

.method public glDeleteBuffers(I[II)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    add-int v1, p3, v0

    aget v1, p2, v1

    invoke-direct {p0, v1}, Lmaps/j/d;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmaps/j/a;->glDeleteBuffers(I[II)V

    return-void
.end method

.method public glDeleteTextures(ILjava/nio/IntBuffer;)V
    .locals 3

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->position()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->get()I

    move-result v2

    invoke-direct {p0, v2}, Lmaps/j/d;->b(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    invoke-super {p0, p1, p2}, Lmaps/j/a;->glDeleteTextures(ILjava/nio/IntBuffer;)V

    return-void
.end method

.method public glDeleteTextures(I[II)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    add-int v1, p3, v0

    aget v1, p2, v1

    invoke-direct {p0, v1}, Lmaps/j/d;->b(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmaps/j/a;->glDeleteTextures(I[II)V

    return-void
.end method

.method public glGenBuffers(ILjava/nio/IntBuffer;)V
    .locals 5

    invoke-super {p0, p1, p2}, Lmaps/j/a;->glGenBuffers(ILjava/nio/IntBuffer;)V

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->position()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    new-instance v3, Lmaps/j/l;

    invoke-direct {v3, v1}, Lmaps/j/l;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->get()I

    move-result v4

    invoke-direct {p0, v4, v3}, Lmaps/j/d;->a(ILmaps/j/l;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2, v2}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    return-void
.end method

.method public glGenBuffers(I[II)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lmaps/j/a;->glGenBuffers(I[II)V

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    new-instance v2, Lmaps/j/l;

    invoke-direct {v2, v1}, Lmaps/j/l;-><init>(Ljava/lang/Throwable;)V

    add-int v3, v0, p3

    aget v3, p2, v3

    invoke-direct {p0, v3, v2}, Lmaps/j/d;->a(ILmaps/j/l;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public glGenTextures(ILjava/nio/IntBuffer;)V
    .locals 5

    invoke-super {p0, p1, p2}, Lmaps/j/a;->glGenTextures(ILjava/nio/IntBuffer;)V

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->position()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    new-instance v3, Lmaps/j/l;

    invoke-direct {v3, v1}, Lmaps/j/l;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {p2}, Ljava/nio/IntBuffer;->get()I

    move-result v4

    invoke-direct {p0, v4, v3}, Lmaps/j/d;->b(ILmaps/j/l;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2, v2}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    return-void
.end method

.method public glGenTextures(I[II)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lmaps/j/a;->glGenTextures(I[II)V

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    new-instance v2, Lmaps/j/l;

    invoke-direct {v2, v1}, Lmaps/j/l;-><init>(Ljava/lang/Throwable;)V

    add-int v3, p3, v0

    aget v3, p2, v3

    invoke-direct {p0, v3, v2}, Lmaps/j/d;->b(ILmaps/j/l;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
