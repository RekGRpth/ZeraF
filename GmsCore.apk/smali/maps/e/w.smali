.class Lmaps/e/w;
.super Ljava/lang/Object;


# instance fields
.field a:Z

.field b:Lmaps/t/bu;

.field c:Lmaps/t/bx;

.field d:Lmaps/cn/a;

.field e:D

.field f:I

.field g:Z


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x4010000000000000L

    iput-wide v0, p0, Lmaps/e/w;->e:D

    iput v2, p0, Lmaps/e/w;->f:I

    iput-boolean v2, p0, Lmaps/e/w;->g:Z

    return-void
.end method

.method constructor <init>(Lmaps/e/w;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x4010000000000000L

    iput-wide v0, p0, Lmaps/e/w;->e:D

    iput v2, p0, Lmaps/e/w;->f:I

    iput-boolean v2, p0, Lmaps/e/w;->g:Z

    iget-boolean v0, p1, Lmaps/e/w;->a:Z

    iput-boolean v0, p0, Lmaps/e/w;->a:Z

    iget-object v0, p1, Lmaps/e/w;->b:Lmaps/t/bu;

    iput-object v0, p0, Lmaps/e/w;->b:Lmaps/t/bu;

    iget-object v0, p1, Lmaps/e/w;->c:Lmaps/t/bx;

    iput-object v0, p0, Lmaps/e/w;->c:Lmaps/t/bx;

    iget-object v0, p1, Lmaps/e/w;->d:Lmaps/cn/a;

    iput-object v0, p0, Lmaps/e/w;->d:Lmaps/cn/a;

    iget-wide v0, p1, Lmaps/e/w;->e:D

    iput-wide v0, p0, Lmaps/e/w;->e:D

    iget v0, p1, Lmaps/e/w;->f:I

    iput v0, p0, Lmaps/e/w;->f:I

    iget-boolean v0, p1, Lmaps/e/w;->g:Z

    iput-boolean v0, p0, Lmaps/e/w;->g:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/e/w;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lmaps/e/w;

    iget-boolean v1, p0, Lmaps/e/w;->a:Z

    iget-boolean v2, p1, Lmaps/e/w;->a:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmaps/e/w;->b:Lmaps/t/bu;

    iget-object v2, p1, Lmaps/e/w;->b:Lmaps/t/bu;

    invoke-static {v1, v2}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/e/w;->c:Lmaps/t/bx;

    iget-object v2, p1, Lmaps/e/w;->c:Lmaps/t/bx;

    invoke-static {v1, v2}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/e/w;->d:Lmaps/cn/a;

    iget-object v2, p1, Lmaps/e/w;->d:Lmaps/cn/a;

    invoke-static {v1, v2}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lmaps/e/w;->f:I

    iget v2, p1, Lmaps/e/w;->f:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lmaps/e/w;->g:Z

    iget-boolean v2, p1, Lmaps/e/w;->g:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
