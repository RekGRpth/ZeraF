.class Lmaps/e/i;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Landroid/location/LocationListener;

.field final synthetic b:Lmaps/e/s;


# direct methods
.method constructor <init>(Lmaps/e/s;Landroid/location/LocationListener;)V
    .locals 0

    iput-object p1, p0, Lmaps/e/i;->b:Lmaps/e/s;

    iput-object p2, p0, Lmaps/e/i;->a:Landroid/location/LocationListener;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lmaps/cd/b;->a(Landroid/content/Intent;)Lmaps/cd/a;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lmaps/cd/a;->a:Landroid/location/Location;

    sget-boolean v1, Lmaps/ae/h;->e:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lmaps/e/s;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GMM location broadcast: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lmaps/e/i;->a:Landroid/location/LocationListener;

    invoke-interface {v1, v0}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    :cond_1
    return-void
.end method
