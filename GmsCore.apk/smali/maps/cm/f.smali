.class public Lmaps/cm/f;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/f/bc;

.field private final b:Ljava/util/logging/Logger;

.field private final c:Lmaps/cm/e;

.field private final d:Ljava/lang/ThreadLocal;

.field private final e:Ljava/lang/ThreadLocal;

.field private f:Lmaps/v/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "default"

    invoke-direct {p0, v0}, Lmaps/cm/f;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    new-instance v1, Lmaps/cm/b;

    invoke-direct {v1, p0}, Lmaps/cm/b;-><init>(Lmaps/cm/f;)V

    invoke-static {v0, v1}, Lmaps/f/ex;->a(Ljava/util/Map;Lmaps/ap/k;)Lmaps/f/bc;

    move-result-object v0

    iput-object v0, p0, Lmaps/cm/f;->a:Lmaps/f/bc;

    new-instance v0, Lmaps/cm/g;

    invoke-direct {v0}, Lmaps/cm/g;-><init>()V

    iput-object v0, p0, Lmaps/cm/f;->c:Lmaps/cm/e;

    new-instance v0, Lmaps/cm/a;

    invoke-direct {v0, p0}, Lmaps/cm/a;-><init>(Lmaps/cm/f;)V

    iput-object v0, p0, Lmaps/cm/f;->d:Ljava/lang/ThreadLocal;

    new-instance v0, Lmaps/cm/d;

    invoke-direct {v0, p0}, Lmaps/cm/d;-><init>(Lmaps/cm/f;)V

    iput-object v0, p0, Lmaps/cm/f;->e:Ljava/lang/ThreadLocal;

    invoke-static {}, Lmaps/v/l;->a()Lmaps/v/l;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/v/l;->h()Lmaps/v/l;

    move-result-object v0

    new-instance v1, Lmaps/cm/c;

    invoke-direct {v1, p0}, Lmaps/cm/c;-><init>(Lmaps/cm/f;)V

    invoke-virtual {v0, v1}, Lmaps/v/l;->a(Lmaps/v/af;)Lmaps/v/q;

    move-result-object v0

    iput-object v0, p0, Lmaps/cm/f;->f:Lmaps/v/q;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lmaps/cm/f;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lmaps/cm/f;->b:Ljava/util/logging/Logger;

    return-void
.end method
