.class public final Lmaps/az/c;
.super Ljava/lang/Object;


# static fields
.field static final a:Ljava/util/Map;

.field private static volatile b:Z

.field private static volatile c:Lmaps/bb/c;

.field private static volatile d:Lmaps/az/f;

.field private static volatile e:Lmaps/az/l;

.field private static volatile f:Lmaps/az/b;

.field private static volatile g:Lmaps/az/a;

.field private static volatile h:Lmaps/az/j;

.field private static volatile i:Lmaps/az/i;

.field private static volatile j:Lmaps/az/g;

.field private static volatile k:Lmaps/az/h;

.field private static volatile l:Lmaps/ai/b;

.field private static volatile m:Z

.field private static volatile n:Z

.field private static o:Ljava/lang/Object;

.field private static volatile p:Lmaps/az/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/az/c;->b:Z

    const/4 v0, 0x0

    sput-object v0, Lmaps/az/c;->l:Lmaps/ai/b;

    sput-boolean v1, Lmaps/az/c;->m:Z

    sput-boolean v1, Lmaps/az/c;->n:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmaps/az/c;->o:Ljava/lang/Object;

    invoke-static {}, Lmaps/f/fs;->f()Lmaps/f/av;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    move-result-object v0

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/f/av;->a()Lmaps/f/fs;

    move-result-object v0

    sput-object v0, Lmaps/az/c;->a:Ljava/util/Map;

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/az/f;

    invoke-static {}, Lmaps/az/f;->d()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/f;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->d:Lmaps/az/f;

    new-instance v0, Lmaps/az/l;

    invoke-static {}, Lmaps/az/l;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/l;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->e:Lmaps/az/l;

    new-instance v0, Lmaps/az/b;

    invoke-static {}, Lmaps/az/b;->h()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/b;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->f:Lmaps/az/b;

    new-instance v0, Lmaps/az/a;

    invoke-static {}, Lmaps/az/a;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/a;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->g:Lmaps/az/a;

    new-instance v0, Lmaps/az/j;

    invoke-static {}, Lmaps/az/j;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/j;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->h:Lmaps/az/j;

    new-instance v0, Lmaps/az/i;

    invoke-static {}, Lmaps/az/i;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/i;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->i:Lmaps/az/i;

    new-instance v0, Lmaps/az/g;

    invoke-static {}, Lmaps/az/g;->c()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/g;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->j:Lmaps/az/g;

    new-instance v0, Lmaps/az/h;

    invoke-static {}, Lmaps/az/h;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/az/h;-><init>(Lmaps/bb/c;)V

    sput-object v0, Lmaps/az/c;->k:Lmaps/az/h;

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lmaps/ai/b;)Lmaps/ai/b;
    .locals 0

    sput-object p0, Lmaps/az/c;->l:Lmaps/ai/b;

    return-object p0
.end method

.method public static declared-synchronized a()Lmaps/az/f;
    .locals 2

    const-class v0, Lmaps/az/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/az/c;->d:Lmaps/az/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static a(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lmaps/az/c;->e()Lmaps/bb/c;

    move-result-object v0

    sput-object v0, Lmaps/az/c;->c:Lmaps/bb/c;

    :try_start_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lmaps/bj/b;->b(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lmaps/bb/c;

    sget-object v2, Lmaps/c/ft;->c:Lmaps/bb/d;

    invoke-direct {v1, v2}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-virtual {v1, v0}, Lmaps/bb/c;->a([B)Lmaps/bb/c;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lmaps/bb/c;->j(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v3

    invoke-static {v3}, Lmaps/az/c;->e(Lmaps/bb/c;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_0

    const-string v1, "ServerControlledParametersManager:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t parse the CLIENT_PARAMETERS_RESPONSE_PROTO read from the cache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Lmaps/ak/n;)V
    .locals 2

    const-class v1, Lmaps/az/c;

    monitor-enter v1

    :try_start_0
    const-string v0, "ServerControlledParametersManager.data"

    invoke-static {p0, v0}, Lmaps/az/c;->a(Lmaps/ak/n;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Lmaps/ak/n;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lmaps/az/c;->c:Lmaps/bb/c;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lmaps/az/c;->a(Ljava/lang/String;)V

    sget-object v0, Lmaps/az/c;->c:Lmaps/bb/c;

    invoke-virtual {v0, v3}, Lmaps/bb/c;->j(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    sget-object v2, Lmaps/az/c;->c:Lmaps/bb/c;

    invoke-virtual {v2, v3, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v2

    invoke-static {v2}, Lmaps/az/c;->d(Lmaps/bb/c;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-static {p0, p1, v3}, Lmaps/az/c;->b(Lmaps/ak/n;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/ak/n;Ljava/lang/String;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lmaps/az/c;->b(Lmaps/ak/n;Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Lmaps/bb/c;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ft;->b:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/k/b;->f()Z

    move-result v1

    invoke-virtual {v0, v2, v2}, Lmaps/bb/c;->a(IZ)Lmaps/bb/c;

    invoke-virtual {v0, v3, v1}, Lmaps/bb/c;->a(IZ)Lmaps/bb/c;

    invoke-virtual {p0, v3, v0}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    return-void
.end method

.method static a(Lmaps/bb/c;Ljava/lang/String;)Z
    .locals 4

    :try_start_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bb/c;->e()[B

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lmaps/bj/b;->a([BLjava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_0

    const-string v1, "ServerControlledParametersManager:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t write the CLIENT_PARAMETERS_RESPONSE_PROTO to the cache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lmaps/az/c;->n:Z

    return p0
.end method

.method public static declared-synchronized b()Lmaps/az/b;
    .locals 2

    const-class v0, Lmaps/az/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/az/c;->f:Lmaps/az/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized b(Lmaps/ak/n;)V
    .locals 2

    const-class v1, Lmaps/az/c;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lmaps/az/c;->b:Z

    const-string v0, "ServerControlledParametersManager_DA.data"

    invoke-static {p0, v0}, Lmaps/az/c;->a(Lmaps/ak/n;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b(Lmaps/ak/n;Ljava/lang/String;Z)V
    .locals 3

    const-class v1, Lmaps/az/c;

    monitor-enter v1

    if-nez p0, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_0
    sget-object v2, Lmaps/az/c;->o:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lmaps/az/c;->l:Lmaps/ai/b;

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/az/c;->l:Lmaps/ai/b;

    invoke-virtual {v0}, Lmaps/ai/b;->g()I

    const/4 v0, 0x0

    sput-object v0, Lmaps/az/c;->l:Lmaps/ai/b;

    :cond_1
    sget-boolean v0, Lmaps/az/c;->n:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/az/c;->m:Z

    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const/4 v0, 0x1

    :try_start_3
    sput-boolean v0, Lmaps/az/c;->n:Z

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/az/c;->m:Z

    new-instance v0, Lmaps/az/d;

    invoke-direct {v0, p1, p2}, Lmaps/az/d;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lmaps/ak/n;->a(Lmaps/ak/j;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method static synthetic b(Lmaps/bb/c;)Z
    .locals 1

    invoke-static {p0}, Lmaps/az/c;->d(Lmaps/bb/c;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()Lmaps/az/a;
    .locals 2

    const-class v0, Lmaps/az/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/az/c;->g:Lmaps/az/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic c(Lmaps/bb/c;)V
    .locals 0

    invoke-static {p0}, Lmaps/az/c;->e(Lmaps/bb/c;)V

    return-void
.end method

.method public static d()Lmaps/az/g;
    .locals 1

    sget-object v0, Lmaps/az/c;->j:Lmaps/az/g;

    return-object v0
.end method

.method private static d(Lmaps/bb/c;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    invoke-static {p0, v2, v0}, Lmaps/bb/b;->a(Lmaps/bb/c;II)I

    move-result v3

    sget-object v0, Lmaps/az/c;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "MAPS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ServerControlledParametersManager: ParameterGroupProto with type "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is supported"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lmaps/az/c;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/bb/c;->i(I)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0, v0}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_2

    const-string v0, "MAPS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ServerControlledParametersManager: ParameterGroupProto with type "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isn\'t supported"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_1
    sget-object v1, Lmaps/az/c;->d:Lmaps/az/f;

    if-eqz v1, :cond_3

    sget-object v1, Lmaps/az/c;->d:Lmaps/az/f;

    invoke-virtual {v1, v0}, Lmaps/az/f;->a(Lmaps/bb/c;)V

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_3
    new-instance v1, Lmaps/az/f;

    invoke-direct {v1, v0}, Lmaps/az/f;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->d:Lmaps/az/f;

    goto :goto_1

    :pswitch_2
    new-instance v1, Lmaps/az/l;

    invoke-direct {v1, v0}, Lmaps/az/l;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->e:Lmaps/az/l;

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->l()V

    move v0, v2

    goto :goto_0

    :pswitch_3
    new-instance v1, Lmaps/az/b;

    invoke-direct {v1, v0}, Lmaps/az/b;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->f:Lmaps/az/b;

    move v0, v2

    goto :goto_0

    :pswitch_4
    new-instance v1, Lmaps/az/a;

    invoke-direct {v1, v0}, Lmaps/az/a;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->g:Lmaps/az/a;

    move v0, v2

    goto :goto_0

    :pswitch_5
    new-instance v1, Lmaps/az/j;

    invoke-direct {v1, v0}, Lmaps/az/j;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->h:Lmaps/az/j;

    move v0, v2

    goto :goto_0

    :pswitch_6
    new-instance v1, Lmaps/az/i;

    invoke-direct {v1, v0}, Lmaps/az/i;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->i:Lmaps/az/i;

    move v0, v2

    goto/16 :goto_0

    :pswitch_7
    new-instance v1, Lmaps/az/g;

    invoke-direct {v1, v0}, Lmaps/az/g;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->j:Lmaps/az/g;

    move v0, v2

    goto/16 :goto_0

    :pswitch_8
    new-instance v1, Lmaps/az/h;

    invoke-direct {v1, v0}, Lmaps/az/h;-><init>(Lmaps/bb/c;)V

    sput-object v1, Lmaps/az/c;->k:Lmaps/az/h;

    move v0, v2

    goto/16 :goto_0

    :cond_4
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_5

    const-string v0, "MAPS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ServerControlledParametersManager: ParameterGroupProto with type "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not updated"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method static e()Lmaps/bb/c;
    .locals 6

    const/4 v5, 0x1

    new-instance v3, Lmaps/bb/c;

    sget-object v0, Lmaps/c/ft;->c:Lmaps/bb/d;

    invoke-direct {v3, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    sget-object v0, Lmaps/az/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v2, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ft;->d:Lmaps/bb/d;

    invoke-direct {v2, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v5, v1}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v3, v5, v0}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lmaps/az/f;->d()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lmaps/az/l;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :pswitch_3
    invoke-static {}, Lmaps/az/b;->h()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :pswitch_4
    invoke-static {}, Lmaps/az/a;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :pswitch_5
    invoke-static {}, Lmaps/az/j;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :pswitch_6
    invoke-static {}, Lmaps/az/i;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :pswitch_7
    invoke-static {}, Lmaps/az/g;->c()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :pswitch_8
    invoke-static {}, Lmaps/az/h;->a()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    move-object v0, v2

    goto :goto_1

    :cond_1
    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static e(Lmaps/bb/c;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lmaps/bb/c;->d(I)I

    move-result v1

    sget-object v0, Lmaps/az/c;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lmaps/az/c;->c:Lmaps/bb/c;

    invoke-virtual {v0, v4}, Lmaps/bb/c;->j(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    sget-object v3, Lmaps/az/c;->c:Lmaps/bb/c;

    invoke-virtual {v3, v4, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v3

    invoke-virtual {v3, v4}, Lmaps/bb/c;->d(I)I

    move-result v3

    if-ne v1, v3, :cond_2

    sget-object v1, Lmaps/az/c;->c:Lmaps/bb/c;

    invoke-virtual {v1, v4, v0}, Lmaps/bb/c;->i(II)V

    :cond_1
    sget-object v0, Lmaps/az/c;->c:Lmaps/bb/c;

    invoke-virtual {v0, v4, p0}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic f()Lmaps/bb/c;
    .locals 1

    sget-object v0, Lmaps/az/c;->c:Lmaps/bb/c;

    return-object v0
.end method

.method static synthetic g()Lmaps/az/e;
    .locals 1

    sget-object v0, Lmaps/az/c;->p:Lmaps/az/e;

    return-object v0
.end method

.method static synthetic h()Z
    .locals 1

    sget-boolean v0, Lmaps/az/c;->b:Z

    return v0
.end method

.method static synthetic i()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lmaps/az/c;->o:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic j()Z
    .locals 1

    sget-boolean v0, Lmaps/az/c;->m:Z

    return v0
.end method

.method static synthetic k()Lmaps/ai/b;
    .locals 1

    sget-object v0, Lmaps/az/c;->l:Lmaps/ai/b;

    return-object v0
.end method
