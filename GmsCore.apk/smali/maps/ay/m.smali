.class Lmaps/ay/m;
.super Ljava/lang/Object;


# instance fields
.field protected a:Lmaps/ay/s;

.field protected b:[I

.field c:I


# direct methods
.method protected constructor <init>(Lmaps/ay/s;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/ay/m;->b:[I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/m;->c:I

    return-void
.end method

.method protected constructor <init>(Lmaps/ay/s;[I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iput-object p2, p0, Lmaps/ay/m;->b:[I

    array-length v0, p2

    iput v0, p0, Lmaps/ay/m;->c:I

    return-void
.end method

.method protected constructor <init>([D)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/ay/l;->a([D)Lmaps/ay/l;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/ay/m;->b:[I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/m;->c:I

    return-void
.end method

.method private static a([II)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    return v0

    :cond_1
    const/4 v1, 0x1

    move v3, v0

    move v0, v1

    move v1, v3

    :goto_0
    if-ge v1, p1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    aget v2, p0, v1

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method private static a([II[II)Z
    .locals 4

    const/4 v1, -0x1

    const/4 v0, 0x0

    if-ne p1, v1, :cond_0

    array-length p1, p0

    :cond_0
    if-ne p3, v1, :cond_1

    array-length p3, p2

    :cond_1
    if-ne p1, p3, :cond_2

    if-eqz p0, :cond_2

    if-eqz p2, :cond_2

    array-length v1, p0

    if-lt v1, p1, :cond_2

    array-length v1, p2

    if-ge v1, p1, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    move v1, v0

    :goto_1
    if-ge v1, p1, :cond_4

    aget v2, p0, v1

    aget v3, p2, v1

    if-ne v2, v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private q(I)V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lmaps/ay/m;->c:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lmaps/ay/m;->b:[I

    array-length v1, v1

    if-ge v1, v0, :cond_0

    iget v0, p0, Lmaps/ay/m;->c:I

    iget-object v1, p0, Lmaps/ay/m;->b:[I

    array-length v1, v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x3ff8000000000000L

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x3ff0000000000000L

    add-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    new-array v0, v0, [I

    iget-object v1, p0, Lmaps/ay/m;->b:[I

    iget-object v2, p0, Lmaps/ay/m;->b:[I

    array-length v2, v2

    invoke-static {v1, v5, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lmaps/ay/m;->b:[I

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)D
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->a(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public a(III)D
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/s;->a(III)D

    move-result-wide v0

    return-wide v0
.end method

.method public a(II)I
    .locals 1

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/s;->b(II)I

    move-result v0

    return v0
.end method

.method public a()Lmaps/ay/m;
    .locals 2

    new-instance v0, Lmaps/ay/m;

    iget-object v1, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-direct {v0, v1}, Lmaps/ay/m;-><init>(Lmaps/ay/s;)V

    return-object v0
.end method

.method public varargs a([I)V
    .locals 5

    array-length v0, p1

    invoke-direct {p0, v0}, Lmaps/ay/m;->q(I)V

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p1, v0

    iget-object v3, p0, Lmaps/ay/m;->b:[I

    iget v4, p0, Lmaps/ay/m;->c:I

    invoke-virtual {p0, v2}, Lmaps/ay/m;->o(I)I

    move-result v2

    aput v2, v3, v4

    iget v2, p0, Lmaps/ay/m;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/ay/m;->c:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lmaps/ay/m;

    return v0
.end method

.method public b(I)D
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->b(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public b(III)D
    .locals 8

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v1, p0, Lmaps/ay/m;->b:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Lmaps/ay/s;->a(I)D

    move-result-wide v0

    iget-object v2, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v3, p0, Lmaps/ay/m;->b:[I

    aget v3, v3, p2

    invoke-virtual {v2, v3}, Lmaps/ay/s;->a(I)D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget-object v2, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v3, p0, Lmaps/ay/m;->b:[I

    aget v3, v3, p3

    invoke-virtual {v2, v3}, Lmaps/ay/s;->b(I)D

    move-result-wide v2

    iget-object v4, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v5, p0, Lmaps/ay/m;->b:[I

    aget v5, v5, p2

    invoke-virtual {v4, v5}, Lmaps/ay/s;->b(I)D

    move-result-wide v4

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iget-object v2, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v3, p0, Lmaps/ay/m;->b:[I

    aget v3, v3, p1

    invoke-virtual {v2, v3}, Lmaps/ay/s;->b(I)D

    move-result-wide v2

    iget-object v4, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v5, p0, Lmaps/ay/m;->b:[I

    aget v5, v5, p2

    invoke-virtual {v4, v5}, Lmaps/ay/s;->b(I)D

    move-result-wide v4

    sub-double/2addr v2, v4

    iget-object v4, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v5, p0, Lmaps/ay/m;->b:[I

    aget v5, v5, p3

    invoke-virtual {v4, v5}, Lmaps/ay/s;->a(I)D

    move-result-wide v4

    iget-object v6, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v7, p0, Lmaps/ay/m;->b:[I

    aget v7, v7, p2

    invoke-virtual {v6, v7}, Lmaps/ay/s;->a(I)D

    move-result-wide v6

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    neg-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0}, Lmaps/ay/s;->d()I

    move-result v0

    return v0
.end method

.method public b(II)I
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->i(I)I

    move-result v0

    :goto_0
    iget v1, p0, Lmaps/ay/m;->c:I

    if-ge p2, v1, :cond_1

    invoke-virtual {p0, p2}, Lmaps/ay/m;->i(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_1
    return p2

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, -0x1

    goto :goto_1
.end method

.method public c(III)D
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/s;->b(III)D

    move-result-wide v0

    return-wide v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget v0, v0, Lmaps/ay/s;->b:I

    return v0
.end method

.method public c(I)I
    .locals 1

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->c(I)I

    move-result v0

    return v0
.end method

.method public c(II)Z
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {p0, p2}, Lmaps/ay/m;->i(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/s;->i(I)I

    move-result v0

    iget-object v1, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v1, p1}, Lmaps/ay/s;->i(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget v1, p0, Lmaps/ay/m;->c:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p0, v1}, Lmaps/ay/m;->i(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/s;->i(I)I

    move-result v0

    return v0
.end method

.method public d(I)I
    .locals 1

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->d(I)I

    move-result v0

    return v0
.end method

.method public d(II)V
    .locals 4

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lmaps/ay/m;->q(I)V

    iget-object v0, p0, Lmaps/ay/m;->b:[I

    iget-object v1, p0, Lmaps/ay/m;->b:[I

    add-int/lit8 v2, p1, 0x1

    iget v3, p0, Lmaps/ay/m;->c:I

    sub-int/2addr v3, p1

    invoke-static {v0, p1, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lmaps/ay/m;->b:[I

    invoke-virtual {p0, p2}, Lmaps/ay/m;->o(I)I

    move-result v1

    aput v1, v0, p1

    iget v0, p0, Lmaps/ay/m;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ay/m;->c:I

    return-void
.end method

.method public e(I)Lmaps/ay/t;
    .locals 1

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->e(I)Lmaps/ay/t;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/m;->c:I

    return-void
.end method

.method public e(II)V
    .locals 3

    iget-object v0, p0, Lmaps/ay/m;->b:[I

    invoke-virtual {p0, p1}, Lmaps/ay/m;->n(I)I

    move-result v1

    invoke-virtual {p0, p2}, Lmaps/ay/m;->o(I)I

    move-result v2

    aput v2, v0, v1

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/ay/m;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lmaps/ay/m;

    invoke-virtual {p1, p0}, Lmaps/ay/m;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    iget-object v3, p1, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmaps/ay/m;->b:[I

    iget v3, p0, Lmaps/ay/m;->c:I

    iget-object v4, p1, Lmaps/ay/m;->b:[I

    iget v5, p1, Lmaps/ay/m;->c:I

    invoke-static {v2, v3, v4, v5}, Lmaps/ay/m;->a([II[II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f(I)D
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {p0, p1}, Lmaps/ay/m;->i(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/s;->a(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public f()V
    .locals 1

    iget v0, p0, Lmaps/ay/m;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/ay/m;->c:I

    return-void
.end method

.method public g(I)D
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {p0, p1}, Lmaps/ay/m;->i(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/s;->b(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public h(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lmaps/ay/m;->i(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lmaps/ay/m;->b:[I

    iget v2, p0, Lmaps/ay/m;->c:I

    invoke-static {v1, v2}, Lmaps/ay/m;->a([II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method

.method public i(I)I
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->b:[I

    invoke-virtual {p0, p1}, Lmaps/ay/m;->n(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public j(I)I
    .locals 2

    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lmaps/ay/m;->i(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/s;->i(I)I

    move-result v0

    return v0
.end method

.method public k(I)Z
    .locals 3

    invoke-virtual {p0, p1}, Lmaps/ay/m;->j(I)I

    move-result v0

    iget-object v1, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {p0, p1}, Lmaps/ay/m;->i(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lmaps/ay/s;->d(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l(I)Z
    .locals 3

    invoke-virtual {p0, p1}, Lmaps/ay/m;->j(I)I

    move-result v0

    iget-object v1, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {p0, p1}, Lmaps/ay/m;->i(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lmaps/ay/s;->c(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m(I)V
    .locals 4

    iget-object v0, p0, Lmaps/ay/m;->b:[I

    add-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lmaps/ay/m;->b:[I

    iget v3, p0, Lmaps/ay/m;->c:I

    sub-int/2addr v3, p1

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lmaps/ay/m;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/ay/m;->c:I

    return-void
.end method

.method protected n(I)I
    .locals 2

    iget v0, p0, Lmaps/ay/m;->c:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/ay/m;->c:I

    rem-int v0, p1, v0

    if-gez v0, :cond_1

    iget v1, p0, Lmaps/ay/m;->c:I

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method protected o(I)I
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->i(I)I

    move-result v0

    goto :goto_0
.end method

.method p(I)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    iget v0, p0, Lmaps/ay/m;->c:I

    add-int/lit8 v2, v0, -0x1

    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    const-string v0, "[]"

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0x5b

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lmaps/ay/m;->b:[I

    aget v6, v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ne v0, v2, :cond_1

    const/16 v0, 0x5d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lmaps/ay/m;->p(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lmaps/ay/m;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmaps/ay/m;->a:Lmaps/ay/s;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
