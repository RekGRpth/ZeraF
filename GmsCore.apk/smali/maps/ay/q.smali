.class public Lmaps/ay/q;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ay/l;


# direct methods
.method constructor <init>(Lmaps/ay/l;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    return-void
.end method


# virtual methods
.method public a()D
    .locals 2

    iget-object v0, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v0}, Lmaps/ay/l;->e()D

    move-result-wide v0

    return-wide v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v0}, Lmaps/ay/l;->g()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v0}, Lmaps/ay/l;->h()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v0}, Lmaps/ay/l;->f()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v0}, Lmaps/ay/l;->i()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lmaps/ay/q;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lmaps/ay/q;

    iget-object v0, p1, Lmaps/ay/q;->a:Lmaps/ay/l;

    iget-object v1, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v0, v1}, Lmaps/ay/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method f()Lmaps/ay/l;
    .locals 1

    iget-object v0, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v0}, Lmaps/ay/l;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmaps/ay/q;->a:Lmaps/ay/l;

    invoke-virtual {v1}, Lmaps/ay/l;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
