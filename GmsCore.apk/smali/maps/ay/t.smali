.class public final enum Lmaps/ay/t;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/ay/t;

.field public static final enum b:Lmaps/ay/t;

.field public static final enum c:Lmaps/ay/t;

.field public static final enum d:Lmaps/ay/t;

.field public static final enum e:Lmaps/ay/t;

.field public static final enum f:Lmaps/ay/t;

.field public static final enum g:Lmaps/ay/t;

.field private static final synthetic h:[Lmaps/ay/t;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/ay/t;

    const-string v1, "SPLIT_VERTEX"

    invoke-direct {v0, v1, v3}, Lmaps/ay/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/t;->a:Lmaps/ay/t;

    new-instance v0, Lmaps/ay/t;

    const-string v1, "MERGE_VERTEX"

    invoke-direct {v0, v1, v4}, Lmaps/ay/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/t;->b:Lmaps/ay/t;

    new-instance v0, Lmaps/ay/t;

    const-string v1, "RIGHT_VERTEX"

    invoke-direct {v0, v1, v5}, Lmaps/ay/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/t;->c:Lmaps/ay/t;

    new-instance v0, Lmaps/ay/t;

    const-string v1, "LEFT_VERTEX"

    invoke-direct {v0, v1, v6}, Lmaps/ay/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/t;->d:Lmaps/ay/t;

    new-instance v0, Lmaps/ay/t;

    const-string v1, "START_VERTEX"

    invoke-direct {v0, v1, v7}, Lmaps/ay/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/t;->e:Lmaps/ay/t;

    new-instance v0, Lmaps/ay/t;

    const-string v1, "END_VERTEX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/ay/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/t;->f:Lmaps/ay/t;

    new-instance v0, Lmaps/ay/t;

    const-string v1, "INTERSECTION_VERTEX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/ay/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/t;->g:Lmaps/ay/t;

    const/4 v0, 0x7

    new-array v0, v0, [Lmaps/ay/t;

    sget-object v1, Lmaps/ay/t;->a:Lmaps/ay/t;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/ay/t;->b:Lmaps/ay/t;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/ay/t;->c:Lmaps/ay/t;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/ay/t;->d:Lmaps/ay/t;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/ay/t;->e:Lmaps/ay/t;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/ay/t;->f:Lmaps/ay/t;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/ay/t;->g:Lmaps/ay/t;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/ay/t;->h:[Lmaps/ay/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/ay/t;
    .locals 1

    const-class v0, Lmaps/ay/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/ay/t;

    return-object v0
.end method

.method public static values()[Lmaps/ay/t;
    .locals 1

    sget-object v0, Lmaps/ay/t;->h:[Lmaps/ay/t;

    invoke-virtual {v0}, [Lmaps/ay/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/ay/t;

    return-object v0
.end method
