.class public abstract Lmaps/an/g;
.super Lmaps/ak/e;


# instance fields
.field private final a:[Lmaps/an/v;

.field private b:I

.field private c:Lmaps/an/f;

.field private d:Ljava/util/Map;


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Lmaps/ak/e;-><init>()V

    new-array v0, p1, [Lmaps/an/v;

    iput-object v0, p0, Lmaps/an/g;->a:[Lmaps/an/v;

    const/4 v0, 0x0

    iput v0, p0, Lmaps/an/g;->b:I

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/an/g;->d:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lmaps/an/g;)Lmaps/an/f;
    .locals 1

    iget-object v0, p0, Lmaps/an/g;->c:Lmaps/an/f;

    return-object v0
.end method

.method static synthetic a(Lmaps/an/g;Lmaps/an/f;)Lmaps/an/f;
    .locals 0

    iput-object p1, p0, Lmaps/an/g;->c:Lmaps/an/f;

    return-object p1
.end method

.method static synthetic a(Lmaps/an/g;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lmaps/an/g;->d(I)Z

    move-result v0

    return v0
.end method

.method private d(I)Z
    .locals 2

    iget v0, p0, Lmaps/an/g;->b:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lmaps/an/g;->a:[Lmaps/an/v;

    array-length v1, v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/util/Pair;)Ljava/lang/Integer;
    .locals 3

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/Pair;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object p1, v0

    :cond_0
    iget-object v0, p0, Lmaps/an/g;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method protected abstract a(I)Lmaps/t/o;
.end method

.method protected final a(Landroid/util/Pair;Lmaps/an/v;)V
    .locals 3

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/Pair;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object p1, v0

    :cond_0
    iget-object v0, p0, Lmaps/an/g;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicate tile key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", already exists in batch for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/an/g;->d:Ljava/util/Map;

    iget v1, p0, Lmaps/an/g;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/an/g;->a:[Lmaps/an/v;

    iget v1, p0, Lmaps/an/g;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/an/g;->b:I

    aput-object p2, v0, v1

    return-void
.end method

.method protected a(Lmaps/an/v;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b(I)Lmaps/an/v;
    .locals 1

    iget-object v0, p0, Lmaps/an/g;->a:[Lmaps/an/v;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected c(I)[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lmaps/an/g;->b:I

    return v0
.end method

.method protected final f()Z
    .locals 2

    iget v0, p0, Lmaps/an/g;->b:I

    iget-object v1, p0, Lmaps/an/g;->a:[Lmaps/an/v;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method
