.class public Lmaps/an/m;
.super Ljava/lang/Object;


# instance fields
.field a:Lmaps/ag/a;

.field b:Lmaps/ag/s;

.field volatile c:Z

.field private d:I

.field private final e:Z

.field private f:Ljava/util/Locale;

.field private final g:Ljava/lang/String;

.field private h:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lmaps/ag/a;Lmaps/ag/s;ZLjava/util/Locale;Ljava/io/File;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/an/m;->g:Ljava/lang/String;

    iput-object p2, p0, Lmaps/an/m;->a:Lmaps/ag/a;

    iput-object p3, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    const/4 v0, -0x1

    iput v0, p0, Lmaps/an/m;->d:I

    iput-boolean p4, p0, Lmaps/an/m;->e:Z

    iput-object p5, p0, Lmaps/an/m;->f:Ljava/util/Locale;

    iput-object p6, p0, Lmaps/an/m;->h:Ljava/io/File;

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    iget-object v1, p0, Lmaps/an/m;->h:Ljava/io/File;

    invoke-interface {v0, v1}, Lmaps/ag/s;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/m;->g:Ljava/lang/String;

    const-string v1, "Unable to init disk cache"

    invoke-static {v0, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    :cond_1
    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/an/m;->f:Ljava/util/Locale;

    iget-object v1, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    invoke-interface {v1}, Lmaps/ag/s;->e()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    iget-object v1, p0, Lmaps/an/m;->f:Ljava/util/Locale;

    invoke-interface {v0, v1}, Lmaps/ag/s;->a(Ljava/util/Locale;)Z

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/m;->c:Z

    :cond_3
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(I)Z
    .locals 2

    invoke-virtual {p0}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmaps/ag/s;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput p1, p0, Lmaps/an/m;->d:I

    iget-boolean v1, p0, Lmaps/an/m;->e:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lmaps/ag/s;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_1
    iget-object v0, p0, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v0}, Lmaps/ag/a;->a()Z

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lmaps/ag/s;
    .locals 1

    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/an/m;->c:Z

    if-nez v0, :cond_1

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/an/m;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    monitor-exit p0

    :goto_1
    return-object v0

    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method protected c()V
    .locals 2

    iget-object v0, p0, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v0}, Lmaps/ag/a;->a()Z

    :cond_0
    invoke-virtual {p0}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lmaps/ag/s;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lmaps/ag/s;->c()V

    iget-object v0, p0, Lmaps/an/m;->g:Ljava/lang/String;

    const-string v1, "Unable to clear disk cache"

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/m;->b:Lmaps/ag/s;

    :cond_1
    return-void
.end method

.method d()I
    .locals 1

    invoke-virtual {p0}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmaps/ag/s;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/an/m;->d:I

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v0}, Lmaps/ag/a;->a()Z

    :cond_0
    return-void
.end method

.method g()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/m;->e:Z

    return v0
.end method
