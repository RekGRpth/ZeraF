.class public Lmaps/an/ac;
.super Lmaps/an/ae;


# static fields
.field public static final i:Lmaps/an/u;


# instance fields
.field private k:Z

.field private volatile l:Lmaps/an/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/an/d;

    invoke-direct {v0}, Lmaps/an/d;-><init>()V

    sput-object v0, Lmaps/an/ac;->i:Lmaps/an/u;

    return-void
.end method

.method public constructor <init>(Lmaps/ak/a;Lmaps/o/c;IFLjava/util/Locale;ZLjava/io/File;Lmaps/an/u;Lmaps/ag/g;)V
    .locals 10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lmaps/an/ae;-><init>(Lmaps/ak/a;Lmaps/o/c;IFLjava/util/Locale;ZLjava/io/File;Lmaps/ag/g;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/an/ac;->k:Z

    move-object/from16 v0, p8

    iput-object v0, p0, Lmaps/an/ac;->l:Lmaps/an/u;

    return-void
.end method

.method private b(Lmaps/t/ah;Lmaps/t/ar;)Z
    .locals 1

    iget-object v0, p0, Lmaps/an/ac;->l:Lmaps/an/u;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/an/ac;->l:Lmaps/an/u;

    invoke-interface {v0, p1, p2}, Lmaps/an/u;->a(Lmaps/t/ah;Lmaps/t/ar;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/ah;Lmaps/bx/b;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lmaps/an/ac;->a(Lmaps/t/ah;Lmaps/t/ar;Lmaps/bx/b;)V

    return-void
.end method

.method public a(Lmaps/t/ah;Lmaps/t/ar;Lmaps/bx/b;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lmaps/an/ac;->b(Lmaps/t/ah;Lmaps/t/ar;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p3}, Lmaps/an/ae;->a(Lmaps/t/ah;Lmaps/bx/b;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-interface {p3, p1, v0, v1}, Lmaps/bx/b;->a(Lmaps/t/ah;ILmaps/t/o;)V

    goto :goto_0
.end method

.method public a(Lmaps/t/ah;Lmaps/t/ar;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/ac;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lmaps/an/ac;->b(Lmaps/t/ah;Lmaps/t/ar;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
