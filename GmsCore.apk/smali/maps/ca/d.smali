.class Lmaps/ca/d;
.super Lmaps/be/i;


# instance fields
.field final synthetic a:Lmaps/ca/a;

.field private final c:Ljava/lang/Long;

.field private final d:Ljava/util/List;


# direct methods
.method public constructor <init>(Lmaps/ca/a;J)V
    .locals 1

    iput-object p1, p0, Lmaps/ca/d;->a:Lmaps/ca/a;

    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lmaps/be/i;-><init>(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ca/d;->d:Ljava/util/List;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmaps/ca/d;->c:Ljava/lang/Long;

    return-void
.end method

.method static synthetic a(Lmaps/ca/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ca/d;->d:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Lmaps/ca/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/ca/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 4

    iget-object v0, p0, Lmaps/ca/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/c;

    iget-object v2, v0, Lmaps/ca/c;->a:Lmaps/l/al;

    invoke-interface {v2, p1}, Lmaps/l/al;->a(Lmaps/cr/c;)V

    iget-object v2, p0, Lmaps/ca/d;->a:Lmaps/ca/a;

    iget v3, v0, Lmaps/ca/c;->b:I

    invoke-static {v2, v3}, Lmaps/ca/a;->a(Lmaps/ca/a;I)I

    iget-object v2, p0, Lmaps/ca/d;->a:Lmaps/ca/a;

    iget v0, v0, Lmaps/ca/c;->c:I

    invoke-static {v2, v0}, Lmaps/ca/a;->b(Lmaps/ca/a;I)I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/ca/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method protected a(Lmaps/t/ah;Lmaps/ca/c;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lmaps/be/i;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/ca/d;->a:Lmaps/ca/a;

    iget v1, p2, Lmaps/ca/c;->b:I

    invoke-static {v0, v1}, Lmaps/ca/a;->a(Lmaps/ca/a;I)I

    iget-object v0, p0, Lmaps/ca/d;->a:Lmaps/ca/a;

    iget v1, p2, Lmaps/ca/c;->c:I

    invoke-static {v0, v1}, Lmaps/ca/a;->b(Lmaps/ca/a;I)I

    iget-object v0, p2, Lmaps/ca/c;->a:Lmaps/l/al;

    if-eqz v0, :cond_0

    iput v2, p2, Lmaps/ca/c;->b:I

    iput v2, p2, Lmaps/ca/c;->c:I

    invoke-virtual {p0, p2}, Lmaps/ca/d;->a(Lmaps/ca/c;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lmaps/t/ah;

    check-cast p2, Lmaps/ca/c;

    invoke-virtual {p0, p1, p2}, Lmaps/ca/d;->a(Lmaps/t/ah;Lmaps/ca/c;)V

    return-void
.end method

.method public f()V
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lmaps/ca/d;->b()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lmaps/ca/d;->e()Lmaps/be/f;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Lmaps/be/f;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lmaps/be/f;->a()Lmaps/be/p;

    move-result-object v2

    iget-object v3, v2, Lmaps/be/p;->a:Ljava/lang/Object;

    sget-object v4, Lmaps/ca/a;->a:Lmaps/t/ah;

    if-ne v3, v4, :cond_1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {p0, v0}, Lmaps/ca/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-object v2, v2, Lmaps/be/p;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public g()V
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lmaps/ca/d;->b()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lmaps/ca/d;->e()Lmaps/be/f;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, Lmaps/be/f;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lmaps/be/f;->a()Lmaps/be/p;

    move-result-object v3

    iget-object v0, v3, Lmaps/be/p;->a:Ljava/lang/Object;

    sget-object v4, Lmaps/ca/a;->a:Lmaps/t/ah;

    if-ne v0, v4, :cond_1

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {p0, v0}, Lmaps/ca/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-object v0, v3, Lmaps/be/p;->b:Ljava/lang/Object;

    check-cast v0, Lmaps/ca/c;

    iget-object v4, v0, Lmaps/ca/c;->a:Lmaps/l/al;

    if-eqz v4, :cond_2

    invoke-interface {v4}, Lmaps/l/al;->a()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    iget-object v0, v3, Lmaps/be/p;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lmaps/ca/d;->c:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lmaps/cr/c;->b(J)Lmaps/cr/c;

    move-result-object v3

    invoke-interface {v4, v3}, Lmaps/l/al;->c(Lmaps/cr/c;)V

    iget-object v3, p0, Lmaps/ca/d;->a:Lmaps/ca/a;

    iget v4, v0, Lmaps/ca/c;->b:I

    invoke-static {v3, v4}, Lmaps/ca/a;->a(Lmaps/ca/a;I)I

    const/4 v3, 0x0

    iput v3, v0, Lmaps/ca/c;->b:I

    goto :goto_0

    :cond_4
    return-void
.end method

.method public h()Lmaps/be/p;
    .locals 2

    invoke-virtual {p0}, Lmaps/ca/d;->e()Lmaps/be/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/f;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/be/f;->a()Lmaps/be/p;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/ca/d;->a:Lmaps/ca/a;

    invoke-static {v0}, Lmaps/ca/a;->a(Lmaps/ca/a;)Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v4

    sget-object v0, Lmaps/ca/a;->a:Lmaps/t/ah;

    invoke-virtual {p0, v0}, Lmaps/ca/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/c;

    if-nez v0, :cond_0

    sget-object v6, Lmaps/ca/a;->a:Lmaps/t/ah;

    new-instance v0, Lmaps/ca/c;

    const/4 v1, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lmaps/ca/c;-><init>(Lmaps/l/al;IIJ)V

    invoke-virtual {p0, v6, v0}, Lmaps/ca/d;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iput-wide v4, v0, Lmaps/ca/c;->d:J

    goto :goto_0
.end method
