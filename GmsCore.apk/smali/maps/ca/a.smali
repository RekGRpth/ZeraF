.class public Lmaps/ca/a;
.super Ljava/lang/Object;


# static fields
.field protected static final a:Lmaps/t/ah;

.field private static b:Lmaps/ca/a;


# instance fields
.field private final c:Lmaps/ae/d;

.field private final d:Ljava/util/Map;

.field private final e:I

.field private final f:I

.field private g:I

.field private h:I

.field private i:J

.field private final j:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lmaps/t/ah;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2, v2}, Lmaps/t/ah;-><init>(III)V

    sput-object v0, Lmaps/ca/a;->a:Lmaps/t/ah;

    return-void
.end method

.method public constructor <init>(Lmaps/ae/d;II)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    iput v1, p0, Lmaps/ca/a;->g:I

    iput v1, p0, Lmaps/ca/a;->h:I

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/ca/a;->j:Ljava/util/List;

    iput-object p1, p0, Lmaps/ca/a;->c:Lmaps/ae/d;

    iput p2, p0, Lmaps/ca/a;->e:I

    iput p3, p0, Lmaps/ca/a;->f:I

    return-void
.end method

.method protected static a(I)I
    .locals 1

    mul-int/lit16 v0, p0, 0x400

    mul-int/lit16 v0, v0, 0x400

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method static synthetic a(Lmaps/ca/a;I)I
    .locals 1

    iget v0, p0, Lmaps/ca/a;->g:I

    sub-int/2addr v0, p1

    iput v0, p0, Lmaps/ca/a;->g:I

    return v0
.end method

.method static synthetic a(Lmaps/ca/a;)Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/ca/a;->c:Lmaps/ae/d;

    return-object v0
.end method

.method public static declared-synchronized a()Lmaps/ca/a;
    .locals 2

    const-class v0, Lmaps/ca/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/ca/a;->b:Lmaps/ca/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private a(JLandroid/util/Pair;)Lmaps/ca/d;
    .locals 2

    iget-object v0, p0, Lmaps/ca/a;->j:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ca/d;

    invoke-direct {v0, p0, p1, p2}, Lmaps/ca/d;-><init>(Lmaps/ca/a;J)V

    iget-object v1, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v1, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a(II)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ca/a;->g:I

    if-gt v0, p1, :cond_1

    iget v0, p0, Lmaps/ca/a;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v0, p2, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/d;

    invoke-virtual {v1}, Lmaps/ca/d;->h()Lmaps/be/p;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v1, v2, Lmaps/be/p;->a:Ljava/lang/Object;

    sget-object v5, Lmaps/ca/a;->a:Lmaps/t/ah;

    if-eq v1, v5, :cond_2

    new-instance v5, Lmaps/ca/b;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v2, Lmaps/be/p;->a:Ljava/lang/Object;

    check-cast v1, Lmaps/t/ah;

    iget-object v2, v2, Lmaps/be/p;->b:Ljava/lang/Object;

    check-cast v2, Lmaps/ca/c;

    invoke-direct {v5, v0, v1, v2}, Lmaps/ca/b;-><init>(Landroid/util/Pair;Lmaps/t/ah;Lmaps/ca/c;)V

    invoke-interface {v3, v5}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Lmaps/ca/a;->g:I

    if-gt v0, p1, :cond_5

    iget v0, p0, Lmaps/ca/a;->h:I

    if-le v0, p2, :cond_7

    :cond_5
    invoke-interface {v3}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/b;

    iget-object v1, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    iget-object v4, v0, Lmaps/ca/b;->a:Landroid/util/Pair;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/d;

    iget-object v4, v0, Lmaps/ca/b;->b:Lmaps/t/ah;

    invoke-virtual {v1, v4}, Lmaps/ca/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lmaps/ca/d;->b()I

    move-result v4

    if-nez v4, :cond_6

    invoke-static {v1}, Lmaps/ca/d;->a(Lmaps/ca/d;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, v0, Lmaps/ca/b;->a:Landroid/util/Pair;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v3, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lmaps/ca/d;->h()Lmaps/be/p;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v4, v1, Lmaps/be/p;->a:Ljava/lang/Object;

    sget-object v5, Lmaps/ca/a;->a:Lmaps/t/ah;

    if-eq v4, v5, :cond_4

    new-instance v4, Lmaps/ca/b;

    iget-object v5, v0, Lmaps/ca/b;->a:Landroid/util/Pair;

    iget-object v0, v1, Lmaps/be/p;->a:Ljava/lang/Object;

    check-cast v0, Lmaps/t/ah;

    iget-object v1, v1, Lmaps/be/p;->b:Ljava/lang/Object;

    check-cast v1, Lmaps/ca/c;

    invoke-direct {v4, v5, v0, v1}, Lmaps/ca/b;-><init>(Landroid/util/Pair;Lmaps/t/ah;Lmaps/ca/c;)V

    invoke-interface {v3, v4}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v2, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public static declared-synchronized a(Lmaps/ae/d;)V
    .locals 4

    const-class v1, Lmaps/ca/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/ca/a;->b:Lmaps/ca/a;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/af/w;->f()I

    move-result v0

    new-instance v2, Lmaps/ca/a;

    invoke-static {v0}, Lmaps/ca/a;->a(I)I

    move-result v3

    invoke-static {v0}, Lmaps/ca/a;->b(I)I

    move-result v0

    invoke-direct {v2, p0, v3, v0}, Lmaps/ca/a;-><init>(Lmaps/ae/d;II)V

    sput-object v2, Lmaps/ca/a;->b:Lmaps/ca/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected static b(I)I
    .locals 1

    mul-int/lit16 v0, p0, 0x400

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x10

    return v0
.end method

.method static synthetic b(Lmaps/ca/a;I)I
    .locals 1

    iget v0, p0, Lmaps/ca/a;->h:I

    sub-int/2addr v0, p1

    iput v0, p0, Lmaps/ca/a;->h:I

    return v0
.end method

.method private declared-synchronized b(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/ca/c;
    .locals 8

    const/4 v6, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    if-nez v0, :cond_1

    if-eqz p4, :cond_0

    invoke-direct {p0, v1, v2, v3}, Lmaps/ca/a;->a(JLandroid/util/Pair;)Lmaps/ca/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v6

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    move-object v0, v6

    goto :goto_0

    :cond_1
    move-object v7, v0

    :try_start_1
    invoke-virtual {v7, p3}, Lmaps/ca/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/c;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/ca/a;->c:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->b()J

    move-result-wide v1

    iput-wide v1, v0, Lmaps/ca/c;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    if-eqz p4, :cond_3

    :try_start_2
    new-instance v0, Lmaps/ca/c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lmaps/ca/a;->c:Lmaps/ae/d;

    invoke-interface {v4}, Lmaps/ae/d;->b()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lmaps/ca/c;-><init>(Lmaps/l/al;IIJ)V

    invoke-virtual {v7, p3, v0}, Lmaps/ca/d;->c(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 3

    mul-int/lit8 v0, p0, 0xa

    const/high16 v1, 0x100000

    invoke-static {v0, v1}, Lmaps/ah/g;->a(II)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v0, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    rem-int/lit8 v0, v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/l/al;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/ca/a;->b(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/ca/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lmaps/ca/c;->a:Lmaps/l/al;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(J)V
    .locals 1

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lmaps/ca/a;->i:J

    :cond_0
    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v2

    iget-object v0, p0, Lmaps/ca/a;->j:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v6, v2

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/d;

    invoke-virtual {v1}, Lmaps/ca/d;->a()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/d;

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lmaps/ca/d;->a(Lmaps/cr/c;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v2, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_2
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/ca/e;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/ca/d;->a()V

    invoke-virtual {v0, p1}, Lmaps/ca/d;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/ca/e;Ljava/util/List;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/ah;

    invoke-virtual {v0, v1}, Lmaps/ca/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/c;

    if-eqz v1, :cond_2

    iget-object v4, v1, Lmaps/ca/c;->a:Lmaps/l/al;

    if-eqz v4, :cond_2

    iget v4, v1, Lmaps/ca/c;->b:I

    if-nez v4, :cond_2

    iget v1, v1, Lmaps/ca/c;->c:I

    add-int/2addr v1, v2

    :goto_2
    move v2, v1

    goto :goto_1

    :cond_1
    iget v0, p0, Lmaps/ca/a;->e:I

    sub-int/2addr v0, v2

    iget v1, p0, Lmaps/ca/a;->f:I

    invoke-direct {p0, v0, v1}, Lmaps/ca/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Lmaps/l/al;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    if-nez v0, :cond_3

    invoke-direct {p0, v1, v2, v3}, Lmaps/ca/a;->a(JLandroid/util/Pair;)Lmaps/ca/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v1, p3}, Lmaps/ca/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/c;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lmaps/ca/c;->a:Lmaps/l/al;

    if-eqz v2, :cond_2

    new-instance v2, Lmaps/ca/c;

    invoke-direct {v2, v0}, Lmaps/ca/c;-><init>(Lmaps/ca/c;)V

    invoke-virtual {v1, v2}, Lmaps/ca/d;->a(Lmaps/ca/c;)V

    :cond_2
    iput-object p4, v0, Lmaps/ca/c;->a:Lmaps/l/al;

    invoke-interface {p4}, Lmaps/l/al;->g()I

    move-result v1

    iput v1, v0, Lmaps/ca/c;->b:I

    invoke-interface {p4}, Lmaps/l/al;->f()I

    move-result v1

    iput v1, v0, Lmaps/ca/c;->c:I

    iget v1, p0, Lmaps/ca/a;->g:I

    iget v2, v0, Lmaps/ca/c;->b:I

    add-int/2addr v1, v2

    iput v1, p0, Lmaps/ca/a;->g:I

    iget v1, p0, Lmaps/ca/a;->h:I

    iget v0, v0, Lmaps/ca/c;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ca/a;->h:I

    iget v0, p0, Lmaps/ca/a;->e:I

    iget v1, p0, Lmaps/ca/a;->f:I

    invoke-direct {p0, v0, v1}, Lmaps/ca/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public declared-synchronized a(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLTileCacheManager.onLowJavaAndNativeMemory("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    const-string v0, "critical"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ca/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_2

    iget v0, p0, Lmaps/ca/a;->g:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ca/a;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string v0, "warning"

    goto :goto_0

    :cond_2
    iget v0, p0, Lmaps/ca/a;->g:I

    iget v1, p0, Lmaps/ca/a;->h:I

    div-int/lit8 v1, v1, 0x2

    invoke-direct {p0, v0, v1}, Lmaps/ca/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lmaps/ca/a;->h:I

    invoke-direct {p0, v0, v1}, Lmaps/ca/a;->a(II)V

    return-void
.end method

.method public declared-synchronized b(Lmaps/cr/c;)V
    .locals 7

    monitor-enter p0

    if-nez p1, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v2

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v1, v5, v2

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/d;

    sget-object v5, Lmaps/ca/a;->a:Lmaps/t/ah;

    invoke-virtual {v1, v5}, Lmaps/ca/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    invoke-virtual {v0, p1}, Lmaps/ca/d;->a(Lmaps/cr/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/cr/c;Lmaps/ca/e;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/ca/d;->f()V

    invoke-virtual {v0}, Lmaps/ca/d;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/cr/c;Lmaps/ca/e;Ljava/util/List;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/ah;

    invoke-virtual {v0, v1}, Lmaps/ca/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/c;

    if-eqz v1, :cond_1

    iget-object v3, v1, Lmaps/ca/c;->a:Lmaps/l/al;

    if-eqz v3, :cond_1

    iget v3, v1, Lmaps/ca/c;->b:I

    if-nez v3, :cond_1

    iget-object v3, v1, Lmaps/ca/c;->a:Lmaps/l/al;

    invoke-interface {v3}, Lmaps/l/al;->g()I

    move-result v3

    iput v3, v1, Lmaps/ca/c;->b:I

    iget v3, p0, Lmaps/ca/a;->g:I

    iget v4, v1, Lmaps/ca/c;->b:I

    add-int/2addr v3, v4

    iput v3, p0, Lmaps/ca/a;->g:I

    iget v3, v1, Lmaps/ca/c;->c:I

    iget-object v4, v1, Lmaps/ca/c;->a:Lmaps/l/al;

    invoke-interface {v4}, Lmaps/l/al;->f()I

    move-result v4

    iput v4, v1, Lmaps/ca/c;->c:I

    iget v1, p0, Lmaps/ca/a;->h:I

    sub-int/2addr v1, v3

    add-int/2addr v1, v4

    iput v1, p0, Lmaps/ca/a;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget v0, p0, Lmaps/ca/a;->e:I

    iget v1, p0, Lmaps/ca/a;->f:I

    invoke-direct {p0, v0, v1}, Lmaps/ca/a;->a(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized c()Ljava/lang/String;
    .locals 5

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ca/d;

    invoke-virtual {v1}, Lmaps/ca/d;->b()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    const-string v4, " + "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "no"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v0, " tiles use "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lmaps/ca/a;->g:I

    invoke-static {v0}, Lmaps/ca/a;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ca/a;->e:I

    invoke-static {v1}, Lmaps/ca/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "M GL, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ca/a;->h:I

    invoke-static {v1}, Lmaps/ca/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ca/a;->f:I

    invoke-static {v1}, Lmaps/ca/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "M J+N"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized c(Lmaps/cr/c;Lmaps/ca/e;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    if-eqz v0, :cond_0

    sget-object v2, Lmaps/ca/a;->a:Lmaps/t/ah;

    invoke-virtual {v0, v2}, Lmaps/ca/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lmaps/ca/d;->g()V

    invoke-virtual {v0, p1}, Lmaps/ca/d;->a(Lmaps/cr/c;)V

    invoke-virtual {v0}, Lmaps/ca/d;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Lmaps/cr/c;Lmaps/ca/e;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/ca/d;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e(Lmaps/cr/c;Lmaps/ca/e;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, p0, Lmaps/ca/a;->d:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/d;

    if-nez v0, :cond_0

    invoke-direct {p0, v1, v2, v3}, Lmaps/ca/a;->a(JLandroid/util/Pair;)Lmaps/ca/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lmaps/ca/d;->i()V

    invoke-virtual {v0, p1}, Lmaps/ca/d;->a(Lmaps/cr/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
