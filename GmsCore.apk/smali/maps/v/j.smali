.class final Lmaps/v/j;
.super Ljava/util/AbstractQueue;


# instance fields
.field final a:Lmaps/v/t;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    new-instance v0, Lmaps/v/bq;

    invoke-direct {v0, p0}, Lmaps/v/bq;-><init>(Lmaps/v/j;)V

    iput-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    return-void
.end method


# virtual methods
.method public a()Lmaps/v/t;
    .locals 2

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    iget-object v1, p0, Lmaps/v/j;->a:Lmaps/v/t;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a(Lmaps/v/t;)Z
    .locals 2

    invoke-interface {p1}, Lmaps/v/t;->g()Lmaps/v/t;

    move-result-object v0

    invoke-interface {p1}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/v/r;->b(Lmaps/v/t;Lmaps/v/t;)V

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->g()Lmaps/v/t;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/v/r;->b(Lmaps/v/t;Lmaps/v/t;)V

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-static {p1, v0}, Lmaps/v/r;->b(Lmaps/v/t;Lmaps/v/t;)V

    const/4 v0, 0x1

    return v0
.end method

.method public b()Lmaps/v/t;
    .locals 2

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    iget-object v1, p0, Lmaps/v/j;->a:Lmaps/v/t;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lmaps/v/j;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lmaps/v/j;->a:Lmaps/v/t;

    if-eq v0, v1, :cond_0

    invoke-interface {v0}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v1

    invoke-static {v0}, Lmaps/v/r;->c(Lmaps/v/t;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    iget-object v1, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0, v1}, Lmaps/v/t;->c(Lmaps/v/t;)V

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    iget-object v1, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0, v1}, Lmaps/v/t;->d(Lmaps/v/t;)V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lmaps/v/t;

    invoke-interface {p1}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    sget-object v1, Lmaps/v/bv;->a:Lmaps/v/bv;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    iget-object v1, p0, Lmaps/v/j;->a:Lmaps/v/t;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lmaps/v/bp;

    invoke-virtual {p0}, Lmaps/v/j;->a()Lmaps/v/t;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lmaps/v/bp;-><init>(Lmaps/v/j;Lmaps/v/t;)V

    return-object v0
.end method

.method public bridge synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lmaps/v/t;

    invoke-virtual {p0, p1}, Lmaps/v/j;->a(Lmaps/v/t;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/v/j;->a()Lmaps/v/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic poll()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/v/j;->b()Lmaps/v/t;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lmaps/v/t;

    invoke-interface {p1}, Lmaps/v/t;->g()Lmaps/v/t;

    move-result-object v0

    invoke-interface {p1}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/v/r;->b(Lmaps/v/t;Lmaps/v/t;)V

    invoke-static {p1}, Lmaps/v/r;->c(Lmaps/v/t;)V

    sget-object v0, Lmaps/v/bv;->a:Lmaps/v/bv;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/v/j;->a:Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lmaps/v/j;->a:Lmaps/v/t;

    if-eq v0, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0}, Lmaps/v/t;->f()Lmaps/v/t;

    move-result-object v0

    goto :goto_0

    :cond_0
    return v1
.end method
