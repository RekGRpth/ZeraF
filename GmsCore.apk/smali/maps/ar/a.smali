.class public Lmaps/ar/a;
.super Lmaps/e/b;

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field protected b:Lmaps/e/d;

.field protected c:Lmaps/e/d;

.field private j:Ljava/lang/String;

.field private k:Landroid/location/Location;

.field private l:J

.field private final m:Z

.field private final n:Z


# direct methods
.method public constructor <init>(Lmaps/ae/d;Landroid/location/LocationManager;Z)V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lmaps/e/b;-><init>(ZLmaps/ae/d;Landroid/location/LocationManager;)V

    const-string v1, "cell"

    iput-object v1, p0, Lmaps/ar/a;->j:Ljava/lang/String;

    invoke-static {}, Lmaps/ar/a;->i()Z

    move-result v1

    iput-boolean v1, p0, Lmaps/ar/a;->n:Z

    if-eqz p3, :cond_0

    iget-boolean v1, p0, Lmaps/ar/a;->n:Z

    if-nez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lmaps/ar/a;->m:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/location/Location;Z)V
    .locals 9

    const/4 v1, 0x0

    if-eqz p1, :cond_a

    if-eqz p2, :cond_3

    iget-object v0, p0, Lmaps/ar/a;->c:Lmaps/e/d;

    :goto_0
    invoke-virtual {v0, p1}, Lmaps/e/d;->a(Landroid/location/Location;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ar/a;->f:Z

    invoke-virtual {p0}, Lmaps/ar/a;->o()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->a()J

    move-result-wide v3

    sget-boolean v0, Lmaps/ae/h;->s:Z

    if-eqz v0, :cond_d

    invoke-static {p1}, Lmaps/ar/a;->a(Landroid/location/Location;)Lmaps/t/bb;

    move-result-object v2

    :goto_1
    if-eqz p2, :cond_8

    const-string v0, "gps"

    iput-object v0, p0, Lmaps/ar/a;->j:Ljava/lang/String;

    iput-wide v3, p0, Lmaps/ar/a;->l:J

    sget-boolean v0, Lmaps/ae/h;->y:Z

    if-eqz v0, :cond_4

    iput-object p1, p0, Lmaps/ar/a;->k:Landroid/location/Location;

    iget-boolean v0, p0, Lmaps/ar/a;->n:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/ar/a;->a(Landroid/location/Location;)Lmaps/t/bb;

    move-result-object v2

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "GPS"

    const-string v1, "Mock location is used."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_2
    iget-object v0, p0, Lmaps/ar/a;->d:Lmaps/e/a;

    invoke-static {v0}, Lmaps/e/a;->a(Lmaps/e/a;)Lmaps/bl/a;

    move-result-object v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "GPS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FIX: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_3
    new-instance v0, Lmaps/e/r;

    invoke-direct {v0}, Lmaps/e/r;-><init>()V

    invoke-virtual {v0, p1}, Lmaps/e/r;->a(Landroid/location/Location;)Lmaps/e/r;

    move-result-object v0

    iget-object v1, p0, Lmaps/ar/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmaps/e/r;->a(Ljava/lang/String;)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/e/r;->a(Lmaps/t/bb;)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/r;->b()Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ar/a;->b(Lmaps/e/a;)V

    :cond_2
    :goto_4
    return-void

    :cond_3
    iget-object v0, p0, Lmaps/ar/a;->b:Lmaps/e/d;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/ar/a;->d:Lmaps/e/a;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/ar/a;->d:Lmaps/e/a;

    invoke-virtual {v0}, Lmaps/e/a;->getTime()J

    move-result-wide v0

    :goto_5
    invoke-virtual {p0}, Lmaps/ar/a;->j()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_5

    invoke-virtual {p0}, Lmaps/ar/a;->j()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v6

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_5

    iget-object v5, p0, Lmaps/ar/a;->k:Landroid/location/Location;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lmaps/ar/a;->k:Landroid/location/Location;

    invoke-virtual {v5, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v5

    invoke-virtual {p0}, Lmaps/ar/a;->j()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_5

    sub-long v0, v3, v0

    const-wide/16 v3, 0x1770

    cmp-long v0, v0, v3

    if-lez v0, :cond_0

    :cond_5
    iget-boolean v0, p0, Lmaps/ar/a;->n:Z

    if-eqz v0, :cond_c

    invoke-static {p1}, Lmaps/ar/a;->a(Landroid/location/Location;)Lmaps/t/bb;

    move-result-object v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_6

    const-string v1, "GPS"

    const-string v2, "Mock location is used."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_6
    iput-object p1, p0, Lmaps/ar/a;->k:Landroid/location/Location;

    move-object v2, v0

    goto/16 :goto_2

    :cond_7
    const-wide/16 v0, 0x0

    goto :goto_5

    :cond_8
    iget-wide v5, p0, Lmaps/ar/a;->l:J

    const-wide/32 v7, 0xea60

    add-long/2addr v5, v7

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    invoke-static {p1}, Lmaps/ar/a;->b(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/ar/a;->j:Ljava/lang/String;

    iget-object v0, p0, Lmaps/ar/a;->d:Lmaps/e/a;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/ar/a;->d:Lmaps/e/a;

    invoke-virtual {v0}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v0

    :goto_7
    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "NETWORK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FIX: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_7

    :cond_a
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_b

    const-string v0, "GPS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid GPS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-virtual {p0}, Lmaps/ar/a;->m()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lmaps/ar/a;->a(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ar/a;->f:Z

    goto/16 :goto_4

    :cond_c
    move-object v0, v2

    goto :goto_6

    :cond_d
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public static b(Landroid/location/Location;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "network"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "networkLocationType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v0, "networkLocationType"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static i()Z
    .locals 2

    const-string v0, "debug.gmm.usegpsonly"

    const-string v1, "false"

    invoke-static {v0, v1}, Lmaps/ae/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private p()V
    .locals 5

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/ar/a;->l:J

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/ar/a;->m:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    invoke-virtual {v1}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v1

    const-string v2, "network"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :cond_0
    invoke-virtual {p0}, Lmaps/ar/a;->o()Lmaps/ae/d;

    move-result-object v1

    invoke-interface {v1}, Lmaps/ae/d;->a()J

    move-result-wide v1

    if-eqz v0, :cond_1

    iget-object v3, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xafc80

    cmp-long v1, v1, v3

    if-gtz v1, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ar/a;->a(Landroid/location/Location;Z)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/ar/a;->k()V

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/e/k;)V
    .locals 0

    invoke-super {p0, p1}, Lmaps/e/b;->a(Lmaps/e/k;)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ar/a;->m:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Lmaps/e/b;->c()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/ar/a;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ar/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected g()V
    .locals 1

    iget-object v0, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    return-void
.end method

.method protected h()V
    .locals 7

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    new-instance v0, Lmaps/e/d;

    const-string v1, "n"

    invoke-direct {v0, v1}, Lmaps/e/d;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lmaps/ar/a;->b:Lmaps/e/d;

    new-instance v0, Lmaps/e/d;

    const-string v1, "g"

    invoke-direct {v0, v1}, Lmaps/e/d;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lmaps/ar/a;->c:Lmaps/e/d;

    invoke-direct {p0}, Lmaps/ar/a;->p()V

    iget-boolean v0, p0, Lmaps/ar/a;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_0
    iget-boolean v0, p0, Lmaps/ar/a;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    const-string v1, "network"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ar/a;->a:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_1
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lmaps/ar/a;->a(Landroid/location/Location;Z)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 1

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/ar/a;->p()V

    :cond_0
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 1

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    invoke-direct {p0}, Lmaps/ar/a;->p()V

    :cond_0
    return-void
.end method
