.class public Lmaps/z/bj;
.super Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate$Stub;


# static fields
.field public static final a:Lmaps/z/ba;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lmaps/cc/d;->U:I

    invoke-static {v0}, Lmaps/z/br;->b(I)Lmaps/z/ba;

    move-result-object v0

    sput-object v0, Lmaps/z/bj;->a:Lmaps/z/ba;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public defaultMarker()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    sget-object v0, Lmaps/z/bj;->a:Lmaps/z/ba;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public defaultMarkerWithHue(F)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    invoke-static {p1}, Lmaps/z/br;->a(F)Lmaps/z/ap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public fromAsset(Ljava/lang/String;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    invoke-static {p1}, Lmaps/z/br;->a(Ljava/lang/String;)Lmaps/z/ay;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    invoke-static {p1}, Lmaps/z/br;->a(Landroid/graphics/Bitmap;)Lmaps/z/aq;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public fromFile(Ljava/lang/String;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    invoke-static {p1}, Lmaps/z/br;->b(Ljava/lang/String;)Lmaps/z/ah;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public fromPath(Ljava/lang/String;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    invoke-static {p1}, Lmaps/z/br;->c(Ljava/lang/String;)Lmaps/z/m;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public fromResource(I)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    invoke-static {p1}, Lmaps/z/br;->a(I)Lmaps/z/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method
