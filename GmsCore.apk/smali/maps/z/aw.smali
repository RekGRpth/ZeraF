.class final Lmaps/z/aw;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/af/o;
.implements Lmaps/y/ar;
.implements Lmaps/z/x;


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lmaps/i/c;

.field private final c:Lmaps/i/h;

.field private final d:Ljava/util/concurrent/Executor;

.field private e:Lcom/google/android/gms/maps/internal/ICancelableCallback;

.field private f:Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;

.field private g:Ljava/util/Collection;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/z/aw;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/z/aw;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lmaps/i/h;Lmaps/i/c;Ljava/util/concurrent/Executor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/z/aw;->c:Lmaps/i/h;

    iput-object p2, p0, Lmaps/z/aw;->b:Lmaps/i/c;

    iput-object p3, p0, Lmaps/z/aw;->d:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Lmaps/i/h;Lmaps/i/c;Ljava/util/concurrent/Executor;)Lmaps/z/aw;
    .locals 1

    new-instance v0, Lmaps/z/aw;

    invoke-direct {v0, p0, p1, p2}, Lmaps/z/aw;-><init>(Lmaps/i/h;Lmaps/i/c;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method private a(Lcom/google/android/gms/maps/internal/ICancelableCallback;)V
    .locals 1

    sget-boolean v0, Lmaps/z/aw;->a:Z

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/z/aw;->h:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/z/aw;->e:Lcom/google/android/gms/maps/internal/ICancelableCallback;

    invoke-direct {p0}, Lmaps/z/aw;->d()V

    return-void
.end method

.method private a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;

    :try_start_0
    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;->onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lmaps/z/aw;->e:Lcom/google/android/gms/maps/internal/ICancelableCallback;

    if-eqz v0, :cond_1

    :try_start_1
    invoke-direct {p0}, Lmaps/z/aw;->e()Lcom/google/android/gms/maps/internal/ICancelableCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/ICancelableCallback;->onFinish()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    iget-object v0, p0, Lmaps/z/aw;->f:Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;

    if-eqz v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lmaps/z/aw;->f:Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;->onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    return-void

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method static synthetic a(Lmaps/z/aw;Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/z/aw;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-void
.end method

.method private d()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/z/aw;->e:Lcom/google/android/gms/maps/internal/ICancelableCallback;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/z/aw;->f:Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lmaps/z/aw;->b:Lmaps/i/c;

    if-eqz v0, :cond_2

    move-object v1, p0

    :goto_1
    invoke-interface {v3, v1}, Lmaps/i/c;->a(Lmaps/af/o;)V

    iget-object v1, p0, Lmaps/z/aw;->b:Lmaps/i/c;

    if-eqz v0, :cond_3

    :goto_2
    invoke-interface {v1, p0}, Lmaps/i/c;->a(Lmaps/y/ar;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_1

    :cond_3
    move-object p0, v2

    goto :goto_2
.end method

.method private e()Lcom/google/android/gms/maps/internal/ICancelableCallback;
    .locals 2

    iget-object v0, p0, Lmaps/z/aw;->e:Lcom/google/android/gms/maps/internal/ICancelableCallback;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/z/aw;->e:Lcom/google/android/gms/maps/internal/ICancelableCallback;

    invoke-direct {p0}, Lmaps/z/aw;->d()V

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lmaps/z/aw;->e:Lcom/google/android/gms/maps/internal/ICancelableCallback;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/z/aw;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/z/aw;->h:I

    :try_start_0
    invoke-direct {p0}, Lmaps/z/aw;->e()Lcom/google/android/gms/maps/internal/ICancelableCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/ICancelableCallback;->onCancel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lmaps/z/aw;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/z/aw;->h:I

    :cond_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iget v1, p0, Lmaps/z/aw;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lmaps/z/aw;->h:I

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lmaps/z/aw;->h:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Camera stopped during a cancellation"

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/z/aw;->b:Lmaps/i/c;

    invoke-interface {v0, v2, v2}, Lmaps/i/c;->a(FF)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    :cond_0
    iget-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lmaps/z/aw;->d()V

    return-void
.end method

.method public a(Lmaps/bq/a;Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/aw;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lmaps/z/p;

    invoke-direct {v1, p0, p1}, Lmaps/z/p;-><init>(Lmaps/z/aw;Lmaps/bq/a;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lmaps/z/b;ILcom/google/android/gms/maps/internal/ICancelableCallback;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Callback supplied with instantaneous camera movement"

    invoke-static {v0, v3}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    iget v0, p0, Lmaps/z/aw;->h:I

    if-nez v0, :cond_2

    :goto_1
    const-string v0, "Camera moved during a cancellation"

    invoke-static {v2, v0}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/z/aw;->c:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/aw;->b:Lmaps/i/c;

    invoke-virtual {p1, v0, v1, p2}, Lmaps/z/b;->a(Lmaps/i/h;Lmaps/i/c;I)V

    invoke-direct {p0, p3}, Lmaps/z/aw;->a(Lcom/google/android/gms/maps/internal/ICancelableCallback;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public b()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    iget-object v0, p0, Lmaps/z/aw;->b:Lmaps/i/c;

    invoke-interface {v0}, Lmaps/i/c;->a()Lmaps/bq/a;

    move-result-object v0

    invoke-static {v0}, Lmaps/au/c;->a(Lmaps/bq/a;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/aw;->g:Ljava/util/Collection;

    :cond_0
    invoke-direct {p0}, Lmaps/z/aw;->d()V

    return-void
.end method

.method public c()V
    .locals 0

    invoke-direct {p0}, Lmaps/z/aw;->f()V

    return-void
.end method

.method public c(Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;)V
    .locals 0

    iput-object p1, p0, Lmaps/z/aw;->f:Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;

    invoke-direct {p0}, Lmaps/z/aw;->d()V

    return-void
.end method
