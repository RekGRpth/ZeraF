.class Lmaps/z/ai;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/aa/b;


# instance fields
.field final synthetic a:Lmaps/z/ag;


# direct methods
.method constructor <init>(Lmaps/z/ag;)V
    .locals 0

    iput-object p1, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(F)F
    .locals 2

    iget-object v0, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v0}, Lmaps/z/ag;->c(Lmaps/z/ag;)F

    move-result v0

    iget-object v1, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v1}, Lmaps/z/ag;->d(Lmaps/z/ag;)F

    move-result v1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v0}, Lmaps/z/ag;->a(Lmaps/z/ag;)Lmaps/bf/b;

    move-result-object v0

    sget-object v1, Lmaps/bf/c;->aJ:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v0}, Lmaps/z/ag;->b(Lmaps/z/ag;)Lmaps/i/c;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v1}, Lmaps/z/ag;->b(Lmaps/z/ag;)Lmaps/i/c;

    move-result-object v1

    invoke-interface {v1}, Lmaps/i/c;->c()F

    move-result v1

    invoke-direct {p0, v1}, Lmaps/z/ai;->a(F)F

    move-result v1

    const/high16 v2, 0x3f800000

    add-float/2addr v1, v2

    const/16 v2, 0x14a

    invoke-interface {v0, v1, v2}, Lmaps/i/c;->b(FI)F

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v0}, Lmaps/z/ag;->a(Lmaps/z/ag;)Lmaps/bf/b;

    move-result-object v0

    sget-object v1, Lmaps/bf/c;->aK:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v0}, Lmaps/z/ag;->b(Lmaps/z/ag;)Lmaps/i/c;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/ai;->a:Lmaps/z/ag;

    invoke-static {v1}, Lmaps/z/ag;->b(Lmaps/z/ag;)Lmaps/i/c;

    move-result-object v1

    invoke-interface {v1}, Lmaps/i/c;->c()F

    move-result v1

    invoke-direct {p0, v1}, Lmaps/z/ai;->a(F)F

    move-result v1

    const/high16 v2, 0x3f800000

    sub-float/2addr v1, v2

    const/16 v2, 0x14a

    invoke-interface {v0, v1, v2}, Lmaps/i/c;->b(FI)F

    return-void
.end method
