.class public Lmaps/z/bo;
.super Lcom/google/android/gms/maps/internal/IMapViewDelegate$Stub;


# instance fields
.field private a:Lmaps/z/ag;

.field private final b:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/IMapViewDelegate$Stub;-><init>()V

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lmaps/z/bo;->c:Landroid/content/Context;

    iput-object p2, p0, Lmaps/z/bo;->b:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method public getMap()Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;
    .locals 1

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    return-object v0
.end method

.method public final getView()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->f()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/z/bo;->c:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/z/bo;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lmaps/au/a;->a(Landroid/app/Activity;)Z

    move-result v0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/z/bo;->c:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v2, p0, Lmaps/z/bo;->b:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {v0, v2, v1}, Lmaps/z/ag;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;Z)Lmaps/z/ag;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->a(Landroid/os/Bundle;)V

    return-void

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 1

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->e()V

    return-void
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->d()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->c()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/bo;->a:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->b(Landroid/os/Bundle;)V

    return-void
.end method
