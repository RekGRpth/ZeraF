.class public Lmaps/z/az;
.super Ljava/lang/Object;


# static fields
.field static final a:Lmaps/l/al;


# instance fields
.field final b:Ljava/util/Set;

.field private final c:Lmaps/ca/e;

.field private final d:Lcom/google/android/gms/maps/model/TileProvider;

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;

.field private final f:Ljava/util/Set;

.field private volatile g:Lmaps/cr/c;

.field private final h:Lmaps/ca/a;

.field private volatile i:Lmaps/z/au;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/l/n;

    invoke-direct {v0}, Lmaps/l/n;-><init>()V

    sput-object v0, Lmaps/z/az;->a:Lmaps/l/al;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/maps/model/TileProvider;Ljava/lang/String;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/az;->f:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/az;->b:Ljava/util/Set;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/z/az;->d:Lcom/google/android/gms/maps/model/TileProvider;

    const-string v0, "to"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lmaps/z/az;->a(I)Lmaps/be/n;

    move-result-object v0

    new-instance v1, Lmaps/ca/e;

    sget-object v2, Lmaps/o/c;->t:Lmaps/o/c;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lmaps/ca/e;-><init>(Lmaps/o/c;Ljava/util/Set;Lmaps/be/n;)V

    iput-object v1, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    iput-object p3, p0, Lmaps/z/az;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lmaps/co/a;

    invoke-direct {v0}, Lmaps/co/a;-><init>()V

    invoke-static {v0}, Lmaps/ca/a;->a(Lmaps/ae/d;)V

    invoke-static {}, Lmaps/ca/a;->a()Lmaps/ca/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    return-void
.end method

.method static synthetic a(Lmaps/z/az;)Lcom/google/android/gms/maps/model/TileProvider;
    .locals 1

    iget-object v0, p0, Lmaps/z/az;->d:Lcom/google/android/gms/maps/model/TileProvider;

    return-object v0
.end method

.method static a(I)Lmaps/be/n;
    .locals 6

    const/16 v5, 0x20

    const/4 v3, 0x0

    new-array v4, v5, [I

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v5, :cond_0

    const/4 v0, 0x1

    shl-int/2addr v0, v2

    and-int/2addr v0, p0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    aput v2, v4, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    new-array v0, v1, [I

    invoke-static {v4, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0}, Lmaps/be/n;->a([I)Lmaps/be/n;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(Lmaps/t/ah;Z)Lmaps/l/al;
    .locals 3

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2, p1, p2}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/l/al;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lmaps/t/ah;Lmaps/l/al;Lmaps/cr/c;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v0, p3, v1, p1, p2}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Lmaps/l/al;)V

    :cond_0
    iget-object v0, p0, Lmaps/z/az;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    if-eqz p2, :cond_1

    sget-object v0, Lmaps/z/az;->a:Lmaps/l/al;

    if-eq p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lmaps/z/az;->b(Lmaps/t/ah;Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/t/ah;Lmaps/t/bz;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    iget-object v1, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-static {p2, v1}, Lmaps/l/r;->a(Lmaps/t/o;Lmaps/cr/c;)Lmaps/l/r;

    move-result-object v0

    :cond_0
    invoke-direct {p0, p1, v0, v1}, Lmaps/z/az;->a(Lmaps/t/ah;Lmaps/l/al;Lmaps/cr/c;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/z/az;Lmaps/t/ah;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/z/az;->c(Lmaps/t/ah;)V

    return-void
.end method

.method static synthetic a(Lmaps/z/az;Lmaps/t/ah;Lmaps/t/bz;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/z/az;->a(Lmaps/t/ah;Lmaps/t/bz;)V

    return-void
.end method

.method static synthetic b(Lmaps/z/az;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    iget-object v0, p0, Lmaps/z/az;->e:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method private b(Lmaps/t/ah;Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/az;->i:Lmaps/z/au;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lmaps/z/au;->a(Lmaps/t/ah;Z)V

    :cond_0
    return-void
.end method

.method private c(Lmaps/t/ah;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    sget-object v1, Lmaps/z/az;->a:Lmaps/l/al;

    invoke-direct {p0, p1, v1, v0}, Lmaps/z/az;->a(Lmaps/t/ah;Lmaps/l/al;Lmaps/cr/c;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/ah;)Lmaps/l/al;
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/z/az;->a(Lmaps/t/ah;Z)Lmaps/l/al;

    move-result-object v0

    sget-object v1, Lmaps/z/az;->a:Lmaps/l/al;

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method public a()V
    .locals 3

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2}, Lmaps/ca/a;->e(Lmaps/cr/c;Lmaps/ca/e;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    sget-object v3, Lmaps/z/az;->a:Lmaps/l/al;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Ljava/util/List;)V

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "state must not be null."

    invoke-static {v0, v1}, Lmaps/ap/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/z/au;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/z/az;->i:Lmaps/z/au;

    if-nez v2, :cond_1

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    :goto_1
    iput-object p1, p0, Lmaps/z/az;->i:Lmaps/z/au;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    :goto_2
    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public a(Z)V
    .locals 3

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v1, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v1, v0, v2}, Lmaps/ca/a;->b(Lmaps/cr/c;Lmaps/ca/e;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v1, v0, v2}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;)V

    goto :goto_0
.end method

.method public b(Lmaps/t/ah;)Lmaps/l/al;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lmaps/z/az;->a(Lmaps/t/ah;Z)Lmaps/l/al;

    move-result-object v1

    sget-object v2, Lmaps/z/az;->a:Lmaps/l/al;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz v1, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lmaps/z/az;->f:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/z/az;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lmaps/z/bx;

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-direct {v2, p0, p1, v3}, Lmaps/z/bx;-><init>(Lmaps/z/az;Lmaps/t/ah;Ljava/util/Random;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    sget-boolean v0, Lmaps/bm/b;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2}, Lmaps/ca/a;->d(Lmaps/cr/c;Lmaps/ca/e;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/util/List;)V
    .locals 4

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    sget-object v3, Lmaps/z/az;->a:Lmaps/l/al;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/ca/a;->b(Lmaps/cr/c;Lmaps/ca/e;Ljava/util/List;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    invoke-virtual {v0, p1}, Lmaps/ca/a;->a(Z)V

    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/z/az;->g:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/z/az;->c:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2}, Lmaps/ca/a;->c(Lmaps/cr/c;Lmaps/ca/e;)V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    iget-object v1, p0, Lmaps/z/az;->b:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/az;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/bx;

    invoke-virtual {v0}, Lmaps/z/bx;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/az;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lmaps/z/az;->h:Lmaps/ca/a;

    invoke-virtual {v0}, Lmaps/ca/a;->b()V

    return-void
.end method
