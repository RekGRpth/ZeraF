.class public Lmaps/h/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field volatile a:Z

.field final b:Landroid/os/Handler;

.field private c:I

.field private final d:Landroid/hardware/SensorManager;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/os/Handler;ILandroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lmaps/h/a;->a:Z

    iput-object p1, p0, Lmaps/h/a;->b:Landroid/os/Handler;

    iput p2, p0, Lmaps/h/a;->c:I

    const-string v0, "sensor"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lmaps/h/a;->d:Landroid/hardware/SensorManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/h/a;->e:Ljava/util/List;

    iget-object v0, p0, Lmaps/h/a;->d:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lmaps/h/a;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private c()V
    .locals 5

    iget-object v0, p0, Lmaps/h/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    iget-object v2, p0, Lmaps/h/a;->d:Landroid/hardware/SensorManager;

    const/4 v3, 0x1

    iget-object v4, p0, Lmaps/h/a;->b:Landroid/os/Handler;

    invoke-virtual {v2, p0, v0, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Lmaps/h/a;->d:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-direct {p0}, Lmaps/h/a;->c()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/h/a;->a:Z

    return-void
.end method

.method public b()V
    .locals 1

    invoke-direct {p0}, Lmaps/h/a;->d()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/h/a;->a:Z

    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2

    iget-boolean v0, p0, Lmaps/h/a;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/a;->b:Landroid/os/Handler;

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "LOCATION"

    const-string v1, "Warning: handler is null in sensorChange."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/h/a;->b:Landroid/os/Handler;

    iget v1, p0, Lmaps/h/a;->c:I

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lmaps/h/a;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
