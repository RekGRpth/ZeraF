.class public Lmaps/ag/l;
.super Ljava/lang/Object;


# static fields
.field static final a:I

.field static final b:I

.field static final synthetic c:Z

.field private static final d:[B

.field private static e:I


# instance fields
.field private A:Lmaps/ag/ak;

.field private B:Lmaps/ag/aj;

.field private C:Ljava/util/Set;

.field private D:I

.field private final f:Ljava/lang/String;

.field private final g:Lmaps/bv/c;

.field private h:Lmaps/bv/a;

.field private i:Lmaps/ag/v;

.field private final j:Lmaps/ag/n;

.field private final k:Lmaps/ag/x;

.field private final l:[Lmaps/bv/a;

.field private final m:Lmaps/be/i;

.field private final n:Ljava/util/Set;

.field private final o:Ljava/util/concurrent/locks/ReentrantLock;

.field private final p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lmaps/ag/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/ag/l;->c:Z

    new-array v0, v1, [B

    sput-object v0, Lmaps/ag/l;->d:[B

    const/16 v0, 0x14

    sput v0, Lmaps/ag/l;->a:I

    const v0, 0x13f88

    sput v0, Lmaps/ag/l;->b:I

    sget v0, Lmaps/ag/l;->a:I

    sput v0, Lmaps/ag/l;->e:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Lmaps/ag/v;Lmaps/ag/n;Lmaps/ag/x;Lmaps/bv/a;Lmaps/bv/c;Lmaps/ag/aj;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v2, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v2, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v2, v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    iput-object v2, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    const/4 v2, -0x1

    iput v2, p0, Lmaps/ag/l;->y:I

    iput-boolean v0, p0, Lmaps/ag/l;->z:Z

    iput-object v3, p0, Lmaps/ag/l;->A:Lmaps/ag/ak;

    iput-object v3, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    const/4 v2, 0x4

    iput v2, p0, Lmaps/ag/l;->D:I

    iput-object p1, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    iput-object p2, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iput-object p3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iput-object p4, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    iput-object p5, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    iput-object p6, p0, Lmaps/ag/l;->g:Lmaps/bv/c;

    iget v2, p2, Lmaps/ag/v;->d:I

    new-array v2, v2, [Lmaps/bv/a;

    iput-object v2, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    new-instance v2, Lmaps/be/i;

    const/16 v3, 0x800

    invoke-virtual {p0}, Lmaps/ag/l;->e()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-direct {v2, v3}, Lmaps/be/i;-><init>(I)V

    iput-object v2, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    iput-object p7, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    iget-object v2, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    move v2, v0

    :goto_0
    :try_start_0
    iget-object v3, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v3, v3, Lmaps/ag/v;->d:I

    if-ge v2, v3, :cond_2

    iget-object v3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v3, v2}, Lmaps/ag/n;->c(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v3, v2}, Lmaps/ag/n;->d(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    invoke-virtual {v3, v2}, Lmaps/ag/x;->c(I)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rebuilding inconsistent shard: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ag/l;->a(Ljava/lang/String;)V

    iget v0, p0, Lmaps/ag/l;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ag/l;->r:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, v2}, Lmaps/ag/l;->d(I)Lmaps/ag/w;

    move-result-object v0

    iget-object v3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v3, v0}, Lmaps/ag/n;->a(Lmaps/ag/w;)V

    iget-object v3, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    invoke-virtual {v3, v0}, Lmaps/ag/x;->a(Lmaps/ag/w;)V

    invoke-direct {p0, v2}, Lmaps/ag/l;->f(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rebuilding shard: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v2}, Lmaps/ag/l;->g(I)V

    move v0, v1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lmaps/ag/l;->n()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0, v1}, Lmaps/ag/x;->a(Ljava/util/concurrent/locks/ReentrantReadWriteLock;)V

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0, v1}, Lmaps/ag/n;->a(Ljava/util/concurrent/locks/ReentrantReadWriteLock;)V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private static a(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x10

    add-int/2addr v0, p1

    return v0
.end method

.method private a(Z)I
    .locals 3

    const/4 v2, -0x1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v1, v1, Lmaps/ag/n;->g:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v1, v0}, Lmaps/ag/n;->d(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v1, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lmaps/ag/n;->a(Ljava/util/Set;)I

    move-result v0

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v0}, Lmaps/ag/l;->g(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v0, v0, Lmaps/ag/n;->g:I

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v1, v1, Lmaps/ag/v;->d:I

    if-ge v0, v1, :cond_4

    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v0}, Lmaps/ag/n;->b()I

    move-result v0

    iget-object v1, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    invoke-virtual {v1, v0}, Lmaps/ag/x;->b(I)V

    iget-object v1, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lmaps/ag/x;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0

    :cond_4
    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v1, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lmaps/ag/n;->a(Ljava/util/Set;)I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v0}, Lmaps/ag/l;->g(I)V

    goto :goto_1
.end method

.method public static a([BI)I
    .locals 4

    add-int/lit8 v0, p1, 0x1

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v1, v1, 0x18

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v3

    return v0
.end method

.method private a(Lmaps/ag/r;I)Lmaps/ag/ac;
    .locals 11

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/ag/r;->a()J

    move-result-wide v2

    iget v0, p0, Lmaps/ag/l;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ag/l;->u:I

    :try_start_0
    invoke-direct {p0, p2}, Lmaps/ag/l;->d(I)Lmaps/ag/w;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ag/w;->b()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, Lmaps/ag/w;->f(I)Lmaps/ag/d;

    move-result-object v6

    iget-wide v7, v6, Lmaps/ag/d;->a:J

    const-wide/16 v9, -0x1

    cmp-long v0, v7, v9

    if-eqz v0, :cond_3

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-wide v7, v6, Lmaps/ag/d;->a:J

    invoke-virtual {p1}, Lmaps/ag/r;->a()J

    move-result-wide v9

    cmp-long v0, v7, v9

    if-nez v0, :cond_3

    invoke-direct {p0, v6, p1}, Lmaps/ag/l;->a(Lmaps/ag/d;Lmaps/ag/r;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lmaps/ag/ac;

    invoke-direct {v0, v3, v6, v2}, Lmaps/ag/ac;-><init>(Lmaps/ag/w;Lmaps/ag/d;I)V

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lookupShardRecordIndexFromShard: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    :try_start_1
    invoke-direct {p0, p2}, Lmaps/ag/l;->e(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :goto_2
    return-object v1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    throw v0

    :cond_0
    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/d;

    iget-object v3, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v3

    :try_start_2
    iget-object v4, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    iget-wide v5, v0, Lmaps/ag/d;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v3

    goto :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    iget-object v2, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    iget-object v3, v1, Lmaps/ag/ac;->b:Lmaps/ag/d;

    iget-wide v3, v3, Lmaps/ag/d;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v1, Lmaps/ag/ac;->b:Lmaps/ag/d;

    invoke-virtual {v0, v3, v4}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2

    goto :goto_2

    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    :cond_2
    iget v0, p0, Lmaps/ag/l;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ag/l;->t:I

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto/16 :goto_1
.end method

.method private a(Lmaps/ag/r;)Lmaps/ag/d;
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/ag/r;->a()J

    move-result-wide v2

    iget-object v4, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/d;

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1}, Lmaps/ag/l;->a(Lmaps/ag/d;Lmaps/ag/r;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object v0, v1

    :cond_0
    if-eqz v0, :cond_1

    iget-object v4, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v5, v0, Lmaps/ag/d;->g:I

    invoke-virtual {v4, v5}, Lmaps/ag/n;->d(I)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v4

    :try_start_1
    iget-object v0, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/be/i;->c(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    :cond_1
    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/ag/l;->b(Lmaps/ag/r;)Lmaps/ag/ac;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, v1, Lmaps/ag/ac;->b:Lmaps/ag/d;

    :cond_2
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;IILjava/util/Locale;Lmaps/bv/c;Lmaps/ag/aj;)Lmaps/ag/l;
    .locals 8

    const/4 v2, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    sget p1, Lmaps/ag/l;->b:I

    move v0, v1

    :cond_0
    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, -0x2

    if-ne p1, v3, :cond_2

    const/16 p1, 0x2fee

    move v3, v1

    move v0, p1

    :goto_0
    if-ge v0, v2, :cond_1

    move v0, v2

    :cond_1
    if-nez v3, :cond_3

    sget v1, Lmaps/ag/l;->b:I

    if-le v0, v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Number of records must be between 4 and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lmaps/ag/l;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v3, -0x3

    if-ne p1, v3, :cond_4

    const p1, 0x28d71

    move v3, v1

    move v0, p1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v0, -0x1

    div-int/lit16 v1, v1, 0x199

    add-int/lit8 v1, v1, 0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x1

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lmaps/ag/l;->a(Ljava/lang/String;IIZILjava/util/Locale;Lmaps/bv/c;Lmaps/ag/aj;)Lmaps/ag/l;

    move-result-object v0

    return-object v0

    :cond_4
    move v3, v0

    move v0, p1

    goto :goto_0
.end method

.method static a(Ljava/lang/String;IIZILjava/util/Locale;Lmaps/bv/c;Lmaps/ag/aj;)Lmaps/ag/l;
    .locals 12

    sget-boolean v1, Lmaps/ag/l;->c:Z

    if-nez v1, :cond_1

    const/4 v1, 0x4

    if-lt p1, v1, :cond_0

    const/16 v1, 0x199

    if-le p1, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_1
    sget-boolean v1, Lmaps/ag/l;->c:Z

    if-nez v1, :cond_2

    if-nez p3, :cond_2

    const/16 v1, 0x23

    if-le p1, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_2
    sget-boolean v1, Lmaps/ag/l;->c:Z

    if-nez v1, :cond_4

    const/4 v1, 0x1

    if-lt p2, v1, :cond_3

    const/16 v1, 0x199

    if-le p2, v1, :cond_4

    :cond_3
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-interface {v0, v1}, Lmaps/bv/c;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v1, v2}, Lmaps/bv/c;->a(Ljava/lang/String;Z)Lmaps/bv/a;

    move-result-object v11

    new-instance v1, Lmaps/ag/v;

    invoke-static {}, Lmaps/ag/l;->j()I

    move-result v2

    invoke-static {}, Lmaps/ag/l;->k()I

    move-result v3

    invoke-static {v2, v3}, Lmaps/ag/l;->a(II)I

    move-result v2

    const/16 v3, 0x2000

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v4

    invoke-interface {v4}, Lmaps/ae/d;->a()J

    move-result-wide v8

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lmaps/ag/v;-><init>(IIIIZIJLjava/util/Locale;)V

    new-instance v5, Lmaps/ag/n;

    invoke-direct {v5, p1}, Lmaps/ag/n;-><init>(I)V

    new-instance v6, Lmaps/ag/x;

    const/4 v2, 0x0

    invoke-direct {v6, p1, v2}, Lmaps/ag/x;-><init>(II)V

    invoke-static {v1, v5, v6, v11}, Lmaps/ag/l;->a(Lmaps/ag/v;Lmaps/ag/n;Lmaps/ag/x;Lmaps/bv/a;)V

    invoke-interface {v11}, Lmaps/bv/a;->b()V

    new-instance v2, Lmaps/ag/l;

    move-object v3, p0

    move-object v4, v1

    move-object v7, v11

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lmaps/ag/l;-><init>(Ljava/lang/String;Lmaps/ag/v;Lmaps/ag/n;Lmaps/ag/x;Lmaps/bv/a;Lmaps/bv/c;Lmaps/ag/aj;)V

    return-object v2
.end method

.method public static a(Ljava/lang/String;Lmaps/bv/c;Lmaps/ag/aj;)Lmaps/ag/l;
    .locals 11

    const v3, 0xffff

    const/16 v4, 0x2000

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lmaps/bv/c;->a(Ljava/lang/String;Z)Lmaps/bv/a;

    move-result-object v5

    new-array v0, v4, [B

    invoke-interface {v5, v0}, Lmaps/bv/a;->b([B)V

    new-instance v2, Lmaps/ag/v;

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lmaps/ag/v;-><init>([BI)V

    invoke-static {}, Lmaps/ag/l;->k()I

    move-result v0

    invoke-static {}, Lmaps/ag/l;->j()I

    move-result v8

    invoke-static {v8, v0}, Lmaps/ag/l;->a(II)I

    move-result v9

    iget v1, v2, Lmaps/ag/v;->a:I

    shr-int/lit8 v1, v1, 0x10

    and-int v10, v1, v3

    iget v1, v2, Lmaps/ag/v;->a:I

    and-int/2addr v1, v3

    if-nez v10, :cond_0

    if-eq v1, v0, :cond_0

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid Cache Header(1): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; cached sever schema is zero but client schema part doesn\'t match:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " cachedClientSchema = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", expectedClientSchema = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    if-eqz v10, :cond_1

    iget v0, v2, Lmaps/ag/v;->a:I

    if-ne v0, v9, :cond_2

    :cond_1
    iget v0, v2, Lmaps/ag/v;->c:I

    if-eq v0, v4, :cond_3

    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid Cache Header(2): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", expect expectedSchema="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mBlockSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v3, Lmaps/ag/n;

    iget v0, v2, Lmaps/ag/v;->d:I

    invoke-direct {v3, v0}, Lmaps/ag/n;-><init>(I)V

    invoke-virtual {v3, v5}, Lmaps/ag/n;->b(Lmaps/bv/a;)V

    new-instance v4, Lmaps/ag/x;

    iget v0, v2, Lmaps/ag/v;->d:I

    iget v1, v3, Lmaps/ag/n;->g:I

    invoke-direct {v4, v0, v1}, Lmaps/ag/x;-><init>(II)V

    invoke-virtual {v4, v5}, Lmaps/ag/x;->b(Lmaps/bv/a;)V

    new-instance v0, Lmaps/ag/l;

    move-object v1, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lmaps/ag/l;-><init>(Ljava/lang/String;Lmaps/ag/v;Lmaps/ag/n;Lmaps/ag/x;Lmaps/bv/a;Lmaps/bv/c;Lmaps/ag/aj;)V

    if-nez v10, :cond_4

    if-eqz v8, :cond_4

    iget-object v1, v0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v1, v1, Lmaps/ag/v;->g:I

    invoke-direct {v0, v1, v9}, Lmaps/ag/l;->b(II)V

    :cond_4
    return-object v0
.end method

.method public static a(JLjava/lang/String;I[B)Lmaps/ag/m;
    .locals 3

    new-instance v0, Lmaps/ag/m;

    invoke-static {p0, p1, p2}, Lmaps/ag/l;->c(JLjava/lang/String;)Lmaps/ag/r;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p3, p4, v2}, Lmaps/ag/m;-><init>(Lmaps/ag/r;I[BLmaps/ag/q;)V

    return-object v0
.end method

.method public static a(JLjava/lang/String;[B)Lmaps/ag/m;
    .locals 3

    new-instance v0, Lmaps/ag/m;

    invoke-static {p0, p1, p2}, Lmaps/ag/l;->c(JLjava/lang/String;)Lmaps/ag/r;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p3, v2}, Lmaps/ag/m;-><init>(Lmaps/ag/r;[BLmaps/ag/q;)V

    return-object v0
.end method

.method public static a(J[B)Lmaps/ag/m;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lmaps/ag/l;->a(JLjava/lang/String;[B)Lmaps/ag/m;

    move-result-object v0

    return-object v0
.end method

.method private a(III)Lmaps/ag/w;
    .locals 4

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->e:I

    mul-int/lit8 v0, v0, 0x32

    div-int/lit8 v0, v0, 0x64

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v1, v1, Lmaps/ag/v;->e:I

    sub-int/2addr v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    const v0, 0x7ffffff

    sub-int v2, v0, p3

    :goto_0
    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v0, v0, Lmaps/ag/n;->g:I

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v0, v0, Lmaps/ag/n;->e:[I

    aget v0, v0, p1

    if-lez v0, :cond_1

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v0, v0, Lmaps/ag/n;->e:[I

    aget v0, v0, p1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lmaps/ag/l;->d(I)Lmaps/ag/w;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ag/w;->d()I

    move-result v3

    if-gt v3, v2, :cond_1

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(ILmaps/ag/r;Ljava/util/Map;)V
    .locals 1

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lmaps/an/w;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Lmaps/ag/d;Ljava/io/IOException;)V
    .locals 2

    iget v0, p0, Lmaps/ag/l;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ag/l;->s:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lmaps/ag/i;Lmaps/ag/w;)V
    .locals 1

    invoke-virtual {p1}, Lmaps/ag/i;->b()V

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lmaps/ag/l;->a(Lmaps/ag/w;Z)V

    return-void
.end method

.method private static a(Lmaps/ag/v;Lmaps/ag/n;Lmaps/ag/x;Lmaps/bv/a;)V
    .locals 2

    const/16 v0, 0x2000

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lmaps/ag/v;->a([BI)I

    invoke-interface {p3, v0}, Lmaps/bv/a;->a([B)V

    invoke-virtual {p1, p3}, Lmaps/ag/n;->a(Lmaps/bv/a;)V

    invoke-virtual {p2, p3}, Lmaps/ag/x;->a(Lmaps/bv/a;)V

    return-void
.end method

.method private a(Lmaps/ag/w;Lmaps/ag/w;Lmaps/ag/i;)V
    .locals 12

    invoke-virtual {p1}, Lmaps/ag/w;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/ag/l;->b(I)Lmaps/bv/a;

    move-result-object v11

    const/4 v0, 0x0

    move v10, v0

    :goto_0
    invoke-virtual {p1}, Lmaps/ag/w;->b()I

    move-result v0

    if-ge v10, v0, :cond_3

    invoke-virtual {p1, v10}, Lmaps/ag/w;->b(I)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    invoke-virtual {p1, v10}, Lmaps/ag/w;->e(I)I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p2}, Lmaps/ag/w;->b()I

    move-result v0

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v1, v1, Lmaps/ag/v;->e:I

    if-ge v0, v1, :cond_0

    invoke-virtual {p2}, Lmaps/ag/w;->c()I

    move-result v0

    const v1, 0x7ffffff

    if-lt v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t fit refcounted records into collecting shard"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1, v10}, Lmaps/ag/w;->f(I)Lmaps/ag/d;

    move-result-object v7

    iget v0, v7, Lmaps/ag/d;->d:I

    iget v1, v7, Lmaps/ag/d;->e:I

    add-int/2addr v0, v1

    new-array v0, v0, [B

    iget v1, v7, Lmaps/ag/d;->b:I

    invoke-static {v11, v1, v0}, Lmaps/ag/l;->a(Lmaps/bv/a;I[B)V

    invoke-virtual {p3, v0}, Lmaps/ag/i;->a([B)V

    new-instance v0, Lmaps/ag/d;

    iget-wide v1, v7, Lmaps/ag/d;->a:J

    invoke-virtual {p2}, Lmaps/ag/w;->c()I

    move-result v3

    iget v4, v7, Lmaps/ag/d;->d:I

    iget v5, v7, Lmaps/ag/d;->e:I

    iget v6, v7, Lmaps/ag/d;->c:I

    iget v7, v7, Lmaps/ag/d;->f:I

    invoke-virtual {p2}, Lmaps/ag/w;->a()I

    move-result v8

    invoke-virtual {p2}, Lmaps/ag/w;->b()I

    move-result v9

    invoke-direct/range {v0 .. v9}, Lmaps/ag/d;-><init>(JIIIIIII)V

    invoke-virtual {p2, v0}, Lmaps/ag/w;->a(Lmaps/ag/d;)V

    iget-object v1, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    iget-wide v2, v0, Lmaps/ag/d;->a:J

    iget v0, v0, Lmaps/ag/d;->g:I

    invoke-interface {v1, v2, v3, v0}, Lmaps/ag/aj;->b(JI)V

    :cond_2
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method private a(Lmaps/ag/w;Z)V
    .locals 5

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v0, v0, Lmaps/ag/n;->a:[I

    invoke-static {p1}, Lmaps/ag/w;->a(Lmaps/ag/w;)I

    move-result v1

    aget v0, v0, v1

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    :try_start_0
    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-static {p1}, Lmaps/ag/w;->a(Lmaps/ag/w;)I

    move-result v2

    invoke-virtual {v1, v2}, Lmaps/ag/n;->b(I)V

    invoke-direct {p0}, Lmaps/ag/l;->n()V

    iget-object v1, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-static {p1}, Lmaps/ag/w;->a(Lmaps/ag/w;)I

    move-result v3

    mul-int/lit16 v3, v3, 0x2000

    iget-object v4, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v4, v4, Lmaps/ag/v;->k:I

    add-int/2addr v3, v4

    int-to-long v3, v3

    invoke-interface {v2, v3, v4}, Lmaps/bv/a;->a(J)V

    iget-object v2, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-virtual {p1, v2}, Lmaps/ag/w;->b(Lmaps/bv/a;)V

    iget-object v2, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v2}, Lmaps/bv/a;->b()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    invoke-virtual {v1, p1}, Lmaps/ag/x;->a(Lmaps/ag/w;)V

    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v1, p1}, Lmaps/ag/n;->a(Lmaps/ag/w;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-static {p1}, Lmaps/ag/w;->a(Lmaps/ag/w;)I

    move-result v1

    invoke-direct {p0}, Lmaps/ag/l;->p()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lmaps/ag/n;->a(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    invoke-static {p1}, Lmaps/ag/w;->a(Lmaps/ag/w;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/ag/l;->f(I)V

    invoke-direct {p0}, Lmaps/ag/l;->n()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0

    :cond_0
    :try_start_5
    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-static {p1}, Lmaps/ag/w;->a(Lmaps/ag/w;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lmaps/ag/n;->a(II)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0
.end method

.method private static a(Lmaps/bv/a;I[B)V
    .locals 2

    monitor-enter p0

    int-to-long v0, p1

    :try_start_0
    invoke-interface {p0, v0, v1}, Lmaps/bv/a;->a(J)V

    invoke-interface {p0, p2}, Lmaps/bv/a;->b([B)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a([BII)V
    .locals 3

    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x18

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    add-int/lit8 v1, v0, 0x1

    shr-int/lit8 v2, p2, 0x10

    int-to-byte v2, v2

    aput-byte v2, p0, v0

    add-int/lit8 v0, v1, 0x1

    shr-int/lit8 v2, p2, 0x8

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    int-to-byte v1, p2

    aput-byte v1, p0, v0

    return-void
.end method

.method public static a([BIJ)V
    .locals 2

    const/16 v0, 0x20

    shr-long v0, p2, v0

    long-to-int v0, v0

    invoke-static {p0, p1, v0}, Lmaps/ag/l;->a([BII)V

    add-int/lit8 v0, p1, 0x4

    long-to-int v1, p2

    invoke-static {p0, v0, v1}, Lmaps/ag/l;->a([BII)V

    return-void
.end method

.method private a(Lmaps/ag/d;Lmaps/ag/r;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p2}, Lmaps/ag/r;->b()[B

    move-result-object v1

    array-length v2, v1

    iget v3, p1, Lmaps/ag/d;->d:I

    if-eq v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    array-length v2, v1

    if-nez v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lmaps/ag/l;->a(Lmaps/ag/d;)[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private a(Lmaps/ag/d;)[B
    .locals 3

    iget v0, p1, Lmaps/ag/d;->d:I

    if-nez v0, :cond_0

    sget-object v0, Lmaps/ag/l;->d:[B

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget v0, p1, Lmaps/ag/d;->g:I

    invoke-virtual {p0, v0}, Lmaps/ag/l;->b(I)Lmaps/bv/a;

    move-result-object v1

    iget v0, p1, Lmaps/ag/d;->d:I

    new-array v0, v0, [B

    iget v2, p1, Lmaps/ag/d;->b:I

    invoke-static {v1, v2, v0}, Lmaps/ag/l;->a(Lmaps/bv/a;I[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, p1, v0}, Lmaps/ag/l;->a(Lmaps/ag/d;Ljava/io/IOException;)V

    throw v0
.end method

.method private b(Ljava/util/Collection;)I
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/m;

    iget-object v3, v0, Lmaps/ag/m;->d:[B

    array-length v3, v3

    iget-object v0, v0, Lmaps/ag/m;->b:[B

    array-length v0, v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static b([BI)I
    .locals 3

    add-int/lit8 v0, p1, 0x1

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method private b(Lmaps/ag/r;)Lmaps/ag/ac;
    .locals 4

    invoke-virtual {p1}, Lmaps/ag/r;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lmaps/ag/x;->a(J)[I

    move-result-object v2

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v3, v0, Lmaps/ag/n;->g:I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v0, v1}, Lmaps/ag/n;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    invoke-virtual {v0, v2, v1}, Lmaps/ag/x;->a([II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v1}, Lmaps/ag/l;->a(Lmaps/ag/r;I)Lmaps/ag/ac;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(II)V
    .locals 10

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->g:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->a:I

    if-eq p2, v0, :cond_1

    :cond_0
    new-instance v0, Lmaps/ag/v;

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v2, v1, Lmaps/ag/v;->c:I

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v3, v1, Lmaps/ag/v;->d:I

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v4, v1, Lmaps/ag/v;->e:I

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-boolean v5, v1, Lmaps/ag/v;->f:Z

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-wide v7, v1, Lmaps/ag/v;->h:J

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-object v9, v1, Lmaps/ag/v;->i:Ljava/util/Locale;

    move v1, p2

    move v6, p1

    invoke-direct/range {v0 .. v9}, Lmaps/ag/v;-><init>(IIIIZIJLjava/util/Locale;)V

    const/16 v1, 0x2000

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/ag/v;->a([BI)I

    iget-object v2, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    const-wide/16 v4, 0x0

    invoke-interface {v3, v4, v5}, Lmaps/bv/a;->a(J)V

    iget-object v3, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v3, v1}, Lmaps/bv/a;->a([B)V

    iget-object v1, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v1}, Lmaps/bv/a;->b()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iput-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v0

    :try_start_5
    invoke-virtual {p0}, Lmaps/ag/l;->f()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private b(ILjava/util/Locale;)V
    .locals 11

    const/4 v10, 0x0

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    iget-object v1, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    move v0, v10

    :goto_0
    iget-object v1, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lmaps/bv/a;->a()V

    iget-object v1, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    :cond_0
    iget-object v1, p0, Lmaps/ag/l;->g:Lmaps/bv/c;

    invoke-virtual {p0, v0}, Lmaps/ag/l;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lmaps/bv/c;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v0}, Lmaps/bv/a;->a()V

    iget-object v0, p0, Lmaps/ag/l;->g:Lmaps/bv/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/bv/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ag/l;->g:Lmaps/bv/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lmaps/bv/c;->a(Ljava/lang/String;Z)Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    new-instance v0, Lmaps/ag/v;

    invoke-static {}, Lmaps/ag/l;->j()I

    move-result v1

    invoke-static {}, Lmaps/ag/l;->k()I

    move-result v2

    invoke-static {v1, v2}, Lmaps/ag/l;->a(II)I

    move-result v1

    iget-object v2, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v2, v2, Lmaps/ag/v;->c:I

    iget-object v3, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v3, v3, Lmaps/ag/v;->d:I

    iget-object v4, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v4, v4, Lmaps/ag/v;->e:I

    iget-object v5, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-boolean v5, v5, Lmaps/ag/v;->f:Z

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v6

    invoke-interface {v6}, Lmaps/ae/d;->a()J

    move-result-wide v7

    move v6, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lmaps/ag/v;-><init>(IIIIZIJLjava/util/Locale;)V

    iput-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v0}, Lmaps/ag/n;->a()V

    iget-object v0, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    invoke-virtual {v0}, Lmaps/ag/x;->a()V

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v2, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    iget-object v3, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-static {v0, v1, v2, v3}, Lmaps/ag/l;->a(Lmaps/ag/v;Lmaps/ag/n;Lmaps/ag/x;Lmaps/bv/a;)V

    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v0}, Lmaps/bv/a;->b()V

    iput-boolean v10, p0, Lmaps/ag/l;->q:Z

    iget-object v0, p0, Lmaps/ag/l;->A:Lmaps/ag/ak;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ag/l;->A:Lmaps/ag/ak;

    invoke-interface {v0}, Lmaps/ag/ak;->b()V

    :cond_2
    return-void
.end method

.method private b(Lmaps/ag/d;)V
    .locals 5

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    :try_start_0
    iget v0, p1, Lmaps/ag/d;->g:I

    invoke-direct {p0, v0}, Lmaps/ag/l;->d(I)Lmaps/ag/w;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ag/w;->b()I

    move-result v1

    iget v2, p1, Lmaps/ag/d;->h:I

    if-le v1, v2, :cond_0

    iget v1, p1, Lmaps/ag/d;->h:I

    invoke-virtual {v0, v1}, Lmaps/ag/w;->f(I)Lmaps/ag/d;

    move-result-object v1

    iget-wide v1, v1, Lmaps/ag/d;->a:J

    iget-wide v3, p1, Lmaps/ag/d;->a:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p1, Lmaps/ag/d;->h:I

    invoke-virtual {v0, v1}, Lmaps/ag/w;->a(I)V

    iget-object v1, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    iget-wide v3, p1, Lmaps/ag/d;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/be/i;->c(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, p0, Lmaps/ag/l;->A:Lmaps/ag/ak;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/ag/l;->A:Lmaps/ag/ak;

    iget-wide v2, p1, Lmaps/ag/d;->a:J

    invoke-interface {v1, v2, v3}, Lmaps/ag/ak;->a(J)V

    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ag/l;->a(Lmaps/ag/w;Z)V

    iget-object v0, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    iget-wide v1, p1, Lmaps/ag/d;->a:J

    invoke-interface {v0, v1, v2}, Lmaps/ag/aj;->a(J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method private b(Z)V
    .locals 13

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->d:I

    iget v1, p0, Lmaps/ag/l;->D:I

    if-gt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_5

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v0, v0, Lmaps/ag/n;->g:I

    :goto_1
    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v1, v1, Lmaps/ag/n;->h:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lmaps/ag/l;->D:I

    if-ge v0, v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1, v4}, Lmaps/ag/l;->a(III)Lmaps/ag/w;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lmaps/ag/w;->a()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    iget-object v7, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v7, v7, Lmaps/ag/n;->e:[I

    aget v0, v7, v0

    invoke-virtual {v4}, Lmaps/ag/w;->d()I

    move-result v7

    invoke-direct {p0, v1, v0, v7}, Lmaps/ag/l;->a(III)Lmaps/ag/w;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lmaps/ag/l;->a(Z)I

    move-result v7

    const/4 v1, -0x1

    if-eq v7, v1, :cond_0

    new-instance v1, Lmaps/ag/w;

    invoke-direct {v1, v7}, Lmaps/ag/w;-><init>(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    const/high16 v8, 0x20000

    :try_start_1
    new-array v8, v8, [B

    const/4 v9, 0x0

    new-instance v10, Lmaps/ag/i;

    invoke-virtual {p0, v7}, Lmaps/ag/l;->b(I)Lmaps/bv/a;

    move-result-object v7

    invoke-direct {v10, v7, v9, v8}, Lmaps/ag/i;-><init>(Lmaps/bv/a;I[B)V

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    :goto_2
    if-eqz v4, :cond_3

    invoke-direct {p0, v4, v1, v10}, Lmaps/ag/l;->a(Lmaps/ag/w;Lmaps/ag/w;Lmaps/ag/i;)V

    invoke-virtual {v4}, Lmaps/ag/w;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    const/4 v9, 0x4

    if-lt v8, v9, :cond_6

    :cond_3
    invoke-direct {p0, v10, v1}, Lmaps/ag/l;->a(Lmaps/ag/i;Lmaps/ag/w;)V

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    :try_start_2
    invoke-direct {p0, v4}, Lmaps/ag/l;->d(I)Lmaps/ag/w;

    move-result-object v8

    move v0, v3

    :goto_4
    invoke-virtual {v8}, Lmaps/ag/w;->b()I

    move-result v9

    if-ge v0, v9, :cond_8

    invoke-virtual {v8, v0}, Lmaps/ag/w;->b(I)J

    move-result-wide v9

    const-wide/16 v11, -0x1

    cmp-long v9, v9, v11

    if-eqz v9, :cond_4

    invoke-virtual {v8, v0}, Lmaps/ag/w;->e(I)I

    move-result v9

    if-lez v9, :cond_4

    invoke-virtual {v8, v0}, Lmaps/ag/w;->a(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->d:I

    goto/16 :goto_1

    :cond_6
    if-eqz v0, :cond_7

    move-object v4, v0

    move-object v0, v2

    goto :goto_2

    :cond_7
    :try_start_3
    invoke-virtual {v4}, Lmaps/ag/w;->a()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Lmaps/ag/w;->b()I

    move-result v8

    invoke-virtual {v1}, Lmaps/ag/w;->c()I

    move-result v9

    invoke-direct {p0, v4, v8, v9}, Lmaps/ag/l;->a(III)Lmaps/ag/w;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v4

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    :try_start_4
    invoke-direct {p0, v8, v0}, Lmaps/ag/l;->a(Lmaps/ag/w;Z)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    :try_start_5
    invoke-direct {p0, v4}, Lmaps/ag/l;->g(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    :goto_5
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lmaps/ag/w;->a()I

    move-result v1

    invoke-direct {p0, v1}, Lmaps/ag/l;->g(I)V

    :cond_9
    const-string v1, "Failed to combine refCounted records"

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_a
    :try_start_6
    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v5, v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Combined refCounted records from "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " shards in "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " milliseconds"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cache:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/bt/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_5
.end method

.method public static b([BII)V
    .locals 2

    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x8

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    int-to-byte v1, p2

    aput-byte v1, p0, v0

    return-void
.end method

.method static c([BII)I
    .locals 2

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static c([BI)J
    .locals 6

    invoke-static {p0, p1}, Lmaps/ag/l;->a([BI)I

    move-result v0

    int-to-long v0, v0

    add-int/lit8 v2, p1, 0x4

    invoke-static {p0, v2}, Lmaps/ag/l;->a([BI)I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private c(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 13

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    new-instance v5, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(I)V

    new-instance v6, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(I)V

    new-instance v7, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/HashMap;-><init>(I)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v8}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/m;

    iget-object v2, v0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-virtual {v2}, Lmaps/ag/r;->a()J

    move-result-wide v2

    const-wide/16 v9, -0x1

    cmp-long v2, v2, v9

    if-eqz v2, :cond_1

    iget-object v2, v0, Lmaps/ag/m;->b:[B

    array-length v2, v2

    const/16 v3, 0xff

    if-le v2, v3, :cond_2

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-interface {v6, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    iget v2, v0, Lmaps/ag/m;->c:I

    if-lez v2, :cond_0

    iget v2, v0, Lmaps/ag/m;->c:I

    iget-object v0, v0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-static {v2, v0, v7}, Lmaps/ag/l;->a(ILmaps/ag/r;Ljava/util/Map;)V

    goto :goto_0

    :cond_3
    iget-object v2, v0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-virtual {v2}, Lmaps/ag/r;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v0, v0, Lmaps/ag/n;->g:I

    if-ge v1, v0, :cond_e

    const/4 v2, 0x0

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v4, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v4, v1}, Lmaps/ag/n;->d(I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v4, v9, v10, v1}, Lmaps/ag/x;->b(JI)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_c

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v1}, Lmaps/ag/l;->d(I)Lmaps/ag/w;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v4, v0

    :goto_3
    if-eqz v4, :cond_c

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v4}, Lmaps/ag/w;->b()I

    move-result v3

    if-ge v0, v3, :cond_b

    invoke-virtual {v4, v0}, Lmaps/ag/w;->b(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    sget-object v3, Lmaps/ag/l;->d:[B

    invoke-virtual {v4, v0}, Lmaps/ag/w;->c(I)I

    move-result v11

    if-lez v11, :cond_6

    invoke-virtual {v4, v0}, Lmaps/ag/w;->f(I)Lmaps/ag/d;

    move-result-object v3

    :try_start_1
    invoke-direct {p0, v3}, Lmaps/ag/l;->a(Lmaps/ag/d;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    :cond_6
    new-instance v11, Lmaps/ag/r;

    invoke-direct {v11, v9, v10, v3}, Lmaps/ag/r;-><init>(J[B)V

    invoke-interface {v6, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget v2, p0, Lmaps/ag/l;->w:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/ag/l;->w:I

    iget-object v2, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    invoke-interface {v2, v9, v10}, Lmaps/ag/aj;->a(J)V

    :cond_7
    iget-object v2, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v2

    :try_start_2
    iget-object v3, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v3, v12}, Lmaps/be/i;->c(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v2, p0, Lmaps/ag/l;->A:Lmaps/ag/ak;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lmaps/ag/l;->A:Lmaps/ag/ak;

    invoke-interface {v2, v9, v10}, Lmaps/ag/ak;->a(J)V

    :cond_8
    invoke-virtual {v4, v0}, Lmaps/ag/w;->e(I)I

    move-result v2

    if-lez v2, :cond_9

    invoke-static {v2, v11, v7}, Lmaps/ag/l;->a(ILmaps/ag/r;Ljava/util/Map;)V

    :cond_9
    invoke-virtual {v4, v0}, Lmaps/ag/w;->a(I)V

    const/4 v2, 0x1

    :cond_a
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeOldRecordsAndFilterInsertions: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v1}, Lmaps/ag/l;->g(I)V

    move-object v4, v0

    goto/16 :goto_3

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_b
    if-eqz v2, :cond_d

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0}, Lmaps/ag/l;->a(Lmaps/ag/w;Z)V

    :cond_c
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_d
    iget v0, p0, Lmaps/ag/l;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ag/l;->v:I

    goto :goto_6

    :cond_e
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_7
    if-ltz v2, :cond_10

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/m;

    iget-object v1, v0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v4, v0, Lmaps/ag/m;->c:I

    invoke-static {v1, v4}, Lmaps/an/w;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v1, Lmaps/ag/m;

    iget-object v5, v0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v0, Lmaps/ag/m;->d:[B

    const/4 v6, 0x0

    invoke-direct {v1, v5, v4, v0, v6}, Lmaps/ag/m;-><init>(Lmaps/ag/r;I[BLmaps/ag/q;)V

    move-object v0, v1

    :cond_f
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_7

    :catch_1
    move-exception v3

    goto :goto_5

    :cond_10
    return-object v3

    :cond_11
    move v0, v2

    goto/16 :goto_2
.end method

.method private static c(JLjava/lang/String;)Lmaps/ag/r;
    .locals 2

    sget-object v0, Lmaps/ag/l;->d:[B

    if-eqz p2, :cond_0

    invoke-static {p2}, Lmaps/bj/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    :cond_0
    new-instance v1, Lmaps/ag/r;

    invoke-direct {v1, p0, p1, v0}, Lmaps/ag/r;-><init>(J[B)V

    return-object v1
.end method

.method private c(Lmaps/ag/r;)[B
    .locals 5

    invoke-direct {p0, p1}, Lmaps/ag/l;->a(Lmaps/ag/r;)Lmaps/ag/d;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget v0, v1, Lmaps/ag/d;->g:I

    invoke-virtual {p0, v0}, Lmaps/ag/l;->b(I)Lmaps/bv/a;

    move-result-object v2

    iget v0, v1, Lmaps/ag/d;->e:I

    new-array v0, v0, [B

    iget v3, v1, Lmaps/ag/d;->b:I

    iget v4, v1, Lmaps/ag/d;->d:I

    add-int/2addr v3, v4

    invoke-static {v2, v3, v0}, Lmaps/ag/l;->a(Lmaps/bv/a;I[B)V

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Lmaps/ag/l;->c([BII)I

    move-result v2

    iget v3, v1, Lmaps/ag/d;->f:I

    if-eq v2, v3, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Checksum mismatch: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " record ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v1, v0}, Lmaps/ag/l;->a(Lmaps/ag/d;Ljava/io/IOException;)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    :try_start_1
    iget-object v2, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v3, v1, Lmaps/ag/d;->g:I

    invoke-direct {p0}, Lmaps/ag/l;->p()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lmaps/ag/n;->a(II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private d(I)Lmaps/ag/w;
    .locals 4

    iget-object v1, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    mul-int/lit16 v2, p1, 0x2000

    iget-object v3, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v3, v3, Lmaps/ag/v;->k:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lmaps/bv/a;->a(J)V

    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-static {v0}, Lmaps/ag/w;->a(Lmaps/bv/a;)Lmaps/ag/w;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e(I)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0, p1}, Lmaps/ag/l;->g(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private f(I)V
    .locals 4

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    iget-object v1, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    mul-int/lit16 v2, p1, 0x400

    add-int/lit16 v2, v2, 0x4000

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lmaps/bv/a;->a(J)V

    iget-object v0, p0, Lmaps/ag/l;->k:Lmaps/ag/x;

    iget-object v2, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-virtual {v0, v2, p1}, Lmaps/ag/x;->a(Lmaps/bv/a;I)V

    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v0}, Lmaps/bv/a;->b()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private g(I)V
    .locals 3

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    new-instance v0, Lmaps/ag/w;

    invoke-direct {v0, p1}, Lmaps/ag/w;-><init>(I)V

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1}, Lmaps/ag/l;->a(Lmaps/ag/w;Z)V

    iget-object v1, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lmaps/ag/l;->m:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    invoke-interface {v0, p1}, Lmaps/ag/aj;->a(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private h(I)V
    .locals 2

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    invoke-direct {p0, p1}, Lmaps/ag/l;->g(I)V

    iget-object v0, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v0, v0, p1

    invoke-interface {v0}, Lmaps/bv/a;->a()V

    iget-object v0, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    :cond_0
    iget-object v0, p0, Lmaps/ag/l;->g:Lmaps/bv/c;

    invoke-virtual {p0, p1}, Lmaps/ag/l;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/bv/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic i()[B
    .locals 1

    sget-object v0, Lmaps/ag/l;->d:[B

    return-object v0
.end method

.method private static j()I
    .locals 2

    invoke-static {}, Lmaps/az/c;->c()Lmaps/az/a;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorMapsParameters is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lmaps/az/a;->c()I

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method private static k()I
    .locals 2

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lmaps/ag/l;->e:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lmaps/ag/l;->a:I

    const v1, 0xffff

    and-int/2addr v0, v1

    goto :goto_0
.end method

.method private l()V
    .locals 3

    iget-object v0, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    iget-object v1, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/d;

    invoke-direct {p0, v0}, Lmaps/ag/l;->b(Lmaps/ag/d;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private m()V
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lmaps/ag/l;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private n()V
    .locals 4

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    iget-object v1, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v2, v3}, Lmaps/bv/a;->a(J)V

    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v2, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-virtual {v0, v2}, Lmaps/ag/n;->a(Lmaps/bv/a;)V

    iget-object v0, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v0}, Lmaps/bv/a;->b()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private o()Lmaps/ag/w;
    .locals 8

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v4, -0x1

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    sget-boolean v0, Lmaps/ag/l;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    :goto_0
    iget-object v3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget v3, v3, Lmaps/ag/n;->g:I

    if-ge v0, v3, :cond_8

    iget-object v3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v3, v3, Lmaps/ag/n;->c:[I

    aget v3, v3, v0

    iget-object v5, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v5, v5, Lmaps/ag/v;->e:I

    if-ge v3, v5, :cond_2

    iget-object v3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v3, v3, Lmaps/ag/n;->b:[I

    aget v3, v3, v0

    const v5, 0x7ffffff

    if-gt v3, v5, :cond_2

    :try_start_0
    invoke-direct {p0, v0}, Lmaps/ag/l;->d(I)Lmaps/ag/w;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move-object v7, v3

    move v3, v0

    move-object v0, v7

    :goto_1
    if-ne v3, v4, :cond_4

    invoke-direct {p0}, Lmaps/ag/l;->q()Z

    move-result v3

    invoke-direct {p0, v3}, Lmaps/ag/l;->b(Z)V

    iget-object v5, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-boolean v5, v5, Lmaps/ag/v;->f:Z

    if-eqz v5, :cond_3

    if-eqz v3, :cond_3

    :goto_2
    const/4 v5, 0x2

    if-ge v1, v5, :cond_3

    iget-object v5, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v6, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    invoke-virtual {v5, v6}, Lmaps/ag/n;->a(Ljava/util/Set;)I

    move-result v5

    if-eq v5, v4, :cond_1

    invoke-direct {p0, v5}, Lmaps/ag/l;->h(I)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "allocateShardToUse: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v3}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v3, v0

    move-object v0, v2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-direct {p0, v3}, Lmaps/ag/l;->a(Z)I

    move-result v1

    if-ne v1, v4, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Tile store full, unable to allocate shard"

    invoke-static {v0, v1}, Lmaps/bt/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    :goto_3
    return-object v0

    :cond_4
    move v1, v3

    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lmaps/ag/w;->b()I

    move-result v2

    iget-object v3, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v3, v3, Lmaps/ag/n;->c:[I

    aget v3, v3, v1

    if-eq v2, v3, :cond_7

    :cond_6
    new-instance v0, Lmaps/ag/w;

    invoke-direct {v0, v1}, Lmaps/ag/w;-><init>(I)V

    :cond_7
    iput v1, p0, Lmaps/ag/l;->x:I

    iget-object v2, p0, Lmaps/ag/l;->C:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    move-object v0, v2

    move v3, v4

    goto/16 :goto_1
.end method

.method private p()I
    .locals 4

    iget v0, p0, Lmaps/ag/l;->y:I

    if-ltz v0, :cond_0

    iget v0, p0, Lmaps/ag/l;->y:I

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method private q()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/ag/l;->h()V

    iget-boolean v2, p0, Lmaps/ag/l;->z:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v2}, Lmaps/ag/n;->e()I

    move-result v2

    const/16 v3, 0x14

    if-ge v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lmaps/bt/a;->f()J

    move-result-wide v2

    iget-object v4, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v4}, Lmaps/ag/n;->d()J

    move-result-wide v4

    add-long/2addr v2, v4

    long-to-double v2, v2

    const-wide/high16 v6, 0x3fd0000000000000L

    mul-double/2addr v2, v6

    double-to-long v2, v2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->g:I

    return v0
.end method

.method public a(JLjava/lang/String;I)I
    .locals 13

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    invoke-static/range {p1 .. p3}, Lmaps/ag/l;->c(JLjava/lang/String;)Lmaps/ag/r;

    move-result-object v1

    invoke-direct {p0, v1}, Lmaps/ag/l;->b(Lmaps/ag/r;)Lmaps/ag/ac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    if-nez v11, :cond_0

    const/4 v7, -0x1

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return v7

    :cond_0
    :try_start_1
    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v1, v1, Lmaps/ag/n;->e:[I

    iget-object v2, v11, Lmaps/ag/ac;->a:Lmaps/ag/w;

    invoke-virtual {v2}, Lmaps/ag/w;->a()I

    move-result v2

    aget v12, v1, v2

    iget-object v10, v11, Lmaps/ag/ac;->b:Lmaps/ag/d;

    iget v1, v10, Lmaps/ag/d;->c:I

    move/from16 v0, p4

    invoke-static {v1, v0}, Lmaps/an/w;->a(II)I

    move-result v1

    and-int/lit8 v7, v1, 0x1f

    new-instance v1, Lmaps/ag/d;

    iget-wide v2, v10, Lmaps/ag/d;->a:J

    iget v4, v10, Lmaps/ag/d;->b:I

    iget v5, v10, Lmaps/ag/d;->d:I

    iget v6, v10, Lmaps/ag/d;->e:I

    iget v8, v10, Lmaps/ag/d;->f:I

    iget v9, v10, Lmaps/ag/d;->g:I

    iget v10, v10, Lmaps/ag/d;->h:I

    invoke-direct/range {v1 .. v10}, Lmaps/ag/d;-><init>(JIIIIIII)V

    iget-object v2, v11, Lmaps/ag/ac;->a:Lmaps/ag/w;

    iget v3, v11, Lmaps/ag/ac;->c:I

    invoke-virtual {v2, v1, v3}, Lmaps/ag/w;->a(Lmaps/ag/d;I)V

    iget-object v1, v11, Lmaps/ag/ac;->a:Lmaps/ag/w;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lmaps/ag/l;->a(Lmaps/ag/w;Z)V

    if-nez v12, :cond_1

    iget-object v1, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    iget-object v1, v1, Lmaps/ag/n;->e:[I

    iget-object v2, v11, Lmaps/ag/ac;->a:Lmaps/ag/w;

    invoke-virtual {v2}, Lmaps/ag/w;->a()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lmaps/ag/l;->b(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public a(Ljava/util/Collection;)I
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/ag/l;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/ag/l;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return v1

    :cond_0
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lmaps/ag/l;->c(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct/range {p0 .. p0}, Lmaps/ag/l;->l()V

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/ag/l;->b(Ljava/util/Collection;)I

    move-result v2

    const/high16 v3, 0x20000

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    new-array v13, v2, [B

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/ag/l;->C:Ljava/util/Set;

    invoke-direct/range {p0 .. p0}, Lmaps/ag/l;->o()Lmaps/ag/w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v1, -0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/ag/l;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lmaps/ag/i;

    invoke-virtual {v3}, Lmaps/ag/w;->a()I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lmaps/ag/l;->b(I)Lmaps/bv/a;

    move-result-object v4

    invoke-virtual {v3}, Lmaps/ag/w;->c()I

    move-result v5

    invoke-direct {v2, v4, v5, v13}, Lmaps/ag/i;-><init>(Lmaps/bv/a;I[B)V

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lmaps/ag/m;

    move-object v8, v0

    iget v1, v8, Lmaps/ag/m;->c:I

    and-int/lit8 v7, v1, 0x1f

    invoke-virtual {v3}, Lmaps/ag/w;->b()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v4, v4, Lmaps/ag/v;->e:I

    if-ge v1, v4, :cond_2

    invoke-virtual {v3}, Lmaps/ag/w;->c()I

    move-result v1

    const v4, 0x7ffffff

    if-le v1, v4, :cond_7

    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lmaps/ag/l;->a(Lmaps/ag/i;Lmaps/ag/w;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    if-eqz v1, :cond_3

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    invoke-virtual {v3}, Lmaps/ag/w;->a()I

    move-result v6

    invoke-interface {v1, v4, v5, v6}, Lmaps/ag/aj;->a(JI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/ag/l;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    :cond_3
    :try_start_3
    invoke-interface {v14}, Ljava/util/List;->clear()V

    invoke-direct/range {p0 .. p0}, Lmaps/ag/l;->o()Lmaps/ag/w;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    if-nez v2, :cond_4

    const/4 v1, -0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/ag/l;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    :cond_4
    :try_start_4
    new-instance v1, Lmaps/ag/i;

    invoke-virtual {v2}, Lmaps/ag/w;->a()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lmaps/ag/l;->b(I)Lmaps/bv/a;

    move-result-object v3

    invoke-virtual {v2}, Lmaps/ag/w;->c()I

    move-result v4

    invoke-direct {v1, v3, v4, v13}, Lmaps/ag/i;-><init>(Lmaps/bv/a;I[B)V

    move-object v11, v1

    move-object v12, v2

    :goto_3
    iget-object v1, v8, Lmaps/ag/m;->b:[B

    invoke-virtual {v11, v1}, Lmaps/ag/i;->a([B)V

    iget-object v1, v8, Lmaps/ag/m;->d:[B

    invoke-virtual {v11, v1}, Lmaps/ag/i;->a([B)V

    new-instance v1, Lmaps/ag/d;

    iget-object v2, v8, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-virtual {v2}, Lmaps/ag/r;->a()J

    move-result-wide v2

    invoke-virtual {v12}, Lmaps/ag/w;->c()I

    move-result v4

    iget-object v5, v8, Lmaps/ag/m;->b:[B

    array-length v5, v5

    iget-object v6, v8, Lmaps/ag/m;->d:[B

    array-length v6, v6

    iget-object v9, v8, Lmaps/ag/m;->d:[B

    const/4 v10, 0x0

    iget-object v8, v8, Lmaps/ag/m;->d:[B

    array-length v8, v8

    invoke-static {v9, v10, v8}, Lmaps/ag/l;->c([BII)I

    move-result v8

    invoke-virtual {v12}, Lmaps/ag/w;->a()I

    move-result v9

    invoke-virtual {v12}, Lmaps/ag/w;->b()I

    move-result v10

    invoke-direct/range {v1 .. v10}, Lmaps/ag/d;-><init>(JIIIIIII)V

    invoke-virtual {v12, v1}, Lmaps/ag/w;->a(Lmaps/ag/d;)V

    iget-wide v1, v1, Lmaps/ag/d;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v11

    move-object v3, v12

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lmaps/ag/l;->a(Lmaps/ag/i;Lmaps/ag/w;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    if-eqz v1, :cond_6

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    invoke-virtual {v3}, Lmaps/ag/w;->a()I

    move-result v6

    invoke-interface {v1, v4, v5, v6}, Lmaps/ag/aj;->a(JI)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/ag/l;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    move-object v11, v2

    move-object v12, v3

    goto :goto_3
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->a:I

    invoke-direct {p0, p1, v0}, Lmaps/ag/l;->b(II)V

    return-void
.end method

.method public a(ILjava/util/Locale;)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/ag/l;->q:Z

    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->B:Lmaps/ag/aj;

    invoke-interface {v0}, Lmaps/ag/aj;->b()V

    :cond_0
    invoke-direct {p0, p1, p2}, Lmaps/ag/l;->b(ILjava/util/Locale;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {p0}, Lmaps/ag/l;->f()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public a(J)[B
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lmaps/ag/l;->a(JLjava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;)[B
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Lmaps/ag/l;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    invoke-direct {p0}, Lmaps/ag/l;->m()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-static {p1, p2, p3}, Lmaps/ag/l;->c(JLjava/lang/String;)Lmaps/ag/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ag/l;->c(Lmaps/ag/r;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    invoke-direct {p0}, Lmaps/ag/l;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    invoke-direct {p0}, Lmaps/ag/l;->m()V

    throw v0
.end method

.method public b()J
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-wide v0, v0, Lmaps/ag/v;->h:J

    return-wide v0
.end method

.method b(I)Lmaps/bv/a;
    .locals 5

    iget-object v1, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    iget-object v2, p0, Lmaps/ag/l;->g:Lmaps/bv/c;

    invoke-virtual {p0, p1}, Lmaps/ag/l;->c(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lmaps/bv/c;->a(Ljava/lang/String;Z)Lmaps/bv/a;

    move-result-object v2

    aput-object v2, v0, p1

    :cond_0
    iget-object v0, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v0, v0, p1

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(JLjava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :try_start_0
    invoke-static {p1, p2, p3}, Lmaps/ag/l;->c(JLjava/lang/String;)Lmaps/ag/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ag/l;->a(Lmaps/ag/r;)Lmaps/ag/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    invoke-direct {p0}, Lmaps/ag/l;->m()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    invoke-direct {p0}, Lmaps/ag/l;->m()V

    throw v0
.end method

.method c(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-object v0, v0, Lmaps/ag/v;->i:Ljava/util/Locale;

    return-object v0
.end method

.method public d()I
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/ag/l;->j:Lmaps/ag/n;

    invoke-virtual {v0}, Lmaps/ag/n;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public e()I
    .locals 2

    iget-object v0, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v0, v0, Lmaps/ag/v;->d:I

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v1, v1, Lmaps/ag/v;->e:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public f()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-boolean v1, p0, Lmaps/ag/l;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lmaps/ag/l;->q:Z

    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-direct {p0}, Lmaps/ag/l;->n()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    iget-object v1, p0, Lmaps/ag/l;->h:Lmaps/bv/a;

    invoke-interface {v1}, Lmaps/bv/a;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    const/4 v1, 0x0

    :goto_3
    :try_start_4
    iget-object v2, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v2, v2, v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v2, :cond_1

    :try_start_5
    iget-object v2, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    aget-object v2, v2, v1

    invoke-interface {v2}, Lmaps/bv/a;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_4
    :try_start_6
    iget-object v2, p0, Lmaps/ag/l;->l:[Lmaps/bv/a;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    if-eqz v0, :cond_3

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_3
    :try_start_8
    iget-object v0, p0, Lmaps/ag/l;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method h()V
    .locals 2

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/l;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Write lock must be held"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/l;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ver:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ag/l;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " locale: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ag/l;->c()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " auto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget-boolean v1, v1, Lmaps/ag/v;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ag/l;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " max:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ag/l;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " max_shards:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/l;->i:Lmaps/ag/v;

    iget v1, v1, Lmaps/ag/v;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
