.class public final Lmaps/k/b;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmaps/k/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/k/b;

    invoke-direct {v0}, Lmaps/k/b;-><init>()V

    sput-object v0, Lmaps/k/b;->a:Lmaps/k/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lmaps/k/b;
    .locals 1

    sget-object v0, Lmaps/k/b;->a:Lmaps/k/b;

    return-object v0
.end method

.method public static k()Z
    .locals 1

    invoke-static {}, Lmaps/be/b;->a()Lmaps/be/o;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/be/b;->a()Lmaps/be/o;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/o;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Landroid/content/Context;
    .locals 1

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->w()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/capabilities/a;->a()Lcom/google/googlenav/capabilities/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lmaps/ae/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/api"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 2

    invoke-static {}, Lmaps/bm/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/capabilities/a;->a()Lcom/google/googlenav/capabilities/a;

    move-result-object v0

    invoke-direct {p0}, Lmaps/k/b;->p()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/capabilities/a;->b(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->G:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/az/c;->a()Lmaps/az/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/az/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public j()Z
    .locals 1

    invoke-static {}, Lmaps/az/c;->a()Lmaps/az/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/az/f;->b()Z

    move-result v0

    return v0
.end method

.method public l()V
    .locals 0

    return-void
.end method

.method public n()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
