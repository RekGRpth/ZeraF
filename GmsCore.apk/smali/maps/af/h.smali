.class public Lmaps/af/h;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ak/a;

.field private b:Lmaps/t/bx;

.field private c:I

.field private final d:Lmaps/t/bx;

.field private e:Lmaps/af/u;

.field private f:Lmaps/af/i;


# direct methods
.method public constructor <init>(Lmaps/ak/a;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0, v1, v1}, Lmaps/t/bx;-><init>(II)V

    iput-object v0, p0, Lmaps/af/h;->b:Lmaps/t/bx;

    const/4 v0, -0x1

    iput v0, p0, Lmaps/af/h;->c:I

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/af/h;->d:Lmaps/t/bx;

    iput-object v2, p0, Lmaps/af/h;->e:Lmaps/af/u;

    iput-object v2, p0, Lmaps/af/h;->f:Lmaps/af/i;

    iput-object p1, p0, Lmaps/af/h;->a:Lmaps/ak/a;

    return-void
.end method


# virtual methods
.method public a(Lmaps/bq/d;Lmaps/t/bx;)Lmaps/t/bx;
    .locals 10

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v2

    invoke-virtual {p2}, Lmaps/t/bx;->f()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v4

    div-float/2addr v2, v4

    float-to-int v5, v2

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v2

    invoke-virtual {p2}, Lmaps/t/bx;->g()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v4

    div-float/2addr v2, v4

    float-to-int v6, v2

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v4

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v2

    div-int/lit8 v7, v4, 0x2

    div-int/lit8 v8, v2, 0x2

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-gt v9, v4, :cond_0

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-le v9, v2, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    neg-int v1, v7

    if-ge v5, v1, :cond_5

    neg-int v1, v4

    move v4, v1

    :cond_3
    :goto_1
    neg-int v1, v8

    if-ge v6, v1, :cond_6

    neg-int v1, v2

    :goto_2
    if-nez v1, :cond_4

    if-eqz v4, :cond_1

    :cond_4
    int-to-float v0, v4

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v1, v1

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lmaps/af/h;->d:Lmaps/t/bx;

    invoke-virtual {v2, v0, v1}, Lmaps/t/bx;->d(II)V

    iget-object v0, p0, Lmaps/af/h;->d:Lmaps/t/bx;

    invoke-virtual {p2, v0}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    goto :goto_0

    :cond_5
    if-gt v5, v7, :cond_3

    move v4, v3

    goto :goto_1

    :cond_6
    if-le v6, v8, :cond_7

    move v1, v2

    goto :goto_2

    :cond_7
    move v1, v3

    goto :goto_2
.end method

.method public a(Lmaps/af/i;)V
    .locals 0

    iput-object p1, p0, Lmaps/af/h;->f:Lmaps/af/i;

    return-void
.end method

.method public a(Lmaps/af/u;)V
    .locals 0

    iput-object p1, p0, Lmaps/af/h;->e:Lmaps/af/u;

    return-void
.end method

.method public a(Lmaps/bq/d;)V
    .locals 5

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x1e

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lmaps/af/h;->c:I

    if-eq v1, v2, :cond_1

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v2

    invoke-virtual {p0, v0, v0, v1, v2}, Lmaps/af/h;->a(Lmaps/t/bx;Lmaps/t/bx;ILmaps/t/bw;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lmaps/af/h;->b:Lmaps/t/bx;

    invoke-virtual {v2, v0}, Lmaps/t/bx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/af/h;->b:Lmaps/t/bx;

    invoke-virtual {p0, p1, v2}, Lmaps/af/h;->a(Lmaps/bq/d;Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lmaps/af/h;->b:Lmaps/t/bx;

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Lmaps/t/bx;->d(Lmaps/t/bx;)F

    move-result v3

    iget-object v4, p0, Lmaps/af/h;->b:Lmaps/t/bx;

    invoke-virtual {v0, v4}, Lmaps/t/bx;->d(Lmaps/t/bx;)F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    :cond_2
    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v3

    invoke-virtual {p0, v2, v0, v1, v3}, Lmaps/af/h;->a(Lmaps/t/bx;Lmaps/t/bx;ILmaps/t/bw;)V

    goto :goto_0
.end method

.method a(Lmaps/t/bx;Lmaps/t/bx;ILmaps/t/bw;)V
    .locals 9

    iput-object p1, p0, Lmaps/af/h;->b:Lmaps/t/bx;

    iput p3, p0, Lmaps/af/h;->c:I

    invoke-virtual {p1}, Lmaps/t/bx;->a()I

    move-result v1

    invoke-virtual {p1}, Lmaps/t/bx;->c()I

    move-result v2

    invoke-virtual {p4}, Lmaps/t/bw;->e()I

    move-result v0

    int-to-double v3, v0

    int-to-double v5, v1

    const-wide v7, 0x3eb0c6f7a0b5ed8dL

    mul-double/2addr v5, v7

    const-wide v7, 0x3f91df46a2529d39L

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    const-wide v5, 0x3fd5752a00000000L

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual {p4}, Lmaps/t/bw;->d()I

    move-result v0

    int-to-double v4, v0

    const-wide v6, 0x3fd5752a00000000L

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iget-object v0, p0, Lmaps/af/h;->f:Lmaps/af/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/h;->f:Lmaps/af/i;

    invoke-interface {v0}, Lmaps/af/i;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const/4 v5, 0x1

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v7, Ljava/io/DataOutputStream;

    invoke-direct {v7, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v7, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p2}, Lmaps/t/bx;->a()I

    move-result v1

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p2}, Lmaps/t/bx;->c()I

    move-result v1

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v7, p3}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v7, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v7, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v7, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    invoke-virtual {v7, v5}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lmaps/af/h;->a:Lmaps/ak/a;

    const/4 v1, 0x7

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-interface/range {v0 .. v5}, Lmaps/ak/a;->a(I[BZZZ)V

    iget-object v0, p0, Lmaps/af/h;->e:Lmaps/af/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/af/h;->e:Lmaps/af/u;

    invoke-interface {v0, p1, p3}, Lmaps/af/u;->a(Lmaps/t/bx;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "view point"

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
