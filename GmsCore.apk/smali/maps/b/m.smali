.class public Lmaps/b/m;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/b/o;
.implements Lmaps/b/u;


# instance fields
.field private final b:Lmaps/an/y;

.field private final c:Lmaps/an/i;

.field private final d:Lmaps/an/ag;

.field private final e:Ljava/util/Map;

.field private final f:Lmaps/be/i;

.field private final g:Ljava/util/Set;

.field private volatile h:I

.field private volatile i:I

.field private volatile j:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/m;->e:Ljava/util/Map;

    new-instance v0, Lmaps/be/i;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/b/m;->f:Lmaps/be/i;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lmaps/b/m;->g:Ljava/util/Set;

    sget-object v0, Lmaps/o/c;->a:Lmaps/o/c;

    invoke-static {v0}, Lmaps/an/t;->a(Lmaps/o/c;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v2, p0, Lmaps/b/m;->b:Lmaps/an/y;

    iput-object v2, p0, Lmaps/b/m;->c:Lmaps/an/i;

    iput-object v2, p0, Lmaps/b/m;->d:Lmaps/an/ag;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lmaps/o/c;->a:Lmaps/o/c;

    invoke-static {v0}, Lmaps/an/t;->b(Lmaps/o/c;)Lmaps/an/y;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/m;->b:Lmaps/an/y;

    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/m;->c:Lmaps/an/i;

    new-instance v0, Lmaps/b/b;

    invoke-direct {v0, p0}, Lmaps/b/b;-><init>(Lmaps/b/m;)V

    iput-object v0, p0, Lmaps/b/m;->d:Lmaps/an/ag;

    iget-object v0, p0, Lmaps/b/m;->b:Lmaps/an/y;

    iget-object v1, p0, Lmaps/b/m;->d:Lmaps/an/ag;

    invoke-interface {v0, v1}, Lmaps/an/y;->a(Lmaps/an/ag;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/b/m;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/b/m;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lmaps/b/m;)Lmaps/be/i;
    .locals 1

    iget-object v0, p0, Lmaps/b/m;->f:Lmaps/be/i;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lmaps/b/m;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/g;

    invoke-interface {v0}, Lmaps/b/g;->j()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lmaps/b/m;)V
    .locals 0

    invoke-direct {p0}, Lmaps/b/m;->b()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/ah;)Ljava/util/Collection;
    .locals 5

    const/16 v1, 0xe

    iget v0, p0, Lmaps/b/m;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/b/m;->h:I

    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v0

    if-le v0, v1, :cond_3

    invoke-virtual {p1, v1}, Lmaps/t/ah;->a(I)Lmaps/t/ah;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v2, p0, Lmaps/b/m;->f:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/b/m;->f:Lmaps/be/i;

    invoke-virtual {v0, v1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget v1, p0, Lmaps/b/m;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/b/m;->i:I

    invoke-virtual {p1}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/b/x;->a(Ljava/util/Collection;Lmaps/t/an;)Ljava/util/Collection;

    move-result-object v0

    :goto_1
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/b/m;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_2
    iget-object v0, p0, Lmaps/b/m;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/i;

    if-nez v0, :cond_2

    new-instance v2, Lmaps/b/i;

    iget-object v0, p0, Lmaps/b/m;->b:Lmaps/an/y;

    iget-object v4, p0, Lmaps/b/m;->c:Lmaps/an/i;

    invoke-direct {v2, v0, v4, v1}, Lmaps/b/i;-><init>(Lmaps/an/y;Lmaps/an/i;Lmaps/t/ah;)V

    iget-object v0, p0, Lmaps/b/m;->e:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    move-object v1, v2

    :goto_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v0, :cond_1

    invoke-virtual {v1, p0}, Lmaps/b/i;->a(Lmaps/b/o;)V

    iget v0, p0, Lmaps/b/m;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/b/m;->j:I

    :cond_1
    sget-object v0, Lmaps/b/u;->a:Ljava/util/Collection;

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object v1, v0

    move v0, v2

    goto :goto_2

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, Lmaps/b/m;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/m;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/b/m;->f:Lmaps/be/i;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/b/m;->f:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0}, Lmaps/b/m;->b()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Lmaps/b/g;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/m;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lmaps/b/i;Ljava/util/Collection;)V
    .locals 3

    iget-object v1, p0, Lmaps/b/m;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/m;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/b/i;->a()Lmaps/t/ah;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/i;

    if-eq v0, p1, :cond_1

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/b/m;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/b/i;->a()Lmaps/t/ah;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lmaps/b/m;->f:Lmaps/be/i;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/b/m;->f:Lmaps/be/i;

    invoke-virtual {p1}, Lmaps/b/i;->a()Lmaps/t/ah;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0}, Lmaps/b/m;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Lmaps/t/v;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lmaps/b/g;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/m;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
