.class public Lmaps/b/n;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/b/u;


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:Z

.field private d:Z

.field private volatile e:Lmaps/b/u;

.field private volatile f:Lmaps/b/u;

.field private final g:Ljava/util/List;

.field private final h:Lmaps/b/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/b/n;->b:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/b/n;->c:Z

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/n;->g:Ljava/util/List;

    new-instance v0, Lmaps/b/h;

    invoke-direct {v0, p0}, Lmaps/b/h;-><init>(Lmaps/b/n;)V

    iput-object v0, p0, Lmaps/b/n;->h:Lmaps/b/p;

    invoke-direct {p0}, Lmaps/b/n;->b()Z

    return-void
.end method

.method private a()V
    .locals 6

    const/4 v3, 0x0

    const/4 v0, 0x0

    sget-boolean v2, Lmaps/ae/h;->u:Z

    sget-boolean v1, Lmaps/ae/h;->v:Z

    invoke-static {}, Lmaps/be/b;->a()Lmaps/be/o;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/be/o;->d()Z

    move-result v4

    if-eqz v4, :cond_5

    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_0

    const-string v1, "CompositeBuildingBoundProvider"

    const-string v2, "Raster device - using legacy building list"

    invoke-static {v1, v2}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v1, v0

    :goto_0
    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_1

    const-string v2, "CompositeBuildingBoundProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Creating providers with baseTiles = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and supplemental list = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/b/n;->h:Lmaps/b/p;

    invoke-interface {v1}, Lmaps/b/p;->a()Lmaps/b/u;

    move-result-object v1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/b/n;->h:Lmaps/b/p;

    const-string v2, "/new.building.list"

    invoke-interface {v0, v2}, Lmaps/b/p;->a(Ljava/lang/String;)Lmaps/b/u;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    iget-object v0, p0, Lmaps/b/n;->h:Lmaps/b/p;

    const-string v2, "/building.list"

    invoke-interface {v0, v2}, Lmaps/b/p;->a(Ljava/lang/String;)Lmaps/b/u;

    move-result-object v0

    :cond_2
    iput-object v0, p0, Lmaps/b/n;->f:Lmaps/b/u;

    iput-object v1, p0, Lmaps/b/n;->e:Lmaps/b/u;

    return-void

    :cond_3
    move-object v0, v3

    goto :goto_2

    :cond_4
    move-object v1, v3

    goto :goto_1

    :cond_5
    move v0, v1

    move v1, v2

    goto :goto_0
.end method

.method private b()Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lmaps/b/n;->b:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lmaps/b/n;->d:Z

    if-eqz v2, :cond_1

    monitor-exit v3

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Lmaps/b/n;->c:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lmaps/be/b;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lmaps/b/n;->c:Z

    move v2, v1

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lmaps/b/n;->a()V

    iget-object v2, p0, Lmaps/b/n;->b:Ljava/lang/Object;

    monitor-enter v2

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lmaps/b/n;->d:Z

    iget-object v0, p0, Lmaps/b/n;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/g;

    invoke-direct {p0, v0}, Lmaps/b/n;->c(Lmaps/b/g;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    :try_start_3
    iget-object v0, p0, Lmaps/b/n;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method private c(Lmaps/b/g;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/n;->f:Lmaps/b/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/b/n;->f:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/b/g;)V

    :cond_0
    iget-object v0, p0, Lmaps/b/n;->e:Lmaps/b/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/b/n;->e:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/b/g;)V

    :cond_1
    return-void
.end method

.method private d(Lmaps/b/g;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/n;->f:Lmaps/b/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/b/n;->f:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->b(Lmaps/b/g;)V

    :cond_0
    iget-object v0, p0, Lmaps/b/n;->e:Lmaps/b/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/b/n;->e:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->b(Lmaps/b/g;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lmaps/t/ah;)Ljava/util/Collection;
    .locals 5

    invoke-direct {p0}, Lmaps/b/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmaps/b/u;->a:Ljava/util/Collection;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v0

    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v2

    iget-object v1, p0, Lmaps/b/n;->f:Lmaps/b/u;

    if-eqz v1, :cond_8

    iget-object v0, p0, Lmaps/b/n;->f:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/t/ah;)Ljava/util/Collection;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/b/n;->e:Lmaps/b/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/b/n;->e:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/t/ah;)Ljava/util/Collection;

    move-result-object v0

    move-object v2, v0

    :cond_1
    sget-object v0, Lmaps/b/u;->a:Ljava/util/Collection;

    if-eq v1, v0, :cond_2

    sget-object v0, Lmaps/b/u;->a:Ljava/util/Collection;

    if-ne v2, v0, :cond_3

    :cond_2
    sget-object v0, Lmaps/b/u;->a:Ljava/util/Collection;

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/x;

    invoke-virtual {v0}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-static {v1}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/x;

    invoke-virtual {v0}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto :goto_0

    :cond_8
    move-object v1, v0

    goto :goto_1
.end method

.method public a(Lmaps/b/g;)V
    .locals 2

    invoke-direct {p0}, Lmaps/b/n;->b()Z

    iget-object v1, p0, Lmaps/b/n;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/b/n;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/b/n;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1}, Lmaps/b/n;->c(Lmaps/b/g;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lmaps/t/v;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/b/n;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/b/n;->f:Lmaps/b/u;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lmaps/b/n;->f:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/t/v;)Z

    move-result v0

    :cond_2
    if-nez v0, :cond_0

    iget-object v1, p0, Lmaps/b/n;->e:Lmaps/b/u;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/b/n;->e:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/t/v;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lmaps/b/g;)V
    .locals 2

    invoke-direct {p0}, Lmaps/b/n;->b()Z

    iget-object v1, p0, Lmaps/b/n;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/b/n;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/b/n;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1}, Lmaps/b/n;->d(Lmaps/b/g;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
