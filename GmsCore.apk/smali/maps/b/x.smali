.class public Lmaps/b/x;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/t/bg;

.field private final b:Lmaps/t/av;

.field private final c:Lmaps/t/bx;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lmaps/t/bg;Lmaps/t/av;Lmaps/t/bx;[Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/b/x;->a:Lmaps/t/bg;

    iput-object p2, p0, Lmaps/b/x;->b:Lmaps/t/av;

    if-nez p3, :cond_0

    invoke-interface {p2}, Lmaps/t/av;->a()Lmaps/t/ax;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ax;->f()Lmaps/t/bx;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lmaps/b/x;->c:Lmaps/t/bx;

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/x;->d:Ljava/util/Set;

    array-length v1, p4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p4, v0

    iget-object v3, p0, Lmaps/b/x;->d:Ljava/util/Set;

    invoke-static {v2}, Lmaps/t/v;->a(Ljava/lang/String;)Lmaps/t/v;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Ljava/util/Collection;Lmaps/t/an;)Ljava/util/Collection;
    .locals 4

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {}, Lmaps/f/fd;->b()Ljava/util/LinkedList;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/x;

    invoke-virtual {v0, p1}, Lmaps/b/x;->a(Lmaps/t/an;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lmaps/b/x;
    .locals 9

    const/4 v8, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const-string v3, "\\s+"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    if-ge v4, v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "INDOOR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse line: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    aget-object v4, v3, v8

    invoke-static {v4}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v3, v5

    invoke-static {v5}, Lmaps/bl/a;->a(Ljava/lang/String;)Lmaps/bl/a;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v6, v3, v6

    invoke-static {v6}, Lmaps/bl/a;->a(Ljava/lang/String;)Lmaps/bl/a;

    move-result-object v6

    if-eqz v4, :cond_3

    if-eqz v5, :cond_3

    if-nez v6, :cond_4

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "INDOOR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse line: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    array-length v2, v3

    if-le v2, v0, :cond_5

    aget-object v1, v3, v0

    invoke-static {v1}, Lmaps/b/x;->b(Ljava/lang/String;)Lmaps/t/bx;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v0, 0x4

    :cond_5
    invoke-virtual {v5}, Lmaps/bl/a;->a()I

    move-result v2

    invoke-virtual {v5}, Lmaps/bl/a;->b()I

    move-result v5

    invoke-static {v2, v5}, Lmaps/t/bx;->b(II)Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v6}, Lmaps/bl/a;->a()I

    move-result v5

    invoke-virtual {v6}, Lmaps/bl/a;->b()I

    move-result v6

    invoke-static {v5, v6}, Lmaps/t/bx;->b(II)Lmaps/t/bx;

    move-result-object v5

    array-length v6, v3

    sub-int/2addr v6, v0

    new-array v6, v6, [Ljava/lang/String;

    array-length v7, v6

    invoke-static {v3, v0, v6, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v0, Lmaps/b/x;

    invoke-static {v2, v5}, Lmaps/t/ax;->a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/ax;

    move-result-object v2

    invoke-direct {v0, v4, v2, v1, v6}, Lmaps/b/x;-><init>(Lmaps/t/bg;Lmaps/t/av;Lmaps/t/bx;[Ljava/lang/String;)V

    move-object v1, v0

    goto/16 :goto_0
.end method

.method public static a(Lmaps/t/bx;D)Lmaps/t/ax;
    .locals 2

    invoke-virtual {p0}, Lmaps/t/bx;->e()D

    move-result-wide v0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    invoke-static {p0, v0}, Lmaps/t/ax;->a(Lmaps/t/bx;I)Lmaps/t/ax;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Lmaps/t/bx;D)Lmaps/t/bg;
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {p1, p2, p3}, Lmaps/b/x;->a(Lmaps/t/bx;D)Lmaps/t/ax;

    move-result-object v4

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/x;

    invoke-virtual {v0, v4}, Lmaps/b/x;->a(Lmaps/t/an;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lmaps/b/x;->c()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v3, p1}, Lmaps/t/bx;->d(Lmaps/t/bx;)F

    move-result v3

    if-eqz v1, :cond_0

    cmpg-float v6, v3, v2

    if-gez v6, :cond_2

    :cond_0
    invoke-virtual {v0}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v0

    move v1, v3

    :goto_1
    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    move v1, v2

    goto :goto_1
.end method

.method static b(Ljava/lang/String;)Lmaps/t/bx;
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x6

    invoke-static {p0}, Lmaps/bl/a;->a(Ljava/lang/String;)Lmaps/bl/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/bl/a;->a()I

    move-result v1

    invoke-virtual {v0}, Lmaps/bl/a;->b()I

    move-result v0

    invoke-static {v1, v0}, Lmaps/t/bx;->b(II)Lmaps/t/bx;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0x1:0x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0xe

    if-gt v0, v2, :cond_1

    const-string v2, "0"

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    :try_start_0
    invoke-static {v2}, Lmaps/ae/e;->b(Ljava/lang/String;)I

    move-result v2

    invoke-static {v0}, Lmaps/ae/e;->b(Ljava/lang/String;)I

    move-result v3

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0, v2, v3}, Lmaps/t/bx;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lmaps/t/bg;
    .locals 1

    iget-object v0, p0, Lmaps/b/x;->a:Lmaps/t/bg;

    return-object v0
.end method

.method a(Ljava/util/Set;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/x;->d:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/b/x;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public a(Lmaps/t/an;)Z
    .locals 1

    iget-object v0, p0, Lmaps/b/x;->b:Lmaps/t/av;

    invoke-interface {v0, p1}, Lmaps/t/av;->a(Lmaps/t/an;)Z

    move-result v0

    return v0
.end method

.method public b()Lmaps/t/ax;
    .locals 1

    iget-object v0, p0, Lmaps/b/x;->b:Lmaps/t/av;

    invoke-interface {v0}, Lmaps/t/av;->a()Lmaps/t/ax;

    move-result-object v0

    return-object v0
.end method

.method public c()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/b/x;->c:Lmaps/t/bx;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/b/x;

    if-eqz v2, :cond_3

    check-cast p1, Lmaps/b/x;

    iget-object v2, p1, Lmaps/b/x;->a:Lmaps/t/bg;

    iget-object v3, p0, Lmaps/b/x;->a:Lmaps/t/bg;

    invoke-virtual {v2, v3}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lmaps/b/x;->b:Lmaps/t/av;

    iget-object v3, p0, Lmaps/b/x;->b:Lmaps/t/av;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lmaps/b/x;->c:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/b/x;->c:Lmaps/t/bx;

    invoke-virtual {v2, v3}, Lmaps/t/bx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lmaps/b/x;->d:Ljava/util/Set;

    iget-object v3, p0, Lmaps/b/x;->d:Ljava/util/Set;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lmaps/b/x;->b:Lmaps/t/av;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/b/x;->c:Lmaps/t/bx;

    invoke-virtual {v1}, Lmaps/t/bx;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/b/x;->d:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/b/x;->a:Lmaps/t/bg;

    invoke-virtual {v1}, Lmaps/t/bg;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/b/x;->a:Lmaps/t/bg;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/b/x;->b:Lmaps/t/av;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/b/x;->c:Lmaps/t/bx;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/b/x;->d:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
