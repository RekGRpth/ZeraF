.class Lmaps/o/e;
.super Lmaps/o/h;


# instance fields
.field private final B:Z


# direct methods
.method private constructor <init>(Lmaps/o/i;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/o/h;-><init>(Lmaps/o/k;Lmaps/o/r;)V

    invoke-static {p1}, Lmaps/o/i;->a(Lmaps/o/i;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/o/e;->B:Z

    return-void
.end method

.method synthetic constructor <init>(Lmaps/o/i;Lmaps/o/r;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/o/e;-><init>(Lmaps/o/i;)V

    return-void
.end method


# virtual methods
.method public a(ILmaps/af/q;)I
    .locals 1

    iget-boolean v0, p0, Lmaps/o/e;->B:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/af/q;->b:Lmaps/af/q;

    if-ne p2, v0, :cond_2

    :cond_0
    sget-object v0, Lmaps/af/q;->e:Lmaps/af/q;

    if-eq p2, v0, :cond_1

    sget-object v0, Lmaps/af/q;->d:Lmaps/af/q;

    if-ne p2, v0, :cond_3

    :cond_1
    const/4 p1, 0x0

    :cond_2
    :goto_0
    return p1

    :cond_3
    sget-object v0, Lmaps/af/q;->a:Lmaps/af/q;

    if-eq p2, v0, :cond_2

    sget-object v0, Lmaps/af/q;->c:Lmaps/af/q;

    if-eq p2, v0, :cond_2

    and-int/lit16 p1, p1, -0x1a07

    goto :goto_0
.end method

.method public a(Lmaps/t/ax;)Lmaps/t/bx;
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/t/ax;->a(I)Lmaps/t/bx;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x3e99999a

    invoke-interface {p1, v1, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    return-void
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public i()Lmaps/l/h;
    .locals 3

    new-instance v0, Lmaps/l/h;

    sget-object v1, Lmaps/l/af;->f:Lmaps/l/af;

    sget-object v2, Lmaps/l/w;->b:Lmaps/l/w;

    invoke-direct {v0, v1, v2}, Lmaps/l/h;-><init>(Lmaps/l/af;Lmaps/l/w;)V

    return-object v0
.end method

.method public j()Lmaps/t/aa;
    .locals 1

    invoke-static {}, Lmaps/o/c;->m()Lmaps/t/aa;

    move-result-object v0

    return-object v0
.end method
