.class public Lmaps/o/o;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/o/l;


# instance fields
.field private final a:Lmaps/t/al;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/t/al;

    invoke-direct {v0}, Lmaps/t/al;-><init>()V

    iput-object v0, p0, Lmaps/o/o;->a:Lmaps/t/al;

    return-void
.end method


# virtual methods
.method public a()Lmaps/t/al;
    .locals 3

    iget-object v1, p0, Lmaps/o/o;->a:Lmaps/t/al;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/t/al;

    iget-object v2, p0, Lmaps/o/o;->a:Lmaps/t/al;

    invoke-direct {v0, v2}, Lmaps/t/al;-><init>(Lmaps/t/al;)V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lmaps/t/al;)Z
    .locals 2

    if-nez p1, :cond_0

    new-instance p1, Lmaps/t/al;

    invoke-direct {p1}, Lmaps/t/al;-><init>()V

    :cond_0
    invoke-virtual {p1}, Lmaps/t/al;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cm;

    invoke-virtual {p0, p1, v0}, Lmaps/o/o;->a(Lmaps/t/al;Lmaps/t/cm;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lmaps/t/al;Lmaps/t/cm;)Z
    .locals 2

    invoke-static {p1, p2}, Lmaps/t/al;->a(Lmaps/t/al;Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v0

    iget-object v1, p0, Lmaps/o/o;->a:Lmaps/t/al;

    invoke-static {v1, p2}, Lmaps/t/al;->a(Lmaps/t/al;Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Lmaps/t/ao;->a(Lmaps/t/ao;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {v1, v0}, Lmaps/t/ao;->a(Lmaps/t/ao;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lmaps/t/ao;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/o/o;->a:Lmaps/t/al;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/o/o;->a:Lmaps/t/al;

    invoke-interface {p1}, Lmaps/t/ao;->a()Lmaps/t/cm;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/t/al;->a(Lmaps/t/cm;)Lmaps/t/ao;

    move-result-object v2

    invoke-static {v2, p1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/o/o;->a:Lmaps/t/al;

    invoke-virtual {v0, p1}, Lmaps/t/al;->a(Lmaps/t/ao;)V

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public b()J
    .locals 4

    iget-object v1, p0, Lmaps/o/o;->a:Lmaps/t/al;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/o/o;->a:Lmaps/t/al;

    invoke-virtual {v0}, Lmaps/t/al;->hashCode()I

    move-result v0

    int-to-long v2, v0

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
