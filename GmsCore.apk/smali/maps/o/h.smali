.class Lmaps/o/h;
.super Lmaps/o/c;


# instance fields
.field private final B:Z


# direct methods
.method private constructor <init>(Lmaps/o/k;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/o/c;-><init>(Lmaps/o/b;Lmaps/o/r;)V

    invoke-static {p1}, Lmaps/o/k;->a(Lmaps/o/k;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/o/h;->B:Z

    return-void
.end method

.method synthetic constructor <init>(Lmaps/o/k;Lmaps/o/r;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/o/h;-><init>(Lmaps/o/k;)V

    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    const/16 v0, 0x1000

    return v0
.end method

.method public a(Lmaps/ak/a;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/an/y;
    .locals 10

    sget-object v0, Lmaps/o/h;->n:Lmaps/o/c;

    if-ne p0, v0, :cond_1

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    invoke-static {}, Lmaps/k/b;->k()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lmaps/o/c;->b(Lmaps/o/c;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    :goto_1
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v3

    sget-object v0, Lmaps/o/c;->u:Lmaps/o/c;

    if-ne p0, v0, :cond_4

    invoke-static {}, Lmaps/o/c;->l()Lmaps/o/m;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/high16 v4, 0x3f800000

    goto :goto_1

    :cond_3
    invoke-static {}, Lmaps/o/c;->l()Lmaps/o/m;

    move-result-object v0

    invoke-interface {v0, p0, v3, v4, p4}, Lmaps/o/m;->a(Lmaps/o/c;IFLjava/util/Locale;)Lmaps/an/y;

    move-result-object v0

    goto :goto_0

    :cond_4
    if-eqz p6, :cond_5

    invoke-static {p0}, Lmaps/o/c;->c(Lmaps/o/c;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v6, 0x1

    :goto_2
    sget-boolean v0, Lmaps/ae/h;->z:Z

    if-eqz v0, :cond_6

    invoke-static {p2}, Lmaps/p/n;->a(Landroid/content/Context;)Lmaps/ag/g;

    move-result-object v9

    :goto_3
    iget-boolean v0, p0, Lmaps/o/c;->y:Z

    if-eqz v0, :cond_7

    new-instance v0, Lmaps/an/ac;

    sget-object v8, Lmaps/an/ac;->i:Lmaps/an/u;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v9}, Lmaps/an/ac;-><init>(Lmaps/ak/a;Lmaps/o/c;IFLjava/util/Locale;ZLjava/io/File;Lmaps/an/u;Lmaps/ag/g;)V

    goto :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    :cond_6
    const/4 v9, 0x0

    goto :goto_3

    :cond_7
    new-instance v0, Lmaps/an/ae;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p4

    move-object v7, p5

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lmaps/an/ae;-><init>(Lmaps/ak/a;Lmaps/o/c;IFLjava/util/Locale;ZLjava/io/File;Lmaps/ag/g;)V

    sget-object v1, Lmaps/o/c;->f:Lmaps/o/c;

    if-eq p0, v1, :cond_8

    sget-object v1, Lmaps/o/c;->g:Lmaps/o/c;

    if-ne p0, v1, :cond_9

    :cond_8
    const-wide/32 v1, 0x1d4c0

    invoke-virtual {v0, v1, v2}, Lmaps/an/ae;->a(J)V

    :cond_9
    iget-boolean v1, p0, Lmaps/o/h;->B:Z

    if-eqz v1, :cond_0

    if-eqz p7, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Lmaps/k/f;->a(B)Lmaps/k/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/an/ae;->a(Lmaps/k/f;)V

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/o/c;

    invoke-super {p0, p1}, Lmaps/o/c;->a(Lmaps/o/c;)I

    move-result v0

    return v0
.end method

.method public k()Lmaps/ag/e;
    .locals 1

    new-instance v0, Lmaps/o/g;

    invoke-direct {v0, p0}, Lmaps/o/g;-><init>(Lmaps/o/c;)V

    return-object v0
.end method
