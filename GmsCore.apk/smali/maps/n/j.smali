.class Lmaps/n/j;
.super Landroid/widget/ArrayAdapter;


# instance fields
.field final synthetic a:Lmaps/n/b;


# direct methods
.method constructor <init>(Lmaps/n/b;Landroid/content/Context;Lmaps/t/e;)V
    .locals 3

    iput-object p1, p0, Lmaps/n/j;->a:Lmaps/n/b;

    const/4 v0, -0x1

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p3}, Lmaps/t/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/n/c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/n/c;-><init>(Lmaps/t/bi;)V

    invoke-virtual {p0, v0}, Lmaps/n/j;->add(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p3}, Lmaps/t/e;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    new-instance v2, Lmaps/n/c;

    invoke-direct {v2, v0}, Lmaps/n/c;-><init>(Lmaps/t/bi;)V

    invoke-virtual {p0, v2}, Lmaps/n/j;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(I)I
    .locals 3

    const/4 v0, 0x1

    int-to-float v1, p1

    iget-object v2, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-virtual {v2}, Lmaps/n/b;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private a(Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)Landroid/widget/ImageView;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lmaps/n/j;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x2

    const/16 v3, 0x10

    invoke-direct {p0, v3}, Lmaps/n/j;->a(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v5}, Lmaps/n/j;->a(I)I

    move-result v1

    invoke-direct {p0, v5}, Lmaps/n/j;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private a(Landroid/widget/RelativeLayout;)Landroid/widget/TextView;
    .locals 4

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lmaps/n/j;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    const/4 v1, 0x2

    const/high16 v2, 0x41900000

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v1}, Lmaps/n/b;->a(Lmaps/n/b;)Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lmaps/ad/a;->Q:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/16 v3, 0x24

    invoke-direct {p0, v3}, Lmaps/n/j;->a(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p1, v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private a(Landroid/view/View;ILmaps/n/a;)V
    .locals 5

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v0}, Lmaps/n/b;->b(Lmaps/n/b;)I

    move-result v0

    if-ne p2, v0, :cond_1

    iget-object v0, p3, Lmaps/n/a;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v3}, Lmaps/n/b;->a(Lmaps/n/b;)Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lmaps/ad/a;->r:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v0}, Lmaps/n/b;->a(Lmaps/n/b;)Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lmaps/ad/e;->U:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Landroid/view/View;->destroyDrawingCache()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v0}, Lmaps/n/b;->c(Lmaps/n/b;)I

    move-result v0

    if-ne p2, v0, :cond_2

    iget-object v0, p3, Lmaps/n/a;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-virtual {p0, p2}, Lmaps/n/j;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/n/c;

    invoke-virtual {v0}, Lmaps/n/c;->a()Lmaps/t/bi;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v3}, Lmaps/n/b;->d(Lmaps/n/b;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v0}, Lmaps/t/bi;->a()Lmaps/t/bb;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iget-object v1, p3, Lmaps/n/a;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v0, p3, Lmaps/n/a;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v3}, Lmaps/n/b;->a(Lmaps/n/b;)Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lmaps/ad/a;->Q:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Landroid/view/View;->destroyDrawingCache()V

    goto :goto_0

    :cond_2
    iget-object v0, p3, Lmaps/n/a;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v4, -0x2

    move-object v0, p2

    check-cast v0, Landroid/widget/RelativeLayout;

    if-nez p2, :cond_1

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lmaps/n/j;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/16 v3, 0x2c

    invoke-direct {p0, v3}, Lmaps/n/j;->a(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/n/a;

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lmaps/n/j;->a(Landroid/widget/RelativeLayout;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lmaps/n/j;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v4, 0xe

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v3}, Lmaps/n/b;->a(Lmaps/n/b;)Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lmaps/ad/e;->V:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lmaps/n/j;->a(Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lmaps/n/j;->a:Lmaps/n/b;

    invoke-static {v4}, Lmaps/n/b;->a(Lmaps/n/b;)Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lmaps/ad/e;->W:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Lmaps/n/j;->a(Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)Landroid/widget/ImageView;

    move-result-object v4

    new-instance v0, Lmaps/n/a;

    invoke-direct {v0, v2, v3, v4}, Lmaps/n/a;-><init>(Landroid/widget/TextView;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    move-object v2, v0

    :goto_1
    iget-object v3, v2, Lmaps/n/a;->a:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lmaps/n/j;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/n/c;

    invoke-virtual {v0}, Lmaps/n/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v1, p1, v2}, Lmaps/n/j;->a(Landroid/view/View;ILmaps/n/a;)V

    return-object v1

    :cond_0
    move-object v2, v0

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
