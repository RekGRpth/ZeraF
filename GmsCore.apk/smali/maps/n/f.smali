.class Lmaps/n/f;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lmaps/b/r;

.field final synthetic b:Lmaps/t/e;

.field final synthetic c:Lmaps/n/b;


# direct methods
.method constructor <init>(Lmaps/n/b;Lmaps/b/r;Lmaps/t/e;)V
    .locals 0

    iput-object p1, p0, Lmaps/n/f;->c:Lmaps/n/b;

    iput-object p2, p0, Lmaps/n/f;->a:Lmaps/b/r;

    iput-object p3, p0, Lmaps/n/f;->b:Lmaps/t/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lmaps/n/f;->a:Lmaps/b/r;

    iget-object v1, p0, Lmaps/n/f;->b:Lmaps/t/e;

    invoke-virtual {v1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/b/r;->b(Lmaps/t/bg;)Lmaps/t/cu;

    move-result-object v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_0

    const-string v1, "INDOOR"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onIndoorLevelActivated: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/n/f;->b:Lmaps/t/e;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lmaps/n/f;->c:Lmaps/n/b;

    invoke-static {v1}, Lmaps/n/b;->f(Lmaps/n/b;)Lmaps/t/e;

    move-result-object v1

    iget-object v2, p0, Lmaps/n/f;->b:Lmaps/t/e;

    invoke-static {v1, v2}, Lmaps/n/b;->a(Lmaps/t/e;Lmaps/t/e;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/n/f;->c:Lmaps/n/b;

    iget-object v2, p0, Lmaps/n/f;->c:Lmaps/n/b;

    invoke-static {v2}, Lmaps/n/b;->f(Lmaps/n/b;)Lmaps/t/e;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lmaps/n/b;->a(Lmaps/n/b;Lmaps/t/e;Lmaps/t/bb;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v0}, Lmaps/t/cu;->c()Lmaps/t/bb;

    move-result-object v0

    goto :goto_0
.end method
