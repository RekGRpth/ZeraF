.class public final Lmaps/aa/f;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/aa/g;


# instance fields
.field private final a:Landroid/widget/RelativeLayout;

.field private final b:Ljava/util/Map;

.field private final c:Landroid/content/res/Resources;

.field private final d:Landroid/content/Context;

.field private e:Lmaps/aa/h;

.field private f:Lmaps/aa/c;

.field private g:Lmaps/i/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/aa/f;->b:Ljava/util/Map;

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmaps/aa/f;->a:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lmaps/aa/f;->a:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object p1, p0, Lmaps/aa/f;->d:Landroid/content/Context;

    iput-object p2, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    invoke-direct {p0}, Lmaps/aa/f;->e()V

    invoke-direct {p0}, Lmaps/aa/f;->f()V

    invoke-direct {p0}, Lmaps/aa/f;->g()V

    return-void
.end method

.method private a(ILandroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lmaps/aa/f;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lmaps/aa/f;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Lmaps/aa/f;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->removeViewInLayout(Landroid/view/View;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setId(I)V

    iget-object v2, p0, Lmaps/aa/f;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v2, p2, v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(ILandroid/widget/RelativeLayout$LayoutParams;)V
    .locals 3

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lmaps/aa/f;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setWillNotDraw(Z)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    invoke-virtual {v0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lmaps/aa/f;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/aa/f;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private e()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v2, Lmaps/cc/h;->c:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/cc/h;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v2, Lmaps/cc/h;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/cc/h;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v1, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/4 v1, 0x2

    invoke-direct {p0, v1, v0}, Lmaps/aa/f;->a(ILandroid/widget/RelativeLayout$LayoutParams;)V

    return-void
.end method

.method private f()V
    .locals 7

    const/4 v6, 0x3

    const/4 v3, 0x2

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/cc/h;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v4, Lmaps/cc/h;->a:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v5, Lmaps/cc/h;->a:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-direct {p0, v6, v0}, Lmaps/aa/f;->a(ILandroid/widget/RelativeLayout$LayoutParams;)V

    return-void
.end method

.method private g()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, -0x2

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v2, Lmaps/cc/h;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/cc/h;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lmaps/aa/f;->a(ILandroid/widget/RelativeLayout$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmaps/aa/f;->a:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public b()Lmaps/aa/h;
    .locals 2

    iget-object v0, p0, Lmaps/aa/f;->e:Lmaps/aa/h;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/aa/f;->d:Landroid/content/Context;

    iget-object v1, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lmaps/aa/a;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/aa/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/aa/f;->e:Lmaps/aa/h;

    const/4 v1, 0x1

    iget-object v0, p0, Lmaps/aa/f;->e:Lmaps/aa/h;

    check-cast v0, Lmaps/aa/a;

    invoke-virtual {v0}, Lmaps/aa/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lmaps/aa/f;->a(ILandroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lmaps/aa/f;->e:Lmaps/aa/h;

    return-object v0
.end method

.method public c()Lmaps/aa/c;
    .locals 2

    iget-object v0, p0, Lmaps/aa/f;->f:Lmaps/aa/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/aa/f;->d:Landroid/content/Context;

    iget-object v1, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lmaps/aa/c;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/aa/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/aa/f;->f:Lmaps/aa/c;

    const/4 v0, 0x2

    iget-object v1, p0, Lmaps/aa/f;->f:Lmaps/aa/c;

    invoke-virtual {v1}, Lmaps/aa/c;->a()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmaps/aa/f;->a(ILandroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lmaps/aa/f;->f:Lmaps/aa/c;

    return-object v0
.end method

.method public d()Lmaps/i/u;
    .locals 2

    iget-object v0, p0, Lmaps/aa/f;->g:Lmaps/i/u;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/aa/f;->d:Landroid/content/Context;

    iget-object v1, p0, Lmaps/aa/f;->c:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lmaps/i/a;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/i/u;

    move-result-object v0

    iput-object v0, p0, Lmaps/aa/f;->g:Lmaps/i/u;

    const/4 v1, 0x3

    iget-object v0, p0, Lmaps/aa/f;->g:Lmaps/i/u;

    check-cast v0, Lmaps/i/a;

    invoke-virtual {v0}, Lmaps/i/a;->b()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lmaps/aa/f;->a(ILandroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lmaps/aa/f;->g:Lmaps/i/u;

    return-object v0
.end method
