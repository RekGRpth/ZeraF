.class public Lmaps/t/br;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lmaps/t/aa;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lmaps/t/aa;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/br;->a:Ljava/lang/String;

    iput-object p2, p0, Lmaps/t/br;->b:Lmaps/t/aa;

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cl;)Lmaps/t/br;
    .locals 3

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-instance v2, Lmaps/t/br;

    invoke-virtual {p1, v1}, Lmaps/t/cl;->a(I)Lmaps/t/aa;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lmaps/t/br;-><init>(Ljava/lang/String;Lmaps/t/aa;)V

    return-object v2
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/br;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/br;->b:Lmaps/t/aa;

    return-object v0
.end method
