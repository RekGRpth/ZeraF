.class public Lmaps/t/bu;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# instance fields
.field private final a:I

.field private final b:Lmaps/t/v;

.field private final c:Lmaps/t/cg;

.field private final d:[Lmaps/t/bh;

.field private final e:Lmaps/t/aa;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:[I


# direct methods
.method public constructor <init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;III[I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/bu;->a:I

    iput-object p2, p0, Lmaps/t/bu;->b:Lmaps/t/v;

    iput-object p3, p0, Lmaps/t/bu;->c:Lmaps/t/cg;

    if-nez p4, :cond_0

    const/4 v0, 0x0

    new-array p4, v0, [Lmaps/t/bh;

    :cond_0
    iput-object p4, p0, Lmaps/t/bu;->d:[Lmaps/t/bh;

    iput-object p5, p0, Lmaps/t/bu;->e:Lmaps/t/aa;

    iput p6, p0, Lmaps/t/bu;->f:I

    iput-object p7, p0, Lmaps/t/bu;->g:Ljava/lang/String;

    iput p8, p0, Lmaps/t/bu;->h:I

    iput p9, p0, Lmaps/t/bu;->i:I

    iput p10, p0, Lmaps/t/bu;->j:I

    iput-object p11, p0, Lmaps/t/bu;->k:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/bu;
    .locals 12

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/t/cd;->b()Lmaps/t/ah;

    move-result-object v1

    invoke-static {p0, v1}, Lmaps/t/cg;->a(Ljava/io/DataInput;Lmaps/t/ah;)Lmaps/t/cg;

    move-result-object v3

    invoke-static {p0, p1}, Lmaps/t/aq;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/aq;

    move-result-object v7

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v4, v2, [Lmaps/t/bh;

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {p0, p1, v7}, Lmaps/t/bh;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/aq;)Lmaps/t/bh;

    move-result-object v5

    aput-object v5, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v8

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v9

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    const/4 v2, 0x0

    const/4 v5, 0x1

    invoke-static {v5, v1}, Lmaps/t/ab;->a(II)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {p0}, Lmaps/t/v;->a(Ljava/io/DataInput;)Lmaps/t/bs;

    move-result-object v2

    :cond_1
    :goto_1
    ushr-int/lit8 v10, v1, 0x2

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v11, v1, [I

    :goto_2
    if-ge v0, v1, :cond_3

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v11, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v5, 0x2

    invoke-static {v5, v1}, Lmaps/t/ab;->a(II)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0}, Lmaps/t/v;->b(Ljava/io/DataInput;)Lmaps/t/bt;

    move-result-object v2

    goto :goto_1

    :cond_3
    new-instance v0, Lmaps/t/bu;

    invoke-virtual {p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-virtual {v7}, Lmaps/t/aq;->a()Lmaps/t/aa;

    move-result-object v5

    invoke-virtual {v7}, Lmaps/t/aq;->c()I

    move-result v6

    invoke-virtual {v7}, Lmaps/t/aq;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v11}, Lmaps/t/bu;-><init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;III[I)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public a(I)Lmaps/t/bh;
    .locals 1

    iget-object v0, p0, Lmaps/t/bu;->d:[Lmaps/t/bh;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()Lmaps/t/v;
    .locals 1

    iget-object v0, p0, Lmaps/t/bu;->b:Lmaps/t/v;

    return-object v0
.end method

.method public c()Lmaps/t/cg;
    .locals 1

    iget-object v0, p0, Lmaps/t/bu;->c:Lmaps/t/cg;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lmaps/t/bu;->d:[Lmaps/t/bh;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/t/bu;->d:[Lmaps/t/bh;

    array-length v0, v0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    iget v0, p0, Lmaps/t/bu;->j:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 2

    iget v0, p0, Lmaps/t/bu;->j:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    invoke-virtual {p0}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/aa;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/t/bu;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Lmaps/t/bu;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lmaps/t/bu;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, p0, Lmaps/t/bu;->h:I

    const/16 v1, 0x80

    if-ge v0, v1, :cond_2

    iget v0, p0, Lmaps/t/bu;->j:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/bu;->e:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/bu;->i:I

    return v0
.end method

.method public j()[I
    .locals 1

    iget-object v0, p0, Lmaps/t/bu;->k:[I

    return-object v0
.end method

.method public k()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/t/bu;->c:Lmaps/t/cg;

    invoke-virtual {v1}, Lmaps/t/cg;->i()I

    move-result v1

    add-int/lit8 v3, v1, 0x3c

    iget-object v1, p0, Lmaps/t/bu;->d:[Lmaps/t/bh;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lmaps/t/bu;->d:[Lmaps/t/bh;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lmaps/t/bh;->d()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/t/bu;->b:Lmaps/t/v;

    invoke-static {v1}, Lmaps/t/ab;->a(Lmaps/t/v;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/t/bu;->e:Lmaps/t/aa;

    invoke-static {v1}, Lmaps/t/ab;->a(Lmaps/t/aa;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/t/bu;->g:Ljava/lang/String;

    invoke-static {v1}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    return v0
.end method

.method public l()Z
    .locals 2

    iget v0, p0, Lmaps/t/bu;->j:I

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/t/bu;->j:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    iget v0, p0, Lmaps/t/bu;->j:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 2

    iget v0, p0, Lmaps/t/bu;->j:I

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method
