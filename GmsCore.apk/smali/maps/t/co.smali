.class public Lmaps/t/co;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# instance fields
.field private final a:I

.field private final b:Lmaps/t/v;

.field private final c:Lmaps/t/ad;

.field private final d:[B

.field private final e:Lmaps/t/aa;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:I

.field private final j:[I


# direct methods
.method public constructor <init>(ILmaps/t/v;Lmaps/t/ad;[BLmaps/t/aa;ILjava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/co;->a:I

    iput-object p2, p0, Lmaps/t/co;->b:Lmaps/t/v;

    iput-object p3, p0, Lmaps/t/co;->c:Lmaps/t/ad;

    iput-object p4, p0, Lmaps/t/co;->d:[B

    iput-object p5, p0, Lmaps/t/co;->e:Lmaps/t/aa;

    iput p6, p0, Lmaps/t/co;->f:I

    iput-object p7, p0, Lmaps/t/co;->g:Ljava/lang/String;

    iput p8, p0, Lmaps/t/co;->h:I

    iput p9, p0, Lmaps/t/co;->i:I

    iput-object p10, p0, Lmaps/t/co;->j:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/co;
    .locals 11

    invoke-virtual {p1}, Lmaps/t/cd;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-static {p0, v0}, Lmaps/t/ad;->a(Ljava/io/DataInput;Lmaps/t/ah;)Lmaps/t/ad;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ad;->a()I

    move-result v0

    new-array v4, v0, [B

    invoke-interface {p0, v4}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {p0, p1}, Lmaps/t/aq;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/aq;

    move-result-object v7

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v8

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v9

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-static {v0, v9}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lmaps/t/v;->a(Ljava/io/DataInput;)Lmaps/t/bs;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v10, v1, [I

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v10, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x2

    invoke-static {v0, v9}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lmaps/t/v;->b(Ljava/io/DataInput;)Lmaps/t/bt;

    move-result-object v2

    goto :goto_0

    :cond_2
    new-instance v0, Lmaps/t/co;

    invoke-virtual {p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-virtual {v7}, Lmaps/t/aq;->a()Lmaps/t/aa;

    move-result-object v5

    invoke-virtual {v7}, Lmaps/t/aq;->c()I

    move-result v6

    invoke-virtual {v7}, Lmaps/t/aq;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v10}, Lmaps/t/co;-><init>(ILmaps/t/v;Lmaps/t/ad;[BLmaps/t/aa;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public b()Lmaps/t/v;
    .locals 1

    iget-object v0, p0, Lmaps/t/co;->b:Lmaps/t/v;

    return-object v0
.end method

.method public c()Lmaps/t/ad;
    .locals 1

    iget-object v0, p0, Lmaps/t/co;->c:Lmaps/t/ad;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lmaps/t/co;->d:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()[B
    .locals 1

    iget-object v0, p0, Lmaps/t/co;->d:[B

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lmaps/t/co;->f:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/co;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/co;->e:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/co;->h:I

    return v0
.end method

.method public j()[I
    .locals 1

    iget-object v0, p0, Lmaps/t/co;->j:[I

    return-object v0
.end method

.method public k()I
    .locals 3

    iget-object v0, p0, Lmaps/t/co;->c:Lmaps/t/ad;

    invoke-virtual {v0}, Lmaps/t/ad;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x3c

    iget-object v1, p0, Lmaps/t/co;->d:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/t/co;->b:Lmaps/t/v;

    invoke-static {v1}, Lmaps/t/ab;->a(Lmaps/t/v;)I

    move-result v1

    iget-object v2, p0, Lmaps/t/co;->g:Ljava/lang/String;

    invoke-static {v2}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/co;->e:Lmaps/t/aa;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/aa;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public l()Z
    .locals 2

    iget v0, p0, Lmaps/t/co;->i:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 2

    iget v0, p0, Lmaps/t/co;->i:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method
