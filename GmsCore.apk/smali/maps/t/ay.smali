.class public Lmaps/t/ay;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# instance fields
.field private a:I

.field private b:Lmaps/t/cg;

.field private c:Lmaps/t/aa;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private final h:[I


# direct methods
.method public constructor <init>(ILmaps/t/cg;Lmaps/t/aa;ILjava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/ay;->a:I

    iput-object p2, p0, Lmaps/t/ay;->b:Lmaps/t/cg;

    iput-object p3, p0, Lmaps/t/ay;->c:Lmaps/t/aa;

    iput p4, p0, Lmaps/t/ay;->d:I

    iput-object p5, p0, Lmaps/t/ay;->e:Ljava/lang/String;

    iput p6, p0, Lmaps/t/ay;->f:I

    iput p7, p0, Lmaps/t/ay;->g:I

    iput-object p8, p0, Lmaps/t/ay;->h:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/ay;
    .locals 9

    invoke-virtual {p1}, Lmaps/t/cd;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-static {p0, v0}, Lmaps/t/cg;->a(Ljava/io/DataInput;Lmaps/t/ah;)Lmaps/t/cg;

    move-result-object v2

    invoke-static {p0, p1}, Lmaps/t/aq;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/aq;

    move-result-object v5

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v6

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v7

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v8, v1, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v3

    aput v3, v8, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/t/ay;

    invoke-virtual {p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-virtual {v5}, Lmaps/t/aq;->a()Lmaps/t/aa;

    move-result-object v3

    invoke-virtual {v5}, Lmaps/t/aq;->c()I

    move-result v4

    invoke-virtual {v5}, Lmaps/t/aq;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v8}, Lmaps/t/ay;-><init>(ILmaps/t/cg;Lmaps/t/aa;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public b()Lmaps/t/v;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Lmaps/t/cg;
    .locals 1

    iget-object v0, p0, Lmaps/t/ay;->b:Lmaps/t/cg;

    return-object v0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/ay;->c:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/ay;->f:I

    return v0
.end method

.method public j()[I
    .locals 1

    iget-object v0, p0, Lmaps/t/ay;->h:[I

    return-object v0
.end method

.method public k()I
    .locals 2

    iget-object v0, p0, Lmaps/t/ay;->b:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->i()I

    move-result v0

    add-int/lit8 v0, v0, 0x2c

    iget-object v1, p0, Lmaps/t/ay;->e:Ljava/lang/String;

    invoke-static {v1}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/t/ay;->c:Lmaps/t/aa;

    invoke-static {v1}, Lmaps/t/ab;->a(Lmaps/t/aa;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
