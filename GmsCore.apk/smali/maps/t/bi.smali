.class public Lmaps/t/bi;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lmaps/t/bb;

.field private final f:Lmaps/t/bw;


# direct methods
.method private constructor <init>(Lmaps/t/bg;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IILmaps/t/bw;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmaps/t/bi;->a:Ljava/util/List;

    iput-object p3, p0, Lmaps/t/bi;->b:Ljava/lang/String;

    iput-object p4, p0, Lmaps/t/bi;->c:Ljava/lang/String;

    iput p5, p0, Lmaps/t/bi;->d:I

    new-instance v0, Lmaps/t/bb;

    invoke-direct {v0, p1, p6}, Lmaps/t/bb;-><init>(Lmaps/t/bg;I)V

    iput-object v0, p0, Lmaps/t/bi;->e:Lmaps/t/bb;

    iput-object p7, p0, Lmaps/t/bi;->f:Lmaps/t/bw;

    return-void
.end method

.method public static a(Lmaps/bb/c;)Lmaps/t/bi;
    .locals 12

    const/4 v11, 0x7

    const/4 v10, 0x5

    const/4 v5, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v1

    if-nez v1, :cond_1

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "INDOOR"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "malformed id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v8}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v9}, Lmaps/bb/c;->j(I)I

    move-result v3

    invoke-static {v3}, Lmaps/f/fd;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    move v0, v5

    :goto_1
    if-ge v0, v3, :cond_4

    invoke-virtual {p0, v9, v0}, Lmaps/bb/c;->h(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    sget-boolean v4, Lmaps/ae/h;->j:Z

    if-eqz v4, :cond_2

    const-string v4, "INDOOR"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "warning: malformed building id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v8}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v4

    if-nez v3, :cond_5

    if-eqz v4, :cond_b

    move-object v0, v4

    :goto_3
    move-object v3, v0

    :cond_5
    if-nez v4, :cond_6

    move-object v4, v3

    :cond_6
    invoke-virtual {p0, v10}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, v10}, Lmaps/bb/c;->d(I)I

    move-result v5

    :cond_7
    const/high16 v6, -0x80000000

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lmaps/bb/c;->d(I)I

    move-result v6

    :cond_8
    const/4 v7, 0x0

    invoke-virtual {p0, v11}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0, v11}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v0, v8}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v7

    invoke-static {v7}, Lmaps/t/bx;->a(Lmaps/bb/c;)Lmaps/t/bx;

    move-result-object v7

    invoke-virtual {v0, v9}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    invoke-static {v0}, Lmaps/t/bx;->a(Lmaps/bb/c;)Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v7}, Lmaps/t/bx;->f()I

    move-result v8

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v9

    if-le v8, v9, :cond_9

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v8

    const/high16 v9, 0x40000000

    add-int/2addr v8, v9

    invoke-virtual {v0, v8}, Lmaps/t/bx;->a(I)V

    :cond_9
    new-instance v8, Lmaps/t/ax;

    invoke-direct {v8, v7, v0}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v8}, Lmaps/t/bw;->a(Lmaps/t/ax;)Lmaps/t/bw;

    move-result-object v7

    :cond_a
    new-instance v0, Lmaps/t/bi;

    invoke-direct/range {v0 .. v7}, Lmaps/t/bi;-><init>(Lmaps/t/bg;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IILmaps/t/bw;)V

    goto/16 :goto_0

    :cond_b
    const-string v0, ""

    goto :goto_3
.end method


# virtual methods
.method public a()Lmaps/t/bb;
    .locals 1

    iget-object v0, p0, Lmaps/t/bi;->e:Lmaps/t/bb;

    return-object v0
.end method

.method public b()Lmaps/t/bg;
    .locals 1

    iget-object v0, p0, Lmaps/t/bi;->e:Lmaps/t/bb;

    invoke-virtual {v0}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/t/bi;->a:Ljava/util/List;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/bi;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/bi;->c:Ljava/lang/String;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lmaps/t/bi;->d:I

    return v0
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lmaps/t/bi;->e:Lmaps/t/bb;

    invoke-virtual {v0}, Lmaps/t/bb;->b()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/bi;->e:Lmaps/t/bb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
