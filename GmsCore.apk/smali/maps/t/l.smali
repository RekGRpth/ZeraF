.class public Lmaps/t/l;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# static fields
.field private static final n:[B

.field private static final o:[I


# instance fields
.field private final a:I

.field private final b:Lmaps/t/v;

.field private final c:Lmaps/t/ad;

.field private final d:Lmaps/t/ad;

.field private final e:[B

.field private final f:Lmaps/t/aa;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/t/l;->n:[B

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lmaps/t/l;->o:[I

    return-void

    :array_0
    .array-data 1
        0x1t
        0x2t
        0x4t
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
        0x1
        0x2
        0x1
        0x2
        0x2
        0x3
    .end array-data
.end method

.method public constructor <init>(ILmaps/t/v;Lmaps/t/ad;Lmaps/t/ad;[BIILmaps/t/aa;ILjava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/l;->a:I

    iput-object p2, p0, Lmaps/t/l;->b:Lmaps/t/v;

    iput-object p3, p0, Lmaps/t/l;->c:Lmaps/t/ad;

    iput-object p4, p0, Lmaps/t/l;->d:Lmaps/t/ad;

    iput-object p5, p0, Lmaps/t/l;->e:[B

    iput p6, p0, Lmaps/t/l;->i:I

    iput p7, p0, Lmaps/t/l;->j:I

    iput-object p8, p0, Lmaps/t/l;->f:Lmaps/t/aa;

    iput p9, p0, Lmaps/t/l;->g:I

    iput-object p10, p0, Lmaps/t/l;->h:Ljava/lang/String;

    iput p11, p0, Lmaps/t/l;->k:I

    iput p12, p0, Lmaps/t/l;->l:I

    iput-object p13, p0, Lmaps/t/l;->m:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/l;
    .locals 14

    invoke-virtual {p1}, Lmaps/t/cd;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-static {p0, v0}, Lmaps/t/ad;->a(Ljava/io/DataInput;Lmaps/t/ah;)Lmaps/t/ad;

    move-result-object v3

    invoke-static {p0, p1}, Lmaps/t/ad;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/ad;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/ad;->a()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v4}, Lmaps/t/ad;->a()I

    move-result v0

    :goto_1
    new-array v5, v0, [B

    invoke-interface {p0, v5}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v6

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v7

    invoke-static {p0, p1}, Lmaps/t/aq;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/aq;

    move-result-object v10

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v11

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v12

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-static {v0, v12}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lmaps/t/v;->a(Ljava/io/DataInput;)Lmaps/t/bs;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v13, v1, [I

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_4

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v8

    aput v8, v13, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Lmaps/t/ad;->a()I

    move-result v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    invoke-static {v0, v12}, Lmaps/t/ab;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lmaps/t/v;->b(Ljava/io/DataInput;)Lmaps/t/bt;

    move-result-object v2

    goto :goto_2

    :cond_4
    new-instance v0, Lmaps/t/l;

    invoke-virtual/range {p2 .. p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-virtual {v10}, Lmaps/t/aq;->a()Lmaps/t/aa;

    move-result-object v8

    invoke-virtual {v10}, Lmaps/t/aq;->c()I

    move-result v9

    invoke-virtual {v10}, Lmaps/t/aq;->b()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v0 .. v13}, Lmaps/t/l;-><init>(ILmaps/t/v;Lmaps/t/ad;Lmaps/t/ad;[BIILmaps/t/aa;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public a(II)Z
    .locals 2

    iget-object v0, p0, Lmaps/t/l;->e:[B

    aget-byte v0, v0, p1

    sget-object v1, Lmaps/t/l;->n:[B

    aget-byte v1, v1, p2

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lmaps/t/v;
    .locals 1

    iget-object v0, p0, Lmaps/t/l;->b:Lmaps/t/v;

    return-object v0
.end method

.method public c()Lmaps/t/ad;
    .locals 1

    iget-object v0, p0, Lmaps/t/l;->c:Lmaps/t/ad;

    return-object v0
.end method

.method public d()I
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lmaps/t/l;->e:[B

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lmaps/t/l;->o:[I

    iget-object v3, p0, Lmaps/t/l;->e:[B

    aget-byte v3, v3, v0

    and-int/lit8 v3, v3, 0x7

    aget v2, v2, v3

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public e()Z
    .locals 2

    iget v0, p0, Lmaps/t/l;->l:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lmaps/t/l;->i:I

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lmaps/t/l;->j:I

    return v0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/l;->f:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/l;->k:I

    return v0
.end method

.method public j()[I
    .locals 1

    iget-object v0, p0, Lmaps/t/l;->m:[I

    return-object v0
.end method

.method public k()I
    .locals 3

    iget-object v0, p0, Lmaps/t/l;->c:Lmaps/t/ad;

    invoke-virtual {v0}, Lmaps/t/ad;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x44

    iget-object v1, p0, Lmaps/t/l;->e:[B

    array-length v1, v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/t/l;->d:Lmaps/t/ad;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lmaps/t/l;->b:Lmaps/t/v;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/v;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/t/l;->h:Ljava/lang/String;

    invoke-static {v2}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/t/l;->f:Lmaps/t/aa;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/aa;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/t/l;->d:Lmaps/t/ad;

    invoke-virtual {v0}, Lmaps/t/ad;->b()I

    move-result v0

    goto :goto_0
.end method
