.class public Lmaps/t/az;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# instance fields
.field private final a:I

.field private final b:Lmaps/t/v;

.field private final c:Lmaps/t/cg;

.field private final d:[Lmaps/t/bh;

.field private final e:Ljava/lang/String;

.field private final f:Lmaps/t/aa;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:F

.field private final k:I

.field private final l:Z

.field private final m:[I


# direct methods
.method public constructor <init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;IFI[I)V
    .locals 13

    const/4 v12, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lmaps/t/az;-><init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;IFI[IZ)V

    return-void
.end method

.method public constructor <init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;IFI[IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/az;->a:I

    iput-object p2, p0, Lmaps/t/az;->b:Lmaps/t/v;

    iput-object p3, p0, Lmaps/t/az;->c:Lmaps/t/cg;

    iput-object p4, p0, Lmaps/t/az;->d:[Lmaps/t/bh;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/t/az;->e:Ljava/lang/String;

    iput-object p5, p0, Lmaps/t/az;->f:Lmaps/t/aa;

    iput p6, p0, Lmaps/t/az;->g:I

    iput-object p7, p0, Lmaps/t/az;->h:Ljava/lang/String;

    iput p8, p0, Lmaps/t/az;->i:I

    iput p9, p0, Lmaps/t/az;->j:F

    iput p10, p0, Lmaps/t/az;->k:I

    iput-object p11, p0, Lmaps/t/az;->m:[I

    iput-boolean p12, p0, Lmaps/t/az;->l:Z

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/az;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lmaps/t/az;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;Z)Lmaps/t/az;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;Z)Lmaps/t/az;
    .locals 12

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/t/cd;->b()Lmaps/t/ah;

    move-result-object v1

    invoke-static {p0, v1}, Lmaps/t/cg;->a(Ljava/io/DataInput;Lmaps/t/ah;)Lmaps/t/cg;

    move-result-object v3

    invoke-static {p0, p1}, Lmaps/t/aq;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/aq;

    move-result-object v7

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v4, v2, [Lmaps/t/bh;

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {p0, p1, v7}, Lmaps/t/bh;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/aq;)Lmaps/t/bh;

    move-result-object v5

    aput-object v5, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v8

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    invoke-static {v1}, Lmaps/t/ab;->c(I)F

    move-result v9

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v10

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {v1, v10}, Lmaps/t/ab;->a(II)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lmaps/t/v;->a(Ljava/io/DataInput;)Lmaps/t/bs;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v11, v1, [I

    :goto_2
    if-ge v0, v1, :cond_3

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v11, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x2

    invoke-static {v1, v10}, Lmaps/t/ab;->a(II)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lmaps/t/v;->b(Ljava/io/DataInput;)Lmaps/t/bt;

    move-result-object v2

    goto :goto_1

    :cond_3
    if-eqz p3, :cond_4

    new-instance v0, Lmaps/t/i;

    invoke-virtual {p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-virtual {v7}, Lmaps/t/aq;->a()Lmaps/t/aa;

    move-result-object v5

    invoke-virtual {v7}, Lmaps/t/aq;->c()I

    move-result v6

    invoke-virtual {v7}, Lmaps/t/aq;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v11}, Lmaps/t/i;-><init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;IFI[I)V

    :goto_3
    return-object v0

    :cond_4
    new-instance v0, Lmaps/t/az;

    invoke-virtual {p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-virtual {v7}, Lmaps/t/aq;->a()Lmaps/t/aa;

    move-result-object v5

    invoke-virtual {v7}, Lmaps/t/aq;->c()I

    move-result v6

    invoke-virtual {v7}, Lmaps/t/aq;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v11}, Lmaps/t/az;-><init>(ILmaps/t/v;Lmaps/t/cg;[Lmaps/t/bh;Lmaps/t/aa;ILjava/lang/String;IFI[I)V

    goto :goto_3
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public a(I)Lmaps/t/bh;
    .locals 1

    iget-object v0, p0, Lmaps/t/az;->d:[Lmaps/t/bh;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()Lmaps/t/v;
    .locals 1

    iget-object v0, p0, Lmaps/t/az;->b:Lmaps/t/v;

    return-object v0
.end method

.method public c()Lmaps/t/cg;
    .locals 1

    iget-object v0, p0, Lmaps/t/az;->c:Lmaps/t/cg;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lmaps/t/az;->d:[Lmaps/t/bh;

    array-length v0, v0

    return v0
.end method

.method public e()F
    .locals 1

    iget v0, p0, Lmaps/t/az;->j:F

    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/t/az;->l:Z

    return v0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/az;->f:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/az;->i:I

    return v0
.end method

.method public j()[I
    .locals 1

    iget-object v0, p0, Lmaps/t/az;->m:[I

    return-object v0
.end method

.method public k()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/t/az;->c:Lmaps/t/cg;

    invoke-virtual {v1}, Lmaps/t/cg;->i()I

    move-result v3

    iget-object v1, p0, Lmaps/t/az;->d:[Lmaps/t/bh;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lmaps/t/az;->d:[Lmaps/t/bh;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lmaps/t/bh;->d()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/t/az;->b:Lmaps/t/v;

    invoke-static {v1}, Lmaps/t/ab;->a(Lmaps/t/v;)I

    move-result v1

    add-int/lit8 v1, v1, 0x3c

    iget-object v2, p0, Lmaps/t/az;->h:Ljava/lang/String;

    invoke-static {v2}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/az;->f:Lmaps/t/aa;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/aa;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    return v0
.end method
