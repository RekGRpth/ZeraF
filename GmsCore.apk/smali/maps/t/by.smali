.class public Lmaps/t/by;
.super Ljava/lang/Object;


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:[I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/t/cg;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/t/by;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lmaps/t/by;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lmaps/t/by;->a:Z

    if-nez v0, :cond_0

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    mul-int/lit8 v0, p1, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/t/by;->b:[I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/t/by;->c:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/t/by;->c:I

    return v0
.end method

.method public a(I)Lmaps/t/bx;
    .locals 5

    mul-int/lit8 v0, p1, 0x3

    new-instance v1, Lmaps/t/bx;

    iget-object v2, p0, Lmaps/t/by;->b:[I

    aget v2, v2, v0

    iget-object v3, p0, Lmaps/t/by;->b:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lmaps/t/by;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lmaps/t/bx;-><init>(III)V

    return-object v1
.end method

.method public a(III)Z
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lmaps/t/by;->c:I

    mul-int/lit8 v1, v1, 0x3

    iget-object v2, p0, Lmaps/t/by;->b:[I

    array-length v2, v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmaps/t/by;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    iget-object v2, p0, Lmaps/t/by;->b:[I

    iget v3, p0, Lmaps/t/by;->c:I

    mul-int/lit8 v3, v3, 0x3

    invoke-static {v2, v0, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lmaps/t/by;->b:[I

    :cond_0
    iget v1, p0, Lmaps/t/by;->c:I

    mul-int/lit8 v1, v1, 0x3

    iget v2, p0, Lmaps/t/by;->c:I

    if-lez v2, :cond_1

    iget-object v2, p0, Lmaps/t/by;->b:[I

    add-int/lit8 v3, v1, -0x3

    aget v2, v2, v3

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lmaps/t/by;->b:[I

    add-int/lit8 v3, v1, -0x2

    aget v2, v2, v3

    if-ne p2, v2, :cond_1

    iget-object v2, p0, Lmaps/t/by;->b:[I

    add-int/lit8 v3, v1, -0x1

    aget v2, v2, v3

    if-ne p3, v2, :cond_1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/t/by;->b:[I

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/t/by;->b:[I

    add-int/lit8 v2, v1, 0x1

    aput p2, v0, v2

    iget-object v0, p0, Lmaps/t/by;->b:[I

    add-int/lit8 v1, v1, 0x2

    aput p3, v0, v1

    iget v0, p0, Lmaps/t/by;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/t/by;->c:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lmaps/t/bx;)Z
    .locals 3

    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v0

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v1

    invoke-virtual {p1}, Lmaps/t/bx;->h()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lmaps/t/by;->a(III)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lmaps/t/by;->c:I

    return-void
.end method

.method public c()Lmaps/t/cg;
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lmaps/t/by;->c:I

    mul-int/lit8 v0, v0, 0x3

    new-array v0, v0, [I

    iget-object v1, p0, Lmaps/t/by;->b:[I

    iget v2, p0, Lmaps/t/by;->c:I

    mul-int/lit8 v2, v2, 0x3

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Lmaps/t/cg;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lmaps/t/cg;-><init>([ILmaps/t/cn;)V

    return-object v1
.end method
