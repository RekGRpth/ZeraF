.class public Lmaps/s/g;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/cr/a;

.field private final b:Lmaps/al/q;

.field private final c:Lmaps/s/n;

.field private final d:Lmaps/s/l;


# direct methods
.method public constructor <init>(ILmaps/s/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x100

    invoke-static {p1, v0}, Lmaps/al/k;->c(II)Lmaps/al/k;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    :goto_0
    new-instance v0, Lmaps/s/n;

    invoke-direct {v0}, Lmaps/s/n;-><init>()V

    iput-object v0, p0, Lmaps/s/g;->c:Lmaps/s/n;

    iput-object p2, p0, Lmaps/s/g;->d:Lmaps/s/l;

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lmaps/al/k;->b(II)Lmaps/al/k;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    goto :goto_0
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    invoke-virtual {v0}, Lmaps/al/q;->d()I

    move-result v0

    return v0
.end method

.method public a(II)V
    .locals 3

    if-lez p2, :cond_0

    iget-object v0, p0, Lmaps/s/g;->d:Lmaps/s/l;

    iget-object v1, p0, Lmaps/s/g;->c:Lmaps/s/n;

    invoke-virtual {v0, p1, v1}, Lmaps/s/l;->a(ILmaps/s/n;)V

    iget-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    iget-object v1, p0, Lmaps/s/g;->c:Lmaps/s/n;

    iget v1, v1, Lmaps/s/n;->a:I

    iget-object v2, p0, Lmaps/s/g;->c:Lmaps/s/n;

    iget v2, v2, Lmaps/s/n;->b:I

    invoke-virtual {v0, v1, v2, p2}, Lmaps/al/q;->a(III)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 2

    iget-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->a()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lmaps/s/g;->c()V

    :cond_0
    iget-object v0, p0, Lmaps/s/g;->d:Lmaps/s/l;

    invoke-virtual {v0, p1}, Lmaps/s/l;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/s/g;->d:Lmaps/s/l;

    invoke-virtual {v0, p1}, Lmaps/s/l;->a(Lmaps/cr/c;)Lmaps/cr/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->f()V

    :cond_1
    iget-object v0, p0, Lmaps/s/g;->a:Lmaps/cr/a;

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    invoke-virtual {v0}, Lmaps/al/q;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    invoke-direct {p0}, Lmaps/s/g;->c()V

    return-void
.end method

.method public c(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/s/g;->b:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    invoke-direct {p0}, Lmaps/s/g;->c()V

    return-void
.end method
