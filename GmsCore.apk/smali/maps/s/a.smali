.class public Lmaps/s/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljavax/microedition/khronos/opengles/GL11;


# static fields
.field private static final g:Z


# instance fields
.field private a:Lmaps/s/o;

.field private b:Lmaps/s/o;

.field private c:Lmaps/s/o;

.field private d:Lmaps/s/o;

.field private e:Lmaps/s/f;

.field private f:I

.field private h:I

.field private i:I

.field private j:I

.field private k:[F

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->t:Z

    sput-boolean v0, Lmaps/s/a;->g:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/s/f;

    invoke-direct {v0}, Lmaps/s/f;-><init>()V

    iput-object v0, p0, Lmaps/s/a;->e:Lmaps/s/f;

    iput v2, p0, Lmaps/s/a;->f:I

    iput v2, p0, Lmaps/s/a;->h:I

    const/16 v0, 0x2100

    iput v0, p0, Lmaps/s/a;->i:I

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/s/a;->k:[F

    iput-boolean v2, p0, Lmaps/s/a;->l:Z

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "GL20 class is not ready for production use"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lmaps/s/o;

    const/16 v1, 0x1700

    invoke-direct {v0, v1}, Lmaps/s/o;-><init>(I)V

    iput-object v0, p0, Lmaps/s/a;->a:Lmaps/s/o;

    new-instance v0, Lmaps/s/o;

    const/16 v1, 0x1701

    invoke-direct {v0, v1}, Lmaps/s/o;-><init>(I)V

    iput-object v0, p0, Lmaps/s/a;->b:Lmaps/s/o;

    new-instance v0, Lmaps/s/o;

    const/16 v1, 0x1702

    invoke-direct {v0, v1}, Lmaps/s/o;-><init>(I)V

    iput-object v0, p0, Lmaps/s/a;->c:Lmaps/s/o;

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/o;

    iput-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/s/a;->c()V

    :cond_1
    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/high16 v1, 0x3f000000

    aput v1, v0, v2

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x3

    const/high16 v2, 0x3f800000

    aput v2, v0, v1

    return-void
.end method

.method private a(ILjava/lang/String;)I
    .locals 5

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v1, p2}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x8b81

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    aget v2, v2, v0

    if-nez v2, :cond_2

    sget-boolean v2, Lmaps/ae/h;->h:Z

    if-eqz v2, :cond_0

    const-string v2, "GL20"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not compile shader "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-boolean v2, Lmaps/ae/h;->h:Z

    if-eqz v2, :cond_1

    const-string v2, "GL20"

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    const v1, 0x8b31

    invoke-direct {p0, v1, p1}, Lmaps/s/a;->a(ILjava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_0

    const-string v1, "GL20"

    const-string v2, "VertexShader == 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    const v1, 0x8b30

    invoke-direct {p0, v1, p2}, Lmaps/s/a;->a(ILjava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_0

    const-string v1, "GL20"

    const-string v2, "FragmentShader == 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "GL20"

    const-string v4, "glAttachShader"

    invoke-static {v2, v4}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "GL20"

    const-string v3, "glAttachShader"

    invoke-static {v2, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "aPosition"

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v2, "GL20"

    const-string v3, "bindAttribute aPosition"

    invoke-static {v2, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x2

    const-string v3, "aColor"

    invoke-static {v1, v2, v3}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v2, "GL20"

    const-string v3, "bindAttribute aColor"

    invoke-static {v2, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x4

    const-string v3, "aTextureCoord0"

    invoke-static {v1, v2, v3}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v2, "GL20"

    const-string v3, "bindAttribute aTextureCoord0"

    invoke-static {v2, v3}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v2, v5, [I

    const v3, 0x8b82

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v2, v2, v0

    if-eq v2, v5, :cond_5

    sget-boolean v2, Lmaps/ae/h;->h:Z

    if-eqz v2, :cond_3

    const-string v2, "GL20"

    const-string v3, "Could not link program: "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sget-boolean v2, Lmaps/ae/h;->h:Z

    if-eqz v2, :cond_4

    const-string v2, "GL20"

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method private b()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c()V
    .locals 3

    const/4 v2, -0x1

    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat4 uTextureMatrix;\nattribute vec4 aPosition;\nattribute vec4 aColor;\nattribute vec2 aTextureCoord0;\nvarying vec2 vTextureCoord0;\nvarying vec4 uColor;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord0 = (uTextureMatrix * vec4(aTextureCoord0, 0.0, 0.0)).st;\n  vColor = aColor;\n}\n"

    const-string v1, "precision mediump float;\nconst int GL_MODULATE = 8448;\nconst int GL_REPLACE = 7681;\nconst int GMM_REPLACE_ALPHA = -2;\nvarying vec4 uColor;\nvarying vec4 vColor;\nvarying vec2 vTextureCoord0;\nuniform int uTextureEnvMode;\nuniform sampler2D sTexture0;\nuniform int uTexturesEnabled;\nvoid main() {\n  if (uTexturesEnabled != 0) {\n    if (uTextureEnvMode == GL_MODULATE) {\n      gl_FragColor = texture2D(sTexture0, vTextureCoord0) * vColor;\n    } else if (uTextureEnvMode == GMM_REPLACE_ALPHA) {\n      gl_FragColor.rgb = vColor.rgb;\n      gl_FragColor.a = texture2D(sTexture0, vTextureCoord0).a;\n    } else if (uTextureEnvMode == GL_REPLACE) {\n      gl_FragColor = texture2D(sTexture0, vTextureCoord0);\n    } else {\n      gl_FragColor = vColor;\n    }\n  } else {\n    gl_FragColor = vColor;\n  }\n}\n"

    invoke-direct {p0, v0, v1}, Lmaps/s/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/a;->m:I

    iget v0, p0, Lmaps/s/a;->m:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Shader init failure"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/s/a;->m:I

    const-string v1, "sTexture0"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/a;->n:I

    const-string v0, "GL20"

    const-string v1, "glGetUniformLocation sTexture0"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->n:I

    if-ne v0, v2, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not get attrib location for sTexture0"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lmaps/s/a;->m:I

    const-string v1, "uMVPMatrix"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/a;->o:I

    const-string v0, "GL20"

    const-string v1, "glGetUniformLocation uMVPMatrix"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->o:I

    if-ne v0, v2, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not get attrib location for uMVPMatrix"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, p0, Lmaps/s/a;->m:I

    const-string v1, "uTextureMatrix"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/a;->p:I

    const-string v0, "GL20"

    const-string v1, "glGetUniformLocation uTextureMatrix"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->p:I

    if-ne v0, v2, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not get attrib location for uTextureMatrix"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget v0, p0, Lmaps/s/a;->m:I

    const-string v1, "uTexturesEnabled"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/a;->q:I

    const-string v0, "GL20"

    const-string v1, "glGetUniformLocation muTextureEnabledHandle"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->q:I

    if-ne v0, v2, :cond_4

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not get attrib location for uTexturesEnabled"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget v0, p0, Lmaps/s/a;->m:I

    const-string v1, "uTextureEnvMode"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/s/a;->j:I

    const-string v0, "GL20"

    const-string v1, "glGetUniformLocation uTextureEnvMode"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->j:I

    if-ne v0, v2, :cond_5

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not get attrib location for uTextureEnvMode"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-void
.end method

.method private d()V
    .locals 7

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lmaps/s/a;->m:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    const-string v0, "GL20"

    const-string v2, "bindAttribute glUseProgram"

    invoke-static {v0, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/s/a;->n:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    invoke-static {v4}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    :goto_0
    const-string v0, "GL20"

    const-string v2, "tex coord array setup"

    invoke-static {v0, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->h:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/s/a;->k:[F

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    invoke-static {v3}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :goto_1
    const-string v0, "GL20"

    const-string v2, "color coord array setup"

    invoke-static {v0, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->j:I

    iget v2, p0, Lmaps/s/a;->i:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    const-string v0, "GL20"

    const-string v2, "tex env mode"

    invoke-static {v0, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lmaps/s/a;->p:I

    iget-object v2, p0, Lmaps/s/a;->c:Lmaps/s/o;

    invoke-virtual {v2}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v2

    invoke-static {v2}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v2

    invoke-static {v0, v6, v1, v2, v1}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    const-string v0, "GL20"

    const-string v2, "tex matrix"

    invoke-static {v0, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, p0, Lmaps/s/a;->q:I

    iget-boolean v0, p0, Lmaps/s/a;->l:Z

    if-eqz v0, :cond_2

    move v0, v6

    :goto_2
    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glUniform1i(II)V

    const-string v0, "GL20"

    const-string v2, "Texture Enabled"

    invoke-static {v0, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/s/a;->e:Lmaps/s/f;

    invoke-static {v0}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v0

    iget-object v2, p0, Lmaps/s/a;->b:Lmaps/s/o;

    invoke-virtual {v2}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v2

    invoke-static {v2}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v2

    iget-object v3, p0, Lmaps/s/a;->a:Lmaps/s/o;

    invoke-virtual {v3}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v3

    invoke-static {v3}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v4

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lmaps/s/a;->o:I

    iget-object v2, p0, Lmaps/s/a;->e:Lmaps/s/f;

    invoke-static {v2}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v2

    invoke-static {v0, v6, v1, v2, v1}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    const-string v0, "GL20"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {v4}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    goto/16 :goto_0

    :cond_1
    invoke-static {v3}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    const/4 v0, 0x2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    return-void
.end method

.method public a(I)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lmaps/s/a;->b()V

    goto :goto_0
.end method

.method public a(IIIZII)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static/range {p1 .. p6}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0}, Lmaps/s/a;->b()V

    goto :goto_0
.end method

.method public a(IIIZILjava/nio/Buffer;)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static/range {p1 .. p6}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0}, Lmaps/s/a;->b()V

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lmaps/s/a;->b()V

    goto :goto_0
.end method

.method public glActiveTexture(I)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glAlphaFunc(IF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glAlphaFuncx(II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glBindBuffer(II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glBindBuffer(II)V

    goto :goto_0
.end method

.method public glBindTexture(II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glBindTexture(II)V

    goto :goto_0
.end method

.method public glBlendFunc(II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glBlendFunc(II)V

    goto :goto_0
.end method

.method public glBufferData(IILjava/nio/Buffer;I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glBufferData(IILjava/nio/Buffer;I)V

    goto :goto_0
.end method

.method public glBufferSubData(IIILjava/nio/Buffer;)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public glClear(I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glClear(I)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glClear(I)V

    goto :goto_0
.end method

.method public glClearColor(FFFF)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glClearColor(FFFF)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public glClearColorx(IIII)V
    .locals 5

    const/high16 v4, 0x43800000

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Draw Count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/s/a;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/s/a;->f:I

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    int-to-float v0, p1

    div-float/2addr v0, v4

    int-to-float v1, p2

    div-float/2addr v1, v4

    int-to-float v2, p3

    div-float/2addr v2, v4

    int-to-float v3, p4

    div-float/2addr v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glClearColorx(IIII)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public glClearDepthf(F)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glClearDepthx(I)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glClearStencil(I)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glClearStencil(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glClearStencil(I)V

    goto :goto_0
.end method

.method public glClientActiveTexture(I)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glClipPlanef(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glClipPlanef(I[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glClipPlanex(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glClipPlanex(I[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glColor4f(FFFF)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColor4f(FFFF)V

    goto :goto_0
.end method

.method public glColor4ub(BBBB)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glColor4x(IIII)V
    .locals 4

    const/high16 v3, 0x10000

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x0

    div-int v2, p1, v3

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x1

    div-int v2, p2, v3

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x2

    div-int v2, p3, v3

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/s/a;->k:[F

    const/4 v1, 0x3

    div-int v2, p4, v3

    int-to-float v2, v2

    aput v2, v0, v1

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColor4x(IIII)V

    goto :goto_0
.end method

.method public glColorMask(ZZZZ)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glColorPointer(IIII)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glColorPointer(IIII)V

    goto :goto_0
.end method

.method public glColorPointer(IIILjava/nio/Buffer;)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColorPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glCompressedTexSubImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glCopyTexImage2D(IIIIIIII)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glCopyTexSubImage2D(IIIIIIII)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glCullFace(I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glCullFace(I)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glCullFace(I)V

    goto :goto_0
.end method

.method public glDeleteBuffers(ILjava/nio/IntBuffer;)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glDeleteBuffers(ILjava/nio/IntBuffer;)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glDeleteBuffers(ILjava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public glDeleteBuffers(I[II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES11;->glDeleteBuffers(I[II)V

    goto :goto_0
.end method

.method public glDeleteTextures(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glDeleteTextures(I[II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glDeleteTextures(I[II)V

    goto :goto_0
.end method

.method public glDepthFunc(I)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glDepthFunc(I)V

    goto :goto_0
.end method

.method public glDepthMask(Z)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glDepthRangef(FF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glDepthRangex(II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glDisable(I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xde1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/s/a;->l:Z

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES20;->glDisable(I)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glDisable(I)V

    goto :goto_0
.end method

.method public glDisableClientState(I)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :pswitch_1
    iget v0, p0, Lmaps/s/a;->h:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lmaps/s/a;->h:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lmaps/s/a;->h:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lmaps/s/a;->h:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lmaps/s/a;->h:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lmaps/s/a;->h:I

    goto :goto_0

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8074
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public glDrawArrays(III)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lmaps/s/a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/a;->f:I

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->d()V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/s/a;->b:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    goto :goto_0
.end method

.method public glDrawElements(IIII)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lmaps/s/a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/a;->f:I

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/s/a;->d()V

    iget v0, p0, Lmaps/s/a;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glDrawElements(IIII)V

    :cond_0
    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/s/a;->b:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glDrawElements(IIII)V

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    goto :goto_0
.end method

.method public glDrawElements(IIILjava/nio/Buffer;)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lmaps/s/a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/a;->f:I

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/s/a;->d()V

    iget v0, p0, Lmaps/s/a;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    :cond_0
    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/s/a;->b:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/s/f;->b(Lmaps/s/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glDrawElements(IIILjava/nio/Buffer;)V

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    goto :goto_0
.end method

.method public glEnable(I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xde1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/s/a;->l:Z

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES20;->glEnable(I)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glEnable(I)V

    goto :goto_0
.end method

.method public glEnableClientState(I)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :pswitch_1
    iget v0, p0, Lmaps/s/a;->h:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/s/a;->h:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lmaps/s/a;->h:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lmaps/s/a;->h:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lmaps/s/a;->h:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/s/a;->h:I

    goto :goto_0

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8074
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public glFinish()V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFlush()V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFogf(IF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFogfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFogfv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFogx(II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFogxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFogxv(I[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFrontFace(I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glFrontFace(I)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glFrontFace(I)V

    goto :goto_0
.end method

.method public glFrustumf(FFFFFF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glFrustumx(IIIIII)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGenBuffers(ILjava/nio/IntBuffer;)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glGenBuffers(ILjava/nio/IntBuffer;)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glGenBuffers(ILjava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public glGenBuffers(I[II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES11;->glGenBuffers(I[II)V

    goto :goto_0
.end method

.method public glGenTextures(ILjava/nio/IntBuffer;)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glGenTextures(ILjava/nio/IntBuffer;)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glGenTextures(ILjava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public glGenTextures(I[II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glGenTextures(I[II)V

    goto :goto_0
.end method

.method public glGetBooleanv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetBooleanv(I[ZI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetBufferParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetBufferParameteriv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetClipPlanef(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetClipPlanef(I[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetClipPlanex(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetClipPlanex(I[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetError()I
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/opengl/GLES10;->glGetError()I

    move-result v0

    goto :goto_0
.end method

.method public glGetFixedv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetFixedv(I[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetFloatv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetFloatv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetIntegerv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetIntegerv(I[II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glGetIntegerv(I[II)V

    goto :goto_0
.end method

.method public glGetLightfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetLightfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetLightxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetLightxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetMaterialfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetMaterialfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetMaterialxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetMaterialxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetPointerv(I[Ljava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetString(I)Ljava/lang/String;
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public glGetTexEnviv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexEnviv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexEnvxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexEnvxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexParameterfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexParameterfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexParameteriv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexParameterxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glGetTexParameterxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glHint(II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GL20"

    const-string v1, "glHint ignored"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glHint(II)V

    goto :goto_0
.end method

.method public glIsBuffer(I)Z
    .locals 1

    invoke-direct {p0}, Lmaps/s/a;->b()V

    const/4 v0, 0x0

    return v0
.end method

.method public glIsEnabled(I)Z
    .locals 1

    invoke-direct {p0}, Lmaps/s/a;->b()V

    const/4 v0, 0x0

    return v0
.end method

.method public glIsTexture(I)Z
    .locals 1

    invoke-direct {p0}, Lmaps/s/a;->b()V

    const/4 v0, 0x0

    return v0
.end method

.method public glLightModelf(IF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightModelfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightModelfv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightModelx(II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightModelxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightModelxv(I[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightf(IIF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightx(III)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLightxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLineWidth(F)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GL20"

    const-string v1, "glLineWidth not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glLineWidth(F)V

    goto :goto_0
.end method

.method public glLineWidthx(I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GL20"

    const-string v1, "glLineWidthx not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glLineWidthx(I)V

    goto :goto_0
.end method

.method public glLoadIdentity()V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/s/f;->a()Lmaps/s/f;

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES10;->glLoadIdentity()V

    :cond_0
    return-void
.end method

.method public glLoadMatrixf(Ljava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLoadMatrixf([FI)V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/s/f;->b([FI)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    :cond_0
    return-void
.end method

.method public glLoadMatrixx(Ljava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLoadMatrixx([II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glLogicOp(I)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMaterialf(IIF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMaterialfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMaterialfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMaterialx(III)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMaterialxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMaterialxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMatrixMode(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unexpected value"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/o;

    iput-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    :goto_0
    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    :cond_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lmaps/s/a;->b:Lmaps/s/o;

    iput-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/s/a;->c:Lmaps/s/o;

    iput-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1700
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public glMultMatrixf(Ljava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMultMatrixf([FI)V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/s/f;->a([FI)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glMultMatrixf([FI)V

    :cond_0
    return-void
.end method

.method public glMultMatrixx(Ljava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMultMatrixx([II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMultiTexCoord4f(IFFFF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glMultiTexCoord4x(IIIII)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glNormal3f(FFF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glNormal3x(III)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glNormalPointer(III)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glNormalPointer(IILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glOrthof(FFFFFF)V
    .locals 8

    const/4 v1, 0x0

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    new-array v0, v0, [F

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    iget-object v2, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v2}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmaps/s/f;->a([FI)V

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p1 .. p6}, Landroid/opengl/GLES10;->glOrthof(FFFFFF)V

    goto :goto_0
.end method

.method public glOrthox(IIIIII)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPixelStorei(II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointParameterf(IF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointParameterfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointParameterfv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointParameterx(II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointParameterxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointParameterxv(I[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointSize(F)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "GL20"

    const-string v1, "glPointSize not supported in ES2"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glPointSize(F)V

    goto :goto_0
.end method

.method public glPointSizePointerOES(IILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPointSizex(I)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glPolygonOffset(FF)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glPolygonOffset(FF)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glPolygonOffset(FF)V

    goto :goto_0
.end method

.method public glPolygonOffsetx(II)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    int-to-float v0, p1

    int-to-float v1, p2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPolygonOffset(FF)V

    :goto_0
    return-void

    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glPolygonOffset(FF)V

    goto :goto_0
.end method

.method public glPopMatrix()V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->b()Lmaps/s/f;

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES10;->glPopMatrix()V

    :cond_0
    return-void
.end method

.method public glPushMatrix()V
    .locals 2

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    iget-object v1, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v1}, Lmaps/s/o;->a()Lmaps/s/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/s/f;->a(Lmaps/s/f;)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES10;->glPushMatrix()V

    :cond_0
    return-void
.end method

.method public glReadPixels(IIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glRotatef(FFFF)V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/s/f;->a(FFFF)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glRotatef(FFFF)V

    :cond_0
    return-void
.end method

.method public glRotatex(IIII)V
    .locals 5

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Lmaps/s/f;->a(FFFF)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glRotatex(IIII)V

    :cond_0
    return-void
.end method

.method public glSampleCoverage(FZ)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glSampleCoveragex(IZ)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glScalef(FFF)V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lmaps/s/f;->b(FFF)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glScalef(FFF)V

    :cond_0
    return-void
.end method

.method public glScalex(III)V
    .locals 4

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/s/f;->b(FFF)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glScalex(III)V

    :cond_0
    return-void
.end method

.method public glScissor(IIII)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glShadeModel(I)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "GL20"

    const-string v1, "glShadeModel unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glShadeModel(I)V

    goto :goto_0
.end method

.method public glStencilFunc(III)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glStencilMask(I)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glStencilMask(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glStencilMask(I)V

    goto :goto_0
.end method

.method public glStencilOp(III)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glStencilOp(III)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glStencilOp(III)V

    goto :goto_0
.end method

.method public glTexCoordPointer(IIII)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    const-string v0, "GL20"

    const-string v1, "glTexCoordPointer"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glTexCoordPointer(IIII)V

    goto :goto_0
.end method

.method public glTexCoordPointer(IIILjava/nio/Buffer;)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public glTexEnvf(IIF)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexEnvfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexEnvfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexEnvi(III)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexEnviv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexEnviv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexEnvx(III)V
    .locals 3

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x2300

    if-ne p1, v0, :cond_3

    const/16 v0, 0x2200

    if-ne p2, v0, :cond_3

    const/16 v0, 0x2100

    if-eq p3, v0, :cond_0

    const/16 v0, 0x1e01

    if-eq p3, v0, :cond_0

    const/4 v0, -0x2

    if-ne p3, v0, :cond_2

    :cond_0
    iput p3, p0, Lmaps/s/a;->i:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-boolean v0, Lmaps/ae/h;->h:Z

    if-eqz v0, :cond_1

    const-string v0, "GL20"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glTexEnvx unsupported GL_TEXTURE_ENV_MODE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->h:Z

    if-eqz v0, :cond_1

    const-string v0, "GL20"

    const-string v1, "glTexEnvx target or pname is unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexEnvx(III)V

    goto :goto_0
.end method

.method public glTexEnvxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexEnvxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexParameterf(IIF)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_2

    const v0, 0x8191

    if-ne p2, v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GL20"

    const-string v1, "glTexParameter GL11.GL_GENERATE_MIPMAP ignored"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    goto :goto_0

    :cond_2
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexParameterf(IIF)V

    goto :goto_0
.end method

.method public glTexParameterfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexParameterfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexParameteri(III)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexParameteriv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexParameterx(III)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    goto :goto_0
.end method

.method public glTexParameterxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexParameterxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTexSubImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    return-void
.end method

.method public glTranslatef(FFF)V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lmaps/s/f;->a(FFF)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTranslatef(FFF)V

    :cond_0
    return-void
.end method

.method public glTranslatex(III)V
    .locals 4

    iget-object v0, p0, Lmaps/s/a;->d:Lmaps/s/o;

    invoke-virtual {v0}, Lmaps/s/o;->c()Lmaps/s/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/s/f;->a(FFF)V

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-nez v0, :cond_0

    int-to-float v0, p1

    int-to-float v1, p2

    int-to-float v2, p3

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES10;->glTranslatef(FFF)V

    :cond_0
    return-void
.end method

.method public glVertexPointer(IIII)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glVertexPointer(IIII)V

    goto :goto_0
.end method

.method public glVertexPointer(IIILjava/nio/Buffer;)V
    .locals 1

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/s/a;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glVertexPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public glViewport(IIII)V
    .locals 2

    sget-boolean v0, Lmaps/s/a;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    :goto_0
    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glViewport(IIII)V

    goto :goto_0
.end method
