.class public Lmaps/s/q;
.super Ljava/lang/Object;


# static fields
.field private static final a:F

.field private static final t:Ljava/lang/ThreadLocal;

.field private static final u:[I

.field private static final v:[I

.field private static final w:[I

.field private static final x:[I

.field private static final y:[[I


# instance fields
.field private final b:Lmaps/t/bx;

.field private final c:Lmaps/t/bx;

.field private final d:Lmaps/t/bx;

.field private final e:Lmaps/t/bx;

.field private final f:Lmaps/t/bx;

.field private final g:Lmaps/t/bx;

.field private final h:Lmaps/t/bx;

.field private final i:Lmaps/t/bx;

.field private final j:Lmaps/t/bx;

.field private final k:Lmaps/t/bx;

.field private final l:Lmaps/t/cq;

.field private final m:Lmaps/t/cq;

.field private final n:Lmaps/t/cq;

.field private final o:Lmaps/t/cq;

.field private final p:Lmaps/t/cq;

.field private final q:Lmaps/t/cq;

.field private final r:Lmaps/t/cq;

.field private final s:Lmaps/t/cq;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v2, 0x8

    const-wide/high16 v0, 0x4000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/s/q;->a:F

    new-instance v0, Lmaps/s/c;

    invoke-direct {v0}, Lmaps/s/c;-><init>()V

    sput-object v0, Lmaps/s/q;->t:Ljava/lang/ThreadLocal;

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/s/q;->u:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lmaps/s/q;->v:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lmaps/s/q;->w:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lmaps/s/q;->x:[I

    const/16 v0, 0x10

    new-array v0, v0, [[I

    sput-object v0, Lmaps/s/q;->y:[[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x10000
        0x10000
        0x10000
        0x0
        0x8000
        0x10000
        0x8000
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x8000
        0x10000
        0x8000
        0x0
        0x10000
        0x10000
        0x10000
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x4000
        0x10000
        0x4000
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x4000
        0x10000
        0x4000
        0x8000
        0x4000
        0x0
        0x4000
        0x10000
        0x4000
    .end array-data
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->b:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->c:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->d:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->e:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->f:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->g:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->h:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->i:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->j:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->k:Lmaps/t/bx;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->l:Lmaps/t/cq;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->m:Lmaps/t/cq;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->n:Lmaps/t/cq;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->o:Lmaps/t/cq;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->p:Lmaps/t/cq;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->q:Lmaps/t/cq;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->r:Lmaps/t/cq;

    new-instance v0, Lmaps/t/cq;

    invoke-direct {v0}, Lmaps/t/cq;-><init>()V

    iput-object v0, p0, Lmaps/s/q;->s:Lmaps/t/cq;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/s/c;)V
    .locals 0

    invoke-direct {p0}, Lmaps/s/q;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)I
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static a(Lmaps/t/cg;)I
    .locals 2

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x8

    goto :goto_0
.end method

.method public static a()Lmaps/s/q;
    .locals 1

    sget-object v0, Lmaps/s/q;->t:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/s/q;

    return-object v0
.end method

.method private a(Lmaps/t/bx;Lmaps/t/bx;FIZLmaps/al/l;)V
    .locals 2

    iget-object v0, p0, Lmaps/s/q;->d:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/s/q;->e:Lmaps/t/bx;

    invoke-static {p2, p1, v0}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v0, p3, v1}, Lmaps/t/cb;->a(Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-static {p1, v1, v0}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-interface {p6, v0, p4}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    invoke-static {p1, v1, v0}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-interface {p6, v0, p4}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    invoke-static {p2, v1, v0}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-interface {p6, v0, p4}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    invoke-static {p2, v1, v0}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-interface {p6, v0, p4}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    if-eqz p5, :cond_0

    invoke-interface {p6, p2, p4}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    :cond_0
    return-void
.end method

.method private static a(I)[I
    .locals 7

    const v6, 0x8000

    const/high16 v5, 0x10000

    const/4 v1, 0x0

    mul-int/lit8 v0, p0, 0x5

    mul-int/lit8 v0, v0, 0x2

    new-array v3, v0, [I

    div-int v0, v6, p0

    move v2, v0

    move v0, v1

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    aput v1, v3, v0

    add-int/lit8 v4, v0, 0x1

    aput v2, v3, v4

    add-int/lit8 v4, v0, 0x2

    aput v5, v3, v4

    add-int/lit8 v4, v0, 0x3

    aput v2, v3, v4

    add-int/lit8 v4, v0, 0x4

    aput v5, v3, v4

    add-int/lit8 v4, v0, 0x5

    aput v2, v3, v4

    add-int/lit8 v4, v0, 0x6

    aput v1, v3, v4

    add-int/lit8 v4, v0, 0x7

    aput v2, v3, v4

    add-int/lit8 v4, v0, 0x8

    aput v6, v3, v4

    add-int/lit8 v4, v0, 0x9

    aput v2, v3, v4

    div-int v4, v5, p0

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public static b(Ljava/util/List;)I
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0}, Lmaps/t/cg;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v2, 0x1

    :goto_1
    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x6

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public static b(Lmaps/t/cg;)I
    .locals 2

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0xc

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v1, v0, 0x2

    mul-int/lit8 v1, v1, 0x6

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private b(Lmaps/t/cg;FLmaps/t/bx;ILmaps/al/l;Lmaps/al/c;Lmaps/al/b;ZZB)I
    .locals 10

    invoke-interface {p5}, Lmaps/al/l;->c()I

    move-result v1

    iget-object v2, p0, Lmaps/s/q;->b:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/s/q;->c:Lmaps/t/bx;

    iget-object v4, p0, Lmaps/s/q;->d:Lmaps/t/bx;

    iget-object v5, p0, Lmaps/s/q;->e:Lmaps/t/bx;

    iget-object v6, p0, Lmaps/s/q;->f:Lmaps/t/bx;

    iget-object v7, p0, Lmaps/s/q;->g:Lmaps/t/bx;

    iget-object v8, p0, Lmaps/s/q;->h:Lmaps/t/bx;

    const/4 v9, 0x0

    invoke-virtual {p1, v9, p3, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    const/4 v9, 0x1

    invoke-virtual {p1, v9, p3, v3}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v3, v2, v4}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v4, p2, v5}, Lmaps/t/cb;->a(Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-static {v5, v6}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;)V

    if-eqz p8, :cond_0

    invoke-static {v2, v6, v2}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    :cond_0
    if-eqz p9, :cond_1

    invoke-static {v3, v6, v3}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    :cond_1
    invoke-static {v2, v5, v8}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    invoke-static {v2, v5, v8}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    invoke-static {v2, v3, v7}, Lmaps/t/cb;->e(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v7, v5, v8}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    invoke-static {v7, v5, v8}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    invoke-static {v3, v5, v8}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    invoke-static {v3, v5, v8}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    const/4 v2, 0x6

    const/high16 v3, -0x41800000

    invoke-virtual {v4}, Lmaps/t/bx;->i()F

    move-result v4

    div-float/2addr v4, p2

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000

    add-float/2addr v3, v4

    const/high16 v4, 0x47800000

    mul-float/2addr v3, v4

    float-to-int v3, v3

    if-eqz p8, :cond_2

    const/4 v4, 0x0

    const/high16 v5, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, Lmaps/al/c;->a(II)V

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, Lmaps/al/c;->a(II)V

    :goto_0
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/al/c;->a(II)V

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/al/c;->a(II)V

    if-eqz p9, :cond_3

    const/4 v3, 0x0

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v4}, Lmaps/al/c;->a(II)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v4}, Lmaps/al/c;->a(II)V

    :goto_1
    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v4, v1, 0x2

    add-int/lit8 v5, v1, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v1, v3, v4, v5}, Lmaps/al/b;->a(IIII)V

    add-int/lit8 v3, v1, 0x2

    add-int/lit8 v4, v1, 0x3

    add-int/lit8 v5, v1, 0x4

    add-int/lit8 v1, v1, 0x5

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v4, v5, v1}, Lmaps/al/b;->a(IIII)V

    return v2

    :cond_2
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/al/c;->a(II)V

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/al/c;->a(II)V

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/al/c;->a(II)V

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/al/c;->a(II)V

    goto :goto_1
.end method

.method private static b(I)[I
    .locals 2

    sget-object v0, Lmaps/s/q;->y:[[I

    aget-object v0, v0, p0

    if-nez v0, :cond_0

    sget-object v0, Lmaps/s/q;->y:[[I

    const/4 v1, 0x1

    shl-int/2addr v1, p0

    invoke-static {v1}, Lmaps/s/q;->a(I)[I

    move-result-object v1

    aput-object v1, v0, p0

    :cond_0
    sget-object v0, Lmaps/s/q;->y:[[I

    aget-object v0, v0, p0

    return-object v0
.end method


# virtual methods
.method public a(Lmaps/t/cg;FLmaps/t/bx;ILmaps/al/l;Lmaps/al/c;Lmaps/al/b;ZZB)I
    .locals 23

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    cmpg-float v4, p2, v4

    if-gez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lmaps/t/cg;->b()I

    move-result v11

    const/4 v4, 0x2

    if-ne v11, v4, :cond_2

    invoke-direct/range {p0 .. p10}, Lmaps/s/q;->b(Lmaps/t/cg;FLmaps/t/bx;ILmaps/al/l;Lmaps/al/c;Lmaps/al/b;ZZB)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_2
    const/4 v4, 0x2

    if-lt v11, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/s/q;->b:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/s/q;->c:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/s/q;->d:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/s/q;->e:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->f:Lmaps/t/bx;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->g:Lmaps/t/bx;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->h:Lmaps/t/bx;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/s/q;->i:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->j:Lmaps/t/bx;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/s/q;->k:Lmaps/t/bx;

    invoke-interface/range {p5 .. p5}, Lmaps/al/l;->c()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v12}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    const/4 v4, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v13}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v13, v12, v15}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v15, v0, v1}, Lmaps/t/cb;->a(Lmaps/t/bx;FLmaps/t/bx;)V

    move-object/from16 v0, v17

    invoke-static {v0, v6}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v12, v6, v12}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    invoke-static {v12, v6, v12}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    sget-object v4, Lmaps/s/q;->u:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Lmaps/al/c;->a([I)V

    const/4 v9, 0x4

    if-eqz p8, :cond_3

    add-int/lit8 v4, v3, 0x1

    add-int/lit8 v7, v3, 0x2

    add-int/lit8 v8, v3, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v4, v7, v8}, Lmaps/al/b;->a(IIII)V

    :goto_1
    add-int/lit8 v4, v3, 0x2

    add-int/lit8 v7, v3, 0x3

    add-int/lit8 v8, v3, 0x4

    add-int/lit8 v10, v3, 0x5

    move-object/from16 v0, p7

    invoke-interface {v0, v4, v7, v8, v10}, Lmaps/al/b;->a(IIII)V

    add-int/lit8 v8, v3, 0x4

    mul-float v20, p2, p2

    const/4 v3, 0x1

    move v10, v3

    :goto_2
    add-int/lit8 v3, v11, -0x1

    if-ge v10, v3, :cond_7

    add-int/lit8 v3, v10, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v14}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, v16

    invoke-static {v14, v13, v0}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, v16

    move/from16 v1, p2

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lmaps/t/cb;->a(Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-static/range {v15 .. v16}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;)J

    move-result-wide v3

    const-wide/16 v21, 0x0

    cmp-long v3, v3, v21

    if-lez v3, :cond_4

    const/4 v3, 0x1

    :goto_3
    const/4 v7, 0x1

    invoke-static/range {v17 .. v19}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static/range {v18 .. v19}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v4

    const/high16 v21, 0x3f800000

    cmpl-float v21, v4, v21

    if-lez v21, :cond_a

    invoke-static/range {v15 .. v16}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v21

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-ltz v21, :cond_a

    div-float v4, v20, v4

    move-object/from16 v0, v19

    move-object/from16 v1, v19

    invoke-static {v0, v4, v1}, Lmaps/t/bx;->a(Lmaps/t/bx;FLmaps/t/bx;)V

    move-object/from16 v0, v19

    invoke-static {v13, v0, v5}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, v19

    invoke-static {v13, v0, v6}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    if-eqz v3, :cond_5

    move-object v4, v5

    :goto_4
    invoke-static {v13, v12, v4}, Lmaps/t/bx;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v21

    const/high16 v22, 0x3f000000

    cmpg-float v21, v21, v22

    if-gez v21, :cond_a

    invoke-static {v13, v14, v4}, Lmaps/t/bx;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)F

    move-result v4

    const/high16 v21, 0x3f000000

    cmpg-float v4, v4, v21

    if-gez v4, :cond_a

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v6, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    add-int/lit8 v9, v9, 0x2

    sget-object v4, Lmaps/s/q;->w:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Lmaps/al/c;->a([I)V

    add-int/lit8 v4, v8, 0x1

    add-int/lit8 v7, v8, 0x2

    add-int/lit8 v21, v8, 0x3

    move-object/from16 v0, p7

    move/from16 v1, v21

    invoke-interface {v0, v8, v4, v7, v1}, Lmaps/al/b;->a(IIII)V

    add-int/lit8 v7, v8, 0x2

    const/4 v4, 0x0

    move v8, v4

    move v4, v7

    move v7, v9

    :goto_5
    if-eqz v8, :cond_9

    move-object/from16 v0, v17

    invoke-static {v13, v0, v5}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v17

    invoke-static {v13, v0, v5}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v13, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v18

    invoke-static {v13, v0, v5}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v18

    invoke-static {v13, v0, v5}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    sget-object v8, Lmaps/s/q;->x:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Lmaps/al/c;->a([I)V

    add-int/lit8 v7, v7, 0x5

    if-eqz v3, :cond_6

    add-int/lit8 v3, v4, 0x2

    add-int/lit8 v8, v4, 0x1

    add-int/lit8 v9, v4, 0x4

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v8, v9}, Lmaps/al/b;->a(III)V

    :goto_6
    add-int/lit8 v3, v4, 0x3

    add-int/lit8 v8, v4, 0x4

    add-int/lit8 v9, v4, 0x5

    add-int/lit8 v21, v4, 0x6

    move-object/from16 v0, p7

    move/from16 v1, v21

    invoke-interface {v0, v3, v8, v9, v1}, Lmaps/al/b;->a(IIII)V

    add-int/lit8 v3, v4, 0x5

    move v4, v7

    :goto_7
    invoke-virtual/range {v17 .. v18}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    invoke-virtual/range {v15 .. v16}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    invoke-virtual {v12, v13}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    invoke-virtual {v13, v14}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    add-int/lit8 v7, v10, 0x1

    move v10, v7

    move v8, v3

    move v9, v4

    goto/16 :goto_2

    :cond_3
    add-int/lit8 v4, v3, 0x2

    add-int/lit8 v7, v3, 0x2

    add-int/lit8 v8, v3, 0x2

    add-int/lit8 v10, v3, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v4, v7, v8, v10}, Lmaps/al/b;->a(IIII)V

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_5
    move-object v4, v6

    goto/16 :goto_4

    :cond_6
    add-int/lit8 v3, v4, 0x0

    add-int/lit8 v8, v4, 0x2

    add-int/lit8 v9, v4, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v8, v9}, Lmaps/al/b;->a(III)V

    goto :goto_6

    :cond_7
    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v18

    invoke-static {v0, v6}, Lmaps/t/cb;->a(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v14, v6, v14}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;IB)V

    sget-object v3, Lmaps/s/q;->v:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Lmaps/al/c;->a([I)V

    add-int/lit8 v3, v9, 0x4

    if-eqz p9, :cond_8

    add-int/lit8 v4, v8, 0x1

    add-int/lit8 v5, v8, 0x2

    add-int/lit8 v6, v8, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v8, v4, v5, v6}, Lmaps/al/b;->a(IIII)V

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p7

    invoke-interface {v0, v8, v8, v8, v8}, Lmaps/al/b;->a(IIII)V

    goto/16 :goto_0

    :cond_9
    move v3, v4

    move v4, v7

    goto/16 :goto_7

    :cond_a
    move v4, v8

    move v8, v7

    move v7, v9

    goto/16 :goto_5
.end method

.method public a(IZI[ILmaps/al/c;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    add-int/lit8 v3, p1, -0x1

    if-eqz p2, :cond_1

    const/4 v0, 0x5

    :goto_0
    mul-int/2addr v3, v0

    invoke-interface {p5}, Lmaps/al/c;->b()I

    move-result v4

    add-int/2addr v3, v4

    invoke-interface {p5, v3}, Lmaps/al/c;->c(I)V

    invoke-static {p3}, Lmaps/s/q;->b(I)[I

    move-result-object v3

    if-eqz p4, :cond_0

    array-length v4, p4

    if-ne v4, v2, :cond_3

    :cond_0
    if-nez p4, :cond_2

    :goto_1
    if-ge v2, p1, :cond_4

    mul-int/lit8 v4, v1, 0x5

    mul-int/lit8 v4, v4, 0x2

    mul-int/lit8 v5, v0, 0x2

    invoke-interface {p5, v3, v4, v5}, Lmaps/al/c;->a([III)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    aget v1, p4, v1

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_2
    if-ge v1, p1, :cond_4

    aget v2, p4, v1

    mul-int/lit8 v2, v2, 0x5

    mul-int/lit8 v2, v2, 0x2

    mul-int/lit8 v4, v0, 0x2

    invoke-interface {p5, v3, v2, v4}, Lmaps/al/c;->a([III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method public a(Lmaps/t/cg;FFLmaps/t/bx;IIILmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V
    .locals 35

    invoke-virtual/range {p1 .. p1}, Lmaps/t/cg;->b()I

    move-result v18

    const/4 v3, 0x1

    move/from16 v0, v18

    if-gt v0, v3, :cond_1

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v3, v18, -0x1

    invoke-interface/range {p8 .. p8}, Lmaps/al/l;->c()I

    move-result v8

    mul-int/lit8 v4, v18, 0x5

    invoke-virtual/range {p1 .. p1}, Lmaps/t/cg;->e()Z

    move-result v19

    add-int v5, v8, v4

    move-object/from16 v0, p8

    invoke-interface {v0, v5}, Lmaps/al/l;->d(I)V

    if-eqz p10, :cond_2

    invoke-interface/range {p10 .. p10}, Lmaps/al/c;->b()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p10

    invoke-interface {v0, v4}, Lmaps/al/c;->c(I)V

    :cond_2
    invoke-interface/range {p9 .. p9}, Lmaps/al/b;->a()I

    move-result v4

    mul-int/lit8 v3, v3, 0x3

    mul-int/lit8 v3, v3, 0x6

    add-int/2addr v3, v4

    move-object/from16 v0, p9

    invoke-interface {v0, v3}, Lmaps/al/b;->b(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->b:Lmaps/t/bx;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->c:Lmaps/t/bx;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->d:Lmaps/t/bx;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->e:Lmaps/t/bx;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->f:Lmaps/t/bx;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->g:Lmaps/t/bx;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->h:Lmaps/t/bx;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->l:Lmaps/t/cq;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->m:Lmaps/t/cq;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->n:Lmaps/t/cq;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->o:Lmaps/t/cq;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->p:Lmaps/t/cq;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/s/q;->q:Lmaps/t/cq;

    move-object/from16 v32, v0

    const/4 v6, -0x1

    const/4 v5, -0x1

    const/4 v7, -0x1

    const/4 v4, -0x1

    const/4 v10, -0x1

    add-float v3, p2, p3

    move/from16 v0, p6

    int-to-float v9, v0

    mul-float v9, v9, p3

    move/from16 v0, p7

    int-to-float v11, v0

    mul-float v11, v11, p2

    add-float/2addr v9, v11

    div-float v3, v9, v3

    float-to-int v0, v3

    move/from16 v33, v0

    const/4 v3, 0x0

    move v11, v3

    move v12, v10

    move v13, v4

    move v14, v7

    move v4, v8

    :goto_0
    move/from16 v0, v18

    if-ge v11, v0, :cond_0

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v21

    invoke-virtual {v0, v11, v1, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    add-int/lit8 v7, v11, -0x1

    add-int/lit8 v3, v11, 0x1

    if-eqz v19, :cond_14

    if-gez v7, :cond_3

    add-int/lit8 v7, v18, -0x2

    :cond_3
    move/from16 v0, v18

    if-lt v3, v0, :cond_14

    const/4 v3, 0x1

    move v8, v3

    move v3, v7

    :goto_1
    if-ltz v3, :cond_8

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v20

    invoke-virtual {v0, v3, v1, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/s/q;->r:Lmaps/t/cq;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/cq;

    move-result-object v3

    move-object v7, v3

    :goto_2
    move/from16 v0, v18

    if-ge v8, v0, :cond_9

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v22

    invoke-virtual {v0, v8, v1, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/s/q;->s:Lmaps/t/cq;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v3, v0, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/cq;

    move-result-object v3

    move-object v8, v3

    :goto_3
    const/4 v3, 0x1

    if-eqz v7, :cond_b

    if-eqz v8, :cond_b

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v9

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Lmaps/t/cq;->b(Lmaps/t/cq;)Lmaps/t/cq;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v9, v10}, Lmaps/t/cq;->b(FF)Z

    move-result v9

    if-eqz v9, :cond_a

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move/from16 v17, v3

    :goto_4
    if-eqz v17, :cond_d

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    move/from16 v0, p2

    neg-float v7, v0

    invoke-virtual {v3, v7}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, p8

    move-object/from16 v1, v23

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    if-eqz p10, :cond_4

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    :cond_4
    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v7, v3, 0x1

    move v8, v5

    move v9, v6

    move v10, v7

    move v7, v3

    move v5, v3

    move v6, v4

    move v3, v4

    :goto_5
    add-int/lit8 v15, v10, 0x1

    move-object/from16 v0, p8

    move-object/from16 v1, v21

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    if-eqz p10, :cond_5

    const/16 v16, 0x0

    move-object/from16 v0, p10

    move/from16 v1, v33

    move/from16 v2, v16

    invoke-interface {v0, v1, v2}, Lmaps/al/c;->a(II)V

    :cond_5
    if-eqz v19, :cond_13

    add-int/lit8 v16, v18, -0x1

    move/from16 v0, v16

    if-ne v11, v0, :cond_13

    const/16 v16, 0x1

    :goto_6
    if-nez v17, :cond_6

    if-nez v16, :cond_6

    move-object/from16 v0, p9

    invoke-interface {v0, v9, v10, v3}, Lmaps/al/b;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v10, v8, v3}, Lmaps/al/b;->a(III)V

    :cond_6
    if-lez v11, :cond_7

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v12, v10}, Lmaps/al/b;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v14, v10}, Lmaps/al/b;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v10, v6}, Lmaps/al/b;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v10, v14, v5}, Lmaps/al/b;->a(III)V

    :cond_7
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    move v12, v10

    move v13, v4

    move v14, v7

    move v5, v8

    move v6, v9

    move v4, v15

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    move-object v7, v3

    goto/16 :goto_2

    :cond_9
    const/4 v3, 0x0

    move-object v8, v3

    goto/16 :goto_3

    :cond_a
    const/4 v3, 0x0

    move/from16 v17, v3

    goto/16 :goto_4

    :cond_b
    if-eqz v7, :cond_c

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move/from16 v17, v3

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/cq;->a()Lmaps/t/cq;

    move/from16 v17, v3

    goto/16 :goto_4

    :cond_d
    invoke-virtual/range {v29 .. v29}, Lmaps/t/cq;->d()Lmaps/t/cq;

    invoke-virtual {v7, v8}, Lmaps/t/cq;->d(Lmaps/t/cq;)Z

    move-result v5

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->d()Lmaps/t/cq;

    invoke-virtual/range {v29 .. v30}, Lmaps/t/cq;->c(Lmaps/t/cq;)F

    move-result v6

    if-eqz v5, :cond_10

    move/from16 v3, p2

    :goto_7
    neg-float v9, v6

    div-float/2addr v3, v9

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v9

    invoke-virtual {v9, v3}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    invoke-virtual {v7}, Lmaps/t/cq;->c()F

    move-result v9

    invoke-virtual {v8}, Lmaps/t/cq;->c()F

    move-result v10

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v15

    invoke-virtual {v15}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-result-object v15

    move-object/from16 v0, v31

    invoke-virtual {v0, v15}, Lmaps/t/cq;->c(Lmaps/t/cq;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-result-object v16

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lmaps/t/cq;->c(Lmaps/t/cq;)F

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v16

    div-float/2addr v9, v15

    div-float v10, v10, v16

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    const/high16 v10, 0x3f800000

    cmpg-float v10, v9, v10

    if-gez v10, :cond_e

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v6, v9}, Ljava/lang/Math;->max(FF)F

    move-result v6

    mul-float/2addr v3, v6

    :cond_e
    if-eqz v5, :cond_11

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v5

    move/from16 v0, p3

    neg-float v6, v0

    invoke-virtual {v5, v6}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v5

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-static {v0, v5, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v5

    neg-float v3, v3

    invoke-virtual {v5, v3}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-result-object v3

    move/from16 v0, p3

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, p8

    move-object/from16 v1, v23

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v25

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v26

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    if-eqz p10, :cond_f

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    :cond_f
    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v5, v6, 0x1

    add-int/lit8 v7, v5, 0x1

    move v8, v5

    move v9, v6

    move v10, v7

    move v7, v6

    move v6, v3

    move/from16 v34, v3

    move v3, v4

    move/from16 v4, v34

    goto/16 :goto_5

    :cond_10
    move/from16 v3, p3

    goto/16 :goto_7

    :cond_11
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v5

    invoke-virtual {v5, v3}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    move/from16 v0, p2

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-result-object v3

    move/from16 v0, p2

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Lmaps/t/cq;->a(Lmaps/t/cq;)Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->e()Lmaps/t/cq;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cq;->d()Lmaps/t/cq;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lmaps/t/cq;->a(F)Lmaps/t/cq;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Lmaps/t/cq;->a(Lmaps/t/bx;Lmaps/t/cq;Lmaps/t/bx;)Lmaps/t/bx;

    move-object/from16 v0, p8

    move-object/from16 v1, v23

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v25

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v26

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    if-eqz p10, :cond_12

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/al/c;->a(II)V

    :cond_12
    add-int/lit8 v5, v4, 0x1

    add-int/lit8 v6, v5, 0x1

    add-int/lit8 v3, v6, 0x1

    add-int/lit8 v7, v3, 0x1

    move v8, v3

    move v9, v6

    move v10, v7

    move v7, v5

    move/from16 v34, v3

    move v3, v4

    move/from16 v4, v34

    goto/16 :goto_5

    :cond_13
    const/16 v16, 0x0

    goto/16 :goto_6

    :cond_14
    move v8, v3

    move v3, v7

    goto/16 :goto_1
.end method

.method public a(Lmaps/t/cg;FLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;Lmaps/al/c;)V
    .locals 16

    invoke-virtual/range {p1 .. p1}, Lmaps/t/cg;->b()I

    move-result v6

    add-int/lit8 v7, v6, -0x1

    invoke-interface/range {p6 .. p6}, Lmaps/al/l;->c()I

    move-result v8

    mul-int/lit8 v2, v7, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/s/q;->b:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/s/q;->c:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/s/q;->d:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/s/q;->e:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/s/q;->f:Lmaps/t/bx;

    invoke-interface/range {p6 .. p6}, Lmaps/al/l;->c()I

    move-result v3

    add-int/2addr v3, v2

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Lmaps/al/l;->d(I)V

    if-eqz p8, :cond_0

    invoke-interface/range {p8 .. p8}, Lmaps/al/c;->b()I

    move-result v3

    add-int/2addr v3, v2

    move-object/from16 v0, p8

    invoke-interface {v0, v3}, Lmaps/al/c;->c(I)V

    if-eqz p9, :cond_0

    invoke-interface/range {p9 .. p9}, Lmaps/al/c;->b()I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, p9

    invoke-interface {v0, v2}, Lmaps/al/c;->c(I)V

    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    move-object/from16 v0, p3

    invoke-static {v5, v0, v5}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    const/4 v3, 0x0

    const/4 v2, 0x1

    move v15, v2

    move v2, v3

    move v3, v15

    :goto_0
    if-ge v3, v6, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    move-object/from16 v0, p3

    invoke-static {v4, v0, v4}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v4, v5, v9}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move/from16 v0, p2

    neg-float v12, v0

    invoke-static {v9, v12, v10}, Lmaps/t/cb;->a(Lmaps/t/bx;FLmaps/t/bx;)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v5, v1}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    invoke-static {v5, v10, v11}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v11, v1}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    invoke-static {v4, v10, v11}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v11, v1}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v4, v1}, Lmaps/al/l;->a(Lmaps/t/bx;I)V

    if-eqz p8, :cond_2

    invoke-virtual {v9}, Lmaps/t/bx;->i()F

    move-result v12

    move/from16 v0, p4

    int-to-float v13, v0

    div-float/2addr v12, v13

    mul-float v12, v12, p5

    const/4 v13, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v2}, Lmaps/al/c;->a(FF)V

    const/high16 v13, 0x3f800000

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v2}, Lmaps/al/c;->a(FF)V

    if-eqz p9, :cond_1

    const/4 v13, 0x0

    sget v14, Lmaps/s/q;->a:F

    mul-float/2addr v14, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v14}, Lmaps/al/c;->a(FF)V

    sget v13, Lmaps/s/q;->a:F

    sget v14, Lmaps/s/q;->a:F

    mul-float/2addr v14, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v14}, Lmaps/al/c;->a(FF)V

    :cond_1
    add-float/2addr v2, v12

    const/high16 v12, 0x3f800000

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v2}, Lmaps/al/c;->a(FF)V

    const/4 v12, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v2}, Lmaps/al/c;->a(FF)V

    if-eqz p9, :cond_2

    sget v12, Lmaps/s/q;->a:F

    sget v13, Lmaps/s/q;->a:F

    mul-float/2addr v13, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v13}, Lmaps/al/c;->a(FF)V

    const/4 v12, 0x0

    sget v13, Lmaps/s/q;->a:F

    mul-float/2addr v13, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v13}, Lmaps/al/c;->a(FF)V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    move-object v15, v5

    move-object v5, v4

    move-object v4, v15

    goto/16 :goto_0

    :cond_3
    mul-int/lit8 v2, v7, 0x2

    add-int/lit8 v3, v7, -0x1

    invoke-interface/range {p7 .. p7}, Lmaps/al/b;->a()I

    move-result v6

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v2, v6

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Lmaps/al/b;->b(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/s/q;->d:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/s/q;->e:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/s/q;->f:Lmaps/t/bx;

    const/4 v2, 0x0

    :goto_1
    mul-int/lit8 v10, v2, 0x4

    add-int/2addr v10, v8

    const/4 v11, 0x0

    cmpl-float v11, p2, v11

    if-lez v11, :cond_4

    add-int/lit8 v11, v10, 0x1

    add-int/lit8 v12, v10, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/al/b;->a(III)V

    add-int/lit8 v11, v10, 0x2

    add-int/lit8 v12, v10, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/al/b;->a(III)V

    :goto_2
    add-int/lit8 v11, v7, -0x1

    if-ne v2, v11, :cond_5

    return-void

    :cond_4
    add-int/lit8 v11, v10, 0x2

    add-int/lit8 v12, v10, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/al/b;->a(III)V

    add-int/lit8 v11, v10, 0x3

    add-int/lit8 v12, v10, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/al/b;->a(III)V

    goto :goto_2

    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v11, v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v4}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v11, v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v3}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-static {v4, v5, v6}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v3, v4, v9}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v6, v9}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;)J

    move-result-wide v11

    long-to-float v11, v11

    mul-float v11, v11, p2

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_6

    add-int/lit8 v11, v10, 0x4

    const/4 v12, 0x0

    cmpl-float v12, p2, v12

    if-lez v12, :cond_7

    add-int/lit8 v12, v10, 0x3

    add-int/lit8 v10, v10, 0x2

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v12, v10, v11}, Lmaps/al/b;->a(III)V

    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_7
    add-int/lit8 v12, v10, 0x2

    add-int/lit8 v10, v10, 0x3

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v12, v10, v11}, Lmaps/al/b;->a(III)V

    goto :goto_3
.end method

.method public a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V
    .locals 17

    invoke-virtual/range {p1 .. p1}, Lmaps/t/cg;->b()I

    move-result v11

    add-int/lit8 v12, v11, -0x1

    invoke-interface/range {p7 .. p7}, Lmaps/al/l;->c()I

    move-result v13

    if-gtz v12, :cond_1

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_0

    const-string v1, "GeometryUtil"

    const-string v2, "Polyline has no segments."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_4

    const/4 v1, 0x5

    move v8, v1

    :goto_1
    mul-int v14, v8, v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/s/q;->b:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/s/q;->c:Lmaps/t/bx;

    invoke-interface/range {p7 .. p7}, Lmaps/al/l;->c()I

    move-result v1

    add-int/2addr v1, v14

    move-object/from16 v0, p7

    invoke-interface {v0, v1}, Lmaps/al/l;->d(I)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/s/q;->d:Lmaps/t/bx;

    if-eqz p9, :cond_2

    invoke-interface/range {p9 .. p9}, Lmaps/al/c;->b()I

    move-result v1

    add-int/2addr v1, v14

    move-object/from16 v0, p9

    invoke-interface {v0, v1}, Lmaps/al/c;->c(I)V

    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    move-object/from16 v0, p4

    invoke-static {v2, v0, v2}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    const/4 v9, 0x0

    const/4 v1, 0x1

    move v10, v1

    :goto_2
    if-ge v10, v11, :cond_5

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v3}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    move-object/from16 v0, p4

    invoke-static {v3, v0, v3}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    move-object/from16 v1, p0

    move/from16 v4, p2

    move/from16 v5, p5

    move/from16 v6, p3

    move-object/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lmaps/s/q;->a(Lmaps/t/bx;Lmaps/t/bx;FIZLmaps/al/l;)V

    if-eqz p9, :cond_f

    invoke-static {v3, v2, v15}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-virtual {v15}, Lmaps/t/bx;->i()F

    move-result v1

    move/from16 v0, p5

    int-to-float v4, v0

    div-float/2addr v1, v4

    mul-float v1, v1, p6

    const/4 v4, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v9}, Lmaps/al/c;->a(FF)V

    const/high16 v4, 0x3f800000

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v9}, Lmaps/al/c;->a(FF)V

    add-float/2addr v1, v9

    const/high16 v4, 0x3f800000

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v1}, Lmaps/al/c;->a(FF)V

    const/4 v4, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v1}, Lmaps/al/c;->a(FF)V

    if-eqz p3, :cond_3

    const/high16 v4, 0x3f000000

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v1}, Lmaps/al/c;->a(FF)V

    :cond_3
    :goto_3
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move v9, v1

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto :goto_2

    :cond_4
    const/4 v1, 0x4

    move v8, v1

    goto/16 :goto_1

    :cond_5
    if-eqz p8, :cond_0

    add-int v1, v13, v14

    const/16 v4, 0x7fff

    if-le v1, v4, :cond_6

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " required, but we can only store "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x7fff

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/s/q;->d:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/s/q;->e:Lmaps/t/bx;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/s/q;->f:Lmaps/t/bx;

    mul-int/lit8 v4, v12, 0x2

    invoke-virtual/range {p1 .. p1}, Lmaps/t/cg;->e()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x0

    :goto_4
    sub-int v1, v12, v1

    if-eqz p3, :cond_8

    invoke-interface/range {p8 .. p8}, Lmaps/al/b;->a()I

    move-result v9

    add-int/2addr v1, v4

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v1, v9

    move-object/from16 v0, p8

    invoke-interface {v0, v1}, Lmaps/al/b;->b(I)V

    :goto_5
    const/4 v1, 0x0

    :goto_6
    if-ge v1, v12, :cond_9

    mul-int v4, v1, v8

    add-int/2addr v4, v13

    add-int/lit8 v9, v4, 0x1

    add-int/lit8 v10, v4, 0x2

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v9, v10}, Lmaps/al/b;->a(III)V

    add-int/lit8 v9, v4, 0x2

    add-int/lit8 v10, v4, 0x3

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v9, v10}, Lmaps/al/b;->a(III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_7
    const/4 v1, 0x1

    goto :goto_4

    :cond_8
    invoke-interface/range {p8 .. p8}, Lmaps/al/b;->a()I

    move-result v1

    mul-int/lit8 v4, v4, 0x3

    add-int/2addr v1, v4

    move-object/from16 v0, p8

    invoke-interface {v0, v1}, Lmaps/al/b;->b(I)V

    goto :goto_5

    :cond_9
    if-eqz p3, :cond_0

    const/4 v1, 0x0

    move v4, v1

    :goto_7
    add-int/lit8 v1, v12, -0x1

    if-ge v4, v1, :cond_c

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v1, v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    add-int/lit8 v1, v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-static {v3, v2, v6}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v5, v3, v7}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v6, v7}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_a

    const/4 v1, 0x1

    :goto_8
    mul-int/lit8 v8, v4, 0x5

    add-int/2addr v8, v13

    add-int/lit8 v9, v8, 0x5

    if-eqz v1, :cond_b

    add-int/lit8 v1, v8, 0x2

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v9, v8}, Lmaps/al/b;->a(III)V

    :goto_9
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_7

    :cond_a
    const/4 v1, 0x0

    goto :goto_8

    :cond_b
    add-int/lit8 v1, v8, 0x3

    add-int/lit8 v8, v8, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v8, v9}, Lmaps/al/b;->a(III)V

    goto :goto_9

    :cond_c
    invoke-virtual/range {p1 .. p1}, Lmaps/t/cg;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v1, v12, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    invoke-static {v3, v2, v6}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v5, v3, v7}, Lmaps/t/cb;->d(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v6, v7}, Lmaps/t/cb;->c(Lmaps/t/bx;Lmaps/t/bx;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_d

    const/4 v1, 0x1

    :goto_a
    add-int/lit8 v2, v12, -0x1

    mul-int/lit8 v2, v2, 0x5

    add-int/2addr v2, v13

    if-eqz v1, :cond_e

    add-int/lit8 v1, v2, 0x2

    add-int/lit8 v3, v13, 0x1

    add-int/lit8 v2, v2, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v3, v2}, Lmaps/al/b;->a(III)V

    goto/16 :goto_0

    :cond_d
    const/4 v1, 0x0

    goto :goto_a

    :cond_e
    add-int/lit8 v1, v2, 0x3

    add-int/lit8 v2, v2, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v2, v13}, Lmaps/al/b;->a(III)V

    goto/16 :goto_0

    :cond_f
    move v1, v9

    goto/16 :goto_3
.end method
