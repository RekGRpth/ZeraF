.class Lmaps/s/f;
.super Ljava/lang/Object;


# instance fields
.field private a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/s/f;->a:[F

    return-void
.end method

.method static synthetic b(Lmaps/s/f;)[F
    .locals 1

    iget-object v0, p0, Lmaps/s/f;->a:[F

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/s/f;
    .locals 3

    const/high16 v2, 0x3f800000

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/4 v1, 0x0

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/4 v1, 0x5

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/16 v1, 0xa

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/16 v1, 0xf

    aput v2, v0, v1

    return-object p0
.end method

.method public a(FFF)V
    .locals 2

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    return-void
.end method

.method public a(FFFF)V
    .locals 6

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/4 v1, 0x0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    return-void
.end method

.method public a(Lmaps/s/f;)V
    .locals 2

    iget-object v0, p1, Lmaps/s/f;->a:[F

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lmaps/s/f;->b([FI)V

    return-void
.end method

.method public a([FI)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/s/f;->a:[F

    iget-object v4, p0, Lmaps/s/f;->a:[F

    move-object v2, p1

    move v3, p2

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    return-void
.end method

.method public b()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/s/f;->a:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lmaps/s/f;->a:[F

    const/4 v2, 0x0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(FFF)V
    .locals 2

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    return-void
.end method

.method public b([FI)V
    .locals 3

    iget-object v0, p0, Lmaps/s/f;->a:[F

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method
