.class final Lmaps/f/ce;
.super Lmaps/f/fs;


# instance fields
.field final transient a:Ljava/lang/Object;

.field final transient b:Ljava/lang/Object;

.field private transient c:Ljava/util/Map$Entry;

.field private transient d:Lmaps/f/bd;

.field private transient e:Lmaps/f/bd;

.field private transient f:Lmaps/f/ak;


# direct methods
.method constructor <init>(Ljava/util/Map$Entry;)V
    .locals 1

    invoke-direct {p0}, Lmaps/f/fs;-><init>()V

    iput-object p1, p0, Lmaps/f/ce;->c:Ljava/util/Map$Entry;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    return-void
.end method

.method private g()Ljava/util/Map$Entry;
    .locals 2

    iget-object v0, p0, Lmaps/f/ce;->c:Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    iget-object v1, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, Lmaps/f/cs;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/ce;->c:Ljava/util/Map$Entry;

    :cond_0
    return-object v0
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lmaps/f/bd;
    .locals 1

    iget-object v0, p0, Lmaps/f/ce;->d:Lmaps/f/bd;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/f/ce;->g()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/bd;->b(Ljava/lang/Object;)Lmaps/f/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/ce;->d:Lmaps/f/bd;

    :cond_0
    return-object v0
.end method

.method public c()Lmaps/f/bd;
    .locals 1

    iget-object v0, p0, Lmaps/f/ce;->e:Lmaps/f/bd;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    invoke-static {v0}, Lmaps/f/bd;->b(Ljava/lang/Object;)Lmaps/f/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/ce;->e:Lmaps/f/bd;

    :cond_0
    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()Lmaps/f/ak;
    .locals 2

    iget-object v0, p0, Lmaps/f/ce;->f:Lmaps/f/ak;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/f/ft;

    iget-object v1, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    invoke-direct {v0, v1}, Lmaps/f/ft;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/f/ce;->f:Lmaps/f/ak;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ce;->b()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    move v2, v1

    :cond_0
    :goto_0
    return v2

    :cond_1
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v3, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ce;->c()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/ce;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/ce;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ce;->d()Lmaps/f/ak;

    move-result-object v0

    return-object v0
.end method
