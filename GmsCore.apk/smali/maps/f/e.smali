.class final Lmaps/f/e;
.super Lmaps/f/am;


# instance fields
.field final transient a:[Ljava/lang/Object;

.field private final transient d:I

.field private final transient e:I


# direct methods
.method constructor <init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/f/am;-><init>([Ljava/lang/Object;)V

    iput-object p3, p0, Lmaps/f/e;->a:[Ljava/lang/Object;

    iput p4, p0, Lmaps/f/e;->d:I

    iput p2, p0, Lmaps/f/e;->e:I

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Lmaps/f/dk;->a(I)I

    move-result v0

    :goto_1
    iget-object v2, p0, Lmaps/f/e;->a:[Ljava/lang/Object;

    iget v3, p0, Lmaps/f/e;->d:I

    and-int/2addr v3, v0

    aget-object v2, v2, v3

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lmaps/f/e;->e:I

    return v0
.end method
