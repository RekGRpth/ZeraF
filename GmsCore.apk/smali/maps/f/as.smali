.class public abstract enum Lmaps/f/as;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/f/as;

.field public static final enum b:Lmaps/f/as;

.field public static final enum c:Lmaps/f/as;

.field public static final enum d:Lmaps/f/as;

.field public static final enum e:Lmaps/f/as;

.field private static final synthetic f:[Lmaps/f/as;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/f/dh;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1, v2}, Lmaps/f/dh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/as;->a:Lmaps/f/as;

    new-instance v0, Lmaps/f/di;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1, v3}, Lmaps/f/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/as;->b:Lmaps/f/as;

    new-instance v0, Lmaps/f/df;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1, v4}, Lmaps/f/df;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/as;->c:Lmaps/f/as;

    new-instance v0, Lmaps/f/dg;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1, v5}, Lmaps/f/dg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/as;->d:Lmaps/f/as;

    new-instance v0, Lmaps/f/dj;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1, v6}, Lmaps/f/dj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/as;->e:Lmaps/f/as;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/f/as;

    sget-object v1, Lmaps/f/as;->a:Lmaps/f/as;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/f/as;->b:Lmaps/f/as;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/f/as;->c:Lmaps/f/as;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/f/as;->d:Lmaps/f/as;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/f/as;->e:Lmaps/f/as;

    aput-object v1, v0, v6

    sput-object v0, Lmaps/f/as;->f:[Lmaps/f/as;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/f/q;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/f/as;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/f/as;
    .locals 1

    const-class v0, Lmaps/f/as;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/f/as;

    return-object v0
.end method

.method public static values()[Lmaps/f/as;
    .locals 1

    sget-object v0, Lmaps/f/as;->f:[Lmaps/f/as;

    invoke-virtual {v0}, [Lmaps/f/as;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/f/as;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
.end method
