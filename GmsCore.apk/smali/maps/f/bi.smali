.class public abstract Lmaps/f/bi;
.super Lmaps/f/ah;

# interfaces
.implements Ljava/util/SortedSet;
.implements Lmaps/f/fk;


# static fields
.field private static final c:Ljava/util/Comparator;

.field private static final d:Lmaps/f/bi;


# instance fields
.field final transient a:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lmaps/f/en;->b()Lmaps/f/en;

    move-result-object v0

    sput-object v0, Lmaps/f/bi;->c:Ljava/util/Comparator;

    new-instance v0, Lmaps/f/bo;

    sget-object v1, Lmaps/f/bi;->c:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lmaps/f/bo;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lmaps/f/bi;->d:Lmaps/f/bi;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0

    invoke-direct {p0}, Lmaps/f/ah;-><init>()V

    iput-object p1, p0, Lmaps/f/bi;->a:Ljava/util/Comparator;

    return-void
.end method

.method static a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    invoke-interface {p0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static a(Ljava/util/Comparator;)Lmaps/f/bi;
    .locals 1

    sget-object v0, Lmaps/f/bi;->c:Ljava/util/Comparator;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/f/bi;->f()Lmaps/f/bi;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/f/bo;

    invoke-direct {v0, p0}, Lmaps/f/bo;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lmaps/f/bi;
    .locals 2

    invoke-static {p0, p1}, Lmaps/f/co;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lmaps/f/bi;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmaps/f/bi;

    invoke-virtual {v0}, Lmaps/f/bi;->c()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lmaps/f/co;->b(Ljava/util/Comparator;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/ef;->a(Ljava/util/Collection;)Lmaps/f/ef;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/f/ef;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lmaps/f/bi;->a(Ljava/util/Comparator;)Lmaps/f/bi;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Lmaps/f/ec;

    invoke-direct {v0, v1, p0}, Lmaps/f/ec;-><init>(Lmaps/f/ef;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Ljava/util/Collection;)Lmaps/f/bi;
    .locals 1

    invoke-static {p0}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1}, Lmaps/f/bi;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method private static f()Lmaps/f/bi;
    .locals 1

    sget-object v0, Lmaps/f/bi;->d:Lmaps/f/bi;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lmaps/f/bi;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, p2}, Lmaps/f/bi;->a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;)Lmaps/f/bi;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmaps/f/bi;->a(Ljava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Object;Z)Lmaps/f/bi;
    .locals 1

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lmaps/f/bi;->c(Ljava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lmaps/f/bi;
    .locals 1

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/f/bi;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lmaps/f/bi;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/bi;
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lmaps/f/bi;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method b(Ljava/lang/Object;Z)Lmaps/f/bi;
    .locals 1

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lmaps/f/bi;->d(Ljava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method abstract b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lmaps/f/bi;
.end method

.method public c(Ljava/lang/Object;)Lmaps/f/bi;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lmaps/f/bi;->b(Ljava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method abstract c(Ljava/lang/Object;Z)Lmaps/f/bi;
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lmaps/f/bi;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method abstract d(Ljava/lang/Object;)I
.end method

.method abstract d(Ljava/lang/Object;Z)Lmaps/f/bi;
.end method

.method public bridge synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/f/bi;->a(Ljava/lang/Object;)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method public abstract i_()Lmaps/f/fr;
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/bi;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lmaps/f/bi;->b(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/f/bi;->c(Ljava/lang/Object;)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method
