.class abstract Lmaps/f/br;
.super Lmaps/f/bd;


# instance fields
.field final c:[Ljava/lang/Object;

.field final d:I


# direct methods
.method constructor <init>([Ljava/lang/Object;I)V
    .locals 0

    invoke-direct {p0}, Lmaps/f/bd;-><init>()V

    iput-object p1, p0, Lmaps/f/br;->c:[Ljava/lang/Object;

    iput p2, p0, Lmaps/f/br;->d:I

    return-void
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final hashCode()I
    .locals 1

    iget v0, p0, Lmaps/f/br;->d:I

    return v0
.end method

.method public i_()Lmaps/f/fr;
    .locals 2

    new-instance v0, Lmaps/f/bl;

    iget-object v1, p0, Lmaps/f/br;->c:[Ljava/lang/Object;

    array-length v1, v1

    invoke-direct {v0, p0, v1}, Lmaps/f/bl;-><init>(Lmaps/f/br;I)V

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/br;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmaps/f/br;->c:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/br;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmaps/f/br;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lmaps/f/br;->size()I

    move-result v0

    array-length v1, p1

    if-ge v1, v0, :cond_1

    invoke-static {p1, v0}, Lmaps/f/bb;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lmaps/f/br;->c:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lmaps/f/br;->c:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lmaps/f/br;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    array-length v1, p1

    if-le v1, v0, :cond_0

    const/4 v1, 0x0

    aput-object v1, p1, v0

    goto :goto_0

    :cond_2
    return-object p1
.end method
