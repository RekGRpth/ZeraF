.class abstract enum Lmaps/f/cx;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/f/cx;

.field public static final enum b:Lmaps/f/cx;

.field public static final enum c:Lmaps/f/cx;

.field private static final synthetic d:[Lmaps/f/cx;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/f/aa;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, Lmaps/f/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/cx;->a:Lmaps/f/cx;

    new-instance v0, Lmaps/f/ab;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, Lmaps/f/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/cx;->b:Lmaps/f/cx;

    new-instance v0, Lmaps/f/ac;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, Lmaps/f/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/cx;->c:Lmaps/f/cx;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/f/cx;

    sget-object v1, Lmaps/f/cx;->a:Lmaps/f/cx;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/f/cx;->b:Lmaps/f/cx;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/f/cx;->c:Lmaps/f/cx;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/f/cx;->d:[Lmaps/f/cx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/f/ep;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/f/cx;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/f/cx;
    .locals 1

    const-class v0, Lmaps/f/cx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/f/cx;

    return-object v0
.end method

.method public static values()[Lmaps/f/cx;
    .locals 1

    sget-object v0, Lmaps/f/cx;->d:[Lmaps/f/cx;

    invoke-virtual {v0}, [Lmaps/f/cx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/f/cx;

    return-object v0
.end method


# virtual methods
.method abstract a()Lmaps/ap/a;
.end method

.method abstract a(Lmaps/f/bm;Lmaps/f/fm;Ljava/lang/Object;)Lmaps/f/cm;
.end method
