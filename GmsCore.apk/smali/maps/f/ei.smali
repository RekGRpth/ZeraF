.class final enum Lmaps/f/ei;
.super Ljava/lang/Enum;

# interfaces
.implements Lmaps/f/fm;


# static fields
.field public static final enum a:Lmaps/f/ei;

.field private static final synthetic b:[Lmaps/f/ei;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lmaps/f/ei;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lmaps/f/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/ei;->a:Lmaps/f/ei;

    const/4 v0, 0x1

    new-array v0, v0, [Lmaps/f/ei;

    sget-object v1, Lmaps/f/ei;->a:Lmaps/f/ei;

    aput-object v1, v0, v2

    sput-object v0, Lmaps/f/ei;->b:[Lmaps/f/ei;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/f/ei;
    .locals 1

    const-class v0, Lmaps/f/ei;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/f/ei;

    return-object v0
.end method

.method public static values()[Lmaps/f/ei;
    .locals 1

    sget-object v0, Lmaps/f/ei;->b:[Lmaps/f/ei;

    invoke-virtual {v0}, [Lmaps/f/ei;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/f/ei;

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/f/cm;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/f/cm;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/f/fm;)V
    .locals 0

    return-void
.end method

.method public b()Lmaps/f/fm;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lmaps/f/fm;)V
    .locals 0

    return-void
.end method

.method public c()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lmaps/f/fm;)V
    .locals 0

    return-void
.end method

.method public d()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d(Lmaps/f/fm;)V
    .locals 0

    return-void
.end method

.method public e()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public f()Lmaps/f/fm;
    .locals 0

    return-object p0
.end method

.method public g()Lmaps/f/fm;
    .locals 0

    return-object p0
.end method

.method public h()Lmaps/f/fm;
    .locals 0

    return-object p0
.end method

.method public i()Lmaps/f/fm;
    .locals 0

    return-object p0
.end method
