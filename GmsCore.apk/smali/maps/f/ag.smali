.class final Lmaps/f/ag;
.super Lmaps/f/fs;


# static fields
.field static final a:Lmaps/f/ag;

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/f/ag;

    invoke-direct {v0}, Lmaps/f/ag;-><init>()V

    sput-object v0, Lmaps/f/ag;->a:Lmaps/f/ag;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmaps/f/fs;-><init>()V

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lmaps/f/bd;
    .locals 1

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public c()Lmaps/f/bd;
    .locals 1

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Lmaps/f/ak;
    .locals 1

    sget-object v0, Lmaps/f/ak;->b:Lmaps/f/ak;

    return-object v0
.end method

.method public bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ag;->b()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ag;->c()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "{}"

    return-object v0
.end method

.method public bridge synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ag;->d()Lmaps/f/ak;

    move-result-object v0

    return-object v0
.end method
