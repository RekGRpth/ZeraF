.class public Lmaps/f/ax;
.super Lmaps/f/dz;

# interfaces
.implements Lmaps/f/bc;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final transient a:Lmaps/f/bi;


# direct methods
.method constructor <init>(Lmaps/f/fs;ILjava/util/Comparator;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/f/dz;-><init>(Lmaps/f/fs;I)V

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lmaps/f/ax;->a:Lmaps/f/bi;

    return-void

    :cond_0
    invoke-static {p3}, Lmaps/f/bi;->a(Ljava/util/Comparator;)Lmaps/f/bi;

    move-result-object v0

    goto :goto_0
.end method

.method public static a()Lmaps/f/ax;
    .locals 1

    sget-object v0, Lmaps/f/h;->a:Lmaps/f/h;

    return-object v0
.end method

.method static synthetic a(Lmaps/f/at;Ljava/util/Comparator;)Lmaps/f/ax;
    .locals 1

    invoke-static {p0, p1}, Lmaps/f/ax;->b(Lmaps/f/at;Ljava/util/Comparator;)Lmaps/f/ax;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lmaps/f/at;Ljava/util/Comparator;)Lmaps/f/ax;
    .locals 6

    invoke-static {p0}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lmaps/f/at;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    invoke-static {}, Lmaps/f/ax;->a()Lmaps/f/ax;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v0, p0, Lmaps/f/ax;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lmaps/f/ax;

    invoke-virtual {v0}, Lmaps/f/ax;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-static {}, Lmaps/f/fs;->f()Lmaps/f/av;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {p0}, Lmaps/f/at;->e()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez p1, :cond_3

    invoke-static {v0}, Lmaps/f/bd;->a(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lmaps/f/bd;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v2, v4, v0}, Lmaps/f/av;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/av;

    invoke-virtual {v0}, Lmaps/f/bd;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_3
    move v1, v0

    goto :goto_1

    :cond_3
    invoke-static {p1, v0}, Lmaps/f/bi;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lmaps/f/bi;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v0, Lmaps/f/ax;

    invoke-virtual {v2}, Lmaps/f/av;->a()Lmaps/f/fs;

    move-result-object v2

    invoke-direct {v0, v2, v1, p1}, Lmaps/f/ax;-><init>(Lmaps/f/fs;ILjava/util/Comparator;)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_3
.end method

.method public static b()Lmaps/f/bz;
    .locals 1

    new-instance v0, Lmaps/f/bz;

    invoke-direct {v0}, Lmaps/f/bz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/f/ax;->b(Ljava/lang/Object;)Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Lmaps/f/bd;
    .locals 1

    iget-object v0, p0, Lmaps/f/ax;->b:Lmaps/f/fs;

    invoke-virtual {v0, p1}, Lmaps/f/fs;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/bd;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/f/ax;->a:Lmaps/f/bi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/f/ax;->a:Lmaps/f/bi;

    goto :goto_0

    :cond_1
    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic c(Ljava/lang/Object;)Lmaps/f/ak;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/f/ax;->b(Ljava/lang/Object;)Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method
