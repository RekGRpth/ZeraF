.class public abstract Lmaps/f/ef;
.super Lmaps/f/ak;

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmaps/f/ak;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Object;)Lmaps/f/ef;
    .locals 1

    new-instance v0, Lmaps/f/dc;

    invoke-direct {v0, p0}, Lmaps/f/dc;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Lmaps/f/ef;
    .locals 2

    instance-of v0, p0, Lmaps/f/ak;

    if-eqz v0, :cond_1

    check-cast p0, Lmaps/f/ak;

    invoke-virtual {p0}, Lmaps/f/ak;->d()Lmaps/f/ef;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/f/ef;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lmaps/f/ef;->b(Ljava/util/Collection;)Lmaps/f/ef;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lmaps/f/ef;->b(Ljava/util/Collection;)Lmaps/f/ef;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;)Lmaps/f/ef;
    .locals 2

    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lmaps/f/ef;->b([Ljava/lang/Object;)Lmaps/f/ef;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Lmaps/f/dc;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Lmaps/f/dc;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Ljava/util/Collection;)Lmaps/f/ef;
    .locals 3

    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v1

    array-length v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {v1}, Lmaps/f/ef;->b([Ljava/lang/Object;)Lmaps/f/ef;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Lmaps/f/dc;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lmaps/f/dc;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static varargs b([Ljava/lang/Object;)Lmaps/f/ef;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-object v1, p0, v0

    invoke-static {v1, v0}, Lmaps/f/ef;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/f/ap;

    invoke-direct {v0, p0}, Lmaps/f/ap;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method public static f()Lmaps/f/ef;
    .locals 1

    sget-object v0, Lmaps/f/fx;->a:Lmaps/f/fx;

    return-object v0
.end method

.method public static g()Lmaps/f/bk;
    .locals 1

    new-instance v0, Lmaps/f/bk;

    invoke-direct {v0}, Lmaps/f/bk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public abstract a(II)Lmaps/f/ef;
.end method

.method public abstract a(I)Lmaps/f/et;
.end method

.method public final add(ILjava/lang/Object;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lmaps/f/et;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/f/ef;->a(I)Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method public d()Lmaps/f/ef;
    .locals 0

    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {p0, p1}, Lmaps/f/fd;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-static {p0}, Lmaps/f/fd;->b(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method public i_()Lmaps/f/fr;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ef;->b()Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ef;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ef;->b()Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/f/ef;->a(I)Lmaps/f/et;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic subList(II)Ljava/util/List;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lmaps/f/ef;->a(II)Lmaps/f/ef;

    move-result-object v0

    return-object v0
.end method
