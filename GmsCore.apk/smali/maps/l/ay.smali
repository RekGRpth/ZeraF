.class public Lmaps/l/ay;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;
.implements Lmaps/y/bh;


# static fields
.field private static E:[F

.field private static F:Lmaps/t/bx;


# instance fields
.field private final A:I

.field private B:Z

.field private C:Lmaps/t/u;

.field private D:Lmaps/l/ab;

.field private G:Lmaps/w/u;

.field private H:Lmaps/q/ac;

.field private I:Lmaps/q/ac;

.field protected final a:Landroid/graphics/Bitmap;

.field protected final b:I

.field protected final c:I

.field private d:Lmaps/t/bx;

.field private final e:Landroid/graphics/Bitmap;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Lmaps/y/k;

.field private i:Lmaps/cr/a;

.field private j:Lmaps/cr/a;

.field private k:Lmaps/al/q;

.field private l:Lmaps/al/q;

.field private m:F

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private final t:Z

.field private final u:F

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, Lmaps/l/ay;->E:[F

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    sput-object v0, Lmaps/l/ay;->F:Lmaps/t/bx;

    return-void
.end method

.method public constructor <init>(Lmaps/t/bx;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/l/ay;->s:Z

    iput-object p1, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    iput-object p2, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    iput p4, p0, Lmaps/l/ay;->b:I

    iput p5, p0, Lmaps/l/ay;->c:I

    iput-object p6, p0, Lmaps/l/ay;->f:Ljava/lang/String;

    iput-object p7, p0, Lmaps/l/ay;->g:Ljava/lang/String;

    iput-boolean v0, p0, Lmaps/l/ay;->q:Z

    iput-boolean p8, p0, Lmaps/l/ay;->t:Z

    iget-boolean v0, p0, Lmaps/l/ay;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->p()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->l()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x43700000

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/l/ay;->u:F

    :goto_0
    iget v0, p0, Lmaps/l/ay;->b:I

    invoke-virtual {p0, v0}, Lmaps/l/ay;->a(I)I

    move-result v0

    iput v0, p0, Lmaps/l/ay;->v:I

    iget v0, p0, Lmaps/l/ay;->c:I

    invoke-virtual {p0, v0}, Lmaps/l/ay;->a(I)I

    move-result v0

    iput v0, p0, Lmaps/l/ay;->w:I

    iget-object v0, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/l/ay;->a(I)I

    move-result v0

    iput v0, p0, Lmaps/l/ay;->x:I

    iget-object v0, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/l/ay;->a(I)I

    move-result v0

    iput v0, p0, Lmaps/l/ay;->y:I

    :goto_1
    iget-object v0, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/l/ay;->a(I)I

    move-result v0

    iput v0, p0, Lmaps/l/ay;->z:I

    iget-object v0, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/l/ay;->a(I)I

    move-result v0

    iput v0, p0, Lmaps/l/ay;->A:I

    :goto_2
    return-void

    :cond_0
    const/high16 v0, 0x3f800000

    iput v0, p0, Lmaps/l/ay;->u:F

    goto :goto_0

    :cond_1
    iput v2, p0, Lmaps/l/ay;->x:I

    iput v2, p0, Lmaps/l/ay;->y:I

    goto :goto_1

    :cond_2
    iput v2, p0, Lmaps/l/ay;->z:I

    iput v2, p0, Lmaps/l/ay;->A:I

    goto :goto_2
.end method

.method private a(Lmaps/cr/a;)Lmaps/al/q;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lmaps/al/q;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lmaps/al/q;-><init>(I)V

    invoke-virtual {p1}, Lmaps/cr/a;->b()F

    move-result v1

    invoke-virtual {p1}, Lmaps/cr/a;->c()F

    move-result v2

    invoke-virtual {v0, v3, v3}, Lmaps/al/q;->a(FF)V

    invoke-virtual {v0, v3, v2}, Lmaps/al/q;->a(FF)V

    invoke-virtual {v0, v1, v3}, Lmaps/al/q;->a(FF)V

    invoke-virtual {v0, v1, v2}, Lmaps/al/q;->a(FF)V

    return-object v0
.end method

.method private a(Lmaps/cr/c;Lmaps/q/ac;II)V
    .locals 4

    const/high16 v2, 0x10000

    iget-object v0, p0, Lmaps/l/ay;->G:Lmaps/w/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ay;->G:Lmaps/w/u;

    invoke-virtual {v0, p1}, Lmaps/w/u;->a(Lmaps/cr/c;)I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/l/ay;->G:Lmaps/w/u;

    :cond_0
    mul-int v1, p3, v0

    div-int/2addr v1, v2

    int-to-float v1, v1

    mul-int/2addr v0, p4

    div-int/2addr v0, v2

    int-to-float v0, v0

    :goto_0
    iget-object v2, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    const/high16 v3, 0x3f800000

    invoke-virtual {p2, v2, v1, v0, v3}, Lmaps/q/ac;->a(Lmaps/t/bx;FFF)V

    return-void

    :cond_1
    int-to-float v1, p3

    int-to-float v0, p4

    goto :goto_0
.end method

.method private b(Lmaps/cr/c;Landroid/graphics/Bitmap;)Lmaps/cr/a;
    .locals 2

    iget-object v0, p0, Lmaps/l/ay;->h:Lmaps/y/k;

    invoke-virtual {v0}, Lmaps/y/k;->h()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/cr/a;->f()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->c(Z)V

    invoke-virtual {v0, p2}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lmaps/l/ay;->h:Lmaps/y/k;

    invoke-virtual {v1}, Lmaps/y/k;->h()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected a(I)I
    .locals 2

    iget-boolean v0, p0, Lmaps/l/ay;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->p()Z

    move-result v0

    if-nez v0, :cond_0

    int-to-float v0, p1

    iget v1, p0, Lmaps/l/ay;->u:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p1

    :cond_0
    return p1
.end method

.method public a(Lmaps/l/ay;)I
    .locals 2

    iget v0, p0, Lmaps/l/ay;->n:I

    iget v1, p1, Lmaps/l/ay;->n:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/l/ay;->n:I

    iget v1, p1, Lmaps/l/ay;->n:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Landroid/graphics/Bitmap;)Lmaps/cr/a;
    .locals 2

    new-instance v0, Lmaps/cr/a;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;Z)V

    invoke-virtual {v0, p2}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/high16 v6, 0x3f800000

    const/4 v5, 0x0

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/l/ay;->v:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/l/ay;->x:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/l/ay;->w:I

    int-to-float v1, v1

    iget v2, p0, Lmaps/l/ay;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/16 v2, 0x14

    new-array v2, v2, [F

    const/4 v3, 0x0

    neg-float v4, v0

    aput v4, v2, v3

    aput v1, v2, v7

    aput v5, v2, v8

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v9

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    new-instance v0, Lmaps/q/ac;

    sget-object v1, Lmaps/y/am;->w:Lmaps/y/am;

    invoke-direct {v0, v1}, Lmaps/q/ac;-><init>(Lmaps/y/am;)V

    iput-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Marker for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lmaps/l/ay;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, v0}, Lmaps/l/ay;->a(Lmaps/cr/c;Landroid/graphics/Bitmap;)Lmaps/cr/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    :cond_0
    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    iget-object v1, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    new-instance v1, Lmaps/q/n;

    const/16 v3, 0x303

    invoke-direct {v1, v7, v3}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    new-array v0, v9, [F

    fill-array-data v0, :array_0

    new-instance v1, Lmaps/q/ar;

    invoke-direct {v1, v8, v0}, Lmaps/q/ar;-><init>(I[F)V

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    new-instance v1, Lmaps/q/ao;

    const/16 v3, 0x9

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, Lmaps/q/ao;-><init>([FII)V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/r;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    :cond_1
    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget v0, p0, Lmaps/l/ay;->v:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/l/ay;->z:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/l/ay;->w:I

    int-to-float v1, v1

    iget v2, p0, Lmaps/l/ay;->A:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/16 v2, 0x14

    new-array v2, v2, [F

    const/4 v3, 0x0

    neg-float v4, v0

    aput v4, v2, v3

    aput v1, v2, v7

    aput v5, v2, v8

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v9

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    new-instance v0, Lmaps/q/ac;

    sget-object v1, Lmaps/y/am;->v:Lmaps/y/am;

    invoke-direct {v0, v1}, Lmaps/q/ac;-><init>(Lmaps/y/am;)V

    iput-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Marker shadow for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lmaps/l/ay;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, v0}, Lmaps/l/ay;->a(Lmaps/cr/c;Landroid/graphics/Bitmap;)Lmaps/cr/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    :cond_2
    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    iget-object v1, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    new-instance v1, Lmaps/q/n;

    const/16 v3, 0x303

    invoke-direct {v1, v7, v3}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    new-array v0, v9, [F

    fill-array-data v0, :array_1

    new-instance v1, Lmaps/q/ar;

    invoke-direct {v1, v8, v0}, Lmaps/q/ar;-><init>(I[F)V

    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    new-instance v1, Lmaps/q/ao;

    const/16 v3, 0x9

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, Lmaps/q/ao;-><init>([FII)V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/r;)V

    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    const/high16 v1, -0x3dcc0000

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(F)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    :cond_3
    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 10

    const/16 v9, 0x303

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/high16 v8, 0x10000

    const/4 v7, 0x0

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-gt v0, v6, :cond_0

    iget-object v0, p0, Lmaps/l/ay;->h:Lmaps/y/k;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/l/ay;->m:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/l/ay;->s:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    iget-object v2, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1, v2}, Lmaps/l/ay;->b(Lmaps/cr/c;Landroid/graphics/Bitmap;)Lmaps/cr/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    invoke-direct {p0, v0}, Lmaps/l/ay;->a(Lmaps/cr/a;)Lmaps/al/q;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ay;->l:Lmaps/al/q;

    :cond_2
    :goto_1
    sget-object v0, Lmaps/l/ay;->F:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    invoke-virtual {v0, v1}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    invoke-virtual {p2}, Lmaps/bq/d;->k()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lmaps/l/ay;->F:Lmaps/t/bx;

    sget-object v1, Lmaps/l/ay;->E:[F

    invoke-virtual {p2, v0, v1}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    sget-object v0, Lmaps/l/ay;->E:[F

    aget v0, v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    sget-object v1, Lmaps/l/ay;->E:[F

    aget v1, v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v0

    sput-object v0, Lmaps/l/ay;->F:Lmaps/t/bx;

    :cond_3
    iget-object v0, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    if-nez v0, :cond_5

    const-string v0, "UI"

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Null point for ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lmaps/l/ay;->E:[F

    aget v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lmaps/l/ay;->E:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "); "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "camera="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    if-nez v0, :cond_2

    invoke-direct {p0, p1, v3}, Lmaps/l/ay;->b(Lmaps/cr/c;Landroid/graphics/Bitmap;)Lmaps/cr/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    invoke-direct {p0, v0}, Lmaps/l/ay;->a(Lmaps/cr/a;)Lmaps/al/q;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ay;->k:Lmaps/al/q;

    goto/16 :goto_1

    :cond_5
    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    monitor-enter p0

    :try_start_0
    sget-object v0, Lmaps/l/ay;->F:Lmaps/t/bx;

    iget v1, p0, Lmaps/l/ay;->m:F

    invoke-static {p1, p2, v0, v1}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-nez v0, :cond_7

    const/high16 v0, -0x2d0000

    invoke-interface {v4, v0, v7, v7, v8}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    const/high16 v0, -0x5a0000

    invoke-interface {v4, v0, v8, v7, v7}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    iget-object v0, p0, Lmaps/l/ay;->l:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    invoke-virtual {v0, v4}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget v1, p0, Lmaps/l/ay;->z:I

    iget v0, p0, Lmaps/l/ay;->A:I

    :goto_2
    iget-object v3, p0, Lmaps/l/ay;->G:Lmaps/w/u;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lmaps/l/ay;->G:Lmaps/w/u;

    invoke-virtual {v3, p1}, Lmaps/w/u;->a(Lmaps/cr/c;)I

    move-result v3

    if-ne v3, v8, :cond_6

    const/4 v5, 0x0

    iput-object v5, p0, Lmaps/l/ay;->G:Lmaps/w/u;

    :cond_6
    mul-int/2addr v1, v3

    mul-int/2addr v0, v3

    :goto_3
    iget v3, p0, Lmaps/l/ay;->b:I

    shl-int/lit8 v3, v3, 0x10

    neg-int v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v3, v5

    const/high16 v5, -0x10000

    iget v6, p0, Lmaps/l/ay;->c:I

    shl-int/lit8 v6, v6, 0x10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int v2, v6, v2

    add-int/2addr v2, v5

    invoke-interface {v4, v1, v8, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    invoke-interface {v4, v3, v7, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatex(III)V

    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {v4, v0, v7, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    invoke-static {v4, p2}, Lmaps/af/d;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/l/ay;->k:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    invoke-virtual {v0, v4}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget v1, p0, Lmaps/l/ay;->x:I

    iget v0, p0, Lmaps/l/ay;->y:I

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v2

    if-ne v2, v6, :cond_8

    const/16 v2, 0x302

    invoke-interface {v4, v2, v9}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v2, 0x2300

    const/16 v5, 0x2200

    const/16 v6, 0x2100

    invoke-interface {v4, v2, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const v2, 0xcccc

    invoke-interface {v4, v8, v8, v8, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object v2, v3

    goto :goto_2

    :cond_8
    invoke-interface {v4, v5, v9}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    move-object v2, v3

    goto :goto_2

    :cond_9
    shl-int/lit8 v1, v1, 0x10

    shl-int/lit8 v0, v0, 0x10

    goto :goto_3
.end method

.method public declared-synchronized a(Lmaps/t/bx;)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iput-object p1, p0, Lmaps/l/ay;->d:Lmaps/t/bx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/y/k;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/ay;->h:Lmaps/y/k;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/l/ay;->r:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/l/ay;->r:Z

    return v0
.end method

.method public declared-synchronized a(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    :cond_1
    :try_start_1
    iget-object v2, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    invoke-virtual {p4, v2}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, Lmaps/l/ay;->v:I

    sub-int/2addr v3, v4

    iget v4, p0, Lmaps/l/ay;->x:I

    add-int/2addr v4, v3

    const/high16 v5, 0x41400000

    invoke-virtual {p4}, Lmaps/bq/d;->n()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_0

    add-int v3, v4, v5

    int-to-float v3, v3

    cmpl-float v3, p1, v3

    if-gtz v3, :cond_0

    const/4 v3, 0x1

    aget v2, v2, v3

    iget v3, p0, Lmaps/l/ay;->w:I

    sub-int/2addr v2, v3

    iget v3, p0, Lmaps/l/ay;->y:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/2addr v3, v2

    sub-int/2addr v2, v5

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_2

    add-int v2, v3, v5

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lmaps/bq/d;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/l/ay;->s:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v2

    iget-object v3, p0, Lmaps/l/ay;->C:Lmaps/t/u;

    invoke-virtual {v2, v3}, Lmaps/t/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v0, p0, Lmaps/l/ay;->B:Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    invoke-virtual {p1, v2}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, Lmaps/l/ay;->v:I

    sub-int v4, v3, v4

    iget v3, p0, Lmaps/l/ay;->x:I

    add-int/2addr v3, v4

    const/4 v5, 0x1

    aget v2, v2, v5

    iget v5, p0, Lmaps/l/ay;->w:I

    sub-int v5, v2, v5

    iget v2, p0, Lmaps/l/ay;->y:I

    add-int/2addr v2, v5

    iget-object v6, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_3

    iget v6, p0, Lmaps/l/ay;->z:I

    add-int/2addr v6, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v6, p0, Lmaps/l/ay;->A:I

    add-int/2addr v6, v5

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_3
    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v6

    if-ge v4, v6, :cond_4

    if-ltz v3, :cond_4

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v3

    if-ge v5, v3, :cond_4

    if-ltz v2, :cond_4

    move v0, v1

    :cond_4
    iput-boolean v0, p0, Lmaps/l/ay;->B:Z

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ay;->C:Lmaps/t/u;

    iget-boolean v0, p0, Lmaps/l/ay;->B:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    invoke-virtual {v0, p1}, Lmaps/q/ac;->b(I)V

    :cond_0
    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    invoke-virtual {v0, p1}, Lmaps/q/ac;->b(I)V

    :cond_1
    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 3

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    iget v1, p0, Lmaps/l/ay;->x:I

    iget v2, p0, Lmaps/l/ay;->y:I

    invoke-direct {p0, p1, v0, v1, v2}, Lmaps/l/ay;->a(Lmaps/cr/c;Lmaps/q/ac;II)V

    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    iget v1, p0, Lmaps/l/ay;->z:I

    iget v2, p0, Lmaps/l/ay;->A:I

    invoke-direct {p0, p1, v0, v1, v2}, Lmaps/l/ay;->a(Lmaps/cr/c;Lmaps/q/ac;II)V

    return-void
.end method

.method public declared-synchronized b(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/l/ay;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/l/ay;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/bq/d;)Z
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/l/ay;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    const/high16 v0, 0x3f800000

    :try_start_1
    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmaps/bq/d;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/l/ay;->m:F

    iget-object v0, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    sget-object v1, Lmaps/l/ay;->E:[F

    invoke-virtual {p1, v0, v1}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    sget-object v0, Lmaps/l/ay;->E:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    const/high16 v1, 0x47800000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lmaps/l/ay;->n:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(FFLmaps/t/bx;Lmaps/bq/d;)I
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/l/ay;->d:Lmaps/t/bx;

    invoke-virtual {p4, v0}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v1, v0, v1

    iget v2, p0, Lmaps/l/ay;->v:I

    sub-int/2addr v1, v2

    iget v2, p0, Lmaps/l/ay;->x:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v2, 0x1

    aget v0, v0, v2

    iget v2, p0, Lmaps/l/ay;->w:I

    sub-int/2addr v0, v2

    iget v2, p0, Lmaps/l/ay;->y:I

    div-int/lit8 v2, v2, 0x2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v0, v2

    int-to-float v2, v1

    sub-float v2, p1, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    mul-float/2addr v1, v2

    int-to-float v2, v0

    sub-float v2, p2, v2

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lmaps/l/ab;
    .locals 1

    iget-object v0, p0, Lmaps/l/ay;->D:Lmaps/l/ab;

    return-object v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 2

    iget-object v0, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/ay;->H:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    :cond_0
    iget-object v0, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/ay;->I:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    :cond_1
    return-void
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/l/ay;->p:Z

    return-void
.end method

.method public declared-synchronized c_()Lmaps/t/bx;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/l/ay;->d:Lmaps/t/bx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/l/ay;

    invoke-virtual {p0, p1}, Lmaps/l/ay;->a(Lmaps/l/ay;)I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/ay;->o:Z

    return-void
.end method

.method public d_()Lmaps/t/bb;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public e()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/l/ay;->o:Z

    iget-boolean v0, p0, Lmaps/l/ay;->p:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lmaps/l/ay;->p:Z

    iget-object v0, p0, Lmaps/l/ay;->h:Lmaps/y/k;

    invoke-virtual {v0, p0}, Lmaps/y/k;->c(Lmaps/l/ay;)V

    :cond_0
    return-void
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lmaps/l/ay;->w:I

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/l/ay;->q:Z

    return v0
.end method

.method public k()Lmaps/y/k;
    .locals 1

    iget-object v0, p0, Lmaps/l/ay;->h:Lmaps/y/k;

    return-object v0
.end method

.method public l()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lmaps/l/ay;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public m()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lmaps/l/ay;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public n()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/l/ay;->o:Z

    return v0
.end method

.method public o_()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/l/ay;->f:Ljava/lang/String;

    return-object v0
.end method

.method public q()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    iget-object v0, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->h()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/l/ay;->i:Lmaps/cr/a;

    :cond_0
    return v0
.end method

.method public r()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    iget-object v0, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->h()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/l/ay;->j:Lmaps/cr/a;

    :cond_0
    return v0
.end method
