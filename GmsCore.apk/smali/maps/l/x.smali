.class public Lmaps/l/x;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/az;


# instance fields
.field private final a:Landroid/graphics/Bitmap;

.field private final b:Lmaps/cr/b;

.field private final c:I

.field private final d:I

.field private e:Z


# direct methods
.method constructor <init>(Landroid/graphics/Bitmap;FLmaps/cr/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lmaps/l/x;->b:Lmaps/cr/b;

    iget-object v0, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lmaps/l/x;->c:I

    iget-object v0, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lmaps/l/x;->d:I

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lmaps/l/x;->c:I

    int-to-float v0, v0

    return v0
.end method

.method public a(Lmaps/af/q;)Lmaps/cr/a;
    .locals 2

    iget-object v0, p0, Lmaps/l/x;->b:Lmaps/cr/b;

    iget-object v1, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/cr/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/cr/a;->f()V

    :cond_0
    return-object v0
.end method

.method public a(Lmaps/cr/c;Lmaps/af/q;)Lmaps/cr/a;
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lmaps/l/x;->b:Lmaps/cr/b;

    iget-object v1, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/cr/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/cr/a;

    sget-boolean v1, Lmaps/ae/h;->F:Z

    invoke-direct {v0, p1, v1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;Z)V

    iget-boolean v1, p0, Lmaps/l/x;->e:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lmaps/cr/a;->e(Z)V

    :cond_0
    invoke-virtual {v0, v2}, Lmaps/cr/a;->c(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/cr/a;->d(Z)V

    invoke-static {}, Lmaps/bm/f;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lmaps/bm/b;->f:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v1, p0, Lmaps/l/x;->b:Lmaps/cr/b;

    iget-object v2, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0}, Lmaps/cr/b;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {v0}, Lmaps/cr/a;->f()V

    return-object v0

    :cond_2
    iget-object v1, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/l/x;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public c()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()F
    .locals 1

    iget v0, p0, Lmaps/l/x;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public f()V
    .locals 1

    iget-boolean v0, p0, Lmaps/l/x;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method
