.class public Lmaps/l/aj;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/az;


# instance fields
.field private final a:Lmaps/p/y;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lmaps/t/aa;

.field private final e:Lmaps/p/l;

.field private final f:F

.field private final g:F

.field private final h:F

.field private final i:F

.field private final j:I


# direct methods
.method constructor <init>(Lmaps/p/y;Ljava/lang/String;ILmaps/t/aa;Lmaps/p/l;)V
    .locals 8

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/l/aj;->a:Lmaps/p/y;

    iput-object p2, p0, Lmaps/l/aj;->b:Ljava/lang/String;

    iput p3, p0, Lmaps/l/aj;->c:I

    iput-object p4, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    const/high16 v6, 0x3f800000

    invoke-virtual {p4}, Lmaps/t/aa;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4}, Lmaps/t/aa;->k()Lmaps/t/s;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/s;->b()I

    move-result v0

    :goto_0
    iput v0, p0, Lmaps/l/aj;->j:I

    invoke-virtual {p4}, Lmaps/t/aa;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cv;->g()F

    move-result v6

    :cond_0
    iput-object p5, p0, Lmaps/l/aj;->e:Lmaps/p/l;

    iget-object v0, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    invoke-virtual {v0}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v3

    :goto_1
    int-to-float v4, p3

    move-object v0, p1

    move-object v1, p2

    move-object v2, p5

    invoke-virtual/range {v0 .. v6}, Lmaps/p/y;->a(Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FZF)[F

    move-result-object v0

    sget-object v1, Lmaps/p/y;->c:Lmaps/p/l;

    if-ne p5, v1, :cond_3

    aget v1, v0, v7

    const v2, 0x3f4ccccd

    mul-float/2addr v1, v2

    iput v1, p0, Lmaps/l/aj;->f:F

    :goto_2
    aget v1, v0, v5

    iput v1, p0, Lmaps/l/aj;->g:F

    const/4 v1, 0x2

    aget v1, v0, v1

    iput v1, p0, Lmaps/l/aj;->h:F

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Lmaps/l/aj;->i:F

    return-void

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    aget v1, v0, v7

    iput v1, p0, Lmaps/l/aj;->f:F

    goto :goto_2
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lmaps/l/aj;->f:F

    return v0
.end method

.method public a(Lmaps/af/q;)Lmaps/cr/a;
    .locals 8

    iget-object v0, p0, Lmaps/l/aj;->a:Lmaps/p/y;

    iget-object v1, p0, Lmaps/l/aj;->b:Ljava/lang/String;

    iget-object v2, p0, Lmaps/l/aj;->e:Lmaps/p/l;

    iget-object v3, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    invoke-virtual {v3}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v3

    :goto_0
    iget v4, p0, Lmaps/l/aj;->c:I

    int-to-float v4, v4

    iget-object v5, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    invoke-static {v5, p1}, Lmaps/l/u;->a(Lmaps/t/aa;Lmaps/af/q;)I

    move-result v5

    iget-object v6, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    invoke-static {v6, p1}, Lmaps/l/u;->b(Lmaps/t/aa;Lmaps/af/q;)I

    move-result v6

    iget v7, p0, Lmaps/l/aj;->j:I

    invoke-virtual/range {v0 .. v7}, Lmaps/p/y;->a(Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FIII)Lmaps/cr/a;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Lmaps/af/q;)Lmaps/cr/a;
    .locals 9

    iget-object v0, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    invoke-static {v0, p2}, Lmaps/l/u;->b(Lmaps/t/aa;Lmaps/af/q;)I

    move-result v7

    iget-object v0, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    invoke-static {v0, p2}, Lmaps/l/u;->a(Lmaps/t/aa;Lmaps/af/q;)I

    move-result v6

    iget v0, p0, Lmaps/l/aj;->j:I

    if-eqz v0, :cond_1

    const/4 v7, 0x0

    sget-object v0, Lmaps/af/q;->b:Lmaps/af/q;

    if-eq p2, v0, :cond_0

    sget-object v0, Lmaps/af/q;->c:Lmaps/af/q;

    if-ne p2, v0, :cond_1

    :cond_0
    iget v0, p0, Lmaps/l/aj;->j:I

    invoke-static {v0}, Lmaps/l/u;->c(I)I

    move-result v6

    :cond_1
    iget-object v0, p0, Lmaps/l/aj;->a:Lmaps/p/y;

    iget-object v2, p0, Lmaps/l/aj;->b:Ljava/lang/String;

    iget-object v3, p0, Lmaps/l/aj;->e:Lmaps/p/l;

    iget-object v1, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/aj;->d:Lmaps/t/aa;

    invoke-virtual {v1}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v4

    :goto_0
    iget v1, p0, Lmaps/l/aj;->c:I

    int-to-float v5, v1

    iget v8, p0, Lmaps/l/aj;->j:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lmaps/p/y;->a(Lmaps/cr/c;Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FIII)Lmaps/cr/a;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/l/aj;->g:F

    return v0
.end method

.method public c()F
    .locals 1

    iget v0, p0, Lmaps/l/aj;->h:F

    return v0
.end method

.method public d()F
    .locals 1

    iget v0, p0, Lmaps/l/aj;->i:F

    return v0
.end method

.method public e()F
    .locals 2

    iget v0, p0, Lmaps/l/aj;->g:F

    iget v1, p0, Lmaps/l/aj;->h:F

    sub-float/2addr v0, v1

    iget v1, p0, Lmaps/l/aj;->i:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public f()V
    .locals 0

    return-void
.end method
