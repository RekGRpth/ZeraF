.class public Lmaps/l/q;
.super Lmaps/l/ak;


# static fields
.field static a:F

.field private static final p:Ljava/util/Map;


# instance fields
.field private final c:Lmaps/t/ah;

.field private final d:Ljava/util/List;

.field private final e:Lmaps/al/o;

.field private final f:Lmaps/al/i;

.field private final g:Lmaps/al/q;

.field private h:Lmaps/cr/a;

.field private final i:I

.field private final j:I

.field private final k:Z

.field private final l:F

.field private m:I

.field private n:Lmaps/t/v;

.field private final o:Lmaps/l/z;

.field private q:Lmaps/s/r;

.field private r:Lmaps/s/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x3f800000

    sput v0, Lmaps/l/q;->a:F

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lmaps/l/q;->p:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lmaps/t/ah;Lmaps/l/ba;Ljava/util/List;Ljava/util/Set;I)V
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p4}, Lmaps/l/ak;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lmaps/l/q;->c:Lmaps/t/ah;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iput-object v3, p0, Lmaps/l/q;->e:Lmaps/al/o;

    iput-object v3, p0, Lmaps/l/q;->f:Lmaps/al/i;

    iput-object v3, p0, Lmaps/l/q;->g:Lmaps/al/q;

    new-instance v0, Lmaps/s/t;

    iget v3, p2, Lmaps/l/ba;->a:I

    const/16 v4, 0x9

    invoke-direct {v0, v3, v4, v1}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/q;->r:Lmaps/s/t;

    :goto_0
    iput-object p3, p0, Lmaps/l/q;->d:Ljava/util/List;

    const/high16 v3, 0x3f800000

    iget-object v0, p0, Lmaps/l/q;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/as;

    invoke-virtual {v0}, Lmaps/l/as;->e()I

    move-result v4

    if-le v4, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lmaps/l/q;->k:Z

    iget-boolean v0, p0, Lmaps/l/q;->k:Z

    if-eqz v0, :cond_2

    div-int/lit8 v0, v4, 0x10

    int-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    mul-int/lit8 v4, v0, 0x2

    iget-object v0, p0, Lmaps/l/q;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/as;

    invoke-virtual {v0}, Lmaps/l/as;->d()F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    add-int/2addr v4, v2

    move v0, v2

    :goto_2
    shl-int v5, v1, v0

    if-ge v5, v4, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    new-instance v0, Lmaps/al/e;

    iget v3, p2, Lmaps/l/ba;->a:I

    invoke-direct {v0, v3, v1}, Lmaps/al/e;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/q;->e:Lmaps/al/o;

    new-instance v0, Lmaps/al/j;

    iget v3, p2, Lmaps/l/ba;->b:I

    invoke-direct {v0, v3, v1}, Lmaps/al/j;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/q;->f:Lmaps/al/i;

    new-instance v0, Lmaps/al/k;

    iget v3, p2, Lmaps/l/ba;->a:I

    invoke-direct {v0, v3, v1}, Lmaps/al/k;-><init>(IZ)V

    iput-object v0, p0, Lmaps/l/q;->g:Lmaps/al/q;

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/l/q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v2

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/as;

    invoke-virtual {v0}, Lmaps/l/as;->e()I

    move-result v6

    invoke-virtual {v0}, Lmaps/l/as;->d()F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    add-int v3, v4, v6

    move v4, v3

    move v3, v0

    goto :goto_3

    :cond_3
    iput v0, p0, Lmaps/l/q;->i:I

    sget v0, Lmaps/l/q;->a:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    const/high16 v1, 0x40000000

    add-float/2addr v0, v1

    float-to-int v1, v0

    const/16 v3, 0x8

    invoke-static {v1, v3}, Lmaps/cr/a;->a(II)I

    move-result v1

    iput v1, p0, Lmaps/l/q;->j:I

    iget v1, p0, Lmaps/l/q;->j:I

    int-to-float v1, v1

    div-float/2addr v1, v0

    iput v1, p0, Lmaps/l/q;->l:F

    new-instance v1, Lmaps/l/z;

    iget-object v3, p0, Lmaps/l/q;->d:Ljava/util/List;

    iget v4, p0, Lmaps/l/q;->l:F

    iget v5, p0, Lmaps/l/q;->i:I

    iget-boolean v6, p0, Lmaps/l/q;->k:Z

    invoke-direct {v1, v3, v4, v5, v6}, Lmaps/l/z;-><init>(Ljava/util/List;FIZ)V

    iput-object v1, p0, Lmaps/l/q;->o:Lmaps/l/z;

    invoke-virtual {p1}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ax;->g()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x43800000

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float v4, v0, v1

    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v5

    move v3, v2

    :goto_4
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    invoke-virtual {p1}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v1

    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/l/as;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lmaps/l/q;->a(Lmaps/t/ax;Lmaps/l/as;IFLmaps/s/q;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_4
    iget-object v0, p0, Lmaps/l/q;->c:Lmaps/t/ah;

    invoke-direct {p0, p5, v0}, Lmaps/l/q;->a(ILmaps/t/ah;)V

    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_2
.end method

.method synthetic constructor <init>(Lmaps/t/ah;Lmaps/l/ba;Ljava/util/List;Ljava/util/Set;ILmaps/l/ae;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lmaps/l/q;-><init>(Lmaps/t/ah;Lmaps/l/ba;Ljava/util/List;Ljava/util/Set;I)V

    return-void
.end method

.method private static declared-synchronized a(Lmaps/cr/c;Lmaps/l/z;)Lmaps/cr/a;
    .locals 2

    const-class v1, Lmaps/l/q;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/l/q;->p:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(F)V
    .locals 0

    sput p0, Lmaps/l/q;->a:F

    return-void
.end method

.method private a(ILmaps/t/ah;)V
    .locals 4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/q;->r:Lmaps/s/t;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/q;->q:Lmaps/s/r;

    invoke-direct {p0}, Lmaps/l/q;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Lmaps/q/aa;

    invoke-direct {v1, p1, p2}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    const-string v2, "Line "

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lmaps/l/q;->q:Lmaps/s/r;

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    new-instance v2, Lmaps/q/ar;

    invoke-direct {v2}, Lmaps/q/ar;-><init>()V

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    new-instance v2, Lmaps/q/au;

    new-instance v3, Lmaps/q/ab;

    invoke-direct {v3, v0}, Lmaps/q/ab;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {v2, v3}, Lmaps/q/au;-><init>(Lmaps/q/ab;)V

    invoke-virtual {v1, v2}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/l/q;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lmaps/q/aa;->b(I)V

    iget-object v0, p0, Lmaps/l/q;->r:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->h()V

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V
    .locals 18

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move/from16 v0, p6

    int-to-float v2, v0

    div-float/2addr v1, v2

    const/high16 v2, 0x41800000

    mul-float v12, v1, v2

    const/4 v1, 0x0

    move v7, v1

    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v7, v1, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/au;

    iget v2, v1, Lmaps/l/au;->a:F

    mul-float v2, v2, p5

    add-float v13, p3, v2

    iget v2, v1, Lmaps/l/au;->b:F

    mul-float v2, v2, p5

    const/high16 v3, 0x3f000000

    mul-float v14, v2, v3

    iget v2, v1, Lmaps/l/au;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v15, v1, Lmaps/l/au;->d:[I

    if-nez v15, :cond_2

    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_1

    sub-float v2, v13, v14

    const/high16 v1, 0x3f000000

    add-float v3, p4, v1

    add-float v4, v13, v14

    const/high16 v1, 0x3f000000

    add-float v5, p4, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_0

    :cond_1
    sub-float v2, v13, v14

    const/4 v3, 0x0

    add-float v4, v13, v14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v5, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    const/4 v2, 0x1

    move/from16 v4, p4

    :goto_2
    move/from16 v0, p6

    if-ge v3, v0, :cond_0

    const/4 v1, 0x0

    move v8, v1

    move v1, v2

    move v2, v3

    move v3, v4

    :goto_3
    array-length v4, v15

    rem-int/lit8 v4, v4, 0x2

    if-gt v8, v4, :cond_6

    array-length v0, v15

    move/from16 v16, v0

    const/4 v4, 0x0

    move v11, v4

    move v9, v1

    move v10, v2

    :goto_4
    move/from16 v0, v16

    if-ge v11, v0, :cond_5

    aget v17, v15, v11

    move/from16 v0, v17

    int-to-float v1, v0

    const/high16 v2, 0x41800000

    div-float/2addr v1, v2

    mul-float/2addr v1, v12

    add-float v5, v3, v1

    if-eqz v9, :cond_3

    sub-float v2, v13, v14

    add-float v4, v13, v14

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    if-nez v9, :cond_4

    const/4 v1, 0x1

    :goto_5
    add-int v10, v10, v17

    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move v9, v1

    move v3, v5

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    goto :goto_5

    :cond_5
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v2, v10

    move v1, v9

    goto :goto_3

    :cond_6
    move v4, v3

    move v3, v2

    move v2, v1

    goto :goto_2

    :cond_7
    return-void
.end method

.method static a(Ljava/util/List;Landroid/graphics/Bitmap;FZ)V
    .locals 10

    const/4 v0, 0x1

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/high16 v3, 0x3f800000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000

    mul-float/2addr v3, v4

    if-eqz p3, :cond_0

    move v8, v0

    :goto_0
    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v8, :cond_1

    invoke-interface {p0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lmaps/l/as;

    invoke-virtual {v7}, Lmaps/l/as;->e()I

    move-result v6

    int-to-float v4, v9

    invoke-virtual {v7}, Lmaps/l/as;->f()Ljava/util/ArrayList;

    move-result-object v0

    move v5, p2

    invoke-static/range {v0 .. v6}, Lmaps/l/q;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    invoke-virtual {v7}, Lmaps/l/as;->g()Ljava/util/ArrayList;

    move-result-object v0

    move v5, p2

    invoke-static/range {v0 .. v6}, Lmaps/l/q;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    move v8, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Lmaps/cr/c;Lmaps/af/q;)V
    .locals 5

    const/high16 v4, 0x10000

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method private static declared-synchronized a(Lmaps/cr/c;Lmaps/l/z;Lmaps/cr/a;)V
    .locals 3

    const-class v1, Lmaps/l/q;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/l/q;->p:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    sget-object v2, Lmaps/l/q;->p:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lmaps/t/ax;Lmaps/l/as;IFLmaps/s/q;)V
    .locals 11

    invoke-virtual {p2}, Lmaps/l/as;->a()Lmaps/t/cg;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cg;->b()I

    move-result v10

    const/4 v0, 0x2

    if-ge v10, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lmaps/l/as;->c()Lmaps/t/v;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/q;->n:Lmaps/t/v;

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {p1}, Lmaps/t/ax;->g()I

    move-result v5

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmaps/l/q;->k:Z

    if-nez v0, :cond_1

    const/4 v3, 0x1

    const/high16 v6, 0x3f800000

    iget-object v7, p0, Lmaps/l/q;->r:Lmaps/s/t;

    iget-object v8, p0, Lmaps/l/q;->r:Lmaps/s/t;

    const/4 v9, 0x0

    move-object/from16 v0, p5

    move v2, p4

    invoke-virtual/range {v0 .. v9}, Lmaps/s/q;->a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    const/4 v2, 0x1

    iget v3, p0, Lmaps/l/q;->i:I

    const/4 v0, 0x1

    new-array v4, v0, [I

    const/4 v0, 0x0

    aput p3, v4, v0

    iget-object v5, p0, Lmaps/l/q;->r:Lmaps/s/t;

    move-object/from16 v0, p5

    move v1, v10

    invoke-virtual/range {v0 .. v5}, Lmaps/s/q;->a(IZI[ILmaps/al/c;)V

    iget-object v0, p0, Lmaps/l/q;->r:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->d()V

    :goto_1
    iget-object v0, p0, Lmaps/l/q;->r:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->d()V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lmaps/l/as;->e()I

    move-result v0

    const/high16 v2, 0x45000000

    int-to-float v0, v0

    div-float v6, v2, v0

    const/4 v3, 0x1

    iget-object v7, p0, Lmaps/l/q;->r:Lmaps/s/t;

    iget-object v8, p0, Lmaps/l/q;->r:Lmaps/s/t;

    iget-object v9, p0, Lmaps/l/q;->r:Lmaps/s/t;

    move-object/from16 v0, p5

    move v2, p4

    invoke-virtual/range {v0 .. v9}, Lmaps/s/q;->a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lmaps/l/q;->k:Z

    if-nez v0, :cond_3

    const/4 v3, 0x1

    const/high16 v6, 0x3f800000

    iget-object v7, p0, Lmaps/l/q;->e:Lmaps/al/o;

    iget-object v8, p0, Lmaps/l/q;->f:Lmaps/al/i;

    const/4 v9, 0x0

    move-object/from16 v0, p5

    move v2, p4

    invoke-virtual/range {v0 .. v9}, Lmaps/s/q;->a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    const/4 v2, 0x1

    iget v3, p0, Lmaps/l/q;->i:I

    const/4 v0, 0x1

    new-array v4, v0, [I

    const/4 v0, 0x0

    aput p3, v4, v0

    iget-object v5, p0, Lmaps/l/q;->g:Lmaps/al/q;

    move-object/from16 v0, p5

    move v1, v10

    invoke-virtual/range {v0 .. v5}, Lmaps/s/q;->a(IZI[ILmaps/al/c;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lmaps/l/as;->e()I

    move-result v0

    const/high16 v2, 0x45000000

    int-to-float v0, v0

    div-float v6, v2, v0

    const/4 v3, 0x1

    iget-object v7, p0, Lmaps/l/q;->e:Lmaps/al/o;

    iget-object v8, p0, Lmaps/l/q;->f:Lmaps/al/i;

    iget-object v9, p0, Lmaps/l/q;->g:Lmaps/al/q;

    move-object/from16 v0, p5

    move v2, p4

    invoke-virtual/range {v0 .. v9}, Lmaps/s/q;->a(Lmaps/t/cg;FZLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    goto/16 :goto_0
.end method

.method static a(Lmaps/t/cg;Lmaps/l/ba;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lmaps/t/cg;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v2, v1, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    mul-int/lit8 v3, v2, 0x5

    iget v4, p1, Lmaps/l/ba;->a:I

    if-lez v4, :cond_2

    iget v4, p1, Lmaps/l/ba;->a:I

    add-int/2addr v4, v3

    const/16 v5, 0x4000

    if-gt v4, v5, :cond_0

    :cond_2
    iget v4, p1, Lmaps/l/ba;->a:I

    add-int/2addr v3, v4

    iput v3, p1, Lmaps/l/ba;->a:I

    iget v3, p1, Lmaps/l/ba;->b:I

    mul-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x3

    invoke-virtual {p0}, Lmaps/t/cg;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x3

    :cond_3
    add-int/2addr v0, v2

    add-int/2addr v0, v3

    iput v0, p1, Lmaps/l/ba;->b:I

    move v0, v1

    goto :goto_0
.end method

.method private b(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lmaps/l/q;->o:Lmaps/l/z;

    invoke-static {p1, v0}, Lmaps/l/q;->a(Lmaps/cr/c;Lmaps/l/z;)Lmaps/cr/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/l/q;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Lmaps/cr/a;

    invoke-direct {v1, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    iput-object v1, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    iget-object v1, p0, Lmaps/l/q;->o:Lmaps/l/z;

    iget-object v2, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-static {p1, v1, v2}, Lmaps/l/q;->a(Lmaps/cr/c;Lmaps/l/z;Lmaps/cr/a;)V

    iget-object v1, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {v1, v3}, Lmaps/cr/a;->c(Z)V

    iget-object v1, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {v1, v3}, Lmaps/cr/a;->b(Z)V

    iget-object v1, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {v1, v0}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->f()V

    goto :goto_0
.end method

.method private static declared-synchronized b(Lmaps/cr/c;Lmaps/l/z;)V
    .locals 2

    const-class v1, Lmaps/l/q;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/l/q;->p:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c()Landroid/graphics/Bitmap;
    .locals 4

    iget v0, p0, Lmaps/l/q;->j:I

    const/4 v1, 0x1

    iget v2, p0, Lmaps/l/q;->i:I

    shl-int/2addr v1, v2

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    iget-object v1, p0, Lmaps/l/q;->d:Ljava/util/List;

    iget v2, p0, Lmaps/l/q;->l:F

    iget-boolean v3, p0, Lmaps/l/q;->k:Z

    invoke-static {v1, v0, v2, v3}, Lmaps/l/q;->a(Ljava/util/List;Landroid/graphics/Bitmap;FZ)V

    return-object v0
.end method

.method private d(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    iget-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->h()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/l/q;->o:Lmaps/l/z;

    invoke-static {p1, v0}, Lmaps/l/q;->b(Lmaps/cr/c;Lmaps/l/z;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/q;->q:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->d()I

    move-result v0

    :goto_0
    iget-object v1, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {v1}, Lmaps/cr/a;->i()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/l/q;->e:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->d()I

    move-result v0

    iget-object v1, p0, Lmaps/l/q;->f:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/q;->g:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->d()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/l/q;->d(Lmaps/cr/c;)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/l/q;->c(Lmaps/cr/c;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/q;->q:Lmaps/s/r;

    iget-object v0, p0, Lmaps/l/q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/q;->e:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/q;->f:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/q;->g:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 8

    const/16 v7, 0x1700

    const/high16 v6, -0x41000000

    const/high16 v0, 0x3f800000

    const/high16 v4, 0x3f000000

    const/4 v5, 0x0

    invoke-interface {p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/p/av;->c()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/l/q;->n:Lmaps/t/v;

    if-eqz v1, :cond_2

    invoke-interface {p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v1

    iget-object v2, p0, Lmaps/l/q;->n:Lmaps/t/v;

    invoke-virtual {v1, v2}, Lmaps/p/av;->b(Lmaps/t/v;)Lmaps/p/z;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v2

    iget-object v3, p0, Lmaps/l/q;->c:Lmaps/t/ah;

    invoke-virtual {v3}, Lmaps/t/ah;->c()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    if-nez v2, :cond_3

    invoke-direct {p0, p1}, Lmaps/l/q;->b(Lmaps/cr/c;)V

    :cond_3
    iput v1, p0, Lmaps/l/q;->m:I

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    const/16 v3, 0x1702

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v4, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    int-to-float v3, v1

    add-float/2addr v3, v0

    iget-boolean v4, p0, Lmaps/l/q;->k:Z

    if-eqz v4, :cond_4

    int-to-float v4, v1

    add-float/2addr v0, v4

    :cond_4
    invoke-interface {v2, v3, v0, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v6, v6, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_5
    iget-object v0, p0, Lmaps/l/q;->h:Lmaps/cr/a;

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/l/q;->e:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/q;->g:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/q;->f:Lmaps/al/i;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto/16 :goto_0
.end method

.method public b()I
    .locals 3

    const/16 v0, 0x260

    iget-object v1, p0, Lmaps/l/q;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/as;

    invoke-virtual {v0}, Lmaps/l/as;->h()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/q;->q:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->c()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/l/q;->e:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->a()I

    move-result v0

    iget-object v2, p0, Lmaps/l/q;->f:Lmaps/al/i;

    invoke-virtual {v2}, Lmaps/al/i;->d()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/l/q;->g:Lmaps/al/q;

    invoke-virtual {v2}, Lmaps/al/q;->a()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    invoke-direct {p0, p1}, Lmaps/l/q;->d(Lmaps/cr/c;)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v1

    iget-object v0, p0, Lmaps/l/q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {v1, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/q;->e:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/q;->f:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/q;->g:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    :cond_1
    return-void
.end method
