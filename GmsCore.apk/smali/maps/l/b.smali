.class public Lmaps/l/b;
.super Lmaps/l/aw;

# interfaces
.implements Lmaps/y/bh;


# instance fields
.field private final t:Lmaps/t/n;

.field private u:Z


# direct methods
.method constructor <init>(Lmaps/t/n;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/y;Lmaps/t/y;FFZZLmaps/l/g;Lmaps/l/g;[Lmaps/l/h;Z)V
    .locals 18

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move/from16 v15, p13

    invoke-direct/range {v2 .. v17}, Lmaps/l/aw;-><init>(Lmaps/t/ca;Lmaps/ac/g;Ljava/lang/String;Lmaps/t/y;Lmaps/t/y;FFZZLmaps/l/g;Lmaps/l/g;[Lmaps/l/h;ZZLmaps/cm/f;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lmaps/l/b;->t:Lmaps/t/n;

    return-void
.end method


# virtual methods
.method public a(Lmaps/bq/d;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget-object v2, p0, Lmaps/l/b;->l:Lmaps/t/y;

    invoke-virtual {v2}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {p1, v2}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v2

    aget v3, v2, v1

    int-to-float v3, v3

    aget v2, v2, v0

    int-to-float v2, v2

    iget v4, p0, Lmaps/l/b;->p:F

    add-float/2addr v4, v3

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    iget v4, p0, Lmaps/l/b;->q:F

    add-float/2addr v3, v4

    cmpl-float v3, v3, v6

    if-ltz v3, :cond_0

    iget v3, p0, Lmaps/l/b;->r:F

    add-float/2addr v3, v2

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    iget v3, p0, Lmaps/l/b;->s:F

    add-float/2addr v2, v3

    cmpl-float v2, v2, v6

    if-ltz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public c(FFLmaps/t/bx;Lmaps/bq/d;)I
    .locals 3

    iget-object v0, p0, Lmaps/l/b;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {p4, v0}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    sub-float v1, p1, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public c_()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/l/b;->l:Lmaps/t/y;

    invoke-virtual {v0}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/b;->u:Z

    return-void
.end method

.method public d_()Lmaps/t/bb;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/l/b;->u:Z

    return-void
.end method

.method public h()I
    .locals 3

    iget-object v0, p0, Lmaps/l/b;->m:Lmaps/l/g;

    invoke-virtual {v0}, Lmaps/l/g;->b()F

    move-result v0

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    iget-object v1, p0, Lmaps/l/b;->n:Lmaps/l/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/b;->o:Lmaps/l/h;

    iget-object v1, v1, Lmaps/l/h;->a:Lmaps/l/af;

    sget-object v2, Lmaps/l/af;->b:Lmaps/l/af;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmaps/l/b;->n:Lmaps/l/g;

    invoke-virtual {v1}, Lmaps/l/g;->b()F

    move-result v1

    add-float/2addr v0, v1

    :cond_0
    float-to-int v0, v0

    return v0
.end method

.method public j()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public o_()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
