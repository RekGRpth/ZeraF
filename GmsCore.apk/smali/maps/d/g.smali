.class public Lmaps/d/g;
.super Lmaps/d/e;


# direct methods
.method public constructor <init>(Lmaps/d/w;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/d/e;-><init>(Lmaps/d/w;)V

    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lmaps/d/f;
    .locals 9

    const v8, 0x3e32b8c2

    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    sget-object v0, Lmaps/d/f;->b:Lmaps/d/f;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/s;

    invoke-virtual {v0}, Lmaps/d/s;->f()F

    move-result v3

    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/d/s;

    invoke-virtual {v1}, Lmaps/d/s;->b()I

    move-result v6

    invoke-virtual {v0}, Lmaps/d/s;->b()I

    move-result v7

    if-eq v6, v7, :cond_2

    :cond_1
    cmpl-float v1, v5, v8

    if-lez v1, :cond_4

    sget-object v0, Lmaps/d/f;->a:Lmaps/d/f;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lmaps/d/s;->f()F

    move-result v2

    invoke-static {v3, v2}, Lmaps/d/g;->a(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v8

    if-lez v2, :cond_3

    sget-object v0, Lmaps/d/f;->a:Lmaps/d/f;

    goto :goto_0

    :cond_3
    move-object v2, v1

    goto :goto_1

    :cond_4
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x3dcccccd

    :goto_2
    invoke-virtual {v2}, Lmaps/d/s;->g()F

    move-result v2

    invoke-virtual {v0}, Lmaps/d/s;->g()F

    move-result v3

    invoke-virtual {v0}, Lmaps/d/s;->c()F

    move-result v4

    invoke-virtual {v0}, Lmaps/d/s;->d()F

    move-result v0

    add-float/2addr v0, v4

    const/high16 v4, 0x3f000000

    mul-float/2addr v0, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float v0, v2, v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    sget-object v0, Lmaps/d/f;->b:Lmaps/d/f;

    goto :goto_0

    :cond_5
    const v1, 0x3e4ccccd

    goto :goto_2

    :cond_6
    sget-object v0, Lmaps/d/f;->c:Lmaps/d/f;

    goto :goto_0
.end method

.method protected b(Lmaps/d/a;)Z
    .locals 2

    iget-object v0, p0, Lmaps/d/g;->a:Lmaps/d/w;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmaps/d/w;->b(Lmaps/d/a;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected d(Lmaps/d/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/d/g;->a:Lmaps/d/w;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmaps/d/w;->c(Lmaps/d/a;Z)V

    return-void
.end method

.method protected f(Lmaps/d/a;)Z
    .locals 2

    iget-object v0, p0, Lmaps/d/g;->a:Lmaps/d/w;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmaps/d/w;->a(Lmaps/d/a;Z)Z

    move-result v0

    return v0
.end method
