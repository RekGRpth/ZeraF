.class public Lmaps/d/t;
.super Lmaps/d/n;


# direct methods
.method public constructor <init>(Lmaps/d/w;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/d/n;-><init>(Lmaps/d/w;)V

    return-void
.end method


# virtual methods
.method protected a(F)F
    .locals 4

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ff921fb54442d18L

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method protected a(Lmaps/d/s;I)F
    .locals 1

    invoke-virtual {p1, p2}, Lmaps/d/s;->a(I)F

    move-result v0

    return v0
.end method

.method protected b(Lmaps/d/s;I)F
    .locals 1

    invoke-virtual {p1, p2}, Lmaps/d/s;->b(I)F

    move-result v0

    return v0
.end method

.method protected b(Lmaps/d/a;)Z
    .locals 1

    const-string v0, "t"

    invoke-virtual {p0, v0}, Lmaps/d/t;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/d/t;->a:Lmaps/d/w;

    invoke-interface {v0, p1}, Lmaps/d/w;->b(Lmaps/d/a;)Z

    move-result v0

    return v0
.end method

.method protected d(Lmaps/d/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/d/t;->a:Lmaps/d/w;

    invoke-interface {v0, p1}, Lmaps/d/w;->c(Lmaps/d/a;)V

    return-void
.end method

.method protected f(Lmaps/d/a;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/t;->a:Lmaps/d/w;

    invoke-interface {v0, p1}, Lmaps/d/w;->a(Lmaps/d/a;)Z

    move-result v0

    return v0
.end method
