.class public Lmaps/d/i;
.super Lmaps/d/n;


# direct methods
.method public constructor <init>(Lmaps/d/w;)V
    .locals 2

    invoke-direct {p0, p1}, Lmaps/d/n;-><init>(Lmaps/d/w;)V

    const v0, 0x3d4ccccd

    iput v0, p0, Lmaps/d/i;->d:F

    const-wide v0, 0x3fd657184ae74487L

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lmaps/d/i;->e:F

    return-void
.end method


# virtual methods
.method protected a(F)F
    .locals 4

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method protected a(Lmaps/d/s;I)F
    .locals 1

    invoke-virtual {p1, p2}, Lmaps/d/s;->b(I)F

    move-result v0

    return v0
.end method

.method protected b(Lmaps/d/s;I)F
    .locals 1

    invoke-virtual {p1, p2}, Lmaps/d/s;->a(I)F

    move-result v0

    return v0
.end method

.method protected b(Lmaps/d/a;)Z
    .locals 1

    const-string v0, "s"

    invoke-virtual {p0, v0}, Lmaps/d/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/d/i;->a:Lmaps/d/w;

    invoke-interface {v0, p1}, Lmaps/d/w;->h(Lmaps/d/a;)Z

    move-result v0

    return v0
.end method

.method protected d(Lmaps/d/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/d/i;->a:Lmaps/d/w;

    invoke-interface {v0, p1}, Lmaps/d/w;->i(Lmaps/d/a;)V

    return-void
.end method

.method protected f(Lmaps/d/a;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/i;->a:Lmaps/d/w;

    invoke-interface {v0, p1}, Lmaps/d/w;->g(Lmaps/d/a;)Z

    move-result v0

    return v0
.end method
