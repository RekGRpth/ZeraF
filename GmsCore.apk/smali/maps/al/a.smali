.class public Lmaps/al/a;
.super Ljava/lang/Object;


# instance fields
.field protected a:[B

.field protected b:I

.field protected c:I

.field d:Ljava/nio/ByteBuffer;

.field protected e:I

.field protected f:Lmaps/av/e;

.field private g:I

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/al/a;-><init>(IZ)V

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/al/a;->e:I

    iput v0, p0, Lmaps/al/a;->i:I

    iput-boolean p2, p0, Lmaps/al/a;->h:Z

    iput p1, p0, Lmaps/al/a;->b:I

    invoke-direct {p0}, Lmaps/al/a;->d()V

    return-void
.end method

.method static synthetic a(Lmaps/al/a;[BI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/al/a;->a([BI)V

    return-void
.end method

.method private a([BI)V
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lmaps/al/a;->i:I

    if-lez v1, :cond_0

    :goto_0
    if-ge v0, p2, :cond_1

    aget-byte v1, p1, v0

    and-int/lit16 v1, v1, 0xff

    iget v2, p0, Lmaps/al/a;->i:I

    iget v3, p0, Lmaps/al/a;->i:I

    rsub-int v3, v3, 0xff

    mul-int/2addr v1, v3

    div-int/lit16 v1, v1, 0xff

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    int-to-byte v1, v1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1, v0, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    :cond_1
    return-void
.end method

.method private d()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lmaps/al/a;->g:I

    iget-object v0, p0, Lmaps/al/a;->a:[B

    if-nez v0, :cond_3

    iget v0, p0, Lmaps/al/a;->b:I

    mul-int/lit8 v0, v0, 0x4

    const/16 v1, 0x1000

    if-lt v0, v1, :cond_0

    iget-boolean v1, p0, Lmaps/al/a;->h:Z

    if-eqz v1, :cond_2

    :cond_0
    new-array v0, v0, [B

    iput-object v0, p0, Lmaps/al/a;->a:[B

    :cond_1
    :goto_0
    iput v2, p0, Lmaps/al/a;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    return-void

    :cond_2
    new-instance v1, Lmaps/av/e;

    invoke-direct {v1, v0}, Lmaps/av/e;-><init>(I)V

    iput-object v1, p0, Lmaps/al/a;->f:Lmaps/av/e;

    invoke-virtual {p0}, Lmaps/al/a;->a()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    invoke-virtual {v0}, Lmaps/av/e;->a()V

    invoke-virtual {p0}, Lmaps/al/a;->a()V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 2

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    iget v1, p0, Lmaps/al/a;->g:I

    invoke-virtual {v0, v1}, Lmaps/av/e;->b(I)V

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    iget-object v0, v0, Lmaps/av/e;->c:Ljava/lang/Object;

    check-cast v0, [B

    iput-object v0, p0, Lmaps/al/a;->a:[B

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    iget v0, v0, Lmaps/av/e;->d:I

    iput v0, p0, Lmaps/al/a;->g:I

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lmaps/al/a;->i:I

    return-void
.end method

.method public a(II)V
    .locals 8

    ushr-int/lit8 v0, p1, 0x18

    int-to-byte v1, v0

    ushr-int/lit8 v0, p1, 0x10

    int-to-byte v2, v0

    ushr-int/lit8 v0, p1, 0x8

    int-to-byte v3, v0

    int-to-byte v4, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v1, v5, v6

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v2, v5, v6

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v3, v5, v6

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v4, v5, v6

    iget v5, p0, Lmaps/al/a;->g:I

    const/16 v6, 0x1000

    if-lt v5, v6, :cond_0

    invoke-virtual {p0}, Lmaps/al/a;->a()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/al/a;->c:I

    add-int/2addr v0, p2

    iput v0, p0, Lmaps/al/a;->c:I

    return-void
.end method

.method protected a(IZ)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/al/a;->a:[B

    invoke-direct {p0, v0, p1}, Lmaps/al/a;->a([BI)V

    :goto_0
    iget-object v0, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    if-eqz p2, :cond_1

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    invoke-virtual {v0}, Lmaps/av/e;->c()V

    iput-object v2, p0, Lmaps/al/a;->f:Lmaps/av/e;

    :cond_0
    iput-object v2, p0, Lmaps/al/a;->a:[B

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lmaps/al/a;->a()V

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    new-instance v1, Lmaps/al/h;

    invoke-direct {v1, p0}, Lmaps/al/h;-><init>(Lmaps/al/a;)V

    invoke-virtual {v0, v1}, Lmaps/av/e;->a(Lmaps/av/k;)V

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmaps/al/a;->e:I

    return v0
.end method

.method public b(I)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lmaps/al/a;->b:I

    if-le p1, v0, :cond_4

    iget v0, p0, Lmaps/al/a;->b:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    mul-int/lit8 v0, v1, 0x4

    iget-object v2, p0, Lmaps/al/a;->f:Lmaps/av/e;

    if-nez v2, :cond_6

    const/16 v2, 0x1000

    if-lt v0, v2, :cond_0

    iget-boolean v2, p0, Lmaps/al/a;->h:Z

    if-eqz v2, :cond_5

    :cond_0
    iget-boolean v2, p0, Lmaps/al/a;->h:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-boolean v2, Lmaps/ae/h;->i:Z

    if-eqz v2, :cond_3

    const-string v2, "ColorBuffer"

    const-string v3, "Attempt to grow fixed size buffer"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-array v0, v0, [B

    iget-object v2, p0, Lmaps/al/a;->a:[B

    iget v3, p0, Lmaps/al/a;->g:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lmaps/al/a;->a:[B

    :goto_0
    iput v1, p0, Lmaps/al/a;->b:I

    :cond_4
    return-void

    :cond_5
    new-instance v2, Lmaps/av/e;

    invoke-direct {v2, v0}, Lmaps/av/e;-><init>(I)V

    iput-object v2, p0, Lmaps/al/a;->f:Lmaps/av/e;

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    iget-object v2, p0, Lmaps/al/a;->a:[B

    iget v3, p0, Lmaps/al/a;->g:I

    invoke-virtual {v0, v2, v3}, Lmaps/av/e;->a(Ljava/lang/Object;I)V

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    iget-object v0, v0, Lmaps/av/e;->c:Ljava/lang/Object;

    check-cast v0, [B

    iput-object v0, p0, Lmaps/al/a;->a:[B

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    iget v0, v0, Lmaps/av/e;->d:I

    iput v0, p0, Lmaps/al/a;->g:I

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lmaps/al/a;->f:Lmaps/av/e;

    invoke-virtual {v2, v0}, Lmaps/av/e;->c(I)V

    goto :goto_0
.end method

.method public b(II)V
    .locals 8

    ushr-int/lit8 v0, p1, 0x18

    int-to-byte v1, v0

    ushr-int/lit8 v0, p1, 0x10

    int-to-byte v2, v0

    ushr-int/lit8 v0, p1, 0x8

    int-to-byte v3, v0

    int-to-byte v4, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v2, v5, v6

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v3, v5, v6

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v4, v5, v6

    iget-object v5, p0, Lmaps/al/a;->a:[B

    iget v6, p0, Lmaps/al/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/al/a;->g:I

    aput-byte v1, v5, v6

    iget v5, p0, Lmaps/al/a;->g:I

    const/16 v6, 0x1000

    if-lt v5, v6, :cond_0

    invoke-virtual {p0}, Lmaps/al/a;->a()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/al/a;->c:I

    add-int/2addr v0, p2

    iput v0, p0, Lmaps/al/a;->c:I

    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lmaps/al/a;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/a;->f:Lmaps/av/e;

    invoke-virtual {v0}, Lmaps/av/e;->c()V

    iput-object v1, p0, Lmaps/al/a;->f:Lmaps/av/e;

    :cond_0
    iput-object v1, p0, Lmaps/al/a;->a:[B

    return-void
.end method

.method public c()I
    .locals 2

    const/16 v0, 0x20

    iget-object v1, p0, Lmaps/al/a;->f:Lmaps/av/e;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/al/a;->f:Lmaps/av/e;

    invoke-virtual {v1}, Lmaps/av/e;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    iget-object v1, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, Lmaps/al/a;->a:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/al/a;->a:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 7

    const/16 v3, 0x1401

    const/4 v2, 0x4

    const/4 v5, 0x0

    iget-object v0, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/al/a;->d(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iput v0, p0, Lmaps/al/a;->e:I

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x2

    iget-object v6, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZILjava/nio/Buffer;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v2, v3, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColorPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method protected d(Lmaps/cr/c;)V
    .locals 2

    iget v0, p0, Lmaps/al/a;->c:I

    mul-int/lit8 v0, v0, 0x4

    invoke-virtual {p1}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/s/h;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lmaps/al/a;->d:Ljava/nio/ByteBuffer;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lmaps/al/a;->a(IZ)V

    return-void
.end method
