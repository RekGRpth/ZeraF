.class public Lmaps/al/q;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/al/c;


# instance fields
.field protected a:I

.field b:[I

.field c:I

.field d:I

.field e:Ljava/nio/Buffer;

.field protected final f:I

.field protected final g:I

.field protected final h:I

.field i:I

.field protected j:Lmaps/av/i;

.field private k:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/al/q;->a:I

    const/16 v0, 0x140c

    iput v0, p0, Lmaps/al/q;->f:I

    const/4 v0, 0x4

    iput v0, p0, Lmaps/al/q;->g:I

    const/4 v0, 0x1

    iput v0, p0, Lmaps/al/q;->h:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/al/q;-><init>(IZ)V

    return-void
.end method

.method protected constructor <init>(IIIZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/al/q;->a:I

    iput-boolean p4, p0, Lmaps/al/q;->k:Z

    iput p1, p0, Lmaps/al/q;->c:I

    sparse-switch p2, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "glNativeType must be one of GL_FIXED, GL_SHORT or GL_BYTE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const/4 v0, 0x4

    iput v0, p0, Lmaps/al/q;->g:I

    :goto_0
    iput p2, p0, Lmaps/al/q;->f:I

    iput p3, p0, Lmaps/al/q;->h:I

    invoke-direct {p0}, Lmaps/al/q;->e()V

    return-void

    :sswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lmaps/al/q;->g:I

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x1

    iput v0, p0, Lmaps/al/q;->g:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1400 -> :sswitch_2
        0x1401 -> :sswitch_2
        0x1402 -> :sswitch_1
        0x140c -> :sswitch_0
    .end sparse-switch
.end method

.method public constructor <init>(IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/al/q;->a:I

    iput-boolean p2, p0, Lmaps/al/q;->k:Z

    iput p1, p0, Lmaps/al/q;->c:I

    const/16 v0, 0x140c

    iput v0, p0, Lmaps/al/q;->f:I

    const/4 v0, 0x4

    iput v0, p0, Lmaps/al/q;->g:I

    const/4 v0, 0x1

    iput v0, p0, Lmaps/al/q;->h:I

    invoke-direct {p0}, Lmaps/al/q;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lmaps/al/q;->i:I

    iget-object v0, p0, Lmaps/al/q;->b:[I

    if-nez v0, :cond_3

    iget v0, p0, Lmaps/al/q;->c:I

    mul-int/lit8 v0, v0, 0x2

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    iget-boolean v1, p0, Lmaps/al/q;->k:Z

    if-eqz v1, :cond_2

    :cond_0
    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/q;->b:[I

    :cond_1
    :goto_0
    iput v2, p0, Lmaps/al/q;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    return-void

    :cond_2
    new-instance v1, Lmaps/av/i;

    invoke-direct {v1, v0}, Lmaps/av/i;-><init>(I)V

    iput-object v1, p0, Lmaps/al/q;->j:Lmaps/av/i;

    invoke-virtual {p0}, Lmaps/al/q;->c()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    invoke-virtual {v0}, Lmaps/av/i;->a()V

    invoke-virtual {p0}, Lmaps/al/q;->c()V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 3

    const/16 v0, 0x2c

    iget-object v1, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/al/q;->j:Lmaps/av/i;

    invoke-virtual {v1}, Lmaps/av/i;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    iget-object v1, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->capacity()I

    move-result v1

    iget v2, p0, Lmaps/al/q;->g:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, Lmaps/al/q;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/al/q;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(FF)V
    .locals 4

    const/high16 v3, 0x47800000

    iget v0, p0, Lmaps/al/q;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/q;->d:I

    iget-object v0, p0, Lmaps/al/q;->b:[I

    iget v1, p0, Lmaps/al/q;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/al/q;->i:I

    mul-float v2, p1, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/al/q;->b:[I

    iget v1, p0, Lmaps/al/q;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/al/q;->i:I

    mul-float v2, p2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v0, v1

    iget v0, p0, Lmaps/al/q;->i:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lmaps/al/q;->c()V

    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 3

    iget v0, p0, Lmaps/al/q;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/q;->d:I

    iget-object v0, p0, Lmaps/al/q;->b:[I

    iget v1, p0, Lmaps/al/q;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/al/q;->i:I

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/al/q;->b:[I

    iget v1, p0, Lmaps/al/q;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/al/q;->i:I

    aput p2, v0, v1

    iget v0, p0, Lmaps/al/q;->i:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lmaps/al/q;->c()V

    :cond_0
    return-void
.end method

.method public a(III)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    iget-object v1, p0, Lmaps/al/q;->b:[I

    iget v2, p0, Lmaps/al/q;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/al/q;->i:I

    aput p1, v1, v2

    iget-object v1, p0, Lmaps/al/q;->b:[I

    iget v2, p0, Lmaps/al/q;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/al/q;->i:I

    aput p2, v1, v2

    iget v1, p0, Lmaps/al/q;->i:I

    const/16 v2, 0x400

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lmaps/al/q;->c()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/al/q;->d:I

    add-int/2addr v0, p3

    iput v0, p0, Lmaps/al/q;->d:I

    return-void
.end method

.method protected a(Ljava/nio/ByteBuffer;I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    iget-object v1, p0, Lmaps/al/q;->b:[I

    aget v1, v1, v0

    iget v2, p0, Lmaps/al/q;->h:I

    div-int/2addr v1, v2

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Ljava/nio/ShortBuffer;I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    iget-object v1, p0, Lmaps/al/q;->b:[I

    aget v1, v1, v0

    iget v2, p0, Lmaps/al/q;->h:I

    div-int/2addr v1, v2

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    invoke-direct {p0}, Lmaps/al/q;->e()V

    return-void
.end method

.method public a(Lmaps/cr/c;I)V
    .locals 7

    const/4 v2, 0x2

    const/4 v5, 0x0

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/al/q;->c(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    mul-int/lit8 v1, p2, 0x2

    invoke-virtual {v0, v1}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v0

    sub-int/2addr v0, p2

    iget v1, p0, Lmaps/al/q;->g:I

    mul-int/2addr v0, v1

    iput v0, p0, Lmaps/al/q;->a:I

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x4

    const/16 v3, 0x140c

    iget-object v6, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZILjava/nio/Buffer;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, Lmaps/al/q;->f:I

    iget-object v3, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-interface {v0, v2, v1, v5, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public a([I)V
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lmaps/al/q;->a([III)V

    return-void
.end method

.method public a([III)V
    .locals 4

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/al/q;->i:I

    add-int/2addr v0, p3

    const/16 v1, 0x400

    if-ge v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lmaps/al/q;->b:[I

    iget v1, p0, Lmaps/al/q;->i:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lmaps/al/q;->i:I

    add-int/2addr v0, p3

    iput v0, p0, Lmaps/al/q;->i:I

    :cond_1
    iget v0, p0, Lmaps/al/q;->d:I

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/al/q;->d:I

    return-void

    :cond_2
    add-int v0, p2, p3

    :goto_0
    if-ge p2, v0, :cond_1

    sub-int v1, v0, p2

    iget v2, p0, Lmaps/al/q;->i:I

    rsub-int v2, v2, 0x400

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lmaps/al/q;->b:[I

    iget v3, p0, Lmaps/al/q;->i:I

    invoke-static {p1, p2, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v1

    iget v2, p0, Lmaps/al/q;->i:I

    add-int/2addr v1, v2

    iput v1, p0, Lmaps/al/q;->i:I

    invoke-virtual {p0}, Lmaps/al/q;->c()V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmaps/al/q;->d:I

    return v0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method protected c()V
    .locals 2

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget v1, p0, Lmaps/al/q;->i:I

    invoke-virtual {v0, v1}, Lmaps/av/i;->b(I)V

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget-object v0, v0, Lmaps/av/i;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, Lmaps/al/q;->b:[I

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget v0, v0, Lmaps/av/i;->d:I

    iput v0, p0, Lmaps/al/q;->i:I

    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lmaps/al/q;->c:I

    if-le p1, v0, :cond_4

    iget v0, p0, Lmaps/al/q;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    mul-int/lit8 v0, v1, 0x2

    iget-object v2, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-nez v2, :cond_6

    const/16 v2, 0x400

    if-lt v0, v2, :cond_0

    iget-boolean v2, p0, Lmaps/al/q;->k:Z

    if-eqz v2, :cond_5

    :cond_0
    iget-boolean v2, p0, Lmaps/al/q;->k:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-boolean v2, Lmaps/ae/h;->i:Z

    if-eqz v2, :cond_3

    const-string v2, "TexCoordBuffer"

    const-string v3, "Attempt to grow fixed size buffer"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-array v0, v0, [I

    iget-object v2, p0, Lmaps/al/q;->b:[I

    iget v3, p0, Lmaps/al/q;->i:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lmaps/al/q;->b:[I

    :goto_0
    iput v1, p0, Lmaps/al/q;->c:I

    :cond_4
    return-void

    :cond_5
    new-instance v2, Lmaps/av/i;

    invoke-direct {v2, v0}, Lmaps/av/i;-><init>(I)V

    iput-object v2, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget-object v2, p0, Lmaps/al/q;->b:[I

    iget v3, p0, Lmaps/al/q;->i:I

    invoke-virtual {v0, v2, v3}, Lmaps/av/i;->a(Ljava/lang/Object;I)V

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget-object v0, v0, Lmaps/av/i;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, Lmaps/al/q;->b:[I

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget v0, v0, Lmaps/av/i;->d:I

    iput v0, p0, Lmaps/al/q;->i:I

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lmaps/al/q;->j:Lmaps/av/i;

    invoke-virtual {v2, v0}, Lmaps/av/i;->c(I)V

    goto :goto_0
.end method

.method protected c(Lmaps/cr/c;)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lmaps/al/q;->d:I

    mul-int/lit8 v1, v0, 0x2

    invoke-virtual {p1}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v0

    iget v2, p0, Lmaps/al/q;->g:I

    mul-int/2addr v2, v1

    invoke-virtual {v0, v2}, Lmaps/s/h;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget v2, p0, Lmaps/al/q;->f:I

    const/16 v3, 0x1402

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    invoke-virtual {p0, v0, v1}, Lmaps/al/q;->a(Ljava/nio/ShortBuffer;I)V

    :goto_0
    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    invoke-virtual {v0}, Lmaps/av/i;->c()V

    iput-object v4, p0, Lmaps/al/q;->j:Lmaps/av/i;

    :cond_0
    iput-object v4, p0, Lmaps/al/q;->b:[I

    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/al/q;->c()V

    iget-object v1, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    iget v2, p0, Lmaps/al/q;->h:I

    invoke-virtual {v1, v0, v2}, Lmaps/av/i;->a(Ljava/nio/ShortBuffer;I)V

    goto :goto_0

    :cond_2
    iget v2, p0, Lmaps/al/q;->f:I

    const/16 v3, 0x1400

    if-eq v2, v3, :cond_3

    iget v2, p0, Lmaps/al/q;->f:I

    const/16 v3, 0x1401

    if-ne v2, v3, :cond_5

    :cond_3
    iput-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-nez v0, :cond_4

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0, v1}, Lmaps/al/q;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/al/q;->c()V

    iget-object v1, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    iget v2, p0, Lmaps/al/q;->h:I

    invoke-virtual {v1, v0, v2}, Lmaps/av/i;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-nez v0, :cond_6

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    iget-object v2, p0, Lmaps/al/q;->b:[I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lmaps/al/q;->c()V

    iget-object v1, p0, Lmaps/al/q;->j:Lmaps/av/i;

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    invoke-virtual {v1, v0}, Lmaps/av/i;->a(Ljava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/al/q;->a:I

    return v0
.end method

.method public d(Lmaps/cr/c;)V
    .locals 7

    const/4 v2, 0x2

    const/4 v5, 0x0

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/al/q;->c(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-virtual {v0, v5}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v0

    iget v1, p0, Lmaps/al/q;->g:I

    mul-int/2addr v0, v1

    iput v0, p0, Lmaps/al/q;->a:I

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x4

    const/16 v3, 0x140c

    iget-object v6, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZILjava/nio/Buffer;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, Lmaps/al/q;->f:I

    iget-object v3, p0, Lmaps/al/q;->e:Ljava/nio/Buffer;

    invoke-interface {v0, v2, v1, v5, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public e(Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/q;->j:Lmaps/av/i;

    invoke-virtual {v0}, Lmaps/av/i;->c()V

    iput-object v1, p0, Lmaps/al/q;->j:Lmaps/av/i;

    :cond_0
    iput-object v1, p0, Lmaps/al/q;->b:[I

    return-void
.end method
