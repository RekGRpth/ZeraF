.class public Lmaps/al/g;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private b:I

.field private final c:Z

.field private final d:Z

.field private e:I

.field private final f:[I

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/al/g;->f:[I

    iput-boolean v2, p0, Lmaps/al/g;->g:Z

    iput p1, p0, Lmaps/al/g;->b:I

    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmaps/al/g;->c:Z

    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v1, p0, Lmaps/al/g;->d:Z

    const/16 v0, 0xc

    iput v0, p0, Lmaps/al/g;->e:I

    iget-boolean v0, p0, Lmaps/al/g;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/al/g;->e:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lmaps/al/g;->e:I

    :cond_0
    iget-boolean v0, p0, Lmaps/al/g;->c:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/al/g;->e:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lmaps/al/g;->e:I

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private e(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/al/g;->f:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    const v1, 0x8892

    iget-object v2, p0, Lmaps/al/g;->f:[I

    aget v2, v2, v3

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/al/g;->h:I

    return v0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 2

    iput-object p1, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/al/g;->g:Z

    iget-object v0, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, Lmaps/al/g;->e:I

    div-int/2addr v0, v1

    iput v0, p0, Lmaps/al/g;->h:I

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/al/g;->d(Lmaps/cr/c;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 9

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v0, p0, Lmaps/al/g;->f:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lmaps/al/g;->e(Lmaps/cr/c;)V

    :goto_0
    const/4 v8, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lmaps/al/g;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    const v0, 0x8892

    iget-object v1, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iget-object v2, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    const v3, 0x88e4

    invoke-interface {v7, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/al/g;->g:Z

    :cond_0
    const/4 v6, 0x0

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_4

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/16 v3, 0x1406

    iget v5, p0, Lmaps/al/g;->e:I

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    :goto_1
    const/16 v6, 0xc

    iget-boolean v0, p0, Lmaps/al/g;->d:Z

    if-eqz v0, :cond_6

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_5

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x2

    const/4 v2, 0x4

    const/16 v3, 0x1406

    iget v5, p0, Lmaps/al/g;->e:I

    move v4, v8

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    :goto_2
    const/16 v6, 0x1c

    :cond_1
    :goto_3
    iget-boolean v0, p0, Lmaps/al/g;->c:Z

    if-eqz v0, :cond_8

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_7

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x4

    const/4 v2, 0x2

    const/16 v3, 0x1406

    iget v5, p0, Lmaps/al/g;->e:I

    move v4, v8

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    :goto_4
    add-int/lit8 v0, v6, 0x8

    :cond_2
    :goto_5
    const v0, 0x8892

    const/4 v1, 0x0

    invoke-interface {v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    return-void

    :cond_3
    const v0, 0x8892

    iget-object v1, p0, Lmaps/al/g;->f:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {v7, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x3

    const/16 v1, 0x1406

    iget v2, p0, Lmaps/al/g;->e:I

    invoke-interface {v7, v0, v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x4

    const/16 v1, 0x1406

    iget v2, p0, Lmaps/al/g;->e:I

    invoke-interface {v7, v0, v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL11;->glColorPointer(IIII)V

    goto :goto_2

    :cond_6
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/s/a;->b(I)V

    goto :goto_3

    :cond_7
    const/4 v0, 0x2

    const/16 v1, 0x1406

    iget v2, p0, Lmaps/al/g;->e:I

    invoke-interface {v7, v0, v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    goto :goto_4

    :cond_8
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_2

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lmaps/s/a;->b(I)V

    goto :goto_5
.end method

.method public c(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/al/g;->d(Lmaps/cr/c;)V

    return-void
.end method

.method public d(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/al/g;->f:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/al/g;->f:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, Lmaps/al/g;->f:[I

    aput v3, v0, v3

    iget-object v0, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/al/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_1
    return-void
.end method
