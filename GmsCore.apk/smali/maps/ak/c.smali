.class public Lmaps/ak/c;
.super Lmaps/ak/e;


# instance fields
.field private final a:Lmaps/bb/c;


# direct methods
.method public constructor <init>(Lmaps/bb/c;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lmaps/ak/e;-><init>()V

    iput-object p1, p0, Lmaps/ak/c;->a:Lmaps/bb/c;

    invoke-virtual {p1, v1}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/ak/c;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1, v0}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    :cond_0
    return-void
.end method

.method private f()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->k()Lmaps/ae/f;

    move-result-object v0

    const-string v1, "Cohort"

    invoke-virtual {v0, v1}, Lmaps/ae/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x3e

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, Lmaps/ak/c;->a:Lmaps/bb/c;

    invoke-static {p1, v0}, Lmaps/bb/b;->a(Ljava/io/DataOutput;Lmaps/bb/c;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    sget-object v0, Lmaps/c/w;->b:Lmaps/bb/d;

    invoke-static {v0, p1}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/DataInput;)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/bb/c;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v4}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/ak/c;->a:Lmaps/bb/c;

    invoke-virtual {v2, v4, v1}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ae/c;->k()Lmaps/ae/f;

    move-result-object v2

    const-string v3, "Cohort"

    invoke-virtual {v2, v3, v1}, Lmaps/ae/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v5}, Lmaps/bb/c;->i(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lmaps/ak/n;->e()Lmaps/ak/n;

    move-result-object v1

    invoke-virtual {v0, v5}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/ak/n;->b(Lmaps/bb/c;)V

    :cond_1
    return v4
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public k_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public l_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
