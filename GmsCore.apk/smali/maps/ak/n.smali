.class public Lmaps/ak/n;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ak/a;


# static fields
.field private static I:I

.field private static volatile K:Lmaps/ak/n;


# instance fields
.field private volatile A:I

.field private volatile B:I

.field private volatile C:I

.field private D:Lmaps/k/d;

.field private final E:Lmaps/ae/d;

.field private volatile F:I

.field private G:Ljava/lang/Throwable;

.field private H:I

.field private J:I

.field private L:Z

.field protected volatile a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/String;

.field protected final d:Z

.field protected final e:Lmaps/ak/m;

.field protected f:Lmaps/ak/l;

.field protected volatile g:Z

.field protected h:I

.field protected i:I

.field protected j:Lmaps/bj/n;

.field protected k:Lmaps/ak/d;

.field private volatile l:Z

.field private final m:Ljava/util/List;

.field private n:Lmaps/ak/a;

.field private final o:Ljava/lang/String;

.field private p:Ljava/lang/Long;

.field private final q:Ljava/util/List;

.field private final r:Ljava/util/Random;

.field private s:J

.field private volatile t:Z

.field private volatile u:I

.field private volatile v:J

.field private volatile w:J

.field private volatile x:Z

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lmaps/ak/n;->I:I

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x0

    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/n;->q:Ljava/util/List;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lmaps/ak/n;->r:Ljava/util/Random;

    sget-boolean v0, Lmaps/ae/h;->r:Z

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x7530

    :goto_0
    iput-wide v0, p0, Lmaps/ak/n;->s:J

    iput-boolean v2, p0, Lmaps/ak/n;->t:Z

    iput-wide v4, p0, Lmaps/ak/n;->v:J

    iput-wide v4, p0, Lmaps/ak/n;->w:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/ak/n;->y:J

    iput-wide v4, p0, Lmaps/ak/n;->z:J

    iput v2, p0, Lmaps/ak/n;->A:I

    iput v2, p0, Lmaps/ak/n;->B:I

    iput v2, p0, Lmaps/ak/n;->C:I

    iput v3, p0, Lmaps/ak/n;->F:I

    iput v3, p0, Lmaps/ak/n;->H:I

    iput v3, p0, Lmaps/ak/n;->J:I

    iput-boolean v2, p0, Lmaps/ak/n;->L:Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const-wide/32 v0, 0x493e0

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lmaps/ak/n;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/n;->a:Ljava/lang/String;

    iput-object p3, p0, Lmaps/ak/n;->c:Ljava/lang/String;

    iput-object p2, p0, Lmaps/ak/n;->b:Ljava/lang/String;

    iput-object p4, p0, Lmaps/ak/n;->o:Ljava/lang/String;

    iput-boolean p5, p0, Lmaps/ak/n;->d:Z

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->d()Lmaps/bj/n;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/n;->j:Lmaps/bj/n;

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/n;->E:Lmaps/ae/d;

    new-instance v0, Lmaps/ak/d;

    iget-object v1, p0, Lmaps/ak/n;->E:Lmaps/ae/d;

    invoke-direct {v0, p0, v1}, Lmaps/ak/d;-><init>(Lmaps/ak/n;Lmaps/ae/d;)V

    iput-object v0, p0, Lmaps/ak/n;->k:Lmaps/ak/d;

    iput v2, p0, Lmaps/ak/n;->h:I

    iput v2, p0, Lmaps/ak/n;->i:I

    new-instance v0, Lmaps/ak/m;

    iget-object v1, p0, Lmaps/ak/n;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v6}, Lmaps/ak/m;-><init>(Lmaps/ak/n;Ljava/lang/String;Lmaps/ak/g;)V

    iput-object v0, p0, Lmaps/ak/n;->e:Lmaps/ak/m;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/ak/n;->m:Ljava/util/List;

    new-instance v0, Lmaps/ak/l;

    invoke-direct {v0, p0, v6}, Lmaps/ak/l;-><init>(Lmaps/ak/n;Lmaps/ak/g;)V

    iput-object v0, p0, Lmaps/ak/n;->f:Lmaps/ak/l;

    iget-object v0, p0, Lmaps/ak/n;->m:Ljava/util/List;

    iget-object v1, p0, Lmaps/ak/n;->f:Lmaps/ak/l;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-boolean v0, Lmaps/ae/h;->s:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x23

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lmaps/ak/n;->a(IZ)V

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_4

    const-string v0, "REQUEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Using server: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/ak/n;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void
.end method

.method private static A()J
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->r()V

    :cond_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->k()Lmaps/ae/f;

    move-result-object v0

    const-string v1, "SessionID"

    invoke-virtual {v0, v1}, Lmaps/ae/f;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "FLASH"

    const-string v1, "SessionID corrupt!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v0

    const-string v1, "SessionID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/bj/b;->a(Ljava/lang/String;[B)Z

    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized B()V
    .locals 2

    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    :try_start_0
    iput-wide v0, p0, Lmaps/ak/n;->z:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ak/n;->x:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/ak/n;->y:J

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ak/n;->H:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private C()Lmaps/k/d;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->D:Lmaps/k/d;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/k/d;

    invoke-direct {v0}, Lmaps/k/d;-><init>()V

    iput-object v0, p0, Lmaps/ak/n;->D:Lmaps/k/d;

    :cond_0
    iget-object v0, p0, Lmaps/ak/n;->D:Lmaps/k/d;

    return-object v0
.end method

.method static synthetic a(Lmaps/ak/n;J)J
    .locals 0

    iput-wide p1, p0, Lmaps/ak/n;->v:J

    return-wide p1
.end method

.method static synthetic a(Lmaps/ak/n;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    iput-object p1, p0, Lmaps/ak/n;->p:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic a(Lmaps/ak/n;)Lmaps/ak/l;
    .locals 1

    invoke-direct {p0}, Lmaps/ak/n;->z()Lmaps/ak/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/ak/n;
    .locals 1

    invoke-static {p0, p1, p2, p3, p4}, Lmaps/ak/n;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/ak/n;

    move-result-object v0

    return-object v0
.end method

.method private a(II)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/l;

    invoke-static {v0}, Lmaps/ak/l;->b(Lmaps/ak/l;)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/l;

    invoke-static {v0}, Lmaps/ak/l;->b(Lmaps/ak/l;)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(ILjava/lang/Throwable;)V
    .locals 5

    const-wide/16 v3, 0x7d0

    const/4 v0, 0x0

    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    iget-object v1, p0, Lmaps/ak/n;->j:Lmaps/bj/n;

    invoke-interface {v1}, Lmaps/bj/n;->c()V

    iput-object p2, p0, Lmaps/ak/n;->G:Ljava/lang/Throwable;

    iput p1, p0, Lmaps/ak/n;->H:I

    const/4 v1, 0x4

    if-ne p1, v1, :cond_5

    iget-wide v1, p0, Lmaps/ak/n;->y:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lmaps/ak/n;->x:Z

    if-eqz v1, :cond_4

    :cond_1
    invoke-direct {p0}, Lmaps/ak/n;->B()V

    iput p1, p0, Lmaps/ak/n;->H:I

    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Lmaps/ak/n;->y:J

    :cond_2
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Lmaps/ak/n;->a(I)V

    :cond_3
    return-void

    :cond_4
    :try_start_1
    iget-wide v1, p0, Lmaps/ak/n;->y:J

    iget-wide v3, p0, Lmaps/ak/n;->s:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    iget-wide v1, p0, Lmaps/ak/n;->y:J

    const-wide/16 v3, 0x2

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lmaps/ak/n;->y:J

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    :try_start_2
    iget-boolean v1, p0, Lmaps/ak/n;->x:Z

    if-nez v1, :cond_7

    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Lmaps/ak/n;->y:J

    iget-wide v1, p0, Lmaps/ak/n;->z:J

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v1, v1, v3

    if-nez v1, :cond_6

    iget-object v1, p0, Lmaps/ak/n;->E:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->b()J

    move-result-wide v1

    iput-wide v1, p0, Lmaps/ak/n;->z:J

    goto :goto_0

    :cond_6
    iget-wide v1, p0, Lmaps/ak/n;->z:J

    const-wide/16 v3, 0x3a98

    add-long/2addr v1, v3

    iget-object v3, p0, Lmaps/ak/n;->E:Lmaps/ae/d;

    invoke-interface {v3}, Lmaps/ae/d;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_7
    iget-wide v1, p0, Lmaps/ak/n;->y:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_8

    const-wide/16 v1, 0x7d0

    iput-wide v1, p0, Lmaps/ak/n;->y:J

    :goto_1
    iget-wide v1, p0, Lmaps/ak/n;->y:J

    iget-wide v3, p0, Lmaps/ak/n;->s:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    iget-wide v1, p0, Lmaps/ak/n;->s:J

    iput-wide v1, p0, Lmaps/ak/n;->y:J

    goto :goto_0

    :cond_8
    iget-wide v1, p0, Lmaps/ak/n;->y:J

    const-wide/16 v3, 0x5

    mul-long/2addr v1, v3

    const-wide/16 v3, 0x4

    div-long/2addr v1, v3

    iput-wide v1, p0, Lmaps/ak/n;->y:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private a(ILmaps/bb/c;)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/l;

    invoke-static {v0}, Lmaps/ak/l;->b(Lmaps/ak/l;)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(IZ)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/l;

    invoke-static {v0}, Lmaps/ak/l;->b(Lmaps/ak/l;)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/bb/c;->a(IZ)Lmaps/bb/c;

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lmaps/ak/n;I)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/n;->b(I)V

    return-void
.end method

.method static synthetic a(Lmaps/ak/n;ILjava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/ak/n;->a(ILjava/lang/Throwable;)V

    return-void
.end method

.method static synthetic a(Lmaps/ak/n;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/n;->g(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lmaps/ak/n;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/n;->b(Z)V

    return-void
.end method

.method static synthetic a(Lmaps/ak/n;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/ak/n;->a(ZZ)V

    return-void
.end method

.method private declared-synchronized a(ZZ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ak/n;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ak/n;->A:I

    if-eqz p1, :cond_0

    iget v0, p0, Lmaps/ak/n;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ak/n;->B:I

    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Lmaps/ak/n;->C:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ak/n;->C:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static a(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/ak/n;I)I
    .locals 0

    iput p1, p0, Lmaps/ak/n;->F:I

    return p1
.end method

.method static synthetic b(Lmaps/ak/n;J)J
    .locals 0

    iput-wide p1, p0, Lmaps/ak/n;->w:J

    return-wide p1
.end method

.method static synthetic b(Lmaps/ak/n;)Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->E:Lmaps/ae/d;

    return-object v0
.end method

.method private static declared-synchronized b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/ak/n;
    .locals 7

    const-class v6, Lmaps/ak/n;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lmaps/ak/n;->K:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to create multiple DataRequestDispatchers"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    :try_start_1
    invoke-static {p0}, Lmaps/ak/n;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lmaps/ak/n;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lmaps/ak/n;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Lmaps/ak/n;->K:Lmaps/ak/n;

    sget-object v0, Lmaps/ak/n;->K:Lmaps/ak/n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v6

    return-object v0
.end method

.method private b(I)V
    .locals 2

    const/16 v0, 0xc8

    if-le p1, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    const/16 v1, 0x16

    invoke-direct {p0, v1, v0}, Lmaps/ak/n;->a(II)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static b(J)V
    .locals 3

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {v1, p0, p1}, Ljava/io/DataOutput;->writeLong(J)V

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v1

    const-string v2, "SessionID"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lmaps/bj/b;->a(Ljava/lang/String;[B)Z

    invoke-interface {v1}, Lmaps/bj/b;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic b(Lmaps/ak/n;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/n;->c(Z)V

    return-void
.end method

.method static synthetic b(Lmaps/ak/n;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/ak/n;->b(ZZ)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(IZ)V

    return-void
.end method

.method private declared-synchronized b(ZZ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ak/n;->A:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/ak/n;->A:I

    if-eqz p1, :cond_0

    iget v0, p0, Lmaps/ak/n;->B:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/ak/n;->B:I

    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Lmaps/ak/n;->C:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/ak/n;->C:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static b(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic c(Lmaps/ak/n;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/n;->d(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 1

    const/16 v0, 0x1d

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(IZ)V

    return-void
.end method

.method protected static c(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic c(Lmaps/ak/n;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ak/n;->t:Z

    return v0
.end method

.method static synthetic d(Lmaps/ak/n;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/n;->B()V

    return-void
.end method

.method static synthetic d(Lmaps/ak/n;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/n;->e(Z)V

    return-void
.end method

.method private d(Z)V
    .locals 1

    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(IZ)V

    return-void
.end method

.method public static e()Lmaps/ak/n;
    .locals 1

    sget-object v0, Lmaps/ak/n;->K:Lmaps/ak/n;

    return-object v0
.end method

.method private e(Z)V
    .locals 1

    const/16 v0, 0x21

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(IZ)V

    return-void
.end method

.method static synthetic e(Lmaps/ak/n;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ak/n;->l:Z

    return v0
.end method

.method static synthetic f(Lmaps/ak/n;)I
    .locals 1

    iget v0, p0, Lmaps/ak/n;->F:I

    return v0
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->I:Z

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static f()Lmaps/ak/a;
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->r:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ak/n;->K:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->n:Lmaps/ak/a;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lmaps/ak/n;->e()Lmaps/ak/n;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic g(Lmaps/ak/n;)Lmaps/k/d;
    .locals 1

    invoke-direct {p0}, Lmaps/ak/n;->C()Lmaps/k/d;

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x1b

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic h(Lmaps/ak/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lmaps/ak/n;)Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->p:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic j(Lmaps/ak/n;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic k(Lmaps/ak/n;)I
    .locals 1

    iget v0, p0, Lmaps/ak/n;->H:I

    return v0
.end method

.method static synthetic l(Lmaps/ak/n;)J
    .locals 2

    iget-wide v0, p0, Lmaps/ak/n;->y:J

    return-wide v0
.end method

.method static synthetic m(Lmaps/ak/n;)Ljava/util/Random;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->r:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic x()Lmaps/ak/n;
    .locals 1

    sget-object v0, Lmaps/ak/n;->K:Lmaps/ak/n;

    return-object v0
.end method

.method static synthetic y()I
    .locals 2

    sget v0, Lmaps/ak/n;->I:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lmaps/ak/n;->I:I

    return v0
.end method

.method private z()Lmaps/ak/l;
    .locals 3

    new-instance v0, Lmaps/ak/l;

    iget-object v1, p0, Lmaps/ak/n;->f:Lmaps/ak/l;

    invoke-static {v1}, Lmaps/ak/l;->b(Lmaps/ak/l;)Lmaps/bb/c;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lmaps/ak/l;-><init>(Lmaps/ak/n;Lmaps/bb/c;Lmaps/ak/g;)V

    iget-object v1, p0, Lmaps/ak/n;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/ak/n;->f:Lmaps/ak/l;

    invoke-static {v0}, Lmaps/ak/l;->b(Lmaps/ak/l;)Lmaps/bb/c;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/ak/n;->x:Z

    if-nez v2, :cond_2

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_0

    const-string v1, "REQUEST"

    const-string v2, "In Error Mode"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/ak/n;->x:Z

    const-wide/high16 v1, -0x8000000000000000L

    iput-wide v1, p0, Lmaps/ak/n;->z:J

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ak/n;->j:Lmaps/bj/n;

    invoke-interface {v1}, Lmaps/bj/n;->a()Z

    move-result v1

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lmaps/ak/n;->a(IZLjava/lang/String;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected a(IZLjava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/ak/n;->m()[Lmaps/ak/i;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2, p3}, Lmaps/ak/i;->a(IZLjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(I[BZZ)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lmaps/ak/n;->a(I[BZZZ)V

    return-void
.end method

.method public a(I[BZZZ)V
    .locals 6

    iget-object v0, p0, Lmaps/ak/n;->f:Lmaps/ak/l;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lmaps/ak/l;->a(I[BZZZ)V

    return-void
.end method

.method public declared-synchronized a(J)V
    .locals 2

    const-wide/16 v0, 0x7d0

    monitor-enter p0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const-wide/16 v0, 0x7d0

    :try_start_0
    iput-wide v0, p0, Lmaps/ak/n;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput-wide p1, p0, Lmaps/ak/n;->s:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x12

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a(Lmaps/ak/a;)V
    .locals 0

    iput-object p1, p0, Lmaps/ak/n;->n:Lmaps/ak/a;

    return-void
.end method

.method public declared-synchronized a(Lmaps/ak/i;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ak/n;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/n;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/ak/j;)V
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->f:Lmaps/ak/l;

    invoke-virtual {v0, p1}, Lmaps/ak/l;->a(Lmaps/ak/j;)V

    return-void
.end method

.method public a(Lmaps/bb/c;)V
    .locals 1

    const/16 v0, 0x29

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(ILmaps/bb/c;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/ak/n;->l:Z

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lmaps/ak/n;->w:J

    return-wide v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x28

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(ILjava/lang/String;)V

    return-void
.end method

.method public declared-synchronized b(Lmaps/ak/i;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ak/n;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b(Lmaps/ak/j;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/ak/n;->m()[Lmaps/ak/i;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lmaps/ak/i;->a(Lmaps/ak/j;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Lmaps/bb/c;)V
    .locals 1

    invoke-direct {p0}, Lmaps/ak/n;->C()Lmaps/k/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/k/d;->a(Lmaps/bb/c;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x27

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(ILjava/lang/String;)V

    return-void
.end method

.method protected c(Lmaps/ak/j;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/ak/n;->m()[Lmaps/ak/i;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lmaps/ak/i;->b(Lmaps/ak/j;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized c()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/ak/n;->t:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/ak/n;->A:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lmaps/ak/n;->j:Lmaps/bj/n;

    invoke-interface {v0}, Lmaps/bj/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/ak/n;->A:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ak/n;->p:Ljava/lang/Long;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/ak/n;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/n;->p:Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Lmaps/ak/n;->p:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x13

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(ILjava/lang/String;)V

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x26

    invoke-direct {p0, v0, p1}, Lmaps/ak/n;->a(ILjava/lang/String;)V

    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->a:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ak/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized i()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ak/n;->u:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized j()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ak/n;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ak/n;->u:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public k()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ak/n;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/ak/n;->u:I

    invoke-virtual {p0}, Lmaps/ak/n;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v0}, Lmaps/ak/m;->c(Lmaps/ak/m;)V

    iget-object v0, p0, Lmaps/ak/n;->k:Lmaps/ak/d;

    invoke-virtual {v0}, Lmaps/ak/d;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected l()J
    .locals 5

    invoke-static {}, Lmaps/ak/n;->A()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/ak/n;->f:Lmaps/ak/l;

    new-instance v3, Lmaps/ak/f;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lmaps/ak/f;-><init>(Lmaps/ak/n;Lmaps/ak/g;)V

    invoke-virtual {v2, v3}, Lmaps/ak/l;->a(Lmaps/ak/j;)V

    :cond_0
    return-wide v0
.end method

.method protected declared-synchronized m()[Lmaps/ak/i;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ak/n;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lmaps/ak/i;

    iget-object v1, p0, Lmaps/ak/n;->q:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected n()V
    .locals 4

    invoke-virtual {p0}, Lmaps/ak/n;->m()[Lmaps/ak/i;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3}, Lmaps/ak/i;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized o()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ak/n;->A:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized p()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/ak/n;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public q()J
    .locals 2

    iget-wide v0, p0, Lmaps/ak/n;->v:J

    return-wide v0
.end method

.method public final s()I
    .locals 1

    iget v0, p0, Lmaps/ak/n;->h:I

    return v0
.end method

.method public final t()I
    .locals 1

    iget v0, p0, Lmaps/ak/n;->i:I

    return v0
.end method

.method public u()I
    .locals 1

    iget v0, p0, Lmaps/ak/n;->F:I

    return v0
.end method

.method public v()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ak/n;->t:Z

    return-void
.end method

.method public w()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ak/n;->t:Z

    iget-object v0, p0, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v0}, Lmaps/ak/m;->e(Lmaps/ak/m;)V

    return-void
.end method
