.class public abstract Lmaps/ak/e;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ak/j;


# instance fields
.field private volatile a:Z

.field private b:I

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/ak/e;->a:Z

    iput v0, p0, Lmaps/ak/e;->b:I

    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/e;->e:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lmaps/ak/e;->e:Ljava/lang/Long;

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ak/e;->a:Z

    return v0
.end method

.method public k_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public l()V
    .locals 1

    iget v0, p0, Lmaps/ak/e;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ak/e;->b:I

    return-void
.end method

.method public l_()Z
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Lmaps/ak/e;->b:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/ak/e;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v1

    invoke-interface {v1}, Lmaps/ae/d;->a()J

    move-result-wide v1

    iget-object v3, p0, Lmaps/ak/e;->d:Ljava/lang/Long;

    if-nez v3, :cond_2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/e;->d:Ljava/lang/Long;

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lmaps/ak/e;->d:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-object v3, p0, Lmaps/ak/e;->c:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    goto :goto_0
.end method

.method public m()V
    .locals 0

    return-void
.end method

.method public n()V
    .locals 0

    return-void
.end method

.method public o()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lmaps/ak/e;->e:Ljava/lang/Long;

    return-object v0
.end method
