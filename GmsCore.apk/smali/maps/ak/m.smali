.class public Lmaps/ak/m;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lmaps/ak/n;

.field private volatile b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lmaps/ak/n;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmaps/ak/m;->b:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ak/n;Ljava/lang/String;Lmaps/ak/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/ak/m;-><init>(Lmaps/ak/n;Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized a()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->j(Lmaps/ak/n;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/l;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lmaps/ak/l;->b(Lmaps/ak/l;Z)Lmaps/ak/k;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lmaps/ak/k;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "REQUEST"

    invoke-static {v0, p1}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->v()V

    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/ak/n;->a(I)V

    return-void
.end method

.method static synthetic a(Lmaps/ak/m;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/m;->a()V

    return-void
.end method

.method static synthetic a(Lmaps/ak/m;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/m;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic b(Lmaps/ak/m;)J
    .locals 2

    invoke-direct {p0}, Lmaps/ak/m;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method private b()V
    .locals 1

    invoke-direct {p0}, Lmaps/ak/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/ak/m;->a()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lmaps/ak/m;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/m;->b()V

    return-void
.end method

.method private c()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->j(Lmaps/ak/n;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/l;

    invoke-static {v0}, Lmaps/ak/l;->c(Lmaps/ak/l;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private declared-synchronized d()J
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->k(Lmaps/ak/n;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->l(Lmaps/ak/n;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :goto_0
    monitor-exit p0

    return-wide v0

    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->l(Lmaps/ak/n;)J

    move-result-wide v0

    const-wide/16 v2, 0x320

    add-long/2addr v0, v2

    iget-object v2, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-static {v2}, Lmaps/ak/n;->m(Lmaps/ak/n;)Ljava/util/Random;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    iget-object v4, p0, Lmaps/ak/m;->a:Lmaps/ak/n;

    invoke-static {v4}, Lmaps/ak/n;->l(Lmaps/ak/n;)J

    move-result-wide v4

    rem-long/2addr v2, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-long/2addr v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic d(Lmaps/ak/m;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ak/m;->b:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/m;->b()V

    return-void
.end method

.method static synthetic e(Lmaps/ak/m;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/m;->e()V

    return-void
.end method
