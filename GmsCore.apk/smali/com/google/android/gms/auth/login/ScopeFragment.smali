.class public Lcom/google/android/gms/auth/login/ScopeFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "ScopeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_FACL_AUDIENCE:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mFaclAudience:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mFaclClickable:Landroid/view/View;

.field private mIndex:I

.field private mOnlyYouAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

.field private mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

.field private mPaclEditLayout:Landroid/view/View;

.field private mPaclRadioButton:Landroid/widget/RadioButton;

.field private mPrivateRadioButton:Landroid/widget/RadioButton;

.field private mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

.field private mScopeListener:Lcom/google/android/gms/auth/login/ScopeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/login/ScopeFragment;->DEFAULT_FACL_AUDIENCE:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/auth/login/ScopeFragment;)Lcom/google/android/gms/auth/login/ScopeListener;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/login/ScopeFragment;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeListener:Lcom/google/android/gms/auth/login/ScopeListener;

    return-object v0
.end method

.method private disablePacl()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private getScopeDetailsClickListener()Landroid/view/View$OnClickListener;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->getDetail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/common/analytics/Aspen$Action;->FRIEND_PICKER_INFO_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;-><init>(Lcom/google/android/gms/auth/login/ScopeFragment;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->hasPaclPickerData()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->getDetail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/common/analytics/Aspen$Action;->CIRCLE_PICKER_INFO_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;-><init>(Lcom/google/android/gms/auth/login/ScopeFragment;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->getDetail()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;-><init>(Lcom/google/android/gms/auth/login/ScopeFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initScopeView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const v1, 0x7f0a002c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a002d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->getDetail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->hasWarnings()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a002e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a0030

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->getWarningText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    new-instance v1, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->getDetail()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;-><init>(Lcom/google/android/gms/auth/login/ScopeFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private initScopeViewWithAcl(Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/view/View;

    const v11, 0x7f0a0030

    const v9, 0x7f0a002e

    const v10, 0x7f0a002b

    const/4 v5, 0x1

    const/4 v6, 0x0

    const v4, 0x7f0a002c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0a002d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->getDetail()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v4

    if-eqz v4, :cond_3

    const v4, 0x7f0a0031

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02000e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->hasWarnings()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->isShowSpeedbump()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v7}, Lcom/google/android/gms/common/acl/ScopeData;->getWarningText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->hasPaclPickerData()Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f0a0034

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    const v4, 0x7f0a0038

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v4, "YOUR_CIRCLES"

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0063

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->getPaclPickerData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->toAudienceFromRenderedSharingRoster(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    const v4, 0x7f0a0036

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/common/people/views/AudienceView;

    iput-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v4, p0}, Lcom/google/android/gms/common/people/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v4, v6}, Lcom/google/android/gms/common/people/views/AudienceView;->setShowEditIcon(Z)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/people/views/AudienceView;->addAll(Ljava/util/List;)V

    const v4, 0x7f0a0037

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f0a003a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/common/people/views/AudienceView;

    iput-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mOnlyYouAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mOnlyYouAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v4, p0}, Lcom/google/android/gms/common/people/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mOnlyYouAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v4, v6}, Lcom/google/android/gms/common/people/views/AudienceView;->setShowEditIcon(Z)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mOnlyYouAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    const-string v7, "ONLY_YOU"

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0065

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/gms/common/people/views/AudienceView;->add(Lcom/google/android/gms/common/people/data/AudienceMember;)V

    const v4, 0x7f0a0035

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v5

    :goto_2
    invoke-virtual {v7, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    const v4, 0x7f0a0039

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v4, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02000f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getScopeDetailsClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->hasWarnings()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v7}, Lcom/google/android/gms/common/acl/ScopeData;->getWarningText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    move v4, v6

    goto :goto_2
.end method

.method public static newInstance(ILcom/google/android/gms/common/acl/ScopeData;)Lcom/google/android/gms/auth/login/ScopeFragment;
    .locals 3
    .param p0    # I
    .param p1    # Lcom/google/android/gms/common/acl/ScopeData;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "index"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "scope_data"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Lcom/google/android/gms/auth/login/ScopeFragment;

    invoke-direct {v1}, Lcom/google/android/gms/auth/login/ScopeFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private setPaclRadioChecked(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static toAudienceFromRenderedSharingRoster(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_1

    const/4 p1, 0x0

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/Base64Utils;->decodeUrlSafeNoPadding(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/people/data/AudienceMemberConversions;->toAudienceMembersFromRenderedSharingRosters([B)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse audience from roster: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private toAudienceFromVisibleEdges(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    :goto_1
    array-length v5, v4

    if-ge v2, v5, :cond_2

    aget-object v5, v4, v2

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    :goto_2
    iget-object v5, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/gms/common/acl/ScopeData;->setAllCirclesVisible(Z)V

    move-object v0, p2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v5, "GLSActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to parse audience from circle ID list: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method private static toSharingRosterFromAudience(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/people/data/AudienceMemberConversions;->toSharingRosterBinary(Ljava/util/List;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/common/util/Base64Utils;->encodeUrlSafeNoPadding([B)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static toVisibleEdgesFromAudience(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v6, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move-object v5, v6

    :goto_0
    return-object v5

    :cond_1
    :try_start_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0x"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v5, v1, 0x1

    if-ge v5, v4, :cond_2

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, "GLSActivity"

    const-string v7, "Failed to convert audience to circle ID list"

    invoke-static {v5, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v5, v6

    goto :goto_0
.end method


# virtual methods
.method public disableFacl()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public enableFacl()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public enablePacl()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public getFaclLoggedCircles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->isAllCirclesVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->ALL_LOGGED_CIRCLES:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->toLoggedCircles(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getPaclLoggedCircles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PRIVATE_LOGGED_CIRCLES:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->getAudience()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->toLoggedCircles(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getPaclSharingRoster()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->getAudience()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->toSharingRosterFromAudience(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getVisibleEdges()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->toVisibleEdgesFromAudience(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasFriendPickerData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v0

    goto :goto_0
.end method

.method public hasPaclAudienceView()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initFromBundle(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mIndex:I

    const-string v0, "scope_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    const-string v0, "facl_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    return-void
.end method

.method public isAllCirclesVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->isAllCirclesVisible()Z

    move-result v0

    goto :goto_0
.end method

.method public maybeSetPaclPrivateRadio()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->getAudience()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->setPaclRadioChecked(Z)V

    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/login/ScopeFragment;->initFromBundle(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclRadioButton:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclEditLayout:Landroid/view/View;

    if-ne p1, v0, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->disablePacl()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->setPaclRadioChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeListener:Lcom/google/android/gms/auth/login/ScopeListener;

    iget v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mIndex:I

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/views/AudienceView;->getAudience()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/auth/login/ScopeListener;->onSelectPaclAudience(ILjava/util/ArrayList;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mOnlyYouAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->setPaclRadioChecked(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclClickable:Landroid/view/View;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->disableFacl()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeListener:Lcom/google/android/gms/auth/login/ScopeListener;

    iget v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mIndex:I

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v3}, Lcom/google/android/gms/common/acl/ScopeData;->getActivityText()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->isAllCirclesVisible()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/auth/login/ScopeListener;->onSelectFaclAudience(ILjava/util/ArrayList;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/gms/auth/login/ScopeListener;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/android/gms/auth/login/ScopeListener;

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeListener:Lcom/google/android/gms/auth/login/ScopeListener;

    if-nez p3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "index"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mIndex:I

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "scope_data"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->getVisibleEdges()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/login/ScopeFragment;->DEFAULT_FACL_AUDIENCE:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->toAudienceFromVisibleEdges(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f04000a

    invoke-virtual {v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->hasPaclPickerData()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/ScopeFragment;->initScopeViewWithAcl(Landroid/view/View;)V

    :goto_2
    return-object v1

    :cond_2
    move-object v0, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p3}, Lcom/google/android/gms/auth/login/ScopeFragment;->initFromBundle(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/ScopeFragment;->initScopeView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "index"

    iget v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "scope_data"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "facl_audience"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public setAllCirclesVisible(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/acl/ScopeData;->setAllCirclesVisible(Z)V

    :cond_0
    return-void
.end method

.method public setFaclAudience(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    return-void
.end method

.method public setPaclAudience(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/people/views/AudienceView;->set(Ljava/util/List;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->setPaclRadioChecked(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-class v1, Lcom/google/android/gms/auth/login/ScopeFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeData:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasPaclAudienceView()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPaclAudienceView:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/views/AudienceView;->getAudience()Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "\n   SharingRoster: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mPrivateRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "No one"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasFriendPickerData()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "\n   Visible edges: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment;->mFaclAudience:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->toVisibleEdgesFromAudience(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n   All circles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ScopeFragment;->isAllCirclesVisible()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-static {v1}, Lcom/google/android/gms/auth/login/ScopeFragment;->toSharingRosterFromAudience(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
