.class public Lcom/google/android/gms/auth/login/CaptchaActivity;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "CaptchaActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mCaptchaEditText:Landroid/widget/EditText;

.field private mImageView:Landroid/widget/ImageView;

.field private mNextButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->finish(I)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040004

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->setContentView(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    const-string v1, "Doing captcha..."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const v0, 0x7f0a001f

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v0, 0x7f0a0020

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCaptchaEditText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCaptchaEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0a001b

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mNextButton:Landroid/widget/Button;

    const v0, 0x7f0a0021

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCancelButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCaptchaEditText:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/CaptchaActivity;->setDefaultButton(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mNextButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/CaptchaActivity;->setDefaultButton(Landroid/view/View;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->updateWidgetState()V

    return-void
.end method

.method public start()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCaptchaEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iput-object v0, v1, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/auth/login/CaptchaActivity;->finish(ILandroid/content/Intent;)V

    return-void
.end method

.method public updateWidgetState()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->updateWidgetState()V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mCaptchaEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setFocusable(Z)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
