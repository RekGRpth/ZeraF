.class public abstract Lcom/google/android/gms/auth/login/BaseAuthActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "BaseAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/BaseAuthActivity$TestHooks;
    }
.end annotation


# static fields
.field protected static final LOCAL_LOGV:Z

.field protected static mTabletLayout:Z

.field public static sTestHooks:Lcom/google/android/gms/auth/login/BaseAuthActivity$TestHooks;


# instance fields
.field protected mAddAccount:Z

.field protected mCallAuthenticatorResponseOnFinish:Z

.field protected mCaptchaData:[B

.field protected mCaptchaToken:Ljava/lang/String;

.field protected mConfirmCredentials:Z

.field protected mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

.field protected mService:Ljava/lang/String;

.field protected mSession:Lcom/google/android/gms/auth/login/GLSSession;

.field private mShowingProgressDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->LOCAL_LOGV:Z

    new-instance v0, Lcom/google/android/gms/auth/login/BaseAuthActivity$TestHooks;

    invoke-direct {v0}, Lcom/google/android/gms/auth/login/BaseAuthActivity$TestHooks;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->sTestHooks:Lcom/google/android/gms/auth/login/BaseAuthActivity$TestHooks;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCallAuthenticatorResponseOnFinish:Z

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mConfirmCredentials:Z

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mAddAccount:Z

    return-void
.end method

.method private checkNotification(Landroid/os/Bundle;)Z
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v1, "notificationId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    return v1

    :cond_0
    const-string v1, "notificationId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    const-string v1, "session"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/auth/login/GLSSession;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    if-eqz v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    const-string v1, "GLSActivity"

    const-string v4, "Notification without session!"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_3

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-virtual {v1, v0, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->finish()V

    move v1, v3

    goto :goto_0

    :cond_3
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    const/4 v4, 0x2

    invoke-virtual {v1, v0, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private copyParams(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    if-nez v0, :cond_2

    const-string v0, "GLSActivity"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GLSActivity"

    const-string v1, "Start intent without session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mService:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "confirmCredentials"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mConfirmCredentials:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "addAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mAddAccount:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private copyParams(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    if-nez v0, :cond_0

    const-string v0, "GLSActivity"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    const-string v1, "Start intent without session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mService:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "confirmCredentials"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mConfirmCredentials:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "addAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mAddAccount:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public static initScreen(Landroid/app/Activity;)V
    .locals 2
    .param p0    # Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v1, 0xf

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mTabletLayout:Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected accountAuthenticatorResult(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    const/4 v0, 0x4

    const-string v1, "canceled"

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->accountAuthenticatorResult(Landroid/os/Bundle;ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->accountAuthenticatorResult(Landroid/os/Bundle;ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected accountAuthenticatorResult(Landroid/os/Bundle;ILjava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCallAuthenticatorResponseOnFinish:Z

    return-void
.end method

.method public finish()V
    .locals 3

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCallAuthenticatorResponseOnFinish:Z

    if-eqz v0, :cond_1

    const-string v0, "GLSActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AccountAuthenticatorResult: finish on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method protected finish(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->finish(ILandroid/content/Intent;)V

    return-void
.end method

.method protected finish(ILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    if-nez p2, :cond_0

    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    :cond_0
    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->finish()V

    return-void
.end method

.method public getCountry()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_country"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasNetworkConnection()Z
    .locals 3

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/gms/auth/login/BaseAuthActivity;->sTestHooks:Lcom/google/android/gms/auth/login/BaseAuthActivity$TestHooks;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/login/BaseAuthActivity$TestHooks;->mForceNoNetwork:Z

    if-eqz v1, :cond_0

    :goto_0
    return v2

    :cond_0
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    if-eqz p2, :cond_0

    const-string v1, "session"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/GLSSession;

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    sget-boolean v2, Lcom/google/android/gms/auth/login/BaseAuthActivity;->LOCAL_LOGV:Z

    if-eqz v2, :cond_0

    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->initScreen(Landroid/app/Activity;)V

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-nez p1, :cond_1

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->checkNotification(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    return-void

    :cond_2
    const-string v2, "session"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/auth/login/GLSSession;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession;->create()Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    :cond_3
    const-string v2, "isTop"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCallAuthenticatorResponseOnFinish:Z

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    const-string v3, "authAccount"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/auth/login/GLSUser;->get(Ljava/lang/String;)Lcom/google/android/gms/auth/login/GLSUser;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    :cond_5
    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    if-nez v2, :cond_6

    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    :cond_6
    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mService:Ljava/lang/String;

    const-string v2, "confirmCredentials"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mConfirmCredentials:Z

    const-string v2, "addAccount"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mAddAccount:Z

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_DATA:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCaptchaData:[B

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCaptchaToken:Ljava/lang/String;

    const-string v2, "showingProgressDialog"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mShowingProgressDialog:Z

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v3, "allowSkip"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "allowSkip"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "GLSActivity"

    const-string v3, "Accepting legacy allowSkip from intent"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v3, "allowSkip"

    const-string v4, "allowSkip"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_7
    const-string v2, "firstRun"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    const-string v3, "firstRun"

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/login/GLSSession;->setSetupWizardInProgress(Z)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/GLSSession;->isInSetupWizard()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mAddAccount:Z

    :cond_8
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "callerExtras"

    iget-object v3, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v5, v1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_DATA:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCaptchaData:[B

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCaptchaToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "showingProgressDialog"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mShowingProgressDialog:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "isTop"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mCallAuthenticatorResponseOnFinish:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->copyParams(Landroid/os/Bundle;)V

    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseAuthActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public shouldDisplayLastNameFirst()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "google_setup:lastnamefirst_countries"

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "*ja*ko*hu*zh*"

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->copyParams(Landroid/content/Intent;)V

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->copyParams(Landroid/content/Intent;)V

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
