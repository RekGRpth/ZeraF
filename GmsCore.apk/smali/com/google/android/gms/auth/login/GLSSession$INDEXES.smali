.class final enum Lcom/google/android/gms/auth/login/GLSSession$INDEXES;
.super Ljava/lang/Enum;
.source "GLSSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/GLSSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "INDEXES"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/gms/auth/login/GLSSession$INDEXES;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum ACCOUNT_AUTHENTICATOR_RESPONSE_CALLED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum AGREED_TO_MOBILE_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum AGREED_TO_PERSONALIZED_CONTENT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum AGREED_TO_PLAY_EMAIL:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum AGREED_TO_PLAY_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum ALLOW_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum CREATING_ACCOUNT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum HAS_ESMOBILE:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum HAS_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum IS_NEW_ACCOUNT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum IS_SETUP_WIZARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum NAME_ACTIVITY_COMPLETED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum NEEDS_CREDIT_CARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum PHOTO_ACTIVITY_COMPLETED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum TOS_SHOWN:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

.field public static final enum USER_SELECTED_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "ACCOUNT_AUTHENTICATOR_RESPONSE_CALLED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ACCOUNT_AUTHENTICATOR_RESPONSE_CALLED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "IS_NEW_ACCOUNT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->IS_NEW_ACCOUNT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "IS_SETUP_WIZARD"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->IS_SETUP_WIZARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "TOS_SHOWN"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->TOS_SHOWN:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "NAME_ACTIVITY_COMPLETED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->NAME_ACTIVITY_COMPLETED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "PHOTO_ACTIVITY_COMPLETED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->PHOTO_ACTIVITY_COMPLETED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "USER_SELECTED_GOOGLE_PLUS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->USER_SELECTED_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "CREATING_ACCOUNT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->CREATING_ACCOUNT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "ALLOW_GOOGLE_PLUS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ALLOW_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "HAS_ESMOBILE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_ESMOBILE:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "HAS_GOOGLE_PLUS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "NEEDS_CREDIT_CARD"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->NEEDS_CREDIT_CARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "AGREED_TO_PLAY_TOS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PLAY_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "AGREED_TO_PLAY_EMAIL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PLAY_EMAIL:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "AGREED_TO_PERSONALIZED_CONTENT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PERSONALIZED_CONTENT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const-string v1, "AGREED_TO_MOBILE_TOS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_MOBILE_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ACCOUNT_AUTHENTICATOR_RESPONSE_CALLED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->IS_NEW_ACCOUNT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->IS_SETUP_WIZARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->TOS_SHOWN:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->NAME_ACTIVITY_COMPLETED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->PHOTO_ACTIVITY_COMPLETED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->USER_SELECTED_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->CREATING_ACCOUNT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ALLOW_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_ESMOBILE:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->NEEDS_CREDIT_CARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PLAY_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PLAY_EMAIL:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PERSONALIZED_CONTENT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_MOBILE_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->$VALUES:[Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/auth/login/GLSSession$INDEXES;
    .locals 1

    const-class v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/auth/login/GLSSession$INDEXES;
    .locals 1

    sget-object v0, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->$VALUES:[Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v0}, [Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    return-object v0
.end method
