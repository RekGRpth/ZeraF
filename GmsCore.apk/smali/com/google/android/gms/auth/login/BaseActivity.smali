.class public abstract Lcom/google/android/gms/auth/login/BaseActivity;
.super Lcom/google/android/gms/auth/login/BaseAuthActivity;
.source "BaseActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field public static SHARED_PREFS:Ljava/lang/String;

.field private static final sWirelessSettingsIntent:Landroid/content/Intent;


# instance fields
.field protected LOCAL_LOGV:Z

.field protected mAllowBackHardKey:Z

.field protected mBackButtonClickListener:Landroid/view/View$OnClickListener;

.field protected mFrameLayout:Landroid/widget/FrameLayout;

.field private mHandler:Landroid/os/Handler;

.field protected mLastResult:I

.field protected mNextRequest:I

.field private mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

.field protected mPrimaryButton:Landroid/view/View;

.field private mStartOnEnterActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field protected mTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.phone"

    const-string v2, "com.android.phone.Settings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/BaseActivity;->sWirelessSettingsIntent:Landroid/content/Intent;

    const-string v0, "SetupWizardAccountInfoSharedPrefs"

    sput-object v0, Lcom/google/android/gms/auth/login/BaseActivity;->SHARED_PREFS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseAuthActivity;-><init>()V

    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->LOCAL_LOGV:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mAllowBackHardKey:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mHandler:Landroid/os/Handler;

    const/16 v0, -0x65

    iput v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mLastResult:I

    new-instance v0, Lcom/google/android/gms/auth/login/BaseActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/BaseActivity$3;-><init>(Lcom/google/android/gms/auth/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mBackButtonClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/gms/auth/login/BaseActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/BaseActivity$4;-><init>(Lcom/google/android/gms/auth/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/gms/auth/login/BaseActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/BaseActivity$5;-><init>(Lcom/google/android/gms/auth/login/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mStartOnEnterActionListener:Landroid/widget/TextView$OnEditorActionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/auth/login/BaseActivity;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/login/BaseActivity;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static isDomainNameValid(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->updateWidgetState()V

    return-void
.end method

.method public appendGmailHost(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p2, 0x0

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    const-string v0, "@"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->getGmailHost(Landroid/content/res/Resources;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public createErrorIntent(Lcom/google/android/gms/auth/login/Status;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/gms/auth/login/ShowErrorActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "session"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/auth/login/Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V

    return-object v0
.end method

.method protected disableBackKey()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_1

    move v0, v5

    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    const/16 v8, 0x52

    if-ne v7, v8, :cond_2

    move v2, v5

    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    const/4 v8, 0x5

    if-ne v7, v8, :cond_3

    move v1, v5

    :goto_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    const/16 v8, 0x18

    if-ne v7, v8, :cond_4

    move v4, v5

    :goto_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    const/16 v8, 0x19

    if-ne v7, v8, :cond_5

    move v3, v5

    :goto_4
    if-eqz v0, :cond_6

    iget-boolean v6, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mAllowBackHardKey:Z

    if-eqz v6, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v5

    :cond_0
    :goto_5
    return v5

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v2, v6

    goto :goto_1

    :cond_3
    move v1, v6

    goto :goto_2

    :cond_4
    move v4, v6

    goto :goto_3

    :cond_5
    move v3, v6

    goto :goto_4

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->isSetupWizard()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v6

    if-eqz v6, :cond_7

    if-nez v2, :cond_7

    if-nez v1, :cond_7

    if-nez v4, :cond_7

    if-eqz v3, :cond_0

    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v5

    goto :goto_5
.end method

.method protected getActivityContentView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getGmailHost(Landroid/content/res/Resources;Z)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Z

    if-eqz p2, :cond_1

    const v2, 0x7f0b00ad

    :goto_0
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->isWifiOnlyBuild()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v2, "de"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p2, :cond_2

    const v2, 0x7f0b00ae

    :goto_1
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_3

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Couldn\'t find gmail_host_name"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const v2, 0x7f0b0078

    goto :goto_0

    :cond_2
    const v2, 0x7f0b0079

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method public getTitleId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected hasMenu()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected isSetupWizard()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSSession;->isInSetupWizard()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const v4, 0x7f020005

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->useActionBars()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->hasMenu()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->requestWindowFeature(I)Z

    invoke-static {p0, v3}, Lcom/google/android/gms/auth/login/Compat;->actionBarSetDisplayShowHomeEnabled(Landroid/app/Activity;Z)V

    invoke-static {p0, v3}, Lcom/google/android/gms/auth/login/Compat;->actionBarSetDisplayShowTitleEnabled(Landroid/app/Activity;Z)V

    :goto_1
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession;->isInSetupWizard()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    const/high16 v2, 0x1770000

    invoke-static {v1, v2}, Lcom/google/android/gms/auth/login/Compat;->viewSetSystemUiVisibility(Landroid/widget/FrameLayout;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->disableBackKey()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mAllowBackHardKey:Z

    :cond_1
    if-eqz p1, :cond_2

    const-string v1, "nextRequest"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mNextRequest:I

    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-le v1, v2, :cond_5

    const v1, 0x7f0a001b

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_3
    const v1, 0x7f0a001a

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_4
    const v1, 0x7f0a0021

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-super {p0, v1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->setContentView(Landroid/view/View;)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->requestWindowFeature(I)Z

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, -0x1

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v2, "currentFocus"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    const-string v2, "currentFocus"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    :goto_0
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "nextRequest"

    iget v2, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mNextRequest:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method protected setBackButton(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mBackButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mAllowBackHardKey:Z

    :cond_0
    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->updateTitle()V

    return-void
.end method

.method protected setDefaultButton(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iput-object p1, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mPrimaryButton:Landroid/view/View;

    :cond_0
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mStartOnEnterActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BaseActivity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const v0, 0x7f0a0006

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseAuthActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected start()V
    .locals 0

    return-void
.end method

.method protected updateTitle()V
    .locals 1

    const v0, 0x7f0a0006

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    return-void
.end method

.method public updateWidgetState()V
    .locals 0

    return-void
.end method

.method protected useActionBars()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->hasActionBar()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/login/Compat;->hasPermanentMenuKey(Landroid/view/ViewConfiguration;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected validateDomainNameOnly(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/auth/login/BaseActivity;->isDomainNameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const-string v1, "@"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    aget-object v1, v0, v3

    invoke-static {v1}, Lcom/google/android/gms/auth/login/BaseActivity;->isDomainNameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    aget-object p1, v0, v3

    goto :goto_0

    :cond_1
    const-string p1, ""

    goto :goto_0
.end method
