.class public Lcom/google/android/gms/auth/login/LoginActivityTask;
.super Lcom/google/android/gms/auth/login/BackgroundTask;
.source "LoginActivityTask.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/auth/login/LoginActivityTask;Landroid/os/Message;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/login/LoginActivityTask;
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/login/LoginActivityTask;->updateAccount(Landroid/os/Message;Ljava/lang/String;)V

    return-void
.end method

.method public static createIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static createIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/auth/login/LoginActivityTask;->createIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "scopeData"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object v0
.end method

.method private updateAccount(Landroid/os/Message;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, v4, Lcom/google/android/gms/auth/login/GLSSession;->mAccessToken:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v6, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/GLSSession;->mAccessToken:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v7, v7, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/gms/auth/login/GLSUser;->updateWithRequestToken(Lcom/google/android/gms/auth/login/GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    const-string v4, "authtoken"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mService:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget v6, v6, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    iget-object v7, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v2

    :cond_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "intent"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {p0, v2}, Lcom/google/android/gms/auth/login/Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/gms/auth/login/Status;->toMessage(Landroid/os/Message;)V

    return-void

    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v4, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaToken:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v0, v4, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    invoke-virtual {v4, v1, v0}, Lcom/google/android/gms/auth/login/GLSUser;->setCaptcha(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    if-eqz p2, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    invoke-virtual {v4, p2}, Lcom/google/android/gms/auth/login/GLSUser;->setPassword(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/auth/login/GLSUser;->updateWithPassword(Lcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BackgroundTask;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "scopeData"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    const v1, 0x7f0b0086

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setMessage(I)V

    const v1, 0x7f0b00c2

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setTitle(I)V

    return-void
.end method

.method protected onError(Lcom/google/android/gms/auth/login/Status;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/auth/login/Status;
    .param p2    # Landroid/content/Intent;

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/auth/login/LoginActivityTask;->finish(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/auth/login/BackgroundTask;->onError(Lcom/google/android/gms/auth/login/Status;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public start()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->start()V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/login/LoginActivityTask$1;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask$1;-><init>(Lcom/google/android/gms/auth/login/LoginActivityTask;Landroid/os/Message;Landroid/os/Message;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;->start()V

    return-void
.end method
