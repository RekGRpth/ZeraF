.class public final Lcom/google/android/gms/auth/login/ShowErrorActivity;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "ShowErrorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/ShowErrorActivity$1;
    }
.end annotation


# instance fields
.field private mExplanation:Landroid/widget/TextView;

.field private mNextButton:Landroid/widget/Button;

.field mResult:I

.field private mSkipButton:Landroid/widget/Button;

.field private mSubmissionTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v3, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/gms/auth/login/Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v2

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->SUCCESS:Lcom/google/android/gms/auth/login/Status;

    iput-object v4, v3, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    sget-object v3, Lcom/google/android/gms/auth/login/ShowErrorActivity$1;->$SwitchMap$com$google$android$gms$auth$login$Status:[I

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_2

    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish(I)V

    :goto_0
    return-void

    :sswitch_0
    const-string v3, "com.google.android.apps.enterprise.dmagent"

    invoke-static {v3}, Lcom/google/android/gms/common/internal/GmsIntents;->createPlayStoreIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish(I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "GLSActivity"

    const-string v4, "Market not found for dmagent"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish(I)V

    goto :goto_0

    :sswitch_2
    iget-boolean v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mAddAccount:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_1

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish(I)V

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_2
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v13, 0x6

    const/4 v12, 0x5

    const v11, 0x7f0b0075

    const/4 v4, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v8}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    const v7, 0x7f040010

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setContentView(I)V

    const v7, 0x7f0a0042

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    const v7, 0x7f0a0043

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v7, 0x7f0a001b

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v7, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v7, 0x7f0a0044

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v7, "title"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    const-string v7, "label"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v7, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-static {p0, v2}, Lcom/google/android/gms/auth/login/Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v5

    const-string v7, "GLSActivity"

    const/4 v9, 0x2

    invoke-static {v7, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "GLSActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ShowError: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v5, Lcom/google/android/gms/auth/login/Status;->resource:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v10, v10, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-object v7, Lcom/google/android/gms/auth/login/ShowErrorActivity$1;->$SwitchMap$com$google$android$gms$auth$login$Status:[I

    invoke-virtual {v5}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v9

    aget v7, v7, v9

    packed-switch v7, :pswitch_data_0

    :goto_0
    iget v7, v5, Lcom/google/android/gms/auth/login/Status;->resource:I

    if-eqz v7, :cond_b

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget v8, v5, Lcom/google/android/gms/auth/login/Status;->resource:I

    invoke-virtual {p0, v8}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :pswitch_0
    const v7, 0x7f0b00aa

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v7, 0x7f0b00a9

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    new-array v9, v4, [Ljava/lang/CharSequence;

    iget-object v10, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v10, v10, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    aput-object v10, v9, v8

    invoke-static {v7, v9}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_1
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, v11}, Landroid/widget/Button;->setText(I)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v8, 0x7f0b00c5

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    iput v13, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    goto :goto_1

    :pswitch_2
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, v11}, Landroid/widget/Button;->setText(I)V

    iput v13, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v7, v7, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v9, v9, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, v11}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_1

    :pswitch_3
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v9, 0x7f0b009e

    invoke-virtual {p0, v9}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    new-array v10, v4, [Ljava/lang/CharSequence;

    iget-object v11, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v11, v11, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    aput-object v11, v10, v8

    invoke-static {v9, v10}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput v12, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    goto/16 :goto_1

    :pswitch_4
    const v7, 0x7f0b00c1

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mNextButton:Landroid/widget/Button;

    const v8, 0x7f0b007b

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    iget-boolean v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mAddAccount:Z

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v8, 0x7f0b0098

    invoke-virtual {p0, v8}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const v8, 0x7f0b0077

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v8, 0x7f0b00ac

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSkipButton:Landroid/widget/Button;

    const v8, 0x7f0b007a

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    :pswitch_5
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v9, 0x7f0b00a1

    invoke-virtual {p0, v9}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    new-array v10, v4, [Ljava/lang/CharSequence;

    iget-object v11, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v11, v11, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    aput-object v11, v10, v8

    invoke-static {v9, v10}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v7, 0x7f0b00a0

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    iput v12, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    goto/16 :goto_1

    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->hasNetworkConnection()Z

    move-result v7

    if-nez v7, :cond_5

    :goto_2
    iput v8, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    if-eqz v4, :cond_7

    const v8, 0x320ce

    const/4 v7, 0x0

    check-cast v7, Ljava/lang/String;

    invoke-static {v8, v7}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iget-object v8, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->isWifiOnlyBuild()Z

    move-result v7

    if-eqz v7, :cond_6

    const v7, 0x7f0b0097

    :goto_3
    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v7, 0x7f0b0095

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    move v4, v8

    goto :goto_2

    :cond_6
    const v7, 0x7f0b0096

    goto :goto_3

    :cond_7
    sget-object v7, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    if-ne v5, v7, :cond_9

    const v8, 0x320cc

    const/4 v7, 0x0

    check-cast v7, Ljava/lang/String;

    invoke-static {v8, v7}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iget-object v8, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->isWifiOnlyBuild()Z

    move-result v7

    if-eqz v7, :cond_8

    const v7, 0x7f0b0082

    :goto_4
    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v7, 0x7f0b0080

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_8
    const v7, 0x7f0b0081

    goto :goto_4

    :cond_9
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v8, 0x7f0b007f

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    const v7, 0x7f0b007e

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_7
    const v7, 0x7f0b0088

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v7}, Lcom/google/android/gms/auth/login/GLSSession;->isCreatingAccount()Z

    move-result v7

    if-eqz v7, :cond_a

    const v7, 0x7f0b0089

    :goto_5
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_a
    const v7, 0x7f0b008a

    goto :goto_5

    :pswitch_8
    iput v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mResult:I

    goto/16 :goto_0

    :cond_b
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v7, v7, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    if-nez v7, :cond_c

    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    const v8, 0x7f0b007f

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    const v7, 0x7f0b007e

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setErrorTitle(Ljava/lang/CharSequence;)V

    const-string v7, "GLSActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GAIA ERROR WITH NO RESOURCE STRING "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_c
    iget-object v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mExplanation:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v8, v8, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public setErrorTitle(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    if-eqz p1, :cond_0

    const v0, 0x7f0b00a2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->mSubmissionTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
