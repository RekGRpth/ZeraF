.class public Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;
.super Ljava/lang/Object;
.source "NotificationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/NotificationStore$NotificationData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/auth/NotificationStore;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/auth/NotificationStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;-><init>(Lcom/google/android/gms/auth/NotificationStore$1;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    # getter for: Lcom/google/android/gms/auth/NotificationStore;->mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore;->access$900(Lcom/google/android/gms/auth/NotificationStore;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    # setter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationId:I
    invoke-static {v0, v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$802(Lcom/google/android/gms/auth/NotificationStore$NotificationData;I)I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/auth/NotificationStore;Lcom/google/android/gms/auth/NotificationStore$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/auth/NotificationStore;
    .param p2    # Lcom/google/android/gms/auth/NotificationStore$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;-><init>(Lcom/google/android/gms/auth/NotificationStore;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    return-object v0
.end method

.method private verify(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;
        }
    .end annotation

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$100(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationId:I
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$800(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)I

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$400(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->type:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$200(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;

    invoke-direct {v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;-><init>()V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method public build(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 1
    .param p1    # Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    # setter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->type:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    invoke-static {v0, p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$202(Lcom/google/android/gms/auth/NotificationStore$NotificationData;Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->verify(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    return-object v0
.end method

.method public build(Z)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;
        }
    .end annotation

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->ACCOUNT_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->build(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->SCOPE_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    goto :goto_0
.end method

.method public setAccount(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    # setter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$402(Lcom/google/android/gms/auth/NotificationStore$NotificationData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setNotificationDataKey(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    # setter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$102(Lcom/google/android/gms/auth/NotificationStore$NotificationData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
