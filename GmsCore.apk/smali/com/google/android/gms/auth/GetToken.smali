.class public Lcom/google/android/gms/auth/GetToken;
.super Landroid/app/Service;
.source "GetToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/GetToken$AuthManagerServiceImpl;
    }
.end annotation


# instance fields
.field private mAuthManagerService:Lcom/google/android/gms/auth/GetToken$AuthManagerServiceImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/auth/GetToken;->mAuthManagerService:Lcom/google/android/gms/auth/GetToken$AuthManagerServiceImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/GetToken$AuthManagerServiceImpl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/auth/GetToken$AuthManagerServiceImpl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/GetToken$AuthManagerServiceImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/GetToken;->mAuthManagerService:Lcom/google/android/gms/auth/GetToken$AuthManagerServiceImpl;

    return-void
.end method
