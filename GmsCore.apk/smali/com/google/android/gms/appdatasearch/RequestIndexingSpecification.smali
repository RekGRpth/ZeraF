.class public Lcom/google/android/gms/appdatasearch/RequestIndexingSpecification;
.super Ljava/lang/Object;
.source "RequestIndexingSpecification.java"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/RequestIndexingSpecificationCreator;


# instance fields
.field mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/RequestIndexingSpecificationCreator;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/RequestIndexingSpecificationCreator;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/RequestIndexingSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/RequestIndexingSpecificationCreator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/RequestIndexingSpecification;->mVersionCode:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/RequestIndexingSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/RequestIndexingSpecificationCreator;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    sget-object v0, Lcom/google/android/gms/appdatasearch/RequestIndexingSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/RequestIndexingSpecificationCreator;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/RequestIndexingSpecificationCreator;->writeToParcel(Lcom/google/android/gms/appdatasearch/RequestIndexingSpecification;Landroid/os/Parcel;I)V

    return-void
.end method
