.class public final Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;
.super Ljava/lang/Object;
.source "UniversalSearchCorpusConfig.java"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfigCreator;


# instance fields
.field mVersionCode:I

.field universalSectionMappings:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfigCreator;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfigCreator;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->CREATOR:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfigCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->mVersionCode:I

    invoke-static {}, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->getSectionsCount()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->universalSectionMappings:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->CREATOR:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfigCreator;

    const/4 v0, 0x0

    return v0
.end method

.method public getUniversalSearchMappings()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->universalSectionMappings:[Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->CREATOR:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfigCreator;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfigCreator;->writeToParcel(Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;Landroid/os/Parcel;I)V

    return-void
.end method
