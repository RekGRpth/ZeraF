.class public Lcom/google/android/gms/appdatasearch/SuggestSpecification;
.super Ljava/lang/Object;
.source "SuggestSpecification.java"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/SuggestSpecificationCreator;


# instance fields
.field mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/SuggestSpecificationCreator;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/SuggestSpecificationCreator;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/SuggestSpecificationCreator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->mVersionCode:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/SuggestSpecificationCreator;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    sget-object v0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/SuggestSpecificationCreator;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/SuggestSpecificationCreator;->writeToParcel(Lcom/google/android/gms/appdatasearch/SuggestSpecification;Landroid/os/Parcel;I)V

    return-void
.end method
