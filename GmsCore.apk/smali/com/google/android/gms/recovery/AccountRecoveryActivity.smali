.class public Lcom/google/android/gms/recovery/AccountRecoveryActivity;
.super Landroid/app/Activity;
.source "AccountRecoveryActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;,
        Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;,
        Lcom/google/android/gms/recovery/AccountRecoveryActivity$URLSpanNoUnderline;
    }
.end annotation


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

.field private mBadPreviousEmailText:Ljava/lang/String;

.field private mBadPreviousPhoneText:Ljava/lang/String;

.field private mCountryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;",
            ">;"
        }
    .end annotation
.end field

.field private mCountryListJson:Ljava/lang/String;

.field private mCountrySpinner:Landroid/widget/Spinner;

.field private mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

.field private mDoneButton:Landroid/widget/Button;

.field private mInputLayout:Landroid/view/View;

.field private mMessage:Landroid/widget/TextView;

.field private mMessageLayout:Landroid/view/View;

.field private mPhoneNumber:Landroid/widget/EditText;

.field private mSecondaryEmailEdit:Landroid/widget/EditText;

.field private mSecondaryEmailError:Z

.field private mShowMessage:Z

.field private mYesButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousPhoneText:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousEmailText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    iget-boolean v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mShowMessage:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mShowMessage:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->displayMessage(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validateEmailInput()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validatePhoneInput()Z

    move-result v0

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailError:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->handleError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountrySpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method private displayMessage(Z)V
    .locals 3
    .param p1    # Z

    const/16 v0, 0x8

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    move v1, v2

    :goto_0
    if-eqz p1, :cond_1

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mMessageLayout:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mYesButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mInputLayout:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private static getCountryNameArray(Ljava/util/List;)[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;

    iget-object v2, v2, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;->name:Ljava/lang/String;

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getLinkifiedMessage(Lcom/google/android/gms/recovery/RecoveryResponse$Action;)Landroid/text/SpannableString;
    .locals 6

    const/4 v5, 0x0

    sget-object v0, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->REQUEST_RECOVERY_INFO:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    if-ne p1, v0, :cond_0

    const v0, 0x7f0b00fe

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "https://support.google.com/mobile/?p=accounts_recovery&hl=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v3, Landroid/text/Annotation;

    invoke-virtual {v2, v5, v0, v3}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    aget-object v0, v0, v5

    invoke-virtual {v2, v0}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v0}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v2, v0}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$URLSpanNoUnderline;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$URLSpanNoUnderline;-><init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v2

    :cond_0
    const v0, 0x7f0b00ff

    goto :goto_0
.end method

.method private handleError(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v1, "BadPhone"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    const v2, 0x7f0b0106

    invoke-virtual {p0, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->storeBadPhoneText()V

    :goto_0
    return-void

    :cond_0
    const-string v1, "BadEmail"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const v2, 0x7f0b0102

    invoke-virtual {p0, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->storeBadEmailText()V

    goto :goto_0

    :cond_1
    const-string v1, "EmailSameAsPrimary"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const v2, 0x7f0b0103

    invoke-virtual {p0, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->storeBadEmailText()V

    goto :goto_0

    :cond_2
    const-string v1, "BadCountry"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b0107

    invoke-virtual {p0, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b010b

    invoke-virtual {p0, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private hasRejectedValueInFields()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousEmailText:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousEmailText:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousPhoneText:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousPhoneText:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initCountrySpinner(Ljava/util/List;Ljava/lang/Integer;)V
    .locals 3
    .param p2    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f04001c

    invoke-static {p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getCountryNameArray(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private initFromBundle(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/16 v13, 0x8

    const/4 v11, 0x1

    const-string v9, "Email"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAccount:Ljava/lang/String;

    const-string v9, "Action"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    const-string v9, "Detail"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    sget-object v9, Lcom/google/android/gms/recovery/RecoveryResponse$Detail;->EMAIL_ONLY:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    :goto_0
    iput-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    const-string v9, "ShowMessage"

    invoke-virtual {p1, v9, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mShowMessage:Z

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    sget-object v10, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->VERIFY_RECOVERY_INFO:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    if-ne v9, v10, :cond_0

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mYesButton:Landroid/widget/Button;

    const v10, 0x7f0b010a

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setText(I)V

    :cond_0
    const-string v9, "CountryList"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryListJson:Ljava/lang/String;

    const-string v9, "Country"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryListJson:Ljava/lang/String;

    invoke-static {v9, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->parseCountryParams(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v9, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v0, v9

    check-cast v0, Ljava/lang/Integer;

    move-object v2, v0

    iget-object v9, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/util/List;

    iput-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryList:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mMessage:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    invoke-direct {p0, v10}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getLinkifiedMessage(Lcom/google/android/gms/recovery/RecoveryResponse$Action;)Landroid/text/SpannableString;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mMessage:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v9, 0x7f0a000c

    invoke-virtual {p0, v9}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v9, 0x7f0b0100

    new-array v10, v11, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAccount:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v9, "SecondaryEmail"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const-string v10, "SecondaryEmail"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    if-eqz v2, :cond_6

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    sget-object v10, Lcom/google/android/gms/recovery/RecoveryResponse$Detail;->EMAIL_ONLY:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    if-eq v9, v10, :cond_6

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryList:Ljava/util/List;

    invoke-direct {p0, v9, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->initCountrySpinner(Ljava/util/List;Ljava/lang/Integer;)V

    const-string v9, "phone"

    invoke-virtual {p0, v9}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v7

    const-string v9, "PhoneNumber"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    const-string v10, "PhoneNumber"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_2
    iget-boolean v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mShowMessage:Z

    invoke-direct {p0, v9}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->displayMessage(Z)V

    return-void

    :cond_4
    invoke-static {v3}, Lcom/google/android/gms/recovery/RecoveryResponse$Detail;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    move-result-object v9

    goto/16 :goto_0

    :catch_0
    move-exception v4

    const-string v9, "Recovery"

    const-string v10, "Unable to parse country list"

    invoke-static {v9, v10, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :cond_5
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "0000000000"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v9, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {v9, v13}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v9, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v9, v13}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_2
.end method

.method private initViews(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const v1, 0x7f0a0010

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mYesButton:Landroid/widget/Button;

    const v1, 0x7f0a0008

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mMessageLayout:Landroid/view/View;

    const v1, 0x7f0a000a

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mInputLayout:Landroid/view/View;

    const v1, 0x7f0a0011

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDoneButton:Landroid/widget/Button;

    const v1, 0x7f0a000e

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const v1, 0x7f0a000d

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    const v1, 0x7f0a000f

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountrySpinner:Landroid/widget/Spinner;

    const v1, 0x7f0a0009

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mMessage:Landroid/widget/TextView;

    const v1, 0x7f0a0006

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b00fd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->initializeListeners()V

    invoke-direct {p0, p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->initFromBundle(Landroid/os/Bundle;)V

    return-void
.end method

.method private initializeListeners()V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mYesButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/gms/recovery/AccountRecoveryActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$1;-><init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDoneButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/gms/recovery/AccountRecoveryActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$2;-><init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/gms/recovery/AccountRecoveryActivity$3;

    invoke-direct {v3, p0, v4}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$3;-><init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Z)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method private static isDomainNameValid(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidPhone(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x80

    if-gt v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static parseCountryParams(Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 9
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 v7, 0x0

    :goto_0
    return-object v7

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v4, v7, :cond_4

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v7, "Code"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v7, "Name"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    if-nez v1, :cond_2

    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move v5, v4

    :cond_3
    new-instance v7, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;

    invoke-direct {v7, v6, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    new-instance v7, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v7, v8, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private storeBadEmailText()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousEmailText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->updateState()V

    :cond_0
    return-void
.end method

.method private storeBadPhoneText()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mBadPreviousPhoneText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->updateState()V

    :cond_0
    return-void
.end method

.method private validateDomainNameOnly(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->isDomainNameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const-string v1, "@"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    aget-object v1, v0, v3

    invoke-static {v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->isDomainNameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    aget-object p1, v0, v3

    goto :goto_0

    :cond_1
    const-string p1, ""

    goto :goto_0
.end method

.method private validateEmail(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validateDomainNameOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ge v0, v2, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    return-object p1
.end method

.method private validateEmailInput()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validateEmail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailError:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const v4, 0x7f0b0083

    invoke-virtual {p0, v4}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->storeBadEmailText()V

    :goto_0
    return v2

    :cond_0
    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const v4, 0x7f0b0102

    invoke-virtual {p0, v4}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->storeBadEmailText()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private validatePhoneInput()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->isValidPhone(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    const v2, 0x7f0b0105

    invoke-virtual {p0, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->storeBadPhoneText()V

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->updateState()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method protected displayError(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$4;-><init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v1, 0x7f040000

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->setContentView(I)V

    if-eqz p1, :cond_0

    move-object v0, p1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->initViews(Landroid/os/Bundle;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    if-ne p1, v2, :cond_3

    if-nez p2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailError:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const v3, 0x7f0b0083

    invoke-virtual {p0, v3}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validateEmail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    const v3, 0x7f0b0102

    invoke-virtual {p0, v3}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    if-ne p1, v2, :cond_0

    if-nez p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validatePhoneInput()Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "Email"

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAccount:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Action"

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Detail"

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/RecoveryResponse$Detail;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "SecondaryEmail"

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PhoneNumber"

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Detail"

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/RecoveryResponse$Detail;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "ShowMessage"

    iget-boolean v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mShowMessage:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "CountryList"

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryListJson:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryList:Ljava/util/List;

    if-eqz v1, :cond_0

    const-string v2, "Country"

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;

    iget-object v1, v1, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;->code:Ljava/lang/String;

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public updateState()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    move v3, v5

    :goto_0
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    move v4, v5

    :goto_1
    iget-boolean v7, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailError:Z

    if-nez v7, :cond_3

    if-nez v3, :cond_0

    if-eqz v4, :cond_3

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->hasRejectedValueInFields()Z

    move-result v7

    if-nez v7, :cond_3

    move v1, v5

    :goto_2
    iget-object v5, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v5, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v5, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v5, v1}, Landroid/widget/Button;->setFocusable(Z)V

    return-void

    :cond_1
    move v3, v6

    goto :goto_0

    :cond_2
    move v4, v6

    goto :goto_1

    :cond_3
    move v1, v6

    goto :goto_2
.end method
