.class Lcom/google/android/gms/recovery/AccountRecoveryActivity$2;
.super Ljava/lang/Object;
.source "AccountRecoveryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/recovery/AccountRecoveryActivity;->initializeListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$2;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$2;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    # invokes: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validateEmailInput()Z
    invoke-static {v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$200(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$2;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    # invokes: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->validatePhoneInput()Z
    invoke-static {v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$300(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;

    iget-object v1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$2;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;-><init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Lcom/google/android/gms/recovery/AccountRecoveryActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
