.class public Lcom/google/android/gms/recovery/RecoveryState;
.super Ljava/lang/Object;
.source "RecoveryState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/recovery/RecoveryState$1;,
        Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;,
        Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;
    }
.end annotation


# static fields
.field private static sState:Lcom/google/android/gms/recovery/RecoveryState;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/recovery/RecoveryState;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized getSingleton()Lcom/google/android/gms/recovery/RecoveryState;
    .locals 3

    const-class v1, Lcom/google/android/gms/recovery/RecoveryState;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/recovery/RecoveryState;->sState:Lcom/google/android/gms/recovery/RecoveryState;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/recovery/RecoveryState;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/recovery/RecoveryState;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/recovery/RecoveryState;->sState:Lcom/google/android/gms/recovery/RecoveryState;

    :cond_0
    sget-object v0, Lcom/google/android/gms/recovery/RecoveryState;->sState:Lcom/google/android/gms/recovery/RecoveryState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static makeKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getAccountRecord(Ljava/lang/String;)Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, -0x1

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/recovery/RecoveryState;->mContext:Landroid/content/Context;

    const-string v4, "recovery_state"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "notification_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/recovery/RecoveryState;->makeKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v6, :cond_0

    const-string v3, "next_notification_id"

    const v4, 0x1574c

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "notification_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/recovery/RecoveryState;->makeKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "next_notification_id"

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    new-instance v3, Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;

    const/4 v4, 0x0

    invoke-direct {v3, p1, v1, v4}, Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;-><init>(Ljava/lang/String;ILcom/google/android/gms/recovery/RecoveryState$1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v3

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getNextAlarmTimeMs(J)J
    .locals 4
    .param p1    # J

    iget-object v1, p0, Lcom/google/android/gms/recovery/RecoveryState;->mContext:Landroid/content/Context;

    const-string v2, "recovery_state"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "next_alarm_time"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public getRecoveryPolicy()Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;
    .locals 8

    iget-object v5, p0, Lcom/google/android/gms/recovery/RecoveryState;->mContext:Landroid/content/Context;

    const-string v6, "recovery_state"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "PolicySilenceMs"

    const-wide/32 v6, 0x240c8400

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v5, "PolicySilenceRandomizePeriodMs"

    const-wide/32 v6, 0x48190800

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance v5, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;

    invoke-direct {v5, v2, v3, v0, v1}, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;-><init>(JJ)V

    return-object v5
.end method

.method public setNextAlarmTimeMs(J)V
    .locals 5
    .param p1    # J

    iget-object v2, p0, Lcom/google/android/gms/recovery/RecoveryState;->mContext:Landroid/content/Context;

    const-string v3, "recovery_state"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "next_alarm_time"

    invoke-interface {v0, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setRecoveryPolicy(Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/recovery/RecoveryState;->mContext:Landroid/content/Context;

    const-string v3, "recovery_state"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "PolicySilenceMs"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->getPolicySilenceMs()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v2, "PolicySilenceRandomizePeriodMs"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->getPolicySilenceRandomizePeriodMs()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
