.class Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;
.super Landroid/os/AsyncTask;
.source "AccountRecoveryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/recovery/AccountRecoveryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncUpdate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Lcom/google/android/gms/recovery/AccountRecoveryActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/recovery/AccountRecoveryActivity;
    .param p2    # Lcom/google/android/gms/recovery/AccountRecoveryActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;-><init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    # getter for: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountryList:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$800(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    # getter for: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mCountrySpinner:Landroid/widget/Spinner;
    invoke-static {v3}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$700(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;

    iget-object v0, v2, Lcom/google/android/gms/recovery/AccountRecoveryActivity$DisplayCountry;->code:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/recovery/RecoveryServerAdapter;

    iget-object v3, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {v2, v3}, Lcom/google/android/gms/recovery/RecoveryServerAdapter;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    # getter for: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mAccount:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$900(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    # getter for: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$1000(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    # getter for: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mPhoneNumber:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$1100(Lcom/google/android/gms/recovery/AccountRecoveryActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/google/android/gms/recovery/RecoveryServerAdapter;->updateRecoveryInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->finish()V

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$AsyncUpdate;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->displayError(Ljava/lang/String;)V

    goto :goto_0
.end method
