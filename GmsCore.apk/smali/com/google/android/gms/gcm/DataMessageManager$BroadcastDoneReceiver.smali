.class public Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DataMessageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/gcm/DataMessageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BroadcastDoneReceiver"
.end annotation


# instance fields
.field private mBroadcastIntent:Landroid/content/Intent;

.field private mBroadcastStartTime:J

.field private mDataMessageManager:Lcom/google/android/gms/gcm/DataMessageManager;

.field private mMsg:Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

.field protected mResult:I

.field protected mUserSerial:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/gcm/DataMessageManager;Landroid/content/Intent;Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;J)V
    .locals 0
    .param p1    # Lcom/google/android/gms/gcm/DataMessageManager;
    .param p2    # Landroid/content/Intent;
    .param p3    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .param p4    # J

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mDataMessageManager:Lcom/google/android/gms/gcm/DataMessageManager;

    iput-object p2, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mBroadcastIntent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mMsg:Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    iput-wide p4, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mBroadcastStartTime:J

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v5, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mBroadcastStartTime:J

    sub-long v3, v0, v5

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->getResultCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mResult:I

    const-string v0, "GCM/DMM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "broadcast done: timeElapsed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms, resultCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mResult:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->getResultData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v7}, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/google/android/gms/gcm/DataMessageManager;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/google/android/gms/gcm/DataMessageManager;->access$000(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mDataMessageManager:Lcom/google/android/gms/gcm/DataMessageManager;

    iget-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mBroadcastIntent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mMsg:Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    iget v5, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mResult:I

    invoke-virtual {p0, v7}, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v6

    iget v7, p0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mUserSerial:I

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/gcm/DataMessageManager;->afterBroadcast(Landroid/content/Intent;Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;JILandroid/os/Bundle;I)V

    return-void
.end method
