.class public Lcom/google/android/gms/panorama/viewer/TileProviderImpl;
.super Ljava/lang/Object;
.source "TileProviderImpl.java"

# interfaces
.implements Lcom/google/android/gms/panorama/viewer/TileProvider;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final decoder:Landroid/graphics/BitmapRegionDecoder;

.field private final lastColumnWidth:I

.field private final lastRowHeight:I

.field private final tileCountX:I

.field private final tileCountY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 14
    .param p1    # Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v5

    :goto_0
    const/4 v10, 0x0

    :try_start_1
    invoke-static {v4, v10}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->decoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v10, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v10}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v10

    int-to-double v10, v10

    const-wide/high16 v12, 0x4080000000000000L

    div-double v2, v10, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    iput v10, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->tileCountX:I

    iget-object v10, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v10}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v10

    rem-int/lit16 v6, v10, 0x200

    if-lez v6, :cond_0

    :goto_1
    iput v6, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->lastColumnWidth:I

    iget-object v10, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v10}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v10

    int-to-double v10, v10

    const-wide/high16 v12, 0x4080000000000000L

    div-double v8, v10, v12

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    iput v10, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->tileCountY:I

    iget-object v10, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v10}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v10

    rem-int/lit16 v7, v10, 0x200

    if-lez v7, :cond_1

    :goto_2
    iput v7, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->lastRowHeight:I

    return-void

    :catch_0
    move-exception v1

    sget-object v10, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->TAG:Ljava/lang/String;

    const-string v11, "File not found"

    invoke-static {v10, v11, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Could not create decoder"

    invoke-direct {v10, v11, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10

    :cond_0
    const/16 v6, 0x200

    goto :goto_1

    :cond_1
    const/16 v7, 0x200

    goto :goto_2
.end method

.method private getImageRegionForTileCoordinate(II)Landroid/graphics/Rect;
    .locals 5
    .param p1    # I
    .param p2    # I

    mul-int/lit16 v1, p1, 0x200

    mul-int/lit16 v3, p2, 0x200

    add-int/lit16 v2, v1, 0x200

    add-int/lit16 v0, v3, 0x200

    iget v4, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->tileCountX:I

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_0

    iget v4, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->lastColumnWidth:I

    rsub-int v4, v4, 0x200

    sub-int/2addr v2, v4

    :cond_0
    iget v4, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->tileCountY:I

    add-int/lit8 v4, v4, -0x1

    if-ne p2, v4, :cond_1

    iget v4, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->lastRowHeight:I

    rsub-int v4, v4, 0x200

    sub-int/2addr v0, v4

    :cond_1
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v1, v3, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method


# virtual methods
.method public getLastColumnWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->lastColumnWidth:I

    return v0
.end method

.method public getLastRowHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->lastRowHeight:I

    return v0
.end method

.method public getScale()F
    .locals 1

    const/high16 v0, 0x3f800000

    return v0
.end method

.method public declared-synchronized getTile(II)Lcom/google/android/gms/panorama/viewer/Tile;
    .locals 7
    .param p1    # I
    .param p2    # I

    monitor-enter p0

    :try_start_0
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v6, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v0, 0x1

    iput-boolean v0, v6, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->getImageRegionForTileCoordinate(II)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/panorama/viewer/Tile;

    const/16 v4, 0x200

    const/16 v5, 0x200

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/panorama/viewer/Tile;-><init>(Landroid/graphics/Bitmap;IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTileCountX()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->tileCountX:I

    return v0
.end method

.method public getTileCountY()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;->tileCountY:I

    return v0
.end method

.method public getTileSize()I
    .locals 1

    const/16 v0, 0x200

    return v0
.end method

.method public setMaximumTextureSize(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
