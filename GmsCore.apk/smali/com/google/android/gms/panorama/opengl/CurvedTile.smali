.class public Lcom/google/android/gms/panorama/opengl/CurvedTile;
.super Ljava/lang/Object;
.source "CurvedTile.java"


# instance fields
.field private indices:Ljava/nio/ShortBuffer;

.field private final numIndices:I

.field private final numVertices:I

.field private texCoords:Ljava/nio/FloatBuffer;

.field public final textureId:I

.field private vertIndex:I

.field private vertices:Ljava/nio/FloatBuffer;


# direct methods
.method public constructor <init>(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v9, 0x0

    iput v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertIndex:I

    iput p1, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->textureId:I

    add-int/lit8 v6, p2, 0x1

    mul-int v9, v6, v6

    iput v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->numVertices:I

    add-int/lit8 v9, v6, -0x1

    add-int/lit8 v10, v6, -0x1

    mul-int/2addr v9, v10

    mul-int/lit8 v9, v9, 0x3

    mul-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->numIndices:I

    invoke-direct {p0}, Lcom/google/android/gms/panorama/opengl/CurvedTile;->initBuffers()V

    const/4 v0, 0x0

    const/4 v8, 0x0

    :goto_0
    add-int/lit8 v9, v6, -0x1

    if-ge v8, v9, :cond_1

    mul-int v2, v8, v6

    const/4 v7, 0x0

    move v1, v0

    :goto_1
    add-int/lit8 v9, v6, -0x1

    if-ge v7, v9, :cond_0

    add-int v5, v2, v7

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v1, 0x1

    add-int/lit8 v10, v5, 0x0

    int-to-short v10, v10

    invoke-virtual {v9, v1, v10}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    add-int/lit8 v1, v0, 0x1

    add-int/lit8 v10, v5, 0x1

    int-to-short v10, v10

    invoke-virtual {v9, v0, v10}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v1, 0x1

    add-int v10, v5, v6

    int-to-short v10, v10

    invoke-virtual {v9, v1, v10}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    add-int/lit8 v1, v0, 0x1

    add-int v10, v5, v6

    int-to-short v10, v10

    invoke-virtual {v9, v0, v10}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v1, 0x1

    add-int/lit8 v10, v5, 0x1

    int-to-short v10, v10

    invoke-virtual {v9, v1, v10}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    add-int/lit8 v1, v0, 0x1

    add-int v10, v5, v6

    add-int/lit8 v10, v10, 0x1

    int-to-short v10, v10

    invoke-virtual {v9, v0, v10}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v8, v8, 0x1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v6, :cond_3

    const/4 v7, 0x0

    move v1, v0

    :goto_3
    if-ge v7, v6, :cond_2

    int-to-float v9, v7

    add-int/lit8 v10, v6, -0x1

    int-to-float v10, v10

    div-float v3, v9, v10

    const/high16 v9, 0x3f800000

    int-to-float v10, v8

    add-int/lit8 v11, v6, -0x1

    int-to-float v11, v11

    div-float/2addr v10, v11

    sub-float v4, v9, v10

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->texCoords:Ljava/nio/FloatBuffer;

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v9, v1, v3}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->texCoords:Ljava/nio/FloatBuffer;

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v9, v0, v4}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v8, v8, 0x1

    move v0, v1

    goto :goto_2

    :cond_3
    return-void
.end method

.method private initBuffers()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->numVertices:I

    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertices:Ljava/nio/FloatBuffer;

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->numVertices:I

    mul-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->texCoords:Ljava/nio/FloatBuffer;

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->numIndices:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    return-void
.end method


# virtual methods
.method public draw(Lcom/google/android/gms/panorama/opengl/Shader;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/panorama/opengl/Shader;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertices:Ljava/nio/FloatBuffer;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/panorama/opengl/Shader;->setVertices(Ljava/nio/FloatBuffer;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->texCoords:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->texCoords:Ljava/nio/FloatBuffer;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/panorama/opengl/Shader;->setTexCoords(Ljava/nio/FloatBuffer;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->numIndices:I

    const/16 v2, 0x1403

    iget-object v3, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->indices:Ljava/nio/ShortBuffer;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    return-void
.end method

.method public putVertex(Lcom/google/android/gms/panorama/opengl/Vertex;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/panorama/opengl/Vertex;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertices:Ljava/nio/FloatBuffer;

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/panorama/opengl/Vertex;->addToBuffer(Ljava/nio/FloatBuffer;I)V

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertIndex:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/CurvedTile;->vertIndex:I

    return-void
.end method
