.class public Lcom/google/android/gms/panorama/opengl/GLTexture;
.super Ljava/lang/Object;
.source "GLTexture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/panorama/opengl/GLTexture$1;,
        Lcom/google/android/gms/panorama/opengl/GLTexture$TextureType;
    }
.end annotation


# instance fields
.field private mTextureIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/panorama/opengl/GLTexture$TextureType;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/panorama/opengl/GLTexture$TextureType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    sget-object v0, Lcom/google/android/gms/panorama/opengl/GLTexture$1;->$SwitchMap$com$google$android$gms$panorama$opengl$GLTexture$TextureType:[I

    invoke-virtual {p1}, Lcom/google/android/gms/panorama/opengl/GLTexture$TextureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/google/android/gms/panorama/opengl/GLTexture;->createStandardTexture()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/google/android/gms/panorama/opengl/GLTexture;->createStandardTexture()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/android/gms/panorama/opengl/GLTexture;->createNNTexture()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static createNNTexture()I
    .locals 6

    const v5, 0x812f

    const/4 v1, 0x1

    const/high16 v4, 0x46180000

    const/4 v3, 0x0

    const/16 v2, 0xde1

    new-array v0, v1, [I

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    aget v1, v0, v3

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    const/16 v1, 0x2801

    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const/16 v1, 0x2800

    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const/16 v1, 0x2802

    invoke-static {v2, v1, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2803

    invoke-static {v2, v1, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    aget v1, v0, v3

    return v1
.end method

.method public static createStandardTexture()I
    .locals 6

    const v5, 0x812f

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xde1

    new-array v0, v1, [I

    invoke-static {v1, v0, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    aget v1, v0, v4

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    const/16 v1, 0x2801

    const/high16 v2, 0x46180000

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const/16 v1, 0x2800

    const v2, 0x46180400

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const/16 v1, 0x2802

    invoke-static {v3, v1, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2803

    invoke-static {v3, v1, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    aget v1, v0, v4

    return v1
.end method


# virtual methods
.method public bind(Lcom/google/android/gms/panorama/opengl/Shader;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/panorama/opengl/Shader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    if-gez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/panorama/opengl/OpenGLException;

    const-string v1, "Trying to bind without a loaded texture"

    invoke-direct {v0, v1}, Lcom/google/android/gms/panorama/opengl/OpenGLException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v0, 0xde1

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    const-string v0, "glBindTexture"

    invoke-static {v0}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->logError(Ljava/lang/String;)V

    return-void
.end method

.method public loadBitmap(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    const v5, 0x812f

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xde1

    new-array v0, v1, [I

    invoke-static {v1, v0, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    aget v1, v0, v4

    iput v1, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/GLTexture;->mTextureIndex:I

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    const/16 v1, 0x2801

    const/high16 v2, 0x46180000

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const/16 v1, 0x2800

    const v2, 0x46180400

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const/16 v1, 0x2802

    invoke-static {v3, v1, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2803

    invoke-static {v3, v1, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    invoke-static {v3, v4, p1, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    const-string v1, "Texture : loadBitmap"

    invoke-static {v1}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->logError(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    return-void
.end method
