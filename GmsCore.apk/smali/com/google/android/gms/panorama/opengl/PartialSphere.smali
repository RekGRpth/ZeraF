.class public Lcom/google/android/gms/panorama/opengl/PartialSphere;
.super Lcom/google/android/gms/panorama/opengl/DrawableGL;
.source "PartialSphere.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private curvedTiles:[[Lcom/google/android/gms/panorama/opengl/CurvedTile;

.field private final image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

.field private final tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/panorama/viewer/PanoramaImage;F)V
    .locals 2
    .param p1    # Lcom/google/android/gms/panorama/viewer/PanoramaImage;
    .param p2    # F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/opengl/DrawableGL;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-virtual {p1}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getTileProvider()Lcom/google/android/gms/panorama/viewer/TileProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/opengl/PartialSphere;->loadTextures()Z

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->init()V

    invoke-direct {p0, p2}, Lcom/google/android/gms/panorama/opengl/PartialSphere;->generateGeometry(F)V

    return-void
.end method

.method private generateGeometry(F)V
    .locals 42
    .param p1    # F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getTileSizeRad()F

    move-result v39

    const v40, 0x3df5c28f

    div-float v39, v39, v40

    invoke-static/range {v39 .. v39}, Landroid/util/FloatMath;->ceil(F)F

    move-result v39

    move/from16 v0, v39

    float-to-int v0, v0

    move/from16 v26, v0

    sget-object v39, Lcom/google/android/gms/panorama/opengl/PartialSphere;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "tesselation factor: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileCountY()I

    move-result v39

    mul-int v39, v39, v26

    add-int/lit8 v14, v39, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileCountX()I

    move-result v39

    mul-int v39, v39, v26

    add-int/lit8 v15, v39, 0x1

    mul-int v16, v14, v15

    add-int/lit8 v39, v14, -0x1

    add-int/lit8 v40, v15, -0x1

    mul-int v39, v39, v40

    mul-int/lit8 v13, v39, 0x6

    const/16 v39, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v39

    invoke-virtual {v0, v1, v13, v2}, Lcom/google/android/gms/panorama/opengl/PartialSphere;->initGeometry(IIZ)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getTileSizeRad()F

    move-result v39

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v40, v0

    div-float v4, v39, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getTileSizeRad()F

    move-result v39

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v40, v0

    div-float v6, v39, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getLastRowHeightRad()F

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getTileSizeRad()F

    move-result v40

    div-float v39, v39, v40

    mul-float v5, v4, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getLastColumnWidthRad()F

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getTileSizeRad()F

    move-result v40

    div-float v39, v39, v40

    mul-float v7, v6, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getOffsetTopRad()F

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getPanoHeightRad()F

    move-result v40

    add-float v39, v39, v40

    const v40, 0x3fc90fdb

    sub-float v17, v39, v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getOffsetLeftRad()F

    move-result v39

    move/from16 v0, v39

    neg-float v0, v0

    move/from16 v39, v0

    const v40, 0x40490fdb

    sub-float v18, v39, v40

    sub-int v39, v15, v26

    add-int/lit8 v23, v39, -0x1

    move/from16 v22, v26

    const/16 v32, 0x0

    const/16 v27, 0x0

    const v19, 0x3fc90fdb

    filled-new-array {v15, v14}, [I

    move-result-object v39

    const-class v40, Lcom/google/android/gms/panorama/opengl/Vertex;

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, [[Lcom/google/android/gms/panorama/opengl/Vertex;

    const/16 v21, 0x0

    :goto_0
    move/from16 v0, v21

    if-ge v0, v14, :cond_3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v39, v0

    mul-float v39, v39, v5

    sub-float v11, v39, v17

    :goto_1
    const/4 v8, 0x0

    :goto_2
    if-ge v8, v15, :cond_2

    int-to-float v0, v8

    move/from16 v39, v0

    mul-float v12, v39, v6

    move/from16 v0, v23

    if-le v8, v0, :cond_0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v39, v0

    mul-float v20, v39, v6

    sub-int v39, v8, v23

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    mul-float v39, v39, v7

    add-float v12, v20, v39

    :cond_0
    const v39, 0x3fc90fdb

    sub-float v39, v12, v39

    sub-float v12, v39, v18

    invoke-static {v11}, Landroid/util/FloatMath;->sin(F)F

    move-result v24

    invoke-static {v12}, Landroid/util/FloatMath;->sin(F)F

    move-result v25

    invoke-static {v11}, Landroid/util/FloatMath;->cos(F)F

    move-result v9

    invoke-static {v12}, Landroid/util/FloatMath;->cos(F)F

    move-result v10

    mul-float v39, v9, v10

    mul-float v34, v39, p1

    mul-float v36, v24, p1

    mul-float v39, v9, v25

    mul-float v38, v39, p1

    aget-object v39, v33, v8

    new-instance v40, Lcom/google/android/gms/panorama/opengl/Vertex;

    move-object/from16 v0, v40

    move/from16 v1, v34

    move/from16 v2, v36

    move/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/panorama/opengl/Vertex;-><init>(FFF)V

    aput-object v40, v39, v21

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_1
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v39, v0

    mul-float v39, v39, v4

    sub-float v39, v39, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getTileSizeRad()F

    move-result v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->image:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getLastRowHeightRad()F

    move-result v41

    sub-float v40, v40, v41

    sub-float v11, v39, v40

    goto :goto_1

    :cond_2
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileCountX()I

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileCountY()I

    move-result v29

    filled-new-array/range {v28 .. v29}, [I

    move-result-object v39

    const-class v40, Lcom/google/android/gms/panorama/opengl/CurvedTile;

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, [[Lcom/google/android/gms/panorama/opengl/CurvedTile;

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/panorama/opengl/PartialSphere;->curvedTiles:[[Lcom/google/android/gms/panorama/opengl/CurvedTile;

    const/16 v36, 0x0

    const/16 v31, 0x0

    :goto_3
    move/from16 v0, v31

    move/from16 v1, v29

    if-ge v0, v1, :cond_7

    const/16 v34, 0x0

    const/16 v30, 0x0

    :goto_4
    move/from16 v0, v30

    move/from16 v1, v28

    if-ge v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->curvedTiles:[[Lcom/google/android/gms/panorama/opengl/CurvedTile;

    move-object/from16 v39, v0

    aget-object v39, v39, v30

    new-instance v40, Lcom/google/android/gms/panorama/opengl/CurvedTile;

    mul-int v41, v30, v29

    add-int v41, v41, v31

    move-object/from16 v0, v40

    move/from16 v1, v41

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/panorama/opengl/CurvedTile;-><init>(II)V

    aput-object v40, v39, v31

    const/16 v37, 0x0

    :goto_5
    add-int/lit8 v39, v26, 0x1

    move/from16 v0, v37

    move/from16 v1, v39

    if-ge v0, v1, :cond_5

    const/16 v35, 0x0

    :goto_6
    add-int/lit8 v39, v26, 0x1

    move/from16 v0, v35

    move/from16 v1, v39

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->curvedTiles:[[Lcom/google/android/gms/panorama/opengl/CurvedTile;

    move-object/from16 v39, v0

    aget-object v39, v39, v30

    aget-object v39, v39, v31

    add-int v40, v34, v35

    aget-object v40, v33, v40

    add-int v41, v36, v37

    aget-object v40, v40, v41

    invoke-virtual/range {v39 .. v40}, Lcom/google/android/gms/panorama/opengl/CurvedTile;->putVertex(Lcom/google/android/gms/panorama/opengl/Vertex;)V

    add-int/lit8 v35, v35, 0x1

    goto :goto_6

    :cond_4
    add-int/lit8 v37, v37, 0x1

    goto :goto_5

    :cond_5
    add-int v34, v34, v26

    add-int/lit8 v30, v30, 0x1

    goto :goto_4

    :cond_6
    add-int v36, v36, v26

    add-int/lit8 v31, v31, 0x1

    goto :goto_3

    :cond_7
    return-void
.end method


# virtual methods
.method public drawObject([F)V
    .locals 6
    .param p1    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    invoke-virtual {v4}, Lcom/google/android/gms/panorama/opengl/Shader;->bind()V

    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->curvedTiles:[[Lcom/google/android/gms/panorama/opengl/CurvedTile;

    array-length v4, v4

    if-ge v2, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->curvedTiles:[[Lcom/google/android/gms/panorama/opengl/CurvedTile;

    aget-object v0, v4, v2

    const/4 v3, 0x0

    :goto_1
    array-length v4, v0

    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mTextures:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    iget v5, v1, Lcom/google/android/gms/panorama/opengl/CurvedTile;->textureId:I

    if-le v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mTextures:Ljava/util/Vector;

    iget v5, v1, Lcom/google/android/gms/panorama/opengl/CurvedTile;->textureId:I

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/panorama/opengl/GLTexture;

    iget-object v5, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/panorama/opengl/GLTexture;->bind(Lcom/google/android/gms/panorama/opengl/Shader;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    invoke-virtual {v4, p1}, Lcom/google/android/gms/panorama/opengl/Shader;->setTransform([F)V

    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/panorama/opengl/CurvedTile;->draw(Lcom/google/android/gms/panorama/opengl/Shader;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public loadTextures()Z
    .locals 9

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    if-nez v6, :cond_1

    sget-object v6, Lcom/google/android/gms/panorama/opengl/PartialSphere;->TAG:Ljava/lang/String;

    const-string v7, "tile provider is null. Cannot load textures"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v6, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mTextures:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->clear()V

    const/4 v3, 0x0

    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v6}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileCountX()I

    move-result v6

    if-ge v3, v6, :cond_3

    const/4 v4, 0x0

    :goto_2
    iget-object v6, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v6}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileCountY()I

    move-result v6

    if-ge v4, v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    iget-object v7, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v7}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileCountY()I

    move-result v7

    sub-int/2addr v7, v4

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v3, v7}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTile(II)Lcom/google/android/gms/panorama/viewer/Tile;

    move-result-object v2

    iget-object v6, v2, Lcom/google/android/gms/panorama/viewer/Tile;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-ltz v6, :cond_0

    iget-object v6, v2, Lcom/google/android/gms/panorama/viewer/Tile;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-ltz v6, :cond_0

    new-instance v1, Lcom/google/android/gms/panorama/opengl/GLTexture;

    sget-object v6, Lcom/google/android/gms/panorama/opengl/GLTexture$TextureType;->Standard:Lcom/google/android/gms/panorama/opengl/GLTexture$TextureType;

    invoke-direct {v1, v6}, Lcom/google/android/gms/panorama/opengl/GLTexture;-><init>(Lcom/google/android/gms/panorama/opengl/GLTexture$TextureType;)V

    :try_start_0
    iget-object v6, v2, Lcom/google/android/gms/panorama/viewer/Tile;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v6}, Lcom/google/android/gms/panorama/opengl/GLTexture;->loadBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Lcom/google/android/gms/panorama/opengl/OpenGLException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v6, p0, Lcom/google/android/gms/panorama/opengl/PartialSphere;->mTextures:Ljava/util/Vector;

    invoke-virtual {v6, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    sget-object v6, Lcom/google/android/gms/panorama/opengl/PartialSphere;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not load texture ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v5, 0x1

    goto :goto_0
.end method
