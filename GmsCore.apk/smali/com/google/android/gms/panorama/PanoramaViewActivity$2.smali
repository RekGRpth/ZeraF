.class Lcom/google/android/gms/panorama/PanoramaViewActivity$2;
.super Ljava/lang/Object;
.source "PanoramaViewActivity.java"

# interfaces
.implements Lcom/google/android/gms/panorama/util/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/panorama/PanoramaViewActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/panorama/util/Callback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/panorama/PanoramaViewActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;->this$0:Lcom/google/android/gms/panorama/PanoramaViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallback(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;->this$0:Lcom/google/android/gms/panorama/PanoramaViewActivity;

    # getter for: Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->access$100(Lcom/google/android/gms/panorama/PanoramaViewActivity;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;->this$0:Lcom/google/android/gms/panorama/PanoramaViewActivity;

    # getter for: Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->access$100(Lcom/google/android/gms/panorama/PanoramaViewActivity;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;->this$0:Lcom/google/android/gms/panorama/PanoramaViewActivity;

    # getter for: Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->access$100(Lcom/google/android/gms/panorama/PanoramaViewActivity;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;->this$0:Lcom/google/android/gms/panorama/PanoramaViewActivity;

    # getter for: Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->access$100(Lcom/google/android/gms/panorama/PanoramaViewActivity;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0
.end method

.method public bridge synthetic onCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;->onCallback(Ljava/lang/Boolean;)V

    return-void
.end method
