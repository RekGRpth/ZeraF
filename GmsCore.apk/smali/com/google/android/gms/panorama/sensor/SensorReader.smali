.class public Lcom/google/android/gms/panorama/sensor/SensorReader;
.super Ljava/lang/Object;
.source "SensorReader.java"


# instance fields
.field private accelFilterCoefficient:F

.field private acceleration:Lcom/google/android/gms/panorama/math/Vector3;

.field private angularVelocitySqrRad:F

.field private ekf:Lcom/google/android/gms/panorama/sensor/OrientationEKF;

.field private filterInitialized:Z

.field private filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

.field private geomagnetic:[F

.field private gyroBias:[F

.field private gyroLastTimestamp:F

.field private imuOrientationDeg:F

.field private numGyroSamples:I

.field private rotationAccumulator:[F

.field private final sensorEventListener:Landroid/hardware/SensorEventListener;

.field private sensorManager:Landroid/hardware/SensorManager;

.field private sensorVelocityCallback:Lcom/google/android/gms/panorama/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private tForm:[F

.field private useEkf:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->useEkf:Z

    iput-object v4, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->acceleration:Lcom/google/android/gms/panorama/math/Vector3;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iput-boolean v3, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filterInitialized:Z

    const v0, 0x3e19999a

    iput v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->accelFilterCoefficient:F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->geomagnetic:[F

    iput v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->gyroLastTimestamp:F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->rotationAccumulator:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->gyroBias:[F

    iput v3, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->numGyroSamples:I

    new-instance v0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->ekf:Lcom/google/android/gms/panorama/sensor/OrientationEKF;

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->tForm:[F

    const/high16 v0, 0x42b40000

    iput v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->imuOrientationDeg:F

    iput-object v4, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorVelocityCallback:Lcom/google/android/gms/panorama/util/Callback;

    iput v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->angularVelocitySqrRad:F

    new-instance v0, Lcom/google/android/gms/panorama/sensor/SensorReader$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/panorama/sensor/SensorReader$1;-><init>(Lcom/google/android/gms/panorama/sensor/SensorReader;)V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorEventListener:Landroid/hardware/SensorEventListener;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/gms/panorama/sensor/SensorReader;Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;
    .param p1    # Landroid/hardware/SensorEvent;

    invoke-direct {p0, p1}, Lcom/google/android/gms/panorama/sensor/SensorReader;->updateAccelerometerState(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/panorama/sensor/SensorReader;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->useEkf:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/panorama/sensor/SensorReader;)Lcom/google/android/gms/panorama/sensor/OrientationEKF;
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->ekf:Lcom/google/android/gms/panorama/sensor/OrientationEKF;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/panorama/sensor/SensorReader;)[F
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->geomagnetic:[F

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/panorama/sensor/SensorReader;)[F
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->gyroBias:[F

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gms/panorama/sensor/SensorReader;)F
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iget v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->angularVelocitySqrRad:F

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/gms/panorama/sensor/SensorReader;F)F
    .locals 0
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;
    .param p1    # F

    iput p1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->angularVelocitySqrRad:F

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/gms/panorama/sensor/SensorReader;)Lcom/google/android/gms/panorama/util/Callback;
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorVelocityCallback:Lcom/google/android/gms/panorama/util/Callback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gms/panorama/sensor/SensorReader;Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/panorama/sensor/SensorReader;
    .param p1    # Landroid/hardware/SensorEvent;

    invoke-direct {p0, p1}, Lcom/google/android/gms/panorama/sensor/SensorReader;->updateGyroState(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method private updateAccelerometerState(Landroid/hardware/SensorEvent;)V
    .locals 9
    .param p1    # Landroid/hardware/SensorEvent;

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->acceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v7

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v6

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/panorama/math/Vector3;->set(FFF)V

    iget-boolean v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filterInitialized:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v7

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v6

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/panorama/math/Vector3;->set(FFF)V

    iput-boolean v6, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filterInitialized:Z

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->accelFilterCoefficient:F

    const/high16 v2, 0x3f800000

    sub-float v1, v2, v0

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v7

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget v4, v4, Lcom/google/android/gms/panorama/math/Vector3;->x:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iput v3, v2, Lcom/google/android/gms/panorama/math/Vector3;->x:F

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v6

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget v4, v4, Lcom/google/android/gms/panorama/math/Vector3;->y:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iput v3, v2, Lcom/google/android/gms/panorama/math/Vector3;->y:F

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v8

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filteredAcceleration:Lcom/google/android/gms/panorama/math/Vector3;

    iget v4, v4, Lcom/google/android/gms/panorama/math/Vector3;->z:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iput v3, v2, Lcom/google/android/gms/panorama/math/Vector3;->z:F

    goto :goto_0
.end method

.method private updateGyroState(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1    # Landroid/hardware/SensorEvent;

    iget v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->gyroLastTimestamp:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    iget-wide v1, p1, Landroid/hardware/SensorEvent;->timestamp:J

    long-to-float v1, v1

    iget v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->gyroLastTimestamp:F

    sub-float/2addr v1, v2

    const v2, 0x3089705f

    mul-float v0, v1, v2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->rotationAccumulator:[F

    const/4 v2, 0x0

    aget v3, v1, v2

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    aput v3, v1, v2

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->rotationAccumulator:[F

    const/4 v2, 0x1

    aget v3, v1, v2

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    aput v3, v1, v2

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->rotationAccumulator:[F

    const/4 v2, 0x2

    aget v3, v1, v2

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    aput v3, v1, v2

    iget v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->numGyroSamples:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->numGyroSamples:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-wide v1, p1, Landroid/hardware/SensorEvent;->timestamp:J

    long-to-float v1, v1

    iput v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->gyroLastTimestamp:F

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public getFilterOutput()[F
    .locals 13

    const/high16 v3, 0x3f800000

    const/16 v8, 0x10

    const/4 v4, 0x0

    const/4 v1, 0x0

    new-array v0, v8, [F

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->ekf:Lcom/google/android/gms/panorama/sensor/OrientationEKF;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->getGLMatrix()[D

    move-result-object v12

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v8, :cond_0

    aget-wide v6, v12, v11

    double-to-float v2, v6

    aput v2, v0, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_0
    const/high16 v2, 0x42b40000

    move v5, v4

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    new-array v5, v8, [F

    invoke-static {v5, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    iget v7, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->imuOrientationDeg:F

    move v6, v1

    move v8, v4

    move v9, v4

    move v10, v3

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v3, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->tForm:[F

    move v4, v1

    move v6, v1

    move-object v7, v0

    move v8, v1

    invoke-static/range {v3 .. v8}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->tForm:[F

    return-object v1
.end method

.method public start(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v5, 0x1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_1

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v6, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v1, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->imuOrientationDeg:F

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Model is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Nexus 7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x42b40000

    iput v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->imuOrientationDeg:F

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Camera orientation is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    :cond_1
    const-string v1, "sensor"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorEventListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v3, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorEventListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorEventListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iput-boolean v6, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->filterInitialized:Z

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/SensorReader;->sensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    return-void
.end method
