.class public final Lcom/google/android/gms/plus/plusone/PreviewView;
.super Landroid/widget/FrameLayout;
.source "PreviewView.java"


# instance fields
.field private mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

.field private mPlusClient:Lcom/google/android/gms/plus/PlusClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private addArticlePreviewLayout()Landroid/view/View;
    .locals 13

    const/16 v12, 0x8

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f04001e

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v8, 0x7f0a0061

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v8, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2, v8}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    const v8, 0x7f0a0062

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f0a0064

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v8, 0x7f0a0063

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v8, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v4, v8}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    iget-object v8, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v8}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/plus/plusone/PreviewView;->removeTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v8}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/plus/plusone/PreviewView;->removeTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v8}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v2, v12}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v4, v11}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v3}, Lcom/google/android/gms/plus/internal/PlusContent$Image;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->loadFromUri(Landroid/net/Uri;)V

    :goto_2
    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    return-object v5

    :cond_0
    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v4, v12}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method private addPhotoPreviewLayout()Landroid/view/View;
    .locals 10

    const/16 v9, 0x8

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f04002e

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0a008d

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0a008e

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/plus/plusone/PreviewView;->removeTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v1, v8}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusContent$Image;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->loadFromUri(Landroid/net/Uri;)V

    :goto_1
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    return-object v2

    :cond_0
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v9}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private addVideoPreviewLayout()Landroid/view/View;
    .locals 10

    const/16 v9, 0x8

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040032

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0a009a

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0a009b

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/plus/plusone/PreviewView;->removeTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v1, v8}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusContent$Image;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->loadFromUri(Landroid/net/Uri;)V

    :goto_1
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    return-object v2

    :cond_0
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v9}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private static removeTags(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private updatePreviewLayout()Z
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->removeAllViews()V

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v3, "article"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->addArticlePreviewLayout()Landroid/view/View;

    move-result-object v1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/PreviewView;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->requestLayout()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const-string v3, "video"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->addVideoPreviewLayout()Landroid/view/View;

    move-result-object v1

    goto :goto_1

    :cond_2
    const-string v3, "photo"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->addPhotoPreviewLayout()Landroid/view/View;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v3, "PreviewView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported content type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public initialize(Lcom/google/android/gms/plus/PlusClient;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/PlusClient;

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    return-void
.end method

.method public setLinkPreview(Lcom/google/android/gms/plus/data/plusone/LinkPreview;)Z
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    const-string v1, "Call initialize first"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PreviewView;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PreviewView;->updatePreviewLayout()Z

    move-result v0

    return v0
.end method
