.class public final Lcom/google/android/gms/plus/broker/PosAgent;
.super Ljava/lang/Object;
.source "PosAgent.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApi:Lcom/google/android/gms/plus/service/pos/PlusonesApi;

.field private final mRpcApi:Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/broker/PosAgent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/pos/PlusonesApi;Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/service/pos/PlusonesApi;
    .param p2    # Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/broker/PosAgent;->mApi:Lcom/google/android/gms/plus/service/pos/PlusonesApi;

    iput-object p2, p0, Lcom/google/android/gms/plus/broker/PosAgent;->mRpcApi:Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;

    return-void
.end method

.method private static checkAccount(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v7

    :cond_0
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getRequestedAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "https://www.googleapis.com/auth/pos"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->addGrantedScope(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/gms/plus/broker/DataBroker;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/broker/DataBroker;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/internal/GmsClient;->GOOGLE_PLUS_REQUIRED_FEATURES:[Ljava/lang/String;

    invoke-static {p0, v1, v0, p1, v2}, Lcom/google/android/gms/plus/service/PlusOperationsUtils;->checkSignUp(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    move-result-object v6

    iget v1, v6, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;->mStatusCode:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v7, v1

    goto :goto_0

    :cond_1
    move v1, v7

    goto :goto_1
.end method

.method private cleanUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, ""

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private toPlusones(Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/Plusones;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    new-instance v0, Lcom/google/android/gms/plus/service/pos/Plusones;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/pos/Plusones;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/pos/Plusones;->getValues()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/pos/Plusones;->getValues()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "isSetByViewer"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method


# virtual methods
.method public deletePlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 28
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/plus/broker/PlusCache;->getPlusOne(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    move-result-object v23

    if-nez v23, :cond_0

    const/4 v11, 0x0

    :goto_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/plus/broker/ContainerParam;->get(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/broker/PosAgent;->mRpcApi:Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/broker/PosAgent;->cleanUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-string v9, "native:android_app"

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v3 .. v12}, Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;->deleteBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/rpc/Plusones;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/gms/plus/service/rpc/Plusones;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/service/rpc/PlusoneResult;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/rpc/PlusoneResult;->getResult()Lcom/google/android/gms/plus/service/rpc/Plusone;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->isSetByViewer(Lcom/google/android/gms/plus/service/rpc/Plusone;)Z

    move-result v26

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-virtual {v3, v4, v0, v1}, Lcom/google/android/gms/plus/broker/PlusCache;->updatePlusOne(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v20

    if-nez v20, :cond_1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/broker/PosAgent;->toPlusones(Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v16

    :goto_1
    sget-object v27, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getRequestedAccountName()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v12, p1

    move-object/from16 v15, p3

    move-object/from16 v19, p4

    invoke-static/range {v12 .. v19}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->buildPlusOne(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/pos/Plusones;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/data/plusone/PlusOne;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->asBundle()Landroid/os/Bundle;

    move-result-object v22

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    :cond_0
    invoke-virtual/range {v23 .. v23}, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->getClickDelta()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0

    :cond_1
    move-object/from16 v16, v20

    goto :goto_1

    :catch_0
    move-exception v21

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/plus/broker/PlusCache;->removePlusOne(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    const-string v4, "Network error deleting +1."

    move-object/from16 v0, v21

    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    throw v21
.end method

.method public getPlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/util/Pair;
    .locals 23
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/plus/broker/ContainerParam;->get(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v18

    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/plus/broker/PosAgent;->checkAccount(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v16

    const/16 v19, 0x0

    const/4 v3, 0x1

    move/from16 v0, p5

    if-eq v0, v3, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/plus/broker/PlusCache;->getPlusOne(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    move-result-object v19

    move-object/from16 v20, v19

    :goto_0
    if-nez v20, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/broker/PosAgent;->mApi:Lcom/google/android/gms/plus/service/pos/PlusonesApi;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/broker/PosAgent;->cleanUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-wide/16 v8, 0x4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    const-string v10, "native:android_app"

    const/4 v11, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/plus/service/pos/PlusonesApi;->getBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v21

    new-instance v19, Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/gms/plus/broker/PlusOneMetadata;-><init>(Lcom/google/android/gms/plus/service/pos/Plusones;J)V
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/gms/plus/broker/PlusCache;->updatePlusOne(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/broker/PlusOneMetadata;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getRequestedAccountName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->getPlusones()Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v8, p1

    move/from16 v10, v16

    move-object/from16 v11, p3

    move-object/from16 v15, p4

    invoke-static/range {v8 .. v15}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->buildPlusOne(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/pos/Plusones;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/data/plusone/PlusOne;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->asBundle()Landroid/os/Bundle;

    move-result-object v22

    sget-object v3, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    move-object/from16 v0, v22

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_1
    .catch Lcom/android/volley/VolleyError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    return-object v3

    :cond_0
    :try_start_2
    new-instance v19, Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->getPlusones()Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, v19

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/gms/plus/broker/PlusOneMetadata;-><init>(Lcom/google/android/gms/plus/service/pos/Plusones;J)V
    :try_end_2
    .catch Lcom/android/volley/VolleyError; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v17

    move-object/from16 v19, v20

    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/plus/broker/PlusCache;->removePlusOne(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    invoke-virtual/range {v17 .. v17}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    throw v17

    :catch_1
    move-exception v17

    goto :goto_2

    :cond_2
    move-object/from16 v20, v19

    goto/16 :goto_0
.end method

.method public getSignUpState(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/data/plusone/SignUpState;
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/broker/ContainerParam;->get(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PosAgent;->mApi:Lcom/google/android/gms/plus/service/pos/PlusonesApi;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "native:android_app"

    const/4 v5, 0x0

    move-object/from16 v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/service/pos/PlusonesApi;->getSignupStateBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/pos/Getsignupstate;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/plus/service/pos/Getsignupstate;->getValues()Landroid/content/ContentValues;

    move-result-object v13

    new-instance v0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;

    const-string v1, "profile_image_url"

    invoke-virtual {v13, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;-><init>(Ljava/lang/String;)V

    const v1, 0x7f090030

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->setSize(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;->build()Ljava/lang/String;

    move-result-object v12

    const-string v0, "profile_image_url"

    invoke-virtual {v13, v0, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "account_name"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0, v13}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->updatePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->queryPlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v6

    invoke-virtual {v6}, Landroid/database/AbstractWindowedCursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    const-string v1, "Hit data removed condition"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_0
    throw v0

    :cond_1
    :try_start_1
    const-string v0, "display_name"

    invoke-virtual {v6, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v0, "profile_image_url"

    invoke-virtual {v6, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v0, "signedUp"

    invoke-virtual {v6, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/database/AbstractWindowedCursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v8, 0x1

    :goto_0
    new-instance v0, Lcom/google/android/gms/plus/data/plusone/SignUpState;

    invoke-direct {v0, v7, v9, v8}, Lcom/google/android/gms/plus/data/plusone/SignUpState;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_2
    return-object v0

    :cond_3
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public insertPlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 28
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/plus/broker/PlusCache;->getPlusOne(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    move-result-object v26

    if-nez v26, :cond_0

    const/16 v18, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/plus/broker/ContainerParam;->get(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/broker/PosAgent;->mApi:Lcom/google/android/gms/plus/service/pos/PlusonesApi;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/broker/PosAgent;->cleanUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const-string v12, "native:android_app"

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v3, p2

    move-object/from16 v10, p4

    invoke-virtual/range {v2 .. v22}, Lcom/google/android/gms/plus/service/pos/PlusonesApi;->insertBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/android/gms/plus/service/pos/Metadata;)Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v27

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-static/range {v27 .. v27}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->isSetByViewer(Lcom/google/android/gms/plus/service/pos/Plusones;)Z

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/gms/plus/broker/PlusCache;->updatePlusOne(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v23

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getRequestedAccountName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    if-nez v23, :cond_1

    move-object/from16 v11, v27

    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v7, p1

    move-object/from16 v10, p3

    move-object/from16 v14, p5

    invoke-static/range {v7 .. v14}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->buildPlusOne(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/pos/Plusones;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/data/plusone/PlusOne;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->asBundle()Landroid/os/Bundle;

    move-result-object v25

    sget-object v2, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    move-object/from16 v0, v25

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    :cond_0
    invoke-virtual/range {v26 .. v26}, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->getClickDelta()Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_0

    :cond_1
    move-object/from16 v11, v23

    goto :goto_1

    :catch_0
    move-exception v24

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/plus/broker/PlusCache;->removePlusOne(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/gms/plus/broker/PosAgent;->TAG:Ljava/lang/String;

    const-string v3, "Network error inserting +1."

    move-object/from16 v0, v24

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    throw v24
.end method
