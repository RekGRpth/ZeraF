.class public final Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;
.super Ljava/lang/Object;
.source "PlusWhitelistedAgent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent$DisplayNameComparator;
    }
.end annotation


# instance fields
.field private final mActivitiesApi:Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;

.field private final mApplicationsApi:Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;

.field private final mAudiencesApi:Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;

.field private final mRpcApi:Lcom/google/android/gms/plus/service/whitelisted/RpcApi;

.field private final mWhitelistedMomentsApi:Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;Lcom/google/android/gms/plus/service/whitelisted/RpcApi;Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;
    .param p2    # Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;
    .param p3    # Lcom/google/android/gms/plus/service/whitelisted/RpcApi;
    .param p4    # Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;
    .param p5    # Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mActivitiesApi:Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;

    iput-object p2, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mAudiencesApi:Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;

    iput-object p3, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mRpcApi:Lcom/google/android/gms/plus/service/whitelisted/RpcApi;

    iput-object p4, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mApplicationsApi:Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;

    iput-object p5, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mWhitelistedMomentsApi:Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;

    return-void
.end method

.method private static addPreviewContentValues(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/whitelisted/Activity;)V
    .locals 6
    .param p0    # Landroid/content/ContentValues;
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/Activity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/whitelisted/Activity;->getPlusObject()Lcom/google/android/gms/plus/service/whitelisted/PlusObject;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v4, Lcom/android/volley/VolleyError;

    const-string v5, "Link preview requires object."

    invoke-direct {v4, v5}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/whitelisted/PlusObject;->getAttachments()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    new-instance v4, Lcom/android/volley/VolleyError;

    const-string v5, "Link preview requires object.attachments[]."

    invoke-direct {v4, v5}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;

    if-nez v0, :cond_3

    new-instance v4, Lcom/android/volley/VolleyError;

    const-string v5, "Link preview requires attachments."

    invoke-direct {v4, v5}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->getValues()Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->getImage()Lcom/google/android/gms/plus/service/whitelisted/Image;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/whitelisted/Image;->getValues()Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    :cond_4
    return-void
.end method

.method private getAcceptLanguage(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, ""

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    const-string v4, "-"

    goto :goto_1
.end method

.method private getApplicationIconUrl(Lcom/google/android/gms/plus/service/whitelisted/Application;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/Application;

    const/4 v2, 0x0

    if-nez p1, :cond_1

    move-object v1, v2

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/whitelisted/Application;->getIcon()Lcom/google/android/gms/plus/service/whitelisted/Icon;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Icon;->getUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    move-object v1, v2

    goto :goto_0

    :cond_3
    const-string v2, "//"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getEmptyLinkPreview(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3
    .param p0    # Ljava/lang/String;

    new-instance v0, Landroid/content/ContentValues;

    sget-object v2, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->PROJECTION:[Ljava/lang/String;

    array-length v2, v2

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->PROJECTION:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    sget-object v2, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->PROJECTION:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "url"

    invoke-virtual {v0, v2, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getInstalledApp(Landroid/content/pm/PackageManager;Lcom/google/android/gms/plus/service/whitelisted/Application;)Landroid/content/pm/ApplicationInfo;
    .locals 6
    .param p1    # Landroid/content/pm/PackageManager;
    .param p2    # Lcom/google/android/gms/plus/service/whitelisted/Application;

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/whitelisted/Application;->getClients()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/whitelisted/PlusAppClient;

    const-string v4, "android"

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/whitelisted/PlusAppClient;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/whitelisted/PlusAppClient;->getAndroidPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :goto_1
    return-object v4

    :catch_0
    move-exception v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static updatePreview(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/broker/PlusCache;->updatePreview(Ljava/lang/String;Landroid/content/ContentValues;)V

    return-void
.end method


# virtual methods
.method public disconnectSource(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mWhitelistedMomentsApi:Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;->removeByApplicationBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mApplicationsApi:Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;->disconnectBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getAudience(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/ArrayList;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mAudiencesApi:Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->getAcceptLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "me"

    move-object v1, p2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;->listBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/whitelisted/AudiencesFeed;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/AudienceMemberConversions;->toAudienceMembers(Lcom/google/android/gms/plus/service/whitelisted/AudiencesFeed;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getLinkPreview(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;
    .locals 30
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->getEmptyLinkPreview(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v28

    new-instance v26, Lcom/google/android/gms/plus/service/whitelisted/Attachments;

    invoke-direct/range {v26 .. v26}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;-><init>()V

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->getValues()Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "url"

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v14, Lcom/google/android/gms/plus/service/whitelisted/PlusObject;

    invoke-direct {v14}, Lcom/google/android/gms/plus/service/whitelisted/PlusObject;-><init>()V

    const-string v3, "attachments"

    move-object/from16 v0, v27

    invoke-virtual {v14, v3, v0}, Lcom/google/android/gms/plus/service/whitelisted/PlusObject;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mActivitiesApi:Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;

    const-string v5, "me"

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v4, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v3 .. v25}, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->insertBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/PlusObject;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Actor;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Acl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/whitelisted/Activity;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->addPreviewContentValues(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/whitelisted/Activity;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->updatePreview(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)V

    new-instance v3, Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    move-object/from16 v0, v28

    invoke-direct {v3, v0}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;-><init>(Landroid/content/ContentValues;)V

    return-object v3
.end method

.method public listApplications(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;)Landroid/util/Pair;
    .locals 20
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/data/DataHolder;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mApplicationsApi:Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;

    const-string v4, "connected"

    const/4 v5, 0x0

    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->getAcceptLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    move/from16 v0, p3

    int-to-long v7, v0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string v9, "me"

    move-object/from16 v3, p2

    move-object/from16 v8, p4

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;->listBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/whitelisted/Applications;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/whitelisted/Applications;->getItems()Ljava/util/ArrayList;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v18

    new-instance v12, Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    move/from16 v0, v16

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/gms/plus/service/whitelisted/Application;

    invoke-virtual {v14}, Lcom/google/android/gms/plus/service/whitelisted/Application;->getValues()Landroid/content/ContentValues;

    move-result-object v19

    const-string v2, "icon_url"

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->getApplicationIconUrl(Lcom/google/android/gms/plus/service/whitelisted/Application;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v14}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->getInstalledApp(Landroid/content/pm/PackageManager;Lcom/google/android/gms/plus/service/whitelisted/Application;)Landroid/content/pm/ApplicationInfo;

    move-result-object v10

    if-eqz v10, :cond_0

    const-string v2, "display_name"

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "application_info"

    invoke-static {v10}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationColumns;->serializeApplicationInfo(Landroid/content/pm/ApplicationInfo;)[B

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_0
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent$DisplayNameComparator;

    invoke-direct {v2}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent$DisplayNameComparator;-><init>()V

    invoke-static {v12, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    sget-object v2, Lcom/google/android/gms/plus/internal/model/apps/ApplicationColumns;->ALL_COLUMNS:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->builder([Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    move-result-object v15

    const/16 v16, 0x0

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_2

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    invoke-virtual {v15, v2}, Lcom/google/android/gms/common/data/DataHolder$Builder;->withRow(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/gms/common/data/DataHolder$Builder;->build(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/whitelisted/Applications;->getNextPageToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    return-object v2
.end method

.method public sendLogEvents(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;IZ)V
    .locals 8
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # I
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->mRpcApi:Lcom/google/android/gms/plus/service/whitelisted/RpcApi;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/common/util/DefaultClock;->getInstance()Lcom/google/android/gms/common/util/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/Clock;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz p4, :cond_0

    const-string v7, "10"

    :goto_0
    move-object v1, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/service/whitelisted/RpcApi;->insertLogBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v7, "4"

    goto :goto_0
.end method
