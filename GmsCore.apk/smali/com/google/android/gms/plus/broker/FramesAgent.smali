.class public Lcom/google/android/gms/plus/broker/FramesAgent;
.super Ljava/lang/Object;
.source "FramesAgent.java"


# static fields
.field static final COLLECTION:Ljava/lang/String; = "vault"

.field private static final SYNC_EXTRAS_MANUAL_BUNDLE:Landroid/os/Bundle;

.field private static final TAG:Ljava/lang/String;

.field static final USER_ID:Ljava/lang/String; = "me"


# instance fields
.field private final mApiary:Lcom/google/android/gms/common/server/BaseApiaryServer;

.field private final mMomentsApi:Lcom/google/android/gms/plus/service/v1/MomentsApi;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/gms/plus/broker/FramesAgent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/broker/FramesAgent;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/broker/FramesAgent;->SYNC_EXTRAS_MANUAL_BUNDLE:Landroid/os/Bundle;

    sget-object v0, Lcom/google/android/gms/plus/broker/FramesAgent;->SYNC_EXTRAS_MANUAL_BUNDLE:Landroid/os/Bundle;

    const-string v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/BaseApiaryServer;Lcom/google/android/gms/plus/service/v1/MomentsApi;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/BaseApiaryServer;
    .param p2    # Lcom/google/android/gms/plus/service/v1/MomentsApi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/broker/FramesAgent;->mApiary:Lcom/google/android/gms/common/server/BaseApiaryServer;

    iput-object p2, p0, Lcom/google/android/gms/plus/broker/FramesAgent;->mMomentsApi:Lcom/google/android/gms/plus/service/v1/MomentsApi;

    return-void
.end method

.method private static enc(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static urlForInsert(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const-string v0, "people/%1$s/moments/%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/gms/plus/broker/FramesAgent;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Lcom/google/android/gms/plus/broker/FramesAgent;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public insertFrame(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0, v1, p2, p3}, Lcom/google/android/gms/plus/broker/FramesAgent;->insertFrameInDatabase(Landroid/content/ContentResolver;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    new-instance v0, Landroid/accounts/Account;

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.plus.action"

    sget-object v2, Lcom/google/android/gms/plus/broker/FramesAgent;->SYNC_EXTRAS_MANUAL_BUNDLE:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method insertFrameInDatabase(Landroid/content/ContentResolver;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "url"

    const-string v2, "me"

    const-string v3, "vault"

    invoke-static {v2, v3}, Lcom/google/android/gms/plus/broker/FramesAgent;->urlForInsert(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "packageName"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getAuthPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "accountName"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "payload"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusContent$Frames;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method

.method public listMoments(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    if-nez p5, :cond_0

    const/4 v6, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/FramesAgent;->mMomentsApi:Lcom/google/android/gms/plus/service/v1/MomentsApi;

    const-string v3, "vault"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-class v8, Lcom/google/android/gms/plus/service/v1/MomentsFeed;

    const/4 v9, 0x0

    move-object v1, p2

    move-object/from16 v2, p7

    move-object v5, p4

    move-object/from16 v7, p6

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->listBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;

    return-object v0

    :cond_0
    invoke-virtual {p5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public removeMoment(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;

    const/4 v3, 0x3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/plus/broker/FramesAgent;->mMomentsApi:Lcom/google/android/gms/plus/service/v1/MomentsApi;

    invoke-virtual {v2, p2, p3}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->removeBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/gms/plus/broker/FramesAgent;->TAG:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/plus/broker/FramesAgent;->TAG:Ljava/lang/String;

    const-string v3, "Failed to delete moment due to fatal authentication problem."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v2, Lcom/google/android/gms/plus/broker/FramesAgent;->TAG:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/plus/broker/FramesAgent;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to delete momentId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public uploadFrame(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/UserRecoverableAuthException;,
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/FramesAgent;->mApiary:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-virtual {v2, p1, p2, v1}, Lcom/google/android/gms/common/server/BaseApiaryServer;->performNoResponseRequestBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lorg/json/JSONObject;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/gms/plus/broker/FramesAgent;->TAG:Ljava/lang/String;

    const-string v3, "Error parsing JSON"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
