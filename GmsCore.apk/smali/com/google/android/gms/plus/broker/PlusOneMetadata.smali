.class public Lcom/google/android/gms/plus/broker/PlusOneMetadata;
.super Ljava/lang/Object;
.source "PlusOneMetadata.java"


# instance fields
.field private final mImpressionTimeInMillis:Ljava/lang/Long;

.field private final mPlusones:Lcom/google/android/gms/plus/service/pos/Plusones;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/service/pos/Plusones;J)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/service/pos/Plusones;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->mPlusones:Lcom/google/android/gms/plus/service/pos/Plusones;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->mImpressionTimeInMillis:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public getClickDelta()Ljava/lang/String;
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->mImpressionTimeInMillis:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlusones()Lcom/google/android/gms/plus/service/pos/Plusones;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->mPlusones:Lcom/google/android/gms/plus/service/pos/Plusones;

    return-object v0
.end method
