.class public final Lcom/google/android/gms/plus/broker/PlusOneHelper;
.super Ljava/lang/Object;
.source "PlusOneHelper.java"


# static fields
.field private static final DEFAULT_PROFILE_URL:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "default_avatar"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ResourceUtils;->getDrawableUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/broker/PlusOneHelper;->DEFAULT_PROFILE_URL:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static buildPlusOne(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/pos/Plusones;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/data/plusone/PlusOne;
    .locals 24
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/plus/service/pos/Plusones;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->isSetByViewer(Lcom/google/android/gms/plus/service/pos/Plusones;)Z

    move-result v6

    const/16 v19, 0x0

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->getPersonList(Lcom/google/android/gms/plus/service/pos/Plusones;)Ljava/util/List;

    move-result-object v21

    if-nez v21, :cond_0

    const/16 v22, 0x0

    :goto_0
    const/4 v2, 0x4

    new-array v13, v2, [Landroid/net/Uri;

    if-eqz v6, :cond_2

    const/4 v3, 0x0

    if-nez p7, :cond_1

    sget-object v2, Lcom/google/android/gms/plus/broker/PlusOneHelper;->DEFAULT_PROFILE_URL:Landroid/net/Uri;

    :goto_1
    aput-object v2, v13, v3

    move/from16 v20, v19

    :goto_2
    const/4 v3, 0x1

    move/from16 v0, v22

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    add-int/lit8 v19, v20, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/pos/Person;

    invoke-static {v2}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->getProfilePhotoUrl(Lcom/google/android/gms/plus/service/pos/Person;)Landroid/net/Uri;

    move-result-object v2

    :goto_3
    aput-object v2, v13, v3

    const/4 v3, 0x2

    move/from16 v0, v22

    move/from16 v1, v19

    if-le v0, v1, :cond_5

    add-int/lit8 v20, v19, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/pos/Person;

    invoke-static {v2}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->getProfilePhotoUrl(Lcom/google/android/gms/plus/service/pos/Person;)Landroid/net/Uri;

    move-result-object v2

    move/from16 v19, v20

    :goto_4
    aput-object v2, v13, v3

    const/4 v3, 0x3

    move/from16 v0, v22

    move/from16 v1, v19

    if-le v0, v1, :cond_6

    add-int/lit8 v20, v19, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/pos/Person;

    invoke-static {v2}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->getProfilePhotoUrl(Lcom/google/android/gms/plus/service/pos/Person;)Landroid/net/Uri;

    move-result-object v2

    move/from16 v19, v20

    :goto_5
    aput-object v2, v13, v3

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->getCount(Lcom/google/android/gms/plus/service/pos/Plusones;)I

    move-result v3

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->getFriendNames(Lcom/google/android/gms/plus/service/pos/Plusones;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-static {v0, v2, v6, v3, v4}, Lcom/google/android/gms/plus/broker/PlusOneStrings;->makeAnnotations(Landroid/content/res/Resources;Ljava/util/Locale;ZILjava/util/ArrayList;)Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;

    move-result-object v18

    const/4 v2, 0x4

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    aput-object v3, v12, v2

    const/4 v2, 0x1

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    aput-object v3, v12, v2

    const/4 v2, 0x2

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    aput-object v3, v12, v2

    const/4 v2, 0x3

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    aput-object v3, v12, v2

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/plus/service/pos/Plusones;->getValues()Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "token"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/plus/PlusIntents;->newPlusOneIntent(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v14

    new-instance v8, Lcom/google/android/gms/plus/data/plusone/PlusOne;

    move-object/from16 v0, v18

    iget-object v11, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    move-object/from16 v9, p3

    move v10, v6

    move-object v15, v7

    move-object/from16 v16, p5

    move-object/from16 v17, p6

    invoke-direct/range {v8 .. v17}, Lcom/google/android/gms/plus/data/plusone/PlusOne;-><init>(Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;[Landroid/net/Uri;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v8

    :cond_0
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v22

    goto/16 :goto_0

    :cond_1
    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/plus/internal/PlusContent$Image;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :cond_2
    const/4 v3, 0x0

    move/from16 v0, v22

    move/from16 v1, v19

    if-le v0, v1, :cond_3

    add-int/lit8 v20, v19, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/pos/Person;

    invoke-static {v2}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->getProfilePhotoUrl(Lcom/google/android/gms/plus/service/pos/Person;)Landroid/net/Uri;

    move-result-object v2

    move/from16 v19, v20

    :goto_6
    aput-object v2, v13, v3

    move/from16 v20, v19

    goto/16 :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_6

    :cond_4
    const/4 v2, 0x0

    move/from16 v19, v20

    goto/16 :goto_3

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_5
.end method

.method private static getCount(Lcom/google/android/gms/plus/service/pos/Plusones;)I
    .locals 3
    .param p0    # Lcom/google/android/gms/plus/service/pos/Plusones;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Plusones;->getMetadata()Lcom/google/android/gms/plus/service/pos/Metadata;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/pos/Metadata;->getGlobalCounts()Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/pos/GlobalCounts;->getCount()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method private static getFriendNames(Lcom/google/android/gms/plus/service/pos/Plusones;)Ljava/util/ArrayList;
    .locals 8
    .param p0    # Lcom/google/android/gms/plus/service/pos/Plusones;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/plus/service/pos/Plusones;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Plusones;->getMetadata()Lcom/google/android/gms/plus/service/pos/Metadata;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    return-object v1

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/pos/Metadata;->getGlobalCounts()Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/pos/GlobalCounts;->getPerson()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_0

    const/4 v6, 0x4

    if-ge v3, v6, :cond_0

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/plus/service/pos/Person;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/pos/Person;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private static getPersonList(Lcom/google/android/gms/plus/service/pos/Plusones;)Ljava/util/List;
    .locals 3
    .param p0    # Lcom/google/android/gms/plus/service/pos/Plusones;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/plus/service/pos/Plusones;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/plus/service/pos/Person;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Plusones;->getMetadata()Lcom/google/android/gms/plus/service/pos/Metadata;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/pos/Metadata;->getGlobalCounts()Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/pos/GlobalCounts;->getPerson()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private static getProfilePhotoUrl(Lcom/google/android/gms/plus/service/pos/Person;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/service/pos/Person;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Person;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/broker/PlusOneHelper;->DEFAULT_PROFILE_URL:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Person;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusContent$Image;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static isSetByViewer(Lcom/google/android/gms/plus/service/pos/Plusones;)Z
    .locals 3
    .param p0    # Lcom/google/android/gms/plus/service/pos/Plusones;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Plusones;->getValues()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "state"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method static isSetByViewer(Lcom/google/android/gms/plus/service/rpc/Plusone;)Z
    .locals 3
    .param p0    # Lcom/google/android/gms/plus/service/rpc/Plusone;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/rpc/Plusone;->getValues()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "isSetByViewer"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method static modifyCount(Lcom/google/android/gms/plus/service/pos/Plusones;I)V
    .locals 7
    .param p0    # Lcom/google/android/gms/plus/service/pos/Plusones;
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Plusones;->getMetadata()Lcom/google/android/gms/plus/service/pos/Metadata;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/pos/Metadata;->getGlobalCounts()Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "count"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forDouble(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/pos/GlobalCounts;->getCount()Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    int-to-double v5, p1

    add-double/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/plus/service/pos/GlobalCounts;->setDouble(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;D)V

    goto :goto_0
.end method

.method static modifySetByViewer(Lcom/google/android/gms/plus/service/pos/Plusones;Z)V
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/service/pos/Plusones;
    .param p1    # Z

    const-string v0, "state"

    invoke-static {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/pos/Plusones;->setBoolean(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Z)V

    return-void
.end method
