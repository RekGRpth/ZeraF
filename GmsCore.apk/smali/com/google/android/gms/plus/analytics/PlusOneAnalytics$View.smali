.class public final Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;
.super Ljava/lang/Object;
.source "PlusOneAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "View"
.end annotation


# static fields
.field public static final CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final GOOGLE_PLUS_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONED_BY:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final UNKNOWN_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->UNKNOWN_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x1

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x2

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x3

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->GOOGLE_PLUS_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x4

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->PLUS_ONED_BY:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
