.class Lcom/google/android/gms/plus/UnifiedSetupActivity$3;
.super Lcom/google/android/gms/auth/login/CancelableCallbackThread;
.source "UnifiedSetupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/plus/UnifiedSetupActivity;->startNameCheckTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V
    .locals 0
    .param p2    # Landroid/os/Message;

    iput-object p1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$3;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-direct {p0, p2}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;-><init>(Landroid/os/Message;)V

    return-void
.end method


# virtual methods
.method protected runInBackground()V
    .locals 4

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSUser;->getGLSContext()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$3;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # getter for: Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;
    invoke-static {v2}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$500(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->checkRealName(Lcom/google/android/gms/auth/login/GLSSession;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$3;->mCallbackMessage:Landroid/os/Message;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/Status;->toMessage(Landroid/os/Message;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$3;->mIsCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$3;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    const-wide/16 v2, 0x3e8

    # invokes: Lcom/google/android/gms/plus/UnifiedSetupActivity;->ensureBackgroundTaskDelay(J)V
    invoke-static {v1, v2, v3}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$600(Lcom/google/android/gms/plus/UnifiedSetupActivity;J)V

    :cond_0
    return-void
.end method
