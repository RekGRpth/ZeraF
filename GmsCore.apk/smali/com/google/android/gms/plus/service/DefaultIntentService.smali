.class public final Lcom/google/android/gms/plus/service/DefaultIntentService;
.super Lcom/google/android/gms/plus/service/OperationIntentService;
.source "DefaultIntentService.java"


# static fields
.field private static final START:Landroid/content/Intent;

.field private static final TAG:Ljava/lang/String;

.field private static sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/gms/plus/service/DefaultIntentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.service.default.INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->START:Landroid/content/Intent;

    new-instance v0, Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    sget-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/plus/service/DefaultIntentService;->sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/service/OperationIntentService;-><init>(Ljava/lang/String;Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;)V

    return-void
.end method

.method public static startOperation(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gms/plus/service/OperationIntentService$Operation;

    sget-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;->addOperation(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    sget-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->START:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->isLoggingIntent(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;-><init>(Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->onHandleOperation(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/service/OperationIntentService;->onHandleIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method
