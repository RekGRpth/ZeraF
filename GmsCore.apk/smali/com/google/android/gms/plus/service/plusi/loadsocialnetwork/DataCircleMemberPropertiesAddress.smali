.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataCircleMemberPropertiesAddress.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "locality"

    const-string v2, "locality"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "standardTag"

    const-string v2, "standardTag"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "country"

    const-string v2, "country"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "customTag"

    const-string v2, "customTag"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "poBox"

    const-string v2, "poBox"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "value"

    const-string v2, "value"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "streetAddress"

    const-string v2, "streetAddress"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "postalCode"

    const-string v2, "postalCode"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "extendedAddress"

    const-string v2, "extendedAddress"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    const-string v1, "region"

    const-string v2, "region"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    const-string v0, "locality"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const-string v0, "standardTag"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setInteger(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "country"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "customTag"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "poBox"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-virtual {p0, v0, p6}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "streetAddress"

    invoke-virtual {p0, v0, p7}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "postalCode"

    invoke-virtual {p0, v0, p8}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "extendedAddress"

    invoke-virtual {p0, v0, p9}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "region"

    invoke-virtual {p0, v0, p10}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCountry()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCustomTag()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "customTag"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getExtendedAddress()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "extendedAddress"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getLocality()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "locality"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPoBox()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "poBox"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "postalCode"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "region"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getStandardTag()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "standardTag"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getStreetAddress()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "streetAddress"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "value"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
