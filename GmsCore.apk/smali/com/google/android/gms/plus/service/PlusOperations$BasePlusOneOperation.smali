.class public abstract Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BasePlusOneOperation"
.end annotation


# instance fields
.field final mClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v11, 0x4

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->getResult(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)Landroid/util/Pair;

    move-result-object v4

    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v3, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v6, "pendingIntent"

    invoke-virtual {v5}, Lcom/google/android/gms/common/ConnectionResult;->getResolution()Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-virtual {v5}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v8

    iget-object v6, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Landroid/os/Bundle;

    invoke-interface {v7, v8, v3, v6}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v1

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {p1, v10, v6, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v6, "pendingIntent"

    invoke-virtual {v3, v6, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v6, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v6, v11, v3, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v6, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v6}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpBundle(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v6, v11, v0, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v1

    iget-object v6, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v7, 0x7

    invoke-interface {v6, v7, v9, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public abstract getResult(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/plus/broker/DataBroker;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
