.class public Lcom/google/android/gms/plus/service/whitelisted/Application;
.super Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;
.source "Application.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/Application;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Application;->sFields:Ljava/util/HashMap;

    const-string v1, "clients"

    const-string v2, "clients"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/PlusAppClient;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Application;->sFields:Ljava/util/HashMap;

    const-string v1, "displayName"

    const-string v2, "display_name"

    const-class v3, Lcom/google/android/gms/plus/server/UnicodeConverter;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->withConverter(Ljava/lang/String;Ljava/lang/Class;Z)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Application;->sFields:Ljava/util/HashMap;

    const-string v1, "icon"

    const-string v2, "icon"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/Icon;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Application;->sFields:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "application_id"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 1
    .param p1    # Landroid/content/ContentValues;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>(Landroid/content/ContentValues;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Icon;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/plus/service/whitelisted/Icon;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/PlusAppClient;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/Icon;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v0, "clients"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/whitelisted/Application;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Application;->sFields:Ljava/util/HashMap;

    const-string v1, "displayName"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/whitelisted/Application;->setString(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;)V

    const-string v0, "icon"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/whitelisted/Application;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "application_id"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/whitelisted/Application;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getClients()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/PlusAppClient;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "clients"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Application;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getIcon()Lcom/google/android/gms/plus/service/whitelisted/Icon;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "icon"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/Icon;

    return-object v0
.end method

.method protected isConcreteTypeArrayFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Application;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
