.class Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;
.super Ljava/lang/Object;
.source "OfflineActionSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Frame"
.end annotation


# instance fields
.field private id:J

.field private packageName:Ljava/lang/String;

.field private payload:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$1;

    invoke-direct {p0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)J
    .locals 2
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;

    iget-wide v0, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->id:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->id:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->url:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->url:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->payload:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->payload:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->packageName:Ljava/lang/String;

    return-object p1
.end method
