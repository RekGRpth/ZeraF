.class public Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "LoggedPhoto.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->sFields:Ljava/util/HashMap;

    const-string v1, "photoId"

    const-string v2, "photoId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->sFields:Ljava/util/HashMap;

    const-string v1, "photoUrl"

    const-string v2, "photoUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    const-string v0, "photoId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "photoUrl"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getPhotoId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedPhoto;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
