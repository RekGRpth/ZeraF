.class public Lcom/google/android/gms/plus/service/whitelisted/InputData;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "InputData.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    const-string v1, "profileEdit"

    const-string v2, "profileEdit"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ProfileEdit;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    const-string v1, "interestsRemoved"

    const-string v2, "interestsRemoved"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    const-string v1, "invitesSent"

    const-string v2, "invitesSent"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    const-string v1, "interestsAdded"

    const-string v2, "interestsAdded"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    const-string v1, "upgradeInfo"

    const-string v2, "upgradeInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/UpgradeInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    const-string v1, "socialCircleRawQuery"

    const-string v2, "socialCircleRawQuery"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    const-string v1, "profileCreationInfo"

    const-string v2, "profileCreationInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ProfileCreationInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->mConcreteTypes:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/whitelisted/ProfileEdit;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/UpgradeInfo;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/ProfileCreationInfo;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/ProfileEdit;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/Integer;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Lcom/google/android/gms/plus/service/whitelisted/UpgradeInfo;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/gms/plus/service/whitelisted/ProfileCreationInfo;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v0, "profileEdit"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p2, :cond_0

    const-string v0, "interestsRemoved"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->setInteger(Ljava/lang/String;I)V

    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "invitesSent"

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->setInteger(Ljava/lang/String;I)V

    :cond_1
    if-eqz p4, :cond_2

    const-string v0, "interestsAdded"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->setInteger(Ljava/lang/String;I)V

    :cond_2
    const-string v0, "upgradeInfo"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "socialCircleRawQuery"

    invoke-virtual {p0, v0, p6}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "profileCreationInfo"

    invoke-virtual {p0, v0, p7}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getInterestsAdded()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "interestsAdded"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getInterestsRemoved()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "interestsRemoved"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getInvitesSent()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "invitesSent"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getProfileCreationInfo()Lcom/google/android/gms/plus/service/whitelisted/ProfileCreationInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "profileCreationInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ProfileCreationInfo;

    return-object v0
.end method

.method public getProfileEdit()Lcom/google/android/gms/plus/service/whitelisted/ProfileEdit;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "profileEdit"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ProfileEdit;

    return-object v0
.end method

.method public getSocialCircleRawQuery()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/InputData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "socialCircleRawQuery"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getUpgradeInfo()Lcom/google/android/gms/plus/service/whitelisted/UpgradeInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "upgradeInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/UpgradeInfo;

    return-object v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/InputData;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
