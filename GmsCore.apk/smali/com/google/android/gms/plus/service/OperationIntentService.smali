.class public abstract Lcom/google/android/gms/plus/service/OperationIntentService;
.super Landroid/app/IntentService;
.source "OperationIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/service/OperationIntentService$Operation;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDataBroker:Lcom/google/android/gms/plus/broker/DataBroker;

.field private final mQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/service/OperationIntentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/service/OperationIntentService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/gms/plus/service/OperationIntentService;->mQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    return-void
.end method

.method private fatalException(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/service/OperationIntentService$Operation;
    .param p2    # Ljava/lang/Exception;

    sget-object v0, Lcom/google/android/gms/plus/service/OperationIntentService;->TAG:Ljava/lang/String;

    const-string v1, "Failed operation."

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :try_start_0
    invoke-interface {p1, p2}, Lcom/google/android/gms/plus/service/OperationIntentService$Operation;->onFatalException(Ljava/lang/Exception;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/gms/plus/broker/DataBroker;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/broker/DataBroker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/service/OperationIntentService;->mDataBroker:Lcom/google/android/gms/plus/broker/DataBroker;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/OperationIntentService;->mQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;->removeOperations()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/service/OperationIntentService$Operation;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/service/OperationIntentService;->onHandleOperation(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onHandleOperation(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/service/OperationIntentService$Operation;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/OperationIntentService;->mDataBroker:Lcom/google/android/gms/plus/broker/DataBroker;

    invoke-interface {p1, p0, v1}, Lcom/google/android/gms/plus/service/OperationIntentService$Operation;->execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/OperationIntentService;->fatalException(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/OperationIntentService;->fatalException(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/OperationIntentService;->fatalException(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_0
.end method
