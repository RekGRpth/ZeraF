.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "LoadSocialNetworkResponse.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->sFields:Ljava/util/HashMap;

    const-string v1, "personList"

    const-string v2, "personList"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPersonList;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->sFields:Ljava/util/HashMap;

    const-string v1, "systemGroups"

    const-string v2, "systemGroups"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSystemGroups;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->sFields:Ljava/util/HashMap;

    const-string v1, "viewerCircles"

    const-string v2, "viewerCircles"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataViewerCircles;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->sFields:Ljava/util/HashMap;

    const-string v1, "backendTrace"

    const-string v2, "backendTrace"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/TraceRecords;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->mConcreteTypes:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPersonList;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSystemGroups;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataViewerCircles;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/TraceRecords;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPersonList;
    .param p2    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSystemGroups;
    .param p3    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataViewerCircles;
    .param p4    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/TraceRecords;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->mConcreteTypes:Ljava/util/HashMap;

    const-string v0, "personList"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "systemGroups"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "viewerCircles"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "backendTrace"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getBackendTrace()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/TraceRecords;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "backendTrace"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/TraceRecords;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getPersonList()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPersonList;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "personList"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPersonList;

    return-object v0
.end method

.method public getSystemGroups()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSystemGroups;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "systemGroups"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSystemGroups;

    return-object v0
.end method

.method public getViewerCircles()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataViewerCircles;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkResponse;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "viewerCircles"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataViewerCircles;

    return-object v0
.end method
