.class public Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
.super Ljava/lang/Object;
.source "PlusOperationsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperationsUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StatusCodeResolution"
.end annotation


# instance fields
.field public final mResolution:Landroid/content/Intent;

.field public final mStatusCode:I


# direct methods
.method public constructor <init>(ILandroid/content/Intent;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;->mStatusCode:I

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;->mResolution:Landroid/content/Intent;

    return-void
.end method
