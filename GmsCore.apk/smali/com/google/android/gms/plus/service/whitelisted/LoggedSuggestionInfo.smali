.class public Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "LoggedSuggestionInfo.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "friendSuggestionSummarizedInfoBitmask"

    const-string v2, "friendSuggestionSummarizedInfoBitmask"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "friendSuggestionSummarizedAdditionalInfoBitmask"

    const-string v2, "friendSuggestionSummarizedAdditionalInfoBitmask"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "explanationType"

    const-string v2, "explanationType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "experimentNames"

    const-string v2, "experimentNames"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "numberOfCircleMembersAdded"

    const-string v2, "numberOfCircleMembersAdded"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "score"

    const-string v2, "score"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forDouble(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "queryId"

    const-string v2, "queryId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestedCircleMember"

    const-string v2, "suggestedCircleMember"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "deprecatedFriendSuggestionSummarizedInfoBitmask"

    const-string v2, "deprecatedFriendSuggestionSummarizedInfoBitmask"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "placement"

    const-string v2, "placement"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestionType"

    const-string v2, "suggestionType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "explanationsTypesBitmask"

    const-string v2, "explanationsTypesBitmask"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestedCircle"

    const-string v2, "suggestedCircle"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "numberOfCircleMembersRemoved"

    const-string v2, "numberOfCircleMembersRemoved"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;Ljava/lang/Integer;)V
    .locals 4
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/Integer;
    .param p6    # Ljava/lang/Double;
    .param p7    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/Integer;
    .param p11    # Ljava/lang/String;
    .param p12    # Ljava/lang/Integer;
    .param p13    # Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;
    .param p14    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Double;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypeArrays:Ljava/util/HashMap;

    if-eqz p1, :cond_0

    const-string v1, "friendSuggestionSummarizedInfoBitmask"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setInteger(Ljava/lang/String;I)V

    :cond_0
    if-eqz p2, :cond_1

    const-string v1, "friendSuggestionSummarizedAdditionalInfoBitmask"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setInteger(Ljava/lang/String;I)V

    :cond_1
    const-string v1, "explanationType"

    invoke-virtual {p0, v1, p3}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "experimentNames"

    invoke-virtual {p0, v1, p4}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p5, :cond_2

    const-string v1, "numberOfCircleMembersAdded"

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setInteger(Ljava/lang/String;I)V

    :cond_2
    if-eqz p6, :cond_3

    const-string v1, "score"

    invoke-virtual {p6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setDouble(Ljava/lang/String;D)V

    :cond_3
    const-string v1, "queryId"

    invoke-virtual {p0, v1, p7}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "suggestedCircleMember"

    invoke-virtual {p0, v1, p8}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "deprecatedFriendSuggestionSummarizedInfoBitmask"

    invoke-virtual {p0, v1, p9}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p10, :cond_4

    const-string v1, "placement"

    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setInteger(Ljava/lang/String;I)V

    :cond_4
    const-string v1, "suggestionType"

    invoke-virtual {p0, v1, p11}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p12, :cond_5

    const-string v1, "explanationsTypesBitmask"

    invoke-virtual/range {p12 .. p12}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setInteger(Ljava/lang/String;I)V

    :cond_5
    const-string v1, "suggestedCircle"

    move-object/from16 v0, p13

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p14, :cond_6

    const-string v1, "numberOfCircleMembersRemoved"

    invoke-virtual/range {p14 .. p14}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->setInteger(Ljava/lang/String;I)V

    :cond_6
    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getDeprecatedFriendSuggestionSummarizedInfoBitmask()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "deprecatedFriendSuggestionSummarizedInfoBitmask"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getExperimentNames()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "experimentNames"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getExplanationType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "explanationType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getExplanationsTypesBitmask()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "explanationsTypesBitmask"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFriendSuggestionSummarizedAdditionalInfoBitmask()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "friendSuggestionSummarizedAdditionalInfoBitmask"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getFriendSuggestionSummarizedInfoBitmask()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "friendSuggestionSummarizedInfoBitmask"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getNumberOfCircleMembersAdded()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "numberOfCircleMembersAdded"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getNumberOfCircleMembersRemoved()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "numberOfCircleMembersRemoved"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getPlacement()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "placement"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getQueryId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "queryId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getScore()Ljava/lang/Double;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "score"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    return-object v0
.end method

.method public getSuggestedCircle()Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "suggestedCircle"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;

    return-object v0
.end method

.method public getSuggestedCircleMember()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "suggestedCircleMember"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSuggestionType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "suggestionType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected isConcreteTypeArrayFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
