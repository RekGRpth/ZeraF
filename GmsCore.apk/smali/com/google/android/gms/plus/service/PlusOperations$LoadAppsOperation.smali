.class public final Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadAppsOperation"
.end annotation


# instance fields
.field private final mClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mMaxResults:I

.field private final mNextPageToken:Ljava/lang/String;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mMaxResults:I

    iput-object p3, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mNextPageToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v10, 0x4

    const/4 v9, 0x0

    const/4 v8, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iget v6, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mMaxResults:I

    iget-object v7, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mNextPageToken:Ljava/lang/String;

    invoke-virtual {p2, p1, v5, v6, v7}, Lcom/google/android/gms/plus/broker/DataBroker;->listApplications(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/gms/common/data/DataHolder;

    iget-object v6, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v5, v6}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAppsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v1

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {p1, v9, v5, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v5, "pendingIntent"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-static {v10, v4}, Lcom/google/android/gms/common/data/DataHolder;->empty(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v6

    invoke-interface {v5, v6, v8}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAppsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v5}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpBundle(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-static {v10, v0}, Lcom/google/android/gms/common/data/DataHolder;->empty(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v6

    invoke-interface {v5, v6, v8}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAppsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v1

    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v6, 0x7

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v6

    invoke-interface {v5, v6, v8}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAppsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAppsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
