.class public Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListPeopleOperation"
.end annotation


# instance fields
.field private final mClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mCollection:I

.field private final mMaxResults:I

.field private final mOrderBy:I

.field private final mPageToken:Ljava/lang/String;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mCollection:I

    iput p3, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mOrderBy:I

    iput p4, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mMaxResults:I

    iput-object p5, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPageToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iget v3, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mCollection:I

    iget v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mOrderBy:I

    iget v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mMaxResults:I

    iget-object v6, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPageToken:Ljava/lang/String;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/broker/DataBroker;->listPeople(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;)Landroid/util/Pair;

    move-result-object v11

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    iget-object v0, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iget-object v1, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onPeopleLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v8

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v8}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    const-string v0, "pendingIntent"

    invoke-virtual {v10, v0, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v1, 0x4

    invoke-static {v1, v10}, Lcom/google/android/gms/common/data/DataHolder;->empty(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onPeopleLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v8

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpBundle(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v1, 0x4

    invoke-static {v1, v7}, Lcom/google/android/gms/common/data/DataHolder;->empty(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onPeopleLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v8

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onPeopleLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onPeopleLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
