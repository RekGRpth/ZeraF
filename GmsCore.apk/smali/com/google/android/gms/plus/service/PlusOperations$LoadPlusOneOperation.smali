.class public final Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;
.super Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;
.source "PlusOperations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadPlusOneOperation"
.end annotation


# instance fields
.field final mCacheOption:I

.field final mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;->mUrl:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;->mCacheOption:I

    return-void
.end method


# virtual methods
.method public getResult(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)Landroid/util/Pair;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/plus/broker/DataBroker;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;->mUrl:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;->mCacheOption:I

    invoke-virtual {p2, p1, v0, v1, v2}, Lcom/google/android/gms/plus/broker/DataBroker;->loadPlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
