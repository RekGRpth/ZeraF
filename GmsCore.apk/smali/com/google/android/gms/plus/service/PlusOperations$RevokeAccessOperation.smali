.class public final Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RevokeAccessOperation"
.end annotation


# instance fields
.field private final mClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v4}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/google/android/gms/common/account/AccountUtils;->getSelectedAccount(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->deletePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2, p1, v4}, Lcom/google/android/gms/plus/broker/DataBroker;->revokeAccess(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAccessRevoked(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v1

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {p1, v7, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v4, "pendingIntent"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v4, v8, v3}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAccessRevoked(ILandroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v4}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpBundle(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v4, v8, v3}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAccessRevoked(ILandroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v1

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v5, 0x7

    invoke-interface {v4, v5, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAccessRevoked(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onAccessRevoked(ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method
