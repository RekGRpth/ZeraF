.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataCirclePerson.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->sFields:Ljava/util/HashMap;

    const-string v1, "membership"

    const-string v2, "membership"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataMembership;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->sFields:Ljava/util/HashMap;

    const-string v1, "joinKey"

    const-string v2, "joinKey"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->sFields:Ljava/util/HashMap;

    const-string v1, "primaryContact"

    const-string v2, "primaryContact"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContact;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->sFields:Ljava/util/HashMap;

    const-string v1, "memberId"

    const-string v2, "memberId"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->sFields:Ljava/util/HashMap;

    const-string v1, "memberProperties"

    const-string v2, "memberProperties"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContact;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;)V
    .locals 1
    .param p3    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContact;
    .param p4    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;
    .param p5    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataMembership;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;",
            ">;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContact;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v0, "membership"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "joinKey"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "primaryContact"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "memberId"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "memberProperties"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getJoinKey()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "joinKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMemberId()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "memberId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberId;

    return-object v0
.end method

.method public getMemberProperties()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "memberProperties"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;

    return-object v0
.end method

.method public getMembership()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataMembership;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "membership"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPrimaryContact()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContact;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCirclePerson;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "primaryContact"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContact;

    return-object v0
.end method
