.class public Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "ClientLoggedRibbonOrderOrder.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->sFields:Ljava/util/HashMap;

    const-string v1, "item"

    const-string v2, "item"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forIntegers(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->sFields:Ljava/util/HashMap;

    const-string v1, "morePosition"

    const-string v2, "morePosition"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/Integer;)V
    .locals 2
    .param p2    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    const-string v0, "item"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->setIntegers(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p2, :cond_0

    const-string v0, "morePosition"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->setInteger(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getItem()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "item"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMorePosition()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrderOrder;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "morePosition"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method
