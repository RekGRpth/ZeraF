.class public Lcom/google/android/gms/plus/PlusClient$Builder;
.super Ljava/lang/Object;
.source "PlusClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/PlusClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mConnectionCallbacks:Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

.field private mConnectionFailedListener:Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

.field private mContext:Landroid/content/Context;

.field private mRequestedScopes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRequiredFeatures:[Ljava/lang/String;

.field private mVisibleActivities:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mConnectionCallbacks:Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    iput-object p3, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mConnectionFailedListener:Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequestedScopes:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequestedScopes:Ljava/util/ArrayList;

    const-string v1, "https://www.googleapis.com/auth/plus.login"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/plus/PlusClient;
    .locals 11

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mAccountName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "<<default account>>"

    iput-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mAccountName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequestedScopes:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequestedScopes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mAccountName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mConnectionCallbacks:Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    iget-object v6, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mConnectionFailedListener:Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    iget-object v7, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mVisibleActivities:[Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequiredFeatures:[Ljava/lang/String;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/gms/plus/PlusClient$1;)V

    return-object v0
.end method

.method public setAccountName(Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mAccountName:Ljava/lang/String;

    return-object p0
.end method

.method public varargs setRequiredFeatures([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequiredFeatures:[Ljava/lang/String;

    return-object p0
.end method

.method public varargs setScopes([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;
    .locals 2
    .param p1    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequestedScopes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->mRequestedScopes:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method
