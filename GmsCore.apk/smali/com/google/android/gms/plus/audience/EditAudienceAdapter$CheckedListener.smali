.class final Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;
.super Ljava/lang/Object;
.source "EditAudienceAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CheckedListener"
.end annotation


# instance fields
.field private final audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

.field final synthetic this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/common/people/data/AudienceMember;

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mListener:Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$200(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$300(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$300(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mListener:Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$200(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;->audienceMemberAdded(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$300(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$300(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mListener:Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$200(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;->audienceMemberRemoved(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_0
.end method
