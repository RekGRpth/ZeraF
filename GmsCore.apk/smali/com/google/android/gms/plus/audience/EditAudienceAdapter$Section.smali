.class public Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;
.super Ljava/lang/Object;
.source "EditAudienceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Section"
.end annotation


# instance fields
.field public final mAudience:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public final mIndexer:Ljava/lang/String;

.field public final mTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .param p2    # Ljava/util/List;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mTitle:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mIndexer:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .param p2    # Ljava/util/List;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getIndexer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mIndexer:Ljava/lang/String;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSectionTitles:Z
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$400(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mTitle:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getLength()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->this$0:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    # getter for: Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSectionTitles:Z
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->access$400(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getIndexer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
