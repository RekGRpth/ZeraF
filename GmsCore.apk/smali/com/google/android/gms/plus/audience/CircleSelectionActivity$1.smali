.class final Lcom/google/android/gms/plus/audience/CircleSelectionActivity$1;
.super Ljava/lang/Object;
.source "CircleSelectionActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/gms/common/people/data/AudienceMember;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/gms/common/people/data/AudienceMember;Lcom/google/android/gms/common/people/data/AudienceMember;)I
    .locals 3
    .param p1    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p2    # Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v1, 0x1

    const/4 v0, -0x1

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->isHidden()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->isHidden()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->isHidden()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->isCircle()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->isCircle()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->isCircle()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->isCircle()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    # getter for: Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->COLLATOR:Ljava/text/Collator;
    invoke-static {}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->access$000()Ljava/text/Collator;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gms/common/people/data/AudienceMember;

    check-cast p2, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$1;->compare(Lcom/google/android/gms/common/people/data/AudienceMember;Lcom/google/android/gms/common/people/data/AudienceMember;)I

    move-result v0

    return v0
.end method
