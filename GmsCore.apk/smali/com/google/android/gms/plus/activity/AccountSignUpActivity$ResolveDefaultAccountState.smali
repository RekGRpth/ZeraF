.class Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;
.super Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;
.source "AccountSignUpActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/util/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResolveDefaultAccountState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;",
        "Lcom/google/android/gms/common/util/Function",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;->apply(Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/String;)Ljava/lang/Void;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # setter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$402(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$600(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public isFinished()Z
    .locals 2

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$500(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->getDefaultAccountName(Ljava/lang/String;Lcom/google/android/gms/common/util/Function;)V

    return-void
.end method
