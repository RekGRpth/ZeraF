.class Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;
.super Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;
.source "AccountSignUpActivity.java"

# interfaces
.implements Lcom/google/android/gms/plus/activity/AccountSignUpClient$AuthIntentCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAuthTokenIntentState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public isFinished()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mScopeString:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mClientAuthTokenIntent:Landroid/app/PendingIntent;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1300(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAuthIntent(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mScopeString:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1202(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$600(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getResolution()Landroid/app/PendingIntent;

    move-result-object v1

    # setter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mClientAuthTokenIntent:Landroid/app/PendingIntent;
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1302(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$600(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v2, 0x7f0b0026

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v2, 0x7f0b0080

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v2, 0x7f0b0027

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 8

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iget-object v0, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAuthPackageName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v3, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    :cond_1
    const/4 v0, -0x1

    if-eq v3, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAuthPackageName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mScopeString:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v4

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->getAuthIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/activity/AccountSignUpClient$AuthIntentCallback;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
