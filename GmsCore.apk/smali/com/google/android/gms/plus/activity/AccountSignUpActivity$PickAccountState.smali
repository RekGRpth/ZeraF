.class Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;
.super Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SubActivityState;
.source "AccountSignUpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PickAccountState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SubActivityState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public isFinished()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 10

    const/4 v9, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->hideSpinner()V
    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$800(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    new-array v2, v9, [Ljava/lang/String;

    const-string v1, "com.google"

    aput-object v1, v2, v3

    move-object v1, v0

    move-object v4, v0

    move-object v5, v0

    move-object v6, v0

    move-object v7, v0

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/common/AccountPicker;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
