.class public Lcom/google/android/gms/plus/PlusClient;
.super Ljava/lang/Object;
.source "PlusClient.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/PlusClient$1;,
        Lcom/google/android/gms/plus/PlusClient$Builder;,
        Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnPeopleLoadedListener;,
        Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;
    }
.end annotation


# instance fields
.field final mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p5    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p6    # [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p6    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p7    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/internal/PlusClientImpl;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    return-void
.end method

.method private varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p6    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p7    # [Ljava/lang/String;
    .param p8    # [Ljava/lang/String;
    .param p9    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/internal/PlusClientImpl;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/gms/plus/PlusClient$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p6    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p7    # [Ljava/lang/String;
    .param p8    # [Ljava/lang/String;
    .param p9    # [Ljava/lang/String;
    .param p10    # Lcom/google/android/gms/plus/PlusClient$1;

    invoke-direct/range {p0 .. p9}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public connect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->connect()V

    return-void
.end method

.method public deletePlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->deletePlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    return-void
.end method

.method public disconnect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->disconnect()V

    return-void
.end method

.method public disconnectSource(Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->disconnectSource(Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;Ljava/lang/String;Z)V

    return-void
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getAccountName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSignUpState(Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getSignUpState(Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;)V

    return-void
.end method

.method public insertPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->insertPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->isConnected()Z

    move-result v0

    return v0
.end method

.method public isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z
    .locals 1
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z

    move-result v0

    return v0
.end method

.method public isConnectionFailedListenerRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Z
    .locals 1
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->isConnectionFailedListenerRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Z

    move-result v0

    return v0
.end method

.method public loadCirclesInternal(Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->loadAudienceInternal(Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;)V

    return-void
.end method

.method public loadConnectedApps(Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;ILjava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->loadConnectedApps(Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;ILjava/lang/String;)V

    return-void
.end method

.method public loadImage(Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;Landroid/net/Uri;I)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;
    .param p2    # Landroid/net/Uri;
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->loadImage(Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;Landroid/net/Uri;I)V

    return-void
.end method

.method public loadLinkPreviewInternal(Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->loadLinkPreviewInternal(Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;Ljava/lang/String;)V

    return-void
.end method

.method public loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    return-void
.end method

.method public registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    return-void
.end method

.method public registerConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->registerConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    return-void
.end method

.method public unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public writeMoment(Lcom/google/android/gms/plus/model/moments/Moment;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/model/moments/Moment;

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->mPlusClientImpl:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->writeMoment(Lcom/google/android/gms/plus/model/moments/Moment;)V

    return-void
.end method
