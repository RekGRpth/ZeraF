.class public final Lcom/google/android/gms/plus/PlusIntents;
.super Ljava/lang/Object;
.source "PlusIntents.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newAccountSignUpBundle(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;

    const/4 v3, 0x0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v3, "pendingIntent"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v2
.end method

.method public static newAccountSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/content/Intent;
    .locals 8

    const/4 v1, 0x0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->hasAnyScopes()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getScopesString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getAuthPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getVisibleActions()[Ljava/lang/String;

    move-result-object v6

    move-object v4, v1

    move-object v7, v1

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->serializeToBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v1

    :cond_0
    move-object v5, v1

    goto :goto_0
.end method

.method public static newPlusOneIntent(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 2

    if-eqz p4, :cond_0

    const-string v0, "com.google.android.gms.plus.action.UNDO_PLUS_ONE"

    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.intent.extra.EXTRA_SIGNED_UP"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.intent.extra.TOKEN"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.intent.extra.URL"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.plus.action.PLUS_ONE"

    goto :goto_0
.end method

.method public static newSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/Boolean;)Landroid/content/Intent;
    .locals 8

    const/4 v7, 0x0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->hasAnyScopes()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getScopesString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getAuthPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getVisibleActions()[Ljava/lang/String;

    move-result-object v6

    move-object v4, p2

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->serializeToBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v1

    :cond_0
    move-object v5, v7

    goto :goto_0
.end method
