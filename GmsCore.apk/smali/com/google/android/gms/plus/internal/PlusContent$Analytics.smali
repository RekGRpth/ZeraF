.class public interface abstract Lcom/google/android/gms/plus/internal/PlusContent$Analytics;
.super Ljava/lang/Object;
.source "PlusContent.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Analytics"
.end annotation


# static fields
.field public static final ACCOUNT_NAME:Ljava/lang/String; = "accountName"

.field public static final BASE_PATH:Ljava/lang/String; = "analytics"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final PAYLOAD:Ljava/lang/String; = "payload"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final TYPE:Ljava/lang/String; = "type"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lcom/google/android/gms/plus/internal/PlusContent;->ACTION_AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "analytics"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusContent$Analytics;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method
