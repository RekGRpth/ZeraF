.class public final Lcom/google/android/gms/plus/internal/PlusContent;
.super Ljava/lang/Object;
.source "PlusContent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/internal/PlusContent$Avatar;,
        Lcom/google/android/gms/plus/internal/PlusContent$Image;,
        Lcom/google/android/gms/plus/internal/PlusContent$DefaultAccount;,
        Lcom/google/android/gms/plus/internal/PlusContent$Analytics;,
        Lcom/google/android/gms/plus/internal/PlusContent$PlusProfiles;,
        Lcom/google/android/gms/plus/internal/PlusContent$Frames;,
        Lcom/google/android/gms/plus/internal/PlusContent$PlusAccount;
    }
.end annotation


# static fields
.field public static final ACTION_AUTHORITY:Ljava/lang/String; = "com.google.android.gms.plus.action"

.field public static final ACTION_AUTHORITY_URI:Landroid/net/Uri;

.field public static final AUTHORITY:Ljava/lang/String; = "com.google.android.gms.plus"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.google.android.gms.plus.action"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusContent;->ACTION_AUTHORITY_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
