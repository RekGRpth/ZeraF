.class public final Lcom/google/android/gms/plus/internal/model/apps/ApplicationRef;
.super Lcom/google/android/gms/common/data/DataBufferRef;
.source "ApplicationRef.java"

# interfaces
.implements Lcom/google/android/gms/plus/model/apps/Application;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/data/DataHolder;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/DataBufferRef;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-void
.end method


# virtual methods
.method public freeze()Lcom/google/android/gms/plus/model/apps/Application;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please use ApplicationEntity.from(application)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic freeze()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationRef;->freeze()Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 2

    const-string v1, "application_info"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationRef;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationColumns;->deserializeApplicationInfo([B)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    const-string v0, "display_name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationRef;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "icon_url"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationRef;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    const-string v0, "application_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationRef;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
