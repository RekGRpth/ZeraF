.class public final Lcom/google/android/gms/plus/internal/PlusResources$PlusOneResources;
.super Ljava/lang/Object;
.source "PlusResources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlusOneResources"
.end annotation


# static fields
.field public static final BUBBLE_MEDIUM:Ljava/lang/String; = "global_count_bubble_medium"

.field public static final BUBBLE_SMALL:Ljava/lang/String; = "global_count_bubble_small"

.field public static final BUBBLE_STANDARD:Ljava/lang/String; = "global_count_bubble_standard"

.field public static final BUBBLE_TALL:Ljava/lang/String; = "global_count_bubble_tall"

.field public static final BUTTON_PLUS_MEDIUM:Ljava/lang/String; = "ic_plusone_medium"

.field public static final BUTTON_PLUS_SMALL:Ljava/lang/String; = "ic_plusone_small"

.field public static final BUTTON_PLUS_STANDARD:Ljava/lang/String; = "ic_plusone_standard"

.field public static final BUTTON_PLUS_TALL:Ljava/lang/String; = "ic_plusone_tall"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
