.class public Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
.super Ljava/lang/Object;
.source "ApplicationEntity.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/plus/model/apps/Application;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mApplicationInfo:Landroid/content/pm/ApplicationInfo;

.field private final mDisplayName:Ljava/lang/String;

.field private final mIconUrl:Ljava/lang/String;

.field private final mId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity$1;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mIconUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mId:Ljava/lang/String;

    const-class v0, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/plus/model/apps/Application;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/apps/Application;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mDisplayName:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/apps/Application;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mIconUrl:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/apps/Application;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mId:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/apps/Application;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    return-void
.end method

.method public static from(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/model/apps/Application;

    if-nez p0, :cond_0

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_0
    instance-of v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Lcom/google/android/gms/plus/model/apps/Application;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public freeze()Lcom/google/android/gms/plus/model/apps/Application;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic freeze()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->freeze()Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mIconUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
