.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;
.super Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "PersonLoadedBinderCallbacks"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;

    return-void
.end method


# virtual methods
.method public onPersonLoaded(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const-string v3, "pendingIntent"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    :cond_0
    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v2, p1, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    if-nez p3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    new-instance v4, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedCallback;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v2, v7}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedCallback;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/people/Person;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    :goto_0
    return-void

    :cond_1
    sget-object v3, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/people/PersonEntityCreator;

    invoke-virtual {p3, v3}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->inflate(Landroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    new-instance v4, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedCallback;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;

    invoke-direct {v4, v5, v6, v2, v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedCallback;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/people/Person;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    goto :goto_0
.end method
