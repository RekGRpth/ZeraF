.class public interface abstract Lcom/google/android/gms/plus/internal/PlusContent$PlusProfiles;
.super Ljava/lang/Object;
.source "PlusContent.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PlusProfiles"
.end annotation


# static fields
.field public static final BASE_PATH:Ljava/lang/String; = "profiles"

.field public static final COLUMN_3P_PROFILE_JSON:Ljava/lang/String; = "profileJson"

.field public static final COLUMN_ACCOUNT_NAME:Ljava/lang/String; = "accountName"

.field public static final COLUMN_PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field public static final COLUMN_UPDATED:Ljava/lang/String; = "updated"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lcom/google/android/gms/plus/internal/PlusContent;->ACTION_AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "profiles"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusContent$PlusProfiles;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method
