.class public Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.super Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;
.source "PlusOneButtonWithPopupContentView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;,
        Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PlusOneWithPopupLoadedListener;
    }
.end annotation


# static fields
.field static final LAYOUT_CONFIRMATION:Ljava/lang/String; = "plus_popup_confirmation"

.field static final LAYOUT_TEXT:Ljava/lang/String; = "plus_popup_text"

.field static final POPUP_BEAK_BG_URI:Landroid/net/Uri;

.field static final POPUP_BEAK_DOWN_URI:Landroid/net/Uri;

.field static final POPUP_BEAK_UP_URI:Landroid/net/Uri;


# instance fields
.field private mAnonymous:Z

.field private mAttachedToWindow:Z

.field protected mClickPending:Z

.field private final mContext:Landroid/content/Context;

.field private final mDismissPopupRunnable:Ljava/lang/Runnable;

.field private final mDisplay:Landroid/view/Display;

.field private mExternalOnClickListener:Landroid/view/View$OnClickListener;

.field private final mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

.field private final mPlusOneNoOpListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

.field private final mPopupBeak:Landroid/widget/ImageView;

.field private final mPopupBg:Landroid/widget/ImageView;

.field private mPopupWindow:Landroid/widget/PopupWindow;

.field private mResultPending:Z

.field private mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

.field private final mSignUpStateListener:Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

.field private mUserClicked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "plus_one_button_popup_beak_up"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ResourceUtils;->getDrawableUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->POPUP_BEAK_UP_URI:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_beak_down"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ResourceUtils;->getDrawableUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->POPUP_BEAK_DOWN_URI:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_bg"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ResourceUtils;->getDrawableUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->POPUP_BEAK_BG_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDismissPopupRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PlusOneWithPopupLoadedListener;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PlusOneWithPopupLoadedListener;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneNoOpListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpStateListener:Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDisplay:Landroid/view/Display;

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupBeak:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupBeak:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->POPUP_BEAK_UP_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupBg:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupBg:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->POPUP_BEAK_BG_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
    .param p1    # Landroid/widget/PopupWindow;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Lcom/google/android/gms/plus/data/plusone/SignUpState;)Lcom/google/android/gms/plus/data/plusone/SignUpState;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
    .param p1    # Lcom/google/android/gms/plus/data/plusone/SignUpState;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneNoOpListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/view/Display;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDisplay:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupBeak:Landroid/widget/ImageView;

    return-object v0
.end method

.method private createConfirmationContentView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/widget/FrameLayout;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v4, "plus_popup_confirmation"

    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->getLayout(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->createProfileImageView(Ljava/lang/String;)Landroid/widget/ImageView;

    move-result-object v1

    const-string v4, "profile_image"

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    const-string v4, "text"

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->wrapWithFrameLayout(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v4

    return-object v4
.end method

.method private createProfileImageView(Ljava/lang/String;)Landroid/widget/ImageView;
    .locals 6
    .param p1    # Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    const/4 v3, 0x1

    const/high16 v4, 0x42000000

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    new-instance v3, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;

    invoke-direct {v3, p1}, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;-><init>(Ljava/lang/String;)V

    float-to-int v4, v0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->setSize(I)Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;->build()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/plus/internal/PlusContent$Image;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->loadFromUri(Landroid/net/Uri;)V

    return-object v1
.end method

.method private createTextConfirmationContentView(Ljava/lang/String;)Landroid/widget/FrameLayout;
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v2, "plus_popup_text"

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->getLayout(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    const-string v2, "text"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->wrapWithFrameLayout(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v2

    return-object v2
.end method

.method private dismissPopupWindow()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method private getLayout(Ljava/lang/String;)Landroid/view/View;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mGmsResources:Landroid/content/res/Resources;

    const-string v2, "layout"

    const-string v3, "com.google.android.gms"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mGmsLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mGmsResources:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method private initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v0, 0x0

    invoke-super {p0, p1, p2, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    iput-boolean p3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAnonymous:Z

    return-void
.end method

.method private showConfirmationPopup()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x0

    iget-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUserClicked:Z

    if-eqz v4, :cond_1

    iput-boolean v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUserClicked:Z

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/data/plusone/SignUpState;->getProfileImageUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getConfirmationMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/data/plusone/SignUpState;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getVisibility()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->createConfirmationContentView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/widget/FrameLayout;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->showPopup(Landroid/widget/FrameLayout;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v4, "PlusOneButtonWithPopup"

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "PlusOneButtonWithPopup"

    const-string v5, "Confirmation popup requested but content view cannot be created"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setType(I)V

    goto :goto_0
.end method

.method private showTextConfirmationPopup()V
    .locals 4

    const/4 v3, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUserClicked:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUserClicked:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getConfirmationMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->createTextConfirmationContentView(Ljava/lang/String;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->showPopup(Landroid/widget/FrameLayout;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "PlusOneButtonWithPopup"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "PlusOneButtonWithPopup"

    const-string v2, "Text confirmation popup requested but text is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setType(I)V

    goto :goto_0
.end method

.method private wrapWithFrameLayout(Landroid/view/View;)Landroid/widget/FrameLayout;
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, -0x1

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method


# virtual methods
.method public cancelClick()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mTogglePending:Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->updateView()V

    return-void
.end method

.method public initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;Z)V

    return-void
.end method

.method public initializeAnonymous(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;Z)V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v1, 0x2

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z

    if-eqz v0, :cond_1

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: result pending, ignoring +1 button click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAnonymous:Z

    if-eqz v0, :cond_4

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: anonymous button, forwarding to external click listener"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mExternalOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mExternalOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_3
    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mClickPending:Z

    goto :goto_0

    :cond_4
    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUserClicked:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    if-nez v0, :cond_7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUserClicked:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: reload +1"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/PlusClient;->loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mExternalOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mExternalOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->hasPlusOne()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: undo +1"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/PlusClient;->deletePlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: +1"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/plus/PlusClient;->insertPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z

    goto :goto_1
.end method

.method public onConnected()V
    .locals 3

    const-string v0, "PlusOneButtonWithPopup"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/PlusClient;->loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAnonymous:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpStateListener:Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient;->getSignUpState(Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;)V

    :cond_2
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAnonymous:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->showDisabled()V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    if-ne p1, p0, :cond_0

    invoke-super {p0, p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mExternalOnClickListener:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method

.method public showDisabled()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showDisabled()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->dismissPopupWindow()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->showTextConfirmationPopup()V

    return-void
.end method

.method public showNotPlusOned()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showNotPlusOned()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->dismissPopupWindow()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->showTextConfirmationPopup()V

    return-void
.end method

.method public showPlusOned()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showPlusOned()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->dismissPopupWindow()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->showConfirmationPopup()V

    return-void
.end method

.method protected showPopup(Landroid/widget/FrameLayout;)V
    .locals 7
    .param p1    # Landroid/widget/FrameLayout;

    const/4 v5, 0x0

    const/4 v4, -0x2

    new-instance v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Landroid/widget/FrameLayout;)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-boolean v3, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->showPopupAboveButton:Z

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->POPUP_BEAK_DOWN_URI:Landroid/net/Uri;

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakGravity:I

    invoke-direct {v1, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget v3, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginLeft:I

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v4, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginRight:I

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v1, v3, v5, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p1, v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v3, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v5

    invoke-direct {v3, p1, v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-boolean v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;

    const/16 v4, 0x33

    iget v5, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->popupX:I

    iget v6, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->popupY:I

    invoke-virtual {v3, p0, v4, v5, v6}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDismissPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDismissPopupRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xbb8

    invoke-virtual {p0, v3, v4, v5}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void

    :cond_1
    sget-object v3, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->POPUP_BEAK_UP_URI:Landroid/net/Uri;

    goto :goto_0
.end method
