.class public Lcom/google/android/gms/plus/apps/ListAppsImageManager;
.super Ljava/lang/Object;
.source "ListAppsImageManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;,
        Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/google/android/gms/plus/apps/ListAppsImageManager;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mHandler:Landroid/os/Handler;

.field private final mListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;

.field private final mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mListeners:Ljava/util/HashSet;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mApplicationContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/common/server/BaseApiaryServer;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/BaseApiaryServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mResources:Landroid/content/res/Resources;

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    const/16 v1, 0x140

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Lcom/google/android/gms/common/server/BaseApiaryServer;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/graphics/BitmapFactory$Options;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ListAppsImageManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mInstance:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mInstance:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mInstance:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x1

    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    const/4 v4, 0x0

    :cond_0
    return v4

    :pswitch_0
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mResources:Landroid/content/res/Resources;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->access$400(Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->access$500(Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;)Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v5

    invoke-interface {v2, v5, v0}, Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;->imageLoaded(Lcom/google/android/gms/plus/model/apps/Application;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->access$500(Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;)Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v2, v5, v6}, Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;->imageLoaded(Lcom/google/android/gms/plus/model/apps/Application;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public loadImage(Lcom/google/android/gms/plus/model/apps/Application;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;-><init>(Lcom/google/android/gms/plus/apps/ListAppsImageManager;Lcom/google/android/gms/plus/model/apps/Application;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public registerListener(Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public unregisterListener(Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method
