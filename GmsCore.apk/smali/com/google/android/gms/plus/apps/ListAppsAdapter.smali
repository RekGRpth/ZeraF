.class public Lcom/google/android/gms/plus/apps/ListAppsAdapter;
.super Landroid/widget/BaseAdapter;
.source "ListAppsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;
    }
.end annotation


# instance fields
.field private final mAppCache:Lcom/google/android/gms/plus/apps/AppDisplayCache;

.field private final mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;

.field private mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;

    invoke-static {p1}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/AppDisplayCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mAppCache:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    return-void
.end method

.method private updateView(ILandroid/view/View;Lcom/google/android/gms/plus/model/apps/Application;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/gms/plus/model/apps/Application;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mAppCache:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    invoke-virtual {v3, p3}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getData(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    move-result-object v0

    const v3, 0x7f0a007d

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0a003c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p3}, Lcom/google/android/gms/plus/model/apps/Application;->getIconUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->isDefaultImage()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;

    invoke-interface {v3, p3, v1}, Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;->loadImage(Lcom/google/android/gms/plus/model/apps/Application;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Lcom/google/android/gms/plus/model/apps/Application;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;->get(I)Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->getItem(I)Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04002a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;->get(I)Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->updateView(ILandroid/view/View;Lcom/google/android/gms/plus/model/apps/Application;)V

    return-object p2
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public swapDataBuffer(Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;)Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    if-ne p1, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mDataBuffer:Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method public updateImage(Lcom/google/android/gms/plus/model/apps/Application;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p2    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->mAppCache:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->updateDrawable(Lcom/google/android/gms/plus/model/apps/Application;Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->notifyDataSetChanged()V

    return-void
.end method
