.class public Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "DisconnectSourceFragment.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAppToDisconnect:Lcom/google/android/gms/plus/model/apps/Application;

.field private mCallbacks:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

.field private mDeleteAllFrames:Z

.field private mDisconnectApp:Z

.field private mIsConnecting:Z

.field private mPlusClient:Lcom/google/android/gms/plus/PlusClient;

.field private mResult:Lcom/google/android/gms/common/ConnectionResult;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;
    .locals 3
    .param p0    # Landroid/accounts/Account;

    new-instance v1, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public disconnectApplication(Lcom/google/android/gms/plus/model/apps/Application;Z)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p2    # Z

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can only disconnect one app at a time."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->from(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mAppToDisconnect:Lcom/google/android/gms/plus/model/apps/Application;

    iput-boolean p2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDeleteAllFrames:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->onConnected()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DisconnectSourceFragment must be hosted by an activity that implements DisconnectSourceCallbacks."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onConnected()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mAppToDisconnect:Lcom/google/android/gms/plus/model/apps/Application;

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/apps/Application;->getId()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDeleteAllFrames:Z

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/plus/PlusClient;->disconnectSource(Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mResult:Lcom/google/android/gms/common/ConnectionResult;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mAppToDisconnect:Lcom/google/android/gms/plus/model/apps/Application;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;->onSourceDisconnected(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/apps/Application;)V

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mAccount:Landroid/accounts/Account;

    new-instance v0, Lcom/google/android/gms/plus/PlusClient$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcom/google/android/gms/plus/PlusClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "https://www.googleapis.com/auth/plus.applications.manage"

    aput-object v2, v1, v4

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->setScopes([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "service_googleme"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->setRequiredFeatures([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient$Builder;->build()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    :cond_1
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    iput-object v2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mAppToDisconnect:Lcom/google/android/gms/plus/model/apps/Application;

    return-void
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    return-void
.end method

.method public onDisconnected()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mIsConnecting:Z

    goto :goto_0
.end method

.method public onSourceDisconnected(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mResult:Lcom/google/android/gms/common/ConnectionResult;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mAppToDisconnect:Lcom/google/android/gms/plus/model/apps/Application;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;->onSourceDisconnected(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/apps/Application;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->mDisconnectApp:Z

    return-void
.end method
