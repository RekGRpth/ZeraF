.class public Lcom/google/android/gms/plus/data/plusone/PlusOne;
.super Ljava/lang/Object;
.source "PlusOne.java"


# instance fields
.field mBundle:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;[Landroid/net/Uri;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # [Landroid/net/Uri;
    .param p6    # Landroid/content/Intent;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "has_plus_one"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "bubble_text"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "inline_annotations"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "profile_photo_uris"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "token"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "confirmation_message"

    invoke-virtual {v0, v1, p8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "visibility"

    invoke-virtual {v0, v1, p9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public asBundle()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getBubbleText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "bubble_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConfirmationMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "confirmation_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInlineAnnotationText()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "inline_annotations"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method public getProfilePhotoUris()[Landroid/net/Uri;
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v3, "profile_photo_uris"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    array-length v2, v0

    new-array v1, v2, [Landroid/net/Uri;

    array-length v2, v0

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public getToken()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVisibility()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "visibility"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasPlusOne()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "has_plus_one"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
