.class public final Lcom/google/android/gms/ads/settings/Crypto;
.super Ljava/lang/Object;
.source "Crypto.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byteArrayToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0    # [B

    const/16 v3, 0x10

    new-array v0, v3, [C

    fill-array-data v0, :array_0

    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_0

    aget-byte v3, p0, v1

    and-int/lit16 v3, v3, 0xf0

    ushr-int/lit8 v3, v3, 0x4

    aget-char v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    aget-byte v3, p0, v1

    and-int/lit8 v3, v3, 0xf

    aget-char v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public static calculateCrc([BII)V
    .locals 8
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    const v7, 0xfff1

    const v0, 0xfff1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v3, p1

    :goto_0
    add-int/lit8 v6, v3, 0x10

    if-gt v6, p2, :cond_1

    const/4 v2, 0x0

    :goto_1
    const/16 v6, 0x10

    if-ge v2, v6, :cond_0

    add-int v6, v3, v2

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    add-int/2addr v4, v6

    add-int/2addr v5, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x10

    goto :goto_0

    :cond_1
    :goto_2
    if-ge v3, p2, :cond_2

    aget-byte v6, p0, v3

    and-int/lit16 v6, v6, 0xff

    add-int/2addr v4, v6

    add-int/2addr v5, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    rem-int/2addr v4, v7

    rem-int/2addr v5, v7

    shl-int/lit8 v6, v5, 0x10

    or-int v1, v6, v4

    invoke-static {v1, p0, p2}, Lcom/google/android/gms/ads/settings/Crypto;->loadInt(I[BI)V

    return-void
.end method

.method public static calculateMd5(Ljava/lang/String;)[B
    .locals 3
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    return-object v1
.end method

.method public static encryptMobileId(IILjava/lang/String;)[B
    .locals 17
    .param p0    # I
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/16 v4, 0x8

    const/16 v3, 0x28

    const/16 v2, 0x2c

    const/16 v5, 0x30

    const/16 v1, 0x100

    const/16 v15, 0x100

    new-array v13, v15, [B

    const/4 v15, 0x0

    move/from16 v0, p0

    invoke-static {v0, v13, v15}, Lcom/google/android/gms/ads/settings/Crypto;->loadInt(I[BI)V

    const/4 v15, 0x4

    move/from16 v0, p1

    invoke-static {v0, v13, v15}, Lcom/google/android/gms/ads/settings/Crypto;->loadInt(I[BI)V

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/ads/settings/Crypto;->calculateMd5(Ljava/lang/String;)[B

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/gms/ads/settings/Crypto;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v12

    const-string v15, "UTF-8"

    invoke-virtual {v12, v15}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    const/4 v9, 0x0

    :goto_0
    array-length v15, v10

    if-ge v9, v15, :cond_0

    add-int/lit8 v15, v9, 0x8

    aget-byte v16, v10, v9

    aput-byte v16, v13, v15

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v15, 0x0

    const/16 v16, 0x28

    move/from16 v0, v16

    invoke-static {v15, v13, v0}, Lcom/google/android/gms/ads/settings/Crypto;->loadInt(I[BI)V

    const/4 v15, 0x0

    const/16 v16, 0x2c

    move/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/google/android/gms/ads/settings/Crypto;->calculateCrc([BII)V

    new-instance v14, Ljava/util/Random;

    invoke-direct {v14}, Ljava/util/Random;-><init>()V

    const/16 v9, 0x30

    :goto_1
    const/16 v15, 0x100

    if-ge v9, v15, :cond_1

    const/16 v15, 0x100

    invoke-virtual {v14, v15}, Ljava/util/Random;->nextInt(I)I

    move-result v15

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v13, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    const/16 v15, 0x100

    new-array v8, v15, [B

    new-instance v15, Lcom/google/android/gms/ads/settings/Encryptor;

    invoke-direct {v15}, Lcom/google/android/gms/ads/settings/Encryptor;-><init>()V

    invoke-virtual {v15, v13, v8}, Lcom/google/android/gms/ads/settings/Encryptor;->encrypt([B[B)V

    return-object v8
.end method

.method private static loadInt(I[BI)V
    .locals 2
    .param p0    # I
    .param p1    # [B
    .param p2    # I

    and-int/lit16 v0, p0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, p2

    add-int/lit8 v0, p2, 0x1

    const v1, 0xff00

    and-int/2addr v1, p0

    ushr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x2

    const/high16 v1, 0xff0000

    and-int/2addr v1, p0

    ushr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x3

    const/high16 v1, -0x1000000

    and-int/2addr v1, p0

    ushr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    return-void
.end method
