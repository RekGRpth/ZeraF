.class public final Lcom/google/android/gms/ads/settings/AdsSettingsActivity$AdsSettingsAdapter;
.super Ljava/lang/Object;
.source "AdsSettingsActivity.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/ads/settings/AdsSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "AdsSettingsAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity$AdsSettingsAdapter;->this$0:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0
    .param p1    # I

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity$AdsSettingsAdapter;->this$0:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    # getter for: Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mAdsCheckBox:Lcom/google/android/gms/ads/settings/AdsCheckBox;
    invoke-static {v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->access$000(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)Lcom/google/android/gms/ads/settings/AdsCheckBox;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity$AdsSettingsAdapter;->this$0:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    # invokes: Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->setupLearnMoreLinkView()Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->access$100(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method
