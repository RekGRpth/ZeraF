.class public final Lcom/google/android/gms/icing/Proto;
.super Ljava/lang/Object;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$1;,
        Lcom/google/android/gms/icing/Proto$UsageStats;,
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec;,
        Lcom/google/android/gms/icing/Proto$Document;,
        Lcom/google/android/gms/icing/Proto$Enums;,
        Lcom/google/android/gms/icing/Proto$InitStatus;,
        Lcom/google/android/gms/icing/Proto$CompactStatus;,
        Lcom/google/android/gms/icing/Proto$FlushStatus;,
        Lcom/google/android/gms/icing/Proto$CorpusData;,
        Lcom/google/android/gms/icing/Proto$CorpusStatusProto;,
        Lcom/google/android/gms/icing/Proto$CorpusConfig;,
        Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;,
        Lcom/google/android/gms/icing/Proto$SectionConfig;
    }
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static internalForceInit()V
    .locals 0

    return-void
.end method
