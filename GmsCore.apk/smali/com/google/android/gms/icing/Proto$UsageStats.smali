.class public final Lcom/google/android/gms/icing/Proto$UsageStats;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UsageStats"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$UsageStats$Builder;,
        Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats;


# instance fields
.field private corpus_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$UsageStats;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$UsageStats;->defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$UsageStats;->defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$UsageStats;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->corpus_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UsageStats;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UsageStats;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->corpus_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$22500(Lcom/google/android/gms/icing/Proto$UsageStats;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->corpus_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$22502(Lcom/google/android/gms/icing/Proto$UsageStats;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->corpus_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$UsageStats;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$UsageStats;->defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$UsageStats$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$UsageStats$Builder;->create()Lcom/google/android/gms/icing/Proto$UsageStats$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats$Builder;->access$22300()Lcom/google/android/gms/icing/Proto$UsageStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/gms/icing/Proto$UsageStats;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats;->newBuilder()Lcom/google/android/gms/icing/Proto$UsageStats$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UsageStats$Builder;

    # invokes: Lcom/google/android/gms/icing/Proto$UsageStats$Builder;->buildParsed()Lcom/google/android/gms/icing/Proto$UsageStats;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Builder;->access$22200(Lcom/google/android/gms/icing/Proto$UsageStats$Builder;)Lcom/google/android/gms/icing/Proto$UsageStats;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCorpus(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->corpus_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    return-object v0
.end method

.method public getCorpusCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->corpus_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCorpusList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->corpus_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v2, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats;->getCorpusList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_1
    iput v2, p0, Lcom/google/android/gms/icing/Proto$UsageStats;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$UsageStats$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats;->newBuilder()Lcom/google/android/gms/icing/Proto$UsageStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats;->newBuilderForType()Lcom/google/android/gms/icing/Proto$UsageStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats;->getCorpusList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_0
    return-void
.end method
