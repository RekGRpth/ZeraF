.class public final Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;",
        "Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$9400()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->create()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->build()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->create()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->clone()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->clone()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->clone()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasCorpusId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getCorpusId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasLastSeqno()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getLastSeqno()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->setLastSeqno(J)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->setLastSeqno(J)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setCorpusId(I)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasCorpusId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->access$9602(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    # setter for: Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->corpusId_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->access$9702(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;I)I

    return-object p0
.end method

.method public setLastSeqno(J)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasLastSeqno:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->access$9802(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->result:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    # setter for: Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->lastSeqno_:J
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->access$9902(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;J)J

    return-object p0
.end method
