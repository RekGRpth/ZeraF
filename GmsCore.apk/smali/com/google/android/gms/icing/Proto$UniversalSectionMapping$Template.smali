.class public final Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Template"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;,
        Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;


# instance fields
.field private arguments_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;",
            ">;"
        }
    .end annotation
.end field

.field private formatPart_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->formatPart_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->arguments_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->formatPart_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->arguments_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->formatPart_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->formatPart_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->arguments_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->arguments_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;->create()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;->access$2100()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getArgumentsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->arguments_:Ljava/util/List;

    return-object v0
.end method

.method public getFormatPartList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->formatPart_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v3, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getFormatPartList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    :cond_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getFormatPartList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getArgumentsList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;

    const/4 v5, 0x2

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_2

    :cond_2
    iput v3, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->memoizedSerializedSize:I

    move v4, v3

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->newBuilderForType()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getFormatPartList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getArgumentsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_1
    return-void
.end method
