.class public abstract Lcom/google/android/gms/icing/impl/KeepAwake;
.super Ljava/lang/Object;
.source "KeepAwake.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/impl/KeepAwake$Wrapper;
    }
.end annotation


# instance fields
.field mDead:Z

.field private mLock:Ljava/lang/Object;

.field mRefs:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mRefs:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mDead:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mLock:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public acquire()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mRefs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mRefs:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mDead:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isDead()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mDead:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract onDead()V
.end method

.method public release()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mRefs:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Released without acquire"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mRefs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mRefs:I

    iget v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mRefs:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake;->mDead:Z

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/KeepAwake;->onDead()V

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
