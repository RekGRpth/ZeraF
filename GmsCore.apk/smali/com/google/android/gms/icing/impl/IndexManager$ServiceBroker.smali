.class final Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;
.super Lcom/google/android/gms/common/internal/AbstractServiceBroker;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/IndexManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceBroker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/AbstractServiceBroker;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;Lcom/google/android/gms/icing/impl/IndexManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p2    # Lcom/google/android/gms/icing/impl/IndexManager$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public getAppDataSearchService(Lcom/google/android/gms/common/internal/IGmsCallbacks;ILjava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/internal/IGmsCallbacks;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/IndexManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Not initialized"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-interface {p1, v3, v2, v0}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/IndexManager;->access$100(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/AppAuthenticator;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->isAppAllowed(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "App "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not allowed to use icing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-interface {p1, v3, v2, v0}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_0
.end method
