.class public Lcom/google/android/gms/icing/impl/CorpusMap;
.super Ljava/lang/Object;
.source "CorpusMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/impl/CorpusMap$1;,
        Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;
    }
.end annotation


# static fields
.field private static final sTokenizerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCorpusKeyMaccer:Ljavax/crypto/Mac;

.field mCorpusKeyToData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/icing/Proto$CorpusData;",
            ">;"
        }
    .end annotation
.end field

.field mPackageNameToCorpusKeys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPrefsConfig:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/icing/impl/CorpusMap;->sTokenizerMap:Ljava/util/Map;

    sget-object v0, Lcom/google/android/gms/icing/impl/CorpusMap;->sTokenizerMap:Ljava/util/Map;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->TOKENIZER_TEXT:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/icing/impl/CorpusMap;->sTokenizerMap:Ljava/util/Map;

    const-string v1, "plain"

    sget-object v2, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->TOKENIZER_TEXT:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/icing/impl/CorpusMap;->sTokenizerMap:Ljava/util/Map;

    const-string v1, "html"

    sget-object v2, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->TOKENIZER_HTML:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/icing/impl/CorpusMap;->sTokenizerMap:Ljava/util/Map;

    const-string v1, "rfc822"

    sget-object v2, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->TOKENIZER_RFC822_FORMATTED:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    const/4 v4, 0x0

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    const-string v5, "created"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "created"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/CorpusMap;->setupMaccer()V

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "corpuskey:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "corpuskey:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/Utils;->stringToProtoMessage(Ljava/lang/String;Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->addCorpusData(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusData;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private addCorpusData(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusData;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$CorpusData;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static corpusDataToString(Lcom/google/android/gms/icing/Proto$CorpusData;)Ljava/lang/String;
    .locals 12
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;

    const/16 v9, 0xa

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/icing/Proto$SectionConfig;

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getIndexed()Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "(noindex)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    sget-object v7, Lcom/google/android/gms/icing/impl/CorpusMap$1;->$SwitchMap$com$google$android$gms$icing$Proto$SectionConfig$Tokenizer:[I

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getTokenizer()Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v3, v4

    goto :goto_0

    :pswitch_1
    const-string v7, "(html)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_2
    const-string v7, "(rfc822)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getCounterList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    const-string v7, "  "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ": "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getCount()I

    move-result v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    const-string v7, "id:%d\nname:%s\nversion:\"%s\"\npackage:%s\nuri:%s\nstate:%s\nlast indexed/flushed:%d/%d\nsections:\n%scounters:\n%s"

    new-array v8, v9, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getVersion()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastFlushedSeqno()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/16 v9, 0x8

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/16 v9, 0x9

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final declared-synchronized getActiveCorpusKeys()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_1
    monitor-exit p0

    return-object v2
.end method

.method private final getConfigData(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusData;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusData;

    return-object v0
.end method

.method private getNewCorpusId()I
    .locals 8

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/util/BitSet;->length()I

    move-result v6

    invoke-virtual {v0}, Ljava/util/BitSet;->cardinality()I

    move-result v7

    if-ne v6, v7, :cond_2

    invoke-virtual {v0}, Ljava/util/BitSet;->length()I

    move-result v3

    :cond_1
    return v3

    :cond_2
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/BitSet;->length()I

    move-result v6

    if-ge v3, v6, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v6

    if-eqz v6, :cond_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static final getPackageNamesForUid(Landroid/content/Context;I)Ljava/util/Set;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    new-array v0, v2, [Ljava/lang/String;

    :cond_0
    new-instance v1, Ljava/util/HashSet;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-object v1
.end method

.method public static final getSectionMap(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/Map;
    .locals 6
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/icing/Proto$CorpusConfig;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsCount()I

    move-result v2

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;

    invoke-direct {v5, v0, v3}, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;-><init>(ILcom/google/android/gms/icing/Proto$SectionConfig;)V

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static makeCorpusConfig(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v1

    iget-object v6, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setPackageName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    iget-object v6, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->version:Ljava/lang/String;

    invoke-virtual {v1, v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setVersion(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    iget-object v6, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setContentProviderUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->sections:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    sget-object v6, Lcom/google/android/gms/icing/impl/CorpusMap;->sTokenizerMap:Ljava/util/Map;

    iget-object v7, v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->format:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v6

    iget-object v7, v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v7

    iget-boolean v6, v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->noIndex:Z

    if-nez v6, :cond_0

    const/4 v6, 0x1

    :goto_1
    invoke-virtual {v7, v6}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setIndexed(Z)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setTokenizer(Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v6

    iget v7, v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->weight:I

    invoke-virtual {v6, v7}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setWeight(I)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->addSections(Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->getUniversalSectionMappings(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->addAllUniversalSectionMappings(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    return-object v1
.end method

.method private removeCorpusKey(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Z
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$CorpusData;

    if-nez v1, :cond_0

    const-string v5, "Can\'t find corpus with key %s"

    invoke-static {v5, p1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return v4

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "Can\'t remove corpus %s, not in expected state %s, actual state=%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v5

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    const/4 v4, 0x2

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v6, v7}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    move v4, v5

    goto :goto_0

    :cond_1
    const-string v6, "Removing inactive corpus %s"

    invoke-static {v6, v2}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v5, "Removing inactive package %s"

    invoke-static {v5, v3}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_0
.end method

.method private sameMappings(Ljava/util/List;Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;)Z"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    if-eq v5, v6, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getUniversalSectionId()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getUniversalSectionId()I

    move-result v6

    if-ne v5, v6, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/common/internal/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_0

    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private setFlushStatus(Landroid/content/SharedPreferences$Editor;Lcom/google/android/gms/icing/Proto$FlushStatus;)V
    .locals 11
    .param p1    # Landroid/content/SharedPreferences$Editor;
    .param p2    # Lcom/google/android/gms/icing/Proto$FlushStatus;

    if-nez p2, :cond_0

    const-string v6, "flushstatus"

    const/4 v7, 0x0

    invoke-interface {p1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v7

    invoke-direct {p0, v6, v7, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->setStatusWithEditor(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$FlushStatus;->toBuilder()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->setTimestampSecs(J)Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->build()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object p2

    const-string v6, "flushstatus"

    invoke-static {p2}, Lcom/google/android/gms/icing/impl/Utils;->protoMessageToString(Lcom/google/protobuf/MessageLite;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v2

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setLastFlushedSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v6, v4, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->setStatusWithEditor(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Landroid/content/SharedPreferences$Editor;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private setStatusWithEditor(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Landroid/content/SharedPreferences$Editor;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p3    # Landroid/content/SharedPreferences$Editor;

    const-string v2, "Setting status for %s"

    invoke-static {v2, p1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfigData(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "Cannot set status for non-existent key: %s"

    invoke-static {v2, p1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->setStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "corpuskey:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/Utils;->protoMessageToString(Lcom/google/protobuf/MessageLite;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p3, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private setupMaccer()V
    .locals 7

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    const-string v5, "hmackey"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "hmackey"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "UTF-8"

    invoke-virtual {v1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    const-string v5, "HmacSHA1"

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const-string v4, "HmacSHA1"

    invoke-static {v4}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyMaccer:Ljavax/crypto/Mac;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyMaccer:Ljavax/crypto/Mac;

    invoke-virtual {v4, v3}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2

    return-void

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "CannotHappenException"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_1
    move-exception v0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Cannot find algo"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_2
    move-exception v0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Bad key type"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method


# virtual methods
.method public authenticateAgainstCorpora(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Z)Ljava/util/Set;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p5, :cond_2

    invoke-static {p1, p2}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->canQueryUniversal(Landroid/content/Context;I)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getActiveCorpusKeys()Ljava/util/Set;

    move-result-object v2

    :cond_1
    :goto_0
    if-eqz p4, :cond_6

    new-instance v7, Ljava/util/ArrayList;

    array-length v8, p4

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, p4

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_5

    aget-object v1, v0, v3

    invoke-virtual {p0, p3, v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->makeCorpusKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p1, p2}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->hasMyUid(Landroid/content/Context;I)Z

    move-result v8

    if-nez v8, :cond_0

    :cond_3
    if-eqz p3, :cond_4

    new-instance v2, Ljava/util/HashSet;

    invoke-virtual {p0, p3}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusKeysForPackageName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {p1, p2}, Lcom/google/android/gms/icing/impl/CorpusMap;->getPackageNamesForUid(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusKeysForPackageName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_5
    invoke-interface {v2, v7}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    :cond_6
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v8

    return-object v8
.end method

.method public declared-synchronized clear()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "created"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/CorpusMap;->setupMaccer()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized dump(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1    # Ljava/io/PrintWriter;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "No corprora\n"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v3, "%s:\n%s\n"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->corpusDataToString(Lcom/google/android/gms/icing/Proto$CorpusData;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {p1, v3, v4}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized finishDeactivateCorpus(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v6, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Key "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " doesn\'t exist for pkg "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :cond_0
    const/4 v5, 0x0

    :try_start_1
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusData;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v6, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-direct {p0, p1, v6}, Lcom/google/android/gms/icing/impl/CorpusMap;->removeCorpusKey(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Z

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "corpuskey:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->INACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->setStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->addCorpusData(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusData;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "corpuskey:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/Utils;->protoMessageToString(Lcom/google/protobuf/MessageLite;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    :cond_1
    monitor-exit p0

    return v5
.end method

.method public final declared-synchronized getActiveCorpusConfigs()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusConfig;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_1
    monitor-exit p0

    return-object v2
.end method

.method public final declared-synchronized getAllPackageNames()[Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getCompactStatus()Lcom/google/android/gms/icing/Proto$CompactStatus;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    const-string v1, "compactstatus"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CompactStatus;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/Utils;->stringToProtoMessage(Ljava/lang/String;Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CompactStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfigData(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getCorpusConfigsOfState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Ljava/util/Map;
    .locals 5
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/icing/Proto$CorpusConfig;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_1
    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getCorpusIdToPackageMapping()[J
    .locals 10

    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    const/4 v7, 0x0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v5

    if-le v5, v7, :cond_0

    move v7, v5

    goto :goto_0

    :cond_1
    add-int/lit8 v8, v7, 0x1

    new-array v6, v8, [J

    new-instance v1, Ljava/util/zip/CRC32;

    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-virtual {v1}, Ljava/util/zip/CRC32;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "UTF-8"

    invoke-virtual {v8, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/zip/CRC32;->update([B)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v5

    invoke-virtual {v1}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v8

    aput-wide v8, v6, v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    :catch_0
    move-exception v2

    :try_start_3
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "CannotHappenException"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    monitor-exit p0

    return-object v6
.end method

.method public final declared-synchronized getCorpusKeysForPackageName(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-nez v4, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    :goto_0
    monitor-exit p0

    return-object v5

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$CorpusData;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_2
    :try_start_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    goto :goto_0
.end method

.method public declared-synchronized getCreationTimestamp()J
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    const-string v1, "created"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getFlushStatus()Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    const-string v1, "flushstatus"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/Utils;->stringToProtoMessage(Ljava/lang/String;Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/MessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$FlushStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getStatus(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfigData(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized makeCorpusKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyMaccer:Ljavax/crypto/Mac;

    invoke-virtual {v2}, Ljavax/crypto/Mac;->reset()V

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyMaccer:Ljavax/crypto/Mac;

    invoke-virtual {v2, v1}, Ljavax/crypto/Mac;->update([B)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyMaccer:Ljavax/crypto/Mac;

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljavax/crypto/Mac;->update(B)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyMaccer:Ljavax/crypto/Mac;

    invoke-virtual {v2, v0}, Ljavax/crypto/Mac;->update([B)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyMaccer:Ljavax/crypto/Mac;

    invoke-virtual {v2}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/Utils;->bytesToHexString([B)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized purgeInactiveCorpora()Z
    .locals 7

    monitor-enter p0

    const/4 v4, 0x0

    const/4 v0, 0x0

    :try_start_0
    sget-object v5, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->INACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {p0, v5}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusConfigsOfState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->INACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-direct {p0, v3, v5}, Lcom/google/android/gms/icing/impl/CorpusMap;->removeCorpusKey(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    :cond_0
    if-nez v0, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "corpuskey:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_2
    if-eqz v0, :cond_3

    :try_start_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    monitor-exit p0

    return v4
.end method

.method public declared-synchronized resetCorporaData()V
    .locals 6

    monitor-enter p0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v5}, Lcom/google/android/gms/icing/impl/CorpusMap;->setFlushStatus(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusData;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->setStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setCompactStatus(Lcom/google/android/gms/icing/Proto$CompactStatus;Lcom/google/android/gms/icing/Proto$FlushStatus;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/icing/Proto$CompactStatus;
    .param p2    # Lcom/google/android/gms/icing/Proto$FlushStatus;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "compactstatus"

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/Utils;->protoMessageToString(Lcom/google/protobuf/MessageLite;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/icing/impl/CorpusMap;->setFlushStatus(Landroid/content/SharedPreferences$Editor;Lcom/google/android/gms/icing/Proto$FlushStatus;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setConfig(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    monitor-enter p0

    :try_start_0
    const-string v5, "Setting config for %s"

    invoke-static {v5, p1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfigData(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getNewCorpusId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    const v5, 0xfffe

    if-le v3, v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    monitor-exit p0

    return v5

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    :goto_1
    invoke-virtual {p2, v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->hasName()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->hasPackageName()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_1
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Need name and package name"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_2
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {v0, p2}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->setConfig(Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/google/android/gms/icing/impl/CorpusMap;->addCorpusData(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusData;)V

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "corpuskey:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/Utils;->protoMessageToString(Lcom/google/protobuf/MessageLite;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v5, 0x1

    goto :goto_0
.end method

.method public declared-synchronized setFlushStatus(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$FlushStatus;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->setFlushStatus(Landroid/content/SharedPreferences$Editor;Lcom/google/android/gms/icing/Proto$FlushStatus;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setStatus(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPrefsConfig:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->setStatusWithEditor(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Landroid/content/SharedPreferences$Editor;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized startDeactivateCorpus(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mPackageNameToCorpusKeys:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const/4 v0, -0x1

    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, -0x1

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$CorpusData;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->setStatus(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized updateCurSeqnosForInit(Lcom/google/android/gms/icing/Proto$InitStatus;)V
    .locals 12
    .param p1    # Lcom/google/android/gms/icing/Proto$InitStatus;

    const/4 v11, -0x1

    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$InitStatus;->getCorpusInfoCount()I

    move-result v9

    if-ge v2, v9, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/android/gms/icing/Proto$InitStatus;->getCorpusInfo(I)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getCorpusId()I

    move-result v9

    invoke-virtual {v0, v9, v2}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v9, p0, Lcom/google/android/gms/icing/impl/CorpusMap;->mCorpusKeyToData:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v9

    const/4 v10, -0x1

    invoke-virtual {v0, v9, v10}, Landroid/util/SparseIntArray;->get(II)I

    move-result v4

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastFlushedSeqno()J

    move-result-wide v5

    if-eq v4, v11, :cond_2

    invoke-virtual {p1, v4}, Lcom/google/android/gms/icing/Proto$InitStatus;->getCorpusInfo(I)Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getLastSeqno()J

    move-result-wide v5

    :cond_2
    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v9

    cmp-long v9, v5, v9

    if-eqz v9, :cond_1

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setLastSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9, v7}, Lcom/google/android/gms/icing/impl/CorpusMap;->setStatus(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    :cond_3
    monitor-exit p0

    return-void
.end method

.method public updateIfRequired(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p3    # Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsCount()I

    move-result v1

    invoke-virtual {p3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getSectionsCount()I

    move-result v4

    if-ne v1, v4, :cond_0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-virtual {p2, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$SectionConfig;->toByteArray()[B

    move-result-object v4

    invoke-virtual {p3, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$SectionConfig;->toByteArray()[B

    move-result-object v5

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getContentProviderUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    if-nez v2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v2

    :cond_3
    invoke-virtual {p3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getContentProviderUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setContentProviderUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/icing/impl/CorpusMap;->sameMappings(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_6

    if-nez v2, :cond_5

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v2

    :cond_5
    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->clearUniversalSectionMappings()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-virtual {p3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->addAllUniversalSectionMappings(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_6
    if-eqz v2, :cond_7

    invoke-virtual {p0, p1, v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->setConfig(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z

    :cond_7
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public verifyUniqueCorpusForUid(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/google/android/gms/icing/impl/CorpusMap;->getPackageNamesForUid(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusKeysForPackageName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0, v3, p3}, Lcom/google/android/gms/icing/impl/CorpusMap;->makeCorpusKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x1

    goto :goto_0
.end method
