.class abstract Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.super Ljava/lang/Object;
.source "IndexManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/IndexManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "KeepAwakeRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mKeepAwake:Lcom/google/android/gms/icing/impl/KeepAwake;
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$000(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/KeepAwake;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/KeepAwake;->acquire()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;->runAwake()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mKeepAwake:Lcom/google/android/gms/icing/impl/KeepAwake;
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/IndexManager;->access$000(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/KeepAwake;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/KeepAwake;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mKeepAwake:Lcom/google/android/gms/icing/impl/KeepAwake;
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$000(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/KeepAwake;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/KeepAwake;->release()V

    throw v0
.end method

.method public abstract runAwake()V
.end method
