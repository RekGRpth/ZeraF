.class public Lcom/google/android/gms/icing/impl/IndexManager;
.super Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch$Stub;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/impl/IndexManager$15;,
        Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;,
        Lcom/google/android/gms/icing/impl/IndexManager$Options;,
        Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;,
        Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;,
        Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;
    }
.end annotation


# static fields
.field private static final INDEXING_STATE_CANCELED:Ljava/lang/Integer;

.field private static final INDEXING_STATE_RUNNING:Ljava/lang/Integer;

.field private static final INDEXING_STATE_WAITING:Ljava/lang/Integer;

.field private static INDEX_MIN_FREE_FRAC_THRESHOLD:D


# instance fields
.field private mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

.field private mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

.field private mContext:Landroid/content/Context;

.field private mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

.field private final mFileRoot:Ljava/io/File;

.field private mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

.field private final mIndexingState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mInitSem:Ljava/util/concurrent/Semaphore;

.field private mKeepAwake:Lcom/google/android/gms/icing/impl/KeepAwake;

.field private final mName:Ljava/lang/String;

.field private final mOptions:Lcom/google/android/gms/icing/impl/IndexManager$Options;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

.field private final mUiThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide v0, 0x3fa999999999999aL

    sput-wide v0, Lcom/google/android/gms/icing/impl/IndexManager;->INDEX_MIN_FREE_FRAC_THRESHOLD:D

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_WAITING:Ljava/lang/Integer;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_RUNNING:Ljava/lang/Integer;

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_CANCELED:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/KeepAwake;Ljava/io/File;Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$Options;Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/icing/impl/KeepAwake;
    .param p3    # Ljava/io/File;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/icing/impl/IndexManager$Options;
    .param p6    # Landroid/os/Handler;

    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch$Stub;-><init>()V

    new-instance v0, Lcom/google/android/gms/icing/impl/AsyncScheduler;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/AsyncScheduler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/icing/impl/IndexManager$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/icing/impl/IndexManager$1;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;Lcom/google/android/gms/icing/impl/KeepAwake;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mKeepAwake:Lcom/google/android/gms/icing/impl/KeepAwake;

    iput-object p6, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mUiThreadHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->start()V

    new-instance v0, Ljava/io/File;

    const-string v1, "AppDataSearch"

    invoke-direct {v0, p3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mFileRoot:Ljava/io/File;

    iput-object p4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mOptions:Lcom/google/android/gms/icing/impl/IndexManager$Options;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/KeepAwake;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mKeepAwake:Lcom/google/android/gms/icing/impl/KeepAwake;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/AppAuthenticator;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/NativeIndex;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/StorageMonitor;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->compactInternal()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->flushInternal()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/gms/icing/impl/IndexManager;D)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # D

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/icing/impl/IndexManager;->compactAndPurgeInternal(D)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->doMaintenanceInternal()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/CorpusMap;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;Lcom/google/android/gms/icing/impl/ContentCursors;)I
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/impl/ContentCursors;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProviderWithLimit(Ljava/lang/String;Lcom/google/android/gms/icing/impl/ContentCursors;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;Lcom/google/android/gms/icing/impl/ContentCursors;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;
    .param p3    # Lcom/google/android/gms/icing/impl/ContentCursors;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProvider(Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;Lcom/google/android/gms/icing/impl/ContentCursors;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->onSleep()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/gms/icing/impl/IndexManager;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->displayNotification(Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V

    return-void
.end method

.method static synthetic access$2200()Ljava/lang/Integer;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_WAITING:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2300()Ljava/lang/Integer;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_RUNNING:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2400()Ljava/lang/Integer;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_CANCELED:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProviders(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/gms/icing/impl/IndexManager;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->initIndexInternal(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->destroyInternal()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/gms/icing/impl/IndexManager;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/icing/impl/IndexManager;->registerCorpusInternal(ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->scheduleIndexing(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterAppInternal(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private canIndex(Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v3, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_CANCELED:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v2

    move v0, v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->enoughStorage()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Not enough disk space for indexing"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->i(Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private cancelIndexing(Ljava/lang/String;)Z
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v9, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    monitor-enter v9

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    monitor-exit v9

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v3, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_CANCELED:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    sget-object v3, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_CANCELED:Ljava/lang/Integer;

    invoke-interface {v0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v5

    const-wide/16 v3, 0x7530

    move v0, v2

    :cond_2
    :goto_1
    if-nez v0, :cond_3

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long v5, v7, v5

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-gtz v5, :cond_5

    :cond_3
    monitor-exit v9

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "Received unexpected interruption while canceling indexing"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move-wide v5, v7

    goto :goto_1
.end method

.method private checkInWorkerThread()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->isInThread()Z

    move-result v0

    const-string v1, "Must be in worker thread"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    return-void
.end method

.method private compactAndPurgeInternal(D)V
    .locals 7

    const-wide/high16 v5, 0x4059000000000000L

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    const-string v0, "Starting purge with target free %.3f%% min disk %.3f%% min index %.3f%%"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    mul-double v3, p1, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/StorageMonitor;->freeDiskFrac()D

    move-result-wide v3

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->minFreeFraction()D

    move-result-wide v3

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusIdToPackageMapping()[J

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->compactAndPurge(D[J)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/IndexManager;->postCompactInternal(Lcom/google/android/gms/icing/Proto$FlushStatus;)V

    return-void
.end method

.method private compactInternal()V
    .locals 6

    const-wide/high16 v4, 0x4059000000000000L

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    const-string v0, "Starting compaction min disk %.3f%% min index %.3f%%"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/StorageMonitor;->freeDiskFrac()D

    move-result-wide v1

    mul-double/2addr v1, v4

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->minFreeFraction()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->compact()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/IndexManager;->postCompactInternal(Lcom/google/android/gms/icing/Proto$FlushStatus;)V

    return-void
.end method

.method private destroyInternal()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mPrefs:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->destroy()V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    :cond_0
    return-void
.end method

.method private displayNotification(Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v6, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.icing.ui/.Indexer"

    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, v6, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;->packageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_0

    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Indexed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;->corpusName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Icing Indexer for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Finished indexing "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;->corpusName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    const v2, 0x108004f

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    iget-object v2, p1, Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;->corpusName:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/google/android/gms/icing/impl/CorpusMap;->makeCorpusKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v6, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method private doMaintenanceInternal()V
    .locals 11

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    const-string v0, "Performing maintenance"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCreationTimestamp()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCompactStatus()Lcom/google/android/gms/icing/Proto$CompactStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/32 v7, 0x240c8400

    add-long/2addr v3, v7

    cmp-long v0, v5, v3

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/StorageMonitor;->enoughDiskSpace()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/StorageMonitor;->freeDiskFrac()D

    move-result-wide v2

    move-wide v9, v2

    move v3, v1

    move-wide v1, v9

    :goto_1
    if-eqz v3, :cond_3

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/impl/StorageMonitor;->purgeTarget(D)D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->compactAndPurgeInternal(D)V

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->minFreeFraction()D

    move-result-wide v3

    sget-wide v5, Lcom/google/android/gms/icing/impl/IndexManager;->INDEX_MIN_FREE_FRAC_THRESHOLD:D

    cmpg-double v5, v3, v5

    if-gez v5, :cond_2

    :goto_3
    move-wide v9, v3

    move v3, v1

    move-wide v1, v9

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_3

    :cond_3
    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->compactInternal()V

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->flushInternal()V

    goto :goto_2
.end method

.method private enoughStorage()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/StorageMonitor;->enoughDiskSpace()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->minFreeFraction()D

    move-result-wide v0

    sget-wide v2, Lcom/google/android/gms/icing/impl/IndexManager;->INDEX_MIN_FREE_FRAC_THRESHOLD:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private flushInternal()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->flush()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->setFlushStatus(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->postFlush(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Error running post-flush cleanup"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Failed to commit flush status"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSectionNames(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsCount()I

    move-result v2

    new-array v3, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "section_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private indexContentProvider(Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;Lcom/google/android/gms/icing/impl/ContentCursors;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;
    .param p3    # Lcom/google/android/gms/icing/impl/ContentCursors;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$13;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/icing/impl/IndexManager$13;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;Lcom/google/android/gms/icing/impl/ContentCursors;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    return-void
.end method

.method private indexContentProviderWithLimit(Ljava/lang/String;Lcom/google/android/gms/icing/impl/ContentCursors;)I
    .locals 11

    const-wide/16 v2, 0x0

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->canIndex(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getStatus(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v4

    cmp-long v9, v4, v2

    if-gez v9, :cond_4

    :goto_1
    new-instance v5, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->toBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;-><init>(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;)V

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/IndexManager;->indexCursors(Lcom/google/android/gms/icing/Proto$CorpusConfig;JLcom/google/android/gms/icing/impl/ContentCursors;Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;)Z

    move-result v0

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->flushCounters()V

    if-nez v0, :cond_1

    iget-object v1, v5, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->getLastSeqno()J

    move-result-wide v9

    cmp-long v1, v9, v2

    if-lez v1, :cond_3

    :cond_1
    const-string v1, "Indexed %s from %d to %d need flush %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v7

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v8

    iget-object v2, v5, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->getLastSeqno()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v6

    const/4 v2, 0x3

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    iget-object v2, v5, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->setStatus(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->flushInternal()V

    :cond_2
    move v0, v8

    goto :goto_0

    :cond_3
    const-string v0, "Query from %s found nothing"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move v0, v7

    goto :goto_0

    :cond_4
    move-wide v2, v4

    goto :goto_1
.end method

.method private indexContentProviders(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/gms/icing/impl/ContentCursors;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->getSectionNames(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/icing/impl/ContentCursors;-><init>(Landroid/content/ContentResolver;[Ljava/lang/String;)V

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProvider(Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;Lcom/google/android/gms/icing/impl/ContentCursors;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private indexCursors(Lcom/google/android/gms/icing/Proto$CorpusConfig;JLcom/google/android/gms/icing/impl/ContentCursors;Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;)Z
    .locals 20

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    move-wide/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/ContentCursors;->refillIfNecessary(Landroid/net/Uri;J)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getSectionMap(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/Map;

    move-result-object v9

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    const/16 v16, 0x0

    :goto_0
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/ContentCursors;->anyHasData()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/ContentCursors;->docs()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getSeqno()J

    move-result-wide v4

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/ContentCursors;->tags()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getSeqno()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    cmp-long v8, v6, p2

    if-gtz v8, :cond_0

    const-string v8, "Out of order seqno %d last %d"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-static {v8, v10, v11}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_0
    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/ContentCursors;->docs()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v8

    move-object/from16 v4, p0

    move-object/from16 v10, p5

    invoke-direct/range {v4 .. v10}, Lcom/google/android/gms/icing/impl/IndexManager;->indexDocumentFromCursor(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;JILjava/util/Map;Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, p5

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-wide/from16 v0, p2

    invoke-virtual {v5, v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setLastSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    return v4

    :cond_1
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/ContentCursors;->docs()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->advance()Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/gms/icing/impl/ContentCursors;->refillIfNecessary(Landroid/net/Uri;J)V

    :cond_2
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v18

    const-wide/16 v10, 0x1388

    cmp-long v4, v4, v10

    if-ltz v4, :cond_4

    move/from16 v4, v16

    move-wide/from16 p2, v6

    goto :goto_1

    :cond_3
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/ContentCursors;->tags()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v14

    move-object/from16 v10, p0

    move-wide v12, v6

    move-object/from16 v15, p5

    invoke-direct/range {v10 .. v15}, Lcom/google/android/gms/icing/impl/IndexManager;->indexTagFromCursor(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;JILcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;)V

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/ContentCursors;->tags()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->advance()Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/gms/icing/impl/ContentCursors;->refillIfNecessary(Landroid/net/Uri;J)V

    goto :goto_2

    :cond_4
    move-wide/from16 p2, v6

    goto/16 :goto_0

    :cond_5
    move/from16 v4, v16

    goto :goto_1
.end method

.method private indexDocumentFromCursor(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;JILjava/util/Map;Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;",
            "JI",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;",
            ">;",
            "Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;",
            ")Z"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    const-string v0, "action"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getColumnString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {p1, v1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getColumnString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "Uri"

    const/16 v4, 0x100

    invoke-static {v3, v1, v4}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThan(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v0, "bad uri"

    invoke-virtual {p6, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const-string v3, "add"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document;->newBuilder()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    const-string v1, "doc_score"

    invoke-virtual {p1, v1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getColumnInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setScore(I)Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v3

    invoke-interface {p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "section_"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getColumnString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v0, "missing section content"

    invoke-virtual {p6, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v5, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mSectionConfig:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getTokenizer()Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->TOKENIZER_RFC822_FORMATTED:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/TokenizeRfc822;->Tokenize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Section;->newBuilder()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v5

    iget v6, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v5

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mSectionConfig:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setConfig(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setContent(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->addSections(Lcom/google/android/gms/icing/Proto$Document$Section$Builder;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$Document$Builder;->build()Lcom/google/android/gms/icing/Proto$Document;

    move-result-object v1

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->indexDocument(JLcom/google/android/gms/icing/Proto$Document;)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/icing/impl/IndexManager$15;->$SwitchMap$com$google$android$gms$icing$Proto$Enums$NativeIndexResult:[I

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->resultToUserString(Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    :goto_2
    move v0, v2

    goto/16 :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_4
    const-string v3, "del"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0, p2, p3, p4, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->deleteDocument(JILjava/lang/String;)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->resultToUserString(Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    const-string v0, "bad action"

    invoke-virtual {p6, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private indexTagFromCursor(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;JILcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;)V
    .locals 7

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    const-string v0, "action"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getColumnString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {p1, v1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getColumnString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "tag"

    invoke-virtual {p1, v1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getColumnString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :try_start_0
    const-string v1, "Uri"

    const/16 v2, 0x100

    invoke-static {v1, v4, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThan(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    const-string v1, "Tag"

    const/16 v2, 0x3e8

    invoke-static {v1, v5, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThan(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "add"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v6, 0x1

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    move-wide v1, p2

    move v3, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->tagDocument(JILjava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->resultToUserString(Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "bad tag args"

    invoke-virtual {p5, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v1, "del"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v6, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "bad action"

    invoke-virtual {p5, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private initIndexInternal(Z)V
    .locals 14

    const/16 v8, 0xf

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->destroyInternal()V

    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/StorageMonitor;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mFileRoot:Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mOptions:Lcom/google/android/gms/icing/impl/IndexManager$Options;

    iget-wide v4, v4, Lcom/google/android/gms/icing/impl/IndexManager$Options;->mMaxDiskAvailable:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/StorageMonitor;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppDataSearch-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-version"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mPrefs:Landroid/content/SharedPreferences;

    new-instance v1, Lcom/google/android/gms/icing/impl/CorpusMap;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-config"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/icing/impl/CorpusMap;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    new-instance v1, Lcom/google/android/gms/icing/impl/AppAuthenticator;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-appauth"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/icing/impl/AppAuthenticator;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    if-nez p1, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "version"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v8, :cond_13

    const-string v0, "Version mismatch, clearing"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    move v2, v6

    :goto_0
    if-eqz v2, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/StorageMonitor;->clearStorage()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "Unable to clear storage, can\'t init index"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->destroyInternal()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v0, :cond_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "Unable to create storage"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->clear()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->clear()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "version"

    invoke-interface {v1, v3, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/StorageMonitor;->getStorageDirectory()Ljava/io/File;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v5}, Lcom/google/android/gms/icing/impl/StopWords;->setup(Landroid/content/Context;Ljava/io/File;)V

    move v4, v7

    move v3, v7

    move v1, v0

    :goto_3
    :try_start_1
    new-instance v0, Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-direct {v0, v5}, Lcom/google/android/gms/icing/impl/NativeIndex;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->clear()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/LinkageError; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    if-nez v0, :cond_b

    const-string v0, "Internal init failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->destroyInternal()V

    goto :goto_1

    :cond_4
    move v1, v0

    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->getCompactStatus()Lcom/google/android/gms/icing/Proto$CompactStatus;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCompactStatus()Lcom/google/android/gms/icing/Proto$CompactStatus;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v9

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-gez v9, :cond_8

    new-instance v0, Ljava/io/IOException;

    const-string v8, "Index compact timestamp in the past"

    invoke-direct {v0, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/LinkageError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_1
    move-exception v0

    move-object v13, v0

    move v0, v1

    move-object v1, v13

    const-string v3, "Error initializing, resetting corpora"

    new-array v8, v7, [Ljava/lang/Object;

    invoke-static {v1, v3, v8}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    if-lez v4, :cond_6

    move v0, v7

    :cond_6
    move v3, v6

    :goto_5
    if-eqz v3, :cond_11

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->resetCorporaData()V

    move v1, v6

    :goto_6
    add-int/lit8 v2, v4, 0x1

    if-eqz v3, :cond_7

    if-le v2, v6, :cond_10

    :cond_7
    move v2, v1

    goto :goto_4

    :cond_8
    :try_start_3
    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v9

    invoke-virtual {v8}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v11

    cmp-long v0, v9, v11

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->initFlushed()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v8, v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->setFlushStatus(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z

    :goto_7
    move v0, v1

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/impl/CorpusMap;->getFlushStatus()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/android/gms/icing/impl/NativeIndex;->init(Lcom/google/android/gms/icing/Proto$FlushStatus;)Lcom/google/android/gms/icing/Proto$InitStatus;

    move-result-object v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/io/IOException;

    const-string v8, "Index init failed"

    invoke-direct {v0, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/LinkageError; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    const-string v1, "Error linking native code, bailing from initialization"

    new-array v8, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v8}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v7

    goto :goto_5

    :cond_a
    :try_start_4
    iget-object v8, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v8, v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->updateCurSeqnosForInit(Lcom/google/android/gms/icing/Proto$InitStatus;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/LinkageError; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_7

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getAllPackageNames()[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    :goto_8
    if-ge v7, v1, :cond_d

    aget-object v3, v0, v7

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->authenticate(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c

    invoke-direct {p0, v3}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterAppInternal(Ljava/lang/String;)Z

    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusConfigsOfState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    const-string v4, "Found corpus [%s] in limbo"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterCorpusInternal(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_9

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-static {}, Lcom/google/android/gms/icing/LogUtil;->getLogPriority()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->setLogPriority(I)V

    const-string v0, "Internal init done"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->enoughStorage()Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "Not enough disk space. Will not index."

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :cond_f
    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$12;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/IndexManager$12;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_10
    move v4, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_3

    :cond_11
    move v1, v2

    goto/16 :goto_6

    :cond_12
    move v0, v6

    goto/16 :goto_2

    :cond_13
    move v2, p1

    goto/16 :goto_0
.end method

.method private onSleep()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->onSleep()V

    :cond_0
    return-void
.end method

.method private postCompactInternal(Lcom/google/android/gms/icing/Proto$FlushStatus;)V
    .locals 6

    const-wide/high16 v4, 0x4059000000000000L

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    if-nez p1, :cond_0

    const-string v0, "Compaction failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->getCompactStatus()Lcom/google/android/gms/icing/Proto$CompactStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->setCompactStatus(Lcom/google/android/gms/icing/Proto$CompactStatus;Lcom/google/android/gms/icing/Proto$FlushStatus;)V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->purgeInactiveCorpora()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getAllPackageNames()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->unregisterInactivePackages([Ljava/lang/String;)V

    :cond_1
    const-string v0, "Done compaction min disk %.3f%% min index %.3f%%"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/StorageMonitor;->freeDiskFrac()D

    move-result-wide v1

    mul-double/2addr v1, v4

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->minFreeFraction()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private registerCorpusInternal(ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInWorkerThread()V

    if-eqz p1, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterCorpusInternal(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0, p2, p4}, Lcom/google/android/gms/icing/impl/CorpusMap;->setConfig(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z

    move-result v0

    return v0
.end method

.method private scheduleIndexing(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    sget-object v2, Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_WAITING:Ljava/lang/Integer;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$14;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/IndexManager$14;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;J)V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private unregisterAppInternal(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v4, p1}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusKeysForPackageName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterCorpusInternal(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    :goto_1
    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    invoke-virtual {v4, p1}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->unregister(Ljava/lang/String;)V

    return v3
.end method

.method private unregisterCorpusInternal(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    const-string v1, "Removing corpus key %s for package %s"

    invoke-static {v1, p1, p2}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/gms/icing/impl/CorpusMap;->startDeactivateCorpus(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->cancelIndexing(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v2, "Could not unregister corpus because indexing not canceled"

    invoke-static {v2}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ltz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->deleteCorpus(I)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->setFlushStatus(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/icing/impl/CorpusMap;->finishDeactivateCorpus(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    const-string v1, "Failed to completely deactivate corpus key %s"

    invoke-static {v1, p1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    const-string v1, "Failed to delete corpus key %s"

    invoke-static {v1, p1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    :cond_3
    const-string v1, "Corpus key %s not found for deactivation (already removed?)"

    invoke-static {v1, p1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method


# virtual methods
.method public checkInitialized()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->isInitialized()Z

    move-result v0

    const-string v1, "Not initialized"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    return-void
.end method

.method public clear()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->destroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/IndexManager;->init(Z)V

    return-void
.end method

.method public compact()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$8;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/IndexManager$8;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    return-void
.end method

.method public compactAndPurge(D)V
    .locals 2
    .param p1    # D

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$9;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/icing/impl/IndexManager$9;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;D)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    return-void
.end method

.method public destroy()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->clear()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v2, Lcom/google/android/gms/icing/impl/IndexManager$3;

    invoke-direct {v2, p0}, Lcom/google/android/gms/icing/impl/IndexManager$3;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "Clear scheduler interrupted"

    invoke-static {v1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public diagnostic(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->hasMyUid(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v0, "Unauthorized"

    :goto_0
    const-string v2, "error_message"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    if-nez v2, :cond_1

    const-string v0, "No operation named"

    goto :goto_0

    :cond_1
    const-string v3, "flush"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->flush()V

    goto :goto_0

    :cond_2
    const-string v3, "clear"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->clear()V

    goto :goto_0

    :cond_3
    const-string v3, "compact"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->compact()V

    goto :goto_0

    :cond_4
    const-string v3, "compactAndPurge"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v2, "target"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "target"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/icing/impl/IndexManager;->compactAndPurge(D)V

    goto :goto_0

    :cond_5
    const-string v0, "No target free for compactAndPurge specified"

    goto :goto_0

    :cond_6
    const-string v3, "getDebugString"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v2, "debug"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->getDebugString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown operation \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public doMaintenance()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$10;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/IndexManager$10;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 11

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p3, :cond_2

    array-length v3, p3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, p3, v2

    const-string v5, "native"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v1, 0x1

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v5, "verbose"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const-string v2, "Icing on the Cake"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    const-string v3, "Apk version code: %d\n"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p2, v3, v4}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v3, "Apk version name: %s\n"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v2, v4, v5

    invoke-virtual {p2, v3, v4}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-string v2, "Version: %d\n"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "version"

    const/4 v7, -0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p2, v2, v3}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->enoughStorage()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "OUT OF STORAGE"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCompactStatus()Lcom/google/android/gms/icing/Proto$CompactStatus;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCreationTimestamp()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/CorpusMap;->getFlushStatus()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getTimestampSecs()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    new-instance v6, Ljava/util/Date;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    const-string v7, "Created \"%s\"\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-virtual {p2, v7, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v3, "Flushed \"%s\" num docs %d\n"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v5}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    const/4 v5, 0x1

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getNumDocuments()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v5

    invoke-virtual {p2, v3, v7}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v3, "Compacted \"%s\" num docs %d\n"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v6}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getNumDocuments()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {p2, v3, v4}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v2, "Min free disk %.3f%% index free frac %.3f%%\n"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/StorageMonitor;->freeDiskFrac()D

    move-result-wide v5

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->minFreeFraction()D

    move-result-wide v5

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p2, v2, v3}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v2, "\nRegistered Apps:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    invoke-virtual {v2, p2}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->dump(Ljava/io/PrintWriter;)V

    const-string v2, "\nCorpora:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2, p2}, Lcom/google/android/gms/icing/impl/CorpusMap;->dump(Ljava/io/PrintWriter;)V

    const-string v2, "\nCorpus Usage Stats:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->getUsageStats()Lcom/google/android/gms/icing/Proto$UsageStats;

    move-result-object v3

    if-eqz v3, :cond_4

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$UsageStats;->getCorpusCount()I

    move-result v4

    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, Lcom/google/android/gms/icing/Proto$UsageStats;->getCorpus(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    move-result-object v4

    const-string v5, "id: %d\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p2, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v5, "docs: %d\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocuments()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p2, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v5, "size: %s\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocumentsSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/android/gms/icing/impl/Utils;->PrettyPrintBytes(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p2, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v5, "deleted docs: %d\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocuments()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p2, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v5, "deleted size: %d\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocumentsSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-virtual {p2, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    const-string v2, "\nError getting usage stats"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_5
    if-eqz v1, :cond_6

    const-string v1, "\nNative Index:"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->getDebugInfo(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_6
    return-void

    :catch_0
    move-exception v2

    goto/16 :goto_2
.end method

.method public flush()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$7;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/IndexManager$7;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCorpusNames(Ljava/lang/String;)[Ljava/lang/String;
    .locals 14
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateGetCorpusNames(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v0, "Bad get corpus status args: %s"

    invoke-static {v0, v7}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return-object v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/CorpusMap;->authenticateAgainstCorpora(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0, v10}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0, v10}, Lcom/google/android/gms/icing/impl/CorpusMap;->getStatus(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v13

    if-eqz v6, :cond_1

    if-eqz v13, :cond_1

    invoke-virtual {v13}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v12, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v4, v0

    goto :goto_0
.end method

.method public getCorpusStatus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 19
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateGetCorpusStatus(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    const-string v2, "Bad get corpus status args: %s"

    invoke-static {v2, v10}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v15, 0x0

    :cond_0
    :goto_0
    return-object v15

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v6, v5

    const/4 v7, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/icing/impl/CorpusMap;->authenticateAgainstCorpora(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2, v13}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2, v13}, Lcom/google/android/gms/icing/impl/CorpusMap;->getStatus(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->getFlushStatus()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v11

    if-eqz v8, :cond_2

    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    if-nez v15, :cond_0

    invoke-static {}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createCorpusStatus()Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v15

    goto :goto_0

    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v5

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastFlushedSeqno()J

    move-result-wide v17

    if-eqz v11, :cond_5

    invoke-virtual {v11}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getNumDocuments()I

    move-result v2

    :goto_1
    move-wide/from16 v0, v17

    invoke-static {v5, v6, v0, v1, v2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createCorpusStatus(JJI)Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v15

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getCounterList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->setCounter(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getDebugString()Ljava/lang/String;
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    const/4 v1, 0x0

    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "native"

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/IndexManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDocuments([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 27
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static/range {p1 .. p4}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateGetDocuments([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_0

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createDocumentResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v20

    :goto_0
    return-object v20

    :cond_0
    new-instance v26, Landroid/util/TimingLogger;

    const-string v3, "Icing"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDocuments "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    array-length v6, v0

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-direct {v0, v3, v4}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    const-string v3, "wait index init"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v5

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedTags()Ljava/util/List;

    move-result-object v25

    :try_start_0
    invoke-static/range {v25 .. v25}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateTagsList(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p3, v7, v6

    const/4 v8, 0x1

    move-object/from16 v6, p2

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/icing/impl/CorpusMap;->authenticateAgainstCorpora(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v12

    const-string v3, "authentication"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-interface {v12}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not authorized to read corpus \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createDocumentResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v20

    goto/16 :goto_0

    :catch_0
    move-exception v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createDocumentResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v20

    goto/16 :goto_0

    :cond_1
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v3, v11}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v9

    if-nez v9, :cond_2

    const-string v3, "Corpus does not exist"

    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createDocumentResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v20

    goto/16 :goto_0

    :cond_2
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v10

    invoke-virtual {v9}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v3

    invoke-virtual {v10, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    invoke-static {v9}, Lcom/google/android/gms/icing/impl/CorpusMap;->getSectionMap(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/Map;

    move-result-object v22

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedSections()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/gms/appdatasearch/Section;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v4

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/Section;->name:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;

    iget v3, v3, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    invoke-virtual {v4, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSectionId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addSectionSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    goto :goto_1

    :cond_3
    if-eqz v25, :cond_4

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addAllTag(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    :cond_4
    invoke-virtual {v10}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v13

    const-string v3, "build corpus spec"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v13}, Lcom/google/android/gms/icing/impl/NativeIndex;->getDocuments([Ljava/lang/String;Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;

    move-result-object v19

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    sub-long v14, v3, v23

    const-string v3, "execute query"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    const-string v3, "Retrieved: %d Docs: %d Elapsed: %d ms"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, v19

    iget v7, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumScored:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    move-object/from16 v0, v19

    iget v7, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x2

    const-wide/32 v7, 0xf4240

    div-long v7, v14, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, p4

    invoke-static {v0, v13, v1, v9, v2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createDocumentResults([Ljava/lang/String;Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v20

    const-string v3, "build DocumentResults"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Landroid/util/TimingLogger;->dumpToLog()V

    goto/16 :goto_0
.end method

.method public getNotAuthorizedMessage([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    const-string v0, "Not authorized to read requested corpora"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Found no matching corpora for package"

    goto :goto_0
.end method

.method public getServiceBroker()Landroid/os/IBinder;
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;Lcom/google/android/gms/icing/impl/IndexManager$1;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/IndexManager$ServiceBroker;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public handlePackageOrDataRemovedIntent(Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.intent.extra.REPLACING"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v2, Lcom/google/android/gms/icing/impl/IndexManager$5;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/gms/icing/impl/IndexManager$5;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;Landroid/content/Intent;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public init(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$2;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    return-void
.end method

.method public isInitialized()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->isInThread()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public query(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 36
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static/range {p1 .. p6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateQuery(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_0

    invoke-static/range {v20 .. v20}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v27

    :goto_0
    return-object v27

    :cond_0
    new-instance v34, Landroid/util/TimingLogger;

    const-string v2, "Icing"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "query ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-direct {v0, v2, v3}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    const-string v2, "wait index init"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v4

    const-string v2, "Querying: [%s] start %d num %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v3, v5

    const/4 v5, 0x1

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v2, "Index docs: %d pls: %d"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->numDocuments()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->numPostingLists()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    if-nez p1, :cond_1

    const-string v2, "Null query"

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v27

    goto :goto_0

    :cond_1
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedTags()Ljava/util/List;

    move-result-object v33

    :try_start_0
    invoke-static/range {v33 .. v33}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateTagsList(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/icing/impl/CorpusMap;->authenticateAgainstCorpora(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v16

    const-string v2, "authentication"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->getNotAuthorizedMessage([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v27

    goto/16 :goto_0

    :catch_0
    move-exception v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v27

    goto/16 :goto_0

    :cond_2
    const/16 v35, 0x0

    if-nez p3, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->canQueryUniversal(Landroid/content/Context;I)Z

    move-result v35

    :cond_3
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setNoCorpusFilter(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantUris()Z

    move-result v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setWantUris(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    const/4 v11, 0x1

    const/16 v22, 0x0

    new-instance v14, Landroid/util/SparseArray;

    invoke-direct {v14}, Landroid/util/SparseArray;-><init>()V

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :cond_4
    :goto_1
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2, v15}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v12

    if-eqz v12, :cond_4

    invoke-virtual {v12}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v2

    invoke-virtual {v14, v2, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v13

    invoke-virtual {v12}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v2

    invoke-virtual {v13, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    invoke-static {v12}, Lcom/google/android/gms/icing/impl/CorpusMap;->getSectionMap(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/Map;

    move-result-object v30

    if-eqz v11, :cond_5

    if-nez v22, :cond_7

    move-object/from16 v22, v30

    :cond_5
    :goto_2
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedSections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_6
    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/gms/appdatasearch/Section;

    move-object/from16 v0, v28

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/Section;->name:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown section "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v28

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/Section;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v27

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v11, 0x0

    goto :goto_2

    :cond_8
    move-object/from16 v0, v28

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/Section;->name:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v2

    move-object/from16 v0, v29

    iget v3, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSectionId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v2

    move-object/from16 v0, v28

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/Section;->snippeted:Z

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippet(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v2

    move-object/from16 v0, v28

    iget v3, v0, Lcom/google/android/gms/appdatasearch/Section;->snippetLength:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippetLength(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addSectionSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mSectionConfig:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getWeight()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_6

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v2

    move-object/from16 v0, v29

    iget v3, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->setSectionId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v2

    invoke-virtual {v12}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v2

    move-object/from16 v0, v29

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mSectionConfig:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getWeight()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->setWeight(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addSectionWeight(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto/16 :goto_3

    :cond_9
    if-eqz v33, :cond_a

    move-object/from16 v0, v33

    invoke-virtual {v13, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addAllTag(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    :cond_a
    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto/16 :goto_1

    :cond_b
    if-eqz v11, :cond_c

    if-eqz v22, :cond_c

    invoke-interface/range {v22 .. v22}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_4
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Map$Entry;

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    move-result-object v3

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    move-result-object v2

    move-object/from16 v0, v29

    iget v3, v0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addSectionMapping(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_4

    :cond_c
    invoke-virtual/range {v25 .. v25}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    move-result-object v7

    const-string v2, "build query spec"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v31

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    const v8, 0x186a0

    move-object/from16 v6, p1

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/icing/impl/NativeIndex;->executeQuery(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$QueryRequestSpec;III)Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;

    move-result-object v26

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long v17, v2, v31

    const-string v2, "execute query"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    const-string v2, "Retrieved: %d Docs: %d Elapsed: %d ms"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v26

    iget v6, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumScored:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, v26

    iget v6, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-wide/32 v8, 0xf4240

    div-long v8, v17, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, v26

    invoke-static {v7, v0, v14}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResults(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v27

    const-string v2, "build SearchResults"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-virtual/range {v34 .. v34}, Landroid/util/TimingLogger;->dumpToLog()V

    goto/16 :goto_0
.end method

.method public queryUniversal(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/UniversalQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 10

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->canQueryUniversal(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-nez p1, :cond_1

    const-string v0, "Null query"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v6, Landroid/util/TimingLogger;

    const-string v0, "Icing"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    const-string v0, "wait index init"

    invoke-virtual {v6, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    const-string v0, "Querying global: [%s] start %d num %d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v0, "Index docs: %d pls: %d"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->numDocuments()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->numPostingLists()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    new-instance v7, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getActiveCorpusConfigs()Ljava/util/List;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;-><init>(Ljava/util/List;)V

    const-string v0, "build query spec"

    invoke-virtual {v6, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v7}, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->getRequestSpec()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    move-result-object v2

    const v3, 0x186a0

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->executeQuery(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$QueryRequestSpec;III)Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    sub-long/2addr v1, v8

    const-string v3, "execute query"

    invoke-virtual {v6, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    const-string v3, "Retrieved: %d Docs: %d Elapsed: %d ms"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v8, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumScored:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    iget v8, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x2

    const-wide/32 v8, 0xf4240

    div-long/2addr v1, v8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v7, v0}, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->createResults(Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    const-string v1, "build SearchResults"

    invoke-virtual {v6, v1}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/util/TimingLogger;->dumpToLog()V

    goto/16 :goto_0
.end method

.method public registerCorpusInfo(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)V
    .locals 18
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validatePreRegister(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v9}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->hasMyUid(Landroid/content/Context;I)Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v10

    if-nez v10, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CP "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " does not exist"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, v10, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v12, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v9, v12, :cond_2

    if-nez v8, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CP "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " authority "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " uid "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v10, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " does not match calling uid "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    if-eqz v8, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v11, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CP "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " package name "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " does not match argument "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    iget-object v1, v10, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v11, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->register(Ljava/lang/String;)I

    move-result v7

    const/4 v1, 0x2

    if-ne v7, v1, :cond_5

    const-string v1, "App %s registering with different sigs, clearing old corpora"

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/LogUtil;->i(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterAppInternal(Ljava/lang/String;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mAppAuthenticator:Lcom/google/android/gms/icing/impl/AppAuthenticator;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->register(Ljava/lang/String;)I

    move-result v7

    :cond_5
    if-eqz v7, :cond_6

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " cannot register: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->getMessage(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    const/4 v15, 0x0

    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/icing/impl/CorpusMap;->makeCorpusConfig(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->makeCorpusKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v14

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/icing/impl/CorpusMap;->getStatus(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v16

    if-eqz v16, :cond_7

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v1, v4, v14, v6}, Lcom/google/android/gms/icing/impl/CorpusMap;->updateIfRequired(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_1
    return-void

    :cond_7
    const-string v1, "Corpus registration info changed, replacing corpus"

    invoke-static {v1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    const/4 v15, 0x1

    :cond_8
    move v3, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mAsyncScheduler:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    move-object/from16 v17, v0

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexManager$4;

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/icing/impl/IndexManager$4;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->schedule(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v12, v5}, Lcom/google/android/gms/icing/impl/CorpusMap;->verifyUniqueCorpusForUid(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Corpus "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " already exists in a different package from this uid"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public requestIndexing(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Lcom/google/android/gms/appdatasearch/RequestIndexingSpecification;

    invoke-static/range {p1 .. p4}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateRequestIndexing(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    const-string v0, "Bad request indexing args: %s"

    invoke-static {v0, v6}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v10, 0x0

    :goto_0
    return v10

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v4, v3

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/CorpusMap;->authenticateAgainstCorpora(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/icing/impl/CorpusMap;->getStatus(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-virtual {v11}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v11}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v3

    cmp-long v0, p3, v3

    if-lez v0, :cond_1

    invoke-direct {p0, v8}, Lcom/google/android/gms/icing/impl/IndexManager;->scheduleIndexing(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v10, :cond_3

    :cond_2
    const/4 v10, 0x1

    :goto_2
    goto :goto_1

    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    :cond_4
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public suggest(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/gms/appdatasearch/SuggestSpecification;)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    .locals 20
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # I
    .param p5    # Lcom/google/android/gms/appdatasearch/SuggestSpecification;

    invoke-static/range {p1 .. p4}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateSuggest(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-static {v11}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSuggestionResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SuggestionResults;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/google/android/gms/icing/impl/AppAuthenticator;->canQueryUniversal(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez p3, :cond_2

    const/4 v8, 0x0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;

    move-object/from16 v0, p1

    move/from16 v1, p4

    invoke-virtual {v2, v0, v8, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->querySuggestions(Ljava/lang/String;[II)[Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    array-length v2, v0

    div-int/lit8 v2, v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v19

    array-length v2, v0

    div-int/lit8 v2, v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v12, 0x0

    const/16 v16, 0x0

    :goto_1
    move-object/from16 v0, v19

    array-length v2, v0

    if-ge v12, v2, :cond_5

    aget-object v2, v19, v12

    aput-object v2, v17, v16

    add-int/lit8 v2, v12, 0x1

    aget-object v2, v19, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_2
    aput-object v2, v18, v16

    add-int/lit8 v12, v12, 0x2

    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/icing/impl/CorpusMap;->authenticateAgainstCorpora(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->getNotAuthorizedMessage([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSuggestionResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SuggestionResults;

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v2

    new-array v8, v2, [I

    const/4 v14, 0x0

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    add-int/lit8 v15, v14, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v2, v9}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v2

    aput v2, v8, v14

    move v14, v15

    goto :goto_3

    :cond_4
    add-int/lit8 v2, v12, 0x1

    aget-object v2, v19, v2

    goto :goto_2

    :cond_5
    invoke-static/range {v17 .. v18}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSuggestionResults([Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SuggestionResults;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public unregister(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 16
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateUnregister(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    new-instance v11, Ljava/lang/IllegalArgumentException;

    invoke-direct {v11, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/IndexManager;->checkInitialized()V

    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->getCallingUid()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mContext:Landroid/content/Context;

    invoke-static {v11, v2}, Lcom/google/android/gms/icing/impl/CorpusMap;->getPackageNamesForUid(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-interface {v9, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "Package name %s does not have caller\'s uid %d"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p1, v13, v14

    const/4 v14, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v11, v0, v1}, Lcom/google/android/gms/icing/impl/CorpusMap;->makeCorpusKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v11, v8}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterCorpusInternal(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_3
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const-string v12, "content_provider_uris"

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v11

    new-array v11, v11, [Ljava/lang/String;

    invoke-interface {v5, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    invoke-virtual {v10, v12, v11}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-object v10

    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->getCorpusKeysForPackageName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;

    invoke-virtual {v11, v8}, Lcom/google/android/gms/icing/impl/CorpusMap;->getConfig(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->unregisterCorpusInternal(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
