.class public Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;
.super Ljava/lang/Object;
.source "ContentCursors.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/ContentCursors;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContentCursor"
.end annotation


# instance fields
.field private final mColumnToIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCursor:Landroid/database/Cursor;

.field private mHasData:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mColumnToIndex:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/impl/ContentCursors$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/impl/ContentCursors$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->close()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->needsRefill()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getWritableColumnMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->set(Landroid/database/Cursor;)V

    return-void
.end method

.method private close()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    :cond_0
    return-void
.end method

.method private getWritableColumnMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mColumnToIndex:Ljava/util/Map;

    return-object v0
.end method

.method private needsRefill()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private set(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->advance()Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public advance()Z
    .locals 4

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->close()V

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    return v2

    :cond_1
    move v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Could not advance cursor"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public getColumnInt(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mColumnToIndex:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :cond_0
    return v1
.end method

.method public getColumnString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mColumnToIndex:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public getSeqno()J
    .locals 5

    const-wide v0, 0x7fffffffffffffffL

    iget-boolean v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mColumnToIndex:Ljava/util/Map;

    const-string v4, "seqno"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_0
    return-wide v0
.end method
