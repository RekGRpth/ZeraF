.class Lcom/google/android/gms/icing/impl/IndexManager$10;
.super Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexManager;->doMaintenance()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$10;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public runAwake()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$10;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/IndexManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Maintenance skipped because init failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$10;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->doMaintenanceInternal()V
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1500(Lcom/google/android/gms/icing/impl/IndexManager;)V

    goto :goto_0
.end method
