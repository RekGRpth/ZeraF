.class public final Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
.super Ljava/lang/Object;
.source "NativeIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/NativeIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QueryResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;
    }
.end annotation


# instance fields
.field public mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

.field public mCorpusIds:[I

.field public final mNumResults:I

.field public final mNumScored:I

.field public mUriBuffer:[B

.field public mUriLengths:[I


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;J)V
    .locals 9
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseNumResults(J)I
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$000(J)I

    move-result v7

    iput v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseNumScored(J)I
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$100(J)I

    move-result v7

    iput v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumScored:I

    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseCorpusIds(J)Ljava/nio/ByteBuffer;
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$200(J)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->toIntArray(Ljava/nio/ByteBuffer;)[I

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpusIds:[I

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpusIds:[I

    array-length v7, v7

    const-string v8, "mCorpusIds"

    invoke-direct {p0, v7, v8}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->checkArrayLen(ILjava/lang/String;)V

    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseUriLengths(J)Ljava/nio/ByteBuffer;
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$300(J)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->toIntArray(Ljava/nio/ByteBuffer;)[I

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriLengths:[I

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getWantUris()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriLengths:[I

    array-length v7, v7

    const-string v8, "mUriLengths"

    invoke-direct {p0, v7, v8}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->checkArrayLen(ILjava/lang/String;)V

    :cond_0
    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseUriBuffer(J)Ljava/nio/ByteBuffer;
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$400(J)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->toByteArray(Ljava/nio/ByteBuffer;)[B

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriBuffer:[B

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecCount()I

    move-result v7

    new-array v7, v7, [Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    iput-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    const/4 v2, 0x0

    :goto_0
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    array-length v7, v7

    if-ge v2, v7, :cond_4

    new-instance v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;-><init>()V

    invoke-virtual {p1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpec(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getUniversalSearch()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getUniversalSectionMappingsCount()I

    move-result v5

    :goto_1
    new-array v7, v5, [Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    iput-object v7, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mSections:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_2

    new-instance v4, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    invoke-direct {v4}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;-><init>()V

    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseSectionContentLengths(JII)Ljava/nio/ByteBuffer;
    invoke-static {p2, p3, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$500(JII)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->toIntArray(Ljava/nio/ByteBuffer;)[I

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentLengths:[I

    iget-object v7, v4, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentLengths:[I

    array-length v7, v7

    const-string v8, "mContentLengths"

    invoke-direct {p0, v7, v8}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->checkArrayLen(ILjava/lang/String;)V

    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseSectionContentBuffer(JII)Ljava/nio/ByteBuffer;
    invoke-static {p2, p3, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$600(JII)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->toByteArray(Ljava/nio/ByteBuffer;)[B

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentBuffer:[B

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mSections:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    aput-object v4, v7, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSectionSpecCount()I

    move-result v5

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTagCount()I

    move-result v7

    new-array v7, v7, [Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;

    iput-object v7, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mTags:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;

    const/4 v3, 0x0

    :goto_3
    iget-object v7, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mTags:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;

    array-length v7, v7

    if-ge v3, v7, :cond_3

    new-instance v6, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;

    invoke-direct {v6}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;-><init>()V

    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDocHasTag(JII)Ljava/nio/ByteBuffer;
    invoke-static {p2, p3, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$700(JII)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->toBooleanArray(Ljava/nio/ByteBuffer;)[Z

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;->mDocHasTag:[Z

    iget-object v7, v6, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;->mDocHasTag:[Z

    array-length v7, v7

    const-string v8, "mDocHasTag"

    invoke-direct {p0, v7, v8}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->checkArrayLen(ILjava/lang/String;)V

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mTags:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;

    aput-object v6, v7, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    aput-object v0, v7, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDestroy(J)V
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$800(J)V

    return-void

    :catchall_0
    move-exception v7

    # invokes: Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDestroy(J)V
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->access$800(J)V

    throw v7
.end method

.method private checkArrayLen(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " len "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static toBooleanArray(Ljava/nio/ByteBuffer;)[Z
    .locals 3
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    new-array v1, v2, [Z

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method private static toByteArray(Ljava/nio/ByteBuffer;)[B
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    new-array v0, v1, [B

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method private static toIntArray(Ljava/nio/ByteBuffer;)[I
    .locals 3
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v2

    new-array v1, v2, [I

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    return-object v1
.end method
