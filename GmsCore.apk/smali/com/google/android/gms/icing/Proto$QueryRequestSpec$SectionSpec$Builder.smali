.class public final Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;",
        "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$13100()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 1
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSectionId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSectionId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSectionId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippet()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippet()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippet(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetStripHtml()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippetStripHtml()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippetStripHtml(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetLength()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippetLength()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippetLength(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSectionId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippet(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippetStripHtml(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->setSnippetLength(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setSectionId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSectionId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$13302(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->sectionId_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$13402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;I)I

    return-object p0
.end method

.method public setSnippet(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippet:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$13502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippet_:Z
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$13602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z

    return-object p0
.end method

.method public setSnippetLength(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetLength:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$13902(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetLength_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$14002(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;I)I

    return-object p0
.end method

.method public setSnippetStripHtml(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetStripHtml:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$13702(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetStripHtml_:Z
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->access$13802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z

    return-object p0
.end method
