.class public final enum Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
.super Ljava/lang/Enum;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$CorpusConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

.field public static final enum CONTACTS:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

.field public static final enum EMAIL:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

.field public static final enum OTHER:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    const-string v1, "CONTACTS"

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->CONTACTS:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v2, v2, v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->EMAIL:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v3, v3, v5}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->OTHER:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    new-array v0, v5, [Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->CONTACTS:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->EMAIL:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->OTHER:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->$VALUES:[Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type$1;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->index:I

    iput p4, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->CONTACTS:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->EMAIL:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->OTHER:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
    .locals 1

    const-class v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->$VALUES:[Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    invoke-virtual {v0}, [Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->value:I

    return v0
.end method
