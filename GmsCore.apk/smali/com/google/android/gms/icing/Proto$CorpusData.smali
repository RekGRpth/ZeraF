.class public final Lcom/google/android/gms/icing/Proto$CorpusData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CorpusData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusData;


# instance fields
.field private config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;

.field private hasConfig:Z

.field private hasStatus:Z

.field private memoizedSerializedSize:I

.field private status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusData;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusData;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$7102(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig:Z

    return p1
.end method

.method static synthetic access$7200(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object v0
.end method

.method static synthetic access$7202(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object p1
.end method

.method static synthetic access$7302(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus:Z

    return p1
.end method

.method static synthetic access$7400(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object v0
.end method

.method static synthetic access$7402(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusData;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusData;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusData;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->access$6900()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object v0
.end method

.method public hasConfig()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->newBuilder(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    return-void
.end method
