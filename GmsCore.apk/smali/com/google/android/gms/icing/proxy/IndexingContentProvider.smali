.class public abstract Lcom/google/android/gms/icing/proxy/IndexingContentProvider;
.super Landroid/content/ContentProvider;
.source "IndexingContentProvider.java"


# static fields
.field public static final COMMON_COLUMN_NAMES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "seqno"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "action"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "doc_score"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/icing/proxy/IndexingContentProvider;->COMMON_COLUMN_NAMES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const-string v0, "Deletes are not supported."

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const-string v0, "Inserts are not supported"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const-string v0, "Updates are not supported."

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method
