.class public final Lcom/google/android/gms/icing/Proto$Document$Section;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$Document;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Section"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$Document$Section;


# instance fields
.field private config_:Lcom/google/android/gms/icing/Proto$SectionConfig;

.field private content_:Ljava/lang/String;

.field private hasConfig:Z

.field private hasContent:Z

.field private hasId:Z

.field private hasIndexedLength:Z

.field private hasSnippet:Z

.field private id_:I

.field private indexedLength_:I

.field private memoizedSerializedSize:I

.field private snippet_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Document$Section;->defaultInstance:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$Document$Section;->defaultInstance:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$Document$Section;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->id_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->indexedLength_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->content_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->snippet_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->id_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->indexedLength_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->content_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->snippet_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$11002(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasId:Z

    return p1
.end method

.method static synthetic access$11102(Lcom/google/android/gms/icing/Proto$Document$Section;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->id_:I

    return p1
.end method

.method static synthetic access$11202(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasIndexedLength:Z

    return p1
.end method

.method static synthetic access$11302(Lcom/google/android/gms/icing/Proto$Document$Section;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->indexedLength_:I

    return p1
.end method

.method static synthetic access$11402(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasContent:Z

    return p1
.end method

.method static synthetic access$11502(Lcom/google/android/gms/icing/Proto$Document$Section;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->content_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$11602(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasSnippet:Z

    return p1
.end method

.method static synthetic access$11702(Lcom/google/android/gms/icing/Proto$Document$Section;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->snippet_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$11802(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig:Z

    return p1
.end method

.method static synthetic access$11900(Lcom/google/android/gms/icing/Proto$Document$Section;)Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object v0
.end method

.method static synthetic access$11902(Lcom/google/android/gms/icing/Proto$Document$Section;Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document$Section;
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$Document$Section;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$Document$Section;->defaultInstance:Lcom/google/android/gms/icing/Proto$Document$Section;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->create()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->access$10800()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getConfig()Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->content_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->id_:I

    return v0
.end method

.method public getIndexedLength()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->indexedLength_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasIndexedLength()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getIndexedLength()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getContent()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasSnippet()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getSnippet()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getConfig()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->snippet_:Ljava/lang/String;

    return-object v0
.end method

.method public hasConfig()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig:Z

    return v0
.end method

.method public hasContent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasContent:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasId:Z

    return v0
.end method

.method public hasIndexedLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasIndexedLength:Z

    return v0
.end method

.method public hasSnippet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section;->hasSnippet:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Section;->newBuilder()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->newBuilderForType()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasIndexedLength()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getIndexedLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasSnippet()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getSnippet()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getConfig()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    return-void
.end method
