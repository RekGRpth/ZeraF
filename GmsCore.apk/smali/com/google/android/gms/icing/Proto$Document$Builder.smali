.class public final Lcom/google/android/gms/icing/Proto$Document$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$Document;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$Document;",
        "Lcom/google/android/gms/icing/Proto$Document$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$Document;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$12100()Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Builder;->create()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$Document$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$Document$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$Document;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$Document;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    return-object v0
.end method


# virtual methods
.method public addSections(Lcom/google/android/gms/icing/Proto$Document$Section$Builder;)Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document;->access$12302(Lcom/google/android/gms/icing/Proto$Document;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->build()Lcom/google/android/gms/icing/Proto$Document$Section;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSections(Lcom/google/android/gms/icing/Proto$Document$Section;)Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$Document$Section;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document;->access$12302(Lcom/google/android/gms/icing/Proto$Document;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/gms/icing/Proto$Document;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$Document;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->build()Lcom/google/android/gms/icing/Proto$Document;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$Document;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$Document;->access$12302(Lcom/google/android/gms/icing/Proto$Document;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Builder;->create()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$Document;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->clone()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->clone()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->clone()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Document;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$Document;)Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$Document;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$Document;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document;->hasCorpusId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document;->getCorpusId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$Document$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document;->hasUri()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document;->hasScore()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document;->getScore()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setScore(I)Lcom/google/android/gms/icing/Proto$Document$Builder;

    :cond_4
    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document;->access$12302(Lcom/google/android/gms/icing/Proto$Document;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$Document;->access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/icing/Proto$Document$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$Document$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Builder;->setScore(I)Lcom/google/android/gms/icing/Proto$Document$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Section;->newBuilder()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$Document$Section;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Builder;->addSections(Lcom/google/android/gms/icing/Proto$Document$Section;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$Document$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$Document$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setCorpusId(I)Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->hasCorpusId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document;->access$12402(Lcom/google/android/gms/icing/Proto$Document;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->corpusId_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document;->access$12502(Lcom/google/android/gms/icing/Proto$Document;I)I

    return-object p0
.end method

.method public setScore(I)Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->hasScore:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document;->access$12802(Lcom/google/android/gms/icing/Proto$Document;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->score_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document;->access$12902(Lcom/google/android/gms/icing/Proto$Document;I)I

    return-object p0
.end method

.method public setUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->hasUri:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document;->access$12602(Lcom/google/android/gms/icing/Proto$Document;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Builder;->result:Lcom/google/android/gms/icing/Proto$Document;

    # setter for: Lcom/google/android/gms/icing/Proto$Document;->uri_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document;->access$12702(Lcom/google/android/gms/icing/Proto$Document;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
