.class public final Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
        "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$2600()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->create()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->build()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->create()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->clone()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->clone()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->clone()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v0

    return-object v0
.end method

.method public hasValueTemplate()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 1
    .param p1    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasUniversalSectionId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getUniversalSectionId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->setUniversalSectionId(I)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->mergeValueTemplate(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->setUniversalSectionId(I)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->hasValueTemplate()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->setValueTemplate(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeValueTemplate(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    # getter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$3100(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    # getter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$3100(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->newBuilder(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$3102(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$3002(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    # setter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$3102(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    goto :goto_0
.end method

.method public setUniversalSectionId(I)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasUniversalSectionId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$2802(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    # setter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->universalSectionId_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$2902(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;I)I

    return-object p0
.end method

.method public setValueTemplate(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$3002(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->result:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    # setter for: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->access$3102(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    return-object p0
.end method
