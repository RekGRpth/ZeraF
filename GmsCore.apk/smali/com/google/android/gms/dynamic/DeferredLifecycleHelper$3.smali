.class Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$3;
.super Ljava/lang/Object;
.source "DeferredLifecycleHelper.java"

# interfaces
.implements Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$DeferredStateAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

.field final synthetic val$savedInstanceState:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$3;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    iput-object p2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$3;->val$savedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public run(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/dynamic/LifecycleDelegate;

    iget-object v0, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$3;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # getter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mDelegate:Lcom/google/android/gms/dynamic/LifecycleDelegate;
    invoke-static {v0}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$000(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$3;->val$savedInstanceState:Landroid/os/Bundle;

    invoke-interface {v0, v1}, Lcom/google/android/gms/dynamic/LifecycleDelegate;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method
