.class Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;
.super Ljava/lang/Object;
.source "DeferredLifecycleHelper.java"

# interfaces
.implements Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$DeferredStateAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

.field final synthetic val$container:Landroid/view/ViewGroup;

.field final synthetic val$holder:Landroid/widget/FrameLayout;

.field final synthetic val$inflater:Landroid/view/LayoutInflater;

.field final synthetic val$savedInstanceState:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;Landroid/widget/FrameLayout;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    iput-object p2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$holder:Landroid/widget/FrameLayout;

    iput-object p3, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$inflater:Landroid/view/LayoutInflater;

    iput-object p4, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$container:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$savedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public run(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/dynamic/LifecycleDelegate;

    iget-object v0, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$holder:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$holder:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # getter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mDelegate:Lcom/google/android/gms/dynamic/LifecycleDelegate;
    invoke-static {v1}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$000(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$inflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$container:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$4;->val$savedInstanceState:Landroid/os/Bundle;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/dynamic/LifecycleDelegate;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method
