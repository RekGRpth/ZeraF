.class public final Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;
.super Ljava/lang/Object;
.source "GoogleSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/settings/GoogleSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SettingLink"
.end annotation


# instance fields
.field public action:Landroid/content/Intent;

.field public displayName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;->displayName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;->action:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;->displayName:Ljava/lang/String;

    return-object v0
.end method
