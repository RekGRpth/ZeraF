.class public Lcom/google/android/gms/common/settings/GoogleSettingsActivity;
.super Landroid/app/ListActivity;
.source "GoogleSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;,
        Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;

.field private mPm:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    return-void
.end method

.method private addAdsSettings()V
    .locals 3

    const v0, 0x7f0b006d

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.settings.ADS_PRIVACY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addSetting(ILandroid/content/Intent;)Z

    return-void
.end method

.method private addConnectedAppsSettings()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.MANAGE_APPS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const v1, 0x7f0b006e

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addSetting(ILandroid/content/Intent;)Z

    return-void
.end method

.method private addLocationSettings()V
    .locals 3

    const v0, 0x7f0b006a

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addSetting(ILandroid/content/Intent;)Z

    return-void
.end method

.method private addMapsSettings()V
    .locals 3

    const v0, 0x7f0b0069

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addSetting(ILandroid/content/Intent;)Z

    return-void
.end method

.method private addPlusSettings()V
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.PRIVACY_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "account"

    new-instance v2, Landroid/accounts/Account;

    const-string v3, "fake"

    const-string v4, "com.google"

    invoke-direct {v2, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const v1, 0x7f0b006c

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addSetting(ILandroid/content/Intent;)Z

    return-void
.end method

.method private addSearchSettings()V
    .locals 3

    const v0, 0x7f0b006b

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.googlequicksearchbox.action.PRIVACY_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addSetting(ILandroid/content/Intent;)Z

    return-void
.end method

.method private addSetting(ILandroid/content/Intent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->mPm:Landroid/content/pm/PackageManager;

    const/high16 v3, 0x10000

    invoke-virtual {v2, p2, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->mAdapter:Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;

    new-instance v3, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;

    invoke-direct {v3, v0, p2}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;->add(Ljava/lang/Object;)V

    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04001a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->mPm:Landroid/content/pm/PackageManager;

    new-instance v0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->mAdapter:Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;

    invoke-direct {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addPlusSettings()V

    invoke-direct {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addConnectedAppsSettings()V

    invoke-direct {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addMapsSettings()V

    invoke-direct {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addLocationSettings()V

    invoke-direct {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addSearchSettings()V

    invoke-direct {p0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->addAdsSettings()V

    iget-object v0, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->mAdapter:Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    iget-object v1, p0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->mAdapter:Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;

    invoke-virtual {v1, p3}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$GoogleSettingsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;

    iget-object v1, v0, Lcom/google/android/gms/common/settings/GoogleSettingsActivity$SettingLink;->action:Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/settings/GoogleSettingsActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
