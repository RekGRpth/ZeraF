.class public Lcom/google/android/gms/common/server/ApiaryRequest;
.super Lcom/android/volley/toolbox/JsonRequest;
.source "ApiaryRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
        ">",
        "Lcom/android/volley/toolbox/JsonRequest",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final mEnableCache:Z

.field private final mHeaders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOAuthToken:Ljava/lang/String;

.field private final mResponseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mResponseClassParam:Ljava/lang/Object;


# direct methods
.method protected constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # Ljava/lang/Object;
    .param p7    # Lcom/android/volley/Response$ErrorListener;
    .param p8    # Ljava/lang/String;
    .param p9    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p3, :cond_1

    const/4 v4, 0x0

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/android/volley/toolbox/JsonRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    iput-object p4, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mResponseClass:Ljava/lang/Class;

    iput-object p5, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mResponseClassParam:Ljava/lang/Object;

    iput-object p8, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mOAuthToken:Ljava/lang/String;

    new-instance v1, Lcom/android/volley/DefaultRetryPolicy;

    const/16 v2, 0x2710

    const/4 v3, 0x1

    const/high16 v4, 0x3f800000

    invoke-direct {v1, v2, v3, v4}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/server/ApiaryRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)V

    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mEnableCache:Z

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mHeaders:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mOAuthToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mHeaders:Ljava/util/HashMap;

    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OAuth "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mOAuthToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mHeaders:Ljava/util/HashMap;

    const-string v2, "Accept-Encoding"

    const-string v3, "gzip"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_1
    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public constructor <init>(ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 11
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lorg/json/JSONObject;
    .param p5    # Ljava/lang/Object;
    .param p7    # Lcom/android/volley/Response$ErrorListener;
    .param p8    # Ljava/lang/String;
    .param p9    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p3, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/common/server/ApiaryRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-void

    :cond_0
    invoke-virtual {p3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p4    # Ljava/lang/Object;
    .param p6    # Lcom/android/volley/Response$ErrorListener;
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v1, -0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/common/server/ApiaryRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-void
.end method

.method private newResponseClassInstance()Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/NoSuchMethodException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mResponseClassParam:Ljava/lang/Object;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mResponseClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mResponseClass:Ljava/lang/Class;

    new-array v2, v5, [Ljava/lang/Class;

    iget-object v3, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mResponseClassParam:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mResponseClassParam:Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    goto :goto_0
.end method


# virtual methods
.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/server/ApiaryRequest;->mHeaders:Ljava/util/HashMap;

    return-object v0
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 16
    .param p1    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<TT;>;"
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v2, 0x0

    :try_start_0
    new-instance v9, Ljava/util/zip/GZIPInputStream;

    new-instance v14, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-direct {v14, v15}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v9, v14}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/server/response/FastParser$ParseException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/server/ApiaryRequest;->newResponseClassInstance()Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v11

    new-instance v12, Lcom/google/android/gms/common/server/response/FastParser;

    invoke-direct {v12}, Lcom/google/android/gms/common/server/response/FastParser;-><init>()V

    invoke-virtual {v12, v9, v11}, Lcom/google/android/gms/common/server/response/FastParser;->parse(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/gms/common/server/ApiaryRequest;->mEnableCache:Z

    if-eqz v14, :cond_1

    invoke-static/range {p1 .. p1}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;

    move-result-object v14

    :goto_0
    invoke-static {v11, v14}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Lcom/google/android/gms/common/server/response/FastParser$ParseException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    if-eqz v9, :cond_4

    :try_start_2
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v8, v9

    :goto_1
    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v10

    :cond_0
    return-object v10

    :cond_1
    const/4 v14, 0x0

    goto :goto_0

    :catch_0
    move-exception v7

    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, v7}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    move-object v8, v9

    goto :goto_1

    :catch_1
    move-exception v6

    :goto_2
    :try_start_3
    new-instance v3, Lcom/android/volley/ParseError;

    invoke-direct {v3, v6}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v8, :cond_3

    :try_start_4
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception v7

    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, v7}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_3
    move-exception v13

    :goto_3
    :try_start_5
    new-instance v3, Lcom/android/volley/ParseError;

    invoke-direct {v3, v13}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v8, :cond_3

    :try_start_6
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    move-object v2, v3

    goto :goto_1

    :catch_4
    move-exception v7

    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, v7}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_5
    move-exception v5

    :goto_4
    :try_start_7
    new-instance v14, Ljava/lang/RuntimeException;

    invoke-direct {v14, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v14
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception v14

    :goto_5
    if-eqz v8, :cond_2

    :try_start_8
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_a

    :cond_2
    :goto_6
    throw v14

    :catch_6
    move-exception v4

    :goto_7
    :try_start_9
    new-instance v14, Ljava/lang/RuntimeException;

    invoke-direct {v14, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v14

    :catch_7
    move-exception v1

    :goto_8
    new-instance v14, Ljava/lang/RuntimeException;

    invoke-direct {v14, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v14

    :catch_8
    move-exception v1

    :goto_9
    new-instance v14, Ljava/lang/RuntimeException;

    invoke-direct {v14, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v14

    :catch_9
    move-exception v1

    :goto_a
    new-instance v14, Ljava/lang/RuntimeException;

    invoke-direct {v14, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v14
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_a
    move-exception v7

    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, v7}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    goto :goto_6

    :catchall_1
    move-exception v14

    move-object v8, v9

    goto :goto_5

    :catch_b
    move-exception v1

    move-object v8, v9

    goto :goto_a

    :catch_c
    move-exception v1

    move-object v8, v9

    goto :goto_9

    :catch_d
    move-exception v1

    move-object v8, v9

    goto :goto_8

    :catch_e
    move-exception v4

    move-object v8, v9

    goto :goto_7

    :catch_f
    move-exception v5

    move-object v8, v9

    goto :goto_4

    :catch_10
    move-exception v13

    move-object v8, v9

    goto :goto_3

    :catch_11
    move-exception v6

    move-object v8, v9

    goto :goto_2

    :cond_3
    move-object v2, v3

    goto/16 :goto_1

    :cond_4
    move-object v8, v9

    goto/16 :goto_1
.end method
