.class Lcom/google/android/gms/common/config/GservicesValue$GservicesReaderDefaultValues;
.super Ljava/lang/Object;
.source "GservicesValue.java"

# interfaces
.implements Lcom/google/android/gms/common/config/GservicesValue$GservicesReader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/config/GservicesValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GservicesReaderDefaultValues"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/common/config/GservicesValue$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/config/GservicesValue$1;

    invoke-direct {p0}, Lcom/google/android/gms/common/config/GservicesValue$GservicesReaderDefaultValues;-><init>()V

    return-void
.end method


# virtual methods
.method public getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Boolean;

    return-object p2
.end method

.method public getInt(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Integer;

    return-object p2
.end method

.method public getLong(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    return-object p2
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-object p2
.end method
