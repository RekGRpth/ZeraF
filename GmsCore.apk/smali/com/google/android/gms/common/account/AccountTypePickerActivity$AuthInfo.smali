.class Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;
.super Ljava/lang/Object;
.source "AccountTypePickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/account/AccountTypePickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AuthInfo"
.end annotation


# instance fields
.field final desc:Landroid/accounts/AuthenticatorDescription;

.field final drawable:Landroid/graphics/drawable/Drawable;

.field final name:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AuthenticatorDescription;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/accounts/AuthenticatorDescription;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;->desc:Landroid/accounts/AuthenticatorDescription;

    iput-object p2, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;->drawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method
