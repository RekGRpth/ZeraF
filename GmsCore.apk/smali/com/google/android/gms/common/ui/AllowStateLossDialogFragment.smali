.class public Lcom/google/android/gms/common/ui/AllowStateLossDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "AllowStateLossDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/FragmentManager;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/common/ui/AllowStateLossDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;Z)V

    return-void
.end method

.method public show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/FragmentManager;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lvedroid/support/v4/app/FragmentTransaction;->add(Lvedroid/support/v4/app/Fragment;Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz p3, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
