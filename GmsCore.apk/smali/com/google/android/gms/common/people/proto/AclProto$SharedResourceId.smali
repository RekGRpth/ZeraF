.class public final Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "AclProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/people/proto/AclProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SharedResourceId"
.end annotation


# instance fields
.field private applicationId_:I

.field private cachedSize:I

.field private hasApplicationId:Z

.field private hasItemId:Z

.field private itemId_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->applicationId_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->itemId_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getApplicationId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->applicationId_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->cachedSize:I

    return v0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->itemId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasApplicationId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->getApplicationId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasItemId()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->getItemId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->cachedSize:I

    return v0
.end method

.method public hasApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasApplicationId:Z

    return v0
.end method

.method public hasItemId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasItemId:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->setApplicationId(I)Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->setItemId(Ljava/lang/String;)Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    move-result-object v0

    return-object v0
.end method

.method public setApplicationId(I)Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasApplicationId:Z

    iput p1, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->applicationId_:I

    return-object p0
.end method

.method public setItemId(Ljava/lang/String;)Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasItemId:Z

    iput-object p1, p0, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->itemId_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasApplicationId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->getApplicationId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->hasItemId()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    return-void
.end method
