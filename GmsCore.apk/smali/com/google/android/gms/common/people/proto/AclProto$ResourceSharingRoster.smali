.class public final Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "AclProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/people/proto/AclProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResourceSharingRoster"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasId:Z

.field private hasSharingRoster:Z

.field private id_:Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

.field private sharingRoster_:Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->id_:Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->sharingRoster_:Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->cachedSize:I

    return v0
.end method

.method public getId()Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->id_:Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->getId()Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasSharingRoster()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->getSharingRoster()Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->cachedSize:I

    return v0
.end method

.method public getSharingRoster()Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->sharingRoster_:Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    return-object v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasId:Z

    return v0
.end method

.method public hasSharingRoster()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasSharingRoster:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    invoke-direct {v1}, Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->setId(Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;)Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    invoke-direct {v1}, Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->setSharingRoster(Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;)Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;

    move-result-object v0

    return-object v0
.end method

.method public setId(Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;)Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasId:Z

    iput-object p1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->id_:Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    return-object p0
.end method

.method public setSharingRoster(Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;)Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasSharingRoster:Z

    iput-object p1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->sharingRoster_:Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->getId()Lcom/google/android/gms/common/people/proto/AclProto$SharedResourceId;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->hasSharingRoster()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;->getSharingRoster()Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
