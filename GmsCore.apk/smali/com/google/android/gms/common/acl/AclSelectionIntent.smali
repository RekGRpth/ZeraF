.class public Lcom/google/android/gms/common/acl/AclSelectionIntent;
.super Landroid/content/Intent;
.source "AclSelectionIntent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/acl/AclSelectionIntent$1;,
        Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Intent;Lcom/google/android/gms/common/acl/AclSelectionIntent$1;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/gms/common/acl/AclSelectionIntent$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;-><init>(Landroid/content/Intent;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/common/acl/AclSelectionIntent$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/acl/AclSelectionIntent$1;

    invoke-direct {p0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;-><init>()V

    return-void
.end method

.method public static getAudienceSelectionActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms.plus.audience.AudienceSelectionActivity"

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;-><init>(Landroid/content/ComponentName;Lcom/google/android/gms/common/acl/AclSelectionIntent$1;)V

    return-object v1
.end method

.method public static getCircleSelectionActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms.plus.audience.CircleSelectionActivity"

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;-><init>(Landroid/content/ComponentName;Lcom/google/android/gms/common/acl/AclSelectionIntent$1;)V

    return-object v1
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 1

    const-string v0, "ACCOUNT_NAME"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAllowEmptySelection()Z
    .locals 2

    const-string v0, "ALLOW_EMPTY"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getAudience()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    const-string v0, "AUDIENCE"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCancelText()Ljava/lang/String;
    .locals 1

    const-string v0, "CANCEL_TEXT"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    const-string v0, "DESCRIPTION"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEveryoneCheckboxText()Ljava/lang/String;
    .locals 1

    const-string v0, "EVERYONE_CHECKBOX_TEXT"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKnownAudienceMembers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    const-string v0, "KNOWN_AUDIENCE"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getLoadCircles()Z
    .locals 2

    const-string v0, "LOAD_CIRCLES"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getLoadGroups()Z
    .locals 2

    const-string v0, "LOAD_GROUPS"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getLoadPeople()Z
    .locals 2

    const-string v0, "LOAD_PEOPLE"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getOkText()Ljava/lang/String;
    .locals 1

    const-string v0, "OK_TEXT"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSectionTitles()Z
    .locals 2

    const-string v0, "SECTION_TITLES"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowCancel()Z
    .locals 2

    const-string v0, "CANCEL_VISIBLE"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowChips()Z
    .locals 2

    const-string v0, "CHIPS_VISIBLE"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const-string v0, "TITLE"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isEveryoneChecked()Z
    .locals 2

    const-string v0, "EVERYONE"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
