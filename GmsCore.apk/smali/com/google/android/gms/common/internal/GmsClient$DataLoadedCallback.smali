.class public abstract Lcom/google/android/gms/common/internal/GmsClient$DataLoadedCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "GmsClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/internal/GmsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "DataLoadedCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T",
        "Listener:Ljava/lang/Object;",
        ">",
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<TT;>.CallbackProxy<TT",
        "Listener;",
        ">;"
    }
.end annotation


# instance fields
.field protected final mDataHolder:Lcom/google/android/gms/common/data/DataHolder;

.field final synthetic this$0:Lcom/google/android/gms/common/internal/GmsClient;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 0
    .param p3    # Lcom/google/android/gms/common/data/DataHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "Listener;",
            "Lcom/google/android/gms/common/data/DataHolder;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/common/internal/GmsClient$DataLoadedCallback;->this$0:Lcom/google/android/gms/common/internal/GmsClient;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/common/internal/GmsClient$DataLoadedCallback;->mDataHolder:Lcom/google/android/gms/common/data/DataHolder;

    return-void
.end method


# virtual methods
.method public bridge synthetic deliverCallback()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;->deliverCallback()V

    return-void
.end method

.method public bridge synthetic removeListener()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;->removeListener()V

    return-void
.end method
