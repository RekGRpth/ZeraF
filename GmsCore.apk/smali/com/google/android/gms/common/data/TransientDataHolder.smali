.class public final Lcom/google/android/gms/common/data/TransientDataHolder;
.super Ljava/lang/Object;
.source "TransientDataHolder.java"


# instance fields
.field private mData:Lcom/google/android/gms/common/data/DataHolder$Builder;

.field private mNextToken:Ljava/lang/String;

.field private mPrevToken:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mPrevToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mNextToken:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/google/android/gms/common/data/DataHolder;->builder([Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mData:Lcom/google/android/gms/common/data/DataHolder$Builder;

    return-void
.end method


# virtual methods
.method public addRow(Landroid/content/ContentValues;)V
    .locals 1
    .param p1    # Landroid/content/ContentValues;

    iget-object v0, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mData:Lcom/google/android/gms/common/data/DataHolder$Builder;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/DataHolder$Builder;->withRow(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    return-void
.end method

.method public build(I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1
    .param p1    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/common/data/TransientDataHolder;->build(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public build(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const-string v0, "next_page_token"

    iget-object v1, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mNextToken:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prev_page_token"

    iget-object v1, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mPrevToken:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mData:Lcom/google/android/gms/common/data/DataHolder$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/common/data/DataHolder$Builder;->build(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mData:Lcom/google/android/gms/common/data/DataHolder$Builder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder$Builder;->getCount()I

    move-result v0

    return v0
.end method

.method public getNextToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mNextToken:Ljava/lang/String;

    return-object v0
.end method

.method public getPrevToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mPrevToken:Ljava/lang/String;

    return-object v0
.end method

.method public setNextToken(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mNextToken:Ljava/lang/String;

    return-void
.end method

.method public setPrevToken(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mPrevToken:Ljava/lang/String;

    return-void
.end method

.method public sortData(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/common/data/TransientDataHolder;->mData:Lcom/google/android/gms/common/data/DataHolder$Builder;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/DataHolder$Builder;->sort(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    return-void
.end method
