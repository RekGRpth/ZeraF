.class public final Lcom/google/android/gms/common/images/ImageManager;
.super Ljava/lang/Object;
.source "ImageManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/images/ImageManager$1;,
        Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;,
        Lcom/google/android/gms/common/images/ImageManager$ImageViewListenerHolder;,
        Lcom/google/android/gms/common/images/ImageManager$ImageLoadedListenerHolder;,
        Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;,
        Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;,
        Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;
    }
.end annotation


# static fields
.field public static final PRIORITY_HIGH:I = 0x3

.field public static final PRIORITY_LOW:I = 0x1

.field public static final PRIORITY_MEDIUM:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/google/android/gms/common/images/ImageManager;


# instance fields
.field private final mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/internal/support/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mListenerHolderMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;",
            "Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final mUriMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/common/images/ImageManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/images/ImageManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/common/internal/support/LruCache;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/support/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    invoke-static {}, Lcom/google/android/gms/common/util/PlatformVersion;->isAtLeastIceCreamSandwich()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;

    iget-object v2, p0, Lcom/google/android/gms/common/images/ImageManager;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;-><init>(Lcom/google/android/gms/common/internal/support/LruCache;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mListenerHolderMap:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mUriMap:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/images/ImageManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/common/images/ImageManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/images/ImageManager;

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/common/images/ImageManager;)Lcom/google/android/gms/common/internal/support/LruCache;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/images/ImageManager;

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gms/common/images/ImageManager;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/images/ImageManager;

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mUriMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gms/common/images/ImageManager;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/images/ImageManager;

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager;->mListenerHolderMap:Ljava/util/Map;

    return-object v0
.end method

.method public static create(Landroid/content/Context;)Lcom/google/android/gms/common/images/ImageManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/gms/common/images/ImageManager;->sInstance:Lcom/google/android/gms/common/images/ImageManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/images/ImageManager;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/images/ImageManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/common/images/ImageManager;->sInstance:Lcom/google/android/gms/common/images/ImageManager;

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/images/ImageManager;->sInstance:Lcom/google/android/gms/common/images/ImageManager;

    return-object v0
.end method

.method private loadImage(Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;Landroid/net/Uri;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;
    .param p2    # Landroid/net/Uri;

    if-eqz p2, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/common/images/ImageManager;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    invoke-virtual {v4, p2}, Lcom/google/android/gms/common/internal/support/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    invoke-virtual {p1, p2, v0}, Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;->handleCachedDrawable(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p2}, Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;->shouldLoadImage(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/common/images/ImageManager;->mUriMap:Ljava/util/Map;

    invoke-interface {v4, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;

    if-nez v2, :cond_2

    new-instance v2, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;-><init>(Lcom/google/android/gms/common/images/ImageManager;Landroid/net/Uri;)V

    iget-object v4, p0, Lcom/google/android/gms/common/images/ImageManager;->mUriMap:Ljava/util/Map;

    invoke-interface {v4, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v2, p1}, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->addOnImageLoadedListenerHolder(Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;)V

    iget-object v4, p0, Lcom/google/android/gms/common/images/ImageManager;->mListenerHolderMap:Ljava/util/Map;

    invoke-interface {v4, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.common.images.LOAD_IMAGE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms.extras.uri"

    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v4, "com.google.android.gms.extras.resultReceiver"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v4, "com.google.android.gms.extras.priority"

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/gms/common/images/ImageManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public loadImage(Landroid/widget/ImageView;I)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/common/images/ImageManager;->loadImage(Landroid/widget/ImageView;Landroid/net/Uri;I)V

    return-void
.end method

.method public loadImage(Landroid/widget/ImageView;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/common/images/ImageManager;->loadImage(Landroid/widget/ImageView;Landroid/net/Uri;I)V

    return-void
.end method

.method public loadImage(Landroid/widget/ImageView;Landroid/net/Uri;I)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Landroid/net/Uri;
    .param p3    # I

    new-instance v0, Lcom/google/android/gms/common/images/ImageManager$ImageViewListenerHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p3, v1}, Lcom/google/android/gms/common/images/ImageManager$ImageViewListenerHolder;-><init>(Lcom/google/android/gms/common/images/ImageManager;Landroid/widget/ImageView;ILcom/google/android/gms/common/images/ImageManager$1;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/common/images/ImageManager;->loadImage(Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;Landroid/net/Uri;)V

    return-void
.end method

.method public loadImage(Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;
    .param p2    # Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/common/images/ImageManager;->loadImage(Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;Landroid/net/Uri;I)V

    return-void
.end method

.method public loadImage(Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;Landroid/net/Uri;I)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;
    .param p2    # Landroid/net/Uri;
    .param p3    # I

    new-instance v0, Lcom/google/android/gms/common/images/ImageManager$ImageLoadedListenerHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p3, v1}, Lcom/google/android/gms/common/images/ImageManager$ImageLoadedListenerHolder;-><init>(Lcom/google/android/gms/common/images/ImageManager;Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;ILcom/google/android/gms/common/images/ImageManager$1;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/common/images/ImageManager;->loadImage(Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;Landroid/net/Uri;)V

    return-void
.end method
