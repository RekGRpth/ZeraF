.class final Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;
.super Ljava/lang/Object;
.source "ImageMultiThreadedIntentService.java"

# interfaces
.implements Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LoadImageOperation"
.end annotation


# instance fields
.field private final mPriority:I

.field private final mResultReceiver:Landroid/os/ResultReceiver;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/os/ResultReceiver;I)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/os/ResultReceiver;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->mUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->mResultReceiver:Landroid/os/ResultReceiver;

    iput p3, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->mPriority:I

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/common/images/ImageBroker;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/images/ImageBroker;

    iget-object v2, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->mUri:Landroid/net/Uri;

    invoke-virtual {p2, p1, v2}, Lcom/google/android/gms/common/images/ImageBroker;->loadImageFile(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    # getter for: Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed LoadImageOperation"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->sendFileDescriptor(Landroid/os/ParcelFileDescriptor;)V

    :goto_0
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->sendFileDescriptor(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :catch_0
    move-exception v1

    # getter for: Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to close file"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public sendFileDescriptor(Landroid/os/ParcelFileDescriptor;)V
    .locals 3
    .param p1    # Landroid/os/ParcelFileDescriptor;

    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->mResultReceiver:Landroid/os/ResultReceiver;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.google.android.gms.extra.fileDescriptor"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;->mResultReceiver:Landroid/os/ResultReceiver;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto :goto_0
.end method
