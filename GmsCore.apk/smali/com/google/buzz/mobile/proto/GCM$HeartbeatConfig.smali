.class public final Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HeartbeatConfig"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasIntervalMs:Z

.field private hasIp:Z

.field private hasUploadStat:Z

.field private intervalMs_:I

.field private ip_:Ljava/lang/String;

.field private uploadStat_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v1, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->uploadStat_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->ip_:Ljava/lang/String;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->intervalMs_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->cachedSize:I

    return v0
.end method

.method public getIntervalMs()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->intervalMs_:I

    return v0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->ip_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasUploadStat()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getUploadStat()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIp()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getIp()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIntervalMs()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getIntervalMs()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->cachedSize:I

    return v0
.end method

.method public getUploadStat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->uploadStat_:Z

    return v0
.end method

.method public hasIntervalMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIntervalMs:Z

    return v0
.end method

.method public hasIp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIp:Z

    return v0
.end method

.method public hasUploadStat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasUploadStat:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->setUploadStat(Z)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->setIp(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->setIntervalMs(I)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;

    move-result-object v0

    return-object v0
.end method

.method public setIntervalMs(I)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIntervalMs:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->intervalMs_:I

    return-object p0
.end method

.method public setIp(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIp:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->ip_:Ljava/lang/String;

    return-object p0
.end method

.method public setUploadStat(Z)Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasUploadStat:Z

    iput-boolean p1, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->uploadStat_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasUploadStat()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getUploadStat()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIp()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getIp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->hasIntervalMs()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getIntervalMs()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    return-void
.end method
