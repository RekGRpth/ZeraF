.class public final Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MessageStanza"
.end annotation


# instance fields
.field private accountId_:J

.field private body_:Ljava/lang/String;

.field private cachedSize:I

.field private error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

.field private extension_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$Extension;",
            ">;"
        }
    .end annotation
.end field

.field private from_:Ljava/lang/String;

.field private hasAccountId:Z

.field private hasBody:Z

.field private hasError:Z

.field private hasFrom:Z

.field private hasId:Z

.field private hasLastStreamIdReceived:Z

.field private hasNosave:Z

.field private hasPersistentId:Z

.field private hasRead:Z

.field private hasRmqId:Z

.field private hasStreamId:Z

.field private hasSubject:Z

.field private hasThread:Z

.field private hasTimestamp:Z

.field private hasTo:Z

.field private hasType:Z

.field private id_:Ljava/lang/String;

.field private lastStreamIdReceived_:I

.field private nosave_:Z

.field private persistentId_:Ljava/lang/String;

.field private read_:Z

.field private rmqId_:J

.field private streamId_:I

.field private subject_:Ljava/lang/String;

.field private thread_:Ljava/lang/String;

.field private timestamp_:J

.field private to_:Ljava/lang/String;

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v2, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->rmqId_:J

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->from_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->to_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->subject_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->body_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->thread_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->extension_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->nosave_:Z

    iput-wide v2, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->timestamp_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->persistentId_:Ljava/lang/String;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->streamId_:I

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->lastStreamIdReceived_:I

    iput-boolean v1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->read_:Z

    iput-wide v2, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->accountId_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addExtension(Lcom/google/buzz/mobile/proto/GCM$Extension;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$Extension;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->extension_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->extension_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->accountId_:J

    return-wide v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->body_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->cachedSize:I

    return v0
.end method

.method public getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object v0
.end method

.method public getExtensionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$Extension;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->extension_:Ljava/util/List;

    return-object v0
.end method

.method public getFrom()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->from_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastStreamIdReceived()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->lastStreamIdReceived_:I

    return v0
.end method

.method public getNosave()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->nosave_:Z

    return v0
.end method

.method public getPersistentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->persistentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getRead()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->read_:Z

    return v0
.end method

.method public getRmqId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->rmqId_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRmqId()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getRmqId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasType()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasId()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasFrom()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getFrom()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTo()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getTo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasSubject()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getSubject()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasBody()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasThread()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getThread()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasError()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getExtensionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$Extension;

    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasNosave()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getNosave()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTimestamp()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getTimestamp()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasPersistentId()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasStreamId()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getStreamId()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasLastStreamIdReceived()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getLastStreamIdReceived()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRead()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getRead()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasAccountId()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getAccountId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->cachedSize:I

    return v2
.end method

.method public getStreamId()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->streamId_:I

    return v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->subject_:Ljava/lang/String;

    return-object v0
.end method

.method public getThread()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->thread_:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->timestamp_:J

    return-wide v0
.end method

.method public getTo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->to_:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->type_:I

    return v0
.end method

.method public hasAccountId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasAccountId:Z

    return v0
.end method

.method public hasBody()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasBody:Z

    return v0
.end method

.method public hasError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasError:Z

    return v0
.end method

.method public hasFrom()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasFrom:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasId:Z

    return v0
.end method

.method public hasLastStreamIdReceived()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasLastStreamIdReceived:Z

    return v0
.end method

.method public hasNosave()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasNosave:Z

    return v0
.end method

.method public hasPersistentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasPersistentId:Z

    return v0
.end method

.method public hasRead()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRead:Z

    return v0
.end method

.method public hasRmqId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRmqId:Z

    return v0
.end method

.method public hasStreamId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasStreamId:Z

    return v0
.end method

.method public hasSubject()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasSubject:Z

    return v0
.end method

.method public hasThread()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasThread:Z

    return v0
.end method

.method public hasTimestamp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTimestamp:Z

    return v0
.end method

.method public hasTo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTo:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setRmqId(J)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setType(I)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setFrom(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setSubject(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setBody(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setThread(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$Extension;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$Extension;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->addExtension(Lcom/google/buzz/mobile/proto/GCM$Extension;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setNosave(Z)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setTimestamp(J)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setRead(Z)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    move-result-object v0

    return-object v0
.end method

.method public setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasAccountId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->accountId_:J

    return-object p0
.end method

.method public setBody(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasBody:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->body_:Ljava/lang/String;

    return-object p0
.end method

.method public setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasError:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object p0
.end method

.method public setFrom(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasFrom:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->from_:Ljava/lang/String;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasLastStreamIdReceived:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->lastStreamIdReceived_:I

    return-object p0
.end method

.method public setNosave(Z)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasNosave:Z

    iput-boolean p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->nosave_:Z

    return-object p0
.end method

.method public setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasPersistentId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->persistentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setRead(Z)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRead:Z

    iput-boolean p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->read_:Z

    return-object p0
.end method

.method public setRmqId(J)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRmqId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->rmqId_:J

    return-object p0
.end method

.method public setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasStreamId:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->streamId_:I

    return-object p0
.end method

.method public setSubject(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasSubject:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->subject_:Ljava/lang/String;

    return-object p0
.end method

.method public setThread(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasThread:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->thread_:Ljava/lang/String;

    return-object p0
.end method

.method public setTimestamp(J)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTimestamp:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->timestamp_:J

    return-object p0
.end method

.method public setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTo:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->to_:Ljava/lang/String;

    return-object p0
.end method

.method public setType(I)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasType:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRmqId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getRmqId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasType()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasFrom()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTo()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasSubject()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasBody()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasThread()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getThread()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasError()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getExtensionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$Extension;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasNosave()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getNosave()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasTimestamp()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getTimestamp()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasPersistentId()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasStreamId()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getStreamId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasLastStreamIdReceived()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getLastStreamIdReceived()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasRead()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getRead()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->hasAccountId()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_10
    return-void
.end method
