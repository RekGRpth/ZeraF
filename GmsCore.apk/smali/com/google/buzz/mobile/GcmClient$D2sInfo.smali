.class Lcom/google/buzz/mobile/GcmClient$D2sInfo;
.super Ljava/lang/Object;
.source "GcmClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/GcmClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "D2sInfo"
.end annotation


# instance fields
.field public d2sId:Ljava/lang/String;

.field public streamId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->streamId:I

    iput-object p2, p0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->d2sId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/buzz/mobile/GcmClient$D2sInfo;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->streamId:I

    iget v3, v0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->streamId:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->d2sId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->d2sId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->d2sId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
