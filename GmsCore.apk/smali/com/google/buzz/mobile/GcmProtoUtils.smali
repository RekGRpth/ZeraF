.class public final Lcom/google/buzz/mobile/GcmProtoUtils;
.super Ljava/lang/Object;
.source "GcmProtoUtils.java"


# static fields
.field private static FILTER:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static pbTags:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;"
        }
    .end annotation
.end field

.field static pbTypes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/buzz/mobile/GcmProtoUtils;->pbTypes:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/buzz/mobile/GcmProtoUtils;->pbTags:Ljava/util/Map;

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;-><init>()V

    invoke-static {v2, v0}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;-><init>()V

    invoke-static {v3, v0}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/4 v0, 0x4

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$Close;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$Close;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0xa

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$StreamErrorStanza;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$StreamErrorStanza;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/4 v0, 0x3

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$LoginResponse;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$LoginResponse;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/4 v0, 0x5

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/4 v0, 0x6

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/4 v0, 0x7

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0xf

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$TalkMetadata;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$TalkMetadata;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0x8

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0x9

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0xb

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$HttpRequest;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$HttpRequest;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0xc

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$HttpResponse;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$HttpResponse;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0xd

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/16 v0, 0xe

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    const/4 v0, 0x2

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;-><init>()V

    invoke-static {v0, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->initType(BLcom/google/protobuf/micro/MessageMicro;)V

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "authToken"

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/buzz/mobile/GcmProtoUtils;->FILTER:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getLastStreamId(Lcom/google/protobuf/micro/MessageMicro;)I
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;

    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;->getLastStreamIdReceived()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getLastStreamIdReceived()I

    move-result v0

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getLastStreamIdReceived()I

    move-result v0

    goto :goto_0

    :cond_2
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;->getLastStreamIdReceived()I

    move-result v0

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;->getLastStreamIdReceived()I

    move-result v0

    goto :goto_0

    :cond_4
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    if-eqz v0, :cond_5

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getLastStreamIdReceived()I

    move-result v0

    goto :goto_0

    :cond_5
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    if-eqz v0, :cond_6

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getLastStreamIdReceived()I

    move-result v0

    goto :goto_0

    :cond_6
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    if-eqz v0, :cond_7

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getLastStreamIdReceived()I

    move-result v0

    goto :goto_0

    :cond_7
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static getRmq2Id(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;

    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getPersistentId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    if-eqz v0, :cond_5

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getTag(Lcom/google/protobuf/micro/MessageMicro;)B
    .locals 2
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;

    sget-object v0, Lcom/google/buzz/mobile/GcmProtoUtils;->pbTypes:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    return v0
.end method

.method private static initType(BLcom/google/protobuf/micro/MessageMicro;)V
    .locals 3
    .param p0    # B
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    sget-object v0, Lcom/google/buzz/mobile/GcmProtoUtils;->pbTypes:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/buzz/mobile/GcmProtoUtils;->pbTags:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static newStreamAck()Lcom/google/buzz/mobile/proto/GCM$IqStanza;
    .locals 4

    new-instance v2, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    invoke-direct {v2}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;->setType(I)Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$StreamAck;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$StreamAck;-><init>()V

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$Extension;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$Extension;-><init>()V

    const/16 v3, 0xd

    invoke-virtual {v1, v3}, Lcom/google/buzz/mobile/proto/GCM$Extension;->setId(I)Lcom/google/buzz/mobile/proto/GCM$Extension;

    sget-object v3, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    invoke-virtual {v1, v3}, Lcom/google/buzz/mobile/proto/GCM$Extension;->setData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/buzz/mobile/proto/GCM$Extension;

    invoke-virtual {v2, v1}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;->setExtension(Lcom/google/buzz/mobile/proto/GCM$Extension;)Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    return-object v2
.end method

.method public static parse(B[B)Lcom/google/protobuf/micro/MessageMicro;
    .locals 6
    .param p0    # B
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v4, Lcom/google/buzz/mobile/GcmProtoUtils;->pbTags:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/micro/MessageMicro;

    if-nez v2, :cond_0

    new-instance v4, Ljava/io/IOException;

    const-string v5, "Unknown tag"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/protobuf/micro/MessageMicro;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    array-length v4, p1

    if-lez v4, :cond_1

    invoke-virtual {v3, p1}, Lcom/google/protobuf/micro/MessageMicro;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    :cond_1
    return-object v3

    :catch_0
    move-exception v4

    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method static setLastStreamId(Lcom/google/protobuf/micro/MessageMicro;I)V
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;
    .param p1    # I

    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    goto :goto_0

    :cond_2
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :cond_4
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    if-eqz v0, :cond_5

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :cond_5
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    if-eqz v0, :cond_6

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :cond_6
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    if-eqz v0, :cond_7

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :cond_7
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0
.end method

.method static setRmq2Id(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)V
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;
    .param p1    # Ljava/lang/String;

    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$IqStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$MessageStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$MessageStanza;

    goto :goto_0

    :cond_2
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :cond_4
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    if-eqz v0, :cond_5

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :cond_5
    instance-of v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0
.end method

.method public static toString(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0, p1}, Lcom/google/buzz/mobile/GcmProtoUtils;->toString(Ljava/lang/StringBuilder;Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static toString(Ljava/lang/StringBuilder;Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 19
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v7

    :try_start_0
    const-string v17, "("

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v2, v7

    array-length v11, v2

    const/4 v8, 0x0

    move v9, v8

    :goto_0
    if-ge v9, v11, :cond_5

    aget-object v6, v2, v9

    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v17, "_"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    const/16 v17, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    sget-object v17, Lcom/google/buzz/mobile/GcmProtoUtils;->FILTER:Ljava/util/List;

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x3d

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    instance-of v0, v15, Lcom/google/protobuf/micro/MessageMicro;

    move/from16 v17, v0

    if-eqz v17, :cond_2

    check-cast v15, Lcom/google/protobuf/micro/MessageMicro;

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/buzz/mobile/GcmProtoUtils;->toString(Ljava/lang/StringBuilder;Lcom/google/protobuf/micro/MessageMicro;)V

    :goto_2
    const-string v17, ","

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v5

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Error generating: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    return-void

    :cond_2
    :try_start_1
    instance-of v0, v15, [Lcom/google/protobuf/micro/MessageMicro;

    move/from16 v17, v0

    if-eqz v17, :cond_4

    check-cast v15, [Lcom/google/protobuf/micro/MessageMicro;

    move-object v0, v15

    check-cast v0, [Lcom/google/protobuf/micro/MessageMicro;

    move-object/from16 v16, v0

    const-string v17, "["

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v3, v16

    array-length v12, v3

    const/4 v8, 0x0

    :goto_4
    if-ge v8, v12, :cond_3

    aget-object v13, v3, v8

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/google/buzz/mobile/GcmProtoUtils;->toString(Ljava/lang/StringBuilder;Lcom/google/protobuf/micro/MessageMicro;)V

    const-string v17, ","

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_3
    const-string v17, "]"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    const-string v17, ")"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method
