.class public Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
.super Lcom/google/wireless/gdata2/GDataException;
.source "ResourceUnavailableException.java"


# instance fields
.field private retryAfter:J


# direct methods
.method public constructor <init>(J)V
    .locals 0
    .param p1    # J

    invoke-direct {p0}, Lcom/google/wireless/gdata2/GDataException;-><init>()V

    iput-wide p1, p0, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->retryAfter:J

    return-void
.end method


# virtual methods
.method public getRetryAfter()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->retryAfter:J

    return-wide v0
.end method
