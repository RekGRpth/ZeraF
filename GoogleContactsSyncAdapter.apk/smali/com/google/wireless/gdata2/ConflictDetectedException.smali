.class public Lcom/google/wireless/gdata2/ConflictDetectedException;
.super Lcom/google/wireless/gdata2/GDataException;
.source "ConflictDetectedException.java"


# instance fields
.field private final conflictingEntry:Lcom/google/wireless/gdata2/data/Entry;


# direct methods
.method public constructor <init>(Lcom/google/wireless/gdata2/data/Entry;)V
    .locals 0
    .param p1    # Lcom/google/wireless/gdata2/data/Entry;

    invoke-direct {p0}, Lcom/google/wireless/gdata2/GDataException;-><init>()V

    iput-object p1, p0, Lcom/google/wireless/gdata2/ConflictDetectedException;->conflictingEntry:Lcom/google/wireless/gdata2/data/Entry;

    return-void
.end method
