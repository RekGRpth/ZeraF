.class public Lcom/google/wireless/gdata2/data/Feed;
.super Ljava/lang/Object;
.source "Feed.java"


# instance fields
.field private category:Ljava/lang/String;

.field private categoryScheme:Ljava/lang/String;

.field private eTagValue:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private itemsPerPage:I

.field private lastUpdated:Ljava/lang/String;

.field private startIndex:I

.field private title:Ljava/lang/String;

.field private totalResults:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemsPerPage()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/gdata2/data/Feed;->itemsPerPage:I

    return v0
.end method

.method public getLastUpdated()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/Feed;->lastUpdated:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalResults()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/gdata2/data/Feed;->totalResults:I

    return v0
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Feed;->category:Ljava/lang/String;

    return-void
.end method

.method public setCategoryScheme(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Feed;->categoryScheme:Ljava/lang/String;

    return-void
.end method

.method public setETag(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Feed;->eTagValue:Ljava/lang/String;

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Feed;->id:Ljava/lang/String;

    return-void
.end method

.method public setItemsPerPage(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/gdata2/data/Feed;->itemsPerPage:I

    return-void
.end method

.method public setLastUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Feed;->lastUpdated:Ljava/lang/String;

    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/gdata2/data/Feed;->startIndex:I

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/Feed;->title:Ljava/lang/String;

    return-void
.end method

.method public setTotalResults(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/gdata2/data/Feed;->totalResults:I

    return-void
.end method
