.class public Lcom/jme3/texture/Image;
.super Lcom/jme3/util/NativeObject;
.source "Image.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/texture/Image$Format;
    }
.end annotation


# instance fields
.field protected data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field protected depth:I

.field protected transient efficientData:Ljava/lang/Object;

.field protected format:Lcom/jme3/texture/Image$Format;

.field protected height:I

.field protected mipMapSizes:[I

.field protected multiSamples:I

.field protected width:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    const-class v0, Lcom/jme3/texture/Image;

    invoke-direct {p0, v0}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;)V

    iput v1, p0, Lcom/jme3/texture/Image;->multiSamples:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    return-void
.end method

.method protected constructor <init>(I)V
    .locals 1

    const-class v0, Lcom/jme3/texture/Image;

    invoke-direct {p0, v0, p1}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;I)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/jme3/texture/Image;->multiSamples:I

    return-void
.end method

.method public constructor <init>(Lcom/jme3/texture/Image$Format;IIILjava/util/ArrayList;[I)V
    .locals 2
    .param p1    # Lcom/jme3/texture/Image$Format;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p6    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jme3/texture/Image$Format;",
            "III",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;[I)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jme3/texture/Image;-><init>()V

    if-eqz p6, :cond_0

    array-length v0, p6

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    const/4 p6, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jme3/texture/Image;->setFormat(Lcom/jme3/texture/Image$Format;)V

    iput p2, p0, Lcom/jme3/texture/Image;->width:I

    iput p3, p0, Lcom/jme3/texture/Image;->height:I

    iput-object p5, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    iput p4, p0, Lcom/jme3/texture/Image;->depth:I

    iput-object p6, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    return-void
.end method

.method public constructor <init>(Lcom/jme3/texture/Image$Format;IILjava/nio/ByteBuffer;)V
    .locals 6
    .param p1    # Lcom/jme3/texture/Image$Format;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/jme3/texture/Image;-><init>(Lcom/jme3/texture/Image$Format;IILjava/nio/ByteBuffer;[I)V

    return-void
.end method

.method public constructor <init>(Lcom/jme3/texture/Image$Format;IILjava/nio/ByteBuffer;[I)V
    .locals 2
    .param p1    # Lcom/jme3/texture/Image$Format;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/nio/ByteBuffer;
    .param p5    # [I

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/jme3/texture/Image;-><init>()V

    if-eqz p5, :cond_0

    array-length v0, p5

    if-gt v0, v1, :cond_0

    const/4 p5, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jme3/texture/Image;->setFormat(Lcom/jme3/texture/Image$Format;)V

    iput p2, p0, Lcom/jme3/texture/Image;->width:I

    iput p3, p0, Lcom/jme3/texture/Image;->height:I

    if-eqz p4, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iput-object p5, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    return-void
.end method


# virtual methods
.method public clone()Lcom/jme3/texture/Image;
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/jme3/util/NativeObject;->clone()Lcom/jme3/util/NativeObject;

    move-result-object v0

    check-cast v0, Lcom/jme3/texture/Image;

    iget-object v1, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    :goto_0
    iput-object v1, v0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    iget-object v1, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :cond_0
    iput-object v2, v0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-object v0

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clone()Lcom/jme3/util/NativeObject;
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->clone()Lcom/jme3/texture/Image;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->clone()Lcom/jme3/texture/Image;

    move-result-object v0

    return-object v0
.end method

.method public createDestructableClone()Lcom/jme3/util/NativeObject;
    .locals 2

    new-instance v0, Lcom/jme3/texture/Image;

    iget v1, p0, Lcom/jme3/texture/Image;->id:I

    invoke-direct {v0, v1}, Lcom/jme3/texture/Image;-><init>(I)V

    return-object v0
.end method

.method public deleteObject(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/renderer/Renderer;

    invoke-interface {p1, p0}, Lcom/jme3/renderer/Renderer;->deleteImage(Lcom/jme3/texture/Image;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/jme3/texture/Image;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/jme3/texture/Image;

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getFormat()Lcom/jme3/texture/Image$Format;

    move-result-object v3

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getFormat()Lcom/jme3/texture/Image$Format;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getWidth()I

    move-result v4

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getHeight()I

    move-result v4

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_7

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getMipMapSizes()[I

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getMipMapSizes()[I

    move-result-object v3

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getMipMapSizes()[I

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getMipMapSizes()[I

    move-result-object v3

    if-nez v3, :cond_9

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getMipMapSizes()[I

    move-result-object v3

    if-eqz v3, :cond_9

    move v1, v2

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getMultiSamples()I

    move-result v3

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getMultiSamples()I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto/16 :goto_0
.end method

.method public getData(I)Ljava/nio/ByteBuffer;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDepth()I
    .locals 1

    iget v0, p0, Lcom/jme3/texture/Image;->depth:I

    return v0
.end method

.method public getEfficentData()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jme3/texture/Image;->efficientData:Ljava/lang/Object;

    return-object v0
.end method

.method public getFormat()Lcom/jme3/texture/Image$Format;
    .locals 1

    iget-object v0, p0, Lcom/jme3/texture/Image;->format:Lcom/jme3/texture/Image$Format;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/jme3/texture/Image;->height:I

    return v0
.end method

.method public getMipMapSizes()[I
    .locals 1

    iget-object v0, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    return-object v0
.end method

.method public getMultiSamples()I
    .locals 1

    iget v0, p0, Lcom/jme3/texture/Image;->multiSamples:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/jme3/texture/Image;->width:I

    return v0
.end method

.method public hasMipmaps()Z
    .locals 1

    iget-object v0, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v2, 0x0

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/jme3/texture/Image;->format:Lcom/jme3/texture/Image$Format;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jme3/texture/Image;->format:Lcom/jme3/texture/Image$Format;

    invoke-virtual {v1}, Lcom/jme3/texture/Image$Format;->hashCode()I

    move-result v1

    :goto_0
    add-int/lit16 v0, v1, 0x2a7

    mul-int/lit8 v1, v0, 0x61

    iget v3, p0, Lcom/jme3/texture/Image;->width:I

    add-int v0, v1, v3

    mul-int/lit8 v1, v0, 0x61

    iget v3, p0, Lcom/jme3/texture/Image;->height:I

    add-int v0, v1, v3

    mul-int/lit8 v1, v0, 0x61

    iget v3, p0, Lcom/jme3/texture/Image;->depth:I

    add-int v0, v1, v3

    mul-int/lit8 v1, v0, 0x61

    iget-object v3, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([I)I

    move-result v3

    add-int v0, v1, v3

    mul-int/lit8 v1, v0, 0x61

    iget-object v3, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x61

    iget v2, p0, Lcom/jme3/texture/Image;->multiSamples:I

    add-int v0, v1, v2

    return v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v1

    const-string v0, "format"

    const-class v2, Lcom/jme3/texture/Image$Format;

    sget-object v3, Lcom/jme3/texture/Image$Format;->RGBA8:Lcom/jme3/texture/Image$Format;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/texture/Image$Format;

    iput-object v0, p0, Lcom/jme3/texture/Image;->format:Lcom/jme3/texture/Image$Format;

    const-string v0, "width"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/jme3/texture/Image;->width:I

    const-string v0, "height"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/jme3/texture/Image;->height:I

    const-string v0, "depth"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/jme3/texture/Image;->depth:I

    const-string v0, "mipMapSizes"

    invoke-interface {v1, v0, v5}, Lcom/jme3/export/InputCapsule;->readIntArray(Ljava/lang/String;[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/texture/Image;->mipMapSizes:[I

    const-string v0, "multiSamples"

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/jme3/texture/Image;->multiSamples:I

    const-string v0, "data"

    invoke-interface {v1, v0, v5}, Lcom/jme3/export/InputCapsule;->readByteBufferArrayList(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    return-void
.end method

.method public resetObject()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/jme3/texture/Image;->id:I

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-void
.end method

.method public setData(ILjava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/nio/ByteBuffer;

    if-ltz p1, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "index must be greater than or equal to 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setData(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/jme3/texture/Image;->data:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-void
.end method

.method public setEfficentData(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/jme3/texture/Image;->efficientData:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-void
.end method

.method public setFormat(Lcom/jme3/texture/Image$Format;)V
    .locals 2
    .param p1    # Lcom/jme3/texture/Image$Format;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "format may not be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/jme3/texture/Image;->format:Lcom/jme3/texture/Image$Format;

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jme3/texture/Image;->height:I

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jme3/texture/Image;->width:I

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->setUpdateNeeded()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "[size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jme3/texture/Image;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jme3/texture/Image;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/texture/Image;->depth:I

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jme3/texture/Image;->depth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, ", format="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/texture/Image;->format:Lcom/jme3/texture/Image$Format;

    invoke-virtual {v2}, Lcom/jme3/texture/Image$Format;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/jme3/texture/Image;->hasMipmaps()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ", mips"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0}, Lcom/jme3/texture/Image;->getId()I

    move-result v1

    if-ltz v1, :cond_2

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jme3/texture/Image;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
