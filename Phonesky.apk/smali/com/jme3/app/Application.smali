.class public Lcom/jme3/app/Application;
.super Ljava/lang/Object;
.source "Application.java"

# interfaces
.implements Lcom/jme3/system/SystemListener;


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected assetManager:Lcom/jme3/asset/AssetManager;

.field protected audioRenderer:Lcom/jme3/audio/AudioRenderer;

.field protected cam:Lcom/jme3/renderer/Camera;

.field protected context:Lcom/jme3/system/JmeContext;

.field protected guiViewPort:Lcom/jme3/renderer/ViewPort;

.field protected inputEnabled:Z

.field protected inputManager:Lcom/jme3/input/InputManager;

.field protected joyInput:Lcom/jme3/input/JoyInput;

.field protected keyInput:Lcom/jme3/input/KeyInput;

.field protected listener:Lcom/jme3/audio/Listener;

.field protected mouseInput:Lcom/jme3/input/MouseInput;

.field protected pauseOnFocus:Z

.field protected paused:Z

.field protected renderManager:Lcom/jme3/renderer/RenderManager;

.field protected renderer:Lcom/jme3/renderer/Renderer;

.field protected settings:Lcom/jme3/system/AppSettings;

.field protected speed:F

.field protected stateManager:Lcom/jme3/app/state/AppStateManager;

.field private final taskQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/jme3/app/AppTask",
            "<*>;>;"
        }
    .end annotation
.end field

.field protected timer:Lcom/jme3/system/Timer;

.field protected touchInput:Lcom/jme3/input/TouchInput;

.field protected viewPort:Lcom/jme3/renderer/ViewPort;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/app/Application;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/app/Application;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/system/NanoTimer;

    invoke-direct {v0}, Lcom/jme3/system/NanoTimer;-><init>()V

    iput-object v0, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    iput-boolean v1, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/app/Application;->pauseOnFocus:Z

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/app/Application;->speed:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/app/Application;->paused:Z

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/jme3/app/Application;->taskQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p0}, Lcom/jme3/app/Application;->initStateManager()V

    return-void
.end method

.method private initAssetManager()V
    .locals 4

    iget-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    const-string v1, "AssetConfigURL"

    invoke-virtual {v0, v1}, Lcom/jme3/system/AppSettings;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-nez v0, :cond_1

    const-class v0, Lcom/jme3/app/Application;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/jme3/app/Application;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Unable to access AssetConfigURL in asset config:{0}"

    invoke-virtual {v0, v1, v3, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/jme3/system/JmeSystem;->newAssetManager(Ljava/net/URL;)Lcom/jme3/asset/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->assetManager:Lcom/jme3/asset/AssetManager;

    :cond_2
    iget-object v0, p0, Lcom/jme3/app/Application;->assetManager:Lcom/jme3/asset/AssetManager;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "com/jme3/asset/Desktop.cfg"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/jme3/system/JmeSystem;->newAssetManager(Ljava/net/URL;)Lcom/jme3/asset/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->assetManager:Lcom/jme3/asset/AssetManager;

    goto :goto_1
.end method

.method private initAudio()V
    .locals 2

    iget-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v0}, Lcom/jme3/system/AppSettings;->getAudioRenderer()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getType()Lcom/jme3/system/JmeContext$Type;

    move-result-object v0

    sget-object v1, Lcom/jme3/system/JmeContext$Type;->Headless:Lcom/jme3/system/JmeContext$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-static {v0}, Lcom/jme3/system/JmeSystem;->newAudioRenderer(Lcom/jme3/system/AppSettings;)Lcom/jme3/audio/AudioRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    iget-object v0, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    invoke-interface {v0}, Lcom/jme3/audio/AudioRenderer;->initialize()V

    iget-object v0, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    invoke-static {v0}, Lcom/jme3/audio/AudioContext;->setAudioRenderer(Lcom/jme3/audio/AudioRenderer;)V

    new-instance v0, Lcom/jme3/audio/Listener;

    invoke-direct {v0}, Lcom/jme3/audio/Listener;-><init>()V

    iput-object v0, p0, Lcom/jme3/app/Application;->listener:Lcom/jme3/audio/Listener;

    iget-object v0, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    iget-object v1, p0, Lcom/jme3/app/Application;->listener:Lcom/jme3/audio/Listener;

    invoke-interface {v0, v1}, Lcom/jme3/audio/AudioRenderer;->setListener(Lcom/jme3/audio/Listener;)V

    :cond_0
    return-void
.end method

.method private initCamera()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    new-instance v1, Lcom/jme3/renderer/Camera;

    iget-object v2, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v2}, Lcom/jme3/system/AppSettings;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v3}, Lcom/jme3/system/AppSettings;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/jme3/renderer/Camera;-><init>(II)V

    iput-object v1, p0, Lcom/jme3/app/Application;->cam:Lcom/jme3/renderer/Camera;

    iget-object v1, p0, Lcom/jme3/app/Application;->cam:Lcom/jme3/renderer/Camera;

    const/high16 v2, 0x42340000

    iget-object v3, p0, Lcom/jme3/app/Application;->cam:Lcom/jme3/renderer/Camera;

    invoke-virtual {v3}, Lcom/jme3/renderer/Camera;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/jme3/app/Application;->cam:Lcom/jme3/renderer/Camera;

    invoke-virtual {v4}, Lcom/jme3/renderer/Camera;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x447a0000

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/jme3/renderer/Camera;->setFrustumPerspective(FFFF)V

    iget-object v1, p0, Lcom/jme3/app/Application;->cam:Lcom/jme3/renderer/Camera;

    new-instance v2, Lcom/jme3/math/Vector3f;

    const/high16 v3, 0x41200000

    invoke-direct {v2, v6, v6, v3}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    invoke-virtual {v1, v2}, Lcom/jme3/renderer/Camera;->setLocation(Lcom/jme3/math/Vector3f;)V

    iget-object v1, p0, Lcom/jme3/app/Application;->cam:Lcom/jme3/renderer/Camera;

    new-instance v2, Lcom/jme3/math/Vector3f;

    invoke-direct {v2, v6, v6, v6}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sget-object v3, Lcom/jme3/math/Vector3f;->UNIT_Y:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2, v3}, Lcom/jme3/renderer/Camera;->lookAt(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V

    new-instance v1, Lcom/jme3/renderer/RenderManager;

    iget-object v2, p0, Lcom/jme3/app/Application;->renderer:Lcom/jme3/renderer/Renderer;

    invoke-direct {v1, v2}, Lcom/jme3/renderer/RenderManager;-><init>(Lcom/jme3/renderer/Renderer;)V

    iput-object v1, p0, Lcom/jme3/app/Application;->renderManager:Lcom/jme3/renderer/RenderManager;

    iget-object v1, p0, Lcom/jme3/app/Application;->renderManager:Lcom/jme3/renderer/RenderManager;

    iget-object v2, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    invoke-virtual {v1, v2}, Lcom/jme3/renderer/RenderManager;->setTimer(Lcom/jme3/system/Timer;)V

    iget-object v1, p0, Lcom/jme3/app/Application;->renderManager:Lcom/jme3/renderer/RenderManager;

    const-string v2, "Default"

    iget-object v3, p0, Lcom/jme3/app/Application;->cam:Lcom/jme3/renderer/Camera;

    invoke-virtual {v1, v2, v3}, Lcom/jme3/renderer/RenderManager;->createMainView(Ljava/lang/String;Lcom/jme3/renderer/Camera;)Lcom/jme3/renderer/ViewPort;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/app/Application;->viewPort:Lcom/jme3/renderer/ViewPort;

    iget-object v1, p0, Lcom/jme3/app/Application;->viewPort:Lcom/jme3/renderer/ViewPort;

    invoke-virtual {v1, v8, v8, v8}, Lcom/jme3/renderer/ViewPort;->setClearFlags(ZZZ)V

    new-instance v0, Lcom/jme3/renderer/Camera;

    iget-object v1, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v1}, Lcom/jme3/system/AppSettings;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v2}, Lcom/jme3/system/AppSettings;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/jme3/renderer/Camera;-><init>(II)V

    iget-object v1, p0, Lcom/jme3/app/Application;->renderManager:Lcom/jme3/renderer/RenderManager;

    const-string v2, "Gui Default"

    invoke-virtual {v1, v2, v0}, Lcom/jme3/renderer/RenderManager;->createPostView(Ljava/lang/String;Lcom/jme3/renderer/Camera;)Lcom/jme3/renderer/ViewPort;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/app/Application;->guiViewPort:Lcom/jme3/renderer/ViewPort;

    iget-object v1, p0, Lcom/jme3/app/Application;->guiViewPort:Lcom/jme3/renderer/ViewPort;

    invoke-virtual {v1, v7, v7, v7}, Lcom/jme3/renderer/ViewPort;->setClearFlags(ZZZ)V

    return-void
.end method

.method private initDisplay()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getSettings()Lcom/jme3/system/AppSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    iget-object v0, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getTimer()Lcom/jme3/system/Timer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    :cond_0
    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getRenderer()Lcom/jme3/renderer/Renderer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->renderer:Lcom/jme3/renderer/Renderer;

    return-void
.end method

.method private initInput()V
    .locals 5

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getMouseInput()Lcom/jme3/input/MouseInput;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->mouseInput:Lcom/jme3/input/MouseInput;

    iget-object v0, p0, Lcom/jme3/app/Application;->mouseInput:Lcom/jme3/input/MouseInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/app/Application;->mouseInput:Lcom/jme3/input/MouseInput;

    invoke-interface {v0}, Lcom/jme3/input/MouseInput;->initialize()V

    :cond_0
    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getKeyInput()Lcom/jme3/input/KeyInput;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->keyInput:Lcom/jme3/input/KeyInput;

    iget-object v0, p0, Lcom/jme3/app/Application;->keyInput:Lcom/jme3/input/KeyInput;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/app/Application;->keyInput:Lcom/jme3/input/KeyInput;

    invoke-interface {v0}, Lcom/jme3/input/KeyInput;->initialize()V

    :cond_1
    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getTouchInput()Lcom/jme3/input/TouchInput;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->touchInput:Lcom/jme3/input/TouchInput;

    iget-object v0, p0, Lcom/jme3/app/Application;->touchInput:Lcom/jme3/input/TouchInput;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jme3/app/Application;->touchInput:Lcom/jme3/input/TouchInput;

    invoke-interface {v0}, Lcom/jme3/input/TouchInput;->initialize()V

    :cond_2
    iget-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    const-string v1, "DisableJoysticks"

    invoke-virtual {v0, v1}, Lcom/jme3/system/AppSettings;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->getJoyInput()Lcom/jme3/input/JoyInput;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->joyInput:Lcom/jme3/input/JoyInput;

    iget-object v0, p0, Lcom/jme3/app/Application;->joyInput:Lcom/jme3/input/JoyInput;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jme3/app/Application;->joyInput:Lcom/jme3/input/JoyInput;

    invoke-interface {v0}, Lcom/jme3/input/JoyInput;->initialize()V

    :cond_3
    new-instance v0, Lcom/jme3/input/InputManager;

    iget-object v1, p0, Lcom/jme3/app/Application;->mouseInput:Lcom/jme3/input/MouseInput;

    iget-object v2, p0, Lcom/jme3/app/Application;->keyInput:Lcom/jme3/input/KeyInput;

    iget-object v3, p0, Lcom/jme3/app/Application;->joyInput:Lcom/jme3/input/JoyInput;

    iget-object v4, p0, Lcom/jme3/app/Application;->touchInput:Lcom/jme3/input/TouchInput;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/jme3/input/InputManager;-><init>(Lcom/jme3/input/MouseInput;Lcom/jme3/input/KeyInput;Lcom/jme3/input/JoyInput;Lcom/jme3/input/TouchInput;)V

    iput-object v0, p0, Lcom/jme3/app/Application;->inputManager:Lcom/jme3/input/InputManager;

    return-void
.end method

.method private initStateManager()V
    .locals 2

    new-instance v0, Lcom/jme3/app/state/AppStateManager;

    invoke-direct {v0, p0}, Lcom/jme3/app/state/AppStateManager;-><init>(Lcom/jme3/app/Application;)V

    iput-object v0, p0, Lcom/jme3/app/Application;->stateManager:Lcom/jme3/app/state/AppStateManager;

    iget-object v0, p0, Lcom/jme3/app/Application;->stateManager:Lcom/jme3/app/state/AppStateManager;

    new-instance v1, Lcom/jme3/app/ResetStatsState;

    invoke-direct {v1}, Lcom/jme3/app/ResetStatsState;-><init>()V

    invoke-virtual {v0, v1}, Lcom/jme3/app/state/AppStateManager;->attach(Lcom/jme3/app/state/AppState;)Z

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/app/Application;->stateManager:Lcom/jme3/app/state/AppStateManager;

    invoke-virtual {v0}, Lcom/jme3/app/state/AppStateManager;->cleanup()V

    invoke-virtual {p0}, Lcom/jme3/app/Application;->destroyInput()V

    iget-object v0, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    invoke-interface {v0}, Lcom/jme3/audio/AudioRenderer;->cleanup()V

    :cond_0
    iget-object v0, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    invoke-virtual {v0}, Lcom/jme3/system/Timer;->reset()V

    return-void
.end method

.method protected destroyInput()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/app/Application;->mouseInput:Lcom/jme3/input/MouseInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/app/Application;->mouseInput:Lcom/jme3/input/MouseInput;

    invoke-interface {v0}, Lcom/jme3/input/MouseInput;->destroy()V

    :cond_0
    iget-object v0, p0, Lcom/jme3/app/Application;->keyInput:Lcom/jme3/input/KeyInput;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/app/Application;->keyInput:Lcom/jme3/input/KeyInput;

    invoke-interface {v0}, Lcom/jme3/input/KeyInput;->destroy()V

    :cond_1
    iget-object v0, p0, Lcom/jme3/app/Application;->joyInput:Lcom/jme3/input/JoyInput;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jme3/app/Application;->joyInput:Lcom/jme3/input/JoyInput;

    invoke-interface {v0}, Lcom/jme3/input/JoyInput;->destroy()V

    :cond_2
    iget-object v0, p0, Lcom/jme3/app/Application;->touchInput:Lcom/jme3/input/TouchInput;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jme3/app/Application;->touchInput:Lcom/jme3/input/TouchInput;

    invoke-interface {v0}, Lcom/jme3/input/TouchInput;->destroy()V

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/app/Application;->inputManager:Lcom/jme3/input/InputManager;

    return-void
.end method

.method public getAudioRenderer()Lcom/jme3/audio/AudioRenderer;
    .locals 1

    iget-object v0, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    return-object v0
.end method

.method public getContext()Lcom/jme3/system/JmeContext;
    .locals 1

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    return-object v0
.end method

.method public getInputManager()Lcom/jme3/input/InputManager;
    .locals 1

    iget-object v0, p0, Lcom/jme3/app/Application;->inputManager:Lcom/jme3/input/InputManager;

    return-object v0
.end method

.method public handleError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    sget-object v0, Lcom/jme3/app/Application;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1, p1, p2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/jme3/app/Application;->stop()V

    return-void
.end method

.method public initialize()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/app/Application;->assetManager:Lcom/jme3/asset/AssetManager;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/jme3/app/Application;->initAssetManager()V

    :cond_0
    invoke-direct {p0}, Lcom/jme3/app/Application;->initDisplay()V

    invoke-direct {p0}, Lcom/jme3/app/Application;->initCamera()V

    iget-boolean v0, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/jme3/app/Application;->initInput()V

    :cond_1
    invoke-direct {p0}, Lcom/jme3/app/Application;->initAudio()V

    iget-object v0, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    invoke-virtual {v0}, Lcom/jme3/system/Timer;->reset()V

    return-void
.end method

.method public reshape(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/jme3/app/Application;->renderManager:Lcom/jme3/renderer/RenderManager;

    invoke-virtual {v0, p1, p2}, Lcom/jme3/renderer/RenderManager;->notifyReshape(II)V

    return-void
.end method

.method public restart()V
    .locals 2

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    iget-object v1, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-interface {v0, v1}, Lcom/jme3/system/JmeContext;->setSettings(Lcom/jme3/system/AppSettings;)V

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->restart()V

    return-void
.end method

.method public setSettings(Lcom/jme3/system/AppSettings;)V
    .locals 2
    .param p1    # Lcom/jme3/system/AppSettings;

    iput-object p1, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/jme3/system/AppSettings;->useInput()Z

    move-result v0

    iget-boolean v1, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    if-eq v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    iget-boolean v0, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/jme3/app/Application;->initInput()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/jme3/app/Application;->destroyInput()V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/jme3/system/AppSettings;->useInput()Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    goto :goto_1
.end method

.method public start()V
    .locals 1

    sget-object v0, Lcom/jme3/system/JmeContext$Type;->Display:Lcom/jme3/system/JmeContext$Type;

    invoke-virtual {p0, v0}, Lcom/jme3/app/Application;->start(Lcom/jme3/system/JmeContext$Type;)V

    return-void
.end method

.method public start(Lcom/jme3/system/JmeContext$Type;)V
    .locals 4
    .param p1    # Lcom/jme3/system/JmeContext$Type;

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0}, Lcom/jme3/system/JmeContext;->isCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/app/Application;->logger:Ljava/util/logging/Logger;

    const-string v1, "start() called when application already created!"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    if-nez v0, :cond_1

    new-instance v0, Lcom/jme3/system/AppSettings;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/jme3/system/AppSettings;-><init>(Z)V

    iput-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    :cond_1
    sget-object v0, Lcom/jme3/app/Application;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "Starting application: {0}"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jme3/app/Application;->settings:Lcom/jme3/system/AppSettings;

    invoke-static {v0, p1}, Lcom/jme3/system/JmeSystem;->newContext(Lcom/jme3/system/AppSettings;Lcom/jme3/system/JmeContext$Type;)Lcom/jme3/system/JmeContext;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0, p0}, Lcom/jme3/system/JmeContext;->setSystemListener(Lcom/jme3/system/SystemListener;)V

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/jme3/system/JmeContext;->create(Z)V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jme3/app/Application;->stop(Z)V

    return-void
.end method

.method public stop(Z)V
    .locals 4
    .param p1    # Z

    sget-object v0, Lcom/jme3/app/Application;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "Closing application: {0}"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jme3/app/Application;->context:Lcom/jme3/system/JmeContext;

    invoke-interface {v0, p1}, Lcom/jme3/system/JmeContext;->destroy(Z)V

    return-void
.end method

.method public update()V
    .locals 3

    iget-object v1, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    invoke-static {v1}, Lcom/jme3/audio/AudioContext;->setAudioRenderer(Lcom/jme3/audio/AudioRenderer;)V

    iget-object v1, p0, Lcom/jme3/app/Application;->taskQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/app/AppTask;

    :cond_0
    if-nez v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/jme3/app/Application;->speed:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/jme3/app/Application;->paused:Z

    if-eqz v1, :cond_4

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {v0}, Lcom/jme3/app/AppTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/jme3/app/Application;->taskQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/app/AppTask;

    if-nez v0, :cond_2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/jme3/app/AppTask;->invoke()V

    iget-object v1, p0, Lcom/jme3/app/Application;->taskQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/app/AppTask;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    invoke-virtual {v1}, Lcom/jme3/system/Timer;->update()V

    iget-boolean v1, p0, Lcom/jme3/app/Application;->inputEnabled:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/jme3/app/Application;->inputManager:Lcom/jme3/input/InputManager;

    iget-object v2, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    invoke-virtual {v2}, Lcom/jme3/system/Timer;->getTimePerFrame()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jme3/input/InputManager;->update(F)V

    :cond_5
    iget-object v1, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jme3/app/Application;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    iget-object v2, p0, Lcom/jme3/app/Application;->timer:Lcom/jme3/system/Timer;

    invoke-virtual {v2}, Lcom/jme3/system/Timer;->getTimePerFrame()F

    move-result v2

    invoke-interface {v1, v2}, Lcom/jme3/audio/AudioRenderer;->update(F)V

    goto :goto_1
.end method
