.class public Lcom/jme3/scene/plugins/ogre/matext/MaterialExtensionSet;
.super Ljava/lang/Object;
.source "MaterialExtensionSet.java"


# instance fields
.field private extensions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/jme3/scene/plugins/ogre/matext/MaterialExtension;",
            ">;"
        }
    .end annotation
.end field

.field private nameMappings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/matext/MaterialExtensionSet;->extensions:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/matext/MaterialExtensionSet;->nameMappings:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public getMaterialExtension(Ljava/lang/String;)Lcom/jme3/scene/plugins/ogre/matext/MaterialExtension;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/matext/MaterialExtensionSet;->extensions:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/plugins/ogre/matext/MaterialExtension;

    return-object v0
.end method

.method public getNameMappings(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/matext/MaterialExtensionSet;->nameMappings:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
