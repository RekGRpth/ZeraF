.class public Lcom/jme3/scene/Mesh;
.super Ljava/lang/Object;
.source "Mesh.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/scene/Mesh$1;,
        Lcom/jme3/scene/Mesh$Mode;
    }
.end annotation


# instance fields
.field private buffers:Lcom/jme3/util/IntMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/IntMap",
            "<",
            "Lcom/jme3/scene/VertexBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private buffersList:Lcom/jme3/util/SafeArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/SafeArrayList",
            "<",
            "Lcom/jme3/scene/VertexBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private collisionTree:Lcom/jme3/scene/CollisionData;

.field private elementCount:I

.field private elementLengths:[I

.field private lineWidth:F

.field private lodLevels:[Lcom/jme3/scene/VertexBuffer;

.field private maxNumWeights:I

.field private meshBound:Lcom/jme3/bounding/BoundingVolume;

.field private mode:Lcom/jme3/scene/Mesh$Mode;

.field private modeStart:[I

.field private pointSize:F

.field private vertCount:I

.field private transient vertexArrayID:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/high16 v3, 0x3f800000

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/bounding/BoundingBox;

    invoke-direct {v0}, Lcom/jme3/bounding/BoundingBox;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->meshBound:Lcom/jme3/bounding/BoundingVolume;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    new-instance v0, Lcom/jme3/util/SafeArrayList;

    const-class v1, Lcom/jme3/scene/VertexBuffer;

    invoke-direct {v0, v1}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    new-instance v0, Lcom/jme3/util/IntMap;

    invoke-direct {v0}, Lcom/jme3/util/IntMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    iput v3, p0, Lcom/jme3/scene/Mesh;->pointSize:F

    iput v3, p0, Lcom/jme3/scene/Mesh;->lineWidth:F

    iput v2, p0, Lcom/jme3/scene/Mesh;->vertexArrayID:I

    iput v2, p0, Lcom/jme3/scene/Mesh;->vertCount:I

    iput v2, p0, Lcom/jme3/scene/Mesh;->elementCount:I

    iput v2, p0, Lcom/jme3/scene/Mesh;->maxNumWeights:I

    sget-object v0, Lcom/jme3/scene/Mesh$Mode;->Triangles:Lcom/jme3/scene/Mesh$Mode;

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->mode:Lcom/jme3/scene/Mesh$Mode;

    return-void
.end method

.method private computeNumElements(I)I
    .locals 2
    .param p1    # I

    sget-object v0, Lcom/jme3/scene/Mesh$1;->$SwitchMap$com$jme3$scene$Mesh$Mode:[I

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->mode:Lcom/jme3/scene/Mesh$Mode;

    invoke-virtual {v1}, Lcom/jme3/scene/Mesh$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :pswitch_0
    div-int/lit8 p1, p1, 0x3

    :goto_0
    :pswitch_1
    return p1

    :pswitch_2
    add-int/lit8 p1, p1, -0x2

    goto :goto_0

    :pswitch_3
    div-int/lit8 p1, p1, 0x2

    goto :goto_0

    :pswitch_4
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V
    .locals 3
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer$Type;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jme3/util/IntMap;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/VertexBuffer;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v1, v0}, Lcom/jme3/util/SafeArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/jme3/scene/Mesh;->updateCounts()V

    :cond_0
    return-void
.end method

.method public clone()Lcom/jme3/scene/Mesh;
    .locals 4

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Mesh;

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->meshBound:Lcom/jme3/bounding/BoundingVolume;

    invoke-virtual {v1}, Lcom/jme3/bounding/BoundingVolume;->clone()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v1

    iput-object v1, v0, Lcom/jme3/scene/Mesh;->meshBound:Lcom/jme3/bounding/BoundingVolume;

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    :goto_0
    iput-object v1, v0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    invoke-virtual {v1}, Lcom/jme3/util/IntMap;->clone()Lcom/jme3/util/IntMap;

    move-result-object v1

    iput-object v1, v0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    new-instance v1, Lcom/jme3/util/SafeArrayList;

    const-class v2, Lcom/jme3/scene/VertexBuffer;

    iget-object v3, p0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    invoke-direct {v1, v2, v3}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    const/4 v1, -0x1

    iput v1, v0, Lcom/jme3/scene/Mesh;->vertexArrayID:I

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->elementLengths:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->elementLengths:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, Lcom/jme3/scene/Mesh;->elementLengths:[I

    :cond_0
    iget-object v1, p0, Lcom/jme3/scene/Mesh;->modeStart:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->modeStart:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, Lcom/jme3/scene/Mesh;->modeStart:[I
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/scene/Mesh;->clone()Lcom/jme3/scene/Mesh;

    move-result-object v0

    return-object v0
.end method

.method public cloneForAnim()Lcom/jme3/scene/Mesh;
    .locals 8

    invoke-virtual {p0}, Lcom/jme3/scene/Mesh;->clone()Lcom/jme3/scene/Mesh;

    move-result-object v0

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->BindPosePosition:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v7}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v7

    if-eqz v7, :cond_0

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v7}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jme3/scene/VertexBuffer;->clone()Lcom/jme3/scene/VertexBuffer;

    move-result-object v2

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v0, v7}, Lcom/jme3/scene/Mesh;->clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V

    invoke-virtual {v0, v2}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->BindPoseNormal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v7}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v7

    if-eqz v7, :cond_0

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v7}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->clone()Lcom/jme3/scene/VertexBuffer;

    move-result-object v1

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v0, v7}, Lcom/jme3/scene/Mesh;->clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->BindPoseTangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v7}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v7

    if-eqz v7, :cond_0

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v7}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jme3/scene/VertexBuffer;->clone()Lcom/jme3/scene/VertexBuffer;

    move-result-object v3

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v0, v7}, Lcom/jme3/scene/Mesh;->clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V

    invoke-virtual {v0, v3}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    :cond_0
    return-object v0
.end method

.method public collideWith(Lcom/jme3/collision/Collidable;Lcom/jme3/math/Matrix4f;Lcom/jme3/bounding/BoundingVolume;Lcom/jme3/collision/CollisionResults;)I
    .locals 1
    .param p1    # Lcom/jme3/collision/Collidable;
    .param p2    # Lcom/jme3/math/Matrix4f;
    .param p3    # Lcom/jme3/bounding/BoundingVolume;
    .param p4    # Lcom/jme3/collision/CollisionResults;

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/jme3/scene/Mesh;->createCollisionData()V

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/jme3/scene/CollisionData;->collideWith(Lcom/jme3/collision/Collidable;Lcom/jme3/math/Matrix4f;Lcom/jme3/bounding/BoundingVolume;Lcom/jme3/collision/CollisionResults;)I

    move-result v0

    return v0
.end method

.method public createCollisionData()V
    .locals 1

    new-instance v0, Lcom/jme3/collision/bih/BIHTree;

    invoke-direct {v0, p0}, Lcom/jme3/collision/bih/BIHTree;-><init>(Lcom/jme3/scene/Mesh;)V

    invoke-virtual {v0}, Lcom/jme3/collision/bih/BIHTree;->construct()V

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    return-void
.end method

.method public extractVertexData(Lcom/jme3/scene/Mesh;)V
    .locals 24
    .param p1    # Lcom/jme3/scene/Mesh;

    sget-object v20, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/scene/Mesh;->getIndexBuffer()Lcom/jme3/scene/mesh/IndexBuffer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/jme3/scene/mesh/IndexBuffer;->size()I

    move-result v14

    new-instance v17, Lcom/jme3/util/IntMap;

    move-object/from16 v0, v17

    invoke-direct {v0, v14}, Lcom/jme3/util/IntMap;-><init>(I)V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v14, :cond_1

    invoke-virtual {v7, v5}, Lcom/jme3/scene/mesh/IndexBuffer;->get(I)I

    move-result v16

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/jme3/util/IntMap;->containsKey(I)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-eq v9, v12, :cond_2

    new-instance v20, Ljava/lang/AssertionError;

    invoke-direct/range {v20 .. v20}, Ljava/lang/AssertionError;-><init>()V

    throw v20

    :cond_2
    const/high16 v20, 0x10000

    move/from16 v0, v20

    if-lt v12, v0, :cond_3

    new-instance v10, Lcom/jme3/scene/mesh/IndexIntBuffer;

    invoke-static {v14}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Lcom/jme3/scene/mesh/IndexIntBuffer;-><init>(Ljava/nio/IntBuffer;)V

    :goto_1
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v14, :cond_4

    invoke-virtual {v7, v5}, Lcom/jme3/scene/mesh/IndexBuffer;->get(I)I

    move-result v16

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v10, v5, v9}, Lcom/jme3/scene/mesh/IndexBuffer;->put(II)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    new-instance v10, Lcom/jme3/scene/mesh/IndexShortBuffer;

    invoke-static {v14}, Lcom/jme3/util/BufferUtils;->createShortBuffer(I)Ljava/nio/ShortBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Lcom/jme3/scene/mesh/IndexShortBuffer;-><init>(Ljava/nio/ShortBuffer;)V

    goto :goto_1

    :cond_4
    new-instance v8, Lcom/jme3/scene/VertexBuffer;

    sget-object v20, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v20

    invoke-direct {v8, v0}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    invoke-virtual {v15}, Lcom/jme3/scene/VertexBuffer;->getUsage()Lcom/jme3/scene/VertexBuffer$Usage;

    move-result-object v21

    invoke-virtual {v15}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v22

    instance-of v0, v10, Lcom/jme3/scene/mesh/IndexIntBuffer;

    move/from16 v20, v0

    if-eqz v20, :cond_6

    sget-object v20, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedInt:Lcom/jme3/scene/VertexBuffer$Format;

    :goto_3
    invoke-virtual {v10}, Lcom/jme3/scene/mesh/IndexBuffer;->getBuffer()Ljava/nio/Buffer;

    move-result-object v23

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v20

    move-object/from16 v3, v23

    invoke-virtual {v8, v0, v1, v2, v3}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    sget-object v20, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/Mesh;->getBufferList()Lcom/jme3/util/SafeArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/jme3/util/SafeArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/jme3/scene/VertexBuffer;

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v20

    sget-object v21, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_5

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v20

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v21

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v0, v1, v12}, Lcom/jme3/scene/VertexBuffer;->createBuffer(Lcom/jme3/scene/VertexBuffer$Format;II)Ljava/nio/Buffer;

    move-result-object v4

    new-instance v13, Lcom/jme3/scene/VertexBuffer;

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v13, v0}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->isNormalized()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/jme3/scene/VertexBuffer;->setNormalized(Z)V

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->getUsage()Lcom/jme3/scene/VertexBuffer$Usage;

    move-result-object v20

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v21

    invoke-virtual/range {v18 .. v18}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v22

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v13, v0, v1, v2, v4}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    const/4 v5, 0x0

    :goto_5
    if-ge v5, v12, :cond_7

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v16

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1, v13, v5}, Lcom/jme3/scene/VertexBuffer;->copyElement(ILcom/jme3/scene/VertexBuffer;I)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_6
    sget-object v20, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedShort:Lcom/jme3/scene/VertexBuffer$Format;

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v13}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_4

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/Mesh;->getMaxNumWeights()I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->setMaxNumWeights(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/scene/Mesh;->updateCounts()V

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/scene/Mesh;->updateBound()V

    return-void
.end method

.method public generateBindPose(Z)V
    .locals 10
    .param p1    # Z

    const/4 v9, 0x3

    if-eqz p1, :cond_0

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v6}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v4

    if-eqz v4, :cond_0

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->BoneIndex:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v6}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/jme3/scene/VertexBuffer;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->BindPosePosition:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v1, v6}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Usage;->CpuOnly:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v8

    invoke-static {v8}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/Buffer;)Ljava/nio/Buffer;

    move-result-object v8

    invoke-virtual {v1, v6, v9, v7, v8}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    invoke-virtual {p0, v1}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Usage;->Stream:Lcom/jme3/scene/VertexBuffer$Usage;

    invoke-virtual {v4, v6}, Lcom/jme3/scene/VertexBuffer;->setUsage(Lcom/jme3/scene/VertexBuffer$Usage;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v6}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v3

    if-eqz v3, :cond_2

    new-instance v0, Lcom/jme3/scene/VertexBuffer;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->BindPoseNormal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v0, v6}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Usage;->CpuOnly:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v7, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v8

    invoke-static {v8}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/Buffer;)Ljava/nio/Buffer;

    move-result-object v8

    invoke-virtual {v0, v6, v9, v7, v8}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    invoke-virtual {p0, v0}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Usage;->Stream:Lcom/jme3/scene/VertexBuffer$Usage;

    invoke-virtual {v3, v6}, Lcom/jme3/scene/VertexBuffer;->setUsage(Lcom/jme3/scene/VertexBuffer$Usage;)V

    :cond_2
    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v6}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v5

    if-eqz v5, :cond_0

    new-instance v2, Lcom/jme3/scene/VertexBuffer;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->BindPoseTangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v2, v6}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Usage;->CpuOnly:Lcom/jme3/scene/VertexBuffer$Usage;

    const/4 v7, 0x4

    sget-object v8, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    invoke-virtual {v5}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v9

    invoke-static {v9}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/Buffer;)Ljava/nio/Buffer;

    move-result-object v9

    invoke-virtual {v2, v6, v7, v8, v9}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    invoke-virtual {p0, v2}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Usage;->Stream:Lcom/jme3/scene/VertexBuffer$Usage;

    invoke-virtual {v5, v6}, Lcom/jme3/scene/VertexBuffer;->setUsage(Lcom/jme3/scene/VertexBuffer$Usage;)V

    goto :goto_0
.end method

.method public getBound()Lcom/jme3/bounding/BoundingVolume;
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->meshBound:Lcom/jme3/bounding/BoundingVolume;

    return-object v0
.end method

.method public getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;
    .locals 2
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer$Type;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/VertexBuffer;

    return-object v0
.end method

.method public getBufferList()Lcom/jme3/util/SafeArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jme3/util/SafeArrayList",
            "<",
            "Lcom/jme3/scene/VertexBuffer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    return-object v0
.end method

.method public getElementLengths()[I
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->elementLengths:[I

    return-object v0
.end method

.method public getFloatBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Ljava/nio/FloatBuffer;
    .locals 2
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, p1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v1

    check-cast v1, Ljava/nio/FloatBuffer;

    goto :goto_0
.end method

.method public getIndexBuffer()Lcom/jme3/scene/mesh/IndexBuffer;
    .locals 5

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v2}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    instance-of v2, v0, Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    new-instance v2, Lcom/jme3/scene/mesh/IndexByteBuffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-direct {v2, v0}, Lcom/jme3/scene/mesh/IndexByteBuffer;-><init>(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_1
    instance-of v2, v0, Ljava/nio/ShortBuffer;

    if-eqz v2, :cond_2

    new-instance v2, Lcom/jme3/scene/mesh/IndexShortBuffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    invoke-direct {v2, v0}, Lcom/jme3/scene/mesh/IndexShortBuffer;-><init>(Ljava/nio/ShortBuffer;)V

    goto :goto_0

    :cond_2
    instance-of v2, v0, Ljava/nio/IntBuffer;

    if-eqz v2, :cond_3

    new-instance v2, Lcom/jme3/scene/mesh/IndexIntBuffer;

    check-cast v0, Ljava/nio/IntBuffer;

    invoke-direct {v2, v0}, Lcom/jme3/scene/mesh/IndexIntBuffer;-><init>(Ljava/nio/IntBuffer;)V

    goto :goto_0

    :cond_3
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Index buffer type unsupported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getLineWidth()F
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Mesh;->lineWidth:F

    return v0
.end method

.method public getLodLevel(I)Lcom/jme3/scene/VertexBuffer;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getMaxNumWeights()I
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Mesh;->maxNumWeights:I

    return v0
.end method

.method public getMode()Lcom/jme3/scene/Mesh$Mode;
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->mode:Lcom/jme3/scene/Mesh$Mode;

    return-object v0
.end method

.method public getModeStart()[I
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->modeStart:[I

    return-object v0
.end method

.method public getNumLodLevels()I
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPointSize()F
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Mesh;->pointSize:F

    return v0
.end method

.method public getTriangleCount()I
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Mesh;->elementCount:I

    return v0
.end method

.method public getTriangleCount(I)I
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    if-eqz v0, :cond_2

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LOD level cannot be < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LOD level "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/Buffer;->capacity()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jme3/scene/Mesh;->computeNumElements(I)I

    move-result v0

    :goto_0
    return v0

    :cond_2
    if-nez p1, :cond_3

    iget v0, p0, Lcom/jme3/scene/Mesh;->elementCount:I

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "There are no LOD levels on the mesh!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVertexCount()I
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Mesh;->vertCount:I

    return v0
.end method

.method public prepareForAnim(Z)V
    .locals 7
    .param p1    # Z

    if-eqz p1, :cond_0

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->BoneIndex:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v6}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v3

    check-cast v3, Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->BoneWeight:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v6}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v4

    check-cast v4, Ljava/nio/FloatBuffer;

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v6

    invoke-static {v6}, Ljava/nio/FloatBuffer;->allocate(I)Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v1, v4}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    invoke-virtual {v5, v1}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    :cond_0
    return-void
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v2, -0x1

    const/4 v4, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v1

    const-string v0, "modelBound"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    check-cast v0, Lcom/jme3/bounding/BoundingVolume;

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->meshBound:Lcom/jme3/bounding/BoundingVolume;

    const-string v0, "vertCount"

    invoke-interface {v1, v0, v2}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/jme3/scene/Mesh;->vertCount:I

    const-string v0, "elementCount"

    invoke-interface {v1, v0, v2}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/jme3/scene/Mesh;->elementCount:I

    const-string v0, "max_num_weights"

    invoke-interface {v1, v0, v2}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/jme3/scene/Mesh;->maxNumWeights:I

    const-string v0, "mode"

    const-class v2, Lcom/jme3/scene/Mesh$Mode;

    sget-object v3, Lcom/jme3/scene/Mesh$Mode;->Triangles:Lcom/jme3/scene/Mesh$Mode;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Mesh$Mode;

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->mode:Lcom/jme3/scene/Mesh$Mode;

    const-string v0, "elementLengths"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readIntArray(Ljava/lang/String;[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->elementLengths:[I

    const-string v0, "modeStart"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readIntArray(Ljava/lang/String;[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->modeStart:[I

    const-string v0, "collisionTree"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    check-cast v0, Lcom/jme3/collision/bih/BIHTree;

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->collisionTree:Lcom/jme3/scene/CollisionData;

    const-string v0, "elementLengths"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readIntArray(Ljava/lang/String;[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->elementLengths:[I

    const-string v0, "modeStart"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readIntArray(Ljava/lang/String;[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->modeStart:[I

    const-string v0, "pointSize"

    const/high16 v2, 0x3f800000

    invoke-interface {v1, v0, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/jme3/scene/Mesh;->pointSize:F

    const-string v0, "buffers"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readIntSavableMap(Ljava/lang/String;Lcom/jme3/util/IntMap;)Lcom/jme3/util/IntMap;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    invoke-virtual {v0}, Lcom/jme3/util/IntMap;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/util/IntMap$Entry;

    iget-object v3, p0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0}, Lcom/jme3/util/IntMap$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/jme3/util/SafeArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v0, "lodLevels"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    new-array v1, v1, [Lcom/jme3/scene/VertexBuffer;

    iput-object v1, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    iget-object v2, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    array-length v2, v2

    invoke-static {v0, v5, v1, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    return-void
.end method

.method public setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V
    .locals 3
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;
    .param p2    # I
    .param p3    # Lcom/jme3/scene/VertexBuffer$Format;
    .param p4    # Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer$Type;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/VertexBuffer;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jme3/scene/VertexBuffer;

    invoke-direct {v0, p1}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Usage;->Dynamic:Lcom/jme3/scene/VertexBuffer$Usage;

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    invoke-virtual {p0, v0}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v1

    if-ne v1, p2, :cond_1

    invoke-virtual {v0}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v1

    if-eq v1, p3, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "The buffer already set is incompatible with the given parameters"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-virtual {v0, p4}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    invoke-virtual {p0}, Lcom/jme3/scene/Mesh;->updateCounts()V

    goto :goto_0
.end method

.method public setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V
    .locals 1
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;
    .param p2    # I
    .param p3    # Ljava/nio/FloatBuffer;

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    return-void
.end method

.method public setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/IntBuffer;)V
    .locals 1
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;
    .param p2    # I
    .param p3    # Ljava/nio/IntBuffer;

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedInt:Lcom/jme3/scene/VertexBuffer$Format;

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    return-void
.end method

.method public setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/ShortBuffer;)V
    .locals 1
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;
    .param p2    # I
    .param p3    # Ljava/nio/ShortBuffer;

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedShort:Lcom/jme3/scene/VertexBuffer$Format;

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    return-void
.end method

.method public setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[F)V
    .locals 1
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;
    .param p2    # I
    .param p3    # [F

    invoke-static {p3}, Lcom/jme3/util/BufferUtils;->createFloatBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V

    return-void
.end method

.method public setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[S)V
    .locals 1
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;
    .param p2    # I
    .param p3    # [S

    invoke-static {p3}, Lcom/jme3/util/BufferUtils;->createShortBuffer([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/ShortBuffer;)V

    return-void
.end method

.method public setBuffer(Lcom/jme3/scene/VertexBuffer;)V
    .locals 3
    .param p1    # Lcom/jme3/scene/VertexBuffer;

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer$Type;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/util/IntMap;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Buffer type already set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/Mesh;->buffers:Lcom/jme3/util/IntMap;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer$Type;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0, p1}, Lcom/jme3/util/SafeArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/jme3/scene/Mesh;->updateCounts()V

    return-void
.end method

.method public setLodLevels([Lcom/jme3/scene/VertexBuffer;)V
    .locals 0
    .param p1    # [Lcom/jme3/scene/VertexBuffer;

    iput-object p1, p0, Lcom/jme3/scene/Mesh;->lodLevels:[Lcom/jme3/scene/VertexBuffer;

    return-void
.end method

.method public setMaxNumWeights(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jme3/scene/Mesh;->maxNumWeights:I

    return-void
.end method

.method public setMode(Lcom/jme3/scene/Mesh$Mode;)V
    .locals 0
    .param p1    # Lcom/jme3/scene/Mesh$Mode;

    iput-object p1, p0, Lcom/jme3/scene/Mesh;->mode:Lcom/jme3/scene/Mesh$Mode;

    invoke-virtual {p0}, Lcom/jme3/scene/Mesh;->updateCounts()V

    return-void
.end method

.method public setStatic()V
    .locals 5

    iget-object v4, p0, Lcom/jme3/scene/Mesh;->buffersList:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v4}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/VertexBuffer;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    sget-object v4, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    invoke-virtual {v3, v4}, Lcom/jme3/scene/VertexBuffer;->setUsage(Lcom/jme3/scene/VertexBuffer$Usage;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public updateBound()V
    .locals 3

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/scene/Mesh;->meshBound:Lcom/jme3/bounding/BoundingVolume;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/jme3/scene/Mesh;->meshBound:Lcom/jme3/bounding/BoundingVolume;

    invoke-virtual {v0}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v1

    check-cast v1, Ljava/nio/FloatBuffer;

    invoke-virtual {v2, v1}, Lcom/jme3/bounding/BoundingVolume;->computeFromPoints(Ljava/nio/FloatBuffer;)V

    :cond_0
    return-void
.end method

.method public updateCounts()V
    .locals 4

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->InterleavedData:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v2}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Should update counts before interleave"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v2}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v1

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v2}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/Buffer;->capacity()I

    move-result v2

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v3

    div-int/2addr v2, v3

    iput v2, p0, Lcom/jme3/scene/Mesh;->vertCount:I

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/Buffer;->capacity()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/jme3/scene/Mesh;->computeNumElements(I)I

    move-result v2

    iput v2, p0, Lcom/jme3/scene/Mesh;->elementCount:I

    :goto_0
    return-void

    :cond_2
    iget v2, p0, Lcom/jme3/scene/Mesh;->vertCount:I

    invoke-direct {p0, v2}, Lcom/jme3/scene/Mesh;->computeNumElements(I)I

    move-result v2

    iput v2, p0, Lcom/jme3/scene/Mesh;->elementCount:I

    goto :goto_0
.end method
