.class public final Lcom/jme3/math/Matrix3f;
.super Ljava/lang/Object;
.source "Matrix3f.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final IDENTITY:Lcom/jme3/math/Matrix3f;

.field public static final ZERO:Lcom/jme3/math/Matrix3f;

.field private static final logger:Ljava/util/logging/Logger;

.field static final serialVersionUID:J = 0x1L


# instance fields
.field protected m00:F

.field protected m01:F

.field protected m02:F

.field protected m10:F

.field protected m11:F

.field protected m12:F

.field protected m20:F

.field protected m21:F

.field protected m22:F


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v1, 0x0

    const-class v0, Lcom/jme3/math/Matrix3f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/math/Matrix3f;->logger:Ljava/util/logging/Logger;

    new-instance v0, Lcom/jme3/math/Matrix3f;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    invoke-direct/range {v0 .. v9}, Lcom/jme3/math/Matrix3f;-><init>(FFFFFFFFF)V

    sput-object v0, Lcom/jme3/math/Matrix3f;->ZERO:Lcom/jme3/math/Matrix3f;

    new-instance v0, Lcom/jme3/math/Matrix3f;

    invoke-direct {v0}, Lcom/jme3/math/Matrix3f;-><init>()V

    sput-object v0, Lcom/jme3/math/Matrix3f;->IDENTITY:Lcom/jme3/math/Matrix3f;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lcom/jme3/math/Matrix3f;->loadIdentity()V

    return-void
.end method

.method public constructor <init>(FFFFFFFFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # F
    .param p8    # F
    .param p9    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iput p2, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iput p3, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iput p4, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iput p5, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iput p6, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iput p7, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iput p8, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iput p9, p0, Lcom/jme3/math/Matrix3f;->m22:F

    return-void
.end method


# virtual methods
.method public absoluteLocal()V
    .locals 1

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m12:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m21:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m22:F

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m22:F

    return-void
.end method

.method public clone()Lcom/jme3/math/Matrix3f;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Matrix3f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/math/Matrix3f;->clone()Lcom/jme3/math/Matrix3f;

    move-result-object v0

    return-object v0
.end method

.method public determinant()F
    .locals 7

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v6, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v5, v6

    sub-float v0, v4, v5

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v6, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v5, v6

    sub-float v1, v4, v5

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iget v6, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v5, v6

    sub-float v2, v4, v5

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m00:F

    mul-float/2addr v4, v0

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m01:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix3f;->m02:F

    mul-float/2addr v5, v2

    add-float v3, v4, v5

    return v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    instance-of v3, p1, Lcom/jme3/math/Matrix3f;

    if-eqz v3, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    if-eq p0, p1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/jme3/math/Matrix3f;

    iget v3, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m00:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m01:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m02:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m10:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_6

    move v1, v2

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m11:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    :cond_7
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m12:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_8

    move v1, v2

    goto :goto_0

    :cond_8
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m20:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_9

    move v1, v2

    goto :goto_0

    :cond_9
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m21:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_a

    move v1, v2

    goto :goto_0

    :cond_a
    iget v3, p0, Lcom/jme3/math/Matrix3f;->m22:F

    iget v4, v0, Lcom/jme3/math/Matrix3f;->m22:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method public fillFloatArray([FZ)V
    .locals 6
    .param p1    # [F
    .param p2    # Z

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    aput v0, p1, v1

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    aput v0, p1, v2

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    aput v0, p1, v3

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    aput v0, p1, v4

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    aput v0, p1, v5

    const/4 v0, 0x5

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m21:F

    aput v1, p1, v0

    const/4 v0, 0x6

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m02:F

    aput v1, p1, v0

    const/4 v0, 0x7

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m12:F

    aput v1, p1, v0

    const/16 v0, 0x8

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m22:F

    aput v1, p1, v0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    aput v0, p1, v1

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    aput v0, p1, v2

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    aput v0, p1, v3

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    aput v0, p1, v4

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    aput v0, p1, v5

    const/4 v0, 0x5

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m12:F

    aput v1, p1, v0

    const/4 v0, 0x6

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m20:F

    aput v1, p1, v0

    const/4 v0, 0x7

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m21:F

    aput v1, p1, v0

    const/16 v0, 0x8

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m22:F

    aput v1, p1, v0

    goto :goto_0
.end method

.method public fillFloatBuffer(Ljava/nio/FloatBuffer;Z)Ljava/nio/FloatBuffer;
    .locals 4
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # Z

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v0

    iget-object v1, v0, Lcom/jme3/util/TempVars;->matrixWrite:[F

    invoke-virtual {p0, v1, p2}, Lcom/jme3/math/Matrix3f;->fillFloatArray([FZ)V

    iget-object v1, v0, Lcom/jme3/util/TempVars;->matrixWrite:[F

    const/4 v2, 0x0

    const/16 v3, 0x9

    invoke-virtual {p1, v1, v2, v3}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Lcom/jme3/util/TempVars;->release()V

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x25

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m00:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/lit16 v0, v1, 0x559

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m01:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m02:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m10:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m11:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m12:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m20:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m21:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix3f;->m22:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method

.method public invertLocal()Lcom/jme3/math/Matrix3f;
    .locals 13

    invoke-virtual {p0}, Lcom/jme3/math/Matrix3f;->determinant()F

    move-result v0

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v10

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gtz v10, :cond_0

    invoke-virtual {p0}, Lcom/jme3/math/Matrix3f;->zero()Lcom/jme3/math/Matrix3f;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    iget v10, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v11, v12

    sub-float v1, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v11, v12

    sub-float v2, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m11:F

    mul-float/2addr v11, v12

    sub-float v3, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v11, v12

    sub-float v4, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v11, v12

    sub-float v5, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m12:F

    mul-float/2addr v11, v12

    sub-float v6, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v11, v12

    sub-float v7, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v11, v12

    sub-float v8, v10, v11

    iget v10, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v12, p0, Lcom/jme3/math/Matrix3f;->m10:F

    mul-float/2addr v11, v12

    sub-float v9, v10, v11

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iput v2, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iput v3, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iput v4, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iput v5, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iput v6, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iput v7, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iput v8, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iput v9, p0, Lcom/jme3/math/Matrix3f;->m22:F

    const/high16 v10, 0x3f800000

    div-float/2addr v10, v0

    invoke-virtual {p0, v10}, Lcom/jme3/math/Matrix3f;->multLocal(F)Lcom/jme3/math/Matrix3f;

    goto/16 :goto_0
.end method

.method public loadIdentity()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m22:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    return-void
.end method

.method public mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 5
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Vector3f;

    invoke-direct {p2}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Matrix3f;->m00:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m01:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m02:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p0, Lcom/jme3/math/Matrix3f;->m10:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m11:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m12:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->z:F

    return-object p2
.end method

.method public multLocal(F)Lcom/jme3/math/Matrix3f;
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m12:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m22:F

    return-object p0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 4
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "m00"

    invoke-interface {v0, v1, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m00:F

    const-string v1, "m01"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m01:F

    const-string v1, "m02"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m02:F

    const-string v1, "m10"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m10:F

    const-string v1, "m11"

    invoke-interface {v0, v1, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m11:F

    const-string v1, "m12"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m12:F

    const-string v1, "m20"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m20:F

    const-string v1, "m21"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m21:F

    const-string v1, "m22"

    invoke-interface {v0, v1, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m22:F

    return-void
.end method

.method public set(Lcom/jme3/math/Quaternion;)Lcom/jme3/math/Matrix3f;
    .locals 1
    .param p1    # Lcom/jme3/math/Quaternion;

    invoke-virtual {p1, p0}, Lcom/jme3/math/Quaternion;->toRotationMatrix(Lcom/jme3/math/Matrix3f;)Lcom/jme3/math/Matrix3f;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Matrix3f\n[\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m00:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m01:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m02:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m10:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m11:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m12:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m20:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m21:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m22:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " \n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public transposeLocal()Lcom/jme3/math/Matrix3f;
    .locals 2

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iget v0, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iget v1, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iput v1, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m21:F

    return-object p0
.end method

.method public zero()Lcom/jme3/math/Matrix3f;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m22:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m21:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m20:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m12:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m11:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m10:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m02:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m01:F

    iput v0, p0, Lcom/jme3/math/Matrix3f;->m00:F

    return-object p0
.end method
