.class public final Lcom/jme3/math/Matrix4f;
.super Ljava/lang/Object;
.source "Matrix4f.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final IDENTITY:Lcom/jme3/math/Matrix4f;

.field public static final ZERO:Lcom/jme3/math/Matrix4f;

.field private static final logger:Ljava/util/logging/Logger;

.field static final serialVersionUID:J = 0x1L


# instance fields
.field public m00:F

.field public m01:F

.field public m02:F

.field public m03:F

.field public m10:F

.field public m11:F

.field public m12:F

.field public m13:F

.field public m20:F

.field public m21:F

.field public m22:F

.field public m23:F

.field public m30:F

.field public m31:F

.field public m32:F

.field public m33:F


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const-class v0, Lcom/jme3/math/Matrix4f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/math/Matrix4f;->logger:Ljava/util/logging/Logger;

    new-instance v0, Lcom/jme3/math/Matrix4f;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v0 .. v16}, Lcom/jme3/math/Matrix4f;-><init>(FFFFFFFFFFFFFFFF)V

    sput-object v0, Lcom/jme3/math/Matrix4f;->ZERO:Lcom/jme3/math/Matrix4f;

    new-instance v0, Lcom/jme3/math/Matrix4f;

    invoke-direct {v0}, Lcom/jme3/math/Matrix4f;-><init>()V

    sput-object v0, Lcom/jme3/math/Matrix4f;->IDENTITY:Lcom/jme3/math/Matrix4f;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lcom/jme3/math/Matrix4f;->loadIdentity()V

    return-void
.end method

.method public constructor <init>(FFFFFFFFFFFFFFFF)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # F
    .param p8    # F
    .param p9    # F
    .param p10    # F
    .param p11    # F
    .param p12    # F
    .param p13    # F
    .param p14    # F
    .param p15    # F
    .param p16    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iput p2, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iput p3, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iput p4, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iput p5, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iput p6, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iput p7, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iput p8, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iput p9, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iput p10, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iput p11, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iput p12, p0, Lcom/jme3/math/Matrix4f;->m23:F

    iput p13, p0, Lcom/jme3/math/Matrix4f;->m30:F

    iput p14, p0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v0, p15

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v0, p16

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    return-void
.end method


# virtual methods
.method public clone()Lcom/jme3/math/Matrix4f;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Matrix4f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/math/Matrix4f;->clone()Lcom/jme3/math/Matrix4f;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    instance-of v3, p1, Lcom/jme3/math/Matrix4f;

    if-eqz v3, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    if-eq p0, p1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/jme3/math/Matrix4f;

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m00:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m01:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m02:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m03:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_6

    move v1, v2

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m10:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    :cond_7
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m11:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_8

    move v1, v2

    goto :goto_0

    :cond_8
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m12:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_9

    move v1, v2

    goto :goto_0

    :cond_9
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m13:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_a

    move v1, v2

    goto :goto_0

    :cond_a
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m20:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_b

    move v1, v2

    goto :goto_0

    :cond_b
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m21:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_c

    move v1, v2

    goto :goto_0

    :cond_c
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m22:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_d

    move v1, v2

    goto/16 :goto_0

    :cond_d
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m23:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m23:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_e

    move v1, v2

    goto/16 :goto_0

    :cond_e
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m30:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m30:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_f

    move v1, v2

    goto/16 :goto_0

    :cond_f
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m31:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m31:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_10

    move v1, v2

    goto/16 :goto_0

    :cond_10
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m32:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m32:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_11

    move v1, v2

    goto/16 :goto_0

    :cond_11
    iget v3, p0, Lcom/jme3/math/Matrix4f;->m33:F

    iget v4, v0, Lcom/jme3/math/Matrix4f;->m33:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    goto/16 :goto_0
.end method

.method public fillFloatArray([FZ)V
    .locals 6
    .param p1    # [F
    .param p2    # Z

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    aput v0, p1, v1

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    aput v0, p1, v2

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    aput v0, p1, v3

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    aput v0, p1, v4

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    aput v0, p1, v5

    const/4 v0, 0x5

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m11:F

    aput v1, p1, v0

    const/4 v0, 0x6

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m21:F

    aput v1, p1, v0

    const/4 v0, 0x7

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m31:F

    aput v1, p1, v0

    const/16 v0, 0x8

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m02:F

    aput v1, p1, v0

    const/16 v0, 0x9

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m12:F

    aput v1, p1, v0

    const/16 v0, 0xa

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m22:F

    aput v1, p1, v0

    const/16 v0, 0xb

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m32:F

    aput v1, p1, v0

    const/16 v0, 0xc

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m03:F

    aput v1, p1, v0

    const/16 v0, 0xd

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m13:F

    aput v1, p1, v0

    const/16 v0, 0xe

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m23:F

    aput v1, p1, v0

    const/16 v0, 0xf

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m33:F

    aput v1, p1, v0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    aput v0, p1, v1

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    aput v0, p1, v2

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    aput v0, p1, v3

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    aput v0, p1, v4

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    aput v0, p1, v5

    const/4 v0, 0x5

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m11:F

    aput v1, p1, v0

    const/4 v0, 0x6

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m12:F

    aput v1, p1, v0

    const/4 v0, 0x7

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m13:F

    aput v1, p1, v0

    const/16 v0, 0x8

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m20:F

    aput v1, p1, v0

    const/16 v0, 0x9

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m21:F

    aput v1, p1, v0

    const/16 v0, 0xa

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m22:F

    aput v1, p1, v0

    const/16 v0, 0xb

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m23:F

    aput v1, p1, v0

    const/16 v0, 0xc

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m30:F

    aput v1, p1, v0

    const/16 v0, 0xd

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m31:F

    aput v1, p1, v0

    const/16 v0, 0xe

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m32:F

    aput v1, p1, v0

    const/16 v0, 0xf

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m33:F

    aput v1, p1, v0

    goto :goto_0
.end method

.method public fillFloatBuffer(Ljava/nio/FloatBuffer;Z)Ljava/nio/FloatBuffer;
    .locals 4
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # Z

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v0

    iget-object v1, v0, Lcom/jme3/util/TempVars;->matrixWrite:[F

    invoke-virtual {p0, v1, p2}, Lcom/jme3/math/Matrix4f;->fillFloatArray([FZ)V

    iget-object v1, v0, Lcom/jme3/util/TempVars;->matrixWrite:[F

    const/4 v2, 0x0

    const/16 v3, 0x10

    invoke-virtual {p1, v1, v2, v3}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Lcom/jme3/util/TempVars;->release()V

    return-object p1
.end method

.method public fromFrame(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 6
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;
    .param p4    # Lcom/jme3/math/Vector3f;

    invoke-virtual {p0}, Lcom/jme3/math/Matrix4f;->loadIdentity()V

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v4

    iget-object v5, v4, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {v5, p2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    iget-object v5, v4, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    invoke-virtual {v5, v0}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/jme3/math/Vector3f;->crossLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v1

    iget-object v5, v4, Lcom/jme3/util/TempVars;->vect3:Lcom/jme3/math/Vector3f;

    invoke-virtual {v5, v1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/jme3/math/Vector3f;->crossLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v3

    iget v5, v1, Lcom/jme3/math/Vector3f;->x:F

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iget v5, v1, Lcom/jme3/math/Vector3f;->y:F

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iget v5, v1, Lcom/jme3/math/Vector3f;->z:F

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iget v5, v3, Lcom/jme3/math/Vector3f;->x:F

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iget v5, v3, Lcom/jme3/math/Vector3f;->y:F

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iget v5, v3, Lcom/jme3/math/Vector3f;->z:F

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iget v5, v0, Lcom/jme3/math/Vector3f;->x:F

    neg-float v5, v5

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iget v5, v0, Lcom/jme3/math/Vector3f;->y:F

    neg-float v5, v5

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iget v5, v0, Lcom/jme3/math/Vector3f;->z:F

    neg-float v5, v5

    iput v5, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iget-object v2, v4, Lcom/jme3/util/TempVars;->tempMat4:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v2}, Lcom/jme3/math/Matrix4f;->loadIdentity()V

    iget v5, p1, Lcom/jme3/math/Vector3f;->x:F

    neg-float v5, v5

    iput v5, v2, Lcom/jme3/math/Matrix4f;->m03:F

    iget v5, p1, Lcom/jme3/math/Vector3f;->y:F

    neg-float v5, v5

    iput v5, v2, Lcom/jme3/math/Matrix4f;->m13:F

    iget v5, p1, Lcom/jme3/math/Vector3f;->z:F

    neg-float v5, v5

    iput v5, v2, Lcom/jme3/math/Matrix4f;->m23:F

    invoke-virtual {p0, v2}, Lcom/jme3/math/Matrix4f;->multLocal(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    invoke-virtual {v4}, Lcom/jme3/util/TempVars;->release()V

    return-void
.end method

.method public fromFrustum(FFFFFFZ)V
    .locals 3
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # Z

    const/high16 v2, 0x40000000

    invoke-virtual {p0}, Lcom/jme3/math/Matrix4f;->loadIdentity()V

    if-eqz p7, :cond_0

    sub-float v0, p4, p3

    div-float v0, v2, v0

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    sub-float v0, p5, p6

    div-float v0, v2, v0

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    const/high16 v0, -0x40000000

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    add-float v0, p4, p3

    neg-float v0, v0

    sub-float v1, p4, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    add-float v0, p5, p6

    neg-float v0, v0

    sub-float v1, p5, p6

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    add-float v0, p2, p1

    neg-float v0, v0

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    :goto_0
    return-void

    :cond_0
    mul-float v0, v2, p1

    sub-float v1, p4, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float v0, v2, p1

    sub-float v1, p5, p6

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    add-float v0, p4, p3

    sub-float v1, p4, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    add-float v0, p5, p6

    sub-float v1, p5, p6

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    add-float v0, p2, p1

    neg-float v0, v0

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    mul-float v0, v2, p2

    mul-float/2addr v0, p1

    neg-float v0, v0

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x25

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m00:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/lit16 v0, v1, 0x559

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m01:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m02:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m03:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m10:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m11:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m12:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m13:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m20:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m21:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m22:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m23:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m30:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m31:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m32:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Matrix4f;->m33:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method

.method public invert()Lcom/jme3/math/Matrix4f;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jme3/math/Matrix4f;->invert(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    move-result-object v0

    return-object v0
.end method

.method public invert(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;
    .locals 18
    .param p1    # Lcom/jme3/math/Matrix4f;

    if-nez p1, :cond_0

    new-instance p1, Lcom/jme3/math/Matrix4f;

    invoke-direct/range {p1 .. p1}, Lcom/jme3/math/Matrix4f;-><init>()V

    :cond_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v1, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v2, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v3, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v4, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v5, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v6, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v7, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v8, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v9, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v10, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v11, v15, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v12, v15, v16

    mul-float v15, v1, v12

    mul-float v16, v2, v11

    sub-float v15, v15, v16

    mul-float v16, v3, v10

    add-float v15, v15, v16

    mul-float v16, v4, v9

    add-float v15, v15, v16

    mul-float v16, v5, v8

    sub-float v15, v15, v16

    mul-float v16, v6, v7

    add-float v13, v15, v16

    invoke-static {v13}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v15

    const/16 v16, 0x0

    cmpg-float v15, v15, v16

    if-gtz v15, :cond_1

    new-instance v15, Ljava/lang/ArithmeticException;

    const-string v16, "This matrix cannot be inverted"

    invoke-direct/range {v15 .. v16}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m11:F

    mul-float/2addr v15, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v16, v0

    mul-float v16, v16, v11

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v16, v0

    mul-float v16, v16, v10

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m10:F

    neg-float v15, v15

    mul-float/2addr v15, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v16, v0

    mul-float v16, v16, v9

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v16, v0

    mul-float v16, v16, v8

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m10:F

    mul-float/2addr v15, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v16, v0

    mul-float v16, v16, v9

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v16, v0

    mul-float v16, v16, v7

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m10:F

    neg-float v15, v15

    mul-float/2addr v15, v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v16, v0

    mul-float v16, v16, v8

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v16, v0

    mul-float v16, v16, v7

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m01:F

    neg-float v15, v15

    mul-float/2addr v15, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v16, v0

    mul-float v16, v16, v11

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v16, v0

    mul-float v16, v16, v10

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v15, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v16, v0

    mul-float v16, v16, v9

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v16, v0

    mul-float v16, v16, v8

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    neg-float v15, v15

    mul-float/2addr v15, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v16, v0

    mul-float v16, v16, v9

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v16, v0

    mul-float v16, v16, v7

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v15, v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v16, v0

    mul-float v16, v16, v8

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v16, v0

    mul-float v16, v16, v7

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m31:F

    mul-float/2addr v15, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v16, v0

    mul-float v16, v16, v5

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v16, v0

    mul-float v16, v16, v4

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m30:F

    neg-float v15, v15

    mul-float/2addr v15, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v16, v0

    mul-float v16, v16, v3

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v16, v0

    mul-float v16, v16, v2

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m30:F

    mul-float/2addr v15, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v16, v0

    mul-float v16, v16, v3

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v16, v0

    mul-float v16, v16, v1

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m30:F

    neg-float v15, v15

    mul-float/2addr v15, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v16, v0

    mul-float v16, v16, v2

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v16, v0

    mul-float v16, v16, v1

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m21:F

    neg-float v15, v15

    mul-float/2addr v15, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v16, v0

    mul-float v16, v16, v5

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v16, v0

    mul-float v16, v16, v4

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    mul-float/2addr v15, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v16, v0

    mul-float v16, v16, v3

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v16, v0

    mul-float v16, v16, v2

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    neg-float v15, v15

    mul-float/2addr v15, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v16, v0

    mul-float v16, v16, v3

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v16, v0

    mul-float v16, v16, v1

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    mul-float/2addr v15, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v16, v0

    mul-float v16, v16, v2

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v16, v0

    mul-float v16, v16, v1

    add-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m33:F

    const/high16 v15, 0x3f800000

    div-float v14, v15, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/jme3/math/Matrix4f;->multLocal(F)V

    return-object p1
.end method

.method public invertLocal()Lcom/jme3/math/Matrix4f;
    .locals 35

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v18, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v19, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v20, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v21, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v22, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v23, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v24, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v25, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v26, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v27, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v28, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    sub-float v29, v32, v33

    mul-float v32, v18, v29

    mul-float v33, v19, v28

    sub-float v32, v32, v33

    mul-float v33, v20, v27

    add-float v32, v32, v33

    mul-float v33, v21, v26

    add-float v32, v32, v33

    mul-float v33, v22, v25

    sub-float v32, v32, v33

    mul-float v33, v23, v24

    add-float v30, v32, v33

    invoke-static/range {v30 .. v30}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v32

    const/16 v33, 0x0

    cmpg-float v32, v32, v33

    if-gtz v32, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/math/Matrix4f;->zero()Lcom/jme3/math/Matrix4f;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v32, v0

    mul-float v32, v32, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v33, v0

    mul-float v33, v33, v28

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v33, v0

    mul-float v33, v33, v27

    add-float v2, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v33, v0

    mul-float v33, v33, v26

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v33, v0

    mul-float v33, v33, v25

    sub-float v6, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v32, v0

    mul-float v32, v32, v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v33, v0

    mul-float v33, v33, v26

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v33, v0

    mul-float v33, v33, v24

    add-float v10, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v33, v0

    mul-float v33, v33, v25

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v33, v0

    mul-float v33, v33, v24

    sub-float v14, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v33, v0

    mul-float v33, v33, v28

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v33, v0

    mul-float v33, v33, v27

    sub-float v3, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v32, v0

    mul-float v32, v32, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v33, v0

    mul-float v33, v33, v26

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v33, v0

    mul-float v33, v33, v25

    add-float v7, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v33, v0

    mul-float v33, v33, v26

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v33, v0

    mul-float v33, v33, v24

    sub-float v11, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v32, v0

    mul-float v32, v32, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v33, v0

    mul-float v33, v33, v25

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v33, v0

    mul-float v33, v33, v24

    add-float v15, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v32, v0

    mul-float v32, v32, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v33, v0

    mul-float v33, v33, v22

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v33, v0

    mul-float v33, v33, v21

    add-float v4, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v33, v0

    mul-float v33, v33, v20

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v33, v0

    mul-float v33, v33, v19

    sub-float v8, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v32, v0

    mul-float v32, v32, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v33, v0

    mul-float v33, v33, v20

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v33, v0

    mul-float v33, v33, v18

    add-float v12, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v33, v0

    mul-float v33, v33, v19

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v33, v0

    mul-float v33, v33, v18

    sub-float v16, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v33, v0

    mul-float v33, v33, v22

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v33, v0

    mul-float v33, v33, v21

    sub-float v5, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v32, v0

    mul-float v32, v32, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v33, v0

    mul-float v33, v33, v20

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v33, v0

    mul-float v33, v33, v19

    add-float v9, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v32, v0

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v33, v0

    mul-float v33, v33, v20

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v33, v0

    mul-float v33, v33, v18

    sub-float v13, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v32, v0

    mul-float v32, v32, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v33, v0

    mul-float v33, v33, v19

    sub-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v33, v0

    mul-float v33, v33, v18

    add-float v17, v32, v33

    move-object/from16 v0, p0

    iput v2, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iput v3, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iput v4, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iput v5, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move-object/from16 v0, p0

    iput v6, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move-object/from16 v0, p0

    iput v7, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move-object/from16 v0, p0

    iput v8, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move-object/from16 v0, p0

    iput v9, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move-object/from16 v0, p0

    iput v10, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iput v11, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iput v12, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move-object/from16 v0, p0

    iput v14, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move-object/from16 v0, p0

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/jme3/math/Matrix4f;->m33:F

    const/high16 v32, 0x3f800000

    div-float v31, v32, v30

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/jme3/math/Matrix4f;->multLocal(F)V

    goto/16 :goto_0
.end method

.method public loadIdentity()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m31:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    return-void
.end method

.method public mult(Lcom/jme3/math/Matrix4f;Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;
    .locals 21
    .param p1    # Lcom/jme3/math/Matrix4f;
    .param p2    # Lcom/jme3/math/Matrix4f;

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Matrix4f;

    invoke-direct/range {p2 .. p2}, Lcom/jme3/math/Matrix4f;-><init>()V

    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v2, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v3, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v4, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v5, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v6, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v7, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v8, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v9, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v10, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v11, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v12, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v13, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v14, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v15, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v16, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v17, v18, v19

    move-object/from16 v0, p2

    iput v2, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move-object/from16 v0, p2

    iput v3, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move-object/from16 v0, p2

    iput v4, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move-object/from16 v0, p2

    iput v5, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move-object/from16 v0, p2

    iput v6, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move-object/from16 v0, p2

    iput v7, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move-object/from16 v0, p2

    iput v8, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move-object/from16 v0, p2

    iput v9, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move-object/from16 v0, p2

    iput v10, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move-object/from16 v0, p2

    iput v11, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move-object/from16 v0, p2

    iput v12, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move-object/from16 v0, p2

    iput v13, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move-object/from16 v0, p2

    iput v14, v0, Lcom/jme3/math/Matrix4f;->m30:F

    move-object/from16 v0, p2

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m31:F

    move/from16 v0, v16

    move-object/from16 v1, p2

    iput v0, v1, Lcom/jme3/math/Matrix4f;->m32:F

    move/from16 v0, v17

    move-object/from16 v1, p2

    iput v0, v1, Lcom/jme3/math/Matrix4f;->m33:F

    return-object p2
.end method

.method public mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 5
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Vector3f;

    invoke-direct {p2}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m01:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m02:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m03:F

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m10:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m11:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m12:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m13:F

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m20:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m21:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m22:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m23:F

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->z:F

    return-object p2
.end method

.method public mult(Lcom/jme3/math/Vector4f;Lcom/jme3/math/Vector4f;)Lcom/jme3/math/Vector4f;
    .locals 6
    .param p1    # Lcom/jme3/math/Vector4f;
    .param p2    # Lcom/jme3/math/Vector4f;

    if-nez p1, :cond_0

    sget-object v4, Lcom/jme3/math/Matrix4f;->logger:Ljava/util/logging/Logger;

    const-string v5, "Source vector is null, null result returned."

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    if-nez p2, :cond_1

    new-instance p2, Lcom/jme3/math/Vector4f;

    invoke-direct {p2}, Lcom/jme3/math/Vector4f;-><init>()V

    :cond_1
    iget v1, p1, Lcom/jme3/math/Vector4f;->x:F

    iget v2, p1, Lcom/jme3/math/Vector4f;->y:F

    iget v3, p1, Lcom/jme3/math/Vector4f;->z:F

    iget v0, p1, Lcom/jme3/math/Vector4f;->w:F

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v4, v1

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m01:F

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m02:F

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m03:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Vector4f;->x:F

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m10:F

    mul-float/2addr v4, v1

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m11:F

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m12:F

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m13:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Vector4f;->y:F

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m20:F

    mul-float/2addr v4, v1

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m21:F

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m22:F

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m23:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Vector4f;->z:F

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m30:F

    mul-float/2addr v4, v1

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m31:F

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m32:F

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Matrix4f;->m33:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Vector4f;->w:F

    move-object v4, p2

    goto :goto_0
.end method

.method public multLocal(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;
    .locals 1
    .param p1    # Lcom/jme3/math/Matrix4f;

    invoke-virtual {p0, p1, p0}, Lcom/jme3/math/Matrix4f;->mult(Lcom/jme3/math/Matrix4f;Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    move-result-object v0

    return-object v0
.end method

.method public multLocal(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m31:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m31:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    return-void
.end method

.method public multNormal(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 5
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Vector3f;

    invoke-direct {p2}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m01:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m02:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m10:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m11:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m12:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m20:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m21:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m22:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->z:F

    return-object p2
.end method

.method public multProj(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)F
    .locals 5
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m01:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m02:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m03:F

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m10:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m11:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m12:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m13:F

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m20:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m21:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m22:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m23:F

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Matrix4f;->m30:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m31:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m32:F

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Matrix4f;->m33:F

    add-float/2addr v3, v4

    return v3
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 4
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "m00"

    invoke-interface {v0, v1, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m00:F

    const-string v1, "m01"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m01:F

    const-string v1, "m02"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m02:F

    const-string v1, "m03"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m03:F

    const-string v1, "m10"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m10:F

    const-string v1, "m11"

    invoke-interface {v0, v1, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m11:F

    const-string v1, "m12"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m12:F

    const-string v1, "m13"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m13:F

    const-string v1, "m20"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m20:F

    const-string v1, "m21"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m21:F

    const-string v1, "m22"

    invoke-interface {v0, v1, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m22:F

    const-string v1, "m23"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m23:F

    const-string v1, "m30"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m30:F

    const-string v1, "m31"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m31:F

    const-string v1, "m32"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m32:F

    const-string v1, "m33"

    invoke-interface {v0, v1, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Matrix4f;->m33:F

    return-void
.end method

.method public scale(Lcom/jme3/math/Vector3f;)V
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m31:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m31:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    return-void
.end method

.method public set(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;
    .locals 1
    .param p1    # Lcom/jme3/math/Matrix4f;

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m00:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m01:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m02:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m03:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m10:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m11:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m12:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m13:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m20:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m21:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m22:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m23:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m30:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m31:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m31:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m32:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    iget v0, p1, Lcom/jme3/math/Matrix4f;->m33:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    return-object p0
.end method

.method public setRotationQuaternion(Lcom/jme3/math/Quaternion;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Quaternion;

    invoke-virtual {p1, p0}, Lcom/jme3/math/Quaternion;->toRotationMatrix(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    return-void
.end method

.method public setScale(FFF)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    mul-float/2addr v0, p3

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    return-void
.end method

.method public setTransform(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Matrix3f;)V
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Matrix3f;

    const/4 v2, 0x0

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m00:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m01:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m02:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m10:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m11:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m12:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iget v0, p1, Lcom/jme3/math/Vector3f;->y:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m20:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m21:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p3, Lcom/jme3/math/Matrix3f;->m22:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iget v0, p1, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    iput v2, p0, Lcom/jme3/math/Matrix4f;->m30:F

    iput v2, p0, Lcom/jme3/math/Matrix4f;->m31:F

    iput v2, p0, Lcom/jme3/math/Matrix4f;->m32:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    return-void
.end method

.method public setTranslation(FFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iput p1, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iput p2, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iput p3, p0, Lcom/jme3/math/Matrix4f;->m23:F

    return-void
.end method

.method public setTranslation(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iget v0, p1, Lcom/jme3/math/Vector3f;->y:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iget v0, p1, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    return-void
.end method

.method public toRotationMatrix(Lcom/jme3/math/Matrix3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Matrix3f;

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m00:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m01:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m02:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m10:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m11:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m12:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m20:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m21:F

    iget v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iput v0, p1, Lcom/jme3/math/Matrix3f;->m22:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Matrix4f\n[\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m00:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m01:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m02:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m03:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m10:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m11:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m12:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m13:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m20:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m21:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m22:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m23:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m30:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m31:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m32:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jme3/math/Matrix4f;->m33:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " \n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public zero()Lcom/jme3/math/Matrix4f;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m03:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m02:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m01:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m00:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m13:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m12:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m11:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m10:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m23:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m22:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m21:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m20:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m33:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m32:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m31:F

    iput v0, p0, Lcom/jme3/math/Matrix4f;->m30:F

    return-object p0
.end method
