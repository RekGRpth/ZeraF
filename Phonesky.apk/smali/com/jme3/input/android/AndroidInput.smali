.class public Lcom/jme3/input/android/AndroidInput;
.super Landroid/opengl/GLSurfaceView;
.source "AndroidInput.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Lcom/jme3/input/TouchInput;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/input/android/AndroidInput$1;
    }
.end annotation


# static fields
.field private static final ANDROID_TO_JME:[I

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private detector:Landroid/view/GestureDetector;

.field public dontSendHistory:Z

.field private final eventPool:Lcom/jme3/util/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/RingBuffer",
            "<",
            "Lcom/jme3/input/event/TouchEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/RingBuffer",
            "<",
            "Lcom/jme3/input/event/TouchEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final eventQueue:Lcom/jme3/util/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/RingBuffer",
            "<",
            "Lcom/jme3/input/event/TouchEvent;",
            ">;"
        }
    .end annotation
.end field

.field private isInitialized:Z

.field public keyboardEventsEnabled:Z

.field private final lastPositions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/jme3/math/Vector2f;",
            ">;"
        }
    .end annotation
.end field

.field private lastX:I

.field private lastY:I

.field private listener:Lcom/jme3/input/RawInputListener;

.field public mouseEventsEnabled:Z

.field public mouseEventsInvertX:Z

.field public mouseEventsInvertY:Z

.field private scaledetector:Landroid/view/ScaleGestureDetector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/input/android/AndroidInput;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/input/android/AndroidInput;->logger:Ljava/util/logging/Logger;

    const/16 v0, 0x5c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jme3/input/android/AndroidInput;->ANDROID_TO_JME:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0xc7
        0x1
        0x0
        0x0
        0xb
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0x37
        0x0
        0xc8
        0xd0
        0xcb
        0xcd
        0x1c
        0x0
        0x0
        0xde
        0x0
        0x0
        0x1e
        0x30
        0x2e
        0x20
        0x12
        0x21
        0x22
        0x23
        0x17
        0x24
        0x25
        0x26
        0x32
        0x31
        0x18
        0x19
        0x10
        0x13
        0x1f
        0x14
        0x16
        0x2f
        0x11
        0x2d
        0x15
        0x2c
        0x33
        0x34
        0x38
        0xb8
        0x2a
        0x36
        0xf
        0x39
        0x0
        0x0
        0x0
        0x1c
        0xd3
        0x29
        0xc
        0xd
        0x1a
        0x1b
        0x2b
        0x27
        0x28
        0x35
        0x91
        0x45
        0x0
        0x0
        0x4e
        0xdb
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/16 v2, 0x400

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertX:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertY:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->keyboardEventsEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->dontSendHistory:Z

    new-instance v0, Lcom/jme3/util/RingBuffer;

    invoke-direct {v0, v2}, Lcom/jme3/util/RingBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    new-instance v0, Lcom/jme3/util/RingBuffer;

    invoke-direct {v0, v2}, Lcom/jme3/util/RingBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    new-instance v0, Lcom/jme3/util/RingBuffer;

    invoke-direct {v0, v2}, Lcom/jme3/util/RingBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->lastPositions:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->isInitialized:Z

    iput-object v3, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, v3, p0, v3, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->detector:Landroid/view/GestureDetector;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->scaledetector:Landroid/view/ScaleGestureDetector;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x0

    const/16 v2, 0x400

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertX:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertY:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->keyboardEventsEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->dontSendHistory:Z

    new-instance v0, Lcom/jme3/util/RingBuffer;

    invoke-direct {v0, v2}, Lcom/jme3/util/RingBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    new-instance v0, Lcom/jme3/util/RingBuffer;

    invoke-direct {v0, v2}, Lcom/jme3/util/RingBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    new-instance v0, Lcom/jme3/util/RingBuffer;

    invoke-direct {v0, v2}, Lcom/jme3/util/RingBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->lastPositions:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/jme3/input/android/AndroidInput;->isInitialized:Z

    iput-object v3, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, v3, p0, v3, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->detector:Landroid/view/GestureDetector;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/jme3/input/android/AndroidInput;->scaledetector:Landroid/view/ScaleGestureDetector;

    return-void
.end method

.method private generateEvents()V
    .locals 12

    const/4 v5, 0x0

    const/4 v11, -0x1

    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    if-eqz v6, :cond_5

    :goto_0
    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v6}, Lcom/jme3/util/RingBuffer;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    monitor-enter v6

    :try_start_0
    iget-object v9, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v9}, Lcom/jme3/util/RingBuffer;->pop()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/jme3/input/event/TouchEvent;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v8, :cond_0

    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    invoke-interface {v6, v8}, Lcom/jme3/input/RawInputListener;->onTouchEvent(Lcom/jme3/input/event/TouchEvent;)V

    iget-boolean v6, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsEnabled:Z

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertX:Z

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getWidth()I

    move-result v6

    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getX()F

    move-result v9

    float-to-int v9, v9

    sub-int v1, v6, v9

    :goto_1
    iget-boolean v6, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertY:Z

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v6

    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    sub-int v2, v6, v9

    :goto_2
    sget-object v6, Lcom/jme3/input/android/AndroidInput$1;->$SwitchMap$com$jme3$input$event$TouchEvent$Type:[I

    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getType()Lcom/jme3/input/event/TouchEvent$Type;

    move-result-object v9

    invoke-virtual {v9}, Lcom/jme3/input/event/TouchEvent$Type;->ordinal()I

    move-result v9

    aget v6, v6, v9

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_3
    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->isConsumed()Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    monitor-enter v6

    :try_start_1
    iget-object v9, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v9, v8}, Lcom/jme3/util/RingBuffer;->push(Ljava/lang/Object;)V

    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    :catchall_1
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v5

    :cond_1
    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getX()F

    move-result v6

    float-to-int v1, v6

    goto :goto_1

    :cond_2
    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getY()F

    move-result v6

    float-to-int v2, v6

    goto :goto_2

    :pswitch_0
    new-instance v7, Lcom/jme3/input/event/MouseButtonEvent;

    const/4 v6, 0x1

    invoke-direct {v7, v5, v6, v1, v2}, Lcom/jme3/input/event/MouseButtonEvent;-><init>(IZII)V

    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getTime()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Lcom/jme3/input/event/MouseButtonEvent;->setTime(J)V

    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    invoke-interface {v6, v7}, Lcom/jme3/input/RawInputListener;->onMouseButtonEvent(Lcom/jme3/input/event/MouseButtonEvent;)V

    iput v11, p0, Lcom/jme3/input/android/AndroidInput;->lastX:I

    iput v11, p0, Lcom/jme3/input/android/AndroidInput;->lastY:I

    goto :goto_3

    :pswitch_1
    new-instance v7, Lcom/jme3/input/event/MouseButtonEvent;

    invoke-direct {v7, v5, v5, v1, v2}, Lcom/jme3/input/event/MouseButtonEvent;-><init>(IZII)V

    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getTime()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Lcom/jme3/input/event/MouseButtonEvent;->setTime(J)V

    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    invoke-interface {v6, v7}, Lcom/jme3/input/RawInputListener;->onMouseButtonEvent(Lcom/jme3/input/event/MouseButtonEvent;)V

    iput v11, p0, Lcom/jme3/input/android/AndroidInput;->lastX:I

    iput v11, p0, Lcom/jme3/input/android/AndroidInput;->lastY:I

    goto :goto_3

    :pswitch_2
    iget v6, p0, Lcom/jme3/input/android/AndroidInput;->lastX:I

    if-eq v6, v11, :cond_3

    iget v6, p0, Lcom/jme3/input/android/AndroidInput;->lastX:I

    sub-int v3, v1, v6

    iget v6, p0, Lcom/jme3/input/android/AndroidInput;->lastY:I

    sub-int v4, v2, v6

    :goto_4
    new-instance v0, Lcom/jme3/input/event/MouseMotionEvent;

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/jme3/input/event/MouseMotionEvent;-><init>(IIIIII)V

    invoke-virtual {v8}, Lcom/jme3/input/event/TouchEvent;->getTime()J

    move-result-wide v9

    invoke-virtual {v0, v9, v10}, Lcom/jme3/input/event/MouseMotionEvent;->setTime(J)V

    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    invoke-interface {v6, v0}, Lcom/jme3/input/RawInputListener;->onMouseMotionEvent(Lcom/jme3/input/event/MouseMotionEvent;)V

    iput v1, p0, Lcom/jme3/input/android/AndroidInput;->lastX:I

    iput v2, p0, Lcom/jme3/input/android/AndroidInput;->lastY:I

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    const/4 v4, 0x0

    goto :goto_4

    :cond_4
    iget-object v6, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    monitor-enter v6

    :try_start_3
    iget-object v9, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v9, v8}, Lcom/jme3/util/RingBuffer;->push(Ljava/lang/Object;)V

    monitor-exit v6

    goto/16 :goto_0

    :catchall_2
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v5

    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent(Z)Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    return-object v0
.end method

.method private getNextFreeTouchEvent(Z)Lcom/jme3/input/event/TouchEvent;
    .locals 6
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4}, Lcom/jme3/util/RingBuffer;->size()I

    move-result v3

    :goto_0
    if-lez v3, :cond_0

    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4}, Lcom/jme3/util/RingBuffer;->pop()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/jme3/input/event/TouchEvent;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/jme3/input/event/TouchEvent;->isConsumed()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPoolUnConsumed:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4, v1}, Lcom/jme3/util/RingBuffer;->push(Ljava/lang/Object;)V

    const/4 v1, 0x0

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4}, Lcom/jme3/util/RingBuffer;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz p1, :cond_3

    sget-object v4, Lcom/jme3/input/android/AndroidInput;->logger:Ljava/util/logging/Logger;

    const-string v5, "eventPool buffer underrun"

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :cond_1
    iget-object v5, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    monitor-enter v5

    :try_start_1
    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4}, Lcom/jme3/util/RingBuffer;->isEmpty()Z

    move-result v2

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const-wide/16 v4, 0x32

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    if-nez v2, :cond_1

    iget-object v5, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    monitor-enter v5

    :try_start_3
    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4}, Lcom/jme3/util/RingBuffer;->pop()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/jme3/input/event/TouchEvent;

    move-object v1, v0

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_2
    :goto_2
    return-object v1

    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    :catchall_1
    move-exception v4

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4

    :catchall_2
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v4

    :cond_3
    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4}, Lcom/jme3/util/RingBuffer;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v1, Lcom/jme3/input/event/TouchEvent;

    invoke-direct {v1}, Lcom/jme3/input/event/TouchEvent;-><init>()V

    sget-object v4, Lcom/jme3/input/android/AndroidInput;->logger:Ljava/util/logging/Logger;

    const-string v5, "eventPool buffer underrun"

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    monitor-enter v5

    :try_start_7
    iget-object v4, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v4}, Lcom/jme3/util/RingBuffer;->pop()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/jme3/input/event/TouchEvent;

    move-object v1, v0

    monitor-exit v5

    goto :goto_2

    :catchall_3
    move-exception v4

    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v4

    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method private processEvent(Lcom/jme3/input/event/TouchEvent;)V
    .locals 2
    .param p1    # Lcom/jme3/input/event/TouchEvent;

    iget-object v1, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v0, p1}, Lcom/jme3/util/RingBuffer;->push(Ljava/lang/Object;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/input/android/AndroidInput;->isInitialized:Z

    :goto_0
    iget-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v0}, Lcom/jme3/util/RingBuffer;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v0}, Lcom/jme3/util/RingBuffer;->pop()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v0}, Lcom/jme3/util/RingBuffer;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jme3/input/android/AndroidInput;->eventQueue:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v0}, Lcom/jme3/util/RingBuffer;->pop()Ljava/lang/Object;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public getInputTimeNanos()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public initialize()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x400

    if-ge v0, v2, :cond_0

    new-instance v1, Lcom/jme3/input/event/TouchEvent;

    invoke-direct {v1}, Lcom/jme3/input/event/TouchEvent;-><init>()V

    iget-object v2, p0, Lcom/jme3/input/android/AndroidInput;->eventPool:Lcom/jme3/util/RingBuffer;

    invoke-virtual {v2, v1}, Lcom/jme3/util/RingBuffer;->push(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/jme3/input/android/AndroidInput;->isInitialized:Z

    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->DOUBLETAP:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->FLING:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->KEY_DOWN:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;)V

    invoke-virtual {v0, p1}, Lcom/jme3/input/event/TouchEvent;->setKeyCode(I)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setCharacters(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/16 v1, 0x18

    if-eq p1, v1, :cond_0

    const/16 v1, 0x19

    if-ne p1, v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->KEY_UP:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;)V

    invoke-virtual {v0, p1}, Lcom/jme3/input/event/TouchEvent;->setKeyCode(I)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setCharacters(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/16 v1, 0x18

    if-eq p1, v1, :cond_0

    const/16 v1, 0x19

    if-ne p1, v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->LONGPRESSED:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    return-void
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v6, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->SCALE_MOVE:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    invoke-virtual {v0, v6}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setScaleSpan(F)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setScaleFactor(F)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    return v6
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->SCALE_START:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setScaleSpan(F)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setScaleFactor(F)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 6
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->SCALE_END:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setScaleSpan(F)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setScaleFactor(F)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->SCROLL:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    const/high16 v4, -0x40800000

    mul-float v5, p4, v4

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    invoke-virtual {v0, v6}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    return v6
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->SHOWPRESS:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->TAP:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v6, v1, 0xff

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const v2, 0xff00

    and-int/2addr v1, v2

    shr-int/lit8 v11, v1, 0x8

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v10

    packed-switch v6, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/jme3/input/android/AndroidInput;->detector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v1, p0, Lcom/jme3/input/android/AndroidInput;->scaledetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    return v7

    :pswitch_1
    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->DOWN:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    invoke-virtual {v0, v10}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPressure(F)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/4 v7, 0x1

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->UP:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    sub-float/2addr v3, v5

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    invoke-virtual {v0, v10}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPressure(F)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    const/4 v7, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v9, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-ge v9, v1, :cond_1

    iget-object v1, p0, Lcom/jme3/input/android/AndroidInput;->lastPositions:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/jme3/math/Vector2f;

    if-nez v8, :cond_0

    new-instance v8, Lcom/jme3/math/Vector2f;

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-direct {v8, v1, v2}, Lcom/jme3/math/Vector2f;-><init>(FF)V

    iget-object v1, p0, Lcom/jme3/input/android/AndroidInput;->lastPositions:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->getNextFreeTouchEvent()Lcom/jme3/input/event/TouchEvent;

    move-result-object v0

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->MOVE:Lcom/jme3/input/event/TouchEvent$Type;

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    iget v5, v8, Lcom/jme3/math/Vector2f;->x:F

    sub-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v12

    sub-float/2addr v5, v12

    iget v12, v8, Lcom/jme3/math/Vector2f;->y:F

    sub-float/2addr v5, v12

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    invoke-virtual {v0, v10}, Lcom/jme3/input/event/TouchEvent;->setPointerId(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/input/event/TouchEvent;->setTime(J)V

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jme3/input/event/TouchEvent;->setPressure(F)V

    invoke-direct {p0, v0}, Lcom/jme3/input/android/AndroidInput;->processEvent(Lcom/jme3/input/event/TouchEvent;)V

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {p0}, Lcom/jme3/input/android/AndroidInput;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v8, v1, v2}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    :cond_1
    const/4 v7, 0x1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setInputListener(Lcom/jme3/input/RawInputListener;)V
    .locals 0
    .param p1    # Lcom/jme3/input/RawInputListener;

    iput-object p1, p0, Lcom/jme3/input/android/AndroidInput;->listener:Lcom/jme3/input/RawInputListener;

    return-void
.end method

.method public setMouseEventsEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsEnabled:Z

    return-void
.end method

.method public setMouseEventsInvertX(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertX:Z

    return-void
.end method

.method public setMouseEventsInvertY(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/input/android/AndroidInput;->mouseEventsInvertY:Z

    return-void
.end method

.method public update()V
    .locals 0

    invoke-direct {p0}, Lcom/jme3/input/android/AndroidInput;->generateEvents()V

    return-void
.end method
