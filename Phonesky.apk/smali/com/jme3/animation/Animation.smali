.class public Lcom/jme3/animation/Animation;
.super Ljava/lang/Object;
.source "Animation.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/lang/Cloneable;


# instance fields
.field private length:F

.field private name:Ljava/lang/String;

.field private tracks:[Lcom/jme3/animation/Track;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;F)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jme3/animation/Animation;->name:Ljava/lang/String;

    iput p2, p0, Lcom/jme3/animation/Animation;->length:F

    return-void
.end method


# virtual methods
.method public clone()Lcom/jme3/animation/Animation;
    .locals 5

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/animation/Animation;

    iget-object v3, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    invoke-virtual {v3}, [Lcom/jme3/animation/Track;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/jme3/animation/Track;

    iput-object v3, v2, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    iget-object v3, v2, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    iget-object v4, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    aget-object v4, v4, v1

    invoke-interface {v4}, Lcom/jme3/animation/Track;->clone()Lcom/jme3/animation/Track;

    move-result-object v4

    aput-object v4, v3, v1
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :cond_0
    return-object v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/animation/Animation;->clone()Lcom/jme3/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public getLength()F
    .locals 1

    iget v0, p0, Lcom/jme3/animation/Animation;->length:F

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jme3/animation/Animation;->name:Ljava/lang/String;

    return-object v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 6
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v1

    const-string v2, "name"

    invoke-interface {v1, v2, v5}, Lcom/jme3/export/InputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/animation/Animation;->name:Ljava/lang/String;

    const-string v2, "length"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/jme3/animation/Animation;->length:F

    const-string v2, "tracks"

    invoke-interface {v1, v2, v5}, Lcom/jme3/export/InputCapsule;->readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;

    move-result-object v0

    array-length v2, v0

    new-array v2, v2, [Lcom/jme3/animation/Track;

    iput-object v2, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    iget-object v2, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    array-length v3, v0

    invoke-static {v0, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method setTime(FFLcom/jme3/animation/AnimControl;Lcom/jme3/animation/AnimChannel;Lcom/jme3/util/TempVars;)V
    .locals 7
    .param p1    # F
    .param p2    # F
    .param p3    # Lcom/jme3/animation/AnimControl;
    .param p4    # Lcom/jme3/animation/AnimChannel;
    .param p5    # Lcom/jme3/util/TempVars;

    iget-object v0, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v6, 0x0

    :goto_0
    iget-object v0, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    array-length v0, v0

    if-ge v6, v0, :cond_0

    iget-object v0, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    aget-object v0, v0, v6

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/jme3/animation/Track;->setTime(FFLcom/jme3/animation/AnimControl;Lcom/jme3/animation/AnimChannel;Lcom/jme3/util/TempVars;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public setTracks([Lcom/jme3/animation/Track;)V
    .locals 0
    .param p1    # [Lcom/jme3/animation/Track;

    iput-object p1, p0, Lcom/jme3/animation/Animation;->tracks:[Lcom/jme3/animation/Track;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/animation/Animation;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/animation/Animation;->length:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
