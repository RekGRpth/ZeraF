.class public Lcom/jme3/font/BitmapFont;
.super Ljava/lang/Object;
.source "BitmapFont.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# instance fields
.field private charSet:Lcom/jme3/font/BitmapCharacterSet;

.field private pages:[Lcom/jme3/material/Material;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 5
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v2, "charSet"

    invoke-interface {v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v2

    check-cast v2, Lcom/jme3/font/BitmapCharacterSet;

    iput-object v2, p0, Lcom/jme3/font/BitmapFont;->charSet:Lcom/jme3/font/BitmapCharacterSet;

    const-string v2, "pages"

    invoke-interface {v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;

    move-result-object v1

    array-length v2, v1

    new-array v2, v2, [Lcom/jme3/material/Material;

    iput-object v2, p0, Lcom/jme3/font/BitmapFont;->pages:[Lcom/jme3/material/Material;

    iget-object v2, p0, Lcom/jme3/font/BitmapFont;->pages:[Lcom/jme3/material/Material;

    iget-object v3, p0, Lcom/jme3/font/BitmapFont;->pages:[Lcom/jme3/material/Material;

    array-length v3, v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public setCharSet(Lcom/jme3/font/BitmapCharacterSet;)V
    .locals 0
    .param p1    # Lcom/jme3/font/BitmapCharacterSet;

    iput-object p1, p0, Lcom/jme3/font/BitmapFont;->charSet:Lcom/jme3/font/BitmapCharacterSet;

    return-void
.end method

.method public setPages([Lcom/jme3/material/Material;)V
    .locals 2
    .param p1    # [Lcom/jme3/material/Material;

    iput-object p1, p0, Lcom/jme3/font/BitmapFont;->pages:[Lcom/jme3/material/Material;

    iget-object v0, p0, Lcom/jme3/font/BitmapFont;->charSet:Lcom/jme3/font/BitmapCharacterSet;

    array-length v1, p1

    invoke-virtual {v0, v1}, Lcom/jme3/font/BitmapCharacterSet;->setPageSize(I)V

    return-void
.end method
