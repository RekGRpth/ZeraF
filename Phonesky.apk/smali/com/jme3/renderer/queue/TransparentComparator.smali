.class public Lcom/jme3/renderer/queue/TransparentComparator;
.super Ljava/lang/Object;
.source "TransparentComparator.java"

# interfaces
.implements Lcom/jme3/renderer/queue/GeometryComparator;


# instance fields
.field private cam:Lcom/jme3/renderer/Camera;

.field private final tempVec:Lcom/jme3/math/Vector3f;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/queue/TransparentComparator;->tempVec:Lcom/jme3/math/Vector3f;

    return-void
.end method

.method private distanceToCam(Lcom/jme3/scene/Geometry;)F
    .locals 2
    .param p1    # Lcom/jme3/scene/Geometry;

    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getWorldBound()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/renderer/queue/TransparentComparator;->cam:Lcom/jme3/renderer/Camera;

    invoke-virtual {v1}, Lcom/jme3/renderer/Camera;->getLocation()Lcom/jme3/math/Vector3f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jme3/bounding/BoundingVolume;->distanceToEdge(Lcom/jme3/math/Vector3f;)F

    move-result v0

    return v0
.end method


# virtual methods
.method public compare(Lcom/jme3/scene/Geometry;Lcom/jme3/scene/Geometry;)I
    .locals 3
    .param p1    # Lcom/jme3/scene/Geometry;
    .param p2    # Lcom/jme3/scene/Geometry;

    invoke-direct {p0, p1}, Lcom/jme3/renderer/queue/TransparentComparator;->distanceToCam(Lcom/jme3/scene/Geometry;)F

    move-result v0

    invoke-direct {p0, p2}, Lcom/jme3/renderer/queue/TransparentComparator;->distanceToCam(Lcom/jme3/scene/Geometry;)F

    move-result v1

    cmpl-float v2, v0, v1

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    cmpg-float v2, v0, v1

    if-gez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/scene/Geometry;

    check-cast p2, Lcom/jme3/scene/Geometry;

    invoke-virtual {p0, p1, p2}, Lcom/jme3/renderer/queue/TransparentComparator;->compare(Lcom/jme3/scene/Geometry;Lcom/jme3/scene/Geometry;)I

    move-result v0

    return v0
.end method

.method public setCamera(Lcom/jme3/renderer/Camera;)V
    .locals 0
    .param p1    # Lcom/jme3/renderer/Camera;

    iput-object p1, p0, Lcom/jme3/renderer/queue/TransparentComparator;->cam:Lcom/jme3/renderer/Camera;

    return-void
.end method
