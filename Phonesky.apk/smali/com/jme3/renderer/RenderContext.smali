.class public Lcom/jme3/renderer/RenderContext;
.super Ljava/lang/Object;
.source "RenderContext.java"


# instance fields
.field public alphaTestEnabled:Z

.field public ambient:Lcom/jme3/math/ColorRGBA;

.field public attribIndexList:Lcom/jme3/renderer/IDList;

.field public backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field public backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field public backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

.field public backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field public blendMode:Lcom/jme3/material/RenderState$BlendMode;

.field public boundArrayVBO:I

.field public boundAttribs:[Lcom/jme3/scene/VertexBuffer;

.field public boundDrawBuf:I

.field public boundElementArrayVBO:I

.field public boundFBO:I

.field public boundRB:I

.field public boundReadBuf:I

.field public boundShaderProgram:I

.field public boundTextureUnit:I

.field public boundTextures:[Lcom/jme3/texture/Image;

.field public boundVertexArray:I

.field public clipRectEnabled:Z

.field public color:Lcom/jme3/math/ColorRGBA;

.field public colorWriteEnabled:Z

.field public cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

.field public depthTestEnabled:Z

.field public depthWriteEnabled:Z

.field public diffuse:Lcom/jme3/math/ColorRGBA;

.field public frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field public frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field public frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

.field public frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field public lineWidth:F

.field public matrixMode:I

.field public normalizeEnabled:Z

.field public numTexturesSet:I

.field public pointSize:F

.field public pointSprite:Z

.field public polyOffsetEnabled:Z

.field public polyOffsetFactor:F

.field public polyOffsetUnits:F

.field public shininess:F

.field public specular:Lcom/jme3/math/ColorRGBA;

.field public stencilTest:Z

.field public textureIndexList:Lcom/jme3/renderer/IDList;

.field public useVertexColor:Z

.field public wireframe:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v5, 0x1

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jme3/material/RenderState$FaceCullMode;->Off:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->depthTestEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->alphaTestEnabled:Z

    iput-boolean v5, p0, Lcom/jme3/renderer/RenderContext;->depthWriteEnabled:Z

    iput-boolean v5, p0, Lcom/jme3/renderer/RenderContext;->colorWriteEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->clipRectEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->polyOffsetEnabled:Z

    iput v3, p0, Lcom/jme3/renderer/RenderContext;->polyOffsetFactor:F

    iput v3, p0, Lcom/jme3/renderer/RenderContext;->polyOffsetUnits:F

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->normalizeEnabled:Z

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->matrixMode:I

    iput v4, p0, Lcom/jme3/renderer/RenderContext;->pointSize:F

    iput v4, p0, Lcom/jme3/renderer/RenderContext;->lineWidth:F

    sget-object v0, Lcom/jme3/material/RenderState$BlendMode;->Off:Lcom/jme3/material/RenderState$BlendMode;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->wireframe:Z

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->pointSprite:Z

    iput v1, p0, Lcom/jme3/renderer/RenderContext;->boundFBO:I

    iput v1, p0, Lcom/jme3/renderer/RenderContext;->boundRB:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundDrawBuf:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundReadBuf:I

    iput v1, p0, Lcom/jme3/renderer/RenderContext;->numTexturesSet:I

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/jme3/texture/Image;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->boundTextures:[Lcom/jme3/texture/Image;

    new-instance v0, Lcom/jme3/renderer/IDList;

    invoke-direct {v0}, Lcom/jme3/renderer/IDList;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->textureIndexList:Lcom/jme3/renderer/IDList;

    iput v1, p0, Lcom/jme3/renderer/RenderContext;->boundTextureUnit:I

    iput-boolean v1, p0, Lcom/jme3/renderer/RenderContext;->stencilTest:Z

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    sget-object v0, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/jme3/scene/VertexBuffer;

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->boundAttribs:[Lcom/jme3/scene/VertexBuffer;

    new-instance v0, Lcom/jme3/renderer/IDList;

    invoke-direct {v0}, Lcom/jme3/renderer/IDList;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/RenderContext;->attribIndexList:Lcom/jme3/renderer/IDList;

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    sget-object v1, Lcom/jme3/material/RenderState$FaceCullMode;->Off:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->depthTestEnabled:Z

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->alphaTestEnabled:Z

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->depthWriteEnabled:Z

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->colorWriteEnabled:Z

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->clipRectEnabled:Z

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->polyOffsetEnabled:Z

    iput v3, p0, Lcom/jme3/renderer/RenderContext;->polyOffsetFactor:F

    iput v3, p0, Lcom/jme3/renderer/RenderContext;->polyOffsetUnits:F

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->normalizeEnabled:Z

    iput v4, p0, Lcom/jme3/renderer/RenderContext;->matrixMode:I

    const/high16 v1, 0x3f800000

    iput v1, p0, Lcom/jme3/renderer/RenderContext;->pointSize:F

    sget-object v1, Lcom/jme3/material/RenderState$BlendMode;->Off:Lcom/jme3/material/RenderState$BlendMode;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->wireframe:Z

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundFBO:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundRB:I

    iput v4, p0, Lcom/jme3/renderer/RenderContext;->boundDrawBuf:I

    iput v4, p0, Lcom/jme3/renderer/RenderContext;->boundReadBuf:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundElementArrayVBO:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundVertexArray:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundArrayVBO:I

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->numTexturesSet:I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/jme3/renderer/RenderContext;->boundTextures:[Lcom/jme3/texture/Image;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/jme3/renderer/RenderContext;->boundTextures:[Lcom/jme3/texture/Image;

    aput-object v5, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/jme3/renderer/RenderContext;->textureIndexList:Lcom/jme3/renderer/IDList;

    invoke-virtual {v1}, Lcom/jme3/renderer/IDList;->reset()V

    iput v2, p0, Lcom/jme3/renderer/RenderContext;->boundTextureUnit:I

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/jme3/renderer/RenderContext;->boundAttribs:[Lcom/jme3/scene/VertexBuffer;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/jme3/renderer/RenderContext;->boundAttribs:[Lcom/jme3/scene/VertexBuffer;

    aput-object v5, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/jme3/renderer/RenderContext;->attribIndexList:Lcom/jme3/renderer/IDList;

    invoke-virtual {v1}, Lcom/jme3/renderer/IDList;->reset()V

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->stencilTest:Z

    sget-object v1, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v1, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v1, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v1, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v1, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v1, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v1, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    sget-object v1, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v1, p0, Lcom/jme3/renderer/RenderContext;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v5, p0, Lcom/jme3/renderer/RenderContext;->color:Lcom/jme3/math/ColorRGBA;

    iput-object v5, p0, Lcom/jme3/renderer/RenderContext;->specular:Lcom/jme3/math/ColorRGBA;

    iput-object v5, p0, Lcom/jme3/renderer/RenderContext;->diffuse:Lcom/jme3/math/ColorRGBA;

    iput-object v5, p0, Lcom/jme3/renderer/RenderContext;->ambient:Lcom/jme3/math/ColorRGBA;

    iput v3, p0, Lcom/jme3/renderer/RenderContext;->shininess:F

    iput-boolean v2, p0, Lcom/jme3/renderer/RenderContext;->useVertexColor:Z

    return-void
.end method
