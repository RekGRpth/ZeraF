.class public Lcom/jme3/util/AndroidLogHandler;
.super Ljava/util/logging/Handler;
.source "AndroidLogHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/util/logging/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    return-void
.end method

.method public flush()V
    .locals 0

    return-void
.end method

.method public publish(Ljava/util/logging/LogRecord;)V
    .locals 5
    .param p1    # Ljava/util/logging/LogRecord;

    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getLevel()Ljava/util/logging/Level;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getSourceClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getThrown()Ljava/lang/Throwable;

    move-result-object v3

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    if-ne v1, v4, :cond_1

    invoke-static {v0, v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    if-ne v1, v4, :cond_2

    invoke-static {v0, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-ne v1, v4, :cond_3

    invoke-static {v0, v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    sget-object v4, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    if-ne v1, v4, :cond_4

    invoke-static {v0, v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_4
    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-eq v1, v4, :cond_5

    sget-object v4, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eq v1, v4, :cond_5

    sget-object v4, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    if-ne v1, v4, :cond_0

    :cond_5
    invoke-static {v0, v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
