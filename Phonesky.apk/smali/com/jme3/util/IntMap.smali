.class public final Lcom/jme3/util/IntMap;
.super Ljava/lang/Object;
.source "IntMap.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/util/IntMap$Entry;,
        Lcom/jme3/util/IntMap$IntMapIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/jme3/util/IntMap$Entry",
        "<TT;>;>;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field private capacity:I

.field private final loadFactor:F

.field private mask:I

.field private size:I

.field private table:[Lcom/jme3/util/IntMap$Entry;

.field private threshold:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x10

    const/high16 v1, 0x3f400000

    invoke-direct {p0, v0, v1}, Lcom/jme3/util/IntMap;-><init>(IF)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x3f400000

    invoke-direct {p0, p1, v0}, Lcom/jme3/util/IntMap;-><init>(IF)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 2
    .param p1    # I
    .param p2    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x40000000

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "initialCapacity is too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-gez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "initialCapacity must be greater than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "initialCapacity must be greater than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/jme3/util/IntMap;->capacity:I

    :goto_0
    iget v0, p0, Lcom/jme3/util/IntMap;->capacity:I

    if-ge v0, p1, :cond_3

    iget v0, p0, Lcom/jme3/util/IntMap;->capacity:I

    shl-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jme3/util/IntMap;->capacity:I

    goto :goto_0

    :cond_3
    iput p2, p0, Lcom/jme3/util/IntMap;->loadFactor:F

    iget v0, p0, Lcom/jme3/util/IntMap;->capacity:I

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lcom/jme3/util/IntMap;->threshold:I

    iget v0, p0, Lcom/jme3/util/IntMap;->capacity:I

    new-array v0, v0, [Lcom/jme3/util/IntMap$Entry;

    iput-object v0, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    iget v0, p0, Lcom/jme3/util/IntMap;->capacity:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jme3/util/IntMap;->mask:I

    return-void
.end method

.method static synthetic access$000(Lcom/jme3/util/IntMap;)[Lcom/jme3/util/IntMap$Entry;
    .locals 1
    .param p0    # Lcom/jme3/util/IntMap;

    iget-object v0, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jme3/util/IntMap;)I
    .locals 1
    .param p0    # Lcom/jme3/util/IntMap;

    iget v0, p0, Lcom/jme3/util/IntMap;->size:I

    return v0
.end method


# virtual methods
.method public clear()V
    .locals 3

    iget-object v1, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    array-length v0, v1

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    const/4 v2, 0x0

    aput-object v2, v1, v0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    iput v2, p0, Lcom/jme3/util/IntMap;->size:I

    return-void
.end method

.method public clone()Lcom/jme3/util/IntMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jme3/util/IntMap",
            "<TT;>;"
        }
    .end annotation

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/util/IntMap;

    iget-object v3, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    array-length v3, v3

    new-array v2, v3, [Lcom/jme3/util/IntMap$Entry;

    iget-object v3, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    array-length v3, v3

    add-int/lit8 v1, v3, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v3, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/jme3/util/IntMap$Entry;->clone()Lcom/jme3/util/IntMap$Entry;

    move-result-object v3

    aput-object v3, v2, v1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    iput-object v2, v0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v0

    :catch_0
    move-exception v3

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/util/IntMap;->clone()Lcom/jme3/util/IntMap;

    move-result-object v0

    return-object v0
.end method

.method public containsKey(I)Z
    .locals 3
    .param p1    # I

    iget v2, p0, Lcom/jme3/util/IntMap;->mask:I

    and-int v1, p1, v2

    iget-object v2, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aget-object v0, v2, v1

    :goto_0
    if-eqz v0, :cond_1

    iget v2, v0, Lcom/jme3/util/IntMap$Entry;->key:I

    if-ne v2, p1, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    iget-object v0, v0, Lcom/jme3/util/IntMap$Entry;->next:Lcom/jme3/util/IntMap$Entry;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    iget v2, p0, Lcom/jme3/util/IntMap;->mask:I

    and-int v1, p1, v2

    iget-object v2, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aget-object v0, v2, v1

    :goto_0
    if-eqz v0, :cond_1

    iget v2, v0, Lcom/jme3/util/IntMap$Entry;->key:I

    if-ne v2, p1, :cond_0

    iget-object v2, v0, Lcom/jme3/util/IntMap$Entry;->value:Ljava/lang/Object;

    :goto_1
    return-object v2

    :cond_0
    iget-object v0, v0, Lcom/jme3/util/IntMap$Entry;->next:Lcom/jme3/util/IntMap$Entry;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/jme3/util/IntMap$Entry",
            "<TT;>;>;"
        }
    .end annotation

    new-instance v0, Lcom/jme3/util/IntMap$IntMapIterator;

    invoke-direct {v0, p0}, Lcom/jme3/util/IntMap$IntMapIterator;-><init>(Lcom/jme3/util/IntMap;)V

    invoke-virtual {v0}, Lcom/jme3/util/IntMap$IntMapIterator;->beginUse()V

    return-object v0
.end method

.method public put(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation

    const/4 v7, 0x0

    iget v9, p0, Lcom/jme3/util/IntMap;->mask:I

    and-int v2, p1, v9

    iget-object v9, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aget-object v1, v9, v2

    :goto_0
    if-eqz v1, :cond_2

    iget v9, v1, Lcom/jme3/util/IntMap$Entry;->key:I

    if-eq v9, p1, :cond_0

    iget-object v1, v1, Lcom/jme3/util/IntMap$Entry;->next:Lcom/jme3/util/IntMap$Entry;

    goto :goto_0

    :cond_0
    iget-object v7, v1, Lcom/jme3/util/IntMap$Entry;->value:Ljava/lang/Object;

    iput-object p2, v1, Lcom/jme3/util/IntMap$Entry;->value:Ljava/lang/Object;

    :cond_1
    :goto_1
    return-object v7

    :cond_2
    iget-object v9, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    new-instance v10, Lcom/jme3/util/IntMap$Entry;

    iget-object v11, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aget-object v11, v11, v2

    invoke-direct {v10, p1, p2, v11}, Lcom/jme3/util/IntMap$Entry;-><init>(ILjava/lang/Object;Lcom/jme3/util/IntMap$Entry;)V

    aput-object v10, v9, v2

    iget v9, p0, Lcom/jme3/util/IntMap;->size:I

    add-int/lit8 v10, v9, 0x1

    iput v10, p0, Lcom/jme3/util/IntMap;->size:I

    iget v10, p0, Lcom/jme3/util/IntMap;->threshold:I

    if-lt v9, v10, :cond_1

    iget v9, p0, Lcom/jme3/util/IntMap;->capacity:I

    mul-int/lit8 v4, v9, 0x2

    new-array v5, v4, [Lcom/jme3/util/IntMap$Entry;

    iget-object v8, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    add-int/lit8 v0, v4, -0x1

    const/4 v3, 0x0

    :goto_2
    array-length v9, v8

    if-ge v3, v9, :cond_5

    aget-object v1, v8, v3

    if-eqz v1, :cond_4

    aput-object v7, v8, v3

    :cond_3
    iget-object v6, v1, Lcom/jme3/util/IntMap$Entry;->next:Lcom/jme3/util/IntMap$Entry;

    iget v9, v1, Lcom/jme3/util/IntMap$Entry;->key:I

    and-int v2, v9, v0

    aget-object v9, v5, v2

    iput-object v9, v1, Lcom/jme3/util/IntMap$Entry;->next:Lcom/jme3/util/IntMap$Entry;

    aput-object v1, v5, v2

    move-object v1, v6

    if-nez v1, :cond_3

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    iput-object v5, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    iput v4, p0, Lcom/jme3/util/IntMap;->capacity:I

    int-to-float v9, v4

    iget v10, p0, Lcom/jme3/util/IntMap;->loadFactor:F

    mul-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, p0, Lcom/jme3/util/IntMap;->threshold:I

    iget v9, p0, Lcom/jme3/util/IntMap;->capacity:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lcom/jme3/util/IntMap;->mask:I

    goto :goto_1
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    iget v4, p0, Lcom/jme3/util/IntMap;->mask:I

    and-int v1, p1, v4

    iget-object v4, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aget-object v3, v4, v1

    move-object v0, v3

    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/jme3/util/IntMap$Entry;->next:Lcom/jme3/util/IntMap$Entry;

    iget v4, v0, Lcom/jme3/util/IntMap$Entry;->key:I

    if-ne v4, p1, :cond_1

    iget v4, p0, Lcom/jme3/util/IntMap;->size:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/jme3/util/IntMap;->size:I

    if-ne v3, v0, :cond_0

    iget-object v4, p0, Lcom/jme3/util/IntMap;->table:[Lcom/jme3/util/IntMap$Entry;

    aput-object v2, v4, v1

    :goto_1
    iget-object v4, v0, Lcom/jme3/util/IntMap$Entry;->value:Ljava/lang/Object;

    :goto_2
    return-object v4

    :cond_0
    iput-object v2, v3, Lcom/jme3/util/IntMap$Entry;->next:Lcom/jme3/util/IntMap$Entry;

    goto :goto_1

    :cond_1
    move-object v3, v0

    move-object v0, v2

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lcom/jme3/util/IntMap;->size:I

    return v0
.end method
