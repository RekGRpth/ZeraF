.class public final Lcom/google/api/services/plus/model/Person_OrganizationsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "Person_OrganizationsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/Person$Organizations;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/Person_OrganizationsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person_OrganizationsJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person_OrganizationsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/Person_OrganizationsJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_OrganizationsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/Person$Organizations;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "startDate"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "endDate"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "primary"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "location"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "department"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "name"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/Person_OrganizationsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/Person_OrganizationsJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_OrganizationsJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/Person$Organizations;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/Person$Organizations;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->startDate:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->endDate:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->primary:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->location:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->department:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Organizations;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/Person$Organizations;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/Person_OrganizationsJson;->getValues(Lcom/google/api/services/plus/model/Person$Organizations;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/Person$Organizations;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person$Organizations;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person$Organizations;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/Person_OrganizationsJson;->newInstance()Lcom/google/api/services/plus/model/Person$Organizations;

    move-result-object v0

    return-object v0
.end method
