.class public final Lcom/google/api/services/plus/model/AudiencesFeedJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "AudiencesFeedJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/AudiencesFeed;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/AudiencesFeedJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/AudiencesFeedJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/AudiencesFeedJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/AudiencesFeedJson;->INSTANCE:Lcom/google/api/services/plus/model/AudiencesFeedJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/AudiencesFeed;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "nextPageToken"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plus/model/AudienceJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "items"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "kind"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "etag"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "totalItems"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/AudiencesFeedJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/AudiencesFeedJson;->INSTANCE:Lcom/google/api/services/plus/model/AudiencesFeedJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/AudiencesFeed;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/AudiencesFeed;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/AudiencesFeed;->nextPageToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/AudiencesFeed;->items:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/AudiencesFeed;->kind:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/AudiencesFeed;->etag:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/AudiencesFeed;->totalItems:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/AudiencesFeed;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/AudiencesFeedJson;->getValues(Lcom/google/api/services/plus/model/AudiencesFeed;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/AudiencesFeed;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/AudiencesFeed;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/AudiencesFeed;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/AudiencesFeedJson;->newInstance()Lcom/google/api/services/plus/model/AudiencesFeed;

    move-result-object v0

    return-object v0
.end method
