.class public final Lcom/google/api/services/plus/model/ClientOzExtension;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientOzExtension.java"


# instance fields
.field public callingApplication:Ljava/lang/String;

.field public clientEvent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientOzEvent;",
            ">;"
        }
    .end annotation
.end field

.field public clientId:Ljava/lang/String;

.field public clientVersion:Ljava/lang/String;

.field public sendTimeMsec:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/services/plus/model/ClientOzEvent;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
