.class public final Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedRhsComponentTypeJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "category"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "currentlyVisible"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "id"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;->category:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;->currentlyVisible:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;->id:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;->getValues(Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentTypeJson;->newInstance()Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;

    move-result-object v0

    return-object v0
.end method
