.class Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$7;
.super Ljava/lang/Object;
.source "AddCreditCardFlowFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->displayErrors(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

.field final synthetic val$inputValidationErrors:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$7;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    iput-object p2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$7;->val$inputValidationErrors:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$7;->val$inputValidationErrors:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$InputValidationError;

    iget-object v5, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$7;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    # invokes: Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->displayError(Lcom/google/android/finsky/remoting/protos/ChallengeProtos$InputValidationError;)Landroid/widget/TextView;
    invoke-static {v5, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->access$900(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Lcom/google/android/finsky/remoting/protos/ChallengeProtos$InputValidationError;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$7;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    # getter for: Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v5}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->access$1000(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)Landroid/view/ViewGroup;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/finsky/billing/BillingUtils;->getTopMostView(Landroid/view/ViewGroup;Ljava/util/Collection;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/widget/TextView;->requestFocus()Z

    :cond_2
    return-void
.end method
