.class Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;
.super Ljava/lang/Object;
.source "CheckoutPurchaseFragment.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CommitListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$1;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;
    .param p2    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$1;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;)V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;)V
    .locals 10
    .param p1    # Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->mServerLogsCookie:Lcom/google/protobuf/micro/ByteStringMicro;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$602(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/protobuf/micro/ByteStringMicro;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    const/16 v1, 0x131

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->getCommitServerLatencyMs()J
    invoke-static {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$1600(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->getTimeElapsedSinceCommitMs()J
    invoke-static {v4}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$1700(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;)J

    move-result-wide v4

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->log(IJJ)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$900(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;IJJ)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;->getAppDeliveryData()Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v1

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->handleDeliveryData(Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$1800(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;->getLibraryUpdateList()Ljava/util/List;

    move-result-object v1

    const-string v2, "CheckoutPurchaseFragment.commit"

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->handleLibraryUpdates(Ljava/util/List;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$1900(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;->getPurchaseStatus()Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unknown purchase status: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getStatusCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;

    const/4 v2, 0x0

    invoke-direct {v1, v7, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;-><init>(ILjava/lang/String;)V

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$1202(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->setState(II)V
    invoke-static {v0, v8, v9}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$2500(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;II)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->setState(II)V
    invoke-static {v0, v8, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$2000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;II)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;->getChallenge()Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    move-result-object v1

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->mCompleteChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$2102(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;)Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    const/4 v1, 0x6

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->setState(II)V
    invoke-static {v0, v1, v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$2200(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;II)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;

    invoke-virtual {v6}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getPermissionError()I

    move-result v2

    invoke-virtual {v6}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getErrorMessageHtml()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;-><init>(ILjava/lang/String;)V

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$1202(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CheckoutPurchaseError;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->setState(II)V
    invoke-static {v0, v8, v9}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$2300(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;II)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;

    const/4 v1, 0x2

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->setState(II)V
    invoke-static {v0, v1, v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;->access$2400(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment;II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseFragment$CommitListener;->onResponse(Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;)V

    return-void
.end method
