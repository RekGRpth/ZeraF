.class public Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
.super Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;
.source "BillingProfileSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mBillingProfileResponse:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mErrorMessageHtml:Ljava/lang/String;

.field private mExtraPostParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPurchaseContextToken:Ljava/lang/String;

.field private mVolleyError:Lcom/android/volley/VolleyError;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
    .locals 3
    .param p0    # Landroid/accounts/Account;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "BillingProfileSidecar.account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "BillingProfileSidecar.purchaseContextToken"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method protected getAccount()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method protected getAuthTokenType()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/finsky/config/G;->checkoutAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getBillingProfile()Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;->getBillingProfile()Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    move-result-object v0

    return-object v0
.end method

.method public getErrorMessageHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mErrorMessageHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getVolleyError()Lcom/android/volley/VolleyError;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileSidecar.account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mAccount:Landroid/accounts/Account;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileSidecar.purchaseContextToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mPurchaseContextToken:Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onError(Lcom/android/volley/AuthFailureError;)V
    .locals 2
    .param p1    # Lcom/android/volley/AuthFailureError;

    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1    # Lcom/android/volley/VolleyError;

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    return-void
.end method

.method protected onInvalidToken()V
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;->getResult()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getAuthTokenAndContinue(Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;->getUserMessageHtml()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mErrorMessageHtml:Ljava/lang/String;

    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->onResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    if-eqz v0, :cond_0

    const-string v0, "BillingProfileSidecar.billingProfileResponse"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method protected performRequestWithToken(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mPurchaseContextToken:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mExtraPostParams:Ljava/util/Map;

    move-object v1, p1

    move-object v4, p0

    move-object v5, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->billingProfile(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "BillingProfileSidecar.billingProfileResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BillingProfileSidecar.billingProfileResponse"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    :cond_0
    return-void
.end method

.method public start(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mExtraPostParams:Ljava/util/Map;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getAuthTokenAndContinue(Z)V

    return-void
.end method
