.class public abstract Lcom/google/android/finsky/billing/InstrumentFlowFragment;
.super Lcom/google/android/finsky/billing/BillingFlowFragment;
.source "InstrumentFlowFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingFlowFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected finishWithUpdateInstrumentResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;->hasInstrumentId()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "instrument_id"

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;->getInstrumentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;->hasRedeemedOffer()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "redeemed_offer_message_html"

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;->getRedeemedOffer()Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$RedeemedPromoOffer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$RedeemedPromoOffer;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->finish(Landroid/os/Bundle;)V

    return-void
.end method
