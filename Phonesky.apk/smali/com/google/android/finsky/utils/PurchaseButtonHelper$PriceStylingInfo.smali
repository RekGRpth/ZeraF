.class public Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;
.super Ljava/lang/Object;
.source "PurchaseButtonHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/PurchaseButtonHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PriceStylingInfo"
.end annotation


# instance fields
.field public isActionEnabled:Z

.field public isOwned:Z

.field public labelActionResourceId:I

.field public labelListingResourceId:I

.field public labelOffer:Lcom/google/android/finsky/remoting/protos/Common$Offer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V

    return-void
.end method

.method private setLabels(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    iput p1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    return-void
.end method

.method private setLabels(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    iput p2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    return-void
.end method
