.class Lcom/google/android/finsky/activities/ReviewDialog$10;
.super Ljava/lang/Object;
.source "ReviewDialog.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewDialog;->launchGooglePlusSignup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewDialog;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mClient:Lcom/google/android/gms/plus/PlusClient;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1800(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/gms/plus/PlusClient;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    return-void

    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/16 v3, 0x22

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->dismiss()V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Got error sending Plus intent: %s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/IntentSender$SendIntentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialog;->showErrorToast()V
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1600(Lcom/google/android/finsky/activities/ReviewDialog;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->dismiss()V

    goto :goto_0

    :pswitch_1
    const-string v2, "Unexpected PlusClient connection response %d."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialog;->showErrorToast()V
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1600(Lcom/google/android/finsky/activities/ReviewDialog;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->dismiss()V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v1, v2, v5}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$10;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
