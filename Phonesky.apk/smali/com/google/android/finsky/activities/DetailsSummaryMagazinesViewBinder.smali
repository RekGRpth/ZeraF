.class public Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;
.super Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
.source "DetailsSummaryMagazinesViewBinder.java"


# instance fields
.field private mIssueDoc:Lcom/google/android/finsky/api/model/Document;

.field private mSubscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2    # Landroid/accounts/Account;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;-><init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V

    return-void
.end method

.method private setupSingleOfferButton(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/remoting/protos/Common$Offer;Lcom/google/android/finsky/layout/play/PlayActionButton;I)V
    .locals 9
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/finsky/api/model/Document;
    .param p3    # Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .param p4    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .param p5    # I

    const/4 v3, 0x0

    invoke-virtual {p4, v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, p5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v8

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p3}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOfferType()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mExternalReferrer:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mContinueUrl:Ljava/lang/String;

    const/16 v6, 0xc8

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p4, v8, v0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(ILandroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setupSubscriptionListOfferButton(Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/finsky/layout/play/PlayActionButton;)V
    .locals 7
    .param p1    # Landroid/accounts/Account;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Lcom/google/android/finsky/layout/play/PlayActionButton;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p3, v6}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v4, :cond_0

    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Common$Offer;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mContext:Landroid/content/Context;

    const v3, 0x7f07014d

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    new-instance v3, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;

    invoke-direct {v3, p0, p1}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;Landroid/accounts/Account;)V

    invoke-virtual {p3, v2, v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(ILandroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mContext:Landroid/content/Context;

    const v3, 0x7f07014e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public varargs bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Z
    .param p3    # [Landroid/view/View;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/16 v1, 0x11

    if-ne v0, v1, :cond_1

    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mIssueDoc:Lcom/google/android/finsky/api/model/Document;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/utils/DocUtils;->getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mSubscriptions:Ljava/util/List;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mIssueDoc:Lcom/google/android/finsky/api/model/Document;

    goto :goto_0
.end method

.method protected displayActionButtonsIfNecessary(Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/layout/play/PlayActionButton;)Z
    .locals 12
    .param p1    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .param p2    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .param p4    # Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v7, v0}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mIssueDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mIssueDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v7, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mIssueDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p0, p1, v0, v9}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->configureLaunchButton(Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mSubscriptions:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v7, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v11

    if-eqz v11, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p0, p1, v0, v11}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->configureLaunchButton(Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    const/4 v6, 0x1

    :cond_1
    :goto_1
    return v6

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mIssueDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {v0, v1, v8}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineIssueOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const v5, 0x7f07014c

    :goto_2
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mIssueDoc:Lcom/google/android/finsky/api/model/Document;

    move-object v0, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->setupSingleOfferButton(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/remoting/protos/Common$Offer;Lcom/google/android/finsky/layout/play/PlayActionButton;I)V

    const/4 v6, 0x1

    goto :goto_0

    :cond_3
    const v5, 0x7f07014b

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mSubscriptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    if-eqz v10, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mSubscriptions:Ljava/util/List;

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->setupSubscriptionListOfferButton(Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/finsky/layout/play/PlayActionButton;)V

    const/4 v6, 0x1

    goto :goto_1
.end method

.method protected setupActionButtons(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupActionButtons(Z)V

    const v1, 0x7f0800b5

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected setupCreator(Lcom/google/android/finsky/layout/DecoratedTextView;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected shouldDisplayOfferNote()Z
    .locals 4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mSubscriptions:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v0, v3}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
