.class public Lcom/google/android/finsky/activities/WebViewChallengeActivity;
.super Landroid/app/Activity;
.source "WebViewChallengeActivity.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ButtonBar$ClickListener;


# instance fields
.field private mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

.field private mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

.field private mIsFirstPageLoad:Z

.field private mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private final mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/google/android/finsky/activities/WebViewChallengeActivity$1;

    invoke-direct {v0, p0, p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity$1;-><init>(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mIsFirstPageLoad:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->cancel(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->checkUrlAndLog(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;

    iget-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->onTargetUrlMatch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mIsFirstPageLoad:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mIsFirstPageLoad:Z

    return p1
.end method

.method private cancel(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v4, 0x0

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    const-string v2, "webViewChallenge.cancel"

    invoke-interface {v1, v4, v4, v2}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v2, "webViewChallenge.cancel"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->finish()V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "webViewChallenge.cancel?host="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v4, v4, v2}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v2, "webViewChallenge.cancel"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "host"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private checkUrlAndLog(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v4, "https"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "data"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Detected non-HTTPS resource from host: %s"

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "webViewChallenge.nonHttps?host="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v7, v7, v5}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v5, "webViewChallenge.nonHttps"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "host"

    aput-object v7, v6, v2

    aput-object v0, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public static createIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/activities/WebViewChallengeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "backupTitle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "challenge"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private onTargetUrlMatch(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v3, "Matched URL: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p1, v4, v7

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "webViewChallenge.success?host="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v6, v6, v4}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v4, "webViewChallenge.success"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "host"

    aput-object v6, v5, v7

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;->getResponseTargetUrlParam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "challenge_response"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v3, -0x1

    invoke-virtual {p0, v3, v1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->finish()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->onNegativeButtonClick()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "challenge"

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    iput-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    invoke-virtual {v7}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;->hasTitle()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    invoke-virtual {v7}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;->getTitle()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0, v12}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->requestWindowFeature(I)Z

    :cond_0
    const v7, 0x7f040021

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setContentView(I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "authAccount"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/finsky/FinskyApp;->getAnalytics(Ljava/lang/String;)Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-static {v0, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-static {p0}, Lcom/google/android/finsky/layout/CustomActionBarFactory;->getInstance(Landroid/app/Activity;)Lcom/google/android/finsky/layout/CustomActionBar;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-interface {v1, v7, p0}, Lcom/google/android/finsky/layout/CustomActionBar;->initialize(Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/app/Activity;)V

    invoke-interface {v1, v11}, Lcom/google/android/finsky/layout/CustomActionBar;->updateCurrentBackendId(I)V

    const v7, 0x7f080074

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v3, v11}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonVisible(Z)V

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    invoke-virtual {v7}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;->hasCancelButtonDisplayLabel()Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    invoke-virtual {v7}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;->getCancelButtonDisplayLabel()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v3, p0}, Lcom/google/android/finsky/layout/ButtonBar;->setClickListener(Lcom/google/android/finsky/layout/ButtonBar$ClickListener;)V

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-interface {v1, v5}, Lcom/google/android/finsky/layout/CustomActionBar;->updateBreadcrumb(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    const v7, 0x7f080071

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/webkit/WebView;

    iput-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v8, Lcom/google/android/finsky/activities/WebViewChallengeActivity$2;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity$2;-><init>(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)V

    invoke-virtual {v7, v8}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v8, Lcom/google/android/finsky/activities/WebViewChallengeActivity$3;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity$3;-><init>(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)V

    invoke-virtual {v7, v8}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    if-nez p1, :cond_5

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;

    invoke-virtual {v7}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$WebViewChallenge;->getStartUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "webViewChallenge.start?host="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v10, v10, v8}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v8, "webViewChallenge.start"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const-string v10, "host"

    aput-object v10, v9, v11

    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v7, v8, v9}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "backupTitle"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    :cond_4
    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Lcom/google/android/finsky/layout/ButtonBar;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    const-string v7, "webview_state"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7, v2}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    goto :goto_2
.end method

.method public onNegativeButtonClick()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->cancel(Ljava/lang/String;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->onNegativeButtonClick()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPositiveButtonClick()V
    .locals 0

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    const-string v1, "webview_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method
