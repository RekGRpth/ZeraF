.class public Lcom/google/android/finsky/activities/SimpleAlertDialog;
.super Lvedroid/support/v4/app/DialogFragment;
.source "SimpleAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;,
        Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/SimpleAlertDialog;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-result-object v0

    return-object v0
.end method

.method private getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance(III)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {p0, v0, v1, p1, p2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private static newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 3
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v1, Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "layoutId"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "positive_id"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "negative_id"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    invoke-static {v0, p0, v0, p1, p2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public static newInstanceWithLayout(III)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-static {v0, v1, p0, p1, p2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "extra_arguments"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "target_request_code"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;->onNegativeClick(ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 15
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v13, "message_id"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v13, "message"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v13, "layoutId"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v13, "positive_id"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    const-string v13, "negative_id"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    const-string v13, "extra_arguments"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    const-string v13, "config_arguments"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    const-string v13, "target_request_code"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v13, -0x1

    if-eq v8, v13, :cond_4

    invoke-virtual {v2, v8}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    :cond_0
    :goto_0
    const/4 v13, -0x1

    if-eq v10, v13, :cond_1

    new-instance v13, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;

    invoke-direct {v13, p0, v11, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;-><init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;ILandroid/os/Bundle;)V

    invoke-virtual {v2, v10, v13}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    const/4 v13, -0x1

    if-eq v9, v13, :cond_2

    new-instance v13, Lcom/google/android/finsky/activities/SimpleAlertDialog$2;

    invoke-direct {v13, p0, v11, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$2;-><init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;ILandroid/os/Bundle;)V

    invoke-virtual {v2, v9, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_2
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v13, -0x1

    if-eq v6, v13, :cond_3

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v6, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v0, v12}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    instance-of v13, v12, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    if-eqz v13, :cond_3

    if-eqz v3, :cond_3

    check-cast v12, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    invoke-interface {v12, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;->configureView(Landroid/os/Bundle;)V

    :cond_3
    return-object v0

    :cond_4
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public setCallback(Lvedroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    if-nez p3, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_arguments"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "target_request_code"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    :cond_1
    return-object p0
.end method

.method public setViewConfiguration(Landroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_arguments"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    return-object p0
.end method
