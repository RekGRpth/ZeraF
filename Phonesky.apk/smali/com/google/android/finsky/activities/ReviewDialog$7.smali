.class Lcom/google/android/finsky/activities/ReviewDialog$7;
.super Ljava/lang/Object;
.source "ReviewDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewDialog;->confirmPlus(Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewDialog;

.field final synthetic val$analytics:Lcom/google/android/finsky/analytics/Analytics;

.field final synthetic val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/analytics/Analytics;Lcom/google/android/finsky/analytics/FinskyEventLog;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialog$7;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    iput-object p2, p0, Lcom/google/android/finsky/activities/ReviewDialog$7;->val$analytics:Lcom/google/android/finsky/analytics/Analytics;

    iput-object p3, p0, Lcom/google/android/finsky/activities/ReviewDialog$7;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$7;->val$analytics:Lcom/google/android/finsky/analytics/Analytics;

    const-string v1, "reviews.plusDeclined"

    invoke-interface {v0, v2, v2, v1}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$7;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v1, "reviews.plusDeclined"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$7;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/ReviewDialog;->dismiss()V

    return-void
.end method
