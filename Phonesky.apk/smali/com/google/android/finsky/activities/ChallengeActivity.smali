.class public abstract Lcom/google/android/finsky/activities/ChallengeActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "ChallengeActivity.java"

# interfaces
.implements Lcom/google/android/finsky/billing/BillingFlowContext;
.implements Lcom/google/android/finsky/billing/BillingFlowListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public hideFragment(Lvedroid/support/v4/app/Fragment;Z)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1}, Lvedroid/support/v4/app/FragmentTransaction;->remove(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method

.method public hideProgress()V
    .locals 0

    return-void
.end method

.method public onError(Lcom/google/android/finsky/billing/BillingFlow;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-static {p0, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/ChallengeActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->finish()V

    return-void
.end method

.method public onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2    # Z
    .param p3    # Landroid/os/Bundle;

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/ChallengeActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "challenge_response"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/ChallengeActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->finish()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public persistFragment(Landroid/os/Bundle;Ljava/lang/String;Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;
    .param p3    # Lvedroid/support/v4/app/Fragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lvedroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Lvedroid/support/v4/app/Fragment;)V

    return-void
.end method

.method public restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lvedroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public showDialogFragment(Lvedroid/support/v4/app/DialogFragment;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lvedroid/support/v4/app/DialogFragment;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->remove(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showFragment(Lvedroid/support/v4/app/Fragment;IZ)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f080041

    invoke-virtual {v0, v1, p1}, Lvedroid/support/v4/app/FragmentTransaction;->add(ILvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method public showProgress(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
