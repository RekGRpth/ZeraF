.class public Lcom/google/android/finsky/activities/ReviewDialog;
.super Lvedroid/support/v4/app/DialogFragment;
.source "ReviewDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/ReviewDialog$Listener;
    }
.end annotation


# static fields
.field private static final DESCRIPTION_MAP:[I


# instance fields
.field private mAcceptedPlus:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountName:Ljava/lang/String;

.field private mAuthorName:Landroid/widget/TextView;

.field private mClient:Lcom/google/android/gms/plus/PlusClient;

.field private mCommentView:Landroid/widget/TextView;

.field mEmptyTextWatcher:Landroid/text/TextWatcher;

.field private mPreviousReviewId:Ljava/lang/String;

.field private mProfilePic:Lcom/google/android/finsky/layout/FifeImageView;

.field private mRatingBar:Landroid/widget/RatingBar;

.field private mRequiresPlusCheck:Z

.field private mReviewBy:Landroid/widget/TextView;

.field private mReviewIsPublic:Z

.field private mReviewMode:I

.field private mTitleView:Landroid/widget/TextView;

.field private mUserProfile:Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/activities/ReviewDialog;->DESCRIPTION_MAP:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0701ba
        0x7f0701bb
        0x7f0701bc
        0x7f0701bd
        0x7f0701be
        0x7f0701bf
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewIsPublic:Z

    new-instance v0, Lcom/google/android/finsky/activities/ReviewDialog$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/ReviewDialog$1;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/ReviewDialog;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->syncOkButtonState()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/finsky/activities/ReviewDialog$Listener;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getListener()Lcom/google/android/finsky/activities/ReviewDialog$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/activities/ReviewDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mAuthorName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/finsky/layout/FifeImageView;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mProfilePic:Lcom/google/android/finsky/layout/FifeImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/activities/ReviewDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewBy:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mAcceptedPlus:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;
    .param p1    # Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/ReviewDialog;->confirmPlus(Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;)V

    return-void
.end method

.method static synthetic access$1502(Lcom/google/android/finsky/activities/ReviewDialog;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/google/android/finsky/activities/ReviewDialog;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->showErrorToast()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/finsky/activities/ReviewDialog;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->launchGooglePlusSignup()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/gms/plus/PlusClient;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mClient:Lcom/google/android/gms/plus/PlusClient;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/ReviewDialog;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->isDeletingReview()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/ReviewDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mPreviousReviewId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/ReviewDialog;)I
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserRating()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/ReviewDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/activities/ReviewDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserComment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mUserProfile:Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;)Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;
    .param p1    # Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mUserProfile:Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/finsky/activities/ReviewDialog;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewIsPublic:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/finsky/activities/ReviewDialog;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewIsPublic:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/finsky/activities/ReviewDialog;F)I
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ReviewDialog;
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/ReviewDialog;->getRatingDescription(F)I

    move-result v0

    return v0
.end method

.method private confirmPlus(Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;)V
    .locals 6
    .param p1    # Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f070199

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    if-nez p1, :cond_0

    const v3, 0x7f07019a

    :goto_0
    new-instance v5, Lcom/google/android/finsky/activities/ReviewDialog$8;

    invoke-direct {v5, p0, p1, v0, v2}, Lcom/google/android/finsky/activities/ReviewDialog$8;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;Lcom/google/android/finsky/analytics/Analytics;Lcom/google/android/finsky/analytics/FinskyEventLog;)V

    invoke-virtual {v4, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f070058

    new-instance v5, Lcom/google/android/finsky/activities/ReviewDialog$7;

    invoke-direct {v5, p0, v0, v2}, Lcom/google/android/finsky/activities/ReviewDialog$7;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/analytics/Analytics;Lcom/google/android/finsky/analytics/FinskyEventLog;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_0
    const v3, 0x7f0701da

    goto :goto_0
.end method

.method private getListener()Lcom/google/android/finsky/activities/ReviewDialog$Listener;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/finsky/activities/ReviewDialog$Listener;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/finsky/activities/ReviewDialog$Listener;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/finsky/activities/ReviewDialog$Listener;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/finsky/activities/ReviewDialog$Listener;

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getPlusProfile()V
    .locals 3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/activities/ReviewDialog$5;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/ReviewDialog$5;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;)V

    new-instance v2, Lcom/google/android/finsky/activities/ReviewDialog$6;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/activities/ReviewDialog$6;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    return-void
.end method

.method private getRatingDescription(F)I
    .locals 2
    .param p1    # F

    sget-object v0, Lcom/google/android/finsky/activities/ReviewDialog;->DESCRIPTION_MAP:[I

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method private getUserComment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mCommentView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUserRating()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v0}, Landroid/widget/RatingBar;->getRating()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private getUserTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isDeletingReview()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mPreviousReviewId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserRating()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserComment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchGooglePlusSignup()V
    .locals 8

    new-instance v0, Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mAccountName:Ljava/lang/String;

    new-instance v3, Lcom/google/android/finsky/activities/ReviewDialog$9;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/ReviewDialog$9;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;)V

    new-instance v4, Lcom/google/android/finsky/activities/ReviewDialog$10;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/ReviewDialog$10;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "https://www.googleapis.com/auth/plus.me"

    aput-object v7, v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    return-void
.end method

.method public static newInstance(ILjava/lang/String;Lcom/google/android/finsky/remoting/protos/Rev$Review;I)Lcom/google/android/finsky/activities/ReviewDialog;
    .locals 4
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/finsky/remoting/protos/Rev$Review;
    .param p3    # I

    new-instance v1, Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/ReviewDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "review_mode"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "doc_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "backend"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "previous_review_id"

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/finsky/remoting/protos/Rev$Review;->getCommentId()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const-string v2, "previous_rating"

    invoke-virtual {p2}, Lcom/google/android/finsky/remoting/protos/Rev$Review;->getStarRating()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "previous_title"

    invoke-virtual {p2}, Lcom/google/android/finsky/remoting/protos/Rev$Review;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "previous_comment"

    invoke-virtual {p2}, Lcom/google/android/finsky/remoting/protos/Rev$Review;->getComment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "previous_review_profile"

    invoke-virtual {p2}, Lcom/google/android/finsky/remoting/protos/Rev$Review;->getPlusProfile()Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/ReviewDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private showErrorToast()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f07019d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method private syncOkButtonState()V
    .locals 9

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v7, -0x1

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->isDeletingReview()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-boolean v7, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z

    if-nez v7, :cond_1

    move v2, v5

    :goto_1
    const v5, 0x7f07019c

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(I)V

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    move v2, v6

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserRating()I

    move-result v7

    if-lez v7, :cond_4

    iget-boolean v7, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z

    if-nez v7, :cond_4

    move v2, v5

    :goto_3
    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserTitle()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserComment()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iget v7, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewMode:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_6

    if-nez v4, :cond_5

    if-nez v0, :cond_5

    move v2, v5

    :cond_3
    :goto_4
    const v5, 0x7f0701da

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    :cond_4
    move v2, v6

    goto :goto_3

    :cond_5
    move v2, v6

    goto :goto_4

    :cond_6
    if-eqz v4, :cond_7

    if-eqz v0, :cond_8

    :cond_7
    move v2, v5

    :goto_5
    goto :goto_4

    :cond_8
    move v2, v6

    goto :goto_5
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 20
    .param p1    # Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v9, 0x0

    const-string v17, "review_mode"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewMode:I

    const-string v17, "doc_id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v17, "backend"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v17, "previous_review_id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mPreviousReviewId:Ljava/lang/String;

    const-string v17, "previous_review_profile"

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    if-eqz p1, :cond_1

    move-object/from16 v15, p1

    :goto_0
    const-string v17, "previous_rating"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    const-string v17, "previous_title"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v17, "previous_comment"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v6, Landroid/view/ContextThemeWrapper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v17

    const v18, 0x7f0d0053

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v6, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f040102

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    const v17, 0x7f080208

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RatingBar;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mRatingBar:Landroid/widget/RatingBar;

    const v17, 0x7f080209

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const v17, 0x7f08020a

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mTitleView:Landroid/widget/TextView;

    const v17, 0x7f08020b

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mCommentView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v17, v0

    int-to-float v0, v10

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/RatingBar;->setRating(F)V

    int-to-float v0, v10

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/finsky/activities/ReviewDialog;->getRatingDescription(F)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewMode:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mTitleView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mCommentView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    const v17, 0x7f0701b7

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v14}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/ReviewDialog;->setCancelable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mTitleView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mCommentView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v17, 0x104000a

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    new-instance v18, Lcom/google/android/finsky/activities/ReviewDialog$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v8}, Lcom/google/android/finsky/activities/ReviewDialog$2;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v17, 0x1040000

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    new-instance v18, Lcom/google/android/finsky/activities/ReviewDialog$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/activities/ReviewDialog$3;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v17, v0

    new-instance v18, Lcom/google/android/finsky/activities/ReviewDialog$4;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v13}, Lcom/google/android/finsky/activities/ReviewDialog$4;-><init>(Lcom/google/android/finsky/activities/ReviewDialog;Landroid/widget/TextView;)V

    invoke-virtual/range {v17 .. v18}, Landroid/widget/RatingBar;->setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    sget-object v17, Lcom/google/android/finsky/config/G;->enableGooglePlusReviews:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Boolean;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    if-eqz v17, :cond_3

    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewIsPublic:Z

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f040103

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    const v17, 0x7f08020e

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewBy:Landroid/widget/TextView;

    const v17, 0x7f08020f

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mAuthorName:Landroid/widget/TextView;

    const v17, 0x7f08020d

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mProfilePic:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-static {v6, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mAuthorName:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewBy:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewBy:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mReviewBy:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog;->setCustomTitle(Landroid/view/View;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mAccountName:Ljava/lang/String;

    sget-object v17, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedPlusReviews:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mAccountName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mAcceptedPlus:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z

    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mAuthorName:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual {v11}, Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mProfilePic:Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v17, v0

    invoke-virtual {v11}, Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;->getProfileImage()Lcom/google/android/finsky/remoting/protos/Doc$Image;

    move-result-object v18

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/finsky/utils/BitmapLoader;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/remoting/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getPlusProfile()V

    :goto_2
    return-object v7

    :cond_1
    move-object v15, v3

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mTitleView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/ReviewDialog;->mCommentView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_3
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z

    goto :goto_2
.end method

.method public onDestroyView()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mCommentView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewDialog;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-super {p0}, Lvedroid/support/v4/app/DialogFragment;->onDestroyView()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "previous_rating"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserRating()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "previous_title"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "previous_comment"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->getUserComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lvedroid/support/v4/app/DialogFragment;->onStart()V

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialog;->syncOkButtonState()V

    return-void
.end method
