.class Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;
.super Ljava/lang/Object;
.source "MyAppsEmptyView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView;->configure(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/activities/AuthenticatedActivity;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView;

.field final synthetic val$appCorpus:Lcom/google/android/finsky/remoting/protos/Toc$CorpusMetadata;

.field final synthetic val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field final synthetic val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/remoting/protos/Toc$CorpusMetadata;Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView;

    iput-object p2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->val$appCorpus:Lcom/google/android/finsky/remoting/protos/Toc$CorpusMetadata;

    iput-object p4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->val$appCorpus:Lcom/google/android/finsky/remoting/protos/Toc$CorpusMetadata;

    invoke-virtual {v1}, Lcom/google/android/finsky/remoting/protos/Toc$CorpusMetadata;->getLandingUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->val$appCorpus:Lcom/google/android/finsky/remoting/protos/Toc$CorpusMetadata;

    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/Toc$CorpusMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView$1;->val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    const-string v5, "myApps"

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToCorpusHome(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    return-void
.end method
