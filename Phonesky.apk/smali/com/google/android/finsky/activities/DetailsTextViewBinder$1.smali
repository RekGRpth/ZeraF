.class Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;
.super Ljava/lang/Object;
.source "DetailsTextViewBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsTextViewBinder;->bind(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsTextViewBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    # getter for: Lcom/google/android/finsky/activities/DetailsTextViewBinder;->mIsTranslated:Z
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->access$000(Lcom/google/android/finsky/activities/DetailsTextViewBinder;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    # setter for: Lcom/google/android/finsky/activities/DetailsTextViewBinder;->mIsTranslated:Z
    invoke-static {v4, v1}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->access$002(Lcom/google/android/finsky/activities/DetailsTextViewBinder;Z)Z

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    # getter for: Lcom/google/android/finsky/activities/DetailsTextViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->access$100(Lcom/google/android/finsky/activities/DetailsTextViewBinder;)Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/fragments/PageFragment;->getPageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    # getter for: Lcom/google/android/finsky/activities/DetailsTextViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->access$200(Lcom/google/android/finsky/activities/DetailsTextViewBinder;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getCookie()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    # getter for: Lcom/google/android/finsky/activities/DetailsTextViewBinder;->mIsTranslated:Z
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->access$000(Lcom/google/android/finsky/activities/DetailsTextViewBinder;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "detailsTranslate"

    :goto_1
    invoke-interface {v4, v0, v5, v1}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    # getter for: Lcom/google/android/finsky/activities/DetailsTextViewBinder;->mIsTranslated:Z
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->access$000(Lcom/google/android/finsky/activities/DetailsTextViewBinder;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "detailsTranslate"

    :goto_2
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "c"

    aput-object v6, v5, v3

    aput-object v0, v5, v2

    invoke-virtual {v4, v1, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsTextViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    # invokes: Lcom/google/android/finsky/activities/DetailsTextViewBinder;->updateDescription()V
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->access$300(Lcom/google/android/finsky/activities/DetailsTextViewBinder;)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    const-string v1, "detailsRevertTranslation"

    goto :goto_1

    :cond_2
    const-string v1, "detailsRevertTranslation"

    goto :goto_2
.end method
