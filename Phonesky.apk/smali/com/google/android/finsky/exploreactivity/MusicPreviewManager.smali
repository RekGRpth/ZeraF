.class public Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;
.super Ljava/lang/Object;
.source "MusicPreviewManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;,
        Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;,
        Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;,
        Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;
    }
.end annotation


# static fields
.field private static final BLANK_MP3_FRAME:[B


# instance fields
.field private final mCacheDir:Ljava/io/File;

.field private mCacheFiles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mCacheSizeBytes:I

.field private mDownloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

.field private final mMaxCacheSizeBytes:I

.field private final mMaxSampleBytes:I

.field private final mMinBufferSizeBytes:I

.field private final mPrecacheBytes:I

.field private mRequestQueue:Lcom/android/volley/RequestQueue;

.field private final mSampleRateKbit:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xd1

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->BLANK_MP3_FRAME:[B

    sget-object v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->BLANK_MP3_FRAME:[B

    const/4 v1, 0x0

    const/4 v2, -0x1

    aput-byte v2, v0, v1

    sget-object v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->BLANK_MP3_FRAME:[B

    const/4 v1, 0x1

    const/16 v2, -0xd

    aput-byte v2, v0, v1

    sget-object v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->BLANK_MP3_FRAME:[B

    const/4 v1, 0x2

    const/16 v2, -0x7e

    aput-byte v2, v0, v1

    sget-object v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->BLANK_MP3_FRAME:[B

    const/4 v1, 0x3

    const/16 v2, -0x3c

    aput-byte v2, v0, v1

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheFiles:Ljava/util/Map;

    new-instance v0, Lcom/android/volley/RequestQueue;

    new-instance v2, Lcom/android/volley/toolbox/NoCache;

    invoke-direct {v2}, Lcom/android/volley/toolbox/NoCache;-><init>()V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->createNetwork()Lcom/android/volley/Network;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;)V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->start()V

    sget-object v0, Lcom/google/android/finsky/config/G;->explorerPrefetchThreads:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v0, Lcom/google/android/finsky/config/G;->explorerSampleRateKbit:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mSampleRateKbit:I

    iget v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mSampleRateKbit:I

    div-int/lit8 v0, v0, 0x8

    mul-int/lit16 v8, v0, 0x400

    sget-object v0, Lcom/google/android/finsky/config/G;->explorerPrecacheSecs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/2addr v0, v8

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mPrecacheBytes:I

    sget-object v0, Lcom/google/android/finsky/config/G;->explorerMinBufferSecs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/2addr v0, v8

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMinBufferSizeBytes:I

    sget-object v0, Lcom/google/android/finsky/config/G;->explorerPreviewSecs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/2addr v0, v8

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMaxSampleBytes:I

    sget-object v0, Lcom/google/android/finsky/config/G;->explorerMaxDiskCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMaxCacheSizeBytes:I

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "music"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheDir:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    new-instance v0, Lcom/google/android/volley/GoogleHttpClient;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    const-string v3, "unused/0"

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/volley/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide v3, 0x7fffffffffffffffL

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v7, Lcom/google/android/finsky/utils/BackgroundThreadFactory;

    invoke-direct {v7}, Lcom/google/android/finsky/utils/BackgroundThreadFactory;-><init>()V

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mDownloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;
    .param p1    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->fetchUrl(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mDownloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    return-object v0
.end method

.method private deleteAll()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mDownloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$1;-><init>(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private downloadFile(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Lorg/apache/http/HttpResponse;)V
    .locals 9
    .param p1    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;
    .param p2    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x0

    const-string v5, "Downloading %d bytes for MP3 %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mSize:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$400(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getTitle()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Ljava/io/RandomAccessFile;

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mFile:Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$500(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Ljava/io/File;

    move-result-object v5

    const-string v6, "rw"

    invoke-direct {v3, v5, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mSize:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$400(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v5

    const v6, 0x7fffffff

    if-ge v5, v6, :cond_0

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mOffset:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$600(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v5

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mSize:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$400(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mOffset:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$600(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v7

    sub-int/2addr v6, v7

    invoke-direct {p0, v3, v5, v6}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->writeBlankMp3(Ljava/io/RandomAccessFile;II)V

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mOffset:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$600(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v3, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V

    :cond_0
    const/16 v5, 0x400

    new-array v0, v5, [B

    const/4 v4, 0x0

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mOffset:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$600(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v5

    if-lez v5, :cond_2

    :cond_1
    const/4 v5, 0x0

    array-length v6, v0

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mOffset:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$600(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v7

    sub-int/2addr v7, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v2, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    if-gtz v1, :cond_4

    :goto_0
    const/4 v4, 0x0

    :cond_2
    const/4 v5, 0x0

    array-length v6, v0

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mSize:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$400(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v7

    sub-int/2addr v7, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v2, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    if-gtz v1, :cond_5

    :goto_1
    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mListener:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$700(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;

    move-result-object v5

    if-eqz v5, :cond_3

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mListener:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$700(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;

    move-result-object v5

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mFile:Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$500(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Ljava/io/File;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;->fileReady(Lcom/google/android/finsky/exploreactivity/DocWrapper;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDocId()Ljava/lang/String;

    move-result-object v5

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mFile:Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$500(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Ljava/io/File;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->makeSpaceForFile(Ljava/lang/String;Ljava/io/File;)V

    return-void

    :cond_4
    add-int/2addr v4, v1

    :try_start_1
    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mOffset:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$600(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I

    move-result v5

    if-lt v4, v5, :cond_1

    goto :goto_0

    :cond_5
    add-int/2addr v4, v1

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/RandomAccessFile;->write([BII)V

    iget v5, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMinBufferSizeBytes:I

    if-lt v4, v5, :cond_6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mListener:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$700(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;

    move-result-object v5

    if-eqz v5, :cond_6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mListener:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$700(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;

    move-result-object v5

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mFile:Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$500(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Ljava/io/File;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;->fileReady(Lcom/google/android/finsky/exploreactivity/DocWrapper;Ljava/io/File;)V

    const/4 v5, 0x0

    # setter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mListener:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;
    invoke-static {p1, v5}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$702(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;)Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;

    :cond_6
    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mSize:I
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$400(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-lt v4, v5, :cond_2

    goto :goto_1

    :catchall_0
    move-exception v5

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDocId()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mFile:Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$500(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Ljava/io/File;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->makeSpaceForFile(Ljava/lang/String;Ljava/io/File;)V

    throw v5
.end method

.method private fetchUrl(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)V
    .locals 12
    .param p1    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$300(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/google/android/volley/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v5, 0xc8

    if-ne v3, v5, :cond_1

    invoke-direct {p0, p1, v2}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->downloadFile(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Lorg/apache/http/HttpResponse;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v5, "Location"

    invoke-interface {v2, v5}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v1

    if-eqz v1, :cond_2

    array-length v5, v1

    if-eqz v5, :cond_2

    array-length v5, v1

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v1, v5

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$300(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    # setter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mUrl:Ljava/lang/String;
    invoke-static {p1, v4}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$302(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->fetchUrl(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, "Unable to download song sample %s: %s"

    new-array v6, v11, [Ljava/lang/Object;

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDocId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v5, "Unable to download song sample %s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {p1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDocId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private makeSpaceForFile(Ljava/lang/String;Ljava/io/File;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v3

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheFiles:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;

    if-eqz v0, :cond_1

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    int-to-long v6, v6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->mSize:J
    invoke-static {v0}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->access$800(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;)J

    move-result-wide v8

    sub-long v8, v3, v8

    add-long/2addr v6, v8

    long-to-int v6, v6

    iput v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    # setter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->mSize:J
    invoke-static {v0, v3, v4}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->access$802(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;J)J

    :goto_0
    iget v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMaxCacheSizeBytes:I

    if-gt v6, v7, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheFiles:Ljava/util/Map;

    new-instance v7, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;

    invoke-direct {v7, p1, p2, v3, v4}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;-><init>(Ljava/lang/String;Ljava/io/File;J)V

    invoke-interface {v6, p1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    int-to-long v6, v6

    add-long/2addr v6, v3

    long-to-int v6, v6

    iput v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheFiles:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    int-to-long v6, v6

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->mSize:J
    invoke-static {v1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->access$800(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    long-to-int v6, v6

    iput v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->mFile:Ljava/io/File;
    invoke-static {v1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->access$900(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheFiles:Ljava/util/Map;

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->mDocId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;->access$1000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$CacheFileEntry;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheSizeBytes:I

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMaxCacheSizeBytes:I

    if-gt v6, v7, :cond_3

    goto :goto_1
.end method

.method private replaceUrlParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=[^&]*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private writeBlankMp3(Ljava/io/File;II)V
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # I
    .param p3    # I

    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "rw"

    invoke-direct {v1, p1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v1, p2, p3}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->writeBlankMp3(Ljava/io/RandomAccessFile;II)V

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private writeBlankMp3(Ljava/io/RandomAccessFile;II)V
    .locals 5
    .param p1    # Ljava/io/RandomAccessFile;
    .param p2    # I
    .param p3    # I

    int-to-long v3, p2

    :try_start_0
    invoke-virtual {p1, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    const/4 v0, 0x0

    sget-object v3, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->BLANK_MP3_FRAME:[B

    array-length v2, v3

    :goto_0
    add-int v3, v0, v2

    if-gt v3, p3, :cond_0

    sget-object v3, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->BLANK_MP3_FRAME:[B

    invoke-virtual {p1, v3}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public cancel(Lcom/google/android/finsky/exploreactivity/DocWrapper;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocWrapper;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2, p1}, Lcom/android/volley/RequestQueue;->cancelAll(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mDownloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v2}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    instance-of v2, v1, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;

    # getter for: Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->mWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;
    invoke-static {v1}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;->access$000(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v2

    if-ne v2, p1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mDownloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    invoke-direct {p0}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->deleteAll()V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheFiles:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public destroy()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->deleteAll()V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mDownloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheFiles:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->stop()V

    return-void
.end method

.method public fetchPreview(Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;Z)V
    .locals 20
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocWrapper;
    .param p2    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;
    .param p3    # Z

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getSong()Lcom/google/android/finsky/api/model/Document;

    move-result-object v16

    if-nez p3, :cond_0

    const/16 p2, 0x0

    :cond_0
    if-eqz p3, :cond_2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMaxSampleBytes:I

    :goto_0
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mCacheDir:Ljava/io/File;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v17

    int-to-long v3, v15

    cmp-long v3, v17, v3

    if-ltz v3, :cond_3

    if-eqz p2, :cond_1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;->fileReady(Lcom/google/android/finsky/exploreactivity/DocWrapper;Ljava/io/File;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mPrecacheBytes:I

    goto :goto_0

    :cond_3
    const-string v3, "Inflating MP3 for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move-wide/from16 v0, v17

    long-to-int v8, v0

    sub-int v3, v15, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8, v3}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->writeBlankMp3(Ljava/io/File;II)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mMinBufferSizeBytes:I

    int-to-long v3, v3

    cmp-long v3, v17, v3

    if-lez v3, :cond_4

    if-eqz p2, :cond_4

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;->fileReady(Lcom/google/android/finsky/exploreactivity/DocWrapper;Ljava/io/File;)V

    const/16 p2, 0x0

    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;->hasPreviewUrl()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "Getting Skyjam preview URL for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;->getPreviewUrl()Ljava/lang/String;

    move-result-object v12

    const-string v3, "mode"

    const-string v4, "streaming"

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v3, v4}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->replaceUrlParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v3, "targetkbps"

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mSampleRateKbit:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v3, v4}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->replaceUrlParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    new-instance v2, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;

    const/4 v5, 0x0

    sub-int v9, v15, v8

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v9}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;-><init>(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/google/android/finsky/exploreactivity/DocWrapper;Ljava/lang/String;Ljava/io/File;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;II)V

    new-instance v9, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;

    if-eqz p3, :cond_5

    sget-object v13, Lcom/android/volley/Request$Priority;->IMMEDIATE:Lcom/android/volley/Request$Priority;

    :goto_2
    const/4 v14, 0x0

    move-object/from16 v10, p0

    move-object v11, v2

    invoke-direct/range {v9 .. v14}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;-><init>(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Ljava/lang/String;Lcom/android/volley/Request$Priority;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$1;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v3, v9}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/volley/Request;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_5
    sget-object v13, Lcom/android/volley/Request$Priority;->NORMAL:Lcom/android/volley/Request$Priority;

    goto :goto_2
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1    # Lcom/android/volley/VolleyError;

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
