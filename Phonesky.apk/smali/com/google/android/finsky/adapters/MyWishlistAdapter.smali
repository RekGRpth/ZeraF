.class public Lcom/google/android/finsky/adapters/MyWishlistAdapter;
.super Lcom/google/android/finsky/adapters/CardSimpleListAdapter;
.source "MyWishlistAdapter.java"

# interfaces
.implements Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;


# instance fields
.field private mDismissedDocIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/BucketedList;ILjava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p4    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6    # I
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/finsky/navigationmanager/NavigationManager;",
            "Lcom/google/android/finsky/utils/BitmapLoader;",
            "Lcom/google/android/finsky/api/model/DfeToc;",
            "Lcom/google/android/finsky/api/model/BucketedList",
            "<*>;I",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/BucketedList;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mDismissedDocIds:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method protected bindDocumentToCard(Lcom/google/android/finsky/api/model/Bucket;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/layout/play/PlayCardView;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 8
    .param p1    # Lcom/google/android/finsky/api/model/Bucket;
    .param p2    # Lcom/google/android/finsky/api/model/Document;
    .param p3    # I
    .param p4    # Lcom/google/android/finsky/layout/play/PlayCardView;
    .param p5    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iget-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    iget-object v2, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mCurrentPageUrl:Ljava/lang/String;

    move-object v0, p4

    move-object v1, p2

    move-object v6, p0

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindInList(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method public onDataChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->onDataChanged()V

    return-void
.end method

.method public onWishlistStatusChanged(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    if-nez p3, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
