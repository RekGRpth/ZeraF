.class public Lcom/google/android/finsky/adapters/SearchAdapter;
.super Lcom/google/android/finsky/adapters/CardSimpleListAdapter;
.source "SearchAdapter.java"


# instance fields
.field private mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

.field private mSpinner:Landroid/widget/Spinner;

.field private mSpinnerAdapter:Lcom/google/android/finsky/adapters/SearchSpinnerAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/DfeSearch;ILjava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p4    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5    # Lcom/google/android/finsky/api/model/DfeSearch;
    .param p6    # I
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/BucketedList;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object p5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/SearchAdapter;)Lcom/google/android/finsky/api/model/DfeSearch;
    .locals 1
    .param p0    # Lcom/google/android/finsky/adapters/SearchAdapter;

    iget-object v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/adapters/SearchAdapter;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/google/android/finsky/adapters/SearchAdapter;

    iget-object v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method private bindSuggestionHeader(Lcom/google/android/finsky/layout/SuggestionBarLayout;)V
    .locals 5
    .param p1    # Lcom/google/android/finsky/layout/SuggestionBarLayout;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeSearch;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->bind(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/finsky/adapters/SearchAdapter;->makeSuggestionClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v0, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setClickable(Z)V

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v3}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setFocusable(Z)V

    return-void

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method private getSuggestionHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/ViewGroup;

    if-nez p1, :cond_0

    const v1, 0x7f040111

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/SearchAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    move-object v1, v0

    check-cast v1, Lcom/google/android/finsky/layout/SuggestionBarLayout;

    invoke-direct {p0, v1}, Lcom/google/android/finsky/adapters/SearchAdapter;->bindSuggestionHeader(Lcom/google/android/finsky/layout/SuggestionBarLayout;)V

    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private getSwitcherHeader(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/ViewGroup;

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeSearch;->getBackendId()I

    move-result v0

    if-nez p1, :cond_0

    const v5, 0x7f0400c9

    invoke-virtual {p0, v5, p2, v7}, Lcom/google/android/finsky/adapters/SearchAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinnerAdapter:Lcom/google/android/finsky/adapters/SearchSpinnerAdapter;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeSearch;->getRelatedSearches()Ljava/util/List;

    move-result-object v3

    new-instance v5, Lcom/google/android/finsky/adapters/SearchSpinnerAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6, v3}, Lcom/google/android/finsky/adapters/SearchSpinnerAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinnerAdapter:Lcom/google/android/finsky/adapters/SearchSpinnerAdapter;

    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    if-nez v5, :cond_3

    const v5, 0x7f0801b1

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    iput-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v5, v7}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinnerAdapter:Lcom/google/android/finsky/adapters/SearchSpinnerAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeSearch;->getRelatedSearches()Ljava/util/List;

    move-result-object v4

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_2

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/remoting/protos/Search$RelatedSearch;

    invoke-virtual {v5}, Lcom/google/android/finsky/remoting/protos/Search$RelatedSearch;->getCurrent()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v5, v2}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    invoke-static {v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusSpinnerDrawable(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/google/android/finsky/adapters/SearchAdapter$2;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/adapters/SearchAdapter$2;-><init>(Lcom/google/android/finsky/adapters/SearchAdapter;)V

    invoke-virtual {v5, v6}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v5, 0x7f0801b2

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_3
    return-object p1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private makeSuggestionClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/finsky/adapters/SearchAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/adapters/SearchAdapter$3;-><init>(Lcom/google/android/finsky/adapters/SearchAdapter;Ljava/lang/String;)V

    return-object v0
.end method

.method private shouldShowSuggestionHeader()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeSearch;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowSwitcher()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->getBucketCount()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->getBackendId()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getCount()I

    move-result v3

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSwitcher()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSuggestionHeader()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 6
    .param p1    # I

    const/4 v4, 0x7

    const/4 v1, 0x6

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSwitcher()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSuggestionHeader()Z

    move-result v5

    if-eqz v5, :cond_2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-ne p1, v2, :cond_3

    move v1, v4

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSuggestionHeader()Z

    move-result v5

    if-eqz v5, :cond_4

    if-eqz p1, :cond_0

    :cond_3
    move v0, p1

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSwitcher()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_1
    sub-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSuggestionHeader()Z

    move-result v1

    if-eqz v1, :cond_6

    :goto_2
    sub-int/2addr v0, v2

    invoke-super {p0, v0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getItemViewType(I)I

    move-result v1

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSwitcher()Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez p1, :cond_3

    move v1, v4

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/SearchAdapter;->getItemViewType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v0, p1

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSwitcher()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    sub-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSuggestionHeader()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    sub-int/2addr v0, v2

    if-ltz v0, :cond_2

    invoke-super {p0, v0, p2, p3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_2
    return-object v1

    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/SearchAdapter;->getSuggestionHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_2

    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/SearchAdapter;->getSwitcherHeader(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_2

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uncaught view type=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/SearchAdapter;->getItemViewType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] position=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public isEmpty()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->getCount()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSwitcher()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    sub-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/SearchAdapter;->shouldShowSuggestionHeader()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    sub-int/2addr v0, v1

    if-nez v0, :cond_2

    :goto_2
    return v2

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method protected makeHeaderClickListener(Lcom/google/android/finsky/api/model/Bucket;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1    # Lcom/google/android/finsky/api/model/Bucket;
    .param p2    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getBrowseUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/finsky/adapters/SearchAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/adapters/SearchAdapter$1;-><init>(Lcom/google/android/finsky/adapters/SearchAdapter;Lcom/google/android/finsky/api/model/Bucket;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mSpinner:Landroid/widget/Spinner;

    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->onDestroyView()V

    return-void
.end method

.method public updateAdapterData(Lcom/google/android/finsky/api/model/BucketedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/BucketedList",
            "<*>;)V"
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/BucketedList;)V

    check-cast p1, Lcom/google/android/finsky/api/model/DfeSearch;

    iput-object p1, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    return-void
.end method
