.class public Lcom/google/android/finsky/analytics/FinskyEventLog;
.super Ljava/lang/Object;
.source "FinskyEventLog.java"


# static fields
.field private static final EVENTLOG_TAG_BACKGROUND_ACTION:Ljava/lang/String;

.field private static final EVENTLOG_TAG_CLICK:Ljava/lang/String;

.field private static final EVENTLOG_TAG_DEEPLINK:Ljava/lang/String;

.field private static final EVENTLOG_TAG_IMPRESSION:Ljava/lang/String;

.field private static final EVENTLOG_TAG_SEARCH:Ljava/lang/String;

.field private static final EVENT_TAGS_WHITELIST:Ljava/lang/String;

.field private static sInitializedImpressionId:Z

.field private static sNextImpressionId:J


# instance fields
.field private final mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

.field private final mEventLogger:Lcom/google/android/play/analytics/EventLogger;

.field private final mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

.field private final mTagWhiteList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/finsky/config/G;->eventTagsWhitelist:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENT_TAGS_WHITELIST:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sInitializedImpressionId:Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_IMPRESSION:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_CLICK:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_BACKGROUND_ACTION:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_SEARCH:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_DEEPLINK:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mTagWhiteList:Ljava/util/HashSet;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/finsky/config/G;->enablePlayLogs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v10, Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-direct {v10}, Lcom/google/android/play/analytics/EventLogger$Configuration;-><init>()V

    sget-object v1, Lcom/google/android/finsky/config/G;->playLogNumberOfFiles:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->numberOfFiles:I

    sget-object v1, Lcom/google/android/finsky/config/G;->playLogMaxStorageSize:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    sget-object v1, Lcom/google/android/finsky/config/G;->playLogRecommendedFileSize:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    sget-object v1, Lcom/google/android/finsky/config/G;->playLogDelayBetweenUploadsMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    sget-object v1, Lcom/google/android/finsky/config/G;->playLogMinDelayBetweenUploadsMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    const-string v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/telephony/TelephonyManager;

    invoke-virtual {v13}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v9

    new-instance v0, Lcom/google/android/play/analytics/EventLogger;

    const/4 v2, 0x0

    sget-object v4, Lcom/google/android/play/analytics/EventLogger$LogSource;->MARKET:Lcom/google/android/play/analytics/EventLogger$LogSource;

    const/4 v5, 0x0

    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getVersionCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    move-object v1, p1

    move-object/from16 v3, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/play/analytics/EventLogger;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;)V

    :cond_0
    iput-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    new-instance v1, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    sget-object v1, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENT_TAGS_WHITELIST:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENT_TAGS_WHITELIST:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const/4 v11, 0x0

    :goto_0
    array-length v1, v12

    if-ge v11, v1, :cond_2

    aget-object v1, v12, v11

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mTagWhiteList:Ljava/util/HashSet;

    aget-object v2, v12, v11

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic access$000(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p0    # J
    .param p2    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method private static addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V
    .locals 5
    .param p0    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    :goto_0
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v2, "Unexpected null PlayStoreUiElement from node %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->cloneElement(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    invoke-virtual {p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;->addElementPath(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object p0

    goto :goto_0
.end method

.method public static childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p0    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {p1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "childNode has null element"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-static {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->findOrAddChild(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method private static cloneElement(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V
    .locals 1
    .param p0    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getType()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasClientLogsCookie()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getClientLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setClientLogsCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_1
    return-void
.end method

.method private static deepCloneAndWipeChildren(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V
    .locals 4
    .param p0    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->cloneElement(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->deepCloneAndWipeChildren(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    invoke-virtual {p1, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->addChild(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clearChild()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public static findOrAddChild(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Z
    .locals 5
    .param p0    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v0, 0x0

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {p1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->isEqual(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {v3, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->addChild(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_2
    return v0
.end method

.method public static getNextImpressionId()J
    .locals 6

    const-wide/16 v4, 0x1

    sget-boolean v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sInitializedImpressionId:Z

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sInitializedImpressionId:Z

    :cond_0
    sget-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    add-long/2addr v0, v4

    sput-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    sget-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    add-long/2addr v0, v4

    sput-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    :cond_1
    sget-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    return-wide v0
.end method

.method public static isEqual(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Z
    .locals 6
    .param p0    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getType()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getType()I

    move-result v5

    if-eq v4, v5, :cond_4

    move v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie()Z

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie()Z

    move-result v5

    if-eq v4, v5, :cond_5

    move v2, v3

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/micro/ByteStringMicro;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method protected static pathToTree(Ljava/util/List;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;",
            ">;)",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_0

    invoke-virtual {v1, v4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->addChild(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_0
    move-object v4, v1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v5, "Skipping invalid PlayStoreUiElement."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method public static removeChild(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Z
    .locals 3
    .param p0    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v2, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->isEqual(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static requestImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_0
    return-void
.end method

.method public static requestImpressions(Landroid/view/ViewGroup;)V
    .locals 0
    .param p0    # Landroid/view/ViewGroup;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressionsImpl(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method

.method private static requestImpressionsImpl(Landroid/view/ViewGroup;)V
    .locals 4
    .param p0    # Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressionsImpl(Landroid/view/ViewGroup;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    instance-of v3, p0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-eqz v3, :cond_3

    check-cast p0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_1
.end method

.method public static rootImpression(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p0    # Landroid/os/Handler;
    .param p1    # J
    .param p3    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-eqz p4, :cond_0

    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null child node or element"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->rootImpressionImpl(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method private static rootImpressionImpl(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 5
    .param p0    # Landroid/os/Handler;
    .param p1    # J
    .param p3    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {p3}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    if-eqz p4, :cond_0

    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->findOrAddChild(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Z

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    new-instance v2, Lcom/google/android/finsky/analytics/FinskyEventLog$1;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/finsky/analytics/FinskyEventLog$1;-><init>(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    sget-object v1, Lcom/google/android/finsky/config/G;->playLogImpressionSettleTimeMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private sendBackgroundEventToSinks(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V
    .locals 5
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setBackgroundAction(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_BACKGROUND_ACTION:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_BACKGROUND_ACTION:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static sendImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 5
    .param p0    # J
    .param p2    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreImpressionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v1

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->deepCloneAndWipeChildren(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setTree(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    const-wide/16 v3, 0x0

    cmp-long v3, p0, v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setId(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logImpressionEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)V

    return-void
.end method

.method public static setServerLogCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 0
    .param p0    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_0
    return-object p0
.end method

.method public static startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p0    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    move-object p0, v0

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    goto :goto_0

    :cond_0
    instance-of v1, p0, Lcom/google/android/finsky/layout/play/RootUiElementNode;

    if-eqz v1, :cond_1

    check-cast p0, Lcom/google/android/finsky/layout/play/RootUiElementNode;

    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/RootUiElementNode;->startNewImpression()V

    :cond_1
    return-void
.end method


# virtual methods
.method public logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/play/analytics/PlayStore$AppData;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/play/analytics/PlayStore$AppData;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setDocument(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setReason(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {v0, p4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setErrorCode(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_2
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0, p5}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setExceptionType(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_3
    if-eqz p6, :cond_4

    invoke-virtual {v0, p6}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setAppData(Lcom/google/android/play/analytics/PlayStore$AppData;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    return-void
.end method

.method public logClick(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mTagWhiteList:Ljava/util/HashSet;

    const-string v1, "c"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    const-string v1, "c"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "cidi"

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    const-string v3, "c"

    aput-object v3, v2, v7

    aput-object p2, v2, v8

    const-string v3, "d"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    aput-object p3, v2, v3

    const/4 v3, 0x6

    const-string v4, "lc"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p4, v2, v3

    const/16 v3, 0x8

    const-string v4, "lu"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    aput-object p5, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    const-string v1, "c"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "cidi"

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    const-string v3, "c"

    aput-object v3, v2, v7

    aput-object p2, v2, v8

    const-string v3, "d"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    aput-object p3, v2, v3

    const/4 v3, 0x6

    const-string v4, "lc"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p4, v2, v3

    const/16 v3, 0x8

    const-string v4, "lu"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    aput-object p5, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/protobuf/micro/ByteStringMicro;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreClickEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    new-instance v2, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    invoke-virtual {v2, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {v1, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;->addElementPath(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    if-eqz p3, :cond_1

    invoke-static {p3, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V

    return-void
.end method

.method public logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreClickEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V

    goto :goto_0
.end method

.method public logClickEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V
    .locals 5
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setClick(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_CLICK:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_CLICK:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logClickEventWithClientCookie(ILcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreClickEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    new-instance v2, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    invoke-virtual {v2, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {v1, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setClientLogsCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;->addElementPath(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    if-eqz p3, :cond_1

    invoke-static {p3, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V

    return-void
.end method

.method public logDeepLink(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mTagWhiteList:Ljava/util/HashSet;

    const-string v1, "deepLink"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    const-string v1, "deepLink"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "c"

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    const-string v3, "d"

    aput-object v3, v2, v7

    aput-object p2, v2, v8

    const-string v3, "isDirectPurchase"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "isRedeemGiftCardLink"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "isUnmatchedUrl"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    const-string v1, "deepLink"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "c"

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    const-string v3, "d"

    aput-object v3, v2, v7

    aput-object p2, v2, v8

    const-string v3, "isDirectPurchase"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "isRedeemGiftCardLink"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "isUnmatchedUrl"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logDeepLinkEvent(ILjava/lang/String;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-direct {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;->setResolvedType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;->setExternalUrl(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v2}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setDeepLink(Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    sget-object v3, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_DEEPLINK:Ljava/lang/String;

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    sget-object v3, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_DEEPLINK:Ljava/lang/String;

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logImpressionEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)V
    .locals 5
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setImpression(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_IMPRESSION:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_IMPRESSION:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1    # J
    .param p3    # I
    .param p4    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreImpressionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setId(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    if-eqz p4, :cond_1

    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object p4

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->pathToTree(Ljava/util/List;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setTree(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logImpressionEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)V

    return-void

    :cond_2
    const-string v4, "Encountered empty tree."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1    # J
    .param p3    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreImpressionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setId(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    :cond_0
    move-object v1, p3

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->pathToTree(Ljava/util/List;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setTree(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logImpressionEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)V

    return-void

    :cond_2
    const-string v4, "Encountered empty tree."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;Lcom/google/protobuf/micro/ByteStringMicro;JJ)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/protobuf/micro/ByteStringMicro;
    .param p6    # J
    .param p8    # J

    const-wide/16 v2, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    if-eqz p5, :cond_0

    invoke-virtual {v0, p5}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setDocument(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, p4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setExceptionType(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_2
    invoke-virtual {v0, p3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setOfferType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    cmp-long v1, p6, v2

    if-ltz v1, :cond_3

    invoke-virtual {v0, p6, p7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setServerLatencyMs(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_3
    cmp-long v1, p8, v2

    if-ltz v1, :cond_4

    invoke-virtual {v0, p8, p9}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setClientLatencyMs(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    return-void
.end method

.method public logSearchEvent(Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;)V
    .locals 5
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setSearch(Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_SEARCH:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    sget-object v2, Lcom/google/android/finsky/analytics/FinskyEventLog;->EVENTLOG_TAG_SEARCH:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logSessionData(Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;)V
    .locals 2
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setSessionInfo(Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    return-void
.end method

.method public logSettingsBackgroundEvent(IILjava/lang/Integer;Ljava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/Integer;
    .param p4    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {v0, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setToSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setFromSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setReason(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    return-void
.end method

.method public logSettingsForPackageEvent(IILjava/lang/Integer;Ljava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/Integer;
    .param p4    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {v0, p2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setToSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setFromSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    :cond_0
    invoke-virtual {v0, p4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setDocument(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    return-void
.end method

.method public varargs logTag(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mTagWhiteList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mTagWhiteList:Ljava/util/HashSet;

    const-string v1, "v"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mAnalyticsStreamWriter:Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;

    const-string v1, "v"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "c"

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    const-string v3, "lc"

    aput-object v3, v2, v6

    aput-object p2, v2, v7

    const-string v3, "lu"

    aput-object v3, v2, v8

    const/4 v3, 0x5

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/AnalyticsStreamWriter;->writeEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    const-string v1, "v"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "c"

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    const-string v3, "lc"

    aput-object v3, v2, v6

    aput-object p2, v2, v7

    const-string v3, "lu"

    aput-object v3, v2, v8

    const/4 v3, 0x5

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
