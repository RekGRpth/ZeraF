.class public final Lcom/google/android/finsky/remoting/protos/Doc$Image;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Doc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Doc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;,
        Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;
    }
.end annotation


# instance fields
.field private altTextLocalized_:Ljava/lang/String;

.field private cachedSize:I

.field private citation_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

.field private dimension_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

.field private durationSeconds_:I

.field private fillColorRgb_:Ljava/lang/String;

.field private hasAltTextLocalized:Z

.field private hasCitation:Z

.field private hasDimension:Z

.field private hasDurationSeconds:Z

.field private hasFillColorRgb:Z

.field private hasImageType:Z

.field private hasImageUrl:Z

.field private hasPositionInSequence:Z

.field private hasSecureUrl:Z

.field private hasSupportsFifeUrlOptions:Z

.field private imageType_:I

.field private imageUrl_:Ljava/lang/String;

.field private positionInSequence_:I

.field private secureUrl_:Ljava/lang/String;

.field private supportsFifeUrlOptions_:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->imageType_:I

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->positionInSequence_:I

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->dimension_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->imageUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->secureUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->altTextLocalized_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->supportsFifeUrlOptions_:Z

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->citation_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->durationSeconds_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->fillColorRgb_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAltTextLocalized()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->altTextLocalized_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->cachedSize:I

    return v0
.end method

.method public getCitation()Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->citation_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

    return-object v0
.end method

.method public getDimension()Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->dimension_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

    return-object v0
.end method

.method public getDurationSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->durationSeconds_:I

    return v0
.end method

.method public getFillColorRgb()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->fillColorRgb_:Ljava/lang/String;

    return-object v0
.end method

.method public getImageType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->imageType_:I

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->imageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getPositionInSequence()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->positionInSequence_:I

    return v0
.end method

.method public getSecureUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->secureUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getImageType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDimension()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getDimension()Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeGroupSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageUrl()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasAltTextLocalized()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getAltTextLocalized()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSecureUrl()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getSecureUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasPositionInSequence()Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getPositionInSequence()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSupportsFifeUrlOptions()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getSupportsFifeUrlOptions()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasCitation()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getCitation()Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeGroupSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDurationSeconds()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getDurationSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasFillColorRgb()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getFillColorRgb()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->cachedSize:I

    return v0
.end method

.method public getSupportsFifeUrlOptions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->supportsFifeUrlOptions_:Z

    return v0
.end method

.method public hasAltTextLocalized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasAltTextLocalized:Z

    return v0
.end method

.method public hasCitation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasCitation:Z

    return v0
.end method

.method public hasDimension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDimension:Z

    return v0
.end method

.method public hasDurationSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDurationSeconds:Z

    return v0
.end method

.method public hasFillColorRgb()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasFillColorRgb:Z

    return v0
.end method

.method public hasImageType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageType:Z

    return v0
.end method

.method public hasImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageUrl:Z

    return v0
.end method

.method public hasPositionInSequence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasPositionInSequence:Z

    return v0
.end method

.method public hasSecureUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSecureUrl:Z

    return v0
.end method

.method public hasSupportsFifeUrlOptions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSupportsFifeUrlOptions:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setImageType(I)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;-><init>()V

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readGroup(Lcom/google/protobuf/micro/MessageMicro;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setDimension(Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setImageUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setAltTextLocalized(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setSecureUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setPositionInSequence(I)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setSupportsFifeUrlOptions(Z)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;-><init>()V

    const/16 v2, 0xa

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readGroup(Lcom/google/protobuf/micro/MessageMicro;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setCitation(Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setDurationSeconds(I)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setFillColorRgb(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x13 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x53 -> :sswitch_8
        0x70 -> :sswitch_9
        0x7a -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    move-result-object v0

    return-object v0
.end method

.method public setAltTextLocalized(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasAltTextLocalized:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->altTextLocalized_:Ljava/lang/String;

    return-object p0
.end method

.method public setCitation(Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasCitation:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->citation_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

    return-object p0
.end method

.method public setDimension(Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDimension:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->dimension_:Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

    return-object p0
.end method

.method public setDurationSeconds(I)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDurationSeconds:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->durationSeconds_:I

    return-object p0
.end method

.method public setFillColorRgb(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasFillColorRgb:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->fillColorRgb_:Ljava/lang/String;

    return-object p0
.end method

.method public setImageType(I)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageType:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->imageType_:I

    return-object p0
.end method

.method public setImageUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->imageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setPositionInSequence(I)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasPositionInSequence:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->positionInSequence_:I

    return-object p0
.end method

.method public setSecureUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSecureUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->secureUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSupportsFifeUrlOptions(Z)Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSupportsFifeUrlOptions:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Doc$Image;->supportsFifeUrlOptions_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getImageType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDimension()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getDimension()Lcom/google/android/finsky/remoting/protos/Doc$Image$Dimension;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeGroup(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasImageUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasAltTextLocalized()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getAltTextLocalized()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSecureUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getSecureUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasPositionInSequence()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getPositionInSequence()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasSupportsFifeUrlOptions()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getSupportsFifeUrlOptions()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasCitation()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getCitation()Lcom/google/android/finsky/remoting/protos/Doc$Image$Citation;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeGroup(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasDurationSeconds()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getDurationSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasFillColorRgb()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getFillColorRgb()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    return-void
.end method
