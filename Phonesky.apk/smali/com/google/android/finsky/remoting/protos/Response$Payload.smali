.class public final Lcom/google/android/finsky/remoting/protos/Response$Payload;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Payload"
.end annotation


# instance fields
.field private acceptTosResponse_:Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

.field private ackNotificationResponse_:Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

.field private billingProfileResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

.field private browseResponse_:Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

.field private bulkDetailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

.field private buyResponse_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

.field private cachedSize:I

.field private checkInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

.field private checkPromoOfferResponse_:Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

.field private commitPurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

.field private consumePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

.field private deliveryResponse_:Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

.field private detailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

.field private flagContentResponse_:Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

.field private hasAcceptTosResponse:Z

.field private hasAckNotificationResponse:Z

.field private hasBillingProfileResponse:Z

.field private hasBrowseResponse:Z

.field private hasBulkDetailsResponse:Z

.field private hasBuyResponse:Z

.field private hasCheckInstrumentResponse:Z

.field private hasCheckPromoOfferResponse:Z

.field private hasCommitPurchaseResponse:Z

.field private hasConsumePurchaseResponse:Z

.field private hasDeliveryResponse:Z

.field private hasDetailsResponse:Z

.field private hasFlagContentResponse:Z

.field private hasInitiateAssociationResponse:Z

.field private hasInstrumentSetupInfoResponse:Z

.field private hasLibraryReplicationResponse:Z

.field private hasListResponse:Z

.field private hasLogResponse:Z

.field private hasModifyLibraryResponse:Z

.field private hasPlusOneResponse:Z

.field private hasPlusProfileResponse:Z

.field private hasPreparePurchaseResponse:Z

.field private hasPurchaseStatusResponse:Z

.field private hasRateSuggestedContentResponse:Z

.field private hasRedeemGiftCardResponse:Z

.field private hasResolveLinkResponse:Z

.field private hasReviewResponse:Z

.field private hasRevokeResponse:Z

.field private hasSearchResponse:Z

.field private hasTocResponse:Z

.field private hasUpdateInstrumentResponse:Z

.field private hasUploadDeviceConfigResponse:Z

.field private hasVerifyAssociationResponse:Z

.field private initiateAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

.field private instrumentSetupInfoResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

.field private libraryReplicationResponse_:Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

.field private listResponse_:Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

.field private logResponse_:Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

.field private modifyLibraryResponse_:Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

.field private plusOneResponse_:Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

.field private plusProfileResponse_:Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

.field private preparePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

.field private purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

.field private rateSuggestedContentResponse_:Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

.field private redeemGiftCardResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

.field private resolveLinkResponse_:Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

.field private reviewResponse_:Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

.field private revokeResponse_:Lcom/google/android/finsky/remoting/protos/RevokeResponse;

.field private searchResponse_:Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

.field private tocResponse_:Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

.field private updateInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

.field private uploadDeviceConfigResponse_:Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

.field private verifyAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->listResponse_:Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->detailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->reviewResponse_:Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->buyResponse_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->searchResponse_:Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->tocResponse_:Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->browseResponse_:Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->updateInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->logResponse_:Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->checkInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->plusOneResponse_:Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->flagContentResponse_:Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->ackNotificationResponse_:Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->initiateAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->verifyAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->libraryReplicationResponse_:Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->revokeResponse_:Lcom/google/android/finsky/remoting/protos/RevokeResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->bulkDetailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->resolveLinkResponse_:Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->deliveryResponse_:Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->acceptTosResponse_:Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->rateSuggestedContentResponse_:Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->checkPromoOfferResponse_:Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->instrumentSetupInfoResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->redeemGiftCardResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->modifyLibraryResponse_:Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->uploadDeviceConfigResponse_:Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->plusProfileResponse_:Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->consumePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->billingProfileResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->preparePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->commitPurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAcceptTosResponse()Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->acceptTosResponse_:Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

    return-object v0
.end method

.method public getAckNotificationResponse()Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->ackNotificationResponse_:Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

    return-object v0
.end method

.method public getBillingProfileResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->billingProfileResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    return-object v0
.end method

.method public getBrowseResponse()Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->browseResponse_:Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

    return-object v0
.end method

.method public getBulkDetailsResponse()Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->bulkDetailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

    return-object v0
.end method

.method public getBuyResponse()Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->buyResponse_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->cachedSize:I

    return v0
.end method

.method public getCheckInstrumentResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->checkInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

    return-object v0
.end method

.method public getCheckPromoOfferResponse()Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->checkPromoOfferResponse_:Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    return-object v0
.end method

.method public getCommitPurchaseResponse()Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->commitPurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    return-object v0
.end method

.method public getConsumePurchaseResponse()Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->consumePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

    return-object v0
.end method

.method public getDeliveryResponse()Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->deliveryResponse_:Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

    return-object v0
.end method

.method public getDetailsResponse()Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->detailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

    return-object v0
.end method

.method public getFlagContentResponse()Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->flagContentResponse_:Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

    return-object v0
.end method

.method public getInitiateAssociationResponse()Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->initiateAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

    return-object v0
.end method

.method public getInstrumentSetupInfoResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->instrumentSetupInfoResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

    return-object v0
.end method

.method public getLibraryReplicationResponse()Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->libraryReplicationResponse_:Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

    return-object v0
.end method

.method public getListResponse()Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->listResponse_:Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

    return-object v0
.end method

.method public getLogResponse()Lcom/google/android/finsky/remoting/protos/Log$LogResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->logResponse_:Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

    return-object v0
.end method

.method public getModifyLibraryResponse()Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->modifyLibraryResponse_:Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

    return-object v0
.end method

.method public getPlusOneResponse()Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->plusOneResponse_:Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

    return-object v0
.end method

.method public getPlusProfileResponse()Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->plusProfileResponse_:Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

    return-object v0
.end method

.method public getPreparePurchaseResponse()Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->preparePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

    return-object v0
.end method

.method public getPurchaseStatusResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    return-object v0
.end method

.method public getRateSuggestedContentResponse()Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->rateSuggestedContentResponse_:Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    return-object v0
.end method

.method public getRedeemGiftCardResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->redeemGiftCardResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

    return-object v0
.end method

.method public getResolveLinkResponse()Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->resolveLinkResponse_:Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    return-object v0
.end method

.method public getReviewResponse()Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->reviewResponse_:Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    return-object v0
.end method

.method public getRevokeResponse()Lcom/google/android/finsky/remoting/protos/RevokeResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->revokeResponse_:Lcom/google/android/finsky/remoting/protos/RevokeResponse;

    return-object v0
.end method

.method public getSearchResponse()Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->searchResponse_:Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasListResponse()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getListResponse()Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDetailsResponse()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getDetailsResponse()Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasReviewResponse()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getReviewResponse()Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBuyResponse()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBuyResponse()Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasSearchResponse()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getSearchResponse()Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasTocResponse()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getTocResponse()Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBrowseResponse()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBrowseResponse()Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPurchaseStatusResponse()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPurchaseStatusResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUpdateInstrumentResponse()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getUpdateInstrumentResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLogResponse()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getLogResponse()Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckInstrumentResponse()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getCheckInstrumentResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusOneResponse()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPlusOneResponse()Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasFlagContentResponse()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getFlagContentResponse()Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAckNotificationResponse()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getAckNotificationResponse()Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInitiateAssociationResponse()Z

    move-result v1

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getInitiateAssociationResponse()Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasVerifyAssociationResponse()Z

    move-result v1

    if-eqz v1, :cond_f

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getVerifyAssociationResponse()Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLibraryReplicationResponse()Z

    move-result v1

    if-eqz v1, :cond_10

    const/16 v1, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getLibraryReplicationResponse()Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRevokeResponse()Z

    move-result v1

    if-eqz v1, :cond_11

    const/16 v1, 0x12

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getRevokeResponse()Lcom/google/android/finsky/remoting/protos/RevokeResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBulkDetailsResponse()Z

    move-result v1

    if-eqz v1, :cond_12

    const/16 v1, 0x13

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBulkDetailsResponse()Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasResolveLinkResponse()Z

    move-result v1

    if-eqz v1, :cond_13

    const/16 v1, 0x14

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getResolveLinkResponse()Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDeliveryResponse()Z

    move-result v1

    if-eqz v1, :cond_14

    const/16 v1, 0x15

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getDeliveryResponse()Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAcceptTosResponse()Z

    move-result v1

    if-eqz v1, :cond_15

    const/16 v1, 0x16

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getAcceptTosResponse()Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRateSuggestedContentResponse()Z

    move-result v1

    if-eqz v1, :cond_16

    const/16 v1, 0x17

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getRateSuggestedContentResponse()Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckPromoOfferResponse()Z

    move-result v1

    if-eqz v1, :cond_17

    const/16 v1, 0x18

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getCheckPromoOfferResponse()Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInstrumentSetupInfoResponse()Z

    move-result v1

    if-eqz v1, :cond_18

    const/16 v1, 0x19

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getInstrumentSetupInfoResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRedeemGiftCardResponse()Z

    move-result v1

    if-eqz v1, :cond_19

    const/16 v1, 0x1a

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getRedeemGiftCardResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasModifyLibraryResponse()Z

    move-result v1

    if-eqz v1, :cond_1a

    const/16 v1, 0x1b

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getModifyLibraryResponse()Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUploadDeviceConfigResponse()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/16 v1, 0x1c

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getUploadDeviceConfigResponse()Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusProfileResponse()Z

    move-result v1

    if-eqz v1, :cond_1c

    const/16 v1, 0x1d

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPlusProfileResponse()Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasConsumePurchaseResponse()Z

    move-result v1

    if-eqz v1, :cond_1d

    const/16 v1, 0x1e

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getConsumePurchaseResponse()Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBillingProfileResponse()Z

    move-result v1

    if-eqz v1, :cond_1e

    const/16 v1, 0x1f

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBillingProfileResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPreparePurchaseResponse()Z

    move-result v1

    if-eqz v1, :cond_1f

    const/16 v1, 0x20

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPreparePurchaseResponse()Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCommitPurchaseResponse()Z

    move-result v1

    if-eqz v1, :cond_20

    const/16 v1, 0x21

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getCommitPurchaseResponse()Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_20
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->cachedSize:I

    return v0
.end method

.method public getTocResponse()Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->tocResponse_:Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    return-object v0
.end method

.method public getUpdateInstrumentResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->updateInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    return-object v0
.end method

.method public getUploadDeviceConfigResponse()Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->uploadDeviceConfigResponse_:Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    return-object v0
.end method

.method public getVerifyAssociationResponse()Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->verifyAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;

    return-object v0
.end method

.method public hasAcceptTosResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAcceptTosResponse:Z

    return v0
.end method

.method public hasAckNotificationResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAckNotificationResponse:Z

    return v0
.end method

.method public hasBillingProfileResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBillingProfileResponse:Z

    return v0
.end method

.method public hasBrowseResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBrowseResponse:Z

    return v0
.end method

.method public hasBulkDetailsResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBulkDetailsResponse:Z

    return v0
.end method

.method public hasBuyResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBuyResponse:Z

    return v0
.end method

.method public hasCheckInstrumentResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckInstrumentResponse:Z

    return v0
.end method

.method public hasCheckPromoOfferResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckPromoOfferResponse:Z

    return v0
.end method

.method public hasCommitPurchaseResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCommitPurchaseResponse:Z

    return v0
.end method

.method public hasConsumePurchaseResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasConsumePurchaseResponse:Z

    return v0
.end method

.method public hasDeliveryResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDeliveryResponse:Z

    return v0
.end method

.method public hasDetailsResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDetailsResponse:Z

    return v0
.end method

.method public hasFlagContentResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasFlagContentResponse:Z

    return v0
.end method

.method public hasInitiateAssociationResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInitiateAssociationResponse:Z

    return v0
.end method

.method public hasInstrumentSetupInfoResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInstrumentSetupInfoResponse:Z

    return v0
.end method

.method public hasLibraryReplicationResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLibraryReplicationResponse:Z

    return v0
.end method

.method public hasListResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasListResponse:Z

    return v0
.end method

.method public hasLogResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLogResponse:Z

    return v0
.end method

.method public hasModifyLibraryResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasModifyLibraryResponse:Z

    return v0
.end method

.method public hasPlusOneResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusOneResponse:Z

    return v0
.end method

.method public hasPlusProfileResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusProfileResponse:Z

    return v0
.end method

.method public hasPreparePurchaseResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPreparePurchaseResponse:Z

    return v0
.end method

.method public hasPurchaseStatusResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPurchaseStatusResponse:Z

    return v0
.end method

.method public hasRateSuggestedContentResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRateSuggestedContentResponse:Z

    return v0
.end method

.method public hasRedeemGiftCardResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRedeemGiftCardResponse:Z

    return v0
.end method

.method public hasResolveLinkResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasResolveLinkResponse:Z

    return v0
.end method

.method public hasReviewResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasReviewResponse:Z

    return v0
.end method

.method public hasRevokeResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRevokeResponse:Z

    return v0
.end method

.method public hasSearchResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasSearchResponse:Z

    return v0
.end method

.method public hasTocResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasTocResponse:Z

    return v0
.end method

.method public hasUpdateInstrumentResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUpdateInstrumentResponse:Z

    return v0
.end method

.method public hasUploadDeviceConfigResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUploadDeviceConfigResponse:Z

    return v0
.end method

.method public hasVerifyAssociationResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasVerifyAssociationResponse:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setListResponse(Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setDetailsResponse(Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setReviewResponse(Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setBuyResponse(Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setSearchResponse(Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setTocResponse(Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setBrowseResponse(Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setPurchaseStatusResponse(Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setUpdateInstrumentResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Log$LogResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setLogResponse(Lcom/google/android/finsky/remoting/protos/Log$LogResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setCheckInstrumentResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setPlusOneResponse(Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setFlagContentResponse(Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setAckNotificationResponse(Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setInitiateAssociationResponse(Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setVerifyAssociationResponse(Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setLibraryReplicationResponse(Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_12
    new-instance v1, Lcom/google/android/finsky/remoting/protos/RevokeResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/RevokeResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setRevokeResponse(Lcom/google/android/finsky/remoting/protos/RevokeResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_13
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setBulkDetailsResponse(Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_14
    new-instance v1, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setResolveLinkResponse(Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_15
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setDeliveryResponse(Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setAcceptTosResponse(Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_17
    new-instance v1, Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setRateSuggestedContentResponse(Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_18
    new-instance v1, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setCheckPromoOfferResponse(Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_19
    new-instance v1, Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setInstrumentSetupInfoResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_1a
    new-instance v1, Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setRedeemGiftCardResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_1b
    new-instance v1, Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setModifyLibraryResponse(Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_1c
    new-instance v1, Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setUploadDeviceConfigResponse(Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_1d
    new-instance v1, Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setPlusProfileResponse(Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_1e
    new-instance v1, Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setConsumePurchaseResponse(Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_1f
    new-instance v1, Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setBillingProfileResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_20
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setPreparePurchaseResponse(Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_21
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->setCommitPurchaseResponse(Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
        0xf2 -> :sswitch_1e
        0xfa -> :sswitch_1f
        0x102 -> :sswitch_20
        0x10a -> :sswitch_21
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Response$Payload;

    move-result-object v0

    return-object v0
.end method

.method public setAcceptTosResponse(Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAcceptTosResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->acceptTosResponse_:Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

    return-object p0
.end method

.method public setAckNotificationResponse(Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAckNotificationResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->ackNotificationResponse_:Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

    return-object p0
.end method

.method public setBillingProfileResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBillingProfileResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->billingProfileResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    return-object p0
.end method

.method public setBrowseResponse(Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBrowseResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->browseResponse_:Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

    return-object p0
.end method

.method public setBulkDetailsResponse(Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBulkDetailsResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->bulkDetailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

    return-object p0
.end method

.method public setBuyResponse(Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBuyResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->buyResponse_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    return-object p0
.end method

.method public setCheckInstrumentResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckInstrumentResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->checkInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

    return-object p0
.end method

.method public setCheckPromoOfferResponse(Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckPromoOfferResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->checkPromoOfferResponse_:Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    return-object p0
.end method

.method public setCommitPurchaseResponse(Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCommitPurchaseResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->commitPurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    return-object p0
.end method

.method public setConsumePurchaseResponse(Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasConsumePurchaseResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->consumePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

    return-object p0
.end method

.method public setDeliveryResponse(Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDeliveryResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->deliveryResponse_:Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

    return-object p0
.end method

.method public setDetailsResponse(Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDetailsResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->detailsResponse_:Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

    return-object p0
.end method

.method public setFlagContentResponse(Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasFlagContentResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->flagContentResponse_:Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

    return-object p0
.end method

.method public setInitiateAssociationResponse(Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInitiateAssociationResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->initiateAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

    return-object p0
.end method

.method public setInstrumentSetupInfoResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInstrumentSetupInfoResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->instrumentSetupInfoResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

    return-object p0
.end method

.method public setLibraryReplicationResponse(Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLibraryReplicationResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->libraryReplicationResponse_:Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

    return-object p0
.end method

.method public setListResponse(Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasListResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->listResponse_:Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

    return-object p0
.end method

.method public setLogResponse(Lcom/google/android/finsky/remoting/protos/Log$LogResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLogResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->logResponse_:Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

    return-object p0
.end method

.method public setModifyLibraryResponse(Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasModifyLibraryResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->modifyLibraryResponse_:Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

    return-object p0
.end method

.method public setPlusOneResponse(Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusOneResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->plusOneResponse_:Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

    return-object p0
.end method

.method public setPlusProfileResponse(Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusProfileResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->plusProfileResponse_:Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

    return-object p0
.end method

.method public setPreparePurchaseResponse(Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPreparePurchaseResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->preparePurchaseResponse_:Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

    return-object p0
.end method

.method public setPurchaseStatusResponse(Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPurchaseStatusResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    return-object p0
.end method

.method public setRateSuggestedContentResponse(Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRateSuggestedContentResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->rateSuggestedContentResponse_:Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    return-object p0
.end method

.method public setRedeemGiftCardResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRedeemGiftCardResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->redeemGiftCardResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

    return-object p0
.end method

.method public setResolveLinkResponse(Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasResolveLinkResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->resolveLinkResponse_:Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    return-object p0
.end method

.method public setReviewResponse(Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasReviewResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->reviewResponse_:Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    return-object p0
.end method

.method public setRevokeResponse(Lcom/google/android/finsky/remoting/protos/RevokeResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/RevokeResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRevokeResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->revokeResponse_:Lcom/google/android/finsky/remoting/protos/RevokeResponse;

    return-object p0
.end method

.method public setSearchResponse(Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasSearchResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->searchResponse_:Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

    return-object p0
.end method

.method public setTocResponse(Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasTocResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->tocResponse_:Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    return-object p0
.end method

.method public setUpdateInstrumentResponse(Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUpdateInstrumentResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->updateInstrumentResponse_:Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    return-object p0
.end method

.method public setUploadDeviceConfigResponse(Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUploadDeviceConfigResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->uploadDeviceConfigResponse_:Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    return-object p0
.end method

.method public setVerifyAssociationResponse(Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;)Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasVerifyAssociationResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$Payload;->verifyAssociationResponse_:Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasListResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getListResponse()Lcom/google/android/finsky/remoting/protos/DocList$ListResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDetailsResponse()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getDetailsResponse()Lcom/google/android/finsky/remoting/protos/Details$DetailsResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasReviewResponse()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getReviewResponse()Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBuyResponse()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBuyResponse()Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasSearchResponse()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getSearchResponse()Lcom/google/android/finsky/remoting/protos/Search$SearchResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasTocResponse()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getTocResponse()Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBrowseResponse()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBrowseResponse()Lcom/google/android/finsky/remoting/protos/Browse$BrowseResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPurchaseStatusResponse()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPurchaseStatusResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUpdateInstrumentResponse()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getUpdateInstrumentResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$UpdateInstrumentResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLogResponse()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getLogResponse()Lcom/google/android/finsky/remoting/protos/Log$LogResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckInstrumentResponse()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getCheckInstrumentResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$CheckInstrumentResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusOneResponse()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPlusOneResponse()Lcom/google/android/finsky/remoting/protos/PlusOne$PlusOneResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasFlagContentResponse()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getFlagContentResponse()Lcom/google/android/finsky/remoting/protos/ContentFlagging$FlagContentResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAckNotificationResponse()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getAckNotificationResponse()Lcom/google/android/finsky/remoting/protos/AckNotification$AckNotificationResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInitiateAssociationResponse()Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getInitiateAssociationResponse()Lcom/google/android/finsky/remoting/protos/CarrierBilling$InitiateAssociationResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasVerifyAssociationResponse()Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getVerifyAssociationResponse()Lcom/google/android/finsky/remoting/protos/CarrierBilling$VerifyAssociationResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasLibraryReplicationResponse()Z

    move-result v0

    if-eqz v0, :cond_10

    const/16 v0, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getLibraryReplicationResponse()Lcom/google/android/finsky/remoting/protos/LibraryReplication$LibraryReplicationResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRevokeResponse()Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0x12

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getRevokeResponse()Lcom/google/android/finsky/remoting/protos/RevokeResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBulkDetailsResponse()Z

    move-result v0

    if-eqz v0, :cond_12

    const/16 v0, 0x13

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBulkDetailsResponse()Lcom/google/android/finsky/remoting/protos/Details$BulkDetailsResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasResolveLinkResponse()Z

    move-result v0

    if-eqz v0, :cond_13

    const/16 v0, 0x14

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getResolveLinkResponse()Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasDeliveryResponse()Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0x15

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getDeliveryResponse()Lcom/google/android/finsky/remoting/protos/Delivery$DeliveryResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasAcceptTosResponse()Z

    move-result v0

    if-eqz v0, :cond_15

    const/16 v0, 0x16

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getAcceptTosResponse()Lcom/google/android/finsky/remoting/protos/Tos$AcceptTosResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_15
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRateSuggestedContentResponse()Z

    move-result v0

    if-eqz v0, :cond_16

    const/16 v0, 0x17

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getRateSuggestedContentResponse()Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_16
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCheckPromoOfferResponse()Z

    move-result v0

    if-eqz v0, :cond_17

    const/16 v0, 0x18

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getCheckPromoOfferResponse()Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_17
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasInstrumentSetupInfoResponse()Z

    move-result v0

    if-eqz v0, :cond_18

    const/16 v0, 0x19

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getInstrumentSetupInfoResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$InstrumentSetupInfoResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_18
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasRedeemGiftCardResponse()Z

    move-result v0

    if-eqz v0, :cond_19

    const/16 v0, 0x1a

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getRedeemGiftCardResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$RedeemGiftCardResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_19
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasModifyLibraryResponse()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/16 v0, 0x1b

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getModifyLibraryResponse()Lcom/google/android/finsky/remoting/protos/ModifyLibrary$ModifyLibraryResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasUploadDeviceConfigResponse()Z

    move-result v0

    if-eqz v0, :cond_1b

    const/16 v0, 0x1c

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getUploadDeviceConfigResponse()Lcom/google/android/finsky/remoting/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPlusProfileResponse()Z

    move-result v0

    if-eqz v0, :cond_1c

    const/16 v0, 0x1d

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPlusProfileResponse()Lcom/google/android/finsky/remoting/protos/PlusProfile$PlusProfileResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasConsumePurchaseResponse()Z

    move-result v0

    if-eqz v0, :cond_1d

    const/16 v0, 0x1e

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getConsumePurchaseResponse()Lcom/google/android/finsky/remoting/protos/ConsumePurchaseResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasBillingProfileResponse()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/16 v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getBillingProfileResponse()Lcom/google/android/finsky/remoting/protos/BuyInstruments$BillingProfileResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasPreparePurchaseResponse()Z

    move-result v0

    if-eqz v0, :cond_1f

    const/16 v0, 0x20

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getPreparePurchaseResponse()Lcom/google/android/finsky/remoting/protos/Purchase$PreparePurchaseResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->hasCommitPurchaseResponse()Z

    move-result v0

    if-eqz v0, :cond_20

    const/16 v0, 0x21

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$Payload;->getCommitPurchaseResponse()Lcom/google/android/finsky/remoting/protos/Purchase$CommitPurchaseResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_20
    return-void
.end method
