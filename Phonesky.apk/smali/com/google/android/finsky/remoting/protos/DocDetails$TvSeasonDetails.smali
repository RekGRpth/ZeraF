.class public final Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TvSeasonDetails"
.end annotation


# instance fields
.field private broadcaster_:Ljava/lang/String;

.field private cachedSize:I

.field private episodeCount_:I

.field private expectedEpisodeCount_:I

.field private hasBroadcaster:Z

.field private hasEpisodeCount:Z

.field private hasExpectedEpisodeCount:Z

.field private hasParentDetailsUrl:Z

.field private hasReleaseDate:Z

.field private hasSeasonIndex:Z

.field private parentDetailsUrl_:Ljava/lang/String;

.field private releaseDate_:Ljava/lang/String;

.field private seasonIndex_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->seasonIndex_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->releaseDate_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->broadcaster_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->episodeCount_:I

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBroadcaster()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->broadcaster_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->cachedSize:I

    return v0
.end method

.method public getEpisodeCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->episodeCount_:I

    return v0
.end method

.method public getExpectedEpisodeCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount_:I

    return v0
.end method

.method public getParentDetailsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getReleaseDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->releaseDate_:Ljava/lang/String;

    return-object v0
.end method

.method public getSeasonIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->seasonIndex_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getParentDetailsUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getSeasonIndex()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasReleaseDate()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getReleaseDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasBroadcaster()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getBroadcaster()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getEpisodeCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getExpectedEpisodeCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->cachedSize:I

    return v0
.end method

.method public hasBroadcaster()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    return v0
.end method

.method public hasEpisodeCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    return v0
.end method

.method public hasExpectedEpisodeCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    return v0
.end method

.method public hasParentDetailsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    return v0
.end method

.method public hasReleaseDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    return v0
.end method

.method public hasSeasonIndex()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->setParentDetailsUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->setSeasonIndex(I)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->setReleaseDate(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->setBroadcaster(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->setEpisodeCount(I)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->setExpectedEpisodeCount(I)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    move-result-object v0

    return-object v0
.end method

.method public setBroadcaster(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->broadcaster_:Ljava/lang/String;

    return-object p0
.end method

.method public setEpisodeCount(I)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->episodeCount_:I

    return-object p0
.end method

.method public setExpectedEpisodeCount(I)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount_:I

    return-object p0
.end method

.method public setParentDetailsUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setReleaseDate(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->releaseDate_:Ljava/lang/String;

    return-object p0
.end method

.method public setSeasonIndex(I)Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->seasonIndex_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getParentDetailsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getSeasonIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasReleaseDate()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getReleaseDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasBroadcaster()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getBroadcaster()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getEpisodeCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;->getExpectedEpisodeCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    return-void
.end method
