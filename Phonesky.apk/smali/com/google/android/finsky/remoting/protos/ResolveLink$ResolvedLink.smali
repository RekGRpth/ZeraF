.class public final Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ResolveLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/ResolveLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResolvedLink"
.end annotation


# instance fields
.field private browseUrl_:Ljava/lang/String;

.field private cachedSize:I

.field private detailsUrl_:Ljava/lang/String;

.field private directPurchase_:Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

.field private hasBrowseUrl:Z

.field private hasDetailsUrl:Z

.field private hasDirectPurchase:Z

.field private hasHomeUrl:Z

.field private hasRedeemGiftCard:Z

.field private hasSearchUrl:Z

.field private homeUrl_:Ljava/lang/String;

.field private redeemGiftCard_:Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

.field private searchUrl_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->detailsUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->browseUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->searchUrl_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->directPurchase_:Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->homeUrl_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->redeemGiftCard_:Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBrowseUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->browseUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->cachedSize:I

    return v0
.end method

.method public getDetailsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->detailsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getDirectPurchase()Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->directPurchase_:Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

    return-object v0
.end method

.method public getHomeUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->homeUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getRedeemGiftCard()Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->redeemGiftCard_:Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

    return-object v0
.end method

.method public getSearchUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->searchUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDetailsUrl()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getDetailsUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasBrowseUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getBrowseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasSearchUrl()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getSearchUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDirectPurchase()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getDirectPurchase()Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasHomeUrl()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getHomeUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasRedeemGiftCard()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getRedeemGiftCard()Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->cachedSize:I

    return v0
.end method

.method public hasBrowseUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasBrowseUrl:Z

    return v0
.end method

.method public hasDetailsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDetailsUrl:Z

    return v0
.end method

.method public hasDirectPurchase()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDirectPurchase:Z

    return v0
.end method

.method public hasHomeUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasHomeUrl:Z

    return v0
.end method

.method public hasRedeemGiftCard()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasRedeemGiftCard:Z

    return v0
.end method

.method public hasSearchUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasSearchUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->setDetailsUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->setBrowseUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->setSearchUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->setDirectPurchase(Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->setHomeUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->setRedeemGiftCard(Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;

    move-result-object v0

    return-object v0
.end method

.method public setBrowseUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasBrowseUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->browseUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setDetailsUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDetailsUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->detailsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setDirectPurchase(Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDirectPurchase:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->directPurchase_:Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

    return-object p0
.end method

.method public setHomeUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasHomeUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->homeUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setRedeemGiftCard(Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasRedeemGiftCard:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->redeemGiftCard_:Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

    return-object p0
.end method

.method public setSearchUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasSearchUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->searchUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDetailsUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getDetailsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasBrowseUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getBrowseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasSearchUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getSearchUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasDirectPurchase()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getDirectPurchase()Lcom/google/android/finsky/remoting/protos/ResolveLink$DirectPurchase;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasHomeUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getHomeUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->hasRedeemGiftCard()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/ResolveLink$ResolvedLink;->getRedeemGiftCard()Lcom/google/android/finsky/remoting/protos/ResolveLink$RedeemGiftCard;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    return-void
.end method
