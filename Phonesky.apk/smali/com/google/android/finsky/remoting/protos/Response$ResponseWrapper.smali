.class public final Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResponseWrapper"
.end annotation


# instance fields
.field private cachedSize:I

.field private commands_:Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

.field private hasCommands:Z

.field private hasPayload:Z

.field private hasServerMetadata:Z

.field private notification_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/Notifications$Notification;",
            ">;"
        }
    .end annotation
.end field

.field private payload_:Lcom/google/android/finsky/remoting/protos/Response$Payload;

.field private preFetch_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/Response$PreFetch;",
            ">;"
        }
    .end annotation
.end field

.field private serverMetadata_:Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->payload_:Lcom/google/android/finsky/remoting/protos/Response$Payload;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->commands_:Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->preFetch_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->notification_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->serverMetadata_:Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->cachedSize:I

    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    invoke-direct {v0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    invoke-direct {v0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    return-object v0
.end method


# virtual methods
.method public addNotification(Lcom/google/android/finsky/remoting/protos/Notifications$Notification;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->notification_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->notification_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->notification_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPreFetch(Lcom/google/android/finsky/remoting/protos/Response$PreFetch;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Response$PreFetch;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->preFetch_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->preFetch_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->preFetch_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearCommands()Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasCommands:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->commands_:Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    return-object p0
.end method

.method public clearNotification()Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->notification_:Ljava/util/List;

    return-object p0
.end method

.method public clearPreFetch()Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->preFetch_:Ljava/util/List;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->cachedSize:I

    return v0
.end method

.method public getCommands()Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->commands_:Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    return-object v0
.end method

.method public getNotificationCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->notification_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNotificationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/Notifications$Notification;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->notification_:Ljava/util/List;

    return-object v0
.end method

.method public getPayload()Lcom/google/android/finsky/remoting/protos/Response$Payload;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->payload_:Lcom/google/android/finsky/remoting/protos/Response$Payload;

    return-object v0
.end method

.method public getPreFetchCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->preFetch_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPreFetchList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/Response$PreFetch;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->preFetch_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasPayload()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getPayload()Lcom/google/android/finsky/remoting/protos/Response$Payload;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasCommands()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getCommands()Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getPreFetchList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Response$PreFetch;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getNotificationList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasServerMetadata()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getServerMetadata()Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    iput v2, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->cachedSize:I

    return v2
.end method

.method public getServerMetadata()Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->serverMetadata_:Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;

    return-object v0
.end method

.method public hasCommands()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasCommands:Z

    return v0
.end method

.method public hasPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasPayload:Z

    return v0
.end method

.method public hasServerMetadata()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasServerMetadata:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Response$Payload;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Response$Payload;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->setPayload(Lcom/google/android/finsky/remoting/protos/Response$Payload;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->setCommands(Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Response$PreFetch;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Response$PreFetch;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->addPreFetch(Lcom/google/android/finsky/remoting/protos/Response$PreFetch;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->addNotification(Lcom/google/android/finsky/remoting/protos/Notifications$Notification;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->setServerMetadata(Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;

    move-result-object v0

    return-object v0
.end method

.method public setCommands(Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasCommands:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->commands_:Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    return-object p0
.end method

.method public setPayload(Lcom/google/android/finsky/remoting/protos/Response$Payload;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Response$Payload;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasPayload:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->payload_:Lcom/google/android/finsky/remoting/protos/Response$Payload;

    return-object p0
.end method

.method public setServerMetadata(Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;)Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasServerMetadata:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->serverMetadata_:Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasPayload()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getPayload()Lcom/google/android/finsky/remoting/protos/Response$Payload;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasCommands()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getCommands()Lcom/google/android/finsky/remoting/protos/Response$ServerCommands;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getPreFetchList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Response$PreFetch;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getNotificationList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->hasServerMetadata()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Response$ResponseWrapper;->getServerMetadata()Lcom/google/android/finsky/remoting/protos/Response$ServerMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    return-void
.end method
