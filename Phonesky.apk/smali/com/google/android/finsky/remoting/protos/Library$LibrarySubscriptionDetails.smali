.class public final Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Library.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Library;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibrarySubscriptionDetails"
.end annotation


# instance fields
.field private autoRenewing_:Z

.field private cachedSize:I

.field private deprecatedValidUntilTimestampMsec_:J

.field private hasAutoRenewing:Z

.field private hasDeprecatedValidUntilTimestampMsec:Z

.field private hasInitiationTimestampMsec:Z

.field private hasSignature:Z

.field private hasSignedPurchaseData:Z

.field private hasTrialUntilTimestampMsec:Z

.field private initiationTimestampMsec_:J

.field private signature_:Ljava/lang/String;

.field private signedPurchaseData_:Ljava/lang/String;

.field private trialUntilTimestampMsec_:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->initiationTimestampMsec_:J

    iput-wide v1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec_:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->autoRenewing_:Z

    iput-wide v1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->trialUntilTimestampMsec_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->signedPurchaseData_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->signature_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAutoRenewing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->autoRenewing_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->cachedSize:I

    return v0
.end method

.method public getDeprecatedValidUntilTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec_:J

    return-wide v0
.end method

.method public getInitiationTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->initiationTimestampMsec_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasInitiationTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getInitiationTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getDeprecatedValidUntilTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasAutoRenewing()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getAutoRenewing()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getTrialUntilTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignedPurchaseData()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getSignedPurchaseData()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignature()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getSignature()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->cachedSize:I

    return v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->signature_:Ljava/lang/String;

    return-object v0
.end method

.method public getSignedPurchaseData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->signedPurchaseData_:Ljava/lang/String;

    return-object v0
.end method

.method public getTrialUntilTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->trialUntilTimestampMsec_:J

    return-wide v0
.end method

.method public hasAutoRenewing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasAutoRenewing:Z

    return v0
.end method

.method public hasDeprecatedValidUntilTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec:Z

    return v0
.end method

.method public hasInitiationTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasInitiationTimestampMsec:Z

    return v0
.end method

.method public hasSignature()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignature:Z

    return v0
.end method

.method public hasSignedPurchaseData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignedPurchaseData:Z

    return v0
.end method

.method public hasTrialUntilTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->setInitiationTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->setDeprecatedValidUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->setAutoRenewing(Z)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->setTrialUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->setSignedPurchaseData(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->setSignature(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    move-result-object v0

    return-object v0
.end method

.method public setAutoRenewing(Z)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasAutoRenewing:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->autoRenewing_:Z

    return-object p0
.end method

.method public setDeprecatedValidUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec_:J

    return-object p0
.end method

.method public setInitiationTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasInitiationTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->initiationTimestampMsec_:J

    return-object p0
.end method

.method public setSignature(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignature:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->signature_:Ljava/lang/String;

    return-object p0
.end method

.method public setSignedPurchaseData(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignedPurchaseData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->signedPurchaseData_:Ljava/lang/String;

    return-object p0
.end method

.method public setTrialUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->trialUntilTimestampMsec_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasInitiationTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getInitiationTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getDeprecatedValidUntilTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasAutoRenewing()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getAutoRenewing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getTrialUntilTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignedPurchaseData()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getSignedPurchaseData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->hasSignature()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;->getSignature()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    return-void
.end method
