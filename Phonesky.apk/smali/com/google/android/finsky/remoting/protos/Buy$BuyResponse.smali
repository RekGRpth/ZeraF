.class public final Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Buy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuyResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;
    }
.end annotation


# instance fields
.field private addInstrumentPromptHtml_:Ljava/lang/String;

.field private baseCheckoutUrl_:Ljava/lang/String;

.field private cachedSize:I

.field private challenge_:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

.field private checkoutInfo_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

.field private checkoutServiceId_:Ljava/lang/String;

.field private checkoutTokenRequired_:Z

.field private confirmButtonText_:Ljava/lang/String;

.field private continueViaUrl_:Ljava/lang/String;

.field private hasAddInstrumentPromptHtml:Z

.field private hasBaseCheckoutUrl:Z

.field private hasChallenge:Z

.field private hasCheckoutInfo:Z

.field private hasCheckoutServiceId:Z

.field private hasCheckoutTokenRequired:Z

.field private hasConfirmButtonText:Z

.field private hasContinueViaUrl:Z

.field private hasPermissionError:Z

.field private hasPermissionErrorMessageText:Z

.field private hasPermissionErrorTitleText:Z

.field private hasPurchaseCookie:Z

.field private hasPurchaseResponse:Z

.field private hasPurchaseStatusResponse:Z

.field private hasPurchaseStatusUrl:Z

.field private hasServerLogsCookie:Z

.field private permissionErrorMessageText_:Ljava/lang/String;

.field private permissionErrorTitleText_:Ljava/lang/String;

.field private permissionError_:I

.field private purchaseCookie_:Ljava/lang/String;

.field private purchaseResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

.field private purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

.field private purchaseStatusUrl_:Ljava/lang/String;

.field private serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private tosCheckboxHtml_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutInfo_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->continueViaUrl_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutTokenRequired_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutServiceId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->baseCheckoutUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseStatusUrl_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->tosCheckboxHtml_:Ljava/util/List;

    iput v2, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionError_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseCookie_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->challenge_:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->addInstrumentPromptHtml_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->confirmButtonText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionErrorTitleText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionErrorMessageText_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addTosCheckboxHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->tosCheckboxHtml_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->tosCheckboxHtml_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->tosCheckboxHtml_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAddInstrumentPromptHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->addInstrumentPromptHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseCheckoutUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->baseCheckoutUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->cachedSize:I

    return v0
.end method

.method public getChallenge()Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->challenge_:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    return-object v0
.end method

.method public getCheckoutInfo()Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutInfo_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

    return-object v0
.end method

.method public getCheckoutServiceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutServiceId_:Ljava/lang/String;

    return-object v0
.end method

.method public getCheckoutTokenRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutTokenRequired_:Z

    return v0
.end method

.method public getConfirmButtonText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->confirmButtonText_:Ljava/lang/String;

    return-object v0
.end method

.method public getContinueViaUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->continueViaUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionError()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionError_:I

    return v0
.end method

.method public getPermissionErrorMessageText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionErrorMessageText_:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionErrorTitleText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionErrorTitleText_:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseCookie()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseCookie_:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    return-object v0
.end method

.method public getPurchaseStatusResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    return-object v0
.end method

.method public getPurchaseStatusUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseStatusUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseResponse()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutInfo()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getCheckoutInfo()Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeGroupSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasContinueViaUrl()Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getContinueViaUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusUrl()Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseStatusUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutServiceId()Z

    move-result v4

    if-eqz v4, :cond_4

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getCheckoutServiceId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutTokenRequired()Z

    move-result v4

    if-eqz v4, :cond_5

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getCheckoutTokenRequired()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasBaseCheckoutUrl()Z

    move-result v4

    if-eqz v4, :cond_6

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getBaseCheckoutUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getTosCheckboxHtmlList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_7
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getTosCheckboxHtmlList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionError()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x26

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPermissionError()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusResponse()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0x27

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseStatusResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseCookie()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0x2e

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseCookie()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasChallenge()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0x31

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getChallenge()Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0x32

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getAddInstrumentPromptHtml()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasConfirmButtonText()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0x33

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getConfirmButtonText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorTitleText()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v4, 0x34

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPermissionErrorTitleText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorMessageText()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x35

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPermissionErrorMessageText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasServerLogsCookie()Z

    move-result v4

    if-eqz v4, :cond_10

    const/16 v4, 0x36

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_10
    iput v3, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->cachedSize:I

    return v3
.end method

.method public getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getTosCheckboxHtmlList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->tosCheckboxHtml_:Ljava/util/List;

    return-object v0
.end method

.method public hasAddInstrumentPromptHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml:Z

    return v0
.end method

.method public hasBaseCheckoutUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasBaseCheckoutUrl:Z

    return v0
.end method

.method public hasChallenge()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasChallenge:Z

    return v0
.end method

.method public hasCheckoutInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutInfo:Z

    return v0
.end method

.method public hasCheckoutServiceId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutServiceId:Z

    return v0
.end method

.method public hasCheckoutTokenRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutTokenRequired:Z

    return v0
.end method

.method public hasConfirmButtonText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasConfirmButtonText:Z

    return v0
.end method

.method public hasContinueViaUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasContinueViaUrl:Z

    return v0
.end method

.method public hasPermissionError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionError:Z

    return v0
.end method

.method public hasPermissionErrorMessageText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorMessageText:Z

    return v0
.end method

.method public hasPermissionErrorTitleText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorTitleText:Z

    return v0
.end method

.method public hasPurchaseCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseCookie:Z

    return v0
.end method

.method public hasPurchaseResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseResponse:Z

    return v0
.end method

.method public hasPurchaseStatusResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusResponse:Z

    return v0
.end method

.method public hasPurchaseStatusUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusUrl:Z

    return v0
.end method

.method public hasServerLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasServerLogsCookie:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setPurchaseResponse(Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;-><init>()V

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readGroup(Lcom/google/protobuf/micro/MessageMicro;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setCheckoutInfo(Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setContinueViaUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setPurchaseStatusUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setCheckoutServiceId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setCheckoutTokenRequired(Z)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setBaseCheckoutUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->addTosCheckboxHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setPermissionError(I)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setPurchaseStatusResponse(Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setPurchaseCookie(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setChallenge(Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setAddInstrumentPromptHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setConfirmButtonText(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setPermissionErrorTitleText(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setPermissionErrorMessageText(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x13 -> :sswitch_2
        0x42 -> :sswitch_3
        0x4a -> :sswitch_4
        0x62 -> :sswitch_5
        0x68 -> :sswitch_6
        0x72 -> :sswitch_7
        0x12a -> :sswitch_8
        0x130 -> :sswitch_9
        0x13a -> :sswitch_a
        0x172 -> :sswitch_b
        0x18a -> :sswitch_c
        0x192 -> :sswitch_d
        0x19a -> :sswitch_e
        0x1a2 -> :sswitch_f
        0x1aa -> :sswitch_10
        0x1b2 -> :sswitch_11
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    move-result-object v0

    return-object v0
.end method

.method public setAddInstrumentPromptHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->addInstrumentPromptHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setBaseCheckoutUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasBaseCheckoutUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->baseCheckoutUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setChallenge(Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasChallenge:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->challenge_:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    return-object p0
.end method

.method public setCheckoutInfo(Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutInfo:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutInfo_:Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

    return-object p0
.end method

.method public setCheckoutServiceId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutServiceId:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutServiceId_:Ljava/lang/String;

    return-object p0
.end method

.method public setCheckoutTokenRequired(Z)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutTokenRequired:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->checkoutTokenRequired_:Z

    return-object p0
.end method

.method public setConfirmButtonText(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasConfirmButtonText:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->confirmButtonText_:Ljava/lang/String;

    return-object p0
.end method

.method public setContinueViaUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasContinueViaUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->continueViaUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setPermissionError(I)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionError:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionError_:I

    return-object p0
.end method

.method public setPermissionErrorMessageText(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorMessageText:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionErrorMessageText_:Ljava/lang/String;

    return-object p0
.end method

.method public setPermissionErrorTitleText(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorTitleText:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->permissionErrorTitleText_:Ljava/lang/String;

    return-object p0
.end method

.method public setPurchaseCookie(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseCookie_:Ljava/lang/String;

    return-object p0
.end method

.method public setPurchaseResponse(Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    return-object p0
.end method

.method public setPurchaseStatusResponse(Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseStatusResponse_:Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    return-object p0
.end method

.method public setPurchaseStatusUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->purchaseStatusUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasServerLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseResponse()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutInfo()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getCheckoutInfo()Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse$CheckoutInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeGroup(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasContinueViaUrl()Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getContinueViaUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusUrl()Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseStatusUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutServiceId()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getCheckoutServiceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasCheckoutTokenRequired()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getCheckoutTokenRequired()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasBaseCheckoutUrl()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getBaseCheckoutUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getTosCheckboxHtmlList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0x25

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionError()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x26

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPermissionError()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusResponse()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0x27

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseStatusResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseCookie()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0x2e

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseCookie()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasChallenge()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0x31

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getChallenge()Lcom/google/android/finsky/remoting/protos/ChallengeProtos$Challenge;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0x32

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getAddInstrumentPromptHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasConfirmButtonText()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0x33

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getConfirmButtonText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorTitleText()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x34

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPermissionErrorTitleText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPermissionErrorMessageText()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x35

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPermissionErrorMessageText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasServerLogsCookie()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x36

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_10
    return-void
.end method
