.class Lcom/google/android/finsky/layout/RateReviewSection$1;
.super Ljava/lang/Object;
.source "RateReviewSection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/RateReviewSection;->configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/remoting/protos/Rev$Review;Lvedroid/support/v4/app/Fragment;Lcom/google/android/finsky/library/Libraries;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/RateReviewSection;

.field final synthetic val$document:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$fragment:Lvedroid/support/v4/app/Fragment;

.field final synthetic val$review:Lcom/google/android/finsky/remoting/protos/Rev$Review;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/RateReviewSection;Lvedroid/support/v4/app/Fragment;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/remoting/protos/Rev$Review;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    iput-object p2, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$fragment:Lvedroid/support/v4/app/Fragment;

    iput-object p3, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$document:Lcom/google/android/finsky/api/model/Document;

    iput-object p4, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$review:Lcom/google/android/finsky/remoting/protos/Rev$Review;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$fragment:Lvedroid/support/v4/app/Fragment;

    invoke-virtual {v4}, Lvedroid/support/v4/app/Fragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v4, "review_dialog"

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v4, Lcom/google/android/finsky/config/G;->enableReviewComments:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x2

    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$document:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$review:Lcom/google/android/finsky/remoting/protos/Rev$Review;

    iget-object v6, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$document:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    invoke-static {v2, v4, v5, v6}, Lcom/google/android/finsky/activities/ReviewDialog;->newInstance(ILjava/lang/String;Lcom/google/android/finsky/remoting/protos/Rev$Review;I)Lcom/google/android/finsky/activities/ReviewDialog;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/finsky/layout/RateReviewSection$1;->val$fragment:Lvedroid/support/v4/app/Fragment;

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/google/android/finsky/activities/ReviewDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    const-string v4, "review_dialog"

    invoke-virtual {v1, v0, v4}, Lcom/google/android/finsky/activities/ReviewDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
