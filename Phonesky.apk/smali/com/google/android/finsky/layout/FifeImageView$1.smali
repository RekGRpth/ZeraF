.class Lcom/google/android/finsky/layout/FifeImageView$1;
.super Lcom/google/android/finsky/layout/ThumbnailListener;
.source "FifeImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/FifeImageView;->loadImageIfNecessary()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/FifeImageView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/FifeImageView;Landroid/widget/ImageView;ZLandroid/view/View;)V
    .locals 0
    .param p2    # Landroid/widget/ImageView;
    .param p3    # Z
    .param p4    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/finsky/layout/FifeImageView$1;->this$0:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/finsky/layout/ThumbnailListener;-><init>(Landroid/widget/ImageView;ZLandroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;)V
    .locals 4
    .param p1    # Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;

    invoke-virtual {p1}, Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/FifeImageView$1;->this$0:Lcom/google/android/finsky/layout/FifeImageView;

    iget v3, v2, Lcom/google/android/finsky/layout/FifeImageView;->mRequestCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lcom/google/android/finsky/layout/FifeImageView;->mRequestCount:I

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/layout/ThumbnailListener;->onResponse(Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/FifeImageView$1;->this$0:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/FifeImageView;->setLoaded(Z)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/FifeImageView$1;->this$0:Lcom/google/android/finsky/layout/FifeImageView;

    # invokes: Lcom/google/android/finsky/layout/FifeImageView;->invokeOnLoaded()V
    invoke-static {v2}, Lcom/google/android/finsky/layout/FifeImageView;->access$000(Lcom/google/android/finsky/layout/FifeImageView;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/FifeImageView$1;->onResponse(Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;)V

    return-void
.end method
