.class Lcom/google/android/finsky/layout/ExplorePanel$2;
.super Ljava/lang/Object;
.source "ExplorePanel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/ExplorePanel;->updateButtonState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/ExplorePanel;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/ExplorePanel;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/layout/ExplorePanel$2;->this$0:Lcom/google/android/finsky/layout/ExplorePanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/ExplorePanel$2;->this$0:Lcom/google/android/finsky/layout/ExplorePanel;

    # getter for: Lcom/google/android/finsky/layout/ExplorePanel;->mFragment:Lvedroid/support/v4/app/Fragment;
    invoke-static {v1}, Lcom/google/android/finsky/layout/ExplorePanel;->access$100(Lcom/google/android/finsky/layout/ExplorePanel;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/Fragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel$2;->this$0:Lcom/google/android/finsky/layout/ExplorePanel;

    # getter for: Lcom/google/android/finsky/layout/ExplorePanel;->mDocument:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ExplorePanel;->access$200(Lcom/google/android/finsky/layout/ExplorePanel;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/ExplorePanel$2;->this$0:Lcom/google/android/finsky/layout/ExplorePanel;

    # getter for: Lcom/google/android/finsky/layout/ExplorePanel;->mPageUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/finsky/layout/ExplorePanel;->access$300(Lcom/google/android/finsky/layout/ExplorePanel;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->createIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/ExplorePanel$2;->this$0:Lcom/google/android/finsky/layout/ExplorePanel;

    # getter for: Lcom/google/android/finsky/layout/ExplorePanel;->mFragment:Lvedroid/support/v4/app/Fragment;
    invoke-static {v1}, Lcom/google/android/finsky/layout/ExplorePanel;->access$100(Lcom/google/android/finsky/layout/ExplorePanel;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/Fragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
