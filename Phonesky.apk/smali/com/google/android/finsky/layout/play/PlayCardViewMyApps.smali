.class public Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;
.super Lcom/google/android/finsky/layout/play/PlayCardView;
.source "PlayCardViewMyApps.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;
    }
.end annotation


# instance fields
.field private mArchiveView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mThumbnailAspectRatio:F

    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->onFinishInflate()V

    const v0, 0x7f0801bb

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->measureThumbnailSpanningHeight(I)V

    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;->onMeasure(II)V

    return-void
.end method

.method public setArchivable(ZLcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V
    .locals 3
    .param p1    # Z
    .param p2    # Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const v1, 0x7f080020

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0, p1}, Landroid/widget/Checkable;->setChecked(Z)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnLongClickListener;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardView;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->invalidate()V

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mAccessibilityOverlay:Landroid/view/View;

    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->toggle()V

    return-void
.end method
