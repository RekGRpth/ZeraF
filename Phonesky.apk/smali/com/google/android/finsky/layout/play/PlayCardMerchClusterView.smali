.class public Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterView;
.source "PlayCardMerchClusterView.java"


# instance fields
.field private final mContentVerticalMargin:I

.field private final mContentVerticalPadding:I

.field private mMerchFill:Landroid/view/View;

.field private mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

.field private mMerchImagePosition:I

.field private mOverlapGradient:Landroid/view/View;

.field private mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalMargin:I

    const v1, 0x7f0b0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalPadding:I

    return-void
.end method


# virtual methods
.method public configureMerch(Lcom/google/android/finsky/utils/BitmapLoader;ILcom/google/android/finsky/remoting/protos/Doc$Image;Landroid/view/View$OnClickListener;)V
    .locals 10
    .param p1    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p2    # I
    .param p3    # Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .param p4    # Landroid/view/View$OnClickListener;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz p2, :cond_0

    if-eq p2, v4, :cond_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Merch image position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not supported"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasFillColorRgb()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p3}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getFillColorRgb()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalMargin:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalMargin:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v6

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalPadding:I

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v8

    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalPadding:I

    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setPadding(IIII)V

    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImagePosition:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v3, p3, p1}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/remoting/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v3, p4}, Lcom/google/android/finsky/layout/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    if-eqz p4, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Lcom/google/android/finsky/layout/FifeImageView;->setClickable(Z)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchFill:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundColor(I)V

    if-nez p2, :cond_3

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    :goto_2
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [I

    const v7, 0xffffff

    and-int/2addr v7, v2

    aput v7, v6, v5

    aput v2, v6, v4

    invoke-direct {v3, v1, v6}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/GradientDrawable;->setDither(Z)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0a0015

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_1

    :cond_3
    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_2
.end method

.method public configureNoMerch()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v3

    invoke-virtual {v1, v2, v4, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setPadding(IIII)V

    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    const/16 v0, 0x197

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->onFinishInflate()V

    const v0, 0x7f0801b8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    const v0, 0x7f0801b7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchFill:Landroid/view/View;

    const v0, 0x7f0801b9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 26
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getWidth()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getPaddingTop()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getPaddingBottom()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v17

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v17

    move/from16 v3, v20

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v17

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getHeight()I

    move-result v24

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    sub-int v24, v24, v16

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v20

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getTop()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getBottom()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchFill:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLeft()I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getRight()I

    move-result v23

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v7, v2, v6}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredHeight()I

    move-result v12

    if-lez v14, :cond_2

    mul-int/lit8 v21, v12, 0x3

    div-int/lit8 v10, v21, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImagePosition:I

    move/from16 v21, v0

    if-nez v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLeadingGap()I

    move-result v11

    div-int/lit8 v21, v11, 0x2

    sub-int v15, v21, v10

    add-int v13, v15, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15, v7, v13, v6}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    move-object/from16 v21, v0

    sub-int v22, v13, v9

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v1, v7, v13, v6}, Landroid/view/View;->layout(IIII)V

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v9, v8}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    :goto_1
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getTrailingGap()I

    move-result v19

    div-int/lit8 v21, v19, 0x2

    sub-int v21, v20, v21

    sub-int v15, v21, v10

    add-int v18, v15, v14

    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    sub-int v21, v20, v18

    add-int v15, v15, v21

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v21, v0

    add-int v22, v15, v14

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v15, v7, v1, v6}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    move-object/from16 v21, v0

    add-int v22, v15, v9

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v15, v7, v1, v6}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v1, v7, v14, v6}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/4 v11, 0x0

    const/high16 v10, 0x40000000

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v8, p1, v11}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->measure(II)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v8, p1, v11}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->measure(II)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getPaddingBottom()I

    move-result v6

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-static {v11, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v1}, Lcom/google/android/finsky/layout/FifeImageView;->measure(II)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchFill:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredWidth()I

    move-result v9

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v1}, Landroid/view/View;->measure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    iget v10, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v9, v10

    add-int/2addr v9, v0

    iget v10, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v9, v10

    add-int/2addr v9, v6

    invoke-virtual {p0, v8, v9}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    int-to-float v9, v9

    div-float v4, v8, v9

    int-to-float v8, v0

    div-float/2addr v8, v4

    float-to-int v5, v8

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v1}, Lcom/google/android/finsky/layout/FifeImageView;->measure(II)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    div-int/lit8 v9, v5, 0x4

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v1}, Landroid/view/View;->measure(II)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
