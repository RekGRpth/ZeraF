.class public Lcom/google/android/finsky/layout/play/PlayMerchBannerView;
.super Landroid/view/ViewGroup;
.source "PlayMerchBannerView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mAccessibilityOverlay:Landroid/view/View;

.field private mColumnCount:I

.field private final mCompactHeight:Z

.field private mMerchFill:Landroid/view/View;

.field private mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

.field private mOverlapGradient:Landroid/view/View;

.field private mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mSentImpression:Z

.field private mSubtitle:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    const/16 v1, 0x197

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSentImpression:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mCompactHeight:Z

    return-void
.end method

.method private getMerchImageOffset(I)I
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    int-to-float v0, p1

    const v1, 0x3fe38e39

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000

    mul-float/2addr v0, v1

    const/high16 v1, 0x41200000

    div-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method private measureTexts(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/high16 v8, 0x40000000

    const/4 v7, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getMerchImageOffset(I)I

    move-result v0

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    const/4 v5, 0x4

    if-gt v4, v5, :cond_0

    const v1, 0x3f59999a

    :goto_0
    neg-int v4, v0

    int-to-float v5, p2

    const v6, 0x3fe38e39

    mul-float/2addr v5, v6

    mul-float/2addr v5, v1

    float-to-int v5, v5

    add-int v2, v4, v5

    sub-int v3, p1, v2

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v5, v7}, Landroid/widget/TextView;->measure(II)V

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v5, v7}, Landroid/widget/TextView;->measure(II)V

    return-void

    :cond_0
    const/high16 v1, 0x3f800000

    goto :goto_0
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public configureMerch(Lcom/google/android/finsky/remoting/protos/DocAnnotations$NextBanner;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/remoting/protos/Doc$Image;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/protobuf/micro/ByteStringMicro;)V
    .locals 13
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$NextBanner;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/remoting/protos/Doc$Image;
    .param p4    # Landroid/view/View$OnClickListener;
    .param p5    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p6    # Lcom/google/protobuf/micro/ByteStringMicro;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->hasFillColorRgb()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getFillColorRgb()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    :goto_0
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    move-object/from16 v0, p3

    invoke-virtual {v8, v0, p2}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/remoting/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchFill:Landroid/view/View;

    invoke-virtual {v8, v5}, Landroid/view/View;->setBackgroundColor(I)V

    new-instance v8, Landroid/graphics/drawable/GradientDrawable;

    sget-object v9, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v10, 0x2

    new-array v10, v10, [I

    const/4 v11, 0x0

    const v12, 0xffffff

    and-int/2addr v12, v5

    aput v12, v10, v11

    const/4 v11, 0x1

    aput v5, v10, v11

    invoke-direct {v8, v9, v10}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/GradientDrawable;->setDither(Z)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$NextBanner;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$NextBanner;->getSubtitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$NextBanner;->hasColorTextArgb()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$NextBanner;->getColorTextArgb()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    :goto_1
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v0, p4

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v0, p6

    invoke-static {v8, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iget-boolean v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSentImpression:Z

    if-nez v8, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v8

    invoke-interface {v8, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSentImpression:Z

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0015

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    goto/16 :goto_0

    :cond_2
    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v4

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    mul-int/lit8 v8, v4, 0x15

    mul-int/lit8 v9, v3, 0x48

    add-int/2addr v8, v9

    mul-int/lit8 v9, v1, 0x7

    add-int v2, v8, v9

    const/16 v8, 0x3200

    if-ge v2, v8, :cond_3

    const v7, 0x7f0a0042

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    goto :goto_1

    :cond_3
    const v7, 0x7f0a0043

    goto :goto_2
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public init(I)V
    .locals 2
    .param p1    # I

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Merch banner doesn\'t support non-positive number of columns: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " passed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const v0, 0x7f0801b8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    const v0, 0x7f0801b7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchFill:Landroid/view/View;

    const v0, 0x7f0801b9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    const v0, 0x7f0801c1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f0801c2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    const v0, 0x7f080020

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mAccessibilityOverlay:Landroid/view/View;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getWidth()I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingBottom()I

    move-result v4

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchFill:Landroid/view/View;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchFill:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v5

    invoke-virtual {v10, v11, v5, v9, v12}, Landroid/view/View;->layout(IIII)V

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredWidth()I

    move-result v2

    if-lez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getMerchImageOffset(I)I

    move-result v3

    neg-int v10, v3

    add-int v1, v10, v2

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    neg-int v11, v3

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v12}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v5

    invoke-virtual {v10, v11, v5, v1, v12}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    sub-int v11, v1, v11

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v5

    invoke-virtual {v10, v11, v5, v1, v12}, Landroid/view/View;->layout(IIII)V

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    invoke-virtual {v10, v11, v12, v13, v0}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    :goto_0
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    sub-int v10, v0, v8

    sub-int/2addr v10, v6

    sub-int/2addr v10, v5

    sub-int/2addr v10, v4

    div-int/lit8 v10, v10, 0x2

    add-int v7, v5, v10

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v11

    sub-int v11, v9, v11

    add-int v12, v7, v8

    invoke-virtual {v10, v11, v7, v9, v12}, Landroid/widget/TextView;->layout(IIII)V

    add-int/2addr v7, v8

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v11

    sub-int v11, v9, v11

    add-int v12, v7, v6

    invoke-virtual {v10, v11, v7, v9, v12}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mAccessibilityOverlay:Landroid/view/View;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v5

    invoke-virtual {v10, v11, v5, v9, v12}, Landroid/view/View;->layout(IIII)V

    return-void

    :cond_0
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v12}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v5

    invoke-virtual {v10, v11, v5, v2, v12}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/4 v12, 0x0

    const/high16 v11, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    if-gtz v9, :cond_0

    invoke-virtual {p0, v0, v12}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    div-int v4, v0, v9

    move v3, v4

    iget-boolean v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mCompactHeight:Z

    if-eqz v9, :cond_2

    mul-int/lit8 v9, v4, 0x2

    div-int/lit8 v1, v9, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->measureTexts(II)V

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v10

    add-int v8, v9, v10

    if-le v8, v1, :cond_1

    invoke-direct {p0, v0, v4}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->measureTexts(II)V

    :goto_1
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    if-nez v5, :cond_3

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-static {v12, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v9, v10, v2}, Lcom/google/android/finsky/layout/FifeImageView;->measure(II)V

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchFill:Landroid/view/View;

    invoke-virtual {v9, p1, v2}, Landroid/view/View;->measure(II)V

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v9, p1, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingTop()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingBottom()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {p0, v0, v9}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_1
    move v3, v1

    goto :goto_1

    :cond_2
    invoke-direct {p0, v0, v4}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->measureTexts(II)V

    goto :goto_1

    :cond_3
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    int-to-float v10, v10

    div-float v6, v9, v10

    int-to-float v9, v3

    div-float/2addr v9, v6

    float-to-int v7, v9

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-static {v7, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v9, v10, v2}, Lcom/google/android/finsky/layout/FifeImageView;->measure(II)V

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    div-int/lit8 v10, v7, 0x4

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v9, v10, v2}, Landroid/view/View;->measure(II)V

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v9, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public resetUiElementNode()V
    .locals 2

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    const/16 v1, 0x197

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSentImpression:Z

    return-void
.end method
