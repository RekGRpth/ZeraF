.class public Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;
.super Landroid/view/ViewGroup;
.source "PlayCardClusterViewHeader.java"


# instance fields
.field private mHeader:Landroid/view/View;

.field private final mMinHeight:I

.field private mMoreView:Landroid/widget/TextView;

.field private mTitleGroup:Landroid/view/View;

.field private mTitleMain:Landroid/widget/TextView;

.field private mTitleSecondary:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/android/vending/R$styleable;->PlayCardClusterViewHeader:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMinHeight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const v0, 0x7f0800da

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeader:Landroid/view/View;

    const v0, 0x7f0800db

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    const v1, 0x7f0800dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    const v1, 0x7f0800dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    const v0, 0x7f0800de

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingTop()I

    move-result v4

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    invoke-virtual {v7, v8, v4, v9, v0}, Landroid/view/View;->layout(IIII)V

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingRight()I

    move-result v7

    sub-int v5, v6, v7

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    sub-int v7, v0, v1

    sub-int/2addr v7, v4

    div-int/lit8 v7, v7, 0x2

    add-int v2, v4, v7

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    sub-int v8, v5, v3

    add-int v9, v2, v1

    invoke-virtual {v7, v8, v2, v5, v9}, Landroid/widget/TextView;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingLeft()I

    move-result v3

    sub-int v3, v2, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingRight()I

    move-result v4

    sub-int v1, v3, v4

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v5}, Landroid/widget/TextView;->measure(II)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v1, v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    const/high16 v4, 0x40000000

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMinHeight:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setMeasuredDimension(II)V

    return-void
.end method

.method public setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/view/View$OnClickListener;

    const/16 v4, 0x8

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/google/android/finsky/utils/PlayUtils;->getItalicSafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v2, p5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeader:Landroid/view/View;

    if-eqz p5, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setClickable(Z)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-static {p3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto :goto_1
.end method
