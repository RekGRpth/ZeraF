.class public Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;
.super Lcom/google/android/finsky/layout/play/PlayCardView;
.source "PlayCardViewListingSmall.java"


# instance fields
.field private final mTextContentHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTextContentHeight:I

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->measureThumbnailSpanningWidth(I)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTextContentHeight:I

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingBottom()I

    move-result v3

    add-int v0, v2, v3

    const/high16 v2, 0x40000000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-super {p0, p1, v2}, Lcom/google/android/finsky/layout/play/PlayCardView;->onMeasure(II)V

    return-void
.end method
