.class public Lcom/google/android/finsky/layout/play/PlayCardViewSmall;
.super Lcom/google/android/finsky/layout/play/PlayCardView;
.source "PlayCardViewSmall.java"


# instance fields
.field private final mExtraVSpace:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mExtraVSpace:I

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 11
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/play/PlayCardView;->onLayout(ZIIII)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v6}, Landroid/widget/RatingBar;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getTop()I

    move-result v6

    :goto_1
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v7

    sub-int v4, v6, v7

    if-gtz v4, :cond_3

    :cond_0
    :goto_2
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    :cond_3
    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mExtraVSpace:I

    mul-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x2

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getTop()I

    move-result v8

    add-int/2addr v8, v3

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getBottom()I

    move-result v10

    add-int/2addr v10, v3

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getTop()I

    move-result v8

    add-int/2addr v8, v3

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getBottom()I

    move-result v10

    add-int/2addr v10, v3

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->layout(IIII)V

    sub-int/2addr v4, v3

    if-lez v4, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v6}, Landroid/widget/RatingBar;->getVisibility()I

    move-result v6

    if-nez v6, :cond_4

    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mExtraVSpace:I

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    add-int v0, v6, v3

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v7}, Landroid/widget/RatingBar;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v8}, Landroid/widget/RatingBar;->getTop()I

    move-result v8

    add-int/2addr v8, v0

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v9}, Landroid/widget/RatingBar;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v10}, Landroid/widget/RatingBar;->getBottom()I

    move-result v10

    add-int/2addr v10, v0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/RatingBar;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getTop()I

    move-result v8

    add-int/2addr v8, v0

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getBottom()I

    move-result v10

    add-int/2addr v10, v0

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/google/android/finsky/layout/play/PlayActionButton;->layout(IIII)V

    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_5

    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mExtraVSpace:I

    mul-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x2

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    add-int v2, v6, v3

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTop()I

    move-result v8

    add-int/2addr v8, v2

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/DecoratedTextView;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/DecoratedTextView;->getBottom()I

    move-result v10

    add-int/2addr v10, v2

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/google/android/finsky/layout/DecoratedTextView;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getTop()I

    move-result v8

    add-int/2addr v8, v2

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getBottom()I

    move-result v10

    add-int/2addr v10, v2

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/google/android/finsky/layout/play/PlayActionButton;->layout(IIII)V

    sub-int/2addr v4, v2

    :cond_5
    if-lez v4, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getTop()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v1, v6, 0x2

    if-lez v1, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getTop()I

    move-result v8

    sub-int/2addr v8, v1

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getBottom()I

    move-result v10

    sub-int/2addr v10, v1

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/google/android/finsky/layout/play/PlayCardReason;->layout(IIII)V

    goto/16 :goto_2
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewSmall;->measureThumbnailSpanningWidth(I)V

    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;->onMeasure(II)V

    return-void
.end method
