.class public Lcom/google/android/finsky/layout/play/NativePopupMenu;
.super Ljava/lang/Object;
.source "NativePopupMenu.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayPopupMenu;


# instance fields
.field private final mPopupMenu:Landroid/widget/PopupMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {v0, p1, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    return-void
.end method


# virtual methods
.method public addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v2, v2, v2, p1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    new-instance v1, Lcom/google/android/finsky/layout/play/NativePopupMenu$1;

    invoke-direct {v1, p0, p3}, Lcom/google/android/finsky/layout/play/NativePopupMenu$1;-><init>(Lcom/google/android/finsky/layout/play/NativePopupMenu;Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method
