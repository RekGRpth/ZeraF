.class public Lcom/google/android/finsky/layout/play/LegacyPopupMenu;
.super Ljava/lang/Object;
.source "LegacyPopupMenu.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayPopupMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;,
        Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;
    }
.end annotation


# instance fields
.field private final mAnchor:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private final mPopupActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mAnchor:Landroid/view/View;

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;-><init>(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public show()V
    .locals 6

    new-instance v0, Lcom/google/android/finsky/layout/play/PopupSelector;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mAnchor:Landroid/view/View;

    new-instance v3, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    invoke-direct {v3, v4, v5}, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PopupSelector;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;)V

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PopupSelector;->show()V

    return-void
.end method
