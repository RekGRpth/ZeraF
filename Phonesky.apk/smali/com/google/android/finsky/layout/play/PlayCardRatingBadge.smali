.class public Lcom/google/android/finsky/layout/play/PlayCardRatingBadge;
.super Landroid/widget/RelativeLayout;
.source "PlayCardRatingBadge.java"


# instance fields
.field private mBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

.field private mRating:Landroid/widget/RatingBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRatingBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f080143

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRatingBadge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRatingBadge;->mRating:Landroid/widget/RatingBar;

    const v0, 0x7f0801be

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRatingBadge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRatingBadge;->mBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

    return-void
.end method
