.class public Lcom/google/android/finsky/layout/AccountSelectorView;
.super Landroid/widget/TextView;
.source "AccountSelectorView.java"


# instance fields
.field protected final mMultiAccountDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/layout/AccountSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/AccountSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/android/vending/R$styleable;->AccountSelectorView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/AccountSelectorView;->mMultiAccountDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/activities/AuthenticatedActivity;)V
    .locals 6
    .param p1    # Lcom/google/android/finsky/activities/AuthenticatedActivity;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/AccountSelectorView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AccountSelectorView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v4, v0

    if-le v4, v2, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/AccountSelectorView;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/AccountSelectorView;->mMultiAccountDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v5, v5, v2, v5}, Lcom/google/android/finsky/layout/AccountSelectorView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    new-instance v2, Lcom/google/android/finsky/layout/AccountSelectorView$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/finsky/layout/AccountSelectorView$1;-><init>(Lcom/google/android/finsky/layout/AccountSelectorView;Lcom/google/android/finsky/activities/AuthenticatedActivity;)V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/AccountSelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/AccountSelectorView;->setClickable(Z)V

    invoke-virtual {p0, v5, v5, v5, v5}, Lcom/google/android/finsky/layout/AccountSelectorView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/AccountSelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
