.class public Lcom/google/android/finsky/layout/EpisodeSnippet;
.super Lcom/google/android/finsky/layout/SeparatorRelativeLayout;
.source "EpisodeSnippet.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;
    }
.end annotation


# instance fields
.field private mAddedDrawable:Landroid/view/View;

.field private mAddedState:Landroid/widget/TextView;

.field private final mBaseRowHeight:I

.field private mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

.field private mBuyButton:Landroid/widget/Button;

.field private mCollapsedContent:Landroid/view/View;

.field private mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

.field private mEpisode:Lcom/google/android/finsky/api/model/Document;

.field private mEpisodeNumber:Landroid/widget/TextView;

.field private mEpisodeTitle:Landroid/widget/TextView;

.field private mExpandedContent:Landroid/view/View;

.field private mExpandedDescription:Landroid/widget/TextView;

.field private mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

.field private mExpandedViewStub:Landroid/view/ViewStub;

.field private final mHandler:Landroid/os/Handler;

.field private mIsNewPurchase:Z

.field private mMaxExpandedHeight:I

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mReferrerListCookie:Ljava/lang/String;

.field private mReferrerUrl:Ljava/lang/String;

.field private final mScrollerRunnable:Ljava/lang/Runnable;

.field private mSeasonDocument:Lcom/google/android/finsky/api/model/Document;

.field private mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/SeparatorRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    const/16 v1, 0x1f7

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBaseRowHeight:I

    new-instance v0, Lcom/google/android/finsky/layout/EpisodeSnippet$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/EpisodeSnippet$1;-><init>(Lcom/google/android/finsky/layout/EpisodeSnippet;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mScrollerRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/EpisodeSnippet;)I
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    iget v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBaseRowHeight:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/EpisodeSnippet;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/EpisodeSnippet;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/EpisodeSnippet;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->toggleExpandedVisibility()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/EpisodeSnippet;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->logCurrentState()V

    return-void
.end method

.method private static clearBuyButtonStyle(Landroid/widget/Button;)V
    .locals 2
    .param p0    # Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0, v1, v1, v1, v1}, Landroid/widget/Button;->setPadding(IIII)V

    const v1, 0x7f0a0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/Button;->setTextColor(I)V

    return-void
.end method

.method public static handleBuyButtonClick(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 9
    .param p0    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v2, 0xda

    invoke-virtual {v0, v2, v4, p2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0, v8, p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->openItem(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v2, 0xc8

    invoke-virtual {v0, v2, v4, p2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOfferType()I

    move-result v3

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->buy(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private inflateViewStubIfNecessary()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedViewStub:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    const v0, 0x7f080126

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedDescription:Landroid/widget/TextView;

    const v0, 0x7f080125

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/HeroGraphicView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

    :cond_0
    return-void
.end method

.method private logCurrentState()V
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "episodeSelected?doc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&expanded="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->isExpanded()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mReferrerUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mReferrerListCookie:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setExpandedContentVisibility(I)V
    .locals 9
    .param p1    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->inflateViewStubIfNecessary()V

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    invoke-virtual {v4, p1}, Landroid/view/View;->setVisibility(I)V

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    iget v6, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mMaxExpandedHeight:I

    invoke-static {v4, v5, v3, v6}, Lcom/google/android/finsky/utils/ThumbnailUtils;->getScreenshotUrlFromDocument(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    iget-object v6, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mSeasonDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4, v5, v6, v1, v2}, Lcom/google/android/finsky/layout/HeroGraphicView;->load(Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v4

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070137

    new-array v7, v2, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;->getReleaseDate()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedDescription:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    new-instance v5, Lcom/google/android/finsky/layout/EpisodeSnippet$5;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/EpisodeSnippet$5;-><init>(Lcom/google/android/finsky/layout/EpisodeSnippet;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

    const/16 v5, 0x8

    if-ne p1, v5, :cond_5

    :goto_0
    invoke-interface {v4, p0, v2}, Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;->onCollapsedStateChanged(Lcom/google/android/finsky/layout/EpisodeSnippet;Z)V

    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_5
    move v2, v3

    goto :goto_0
.end method

.method private toggleExpandedVisibility()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setExpandedContentVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static updateBuyButtonState(Landroid/content/res/Resources;Landroid/widget/Button;Landroid/widget/TextView;Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;IZ)V
    .locals 8
    .param p0    # Landroid/content/res/Resources;
    .param p1    # Landroid/widget/Button;
    .param p2    # Landroid/widget/TextView;
    .param p3    # Landroid/view/View;
    .param p4    # Lcom/google/android/finsky/api/model/Document;
    .param p5    # Lcom/google/android/finsky/api/model/Document;
    .param p6    # I
    .param p7    # Z

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setVisibility(I)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-static {p5, v1, v0}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p1}, Landroid/widget/Button;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p1}, Landroid/widget/Button;->getPaddingRight()I

    move-result v4

    const v6, 0x7f0200ce

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1, v3, v6, v4, v7}, Landroid/widget/Button;->setPadding(IIII)V

    const v6, 0x7f0a0001

    invoke-virtual {p0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setTextColor(I)V

    if-eqz v2, :cond_2

    invoke-virtual {p5}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v6

    const/16 v7, 0x13

    if-ne v6, v7, :cond_1

    const v6, 0x7f070111

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setText(I)V

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    if-eqz p2, :cond_0

    if-eqz p7, :cond_9

    const/4 v5, 0x0

    :goto_1
    invoke-virtual {p3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const v6, 0x7f070126

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setText(I)V

    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    const/4 v6, 0x1

    invoke-virtual {p5, v6}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    move-result-object v6

    if-eqz v6, :cond_4

    const/4 v6, -0x1

    if-eq p6, v6, :cond_3

    invoke-virtual {p1, p6}, Landroid/widget/Button;->setText(I)V

    :goto_2
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    const/4 v6, 0x1

    invoke-virtual {p5, v6}, Lcom/google/android/finsky/api/model/Document;->getFormattedPrice(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    const/4 v6, 0x7

    invoke-virtual {p5, v6}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    move-result-object v6

    if-eqz v6, :cond_6

    const/4 v6, -0x1

    if-eq p6, v6, :cond_5

    invoke-virtual {p1, p6}, Landroid/widget/Button;->setText(I)V

    :goto_3
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_5
    const/4 v6, 0x7

    invoke-virtual {p5, v6}, Lcom/google/android/finsky/api/model/Document;->getFormattedPrice(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    if-eqz p4, :cond_8

    const/4 v6, 0x1

    invoke-virtual {p4, v6}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    move-result-object v6

    if-nez v6, :cond_7

    const/4 v6, 0x7

    invoke-virtual {p4, v6}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    move-result-object v6

    if-eqz v6, :cond_8

    :cond_7
    invoke-static {p1}, Lcom/google/android/finsky/layout/EpisodeSnippet;->clearBuyButtonStyle(Landroid/widget/Button;)V

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    const v6, 0x7f07027e

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    :cond_8
    const/4 v6, 0x4

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_9
    const/16 v5, 0x8

    goto :goto_1
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public collapseWithoutNotifyingListeners()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public expand()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setExpandedContentVisibility(I)V

    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isExpanded()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 9

    invoke-super {p0}, Lcom/google/android/finsky/layout/SeparatorRelativeLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v8

    if-nez v8, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeNumber:Landroid/widget/TextView;

    invoke-virtual {v8}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;->getEpisodeIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBuyButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedState:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedDrawable:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mSeasonDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    const/4 v6, -0x1

    iget-boolean v7, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mIsNewPurchase:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/finsky/layout/EpisodeSnippet;->updateBuyButtonState(Landroid/content/res/Resources;Landroid/widget/Button;Landroid/widget/TextView;Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;IZ)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBuyButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/finsky/layout/EpisodeSnippet$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/EpisodeSnippet$2;-><init>(Lcom/google/android/finsky/layout/EpisodeSnippet;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeNumber:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    sget-object v0, Lcom/google/android/finsky/config/G;->prePurchaseSharingEnabled:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/finsky/layout/EpisodeSnippet$3;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/EpisodeSnippet$3;-><init>(Lcom/google/android/finsky/layout/EpisodeSnippet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedContent:Landroid/view/View;

    new-instance v1, Lcom/google/android/finsky/layout/EpisodeSnippet$4;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/EpisodeSnippet$4;-><init>(Lcom/google/android/finsky/layout/EpisodeSnippet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Lcom/google/android/finsky/layout/SeparatorRelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/finsky/layout/SeparatorRelativeLayout;->onFinishInflate()V

    const v0, 0x7f080119

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedContent:Landroid/view/View;

    const v0, 0x7f08011f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedViewStub:Landroid/view/ViewStub;

    const v0, 0x7f08011a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeNumber:Landroid/widget/TextView;

    const v0, 0x7f0800af

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBuyButton:Landroid/widget/Button;

    const v0, 0x7f08011b

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    const v0, 0x7f08011e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedState:Landroid/widget/TextView;

    const v0, 0x7f08011d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedDrawable:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mMaxExpandedHeight:I

    return-void
.end method

.method public setEpisodeDetails(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/api/model/Document;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p4    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;
    .param p9    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-object p1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mSeasonDocument:Lcom/google/android/finsky/api/model/Document;

    iput-object p2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    iput-object p4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    iput-boolean p5, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mIsNewPurchase:Z

    iput-object p8, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

    iput-object p6, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mReferrerUrl:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mReferrerListCookie:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method
