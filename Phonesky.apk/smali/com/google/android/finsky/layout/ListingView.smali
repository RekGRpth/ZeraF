.class public Lcom/google/android/finsky/layout/ListingView;
.super Lcom/google/android/finsky/layout/SeparatorLinearLayout;
.source "ListingView.java"


# instance fields
.field private mCurrentPageUrl:Ljava/lang/String;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mListingLayout:Landroid/widget/LinearLayout;

.field private mRows:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ListingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingView;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/ListingView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/ListingView;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ListingView;->mCurrentPageUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/ListingView;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/ListingView;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method private addEmailLinkRow(ILjava/lang/String;I)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const-string v3, "mailto"

    const/4 v4, 0x0

    invoke-static {v3, p2, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/IntentUtils;->createSendIntentForUrl(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "android.intent.extra.SUBJECT"

    iget-object v4, p0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/finsky/layout/ListingView;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f040093

    iget-object v5, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/ListingRow;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/finsky/layout/ListingRow;->populate(ILjava/lang/String;I)V

    new-instance v3, Lcom/google/android/finsky/layout/ListingView$2;

    invoke-direct {v3, p0, v0, v1, p2}, Lcom/google/android/finsky/layout/ListingView$2;-><init>(Lcom/google/android/finsky/layout/ListingView;Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/ListingRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget v3, p0, Lcom/google/android/finsky/layout/ListingView;->mRows:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/finsky/layout/ListingView;->mRows:I

    return-void
.end method

.method private addFlagContentRow(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 5
    .param p1    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/ListingView;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f040093

    iget-object v3, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ListingRow;

    const v2, 0x7f070238

    iget-object v1, p0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    const v1, 0x7f070239

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/google/android/finsky/layout/ListingRow;->populate(II)V

    new-instance v1, Lcom/google/android/finsky/layout/ListingView$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/layout/ListingView$3;-><init>(Lcom/google/android/finsky/layout/ListingView;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ListingRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    const v1, 0x7f07023b

    goto :goto_0
.end method

.method private addWebLinkRow(ILjava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/IntentUtils;->createViewIntentForUrl(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/layout/ListingView;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040093

    iget-object v4, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/ListingRow;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/finsky/layout/ListingRow;->populate(ILjava/lang/String;I)V

    new-instance v2, Lcom/google/android/finsky/layout/ListingView$1;

    invoke-direct {v2, p0, v0, p4, p2}, Lcom/google/android/finsky/layout/ListingView$1;-><init>(Lcom/google/android/finsky/layout/ListingView;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/ListingRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget v2, p0, Lcom/google/android/finsky/layout/ListingView;->mRows:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/finsky/layout/ListingView;->mRows:I

    return-void
.end method

.method private setupHeader(Ljava/lang/String;I)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/layout/ListingView;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    move v1, p2

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public bindFlagContent(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3    # Z
    .param p4    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-object p1, p0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ListingView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07024a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/layout/ListingView;->setupHeader(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-direct {p0, p2}, Lcom/google/android/finsky/layout/ListingView;->addFlagContentRow(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingView;->setVisibility(I)V

    goto :goto_0
.end method

.method public bindLinks(Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 17
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/finsky/api/model/Document;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ListingView;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/ListingView;->mCurrentPageUrl:Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/layout/ListingView;->mRows:I

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v14}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/ListingView;->mRows:I

    if-nez v14, :cond_6

    const/16 v14, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/finsky/layout/ListingView;->setVisibility(I)V

    :goto_1
    return-void

    :pswitch_1
    const v14, 0x7f07015c

    invoke-virtual {v5, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/google/android/finsky/layout/ListingView;->setupHeader(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->hasDeveloperWebsite()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->getDeveloperWebsite()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    const v14, 0x7f07017f

    const v15, 0x7f02005c

    const-string v16, "developerLink"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v7, v15, v1}, Lcom/google/android/finsky/layout/ListingView;->addWebLinkRow(ILjava/lang/String;ILjava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->hasDeveloperEmail()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->getDeveloperEmail()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    const v14, 0x7f070180

    const v15, 0x7f02005a

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6, v15}, Lcom/google/android/finsky/layout/ListingView;->addEmailLinkRow(ILjava/lang/String;I)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getPrivacyPolicyUrl()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    const v14, 0x7f070181

    const v15, 0x7f02005c

    const-string v16, "privacyPolicy"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v11, v15, v1}, Lcom/google/android/finsky/layout/ListingView;->addWebLinkRow(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const v14, 0x7f070163

    invoke-virtual {v5, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/google/android/finsky/layout/ListingView;->setupHeader(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ListingView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getArtistDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;->hasExternalLinks()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;->getExternalLinks()Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistExternalLinks;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistExternalLinks;->getWebsiteUrlCount()I

    move-result v14

    if-lez v14, :cond_4

    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistExternalLinks;->getWebsiteUrlList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_3

    const v14, 0x7f07017f

    const v15, 0x7f02005c

    const-string v16, "developerLink"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v12, v15, v1}, Lcom/google/android/finsky/layout/ListingView;->addWebLinkRow(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistExternalLinks;->hasYoutubeChannelUrl()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistExternalLinks;->getYoutubeChannelUrl()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_5

    const v14, 0x7f070182

    const v15, 0x7f02005d

    const-string v16, "artistYoutubeLink"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v13, v15, v1}, Lcom/google/android/finsky/layout/ListingView;->addWebLinkRow(ILjava/lang/String;ILjava/lang/String;)V

    :cond_5
    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistExternalLinks;->hasGooglePlusProfileUrl()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistExternalLinks;->getGooglePlusProfileUrl()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_0

    const v14, 0x7f070183

    const v15, 0x7f02005b

    const-string v16, "artistGooglePlusLink"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v9, v15, v1}, Lcom/google/android/finsky/layout/ListingView;->addWebLinkRow(ILjava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/finsky/layout/ListingView;->setVisibility(I)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public bindRentalTerms(Lcom/google/android/finsky/api/model/Document;I)V
    .locals 9
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # I

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getMovieRentalTerms()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoRentalTerm;

    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoRentalTerm;->getOfferType()I

    move-result v5

    if-ne v5, p2, :cond_0

    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoRentalTerm;->getRentalHeader()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/finsky/layout/ListingView;->setupHeader(Ljava/lang/String;I)V

    invoke-virtual {v4}, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoRentalTerm;->getTermList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoRentalTerm$Term;

    iget-object v5, p0, Lcom/google/android/finsky/layout/ListingView;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f040093

    iget-object v7, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/ListingRow;

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoRentalTerm$Term;->getHeader()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoRentalTerm$Term;->getBody()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/google/android/finsky/layout/ListingRow;->populateAsHtml(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/ListingRow;->hideSeparator()V

    iget-object v5, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/ListingView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->onFinishInflate()V

    const v0, 0x7f0800cf

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingView;->mListingLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0800da

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingView;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    return-void
.end method
