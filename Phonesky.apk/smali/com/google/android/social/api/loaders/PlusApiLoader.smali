.class public abstract Lcom/google/android/social/api/loaders/PlusApiLoader;
.super Lvedroid/support/v4/content/Loader;
.source "PlusApiLoader.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/content/Loader",
        "<",
        "Lcom/google/android/gms/common/ConnectionResult;",
        ">;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;"
    }
.end annotation


# instance fields
.field private final apiClient:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lvedroid/support/v4/content/Loader;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0, p2}, Lcom/google/android/social/api/service/PlusInternalClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/social/api/loaders/PlusApiLoader;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    return-void
.end method


# virtual methods
.method public final onConnected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/loaders/PlusApiLoader;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/loaders/PlusApiLoader;->onConnected(Lcom/google/android/social/api/service/PlusInternalClient;)V

    return-void
.end method

.method protected abstract onConnected(Lcom/google/android/social/api/service/PlusInternalClient;)V
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0}, Lcom/google/android/social/api/loaders/PlusApiLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/loaders/PlusApiLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method protected onStartLoading()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/content/Loader;->onStartLoading()V

    iget-object v0, p0, Lcom/google/android/social/api/loaders/PlusApiLoader;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->connect()V

    return-void
.end method

.method protected onStopLoading()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/content/Loader;->onStopLoading()V

    iget-object v0, p0, Lcom/google/android/social/api/loaders/PlusApiLoader;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->disconnect()V

    return-void
.end method
