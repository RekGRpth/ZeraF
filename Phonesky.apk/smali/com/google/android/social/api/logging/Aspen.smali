.class public final Lcom/google/android/social/api/logging/Aspen;
.super Lcom/google/android/social/api/logging/AbstractAnalyticsNamespace;
.source "Aspen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/logging/Aspen$View;,
        Lcom/google/android/social/api/logging/Aspen$Action;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/social/api/logging/AbstractAnalyticsNamespace;-><init>()V

    return-void
.end method

.method static synthetic access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/social/api/logging/Aspen;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    return-object v0
.end method

.method private static build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .locals 1
    .param p0    # I

    const-string v0, "asp"

    invoke-static {v0, p0}, Lcom/google/android/social/api/logging/Aspen;->buildNamespacedType(Ljava/lang/String;I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    return-object v0
.end method
