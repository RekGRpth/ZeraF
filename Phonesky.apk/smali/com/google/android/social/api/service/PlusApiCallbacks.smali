.class public Lcom/google/android/social/api/service/PlusApiCallbacks;
.super Ljava/lang/Object;
.source "PlusApiCallbacks.java"

# interfaces
.implements Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;
.implements Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
.implements Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;
.implements Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;
.implements Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;
.implements Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;
.implements Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;
.implements Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;


# instance fields
.field private callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    return-void
.end method


# virtual methods
.method public onAudiencesList(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onCirclesAddRemovePeople(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/Person;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/Person;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p2, :cond_0

    const-string v1, "person"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onDownloadImage(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/String;IILandroid/graphics/Bitmap;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/graphics/Bitmap;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "url"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "width"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "height"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "bitmap"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v3

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v4

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {p5}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method public onIgnoreSuggestion(Lcom/google/android/gms/common/ConnectionResult;Z)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "ignored"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onPeopleList(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onPeopleLoaded(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/Person;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p2, :cond_0

    const-string v1, "person"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onPostInsertLog(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/social/api/service/PlusApiCallbacks;->callbacks:Lcom/google/android/social/api/service/IPlusInternalCallbacks;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-static {p1}, Lcom/google/android/social/api/service/Results;->getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/social/api/service/IPlusInternalCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
