.class public Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;
.super Lcom/google/android/social/api/service/IPlusInternalService$Stub;
.source "PlusInternalService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "PlusInternalServiceEntity"
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/social/api/service/IPlusInternalService$Stub;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addToCircles(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p2}, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;-><init>(Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method

.method public audiencesList(Lcom/google/android/social/api/service/IPlusInternalCallbacks;ZZ)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Z
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/people/operations/AudiencesListOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p2, p3}, Lcom/google/android/social/api/people/operations/AudiencesListOperation;-><init>(Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;Ljava/lang/String;ZZ)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method

.method public downloadImage(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;II)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/operations/DownloadImageOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    invoke-direct {v1, v2, p2, p3, p4}, Lcom/google/android/social/api/operations/DownloadImageOperation;-><init>(Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;Ljava/lang/String;II)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method

.method public getActivityReplies(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Landroid/os/Bundle;

    return-void
.end method

.method public getCirclesForPeople(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/people/operations/GetPeopleOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p2}, Lcom/google/android/social/api/people/operations/GetPeopleOperation;-><init>(Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method

.method public getFountainComments(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Landroid/os/Bundle;

    return-void
.end method

.method public ignoreSuggestion(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p2, p3}, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;-><init>(Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method

.method public peopleList(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/people/operations/PeopleListOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/social/api/people/operations/PeopleListOperation;-><init>(Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method

.method public postActivity(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;

    return-void
.end method

.method public postComment(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;

    return-void
.end method

.method public postInsertLog(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/operations/PostInsertLogOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p2}, Lcom/google/android/social/api/operations/PostInsertLogOperation;-><init>(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method

.method public searchAudience(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/service/IPlusInternalCallbacks;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->context:Landroid/content/Context;

    new-instance v1, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;

    new-instance v2, Lcom/google/android/social/api/service/PlusApiCallbacks;

    invoke-direct {v2, p1}, Lcom/google/android/social/api/service/PlusApiCallbacks;-><init>(Lcom/google/android/social/api/service/IPlusInternalCallbacks;)V

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;->accountName:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p2}, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;-><init>(Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V

    return-void
.end method
