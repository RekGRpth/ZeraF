.class public final Lcom/google/android/social/api/service/PlusApiService;
.super Lcom/google/android/social/api/service/PlusExecutorService;
.source "PlusApiService.java"


# instance fields
.field private final operationsList:Lcom/google/android/social/api/service/OperationsList;

.field private requestQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lcom/google/android/social/api/service/OperationsList;->getInstance()Lcom/google/android/social/api/service/OperationsList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/social/api/service/PlusApiService;-><init>(Lcom/google/android/social/api/service/OperationsList;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/social/api/service/OperationsList;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/service/OperationsList;

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/social/api/service/PlusExecutorService;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusApiService;->operationsList:Lcom/google/android/social/api/service/OperationsList;

    return-void
.end method

.method public static startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/social/api/operations/PlusApiOperation;

    invoke-static {}, Lcom/google/android/social/api/service/OperationsList;->getInstance()Lcom/google/android/social/api/service/OperationsList;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/social/api/service/PlusApiService;->startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;Lcom/google/android/social/api/service/OperationsList;)V

    return-void
.end method

.method public static startOperation(Landroid/content/Context;Lcom/google/android/social/api/operations/PlusApiOperation;Lcom/google/android/social/api/service/OperationsList;)V
    .locals 4

    invoke-virtual {p2, p1}, Lcom/google/android/social/api/service/OperationsList;->addOperation(Lcom/google/android/social/api/operations/PlusApiOperation;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.social.api.START_PLUS_API_SERVICE"

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/social/api/service/PlusApiService;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 8

    invoke-super {p0}, Lcom/google/android/social/api/service/PlusExecutorService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/social/api/internal/PlusApiSettings;->getInstance(Landroid/content/Context;)Lcom/google/android/social/api/internal/PlusApiSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/social/api/internal/PlusApiSettings;->isEnableSensitiveLogging()Z

    move-result v0

    new-instance v1, Lcom/google/android/social/api/network/ApiaryNetworkStack;

    invoke-direct {v1, p0, v0}, Lcom/google/android/social/api/network/ApiaryNetworkStack;-><init>(Landroid/content/Context;Z)V

    new-instance v2, Lcom/android/volley/RequestQueue;

    new-instance v3, Lcom/android/volley/toolbox/NoCache;

    invoke-direct {v3}, Lcom/android/volley/toolbox/NoCache;-><init>()V

    new-instance v4, Lcom/android/volley/toolbox/BasicNetwork;

    invoke-direct {v4, v1}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;)V

    const/4 v5, 0x4

    new-instance v6, Lcom/android/volley/ExecutorDelivery;

    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    invoke-direct {v6, v7}, Lcom/android/volley/ExecutorDelivery;-><init>(Landroid/os/Handler;)V

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;ILcom/android/volley/ResponseDelivery;)V

    invoke-virtual {p0, v2}, Lcom/google/android/social/api/service/PlusApiService;->setRequestQueue(Lcom/android/volley/RequestQueue;)V

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusApiService;->requestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2}, Lcom/android/volley/RequestQueue;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusApiService;->requestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->stop()V

    invoke-super {p0}, Lcom/google/android/social/api/service/PlusExecutorService;->onDestroy()V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const/4 v8, 0x0

    const-string v5, "com.google.android.social.api.START_PLUS_API_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/social/api/service/PlusApiService;->operationsList:Lcom/google/android/social/api/service/OperationsList;

    invoke-virtual {v5}, Lcom/google/android/social/api/service/OperationsList;->getAndClearOperations()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/operations/PlusApiOperation;

    :try_start_0
    iget-object v5, p0, Lcom/google/android/social/api/service/PlusApiService;->requestQueue:Lcom/android/volley/RequestQueue;

    invoke-interface {v3, p0, v5}, Lcom/google/android/social/api/operations/PlusApiOperation;->execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v5, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-interface {v3, v5}, Lcom/google/android/social/api/operations/PlusApiOperation;->onFailure(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    const/4 v4, 0x0

    instance-of v5, v0, Lcom/android/volley/AuthFailureError;

    if-eqz v5, :cond_0

    move-object v5, v0

    check-cast v5, Lcom/android/volley/AuthFailureError;

    invoke-virtual {v5}, Lcom/android/volley/AuthFailureError;->getResolutionIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_0

    check-cast v0, Lcom/android/volley/AuthFailureError;

    invoke-virtual {v0}, Lcom/android/volley/AuthFailureError;->getResolutionIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0, v8, v5, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    :cond_0
    new-instance v5, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v6, 0x7

    invoke-direct {v5, v6, v4}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-interface {v3, v5}, Lcom/google/android/social/api/operations/PlusApiOperation;->onFailure(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method setRequestQueue(Lcom/android/volley/RequestQueue;)V
    .locals 0
    .param p1    # Lcom/android/volley/RequestQueue;

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusApiService;->requestQueue:Lcom/android/volley/RequestQueue;

    return-void
.end method
