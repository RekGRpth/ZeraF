.class public final Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;
.super Lcom/google/android/gms/common/internal/IGmsServiceBroker$Stub;
.source "PlusInternalService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "ServiceBroker"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalService;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalService;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;->this$0:Lcom/google/android/social/api/service/PlusInternalService;

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/IGmsServiceBroker$Stub;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getPlusService(Lcom/google/android/gms/common/internal/IGmsCallbacks;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/internal/IGmsCallbacks;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;->this$0:Lcom/google/android/social/api/service/PlusInternalService;

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;->context:Landroid/content/Context;

    invoke-virtual {v1, v2, p6}, Lcom/google/android/social/api/service/PlusInternalService;->createService(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/social/api/service/IPlusInternalService$Stub;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method
