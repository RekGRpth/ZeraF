.class public interface abstract Lcom/google/android/social/api/service/IPlusInternalService;
.super Ljava/lang/Object;
.source "IPlusInternalService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/service/IPlusInternalService$Stub;
    }
.end annotation


# virtual methods
.method public abstract addToCircles(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract audiencesList(Lcom/google/android/social/api/service/IPlusInternalCallbacks;ZZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract downloadImage(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getActivityReplies(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getCirclesForPeople(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getFountainComments(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;ILandroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract ignoreSuggestion(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract peopleList(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract postActivity(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract postComment(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract postInsertLog(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract searchAudience(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
