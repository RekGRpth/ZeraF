.class final Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "IgnoreSuggestionCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/social/api/service/IPlusInternalService;",
        ">.CallbackProxy<",
        "Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;",
        ">;"
    }
.end annotation


# instance fields
.field private final ignored:Z

.field private final status:Lcom/google/android/gms/common/ConnectionResult;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;Z)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;
    .param p4    # Z

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iput-boolean p4, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;->ignored:Z

    return-void
.end method


# virtual methods
.method protected deliverCallback(Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iget-boolean v1, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;->ignored:Z

    invoke-interface {p1, v0, v1}, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;->onIgnoreSuggestion(Lcom/google/android/gms/common/ConnectionResult;Z)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;->deliverCallback(Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;)V

    return-void
.end method
