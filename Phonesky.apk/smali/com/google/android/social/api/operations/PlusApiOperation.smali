.class public interface abstract Lcom/google/android/social/api/operations/PlusApiOperation;
.super Ljava/lang/Object;
.source "PlusApiOperation.java"


# virtual methods
.method public abstract execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation
.end method

.method public abstract onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
.end method
