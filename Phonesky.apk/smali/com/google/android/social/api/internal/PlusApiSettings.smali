.class public Lcom/google/android/social/api/internal/PlusApiSettings;
.super Ljava/lang/Object;
.source "PlusApiSettings.java"


# static fields
.field private static instance:Lcom/google/android/social/api/internal/PlusApiSettings;


# instance fields
.field private final enableSensitiveLogging:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "plus_api.enable_sensitive_logging"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Z)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/internal/PlusApiSettings;->enableSensitiveLogging:Lcom/google/android/gsf/GservicesValue;

    invoke-static {p1}, Lcom/google/android/gsf/GservicesValue;->init(Landroid/content/Context;)V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/social/api/internal/PlusApiSettings;
    .locals 2

    const-class v1, Lcom/google/android/social/api/internal/PlusApiSettings;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/social/api/internal/PlusApiSettings;->instance:Lcom/google/android/social/api/internal/PlusApiSettings;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/social/api/internal/PlusApiSettings;

    invoke-direct {v0, p0}, Lcom/google/android/social/api/internal/PlusApiSettings;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/social/api/internal/PlusApiSettings;->instance:Lcom/google/android/social/api/internal/PlusApiSettings;

    :cond_0
    sget-object v0, Lcom/google/android/social/api/internal/PlusApiSettings;->instance:Lcom/google/android/social/api/internal/PlusApiSettings;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public isEnableSensitiveLogging()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/internal/PlusApiSettings;->enableSensitiveLogging:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
