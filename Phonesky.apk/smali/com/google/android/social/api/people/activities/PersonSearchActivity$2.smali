.class Lcom/google/android/social/api/people/activities/PersonSearchActivity$2;
.super Ljava/lang/Object;
.source "PersonSearchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/social/api/people/activities/PersonSearchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;


# direct methods
.method constructor <init>(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$2;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "selected_person"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$2;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchAdapter:Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;
    invoke-static {v2}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->access$300(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$2;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$2;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->finish()V

    return-void
.end method
