.class public final Lcom/google/android/social/api/people/operations/AudiencesListOperation;
.super Ljava/lang/Object;
.source "AudiencesListOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;,
        Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;
    }
.end annotation


# static fields
.field private static final log:Lcom/google/android/social/api/internal/GLog;


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final callback:Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;

.field private final includeCircles:Z

.field private final includeGroups:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/social/api/internal/GLog;

    invoke-direct {v0}, Lcom/google/android/social/api/internal/GLog;-><init>()V

    sput-object v0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->log:Lcom/google/android/social/api/internal/GLog;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->callback:Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;

    iput-object p2, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->accountName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->includeGroups:Z

    iput-boolean p4, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->includeCircles:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/social/api/people/operations/AudiencesListOperation;)Z
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/operations/AudiencesListOperation;

    iget-boolean v0, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->includeGroups:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/social/api/people/operations/AudiencesListOperation;)Z
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/operations/AudiencesListOperation;

    iget-boolean v0, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->includeCircles:Z

    return v0
.end method

.method static synthetic access$200()Lcom/google/android/social/api/internal/GLog;
    .locals 1

    sget-object v0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->log:Lcom/google/android/social/api/internal/GLog;

    return-object v0
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;

    iget-object v2, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->accountName:Ljava/lang/String;

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;-><init>(Lcom/google/android/social/api/people/operations/AudiencesListOperation;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->callback:Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;

    sget-object v2, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {v1, v2, v0}, Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;->onAudiencesList(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->callback:Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;->onAudiencesList(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    return-void
.end method
