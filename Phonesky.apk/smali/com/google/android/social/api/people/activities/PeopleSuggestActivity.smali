.class public Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "PeopleSuggestActivity.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
.implements Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;
.implements Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;
.implements Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;
.implements Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;
.implements Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;,
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;,
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;,
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;,
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientConnectionCallbacks;,
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;,
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;,
        Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/FragmentActivity;",
        "Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/text/TextWatcher;",
        "Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;",
        "Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/gms/common/ConnectionResult;",
        ">;",
        "Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;",
        "Landroid/view/View$OnTouchListener;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;",
        "Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;"
    }
.end annotation


# instance fields
.field private accountName:Ljava/lang/String;

.field private adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

.field private apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

.field private final apiClientRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;",
            ">;"
        }
    .end annotation
.end field

.field private circles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private clearButton:Landroid/widget/ImageView;

.field private currentSearchQuery:Ljava/lang/String;

.field private currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

.field private displayListLoaded:Z

.field private doneButton:Landroid/widget/Button;

.field private handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

.field private hasSearchText:Z

.field inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private installingGooglePlayServices:Z

.field private listView:Landroid/widget/ListView;

.field private loggedSuggestionsShown:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private numberOfSuggestionsLoaded:I

.field private pendingSearchQuery:Ljava/lang/String;

.field private peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

.field private personBeingEdited:Lcom/google/android/social/api/people/model/Person;

.field private plusClient:Lcom/google/android/gms/plus/PlusClient;

.field private popupAlreadyShown:Z

.field private scrollingLockDistance:F

.field private scrollingOriginX:F

.field private scrollingOriginY:F

.field private searchText:Landroid/widget/EditText;

.field private setOfPeopleAdded:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private suggestions:Lcom/google/android/social/api/people/model/PersonList;

.field private swipeDirectionLocked:Z

.field private swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->popupAlreadyShown:Z

    iput v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingOriginX:F

    iput v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingOriginY:F

    iput-boolean v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipeDirectionLocked:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;I)V
    .locals 0
    .param p0    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setResultAndFinish(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/gms/plus/PlusClient;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    iget v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->numberOfSuggestionsLoaded:I

    return v0
.end method

.method private checkHasSearchText()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getFriendsCircle()Lcom/google/android/social/api/people/model/AudienceMember;
    .locals 5

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->circles:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->circles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    const v3, 0x7f0700e9

    invoke-virtual {p0, v3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->circles:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v1, v3}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->circles:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/model/AudienceMember;

    :goto_2
    return-object v3

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private getProgressDialog()Lvedroid/support/v4/app/DialogFragment;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "progressDialog"

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    return-object v0
.end method

.method private getSearchLoaderArguments()Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "searchText"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private ignorePerson(Lcom/google/android/social/api/people/model/Person;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getCircleCount()I

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v1}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;->execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private onCirclePickerResult(ILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getAudienceFromResult(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->addPendingOperation(Ljava/lang/String;I)V

    new-instance v3, Lcom/google/android/social/api/people/model/Person;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/model/Person;->getPerson()Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/social/api/people/model/Person;-><init>(Lcom/google/android/social/api/people/model/AudienceMember;Ljava/util/ArrayList;)V

    invoke-direct {p0, v3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->updatePerson(Lcom/google/android/social/api/people/model/Person;)V

    new-instance v1, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-direct {v1, v3, v2}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;-><init>(Lcom/google/android/social/api/people/model/Person;Ljava/util/ArrayList;)V

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v3}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v1, p0, v3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;->execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v3, v4}, Lcom/google/android/social/api/people/model/PersonList;->hasPerson(Lcom/google/android/social/api/people/model/Person;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v3, v0}, Lcom/google/android/social/api/people/model/PersonList;->removePersonById(Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v3, v4}, Lcom/google/android/social/api/people/model/PersonList;->hasPerson(Lcom/google/android/social/api/people/model/Person;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v3, v4}, Lcom/google/android/social/api/people/model/PersonList;->hasPerson(Lcom/google/android/social/api/people/model/Person;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v3, v4}, Lcom/google/android/social/api/people/model/PersonList;->addToTop(Lcom/google/android/social/api/people/model/Person;)V

    goto :goto_1
.end method

.method private onGooglePlayServicesInstalled()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->displayListLoaded:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->showProgressDialog()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->connect()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendHideProgressDialog(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->setPersonList(Lcom/google/android/social/api/people/model/PersonList;)V

    goto :goto_0
.end method

.method private onLoginResult(ILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v2, 0x0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v1, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSearchLoaderArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setResultAndFinish(I)V

    goto :goto_0
.end method

.method private onPeopleList(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendHideProgressDialog(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p2}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->numberOfSuggestionsLoaded:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->displayListLoaded:Z

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->hasSearchText:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setSuggestionsInAdapter()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendShowErrorDialog(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    goto :goto_0
.end method

.method private onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_3

    iput-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->hasSearchText:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    if-ne v1, p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v1, p2}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->setPersonList(Lcom/google/android/social/api/people/model/PersonList;)V

    iput-object p2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {p0, p2}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollToTop(Lcom/google/android/social/api/people/model/PersonList;)V

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->pendingSearchQuery:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->pendingSearchQuery:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->search(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->pendingSearchQuery:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, p0, v1}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v1}, Lcom/google/android/social/api/service/PlusInternalClient;->disconnect()V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v1}, Lcom/google/android/social/api/service/PlusInternalClient;->connect()V

    goto :goto_1
.end method

.method private scrollToTop(Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->setSelectionAfterHeaderView()V

    :cond_0
    return-void
.end method

.method private search(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->hasSearchText:Z

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSearchLoaderArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method

.method private setResultAndFinish(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "added"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setOfPeopleAdded:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->finish()V

    return-void
.end method

.method private setSuggestionsInAdapter()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    invoke-static {v1, v2}, Lcom/google/android/social/api/people/model/PersonList;->getUnion(Lcom/google/android/social/api/people/model/PersonList;Lcom/google/android/social/api/people/model/PersonList;)Lcom/google/android/social/api/people/model/PersonList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->setPersonList(Lcom/google/android/social/api/people/model/PersonList;)V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollToTop(Lcom/google/android/social/api/people/model/PersonList;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    return-void
.end method

.method private setUpSearchBox()V
    .locals 3

    const v0, 0x7f0801da

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v0, 0x7f0801db

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->clearButton:Landroid/widget/ImageView;

    const v0, 0x7f0801dd

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->doneButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->clearButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->doneButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->setPersonRowListener(Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->setPersonShownListener(Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->setPopupStrategy(Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;)V

    const v0, 0x7f0801dc

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private showInstallGooglePlayServicesDialog(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "installGooglePlayServicesDialog"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    invoke-static {p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;->newDialogFragment(I)Lvedroid/support/v4/app/DialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "installGooglePlayServicesDialog"

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showProgressDialog()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getProgressDialog()Lvedroid/support/v4/app/DialogFragment;

    move-result-object v0

    if-nez v0, :cond_0

    const v1, 0x7f0700e4

    invoke-static {v1}, Lcom/google/android/social/api/people/fragments/ProgressDialogFragment;->createProgressDialog(I)Lcom/google/android/social/api/people/fragments/ProgressDialogFragment;

    move-result-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progressDialog"

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private updateAddedPeopleList(Lcom/google/android/social/api/people/model/Person;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getCircleCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setOfPeopleAdded:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setOfPeopleAdded:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setOfPeopleAdded:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private updatePerson(Lcom/google/android/social/api/people/model/Person;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->updatePerson(Lcom/google/android/social/api/people/model/Person;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {p0, v0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->updatePersonList(Lcom/google/android/social/api/people/model/PersonList;Lcom/google/android/social/api/people/model/Person;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {p0, v0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->updatePersonList(Lcom/google/android/social/api/people/model/PersonList;Lcom/google/android/social/api/people/model/Person;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {p0, v0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->updatePersonList(Lcom/google/android/social/api/people/model/PersonList;Lcom/google/android/social/api/people/model/Person;)V

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->updateAddedPeopleList(Lcom/google/android/social/api/people/model/Person;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/PersonList;->hasPerson(Lcom/google/android/social/api/people/model/Person;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getCircleCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/PersonList;->prependIfNotPresent(Lcom/google/android/social/api/people/model/Person;)Z

    :cond_1
    return-void
.end method

.method private updatePersonList(Lcom/google/android/social/api/people/model/PersonList;Lcom/google/android/social/api/people/model/Person;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/people/model/PersonList;
    .param p2    # Lcom/google/android/social/api/people/model/Person;

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/social/api/people/model/PersonList;->getPerson(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    :cond_0
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    invoke-virtual {p1, v1, p2}, Lcom/google/android/social/api/people/model/PersonList;->setPerson(ILcom/google/android/social/api/people/model/Person;)Z

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1    # Landroid/text/Editable;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendExecuteTextChanged(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    sget-object v1, Lcom/google/android/social/api/logging/Aspen$Action;->PEOPLE_SEARCH_TEXT_ENTERED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    sget-object v2, Lcom/google/android/social/api/logging/Aspen$View;->PEOPLE_SUGGEST_WIDGET:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLogAction(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public editCircles(Lcom/google/android/social/api/people/model/Person;)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    new-instance v0, Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-direct {v0, p0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getCirclesList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setAudience(Ljava/util/ArrayList;)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setAccountName(Ljava/lang/String;)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->circles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setKnownAudienceMembers(Ljava/util/ArrayList;)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setLoadGroups(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setLoadCircles(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setLoadPeople(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setAllowEmptySelection(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setShowChips(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    const v1, 0x7f0700de

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setTitle(Ljava/lang/String;)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->asIntent()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v0, "PeopleSuggestActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSuggestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onCirclePickerResult(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p2, p3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onLoginResult(ILandroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setResultAndFinish(I)V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onCirclesAddRemovePeople(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/Person;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/Person;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->isPendingOperationEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {p2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->removePendingOperation(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->suggestions:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v1, p2}, Lcom/google/android/social/api/people/model/PersonList;->hasPerson(Lcom/google/android/social/api/people/model/Person;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;

    invoke-virtual {p2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v1}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;->execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->clearButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->hasSearchText:Z

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setSuggestionsInAdapter()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    sget-object v1, Lcom/google/android/social/api/logging/Aspen$Action;->PEOPLE_SEARCH_TEXT_CLEARED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    sget-object v2, Lcom/google/android/social/api/logging/Aspen$View;->PEOPLE_SUGGEST_WIDGET:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLogAction(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->doneButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setResultAndFinish(I)V

    goto :goto_0
.end method

.method public onConnected()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->search(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-interface {v2, p0, v3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;->execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x7d2

    :try_start_0
    invoke-virtual {p1, p0, v1}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v1}, Lcom/google/android/social/api/service/PlusInternalClient;->connect()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/4 v12, -0x1

    const/4 v9, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8, v9}, Landroid/view/Window;->requestFeature(I)Z

    const v0, 0x7f0400e9

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setContentView(I)V

    const-string v0, "plus_people_suggest_preferences"

    invoke-virtual {p0, v0, v10}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v0, "suggest_popup_shown"

    invoke-interface {v7, v0, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->popupAlreadyShown:Z

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    const-string v1, "Account name must not be null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v0, 0x7f0700dd

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setTitle(I)V

    new-instance v0, Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    invoke-direct {v0, p0, p0, p0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    new-instance v0, Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    new-instance v3, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientConnectionCallbacks;

    invoke-direct {v3, p0, v11}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientConnectionCallbacks;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;)V

    new-instance v4, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;

    invoke-direct {v4, p0, v11}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;)V

    sget-object v5, Lcom/google/android/social/api/network/ApiaryHttpRequest;->OAUTH2_SCOPES:[Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    new-instance v0, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-direct {v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setUpSearchBox()V

    if-eqz p1, :cond_1

    const-string v0, "hasSearchText"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->hasSearchText:Z

    const-string v0, "personBeingEdited"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/Person;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    const-string v0, "currentlyDisplayedList"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/PersonList;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    if-eqz v0, :cond_0

    move v0, v9

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->displayListLoaded:Z

    const-string v0, "peopleAddedFromSearch"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/PersonList;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    const-string v0, "loggedSuggestionsShown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->loggedSuggestionsShown:Ljava/util/HashSet;

    const-string v0, "setOfPeopleAdded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setOfPeopleAdded:Ljava/util/HashSet;

    const-string v0, "numberOfSuggestionsLoaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->numberOfSuggestionsLoaded:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v12, v12}, Landroid/view/Window;->setLayout(II)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingLockDistance:F

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0, v6}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->showInstallGooglePlayServicesDialog(I)V

    iput-boolean v9, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->installingGooglePlayServices:Z

    :goto_2
    return-void

    :cond_0
    move v0, v10

    goto :goto_0

    :cond_1
    iput v10, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->numberOfSuggestionsLoaded:I

    iput-boolean v10, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->hasSearchText:Z

    iput-boolean v10, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->popupAlreadyShown:Z

    iput-object v11, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    iput-object v11, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->pendingSearchQuery:Ljava/lang/String;

    iput-object v11, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    iput-boolean v10, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->displayListLoaded:Z

    iput-object v11, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    new-instance v0, Lcom/google/android/social/api/people/model/PersonList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/social/api/people/model/PersonList;-><init>(Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->loggedSuggestionsShown:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setOfPeopleAdded:Ljava/util/HashSet;

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onGooglePlayServicesInstalled()V

    goto :goto_2
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Lcom/google/android/social/api/people/loaders/AudienceListLoader;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/social/api/people/loaders/AudienceListLoader;-><init>(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Lcom/google/android/social/api/people/loaders/PeopleListLoader;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/google/android/social/api/people/loaders/PeopleListLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "searchText"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->accountName:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->clearCache()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->destroy(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    :cond_0
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_1
    return v2
.end method

.method public onExecuteTextChanged()V
    .locals 5

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->clearButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_0

    const/4 v2, 0x4

    :goto_0
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    iput-boolean v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->hasSearchText:Z

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setSuggestionsInAdapter()V

    :goto_1
    return-void

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    if-nez v2, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->search(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentSearchQuery:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->pendingSearchQuery:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->pendingSearchQuery:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iput-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->pendingSearchQuery:Ljava/lang/String;

    goto :goto_1
.end method

.method public onHideProgressDialog()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getProgressDialog()Lvedroid/support/v4/app/DialogFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method public onIgnoreSuggestion(Lcom/google/android/gms/common/ConnectionResult;Z)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Z

    return-void
.end method

.method public onLoadFinished(Lvedroid/support/v4/content/Loader;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 5
    .param p2    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            ">;",
            "Lcom/google/android/gms/common/ConnectionResult;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0x7d2

    :try_start_0
    invoke-virtual {p2, p0, v4}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->reset()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/social/api/people/loaders/AudienceListLoader;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->getAudience()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p0, p2, v4}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onLoadSocialNetwork(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    goto :goto_0

    :pswitch_1
    move-object v2, p1

    check-cast v2, Lcom/google/android/social/api/people/loaders/PeopleListLoader;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/loaders/PeopleListLoader;->getPersonList()Lcom/google/android/social/api/people/model/PersonList;

    move-result-object v4

    invoke-direct {p0, p2, v4}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onPeopleList(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    goto :goto_0

    :pswitch_2
    move-object v3, p1

    check-cast v3, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->getPersonList()Lcom/google/android/social/api/people/model/PersonList;

    move-result-object v4

    invoke-direct {p0, p2, v4}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onLoadFinished(Lvedroid/support/v4/content/Loader;Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public onLoadSocialNetwork(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->circles:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method

.method public onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->installingGooglePlayServices:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->disconnect()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->clearPendingOperation()V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onPause()V

    return-void
.end method

.method public onPersonClicked(Lcom/google/android/social/api/people/model/Person;)V
    .locals 6
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getFriendsCircle()Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getCircleCount()I

    move-result v3

    if-gtz v3, :cond_0

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->editCircles(Lcom/google/android/social/api/people/model/Person;)V

    :goto_0
    return-void

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->addPendingOperation(Ljava/lang/String;I)V

    new-instance v3, Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getPerson()Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/social/api/people/model/Person;-><init>(Lcom/google/android/social/api/people/model/AudienceMember;Ljava/util/ArrayList;)V

    invoke-direct {p0, v3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->updatePerson(Lcom/google/android/social/api/people/model/Person;)V

    new-instance v0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;

    invoke-direct {v0, p1, v2}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;-><init>(Lcom/google/android/social/api/people/model/Person;Ljava/util/ArrayList;)V

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v3}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0, p0, v3}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;->execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onPersonShown(Lcom/google/android/social/api/people/model/Person;)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->checkHasSearchText()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->loggedSuggestionsShown:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->loggedSuggestionsShown:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v1}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;->execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClientRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onPostInsertLog(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->installingGooglePlayServices:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->connect()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getProgressDialog()Lvedroid/support/v4/app/DialogFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {v1, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->destroy(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v1, "hasSearchText"

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->checkHasSearchText()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "personBeingEdited"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "currentlyDisplayedList"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->currentlyDisplayedList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "peopleAddedFromSearch"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->peopleAddedFromSearch:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "loggedSuggestionsShown"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->loggedSuggestionsShown:Ljava/util/HashSet;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "setOfPeopleAdded"

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setOfPeopleAdded:Ljava/util/HashSet;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "numberOfSuggestionsLoaded"

    iget v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->numberOfSuggestionsLoaded:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onShowErrorDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-nez v0, :cond_0

    const v1, 0x7f0700e7

    invoke-static {v1}, Lcom/google/android/social/api/people/fragments/ErrorDialogFragment;->createErrorDialog(I)Lcom/google/android/social/api/people/fragments/ErrorDialogFragment;

    move-result-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingOriginX:F

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->searchText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    move v2, v1

    :goto_1
    return v2

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingOriginX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingOriginY:F

    iput-object v5, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move v0, v1

    :goto_2
    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    :cond_1
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipeDirectionLocked:Z

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :pswitch_1
    iget-boolean v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipeDirectionLocked:Z

    if-nez v3, :cond_4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingLockDistance:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    iput-boolean v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipeDirectionLocked:Z

    :cond_4
    :goto_4
    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->getWidth()I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    const/high16 v3, 0x3f800000

    invoke-virtual {v1, v0, v3, v2}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->setSwipeOffset(FFZ)V

    goto :goto_1

    :cond_5
    iget v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingOriginY:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->scrollingLockDistance:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iput-boolean v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipeDirectionLocked:Z

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {v3, v2}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->resetSwipeOffset(Z)V

    iput-object v5, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    goto :goto_4

    :cond_6
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3f000000

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    const v4, 0x3dcccccd

    sub-float v5, v1, v0

    div-float v1, v5, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->setSwipeOffset(FFZ)V

    goto/16 :goto_1

    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipeDirectionLocked:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    if-eqz v2, :cond_7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->getPerson()Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->ignorePerson(Lcom/google/android/social/api/people/model/Person;)V

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->startRemovalAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    new-instance v3, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/people/model/Person;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v5, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    :cond_7
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->swipingView:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->resetSwipeOffset(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->destroy(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->installingGooglePlayServices:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->installingGooglePlayServices:Z

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->setResultAndFinish(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onGooglePlayServicesInstalled()V

    goto :goto_0
.end method

.method public shouldPopupShow(Lcom/google/android/social/api/people/model/Person;I)Z
    .locals 5
    .param p1    # Lcom/google/android/social/api/people/model/Person;
    .param p2    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getCircleCount()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->popupAlreadyShown:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->popupAlreadyShown:Z

    new-instance v3, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;

    invoke-direct {v3, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    new-array v4, v1, [Ljava/lang/Void;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Void;

    aput-object v0, v4, v2

    invoke-virtual {v3, v4}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v1

    goto :goto_0
.end method
