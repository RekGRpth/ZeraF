.class Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;
.super Landroid/view/animation/Animation;
.source "PeopleSuggestRowView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->startRemovalAnimation()Landroid/view/animation/Animation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

.field final synthetic val$initialHeight:I

.field final synthetic val$targetView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/social/api/people/views/PeopleSuggestRowView;Landroid/view/View;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;->this$0:Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    iput-object p2, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;->val$targetView:Landroid/view/View;

    iput p3, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;->val$initialHeight:I

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .param p1    # F
    .param p2    # Landroid/view/animation/Transformation;

    const/high16 v2, 0x3f800000

    cmpl-float v0, p1, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;->val$targetView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;->val$targetView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;->val$targetView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;->val$initialHeight:I

    int-to-float v1, v1

    sub-float/2addr v2, p1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method
