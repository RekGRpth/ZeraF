.class public Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "BaseAudienceSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;
.implements Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;


# instance fields
.field private mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

.field private mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

.field private mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

.field private mCircles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclesLoaded:Z

.field private mGroups:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mGroupsLoaded:Z

.field private mPeople:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mPeopleLoaded:Z

.field private mSearchResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mViewList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    iput-boolean v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public audienceMemberAdded(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
    .param p2    # Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->clear()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getAudienceSelections()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->addAll(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method public audienceMemberRemoved(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
    .param p2    # Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v0, p2}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->remove(Lcom/google/android/social/api/people/model/AudienceMember;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method public audienceMemberRemoved(Lcom/google/android/social/api/people/views/AudienceView;Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/views/AudienceView;
    .param p2    # Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->getAudience()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method protected buildResult()Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2

    new-instance v0, Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-direct {v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;-><init>()V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->getAudience()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setAudience(Ljava/util/ArrayList;)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    return-object v0
.end method

.method protected cancel()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->finish()V

    return-void
.end method

.method protected createAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
    .locals 1

    new-instance v0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    return-object v0
.end method

.method protected final getAudienceIntent()Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    return-object v0
.end method

.method protected getAudienceView()Lcom/google/android/social/api/people/views/AudienceView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    return-object v0
.end method

.method protected final getCirclesLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    return v0
.end method

.method protected final getGroupsLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    return v0
.end method

.method protected getListView()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    return-object v0
.end method

.method protected final getPeopleLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    return v0
.end method

.method protected ok()V
    .locals 2

    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->buildResult()Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->asIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0801ce

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->ok()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0801cd

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->cancel()V

    goto :goto_0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const v10, 0x7f08012b

    const/4 v9, 0x1

    const/4 v8, -0x1

    const v7, 0x7f0801cd

    const/16 v5, 0x8

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v4, Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/google/android/social/api/people/AudienceSelectionIntent;-><init>(Landroid/content/Intent;)V

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    const v4, 0x7f0400e3

    invoke-virtual {p0, v4}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->setContentView(I)V

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getTitle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getShowCancel()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p0, v7}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getOkText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    const v4, 0x7f0801ce

    invoke-virtual {p0, v4}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iget-object v6, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v6}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getOkText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getDescription()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {p0, v10}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v6}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->createAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    invoke-virtual {v4, p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->setAudienceChangedListener(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;)V

    const v4, 0x7f0801c8

    invoke-virtual {p0, v4}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v4, p0}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->setAudienceChangedListener(Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;)V

    iget-object v6, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getShowChips()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v6, v4}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->setVisibility(I)V

    const v4, 0x7f0801cc

    invoke-virtual {p0, v4}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    invoke-virtual {v4, v9}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    const v4, 0x7f0801ce

    invoke-virtual {p0, v4}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v7}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v8, v8}, Landroid/view/Window;->setLayout(II)V

    if-nez p1, :cond_9

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getAudience()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getKnownAudienceMembers()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    const/4 v1, 0x0

    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_a

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/AudienceMember;->isPerson()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    const v4, 0x7f0700d8

    invoke-virtual {p0, v4}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->setTitle(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getCancelText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v7}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iget-object v6, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v6}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getCancelText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p0, v10}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_6
    move v4, v5

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/AudienceMember;->isCircle()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/AudienceMember;->isSystemGroup()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    const-string v4, "groups"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    const-string v4, "circles"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    const-string v4, "people"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    const-string v4, "searchresults"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    const-string v4, "groups.loaded"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    const-string v4, "circles.loaded"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    const-string v4, "people.loaded"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    const-string v4, "selections"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    :cond_a
    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    invoke-virtual {v4, v3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v4, v3}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->set(Ljava/util/List;)V

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    invoke-virtual {v4, v9}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v4, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->swapContents()V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->onCreateComplete()V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method protected onCreateComplete()V
    .locals 0

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "groups.loaded"

    iget-boolean v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "circles.loaded"

    iget-boolean v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "people.loaded"

    iget-boolean v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "groups"

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "circles"

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "people"

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "searchresults"

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "selections"

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->getAudience()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method protected selectionChanged()V
    .locals 2

    const v1, 0x7f0801ce

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/social/api/people/AudienceSelectionIntent;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getAllowEmptySelection()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected final swapContents()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method
