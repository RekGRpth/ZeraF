.class public Lcom/google/android/social/api/people/views/PeopleSuggestRowView;
.super Landroid/widget/FrameLayout;
.source "PeopleSuggestRowView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;
    }
.end annotation


# instance fields
.field private addToCircleButton:Landroid/widget/Button;

.field private addingToCircleLayout:Landroid/widget/LinearLayout;

.field private addingToCircleTextView:Landroid/widget/TextView;

.field private avatarView:Lcom/google/android/social/api/views/PlusInternalApiImageView;

.field private listener:Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

.field private nameTextView:Landroid/widget/TextView;

.field private normalRowLayout:Landroid/widget/RelativeLayout;

.field private offsetX:F

.field private person:Lcom/google/android/social/api/people/model/Person;

.field private popupAnchor:Lcom/google/android/social/api/views/PopupAnchorView;

.field private reusable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->offsetX:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->reusable:Z

    return-void
.end method

.method private getStateText(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0700e0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0700e1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setState(I)V
    .locals 5
    .param p1    # I

    const/4 v3, 0x4

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->normalRowLayout:Landroid/widget/RelativeLayout;

    if-nez p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addingToCircleLayout:Landroid/widget/LinearLayout;

    if-nez p1, :cond_2

    :goto_1
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->getStateText(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addingToCircleTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1
.end method


# virtual methods
.method public getPerson()Lcom/google/android/social/api/people/model/Person;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->person:Lcom/google/android/social/api/people/model/Person;

    return-object v0
.end method

.method public isReusable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->reusable:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->listener:Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-interface {v0, v1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;->onPersonClicked(Lcom/google/android/social/api/people/model/Person;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0801e5

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->nameTextView:Landroid/widget/TextView;

    const v0, 0x7f0801e7

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addToCircleButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addToCircleButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0801e0

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addingToCircleLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0801e2

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addingToCircleTextView:Landroid/widget/TextView;

    const v0, 0x7f0801e3

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->normalRowLayout:Landroid/widget/RelativeLayout;

    const v0, 0x7f0801e4

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/views/PlusInternalApiImageView;

    iput-object v0, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->avatarView:Lcom/google/android/social/api/views/PlusInternalApiImageView;

    return-void
.end method

.method public resetSwipeOffset(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->setSwipeOffset(FFZ)V

    return-void
.end method

.method public setPersonRowListener(Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

    iput-object p1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->listener:Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

    return-void
.end method

.method public setSwipeOffset(FFZ)V
    .locals 6
    .param p1    # F
    .param p2    # F
    .param p3    # Z

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p3, :cond_0

    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v2, p1, p1, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    :goto_0
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, p2, p2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    if-eqz p3, :cond_1

    const-wide/16 v3, 0x1

    :goto_1
    invoke-virtual {v0, v3, v4}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    invoke-virtual {v0, v5}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const v3, 0x7f0801df

    invoke-virtual {p0, v3}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iput p1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->offsetX:F

    return-void

    :cond_0
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget v3, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->offsetX:F

    invoke-direct {v2, v3, p1, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_0

    :cond_1
    const-wide/16 v3, 0x64

    goto :goto_1
.end method

.method public setupView(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/model/Person;IZ)V
    .locals 8
    .param p1    # Lcom/google/android/social/api/service/PlusInternalClient;
    .param p2    # Lcom/google/android/social/api/people/model/Person;
    .param p3    # I
    .param p4    # Z

    const/4 v7, 0x1

    const/4 v6, 0x0

    iput-object p2, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {p2}, Lcom/google/android/social/api/people/model/Person;->getCircleCount()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addToCircleButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700e3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/social/api/people/model/Person;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->avatarView:Lcom/google/android/social/api/views/PlusInternalApiImageView;

    invoke-virtual {p2}, Lcom/google/android/social/api/people/model/Person;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200e7

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->initialize(Lcom/google/android/social/api/service/PlusInternalClient;Ljava/lang/String;I)V

    invoke-direct {p0, p3}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->setState(I)V

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    invoke-virtual {p0, v1, v2, v7}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->setSwipeOffset(FFZ)V

    const v1, 0x7f0801e6

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/views/PopupAnchorView;

    iput-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->popupAnchor:Lcom/google/android/social/api/views/PopupAnchorView;

    if-nez p4, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->popupAnchor:Lcom/google/android/social/api/views/PopupAnchorView;

    invoke-virtual {v1}, Lcom/google/android/social/api/views/PopupAnchorView;->isPopupShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->popupAnchor:Lcom/google/android/social/api/views/PopupAnchorView;

    const/16 v2, 0x55

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/views/PopupAnchorView;->setAnchorLayout(I)V

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->popupAnchor:Lcom/google/android/social/api/views/PopupAnchorView;

    const/16 v2, 0x35

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/views/PopupAnchorView;->setAnchorPopup(I)V

    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->popupAnchor:Lcom/google/android/social/api/views/PopupAnchorView;

    const v2, 0x7f0400ea

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/views/PopupAnchorView;->showPopup(I)V

    :goto_1
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addToCircleButton:Landroid/widget/Button;

    const v2, 0x7f0700e6

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addToCircleButton:Landroid/widget/Button;

    invoke-virtual {p2, v6}, Lcom/google/android/social/api/people/model/Person;->getCircle(I)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->popupAnchor:Lcom/google/android/social/api/views/PopupAnchorView;

    invoke-virtual {v1}, Lcom/google/android/social/api/views/PopupAnchorView;->dismissPopup()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startRemovalAnimation()Landroid/view/animation/Animation;
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->reusable:Z

    iget-object v3, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->normalRowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->addingToCircleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->getHeight()I

    move-result v1

    const v3, 0x7f0801df

    invoke-virtual {p0, v3}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView$1;-><init>(Lcom/google/android/social/api/people/views/PeopleSuggestRowView;Landroid/view/View;I)V

    const-wide/16 v3, 0xfa

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-object v0
.end method
