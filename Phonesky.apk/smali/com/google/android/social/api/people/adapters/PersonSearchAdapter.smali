.class public Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;
.super Landroid/widget/BaseAdapter;
.source "PersonSearchAdapter.java"


# instance fields
.field private inflater:Landroid/view/LayoutInflater;

.field private suggestResults:Lcom/google/android/social/api/people/model/PersonList;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/LayoutInflater;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->inflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->suggestResults:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/google/android/social/api/people/model/Person;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->suggestResults:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/PersonList;->getPerson(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/Person;->getStableId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0400e6

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/people/views/AudienceSelectionPersonView;

    const v2, 0x7f0801d0

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/people/views/AudienceSelectionPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/Person;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/people/views/AudienceSelectionPersonView;->setName(Ljava/lang/String;)V

    :cond_0
    return-object v1
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setSearchResults(Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->notifyDataSetChanged()V

    return-void
.end method
