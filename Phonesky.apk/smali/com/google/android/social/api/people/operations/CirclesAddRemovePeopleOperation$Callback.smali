.class public interface abstract Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;
.super Ljava/lang/Object;
.source "CirclesAddRemovePeopleOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onCirclesAddRemovePeople(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/Person;)V
.end method
