.class public Lcom/google/android/social/api/people/model/PersonList;
.super Ljava/lang/Object;
.source "PersonList.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/social/api/people/model/PersonList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final people:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/Person;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/social/api/people/model/PersonList$1;

    invoke-direct {v0}, Lcom/google/android/social/api/people/model/PersonList$1;-><init>()V

    sput-object v0, Lcom/google/android/social/api/people/model/PersonList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/social/api/people/model/Person;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/social/api/people/model/PersonList$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList$1;

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/model/PersonList;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/Person;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    return-void
.end method

.method public static getUnion(Lcom/google/android/social/api/people/model/PersonList;Lcom/google/android/social/api/people/model/PersonList;)Lcom/google/android/social/api/people/model/PersonList;
    .locals 7
    .param p0    # Lcom/google/android/social/api/people/model/PersonList;
    .param p1    # Lcom/google/android/social/api/people/model/PersonList;

    const/4 v5, 0x0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    if-eqz p0, :cond_2

    iget-object v6, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_0
    if-eqz p1, :cond_0

    iget-object v6, p1, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    add-int v6, v3, v5

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    iget-object v6, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v3, v5

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v5, :cond_5

    iget-object v6, p1, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    new-instance v6, Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {v6, v4}, Lcom/google/android/social/api/people/model/PersonList;-><init>(Ljava/util/ArrayList;)V

    return-object v6
.end method


# virtual methods
.method public addToTop(Lcom/google/android/social/api/people/model/Person;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPerson(I)Lcom/google/android/social/api/people/model/Person;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/Person;

    return-object v0
.end method

.method public getPersonCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public hasPerson(Lcom/google/android/social/api/people/model/Person;)Z
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/model/PersonList;->getPerson(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public prependIfNotPresent(Lcom/google/android/social/api/people/model/Person;)Z
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/model/PersonList;->hasPerson(Lcom/google/android/social/api/people/model/Person;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/model/PersonList;->addToTop(Lcom/google/android/social/api/people/model/Person;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removePerson(Ljava/lang/String;)Lcom/google/android/social/api/people/model/Person;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/model/Person;

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public removePersonById(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/model/PersonList;->getPerson(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/model/PersonList;->removePerson(Ljava/lang/String;)Lcom/google/android/social/api/people/model/Person;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setPerson(ILcom/google/android/social/api/people/model/Person;)Z
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/social/api/people/model/Person;

    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/social/api/people/model/PersonList;->people:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    return-void
.end method
