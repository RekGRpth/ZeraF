.class public Lcom/google/android/social/api/views/PlusInternalApiImageView;
.super Landroid/widget/ImageView;
.source "PlusInternalApiImageView.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;


# instance fields
.field private apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

.field private callbacksRegistered:Z

.field private shouldLoad:Z

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private load(Z)V
    .locals 4
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->url:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->shouldLoad:Z

    if-nez v0, :cond_2

    if-eqz p1, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->getHeight()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v1, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->url:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->getHeight()I

    move-result v3

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/google/android/social/api/service/PlusInternalClient;->downloadImage(Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;Ljava/lang/String;II)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->shouldLoad:Z

    goto :goto_0
.end method

.method private registerCallbacks()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->callbacksRegistered:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/service/PlusInternalClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->callbacksRegistered:Z

    :cond_0
    return-void
.end method

.method private unregisterCallbacks()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->callbacksRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0, p0}, Lcom/google/android/social/api/service/PlusInternalClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->callbacksRegistered:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public initialize(Lcom/google/android/social/api/service/PlusInternalClient;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/service/PlusInternalClient;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p0, p3}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->registerCallbacks()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->url:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p2, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->url:Ljava/lang/String;

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->load(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->registerCallbacks()V

    return-void
.end method

.method public onConnected()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->load(Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->unregisterCallbacks()V

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onDownloadImage(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/String;IILandroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/social/api/views/PlusInternalApiImageView;->url:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p5, :cond_0

    invoke-virtual {p0, p5}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/social/api/views/PlusInternalApiImageView;->load(Z)V

    :cond_0
    return-void
.end method
