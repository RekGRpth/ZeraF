.class Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;
.super Lcom/android/volley/toolbox/JsonRequest;
.source "ApiaryJsonHttpRequest.java"

# interfaces
.implements Lcom/google/android/social/api/network/ApiaryNetworkStack$HttpRequestMethod;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "VolleyJsonRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/volley/toolbox/JsonRequest",
        "<TData;>;",
        "Lcom/google/android/social/api/network/ApiaryNetworkStack$HttpRequestMethod;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;Lcom/google/android/social/api/network/RequestFuture;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/network/RequestFuture",
            "<TData;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;->this$0:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    # getter for: Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    invoke-static {p1}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->access$000(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->url:Ljava/lang/String;

    # invokes: Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->getRequestString()Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->access$100(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2, p2}, Lcom/android/volley/toolbox/JsonRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    return-void
.end method


# virtual methods
.method public getHeaders()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;->this$0:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    invoke-super {p0}, Lcom/android/volley/toolbox/JsonRequest;->getHeaders()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->getHeaders(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getMethodNotVolley()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;->this$0:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    # getter for: Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    invoke-static {v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->access$000(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getPostBody()[B
    .locals 7

    iget-object v2, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;->this$0:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    invoke-virtual {v2}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->buildRequest()Lcom/google/android/apps/plus/json/GenericJson;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;->this$0:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    # getter for: Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    invoke-static {v2}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->access$000(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->jsonWriter:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v0

    # getter for: Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->log:Lcom/google/android/social/api/internal/GLog;
    invoke-static {}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->access$200()Lcom/google/android/social/api/internal/GLog;

    move-result-object v2

    const-string v3, "%s Request (%d bytes)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    array-length v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/social/api/internal/GLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getPostBodyContentType()Ljava/lang/String;
    .locals 1

    const-string v0, "application/json"

    return-object v0
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 2
    .param p1    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<TData;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;->this$0:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    invoke-virtual {p0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;->getCacheEntry()Lcom/android/volley/Cache$Entry;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->processResponseData(Lcom/android/volley/NetworkResponse;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v0

    return-object v0
.end method
