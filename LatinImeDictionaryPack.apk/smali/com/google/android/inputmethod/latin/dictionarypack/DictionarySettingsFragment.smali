.class public Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "DictionarySettingsFragment.java"

# interfaces
.implements Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mChangedSettings:Z

.field private final mConnectivityChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mLoadingView:Landroid/view/View;

.field private mUpdateNowMenu:Landroid/view/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$1;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)V

    iput-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mConnectivityChangedReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->refreshNetworkState()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->refreshInterface()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Landroid/preference/PreferenceGroup;)V
    .locals 0
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;
    .param p1    # Landroid/preference/PreferenceGroup;

    invoke-direct {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->removeAnyDictSettings(Landroid/preference/PreferenceGroup;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mLoadingView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mUpdateNowMenu:Landroid/view/MenuItem;

    return-object v0
.end method

.method private cancelRefresh()V
    .locals 1

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->unregisterUpdateEventListener(Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->cancelUpdate(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->stopLoadingAnimation()V

    return-void
.end method

.method private createErrorMessage(Landroid/app/Activity;I)Landroid/preference/Preference;
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # I

    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Landroid/preference/Preference;->setTitle(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    return-object v0
.end method

.method private createInstalledDictSettingsCollection()Ljava/util/Collection;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<+",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "content"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const/high16 v5, 0x7f070000

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "list"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    if-nez v11, :cond_0

    const/4 v12, 0x0

    :goto_0
    if-nez v12, :cond_1

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    const v2, 0x7f070011

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->createErrorMessage(Landroid/app/Activity;I)Landroid/preference/Preference;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    return-object v22

    :cond_0
    invoke-virtual {v11}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    goto :goto_0

    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    const v2, 0x7f070012

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->createErrorMessage(Landroid/app/Activity;I)Landroid/preference/Preference;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v24

    new-instance v21, Ljava/util/TreeMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/TreeMap;-><init>()V

    const-string v2, "id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    const-string v2, "version"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    const-string v2, "locale"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    const-string v2, "description"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    const-string v2, "status"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    :cond_3
    invoke-interface {v12, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    new-instance v8, Ljava/util/Locale;

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/LocaleUtils;->getMatchLevel(Ljava/lang/String;Ljava/lang/String;)I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/google/android/inputmethod/latin/dictionarypack/LocaleUtils;->getMatchLevelSortedString(I)Ljava/lang/String;

    move-result-object v20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    if-eqz v14, :cond_4

    iget v2, v14, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->hasPriority(II)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    new-instance v4, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    move-object v5, v11

    invoke-direct/range {v4 .. v10}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/Locale;Ljava/lang/String;I)V

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual/range {v21 .. v21}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v22

    goto/16 :goto_1
.end method

.method private findWordListPreference(Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v5, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->TAG:Ljava/lang/String;

    const-string v6, "Could not find the preference group"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {v2}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    :goto_1
    if-ltz v0, :cond_3

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    instance-of v5, v1, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    if-eqz v5, :cond_2

    move-object v3, v1

    check-cast v3, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    iget-object v5, v3, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mId:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    sget-object v5, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not find the preference for a word list id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    goto :goto_0
.end method

.method private hasPriority(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    if-le p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshInterface()V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryService;->getLastUpdateDate(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->createInstalledDictSettingsCollection()Ljava/util/Collection;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f070014

    invoke-virtual {p0, v7}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x11

    invoke-static {v0, v1, v2, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;

    invoke-direct {v6, p0, v4, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$4;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Landroid/preference/PreferenceGroup;Ljava/util/Collection;)V

    invoke-virtual {v0, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private refreshNetworkState()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mUpdateNowMenu:Landroid/view/MenuItem;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mUpdateNowMenu:Landroid/view/MenuItem;

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    goto :goto_0
.end method

.method private removeAnyDictSettings(Landroid/preference/PreferenceGroup;)V
    .locals 2
    .param p1    # Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private startLoadingAnimation()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mLoadingView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mUpdateNowMenu:Landroid/view/MenuItem;

    const v1, 0x7f070018

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    return-void
.end method

.method private startRefresh()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->startLoadingAnimation()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mChangedSettings:Z

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->registerUpdateEventListener(Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$5;

    const-string v2, "updateByHand"

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$5;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Ljava/lang/String;Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$5;->start()V

    return-void
.end method

.method private stopLoadingAnimation()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;

    invoke-direct {v2, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$6;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public downloadedMetadata(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->stopLoadingAnimation()V

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$2;

    const-string v1, "refreshInterface"

    invoke-direct {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$2;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$2;->start()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/high16 v1, 0x7f040000

    invoke-virtual {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->refreshInterface()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const/4 v2, 0x1

    const/4 v1, 0x0

    const v0, 0x7f070013

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mUpdateNowMenu:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mUpdateNowMenu:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->refreshNetworkState()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v1, 0x7f030001

    const/4 v2, 0x1

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mLoadingView:Landroid/view/View;

    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mLoadingView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->startRefresh()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->cancelRefresh()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->unregisterUpdateEventListener(Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;)V

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mConnectivityChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-boolean v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mChangedSettings:Z

    if-eqz v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.inputmethod.latin.dictionarypack.newdict"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mChangedSettings:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mChangedSettings:Z

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->registerUpdateEventListener(Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->mConnectivityChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->refreshNetworkState()V

    return-void
.end method

.method public updateCycleCompleted()V
    .locals 0

    return-void
.end method

.method public wordListDownloadFinished(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->findWordListPreference(Ljava/lang/String;)Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment$3;-><init>(Lcom/google/android/inputmethod/latin/dictionarypack/DictionarySettingsFragment;)V

    invoke-virtual {v0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
