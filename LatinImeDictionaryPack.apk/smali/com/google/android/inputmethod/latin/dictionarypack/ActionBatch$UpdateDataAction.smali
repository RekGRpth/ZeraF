.class public Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;
.super Ljava/lang/Object;
.source "ActionBatch.java"

# interfaces
.implements Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateDataAction"
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field final mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DictionaryProvider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V
    .locals 3
    .param p1    # Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "New UpdateData action : "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 20
    .param p1    # Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->TAG:Ljava/lang/String;

    const-string v3, "UpdateDataAction with a null word list!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v2, v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v3, v3, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    move-object/from16 v0, v17

    invoke-static {v0, v2, v3}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v18

    if-nez v18, :cond_1

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->TAG:Ljava/lang/String;

    const-string v3, "Trying to update data about a non-existing word list. Bailing out."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updating data about a word list : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    const-string v2, "pendingid"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v3, "type"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "status"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v5, v5, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLocale:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v7, v7, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mDescription:Ljava/lang/String;

    const-string v8, "filename"

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v9, v9, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mRemoteFilename:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-wide v10, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLastUpdate:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v12, v12, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mChecksum:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-wide v13, v13, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFileSize:J

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v15, v15, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mFormatVersion:I

    move/from16 v16, v0

    invoke-static/range {v2 .. v16}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->makeContentValues(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JII)Landroid/content/ContentValues;

    move-result-object v19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updating record for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v3, v3, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mDescription:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and locale "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v3, v3, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mLocale:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    const-string v2, "pendingUpdates"

    const-string v3, "id = ? AND version = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0
.end method
