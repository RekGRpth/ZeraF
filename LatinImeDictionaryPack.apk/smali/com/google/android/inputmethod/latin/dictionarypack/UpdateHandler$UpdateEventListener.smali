.class public interface abstract Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler$UpdateEventListener;
.super Ljava/lang/Object;
.source "UpdateHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UpdateEventListener"
.end annotation


# virtual methods
.method public abstract downloadedMetadata(Z)V
.end method

.method public abstract updateCycleCompleted()V
.end method

.method public abstract wordListDownloadFinished(Ljava/lang/String;Z)V
.end method
