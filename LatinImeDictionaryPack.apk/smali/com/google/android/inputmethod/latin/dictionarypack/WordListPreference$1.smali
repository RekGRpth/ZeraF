.class Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;
.super Ljava/lang/Object;
.source "WordListPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;


# direct methods
.method constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    iget v0, v0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->mStatus:I

    # invokes: Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->getActionIdFromStatusAndMenuEntry(I)I
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->access$000(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    # getter for: Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->access$400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unknown menu item pressed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    # invokes: Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->enableDict()V
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->access$100(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    # invokes: Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->disableDict()V
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->access$200(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference$1;->this$0:Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;

    # invokes: Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->deleteDict()V
    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;->access$300(Lcom/google/android/inputmethod/latin/dictionarypack/WordListPreference;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
