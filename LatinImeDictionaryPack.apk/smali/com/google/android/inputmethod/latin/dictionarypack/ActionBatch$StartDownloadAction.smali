.class public Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;
.super Ljava/lang/Object;
.source "ActionBatch.java"

# interfaces
.implements Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StartDownloadAction"
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field final mForceStartNow:Z

.field final mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DictionaryProvider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;Z)V
    .locals 3
    .param p1    # Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "New download action : "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iput-boolean p2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mForceStartNow:Z

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 14
    .param p1    # Landroid/content/Context;

    iget-object v10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    if-nez v10, :cond_1

    sget-object v10, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->TAG:Ljava/lang/String;

    const-string v11, "UpdateAction with a null parameter!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Downloading word list"

    aput-object v12, v10, v11

    invoke-static {v10}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v10, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v11, v11, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v10, v11}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v9

    const-string v10, "status"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const-string v10, "download"

    invoke-virtual {p1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/DownloadManager;

    const/4 v10, 0x2

    if-ne v10, v7, :cond_5

    if-eqz v3, :cond_2

    const/4 v10, 0x1

    new-array v10, v10, [J

    const/4 v11, 0x0

    const-string v12, "pendingid"

    invoke-virtual {v9, v12}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    aput-wide v12, v10, v11

    invoke-virtual {v3, v10}, Landroid/app/DownloadManager;->remove([J)I

    :cond_2
    iget-object v10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v10, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v11, v11, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v10, v11}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAsAvailable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    :cond_3
    :goto_1
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Upgrade word list, downloading"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v12, v12, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mRemoteFilename:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-static {v10}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    if-eqz v3, :cond_0

    iget-object v10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v10, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mRemoteFilename:Ljava/lang/String;

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    new-instance v5, Landroid/app/DownloadManager$Request;

    invoke-direct {v5, v8}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-boolean v10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mForceStartNow:Z

    if-nez v10, :cond_4

    invoke-static {p1}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->getDownloadOverMeteredSetting(Landroid/content/Context;)I

    move-result v4

    const/4 v10, 0x2

    if-ne v10, v4, :cond_6

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    :goto_2
    const v10, 0x7f060001

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    invoke-virtual {v5, v10}, Landroid/app/DownloadManager$Request;->setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;

    :cond_4
    iget-object v10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v10, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mDescription:Ljava/lang/String;

    invoke-virtual {v5, v10}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    const v10, 0x7f060004

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    if-eqz v10, :cond_8

    const/4 v10, 0x0

    :goto_3
    invoke-virtual {v5, v10}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    const v10, 0x7f060002

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    invoke-virtual {v5, v10}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    iget-object v10, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v10, v10, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v11, v11, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v3, v5, v0, v10, v11}, Lcom/google/android/inputmethod/latin/dictionarypack/UpdateHandler;->registerDownloadRequest(Landroid/app/DownloadManager;Landroid/app/DownloadManager$Request;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)J

    move-result-wide v1

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Starting download of"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v8, v10, v11

    const/4 v11, 0x2

    const-string v12, "with id"

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v10}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting download of "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", id : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/log/PrivateLog;->log(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_5
    const/4 v10, 0x1

    if-eq v10, v7, :cond_3

    sget-object v10, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unexpected state of the word list \'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v12, v12, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\' : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " for an upgrade action. Fall back to download."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    const/4 v10, 0x1

    if-ne v10, v4, :cond_7

    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    goto/16 :goto_2

    :cond_7
    const/high16 v10, 0x7f060000

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    invoke-virtual {v5, v10}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    goto/16 :goto_2

    :cond_8
    const/4 v10, 0x2

    goto/16 :goto_3
.end method
