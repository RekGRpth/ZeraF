.class public Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "MetadataDbHelper.java"


# static fields
.field static final DICTIONARIES_LIST_PUBLIC_COLUMNS:[Ljava/lang/String;

.field static final METADATA_TABLE_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-class v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->TAG:Ljava/lang/String;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "pendingid"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v4

    const-string v1, "status"

    aput-object v1, v0, v5

    const-string v1, "id"

    aput-object v1, v0, v6

    const-string v1, "locale"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "filename"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "date"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "checksum"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "filesize"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "version"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "formatversion"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "flags"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "status"

    aput-object v1, v0, v3

    const-string v1, "id"

    aput-object v1, v0, v4

    const-string v1, "locale"

    aput-object v1, v0, v5

    const-string v1, "description"

    aput-object v1, v0, v6

    const-string v1, "date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "filesize"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "version"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->DICTIONARIES_LIST_PUBLIC_COLUMNS:[Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->sInstance:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const-string v0, "pendingUpdates"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static deleteDownloadingEntry(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # J

    const/4 v5, 0x2

    const-string v0, "pendingUpdates"

    const-string v1, "pendingid = ? AND status = ?"

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static deleteEntry(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 5
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v0, "pendingUpdates"

    const-string v1, "id = ? AND version = ?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static getContentValuesByPendingId(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/content/ContentValues;
    .locals 10
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # J

    const/4 v5, 0x0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v3, "pendingid= ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getFirstLineAsContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v9

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v9
.end method

.method public static getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;
    .locals 10
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v5, 0x0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v3, "id= ? AND version= ?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getFirstLineAsContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v9

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v9
.end method

.method public static getContentValuesOfLatestAvailableWordlistById(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 11
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v3, "id= ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const-string v7, "version DESC"

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getFirstLineAsContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v10

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return-object v10
.end method

.method public static getCurrentMetadata(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 9
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v7, "locale"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    return-object v8
.end method

.method public static getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static getDictionariesList(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 9
    .param p0    # Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->DICTIONARIES_LIST_PUBLIC_COLUMNS:[Ljava/lang/String;

    const-string v3, "locale != ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, ""

    aput-object v7, v4, v6

    const-string v7, "locale"

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    return-object v8
.end method

.method private static getFirstLineAsContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 3
    .param p0    # Landroid/database/Cursor;

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/ContentValues;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "pendingid"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "type"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "status"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "id"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putStringResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "locale"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putStringResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "description"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putStringResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "filename"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putStringResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "url"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putStringResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "date"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "checksum"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putStringResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "filesize"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "version"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "formatversion"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    const-string v1, "flags"

    invoke-static {v0, p0, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V

    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->TAG:Ljava/lang/String;

    const-string v2, "Several SQL results when we expected only one!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstalledOrDeletingOrAvailableDictionaryMetadata(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 10
    .param p0    # Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v9, 0x1

    invoke-static {p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v3, "status = ? OR status = ? OR status = ?"

    new-array v4, v7, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v9

    const/4 v6, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const-string v7, "locale"

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    return-object v8
.end method

.method public static getInstalledOrDeletingWordListContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 10
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x3

    const/4 v5, 0x0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v3, "id=? AND (status=? OR status=?)"

    new-array v4, v6, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    const/4 v0, 0x2

    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getFirstLineAsContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v9

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v9
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->sInstance:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;

    invoke-direct {v0, p0}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->sInstance:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;

    :cond_0
    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->sInstance:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isStillWaitingForSomeDownloads(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 10
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    const-string v1, "pendingUpdates"

    sget-object v2, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->METADATA_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v3, "status=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return v9
.end method

.method public static makeContentValues(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JII)Landroid/content/ContentValues;
    .locals 3
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # J
    .param p10    # Ljava/lang/String;
    .param p11    # J
    .param p13    # I
    .param p14    # I

    new-instance v0, Landroid/content/ContentValues;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "pendingid"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "status"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "locale"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "description"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "filename"

    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "url"

    invoke-virtual {v0, v1, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "date"

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "checksum"

    invoke-virtual {v0, v1, p10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "filesize"

    invoke-static {p11, p12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "version"

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "formatversion"

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "flags"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private static markEntryAs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IIJ)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # J

    invoke-static {p0, p1, p2}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    cmp-long v1, v1, p4

    if-eqz v1, :cond_0

    const-string v1, "pendingid"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string v1, "pendingUpdates"

    const-string v2, "id = ? AND version = ?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static markEntryAsAvailable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IIJ)V

    return-void
.end method

.method public static markEntryAsDeleting(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x5

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IIJ)V

    return-void
.end method

.method public static markEntryAsDisabled(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x4

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IIJ)V

    return-void
.end method

.method public static markEntryAsDownloading(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IJ)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J

    const/4 v3, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IIJ)V

    return-void
.end method

.method public static markEntryAsEnabled(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x3

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;IIJ)V

    return-void
.end method

.method public static markEntryAsFinishedDownloadingAndInstalled(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)V
    .locals 14
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Landroid/content/ContentValues;

    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Ended processing a wordlist"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    new-instance v12, Ljava/util/LinkedList;

    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    const-string v1, "pendingUpdates"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "filename"

    aput-object v3, v2, v0

    const-string v3, "locale = ? AND id = ? AND status = ?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "locale"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    const-string v5, "id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x2

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "filename"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Setting for removal"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_2
    const-string v0, "status"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    const-string v0, "pendingUpdates"

    const-string v1, "id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "id"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v0, "pendingUpdates"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    :try_start_0
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static putIntResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/content/ContentValues;
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method private static putStringResult(Landroid/content/ContentValues;Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/content/ContentValues;
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "CREATE TABLE pendingUpdates (pendingid INTEGER, type INTEGER, status INTEGER, id TEXT, locale TEXT, description TEXT, filename TEXT, url TEXT, date INTEGER, checksum TEXT, filesize INTEGER, version INTEGER,formatversion INTEGER,flags INTEGER,PRIMARY KEY (id,version));"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/DefaultMetadata;->getDefaultEntries(Landroid/content/res/Resources;)[Landroid/content/ContentValues;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    const-string v4, "pendingUpdates"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    const-string v0, "DROP TABLE IF EXISTS pendingUpdates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
