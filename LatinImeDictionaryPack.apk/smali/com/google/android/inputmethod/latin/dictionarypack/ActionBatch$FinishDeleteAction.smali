.class public Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;
.super Ljava/lang/Object;
.source "ActionBatch.java"

# interfaces
.implements Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FinishDeleteAction"
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field final mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DictionaryProvider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V
    .locals 3
    .param p1    # Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "New FinishDelete action : "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 10
    .param p1    # Landroid/content/Context;

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    if-nez v4, :cond_0

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->TAG:Ljava/lang/String;

    const-string v5, "FinishDeleteAction with a null word list!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-array v4, v9, [Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Trying to delete word list : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v4, v4, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v5, v5, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v4, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v3

    if-nez v3, :cond_1

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->TAG:Ljava/lang/String;

    const-string v5, "Trying to set a non-existing wordlist for removal. Cancelling."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v4, "status"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v4, 0x5

    if-eq v4, v2, :cond_2

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected status for finish-deleting a word list info : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v4, "url"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "pendingUpdates"

    const-string v5, "id = ? AND version = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v7, v7, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v7, v7, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v4, v4, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v5, v5, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v4, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAsAvailable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
