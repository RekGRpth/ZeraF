.class public Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;
.super Ljava/lang/Object;
.source "ActionBatch.java"

# interfaces
.implements Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DisableAction"
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field final mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DictionaryProvider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;)V
    .locals 3
    .param p1    # Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "New Disable action : "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    if-nez v4, :cond_0

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->TAG:Ljava/lang/String;

    const-string v5, "DisableAction with a null word list!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-array v4, v8, [Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Disabling word list : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v4}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v4, v4, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v5, v5, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v4, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "status"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v4, 0x3

    if-ne v4, v2, :cond_1

    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v4, v4, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v5, v5, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v4, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAsDisabled(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x2

    if-eq v4, v2, :cond_2

    sget-object v4, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected state of the word list \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for a disable action. Fall back to marking as available."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v4, "download"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/DownloadManager;

    if-eqz v1, :cond_3

    new-array v4, v8, [J

    const-string v5, "pendingid"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    aput-wide v5, v4, v7

    invoke-virtual {v1, v4}, Landroid/app/DownloadManager;->remove([J)I

    :cond_3
    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v4, v4, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v5, v5, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v4, v5}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->markEntryAsAvailable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
