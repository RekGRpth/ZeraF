.class Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;
.super Landroid/database/AbstractCursor;
.source "DictionaryProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResourcePathCursor"
.end annotation


# static fields
.field private static final columnNames:[Ljava/lang/String;


# instance fields
.field final mWordLists:[Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "locale"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->columnNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    new-array v0, v1, [Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    invoke-interface {p1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    iput-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mWordLists:[Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    iput v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mPos:I

    return-void
.end method


# virtual methods
.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->columnNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mWordLists:[Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    array-length v0, v0

    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mWordLists:[Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    iget v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mPos:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;->mId:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mWordLists:[Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    iget v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mPos:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;->mLocale:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isNull(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mPos:I

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$ResourcePathCursor;->mWordLists:[Lcom/google/android/inputmethod/latin/dictionarypack/DictionaryProvider$WordListInfo;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
