.class public final Lcom/android/soundrecorder/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept:I = 0x7f080032

.field public static final accessing_db_fail:I = 0x7f080025

.field public static final alert_delete_multiple:I = 0x7f080008

.field public static final alert_delete_single:I = 0x7f080009

.field public static final app_name:I = 0x7f08002b

.field public static final audio_db_album_name:I = 0x7f080035

.field public static final audio_db_artist_name:I = 0x7f08003f

.field public static final audio_db_playlist_name:I = 0x7f080036

.field public static final audio_db_title_format:I = 0x7f08003a

.field public static final button_ok:I = 0x7f080034

.field public static final cancel:I = 0x7f080003

.field public static final delete:I = 0x7f08000b

.field public static final deleting:I = 0x7f08000c

.field public static final deleting_fail:I = 0x7f080024

.field public static final discard:I = 0x7f080033

.field public static final error_app_internal:I = 0x7f080040

.field public static final error_mediadb_new_record:I = 0x7f080038

.field public static final error_sdcard_access:I = 0x7f080037

.field public static final insert_sd_card:I = 0x7f080039

.field public static final max_length_reached:I = 0x7f08002f

.field public static final message_recorded:I = 0x7f08003c

.field public static final min_available:I = 0x7f080030

.field public static final no_recording_file:I = 0x7f08000d

.field public static final not_available:I = 0x7f08002a

.field public static final ok:I = 0x7f08000a

.field public static final player_occupied:I = 0x7f080021

.field public static final playing_fail:I = 0x7f080020

.field public static final record_your_message:I = 0x7f08003b

.field public static final recorder_occupied:I = 0x7f08001f

.field public static final recording:I = 0x7f08002c

.field public static final recording_doc_deleted:I = 0x7f080022

.field public static final recording_effect:I = 0x7f080012

.field public static final recording_effect_AEC:I = 0x7f080027

.field public static final recording_effect_AGC:I = 0x7f080026

.field public static final recording_effect_NS:I = 0x7f080028

.field public static final recording_fail:I = 0x7f08001e

.field public static final recording_file_list:I = 0x7f08000e

.field public static final recording_format_high:I = 0x7f080004

.field public static final recording_format_low:I = 0x7f080006

.field public static final recording_format_mid:I = 0x7f080005

.field public static final recording_format_standard:I = 0x7f080029

.field public static final recording_mode:I = 0x7f080011

.field public static final recording_mode_lecture:I = 0x7f080019

.field public static final recording_mode_meeting:I = 0x7f080018

.field public static final recording_mode_nomal:I = 0x7f080016

.field public static final recording_paused:I = 0x7f08000f

.field public static final recording_stopped:I = 0x7f08002d

.field public static final review_message:I = 0x7f08003d

.field public static final save_record:I = 0x7f080002

.field public static final saving_fail:I = 0x7f080023

.field public static final sd_no:I = 0x7f08001d

.field public static final sd_unmounted:I = 0x7f08001a

.field public static final sec_available:I = 0x7f080031

.field public static final select_recording_effect:I = 0x7f080015

.field public static final select_recording_mode:I = 0x7f080014

.field public static final select_voice_quality:I = 0x7f080013

.field public static final storage_full:I = 0x7f08001c

.field public static final storage_is_full:I = 0x7f08002e

.field public static final storage_low:I = 0x7f08001b

.field public static final tell_save_record_success:I = 0x7f080001

.field public static final three_mins_avail_total:I = 0x7f080007

.field public static final time_available:I = 0x7f080017

.field public static final timer_format:I = 0x7f08003e

.field public static final unknown_artist_name:I = 0x7f080000

.field public static final voice_quality:I = 0x7f080010


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
