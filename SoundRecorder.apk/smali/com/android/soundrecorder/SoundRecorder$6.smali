.class Lcom/android/soundrecorder/SoundRecorder$6;
.super Ljava/lang/Object;
.source "SoundRecorder.java"

# interfaces
.implements Landroid/view/LayoutInflater$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/soundrecorder/SoundRecorder;->addOptionsMenuInflaterFactory()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/SoundRecorder;


# direct methods
.method constructor <init>(Lcom/android/soundrecorder/SoundRecorder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/util/AttributeSet;

    const/4 v6, 0x0

    const-string v5, "com.android.internal.view.menu.ListMenuItemView"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    move-object v3, v6

    :goto_0
    return-object v3

    :cond_0
    invoke-static {}, Lcom/android/soundrecorder/SoundRecorder;->access$400()Ljava/lang/Class;

    move-result-object v5

    if-nez v5, :cond_1

    :try_start_0
    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-virtual {v5}, Landroid/content/ContextWrapper;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/android/soundrecorder/SoundRecorder;->access$402(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    invoke-static {}, Lcom/android/soundrecorder/SoundRecorder;->access$400()Ljava/lang/Class;

    move-result-object v5

    if-nez v5, :cond_2

    move-object v3, v6

    goto :goto_0

    :catch_0
    move-exception v2

    move-object v3, v6

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/soundrecorder/SoundRecorder;->access$500()Ljava/lang/reflect/Constructor;

    move-result-object v5

    if-nez v5, :cond_3

    :try_start_1
    invoke-static {}, Lcom/android/soundrecorder/SoundRecorder;->access$400()Ljava/lang/Class;

    move-result-object v5

    invoke-static {}, Lcom/android/soundrecorder/SoundRecorder;->access$600()[Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    invoke-static {v5}, Lcom/android/soundrecorder/SoundRecorder;->access$502(Ljava/lang/reflect/Constructor;)Ljava/lang/reflect/Constructor;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_3
    invoke-static {}, Lcom/android/soundrecorder/SoundRecorder;->access$500()Ljava/lang/reflect/Constructor;

    move-result-object v5

    if-nez v5, :cond_4

    move-object v3, v6

    goto :goto_0

    :catch_1
    move-exception v2

    move-object v3, v6

    goto :goto_0

    :catch_2
    move-exception v2

    move-object v3, v6

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    const/4 v5, 0x2

    :try_start_2
    new-array v1, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v1, v5

    const/4 v5, 0x1

    aput-object p3, v1, v5

    invoke-static {}, Lcom/android/soundrecorder/SoundRecorder;->access$500()Ljava/lang/reflect/Constructor;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    move-object v0, v5

    check-cast v0, Landroid/view/View;

    move-object v3, v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_6

    if-nez v3, :cond_5

    move-object v3, v6

    goto :goto_0

    :catch_3
    move-exception v2

    move-object v3, v6

    goto :goto_0

    :catch_4
    move-exception v2

    move-object v3, v6

    goto :goto_0

    :catch_5
    move-exception v2

    move-object v3, v6

    goto :goto_0

    :catch_6
    move-exception v2

    move-object v3, v6

    goto :goto_0

    :cond_5
    move-object v4, v3

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/android/soundrecorder/SoundRecorder$6$1;

    invoke-direct {v6, p0, v4}, Lcom/android/soundrecorder/SoundRecorder$6$1;-><init>(Lcom/android/soundrecorder/SoundRecorder$6;Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v5, "SR/SoundRecorder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<create ListMenuItemView> return view = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
