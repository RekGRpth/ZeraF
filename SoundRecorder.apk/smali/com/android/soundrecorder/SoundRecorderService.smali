.class public Lcom/android/soundrecorder/SoundRecorderService;
.super Landroid/app/Service;
.source "SoundRecorderService.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;
.implements Lcom/android/soundrecorder/Player$PlayerListener;
.implements Lcom/android/soundrecorder/Recorder$RecorderListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;,
        Lcom/android/soundrecorder/SoundRecorderService$SaveDataTask;,
        Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;,
        Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;,
        Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;,
        Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;,
        Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;
    }
.end annotation


# static fields
.field private static final ACTION_GOON_PLAY:Ljava/lang/String; = "goon play"

.field private static final ACTION_GOON_RECORD:Ljava/lang/String; = "goon record"

.field private static final ACTION_PAUSE:Ljava/lang/String; = "pause"

.field private static final ACTION_SHUTDOWN_IPO:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN_IPO"

.field private static final ACTION_STOP:Ljava/lang/String; = "stop"

.field public static final CMDPAUSE:Ljava/lang/String; = "pause"

.field private static final COMMAND:Ljava/lang/String; = "command"

.field public static final EVENT_DISCARD_SUCCESS:I = 0x2

.field public static final EVENT_SAVE_SUCCESS:I = 0x1

.field public static final EVENT_STORAGE_MOUNTED:I = 0x3

.field private static final FACTOR_FOR_SECOND_AND_MINUTE:J = 0x3e8L

.field public static final LOW_STORAGE_THRESHOLD:J = 0x200000L

.field private static final ONE_SECOND:I = 0x3e8

.field private static final PLAYLIST_ID_NULL:I = -0x1

.field private static final RECORDING:Ljava/lang/String; = "Recording"

.field public static final SELECTED_RECORDING_EFFECT_AEC:Ljava/lang/String; = "selected_recording_effect_aec"

.field public static final SELECTED_RECORDING_EFFECT_AEC_TMP:Ljava/lang/String; = "selected_recording_effect_aec_tmp"

.field public static final SELECTED_RECORDING_EFFECT_AGC:Ljava/lang/String; = "selected_recording_effect_agc"

.field public static final SELECTED_RECORDING_EFFECT_AGC_TMP:Ljava/lang/String; = "selected_recording_effect_agc_tmp"

.field public static final SELECTED_RECORDING_EFFECT_NS:Ljava/lang/String; = "selected_recording_effect_ns"

.field public static final SELECTED_RECORDING_EFFECT_NS_TMP:Ljava/lang/String; = "selected_recording_effect_ns_tmp"

.field public static final SELECTED_RECORDING_FORMAT:Ljava/lang/String; = "selected_recording_format"

.field public static final SELECTED_RECORDING_MODE:Ljava/lang/String; = "selected_recording_mode"

.field public static final SOUND_POWER_DOWN_MSG:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field private static final SOUND_RECORDER_DATA:Ljava/lang/String; = "sound_recorder_data"

.field private static final START_NOTIFICATION_ID:I = 0x1

.field public static final STATE_IDLE:I = 0x1

.field public static final STATE_PAUSE_PLAYING:I = 0x5

.field public static final STATE_PAUSE_RECORDING:I = 0x3

.field public static final STATE_PLAYING:I = 0x4

.field public static final STATE_RECORDING:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SR/SoundRecorderService"

.field private static final VOLUME_NAME:Ljava/lang/String; = "external"

.field private static final WAIT_TIME:J = 0x3e8L


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mBinder:Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;

.field private mConnection:Landroid/media/MediaScannerConnection;

.field private mCurrentFileDuration:J

.field private mCurrentFilePath:Ljava/lang/String;

.field private mCurrentRecordParams:Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

.field private mCurrentState:I

.field private mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

.field private mFileObserverHandler:Landroid/os/Handler;

.field private mFilePathToScan:Ljava/lang/String;

.field private mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mGetFocus:Z

.field private final mHandler:Landroid/os/Handler;

.field private mNotificationView:Landroid/widget/RemoteViews;

.field private mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

.field private mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

.field private mOnStateChangedListener:Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

.field private mOnUpdateTimeViewListener:Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

.field private mOtherBroastReceiver:Landroid/content/BroadcastReceiver;

.field private mPlayer:Lcom/android/soundrecorder/Player;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mRecorder:Lcom/android/soundrecorder/Recorder;

.field private mRemainingTime:J

.field private mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

.field private mRunForeground:Z

.field private mSelectEffectArray:[Z

.field private mSelectEffectArrayTemp:[Z

.field private mSelectedFormat:I

.field private mSelectedMode:I

.field private mShowNotifiaction:Z

.field private mStorageBroastReceiver:Landroid/content/BroadcastReceiver;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mTotalRecordingDuration:J

.field private final mUpdateTimer:Ljava/lang/Runnable;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;-><init>(Lcom/android/soundrecorder/SoundRecorderService;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mBinder:Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnStateChangedListener:Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mAudioManager:Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageManager:Landroid/os/storage/StorageManager;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentRecordParams:Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mConnection:Landroid/media/MediaScannerConnection;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUri:Landroid/net/Uri;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFilePathToScan:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mTotalRecordingDuration:J

    iput v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageBroastReceiver:Landroid/content/BroadcastReceiver;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOtherBroastReceiver:Landroid/content/BroadcastReceiver;

    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRunForeground:Z

    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mShowNotifiaction:Z

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserverHandler:Landroid/os/Handler;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnUpdateTimeViewListener:Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorderService$1;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorderService$1;-><init>(Lcom/android/soundrecorder/SoundRecorderService;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectedFormat:I

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectedMode:I

    new-array v0, v6, [Z

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArray:[Z

    new-array v0, v6, [Z

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArrayTemp:[Z

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPrefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method private abandonAudioFocus()V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    if-ne v2, v0, :cond_1

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<abandonAudioFocus()> abandon audio focus success"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<abandonAudioFocus()> abandon audio focus faild"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/soundrecorder/SoundRecorderService;)I
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/soundrecorder/SoundRecorderService;)J
    .locals 2
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTime:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/android/soundrecorder/SoundRecorderService;J)J
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    return-wide p1
.end method

.method static synthetic access$102(Lcom/android/soundrecorder/SoundRecorderService;J)J
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTime:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/android/soundrecorder/SoundRecorderService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/soundrecorder/SoundRecorderService;->setCurrentFilePath(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1202(Lcom/android/soundrecorder/SoundRecorderService;J)J
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mTotalRecordingDuration:J

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/soundrecorder/SoundRecorderService;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0, p1, p2}, Lcom/android/soundrecorder/SoundRecorderService;->receiveBroadcast(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/soundrecorder/SoundRecorderService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserverHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/RemainingTimeCalculator;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnUpdateTimeViewListener:Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/soundrecorder/SoundRecorderService;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/soundrecorder/SoundRecorderService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/soundrecorder/SoundRecorderService;Z)Z
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/soundrecorder/SoundRecorderService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/soundrecorder/SoundRecorderService;Ljava/io/File;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/android/soundrecorder/SoundRecorderService;->addToMediaDB(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$902(Lcom/android/soundrecorder/SoundRecorderService;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorderService;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUri:Landroid/net/Uri;

    return-object p1
.end method

.method private addToMediaDB(Ljava/io/File;)Landroid/net/Uri;
    .locals 20
    .param p1    # Ljava/io/File;

    const-string v16, "SR/SoundRecorderService"

    const-string v17, "<addToMediaDB> begin"

    invoke-static/range {v16 .. v17}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v16, "SR/SoundRecorderService"

    const-string v17, "<addToMediaDB> file is null, return null"

    invoke-static/range {v16 .. v17}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    :goto_0
    return-object v12

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/android/soundrecorder/SoundRecorderUtils;->deleteFileFromMediaDB(Landroid/content/Context;Ljava/lang/String;)Z

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v5, v6}, Ljava/util/Date;-><init>(J)V

    new-instance v13, Ljava/text/SimpleDateFormat;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f08003a

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    const/16 v14, 0x8

    new-instance v7, Landroid/content/ContentValues;

    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-direct {v7, v0}, Landroid/content/ContentValues;-><init>(I)V

    const-string v16, "is_music"

    const-string v17, "0"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "title"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "date_added"

    const-wide/16 v17, 0x3e8

    div-long v17, v5, v17

    move-wide/from16 v0, v17

    long-to-int v0, v0

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v16, "SR/SoundRecorderService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "<addToMediaDB> File type is "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentRecordParams:Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/android/soundrecorder/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v16, "mime_type"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentRecordParams:Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "artist"

    const/high16 v17, 0x7f080000

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "album"

    const v17, 0x7f080035

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "_data"

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "duration"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/soundrecorder/SoundRecorderService;->mTotalRecordingDuration:J

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v16, "SR/SoundRecorderService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "<addToMediaDB> Reocrding time output to database is :DURATION= "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/android/soundrecorder/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v4, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    :try_start_0
    invoke-virtual {v11, v4, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    :goto_1
    if-nez v12, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    move-object/from16 v16, v0

    const/16 v17, 0xb

    invoke-interface/range {v16 .. v17}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v9

    const-string v16, "SR/SoundRecorderService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "<addToMediaDB> Save in DB failed: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    const-string v16, "SR/SoundRecorderService"

    const-string v17, "<addToMediaDB> Save susceeded in DB"

    invoke-static/range {v16 .. v17}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v16, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/soundrecorder/SoundRecorderService;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/android/soundrecorder/SoundRecorderService;->createPlaylist(Landroid/content/res/Resources;Landroid/content/ContentResolver;)Landroid/net/Uri;

    :cond_2
    invoke-virtual {v12}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v16, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/soundrecorder/SoundRecorderService;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/soundrecorder/SoundRecorderService;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v16

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v11, v3, v1, v2}, Lcom/android/soundrecorder/SoundRecorderService;->addToPlaylist(Landroid/content/ContentResolver;IJ)V

    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/soundrecorder/SoundRecorderService;->mFilePathToScan:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorderService;->mConnection:Landroid/media/MediaScannerConnection;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/media/MediaScannerConnection;->connect()V

    goto/16 :goto_0
.end method

.method private addToPlaylist(Landroid/content/ContentResolver;IJ)V
    .locals 11
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # I
    .param p3    # J

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "count(*)"

    aput-object v3, v2, v0

    const-string v0, "external"

    invoke-static {v0, p3, p4}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_1

    const-string v0, "SR/SoundRecorderService"

    const-string v3, "<addToPlaylist> cursor is null"

    invoke-static {v0, v3}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    new-instance v10, Landroid/content/ContentValues;

    const/4 v0, 0x2

    invoke-direct {v10, v0}, Landroid/content/ContentValues;-><init>(I)V

    const-string v0, "play_order"

    add-int v3, v6, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "audio_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v9, 0x0

    :try_start_0
    invoke-virtual {p1, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    :goto_1
    if-nez v9, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    const/16 v3, 0xb

    invoke-interface {v0, v3}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    goto :goto_0

    :catch_0
    move-exception v8

    const-string v0, "SR/SoundRecorderService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<addToPlaylist> insert in DB failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x0

    goto :goto_1
.end method

.method private createPlaylist(Landroid/content/res/Resources;Landroid/content/ContentResolver;)Landroid/net/Uri;
    .locals 6
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Landroid/content/ContentResolver;

    new-instance v0, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "name"

    const v4, 0x7f080036

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    const/16 v4, 0xb

    invoke-interface {v3, v4}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    :cond_0
    return-object v2

    :catch_0
    move-exception v1

    const-string v3, "SR/SoundRecorderService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<createPlaylist> insert in DB failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private deleteRecordingFileTmpSuffix()Ljava/lang/String;
    .locals 9

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const-string v6, ".tmp"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const-string v8, ".tmp"

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->stopWatching()V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private getPlaylistId(Landroid/content/res/Resources;)I
    .locals 10
    .param p1    # Landroid/content/res/Resources;

    const/4 v5, 0x1

    const/4 v3, 0x0

    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v3

    const-string v9, "name=?"

    new-array v4, v5, [Ljava/lang/String;

    const v0, 0x7f080036

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "name=?"

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/android/soundrecorder/SoundRecorderUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v8, -0x1

    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    return v8

    :catch_0
    move-exception v7

    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getRecordInfoAfterStopRecord()V
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->getSampleLength()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mTotalRecordingDuration:J

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->getSampleLength()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->getSampleFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/SoundRecorderService;->setCurrentFilePath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->reset()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    :cond_0
    return-void
.end method

.method private hideNotifiaction()V
    .locals 2

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<hideNotifiaction>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRunForeground:Z

    return-void
.end method

.method private isCurrentAccessStorage(Landroid/net/Uri;)Z
    .locals 5
    .param p1    # Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "file"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method private isCurrentFileEndWithTmp()Z
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private receiveBroadcast(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "command"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SR/SoundRecorderService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onReceive> action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentAccessStorage(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v7, v2, :cond_1

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v5, v2, :cond_3

    :cond_1
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    invoke-interface {v2, v6}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    const/16 v3, 0xe

    invoke-interface {v2, v3}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    goto :goto_0

    :cond_4
    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    invoke-interface {v2, v5}, Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;->onEvent(I)V

    goto :goto_1

    :cond_5
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v6, v2, :cond_2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/SoundRecorderService;->showNotification(Landroid/content/Context;)V

    goto :goto_1

    :cond_6
    const-string v2, "com.android.music.musicservicecommand"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "pause"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v7, v2, :cond_7

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v5, v2, :cond_9

    :cond_7
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->stopRecord()Z

    :cond_8
    :goto_2
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->saveRecord()Z

    goto :goto_1

    :cond_9
    const/4 v2, 0x4

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v2, v3, :cond_8

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->stopPlay()Z

    goto :goto_2

    :cond_a
    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string v2, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_b
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->storeRecordParamsSettings()V

    goto :goto_1
.end method

.method private registerBroadcastReceivcer()V
    .locals 3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageBroastReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/soundrecorder/SoundRecorderService$4;

    invoke-direct {v1, p0}, Lcom/android/soundrecorder/SoundRecorderService$4;-><init>(Lcom/android/soundrecorder/SoundRecorderService;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageBroastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageBroastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<registerExternalStorageListener> register mStorageBroastReceiver"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOtherBroastReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_1

    new-instance v1, Lcom/android/soundrecorder/SoundRecorderService$5;

    invoke-direct {v1, p0}, Lcom/android/soundrecorder/SoundRecorderService$5;-><init>(Lcom/android/soundrecorder/SoundRecorderService;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOtherBroastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.music.musicservicecommand"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOtherBroastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<registerExternalStorageListener> register mOtherBroastReceiver"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private requestAudioFocus()Z
    .locals 5

    const/4 v4, 0x1

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-eq v0, v4, :cond_1

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<requestAudioFocus> request audio focus fail"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    return v1

    :cond_1
    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<requestAudioFocus> request audio focus success"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorderService;->mGetFocus:Z

    goto :goto_0
.end method

.method private setCurrentFilePath(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserverHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const/16 v2, 0xc04

    invoke-direct {v0, p0, v1, v2}, Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;-><init>(Lcom/android/soundrecorder/SoundRecorderService;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserverHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/soundrecorder/SoundRecorderService$6;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/soundrecorder/SoundRecorderService$6;-><init>(Lcom/android/soundrecorder/SoundRecorderService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserverHandler:Landroid/os/Handler;

    :cond_1
    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<setCurrentFilePath> start watching file <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->startWatching()V

    :cond_2
    return-void
.end method

.method private setNotification()V
    .locals 9

    const v8, 0x7f0b0021

    const v7, 0x7f0b0020

    const v6, 0x7f0b001f

    const/4 v5, 0x0

    const/16 v4, 0x8

    const-string v2, "SR/SoundRecorderService"

    const-string v3, "<setNotification>"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentFilePath()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v2, ".tmp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ".tmp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v3, 0x7f0b0025

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v3, 0x7f0b0022

    invoke-virtual {v2, v3, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v7, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v8, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v7, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v6, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v8, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v7, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v8, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private setState(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnStateChangedListener:Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnStateChangedListener:Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    invoke-interface {v0, p1}, Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;->onStateChanged(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<setState> mOnStateChangedListener = null, mCurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showNotification(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mShowNotifiaction:Z

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<showNotification> mShowNotifiaction == false, return"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v3, v8, :cond_1

    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<showNotification> not show in STATE_IDLE, return"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<showNotificatoin> create mNotificationView"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030003

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v4, 0x7f0b0024

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08002b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent;

    const-string v3, "goon record"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v4, 0x7f0b001f

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v0, Landroid/content/Intent;

    const-string v3, "goon play"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v4, 0x7f0b0020

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v0, Landroid/content/Intent;

    const-string v3, "stop"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v4, 0x7f0b0022

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v0, Landroid/content/Intent;

    const-string v3, "pause"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v4, 0x7f0b0021

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.soundrecorder.SoundRecorder"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    const v4, 0x7f0b001d

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mNotificationView:Landroid/widget/RemoteViews;

    iput-object v3, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    iget v3, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/app/Notification;->flags:I

    const v3, 0x7f020008

    iput v3, v1, Landroid/app/Notification;->icon:I

    iput-object v2, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->setNotification()V

    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<showNotificatoin> startForeground"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v8, v1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iput-boolean v8, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRunForeground:Z

    goto/16 :goto_0
.end method

.method private stopWatching()V
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFileObserver:Lcom/android/soundrecorder/SoundRecorderService$RecordingFileObserver;

    :cond_0
    return-void
.end method

.method private unregisterBroadcastReceivcer()V
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageBroastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageBroastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOtherBroastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOtherBroastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public discardRecord()Z
    .locals 5

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isStorageMounted()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SR/SoundRecorderService"

    const-string v3, "<discardRecord> no storage mounted"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "SR/SoundRecorderService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<discardRecord> mCurrentFilePath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/SoundRecorderService;->setCurrentFilePath(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;->onEvent(I)V

    :cond_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getCurrentFileDurationInMillSecond()J
    .locals 4

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileEndWithTmp()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mTotalRecordingDuration:J

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    :goto_1
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x3e8

    goto :goto_1
.end method

.method public getCurrentFileDurationInSecond()J
    .locals 6

    const-wide/16 v4, 0x3e8

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileEndWithTmp()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mTotalRecordingDuration:J

    div-long v0, v2, v4

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    :goto_1
    return-wide v0

    :cond_0
    iget-wide v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    div-long v0, v2, v4

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x1

    goto :goto_1
.end method

.method public getCurrentFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentProgressInMillSecond()J
    .locals 2

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Player;->getCurrentProgress()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_1
    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->getCurrentProgress()J

    move-result-wide v0

    goto :goto_0

    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentProgressInSecond()I
    .locals 4

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentProgressInMillSecond()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public getCurrentState()I
    .locals 1

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    return v0
.end method

.method public getRecorder()Lcom/android/soundrecorder/Recorder;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    return-object v0
.end method

.method public getRemainingTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTime:J

    return-wide v0
.end method

.method public getSaveFileUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public isCurrentFileWaitToSave()Z
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isListener(Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;)Z
    .locals 1
    .param p1    # Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnStateChangedListener:Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isStorageFull(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;)Z
    .locals 6
    .param p1    # Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    const/4 v1, 0x0

    new-instance v0, Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-direct {v0, v2, p0}, Lcom/android/soundrecorder/RemainingTimeCalculator;-><init>(Landroid/os/storage/StorageManager;Lcom/android/soundrecorder/SoundRecorderService;)V

    iget v2, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    invoke-virtual {v0, v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/RemainingTimeCalculator;->timeRemaining(Z)J

    move-result-wide v2

    const-wide/16 v4, 0x2

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isStorageLower()Z
    .locals 11

    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_0
    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageManager:Landroid/os/storage/StorageManager;

    const-string v8, "/storage/sdcard0"

    invoke-virtual {v7, v8}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v7, "mounted"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    const-string v3, "/storage/sdcard0"

    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const-string v8, "Recording"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :cond_1
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    int-to-long v9, v9

    mul-long/2addr v7, v9

    const-wide/32 v9, 0x200000

    cmp-long v7, v7, v9

    if-gez v7, :cond_3

    :cond_2
    :goto_0
    return v5

    :cond_3
    move v5, v6

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public isStorageMounted()Z
    .locals 3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageManager:Landroid/os/storage/StorageManager;

    const-string v2, "/storage/sdcard0"

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<onBind>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mBinder:Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onCreate> start"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "storage"

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageManager:Landroid/os/storage/StorageManager;

    new-instance v1, Lcom/android/soundrecorder/Recorder;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-direct {v1, v2, p0}, Lcom/android/soundrecorder/Recorder;-><init>(Landroid/os/storage/StorageManager;Lcom/android/soundrecorder/Recorder$RecorderListener;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    new-instance v1, Lcom/android/soundrecorder/Player;

    invoke-direct {v1, p0}, Lcom/android/soundrecorder/Player;-><init>(Lcom/android/soundrecorder/Player$PlayerListener;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    new-instance v1, Landroid/media/MediaScannerConnection;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mConnection:Landroid/media/MediaScannerConnection;

    new-instance v1, Lcom/android/soundrecorder/SoundRecorderService$3;

    invoke-direct {v1, p0}, Lcom/android/soundrecorder/SoundRecorderService$3;-><init>(Lcom/android/soundrecorder/SoundRecorderService;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->registerBroadcastReceivcer()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "goon play"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "goon record"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "pause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "stop"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onCreate> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<onDestroy>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->storeRecordParamsSettings()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->unregisterBroadcastReceivcer()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onError(Lcom/android/soundrecorder/Player;I)V
    .locals 3
    .param p1    # Lcom/android/soundrecorder/Player;
    .param p2    # I

    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<Player onError> errorCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    invoke-interface {v0, p2}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    return-void
.end method

.method public onError(Lcom/android/soundrecorder/Recorder;I)V
    .locals 3
    .param p1    # Lcom/android/soundrecorder/Recorder;
    .param p2    # I

    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<Recorder onError> errorCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    invoke-interface {v0, p2}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    return-void
.end method

.method public onMediaScannerConnected()V
    .locals 3

    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onMediaScannerConnected> scan file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFilePathToScan:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mConnection:Landroid/media/MediaScannerConnection;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mFilePathToScan:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 17
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;

    const-string v14, "SR/SoundRecorderService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "<onScanCompleted> start, path = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    new-instance v9, Ljava/text/SimpleDateFormat;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f08003a

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v14}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v12

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "_data"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, " LIKE \'%"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "file:///"

    const-string v15, ""

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "\'"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v10, 0x6

    new-instance v4, Landroid/content/ContentValues;

    const/4 v14, 0x6

    invoke-direct {v4, v14}, Landroid/content/ContentValues;-><init>(I)V

    const-string v14, "is_music"

    const-string v15, "0"

    invoke-virtual {v4, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "title"

    invoke-virtual {v4, v14, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "date_added"

    const-wide/16 v15, 0x3e8

    div-long v15, v2, v15

    long-to-int v15, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v4, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v14, "_data"

    move-object/from16 v0, p1

    invoke-virtual {v4, v14, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "artist"

    const/high16 v15, 0x7f080000

    invoke-virtual {v6, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "album"

    const v15, 0x7f080035

    invoke-virtual {v6, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v14, 0x0

    invoke-virtual {v7, v1, v4, v13, v14}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    const-string v14, "SR/SoundRecorderService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "<onScanCompleted> update result = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/soundrecorder/SoundRecorderService;->mConnection:Landroid/media/MediaScannerConnection;

    invoke-virtual {v14}, Landroid/media/MediaScannerConnection;->disconnect()V

    const-string v14, "SR/SoundRecorderService"

    const-string v15, "<onScanCompleted> end"

    invoke-static {v14, v15}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onStartCommand> start"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SR/SoundRecorderService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onStartCommand> action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    goto :goto_0

    :cond_1
    const-string v1, "goon record"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v5, :cond_3

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onStartCommand> ACTION_GOON_RECORD"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentRecordParams:Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/soundrecorder/SoundRecorderService;->record(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;I)Z

    :cond_2
    :goto_1
    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onStartCommand> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    goto :goto_0

    :cond_3
    const-string v1, "goon play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v7, :cond_4

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onStartCommand> ACTION_GOON_PLAY"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->playCurrentFile()Z

    goto :goto_1

    :cond_4
    const-string v1, "pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onStartCommand> ACTION_PAUSE"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v4, :cond_5

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->pauseRecord()Z

    goto :goto_1

    :cond_5
    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v6, :cond_2

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->pausePlay()Z

    goto :goto_1

    :cond_6
    const-string v1, "stop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<onStartCommand> ACTION_STOP"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v1, v4, :cond_7

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v5, :cond_8

    :cond_7
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->stopRecord()Z

    goto :goto_1

    :cond_8
    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v1, v6, :cond_9

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v7, :cond_2

    :cond_9
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->stopPlay()Z

    goto :goto_1
.end method

.method public onStateChanged(Lcom/android/soundrecorder/Player;I)V
    .locals 5
    .param p1    # Lcom/android/soundrecorder/Player;
    .param p2    # I

    const/4 v4, 0x4

    const/4 v3, 0x1

    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<Player onStateChanged> stateCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v4, p2, :cond_2

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "onStateChanged post mUpdateTimer."

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Player;->getFileDuration()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    :cond_0
    :goto_0
    invoke-direct {p0, p2}, Lcom/android/soundrecorder/SoundRecorderService;->setState(I)V

    if-eq p2, v3, :cond_5

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRunForeground:Z

    if-eqz v0, :cond_4

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<Player onStateChanged> update notificaton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->showNotification(Landroid/content/Context;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-eq v3, p2, :cond_3

    const/4 v0, 0x5

    if-ne v0, p2, :cond_0

    :cond_3
    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v4, v0, :cond_0

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "onStateChanged removeCallbacks mUpdateTimer."

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    goto :goto_0

    :cond_4
    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<Player onStateChanged> show notificaton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->showNotification(Landroid/content/Context;)V

    goto :goto_1

    :cond_5
    if-ne v3, p2, :cond_6

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    invoke-virtual {p0, v3, v0}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    goto :goto_1

    :cond_6
    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRunForeground:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    goto :goto_1
.end method

.method public onStateChanged(Lcom/android/soundrecorder/Recorder;I)V
    .locals 5
    .param p1    # Lcom/android/soundrecorder/Recorder;
    .param p2    # I

    const/4 v4, 0x2

    const/4 v3, 0x1

    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<Recorder onStateChanged> stateCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v4, v0, :cond_3

    :cond_0
    if-ne v3, p2, :cond_3

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->getRecordInfoAfterStopRecord()V

    :cond_1
    :goto_0
    invoke-direct {p0, p2}, Lcom/android/soundrecorder/SoundRecorderService;->setState(I)V

    if-eq p2, v3, :cond_5

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRunForeground:Z

    if-eqz v0, :cond_4

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<Recorder onStateChanged> update notificaton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->showNotification(Landroid/content/Context;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    if-ne v4, p2, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->getSampleFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/SoundRecorderService;->setCurrentFilePath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "post: mRemainingTimeCalculateRunnable"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<Recorder onStateChanged> show notificaton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->showNotification(Landroid/content/Context;)V

    goto :goto_1

    :cond_5
    if-ne v3, p2, :cond_6

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    invoke-virtual {p0, v3, v0}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    goto :goto_1

    :cond_6
    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRunForeground:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    goto :goto_1
.end method

.method public pausePlay()Z
    .locals 2

    const/4 v0, 0x4

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v0, v1, :cond_0

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<pausePlay> not in play state, can\'t pause play"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Player;->pausePlayback()Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public pauseRecord()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v1, v0}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setPauseTimeRemaining(Z)V

    const/4 v1, 0x3

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v2, :cond_0

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<pauseRecord> still in STATE_PAUSE_RECORDING, do nothing, return"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x2

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->pauseRecording()Z

    move-result v0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public playCurrentFile()Z
    .locals 4

    const v3, 0x7f08002a

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<playCurrentFile> in record or pause record state, can\'t play"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<playCurrentFile> in pause play state, goon play"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->requestAudioFocus()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Player;->goonPlayback()Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/android/soundrecorder/SoundRecorderUtils;->getToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<playCurrentFile> in play state, pause play"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Player;->pausePlayback()Z

    move-result v0

    goto :goto_0

    :cond_4
    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<playCurrentFile> in idle state, start play"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/soundrecorder/Player;->setCurrentFilePath(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->requestAudioFocus()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Player;->startPlayback()Z

    move-result v0

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/android/soundrecorder/SoundRecorderUtils;->getToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public playFile(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<playFile> path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/soundrecorder/SoundRecorderService;->setCurrentFilePath(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->playCurrentFile()Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public record(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;I)Z
    .locals 10
    .param p1    # Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;
    .param p2    # I

    const-wide/16 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x2

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v3, v4, :cond_1

    const-string v2, "SR/SoundRecorderService"

    const-string v3, "<record> still in STATE_RECORDING, do nothing"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v6, v3, :cond_3

    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<record> in STATE_PAUSE_RECORDING, mRecorder.goonRecording()"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide v8, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTime:J

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->requestAudioFocus()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->goonRecording()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    move v1, v2

    goto :goto_0

    :cond_3
    const/4 v3, 0x5

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-ne v7, v3, :cond_5

    :cond_4
    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<record> in pause playing or playing, stopPlay first"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->stopPlay()Z

    :cond_5
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileEndWithTmp()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "SR/SoundRecorderService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<record> delete not saved file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->stopWatching()V

    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_6
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isStorageMounted()Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<record> no storage mounted"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    invoke-interface {v3, v7}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/SoundRecorderService;->setState(I)V

    move v1, v2

    goto :goto_0

    :cond_7
    invoke-virtual {p0, p1}, Lcom/android/soundrecorder/SoundRecorderService;->isStorageFull(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "SR/SoundRecorderService"

    const-string v4, "<record> storage is full"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    invoke-interface {v3, v6}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/SoundRecorderService;->setState(I)V

    move v1, v2

    goto/16 :goto_0

    :cond_8
    new-instance v2, Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-direct {v2, v3, p0}, Lcom/android/soundrecorder/RemainingTimeCalculator;-><init>(Landroid/os/storage/StorageManager;Lcom/android/soundrecorder/SoundRecorderService;)V

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget v3, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    invoke-virtual {v2, v3}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    iput-wide v8, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTime:J

    const-string v2, "SR/SoundRecorderService"

    const-string v3, "<record> start record"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->requestAudioFocus()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2, p1, p2}, Lcom/android/soundrecorder/Recorder;->startRecording(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;I)Z

    move-result v1

    const-string v2, "SR/SoundRecorderService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<record> mRecorder.startRecording res = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentRecordParams:Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    if-eqz v1, :cond_9

    if-lez p2, :cond_9

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->getSampFile()Ljava/io/File;

    move-result-object v3

    int-to-long v4, p2

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setFileSizeLimit(Ljava/io/File;J)V

    goto/16 :goto_0

    :cond_9
    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08002a

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SoundRecorderUtils;->getToast(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method

.method public reset()Z
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->reset()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    const/4 v2, 0x6

    invoke-interface {v1, v2}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Player;->reset()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/SoundRecorderService;->setCurrentFilePath(Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    invoke-direct {p0, v3}, Lcom/android/soundrecorder/SoundRecorderService;->setState(I)V

    return v3
.end method

.method public saveRecord()Z
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFilePath:Ljava/lang/String;

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v2, "SR/SoundRecorderService"

    const-string v3, "<saveRecord> no file need to be saved"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorderService;->isStorageMounted()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v2, "SR/SoundRecorderService"

    const-string v3, "<saveRecord> no storage mounted"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;->onError(I)V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->deleteRecordingFileTmpSuffix()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/android/soundrecorder/SoundRecorderService;->addToMediaDB(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUri:Landroid/net/Uri;

    iput-wide v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentFileDuration:J

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/android/soundrecorder/SoundRecorderService;->setCurrentFilePath(Ljava/lang/String;)V

    iput-wide v5, p0, Lcom/android/soundrecorder/SoundRecorderService;->mTotalRecordingDuration:J

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUri:Landroid/net/Uri;

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    invoke-interface {v1, v2}, Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;->onEvent(I)V

    :goto_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    move v1, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f080001

    invoke-static {v1, v3}, Lcom/android/soundrecorder/SoundRecorderUtils;->getToast(Landroid/content/Context;I)V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->hideNotifiaction()V

    goto :goto_0
.end method

.method public setAllListenerSelf()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<setAllListenerSelf> set new mOnErrorListener"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/soundrecorder/SoundRecorderService$2;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorderService$2;-><init>(Lcom/android/soundrecorder/SoundRecorderService;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnStateChangedListener:Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnUpdateTimeViewListener:Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

    return-void
.end method

.method public setErrorListener(Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;)V
    .locals 0
    .param p1    # Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnErrorListener:Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;

    return-void
.end method

.method public setEventListener(Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;)V
    .locals 0
    .param p1    # Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnEventListener:Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;

    return-void
.end method

.method public setSelectEffectArray([ZZ)V
    .locals 2
    .param p1    # [Z
    .param p2    # Z

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<setSelectEffectArray>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArrayTemp:[Z

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArray:[Z

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArray:[Z

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Z

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArrayTemp:[Z

    goto :goto_0
.end method

.method public setSelectedFormat(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectedFormat:I

    return-void
.end method

.method public setSelectedMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectedMode:I

    return-void
.end method

.method public setShowNotification(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mShowNotifiaction:Z

    return-void
.end method

.method public setStateChangedListener(Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;)V
    .locals 0
    .param p1    # Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnStateChangedListener:Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;

    return-void
.end method

.method public setUpdateTimeViewListener(Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;)V
    .locals 0
    .param p1    # Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mOnUpdateTimeViewListener:Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

    return-void
.end method

.method public stopPlay()Z
    .locals 2

    const/4 v0, 0x5

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x4

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v0, v1, :cond_0

    const-string v0, "SR/SoundRecorderService"

    const-string v1, "<stopPlay> not in play or pause play state, can\'t stop play"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPlayer:Lcom/android/soundrecorder/Player;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Player;->stopPlayback()Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopRecord()Z
    .locals 3

    const/4 v1, 0x3

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v1, v2, :cond_0

    const/4 v1, 0x2

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mCurrentState:I

    if-eq v1, v2, :cond_0

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<stopRecord> not in pause record or record state, return"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorderService;->abandonAudioFocus()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->stopRecording()Z

    move-result v0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public storeRecordParamsSettings()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "SR/SoundRecorderService"

    const-string v2, "<storeRecordParamsSettings> "

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    const-string v1, "sound_recorder_data"

    invoke-virtual {p0, v1, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPrefs:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_format"

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectedFormat:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_mode"

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectedMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_effect_aec"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArray:[Z

    aget-boolean v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_effect_agc"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArray:[Z

    aget-boolean v2, v2, v5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_effect_ns"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArray:[Z

    aget-boolean v2, v2, v4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_effect_aec_tmp"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArrayTemp:[Z

    aget-boolean v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_effect_agc_tmp"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArrayTemp:[Z

    aget-boolean v2, v2, v5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_effect_ns_tmp"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectEffectArrayTemp:[Z

    aget-boolean v2, v2, v4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v1, "SR/SoundRecorderService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSelectedFormat is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorderService;->mSelectedFormat:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
