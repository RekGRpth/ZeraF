.class public Lcom/android/soundrecorder/ListViewProperty;
.super Ljava/lang/Object;
.source "ListViewProperty.java"


# instance fields
.field private mCheckedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPos:I

.field private mTop:I


# direct methods
.method public constructor <init>(Ljava/util/List;II)V
    .locals 0
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;II)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/soundrecorder/ListViewProperty;->mCheckedList:Ljava/util/List;

    iput p2, p0, Lcom/android/soundrecorder/ListViewProperty;->mCurPos:I

    iput p3, p0, Lcom/android/soundrecorder/ListViewProperty;->mTop:I

    return-void
.end method


# virtual methods
.method public getCheckedList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/soundrecorder/ListViewProperty;->mCheckedList:Ljava/util/List;

    return-object v0
.end method

.method public getCurPos()I
    .locals 1

    iget v0, p0, Lcom/android/soundrecorder/ListViewProperty;->mCurPos:I

    return v0
.end method

.method public getTop()I
    .locals 1

    iget v0, p0, Lcom/android/soundrecorder/ListViewProperty;->mTop:I

    return v0
.end method

.method public setCheckedList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/soundrecorder/ListViewProperty;->mCheckedList:Ljava/util/List;

    return-void
.end method

.method public setCurPos(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/ListViewProperty;->mCurPos:I

    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/ListViewProperty;->mTop:I

    return-void
.end method
