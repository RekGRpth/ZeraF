.class public Lcom/android/soundrecorder/Recorder;
.super Ljava/lang/Object;
.source "Recorder.java"

# interfaces
.implements Landroid/media/MediaRecorder$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/soundrecorder/Recorder$RecorderListener;
    }
.end annotation


# static fields
.field public static final RECORD_FOLDER:Ljava/lang/String; = "Recording"

.field private static final SAMPLE_PREFIX:Ljava/lang/String; = "record"

.field public static final SAMPLE_SUFFIX:Ljava/lang/String; = ".tmp"

.field private static final TAG:Ljava/lang/String; = "SR/Recorder"


# instance fields
.field private mCurrentState:I

.field private mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

.field private mPreviousTime:J

.field private mRecorder:Landroid/media/MediaRecorder;

.field private mSampleFile:Ljava/io/File;

.field private mSampleLength:J

.field private mSampleStart:J

.field private mSelectEffect:[Z

.field private final mStorageManager:Landroid/os/storage/StorageManager;


# direct methods
.method public constructor <init>(Landroid/os/storage/StorageManager;Lcom/android/soundrecorder/Recorder$RecorderListener;)V
    .locals 4
    .param p1    # Landroid/os/storage/StorageManager;
    .param p2    # Lcom/android/soundrecorder/Recorder$RecorderListener;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:J

    iput-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    iput-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mSelectEffect:[Z

    iput-object p1, p0, Lcom/android/soundrecorder/Recorder;->mStorageManager:Landroid/os/storage/StorageManager;

    iput-object p2, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    return-void
.end method

.method private createRecordingFile(Ljava/lang/String;)V
    .locals 13
    .param p1    # Ljava/lang/String;

    const-string v10, "SR/Recorder"

    const-string v11, "<createRecordingFile> begin"

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".tmp"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    iget-object v10, p0, Lcom/android/soundrecorder/Recorder;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v10, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v5, Ljava/io/File;

    const-string v10, "/storage/sdcard0"

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> sd card directory is:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Recording"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_1
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_1

    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x28

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x29

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> make directory ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] fail"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v5, :cond_3

    :try_start_0
    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> sample directory  is:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyyMMddHHmmss"

    invoke-direct {v7, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-direct {v10, v11, v12}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v10}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "record"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v10, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iget-object v10, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    move-result v4

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> creat file success is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> mSampleFile.getAbsolutePath() is: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v10, "SR/Recorder"

    const-string v11, "<createRecordingFile> end"

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    iget-object v10, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/16 v11, 0xa

    invoke-interface {v10, p0, v11}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onError(Lcom/android/soundrecorder/Recorder;I)V

    const-string v10, "SR/Recorder"

    const-string v11, "<createRecordingFile> io exception happens"

    invoke-static {v10, v11}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private initAndStartMediaRecorder(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;I)Z
    .locals 7
    .param p1    # Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v4, "SR/Recorder"

    const-string v5, "<initAndStartMediaRecorder> start"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEffect:[Z

    iput-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSelectEffect:[Z

    new-instance v4, Landroid/media/MediaRecorder;

    invoke-direct {v4}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget v5, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->canSelectMode()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget v5, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mHDRecordMode:I

    invoke-static {v4, v5, v3}, Lcom/mediatek/media/MediaRecorderEx;->setHDRecordMode(Landroid/media/MediaRecorder;IZ)V

    :cond_0
    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->canSelectEffect()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSelectEffect:[Z

    aget-boolean v4, v4, v3

    if-eqz v4, :cond_1

    or-int/lit8 v1, v1, 0x1

    :cond_1
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSelectEffect:[Z

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_2

    or-int/lit8 v1, v1, 0x2

    :cond_2
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSelectEffect:[Z

    const/4 v5, 0x2

    aget-boolean v4, v4, v5

    if-eqz v4, :cond_3

    or-int/lit8 v1, v1, 0x4

    :cond_3
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-static {v4, v1}, Lcom/mediatek/media/MediaRecorderEx;->setPreprocessEffect(Landroid/media/MediaRecorder;I)V

    :cond_4
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget v5, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget v5, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioChannels:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget v5, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget v5, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    if-lez p2, :cond_5

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    int-to-long v5, p2

    invoke-virtual {v4, v5, v6}, Landroid/media/MediaRecorder;->setMaxFileSize(J)V

    :cond_5
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, p0}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    :try_start_0
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->prepare()V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    const-string v3, "SR/Recorder"

    const-string v4, "<initAndStartMediaRecorder> end"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v4, "SR/Recorder"

    const-string v5, "<initAndStartMediaRecorder> IO exception"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2, v0}, Lcom/android/soundrecorder/Recorder;->handleException(ZLjava/lang/Exception;)V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/4 v4, 0x6

    invoke-interface {v2, p0, v4}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onError(Lcom/android/soundrecorder/Recorder;I)V

    move v2, v3

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v4, "SR/Recorder"

    const-string v5, "<initAndStartMediaRecorder> RuntimeException"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2, v0}, Lcom/android/soundrecorder/Recorder;->handleException(ZLjava/lang/Exception;)V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/4 v4, 0x5

    invoke-interface {v2, p0, v4}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onError(Lcom/android/soundrecorder/Recorder;I)V

    move v2, v3

    goto :goto_0
.end method

.method private setState(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    invoke-interface {v0, p0, p1}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onStateChanged(Lcom/android/soundrecorder/Recorder;I)V

    return-void
.end method


# virtual methods
.method public getCurrentProgress()J
    .locals 6

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-ne v2, v3, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    sub-long v2, v0, v2

    iget-wide v4, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    add-long/2addr v2, v4

    :goto_0
    return-wide v2

    :cond_0
    const/4 v2, 0x3

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-ne v2, v3, :cond_1

    iget-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getCurrentState()I
    .locals 1

    iget v0, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    return v0
.end method

.method public getMaxAmplitude()I
    .locals 2

    const/4 v0, 0x2

    iget v1, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->getMaxAmplitude()I

    move-result v0

    goto :goto_0
.end method

.method public getSampFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    return-object v0
.end method

.method public getSampleFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSampleLength()J
    .locals 2

    iget-wide v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:J

    return-wide v0
.end method

.method public goonRecording()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x3

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "SR/Recorder"

    const-string v3, "<goOnRecording> IllegalArgumentException"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->reset()V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->release()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/4 v3, 0x6

    invoke-interface {v2, p0, v3}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onError(Lcom/android/soundrecorder/Recorder;I)V

    goto :goto_0
.end method

.method public handleException(ZLjava/lang/Exception;)V
    .locals 3
    .param p1    # Z
    .param p2    # Ljava/lang/Exception;

    const-string v0, "SR/Recorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<handleException> the exception is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    :cond_1
    return-void
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 3
    .param p1    # Landroid/media/MediaRecorder;
    .param p2    # I
    .param p3    # I

    const-string v0, "SR/Recorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onError> errorType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; extraCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stopRecording()Z

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/4 v1, 0x6

    invoke-interface {v0, p0, v1}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onError(Lcom/android/soundrecorder/Recorder;I)V

    return-void
.end method

.method public pauseRecording()Z
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-static {v2}, Lcom/mediatek/media/MediaRecorderEx;->pause(Landroid/media/MediaRecorder;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-wide v1, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    sub-long/2addr v3, v5

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "SR/Recorder"

    const-string v3, "<pauseRecording> IllegalArgumentException"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v1, v0}, Lcom/android/soundrecorder/Recorder;->handleException(ZLjava/lang/Exception;)V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/4 v3, 0x6

    invoke-interface {v2, p0, v3}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onError(Lcom/android/soundrecorder/Recorder;I)V

    goto :goto_0
.end method

.method public reset()Z
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->reset()V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->release()V

    iput-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iput-wide v5, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    iput-wide v5, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:J

    iput-wide v5, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    const/4 v2, 0x1

    iput v2, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_1
    const-string v2, "SR/Recorder"

    const-string v3, "<stopRecording> recorder illegalstate exception in recorder.stop()"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->reset()V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->release()V

    iput-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->reset()V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->release()V

    iput-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    throw v2
.end method

.method public startRecording(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;I)Z
    .locals 4
    .param p1    # Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "SR/Recorder"

    const-string v3, "<startRecording> begin"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->reset()Z

    iget-object v2, p1, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Recorder;->createRecordingFile(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/android/soundrecorder/Recorder;->initAndStartMediaRecorder(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;I)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "SR/Recorder"

    const-string v2, "<startRecording> initAndStartMediaRecorder return false"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    const-string v0, "SR/Recorder"

    const-string v2, "<startRecording> end"

    invoke-static {v0, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public stopRecording()Z
    .locals 9

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "SR/Recorder"

    const-string v5, "<stopRecording> start"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x3

    iget v5, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-ne v6, v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-nez v4, :cond_2

    :cond_1
    const-string v2, "SR/Recorder"

    const-string v4, "<stopRecording> end 1"

    invoke-static {v2, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    :goto_0
    return v2

    :cond_2
    iget v4, p0, Lcom/android/soundrecorder/Recorder;->mCurrentState:I

    if-ne v6, v4, :cond_5

    move v1, v2

    :goto_1
    :try_start_0
    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->reset()V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->release()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    :cond_3
    if-eqz v1, :cond_4

    iget-wide v3, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    sub-long/2addr v5, v7

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    :cond_4
    iget-wide v3, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    iput-wide v3, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:J

    const-string v3, "SR/Recorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<stopRecording> mSampleLength in ms is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "SR/Recorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<stopRecording> mSampleLength in s is = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    const-string v3, "SR/Recorder"

    const-string v4, "<stopRecording> end 2"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {p0, v3, v0}, Lcom/android/soundrecorder/Recorder;->handleException(ZLjava/lang/Exception;)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mListener:Lcom/android/soundrecorder/Recorder$RecorderListener;

    const/4 v4, 0x6

    invoke-interface {v3, p0, v4}, Lcom/android/soundrecorder/Recorder$RecorderListener;->onError(Lcom/android/soundrecorder/Recorder;I)V

    const-string v3, "SR/Recorder"

    const-string v4, "<stopRecording> recorder illegalstate exception in recorder.stop()"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
