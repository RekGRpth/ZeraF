.class public Lcom/android/soundrecorder/RecordParamsSetting;
.super Ljava/lang/Object;
.source "RecordParamsSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;
    }
.end annotation


# static fields
.field public static final AUDIO_3GPP:Ljava/lang/String; = "audio/3gpp"

.field public static final AUDIO_AAC:Ljava/lang/String; = "audio/aac"

.field public static final AUDIO_AMR:Ljava/lang/String; = "audio/amr"

.field public static final AUDIO_AWB:Ljava/lang/String; = "audio/awb"

.field public static final AUDIO_CHANNELS_MONO:I = 0x1

.field public static final AUDIO_CHANNELS_STEREO:I = 0x2

.field public static final AUDIO_NOT_LIMIT_TYPE:Ljava/lang/String; = "audio/*"

.field public static final AUDIO_OGG:Ljava/lang/String; = "application/ogg"

.field public static final AUDIO_VORBIS:Ljava/lang/String; = "audio/vorbis"

.field public static final AUDIO_WAV:Ljava/lang/String; = "audio/wav"

.field public static final EFFECT_AEC:I = 0x0

.field public static final EFFECT_AGC:I = 0x2

.field public static final EFFECT_NS:I = 0x1

.field public static final ENCODE_BITRATE_3GPP:I = 0x2fa8

.field public static final ENCODE_BITRATE_ADPCM:I = 0x1f400

.field public static final ENCODE_BITRATE_AMR:I = 0x2fa8

.field public static final ENCODE_BITRATE_AWB:I = 0x6f54

.field public static final ENCODE_BITRATE_VORBIS:I = 0x1f400

.field public static final FORMAT_HIGH:I = 0x0

.field public static final FORMAT_LOW:I = 0x2

.field public static final FORMAT_STANDARD:I = 0x1

.field public static final HIGH_ENCODE_BITRATE_AAC:I = 0x1f400

.field public static final HIGH_RECORD_ENCODER:Ljava/lang/String; = "high_record_encoder"

.field public static final HIGH_SAMPLE_RATE_AAC:I = 0xbb80

.field public static final MODE_INDOOR:I = 0x1

.field public static final MODE_NORMAL:I = 0x0

.field public static final MODE_OUTDOOR:I = 0x2

.field public static final NOT_LIMIT_TYPE:Ljava/lang/String; = "*/*"

.field public static final SAMPLE_RATE_ADPCM:I = 0xbb80

.field public static final SAMPLE_RATE_AMR:I = 0x1f40

.field public static final SAMPLE_RATE_AWB:I = 0x3e80

.field public static final SAMPLE_RATE_VORBIS:I = 0xbb80

.field public static final STANDARD_ENCODE_BITRATE_AAC:I = 0xbb80

.field public static final STANDSARD_SAMPLE_RATE_AAC:I = 0x7d00

.field private static final TAG:Ljava/lang/String; = "SR/RecordParamsSetting"

.field private static sEffectArray:[I

.field private static sEnableTestAudioEffect:Z

.field private static sFormatArray:[I

.field private static sFormatSuffixArray:[Ljava/lang/CharSequence;

.field private static sModeArray:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    sput-object v0, Lcom/android/soundrecorder/RecordParamsSetting;->sModeArray:[I

    sput-object v0, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatSuffixArray:[Ljava/lang/CharSequence;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/soundrecorder/RecordParamsSetting;->sEnableTestAudioEffect:Z

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/soundrecorder/RecordParamsSetting;->sEffectArray:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f080027
        0x7f080028
        0x7f080026
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static canSelectEffect()Z
    .locals 1

    sget-boolean v0, Lcom/android/soundrecorder/RecordParamsSetting;->sEnableTestAudioEffect:Z

    return v0
.end method

.method static canSelectFormat()Z
    .locals 2

    const-string v0, "SR/RecordParamsSetting"

    const-string v1, "<canSelectFormat> FeatureOption.HAVE_AACENCODE_FEATURE is:true"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method static canSelectMode()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method static getEffectStringIDArray()[I
    .locals 1

    sget-object v0, Lcom/android/soundrecorder/RecordParamsSetting;->sEffectArray:[I

    return-object v0
.end method

.method static getFormatStringIDArray(Landroid/content/Context;)[I
    .locals 8
    .param p0    # Landroid/content/Context;

    const v3, 0x7f080004

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/mediatek/soundrecorder/ext/ExtensionHelper;->getExtension(Landroid/content/Context;)Lcom/mediatek/soundrecorder/ext/IQualityLevel;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/soundrecorder/ext/IQualityLevel;->getLevelNumber()I

    move-result v1

    if-ne v6, v1, :cond_1

    new-array v0, v6, [I

    aput v3, v0, v4

    const v3, 0x7f080029

    aput v3, v0, v5

    new-array v3, v6, [I

    sput-object v3, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    sget-object v3, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    aput v4, v3, v4

    sget-object v3, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    aput v5, v3, v5

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-ne v7, v1, :cond_0

    new-array v0, v7, [I

    aput v3, v0, v4

    const v3, 0x7f080005

    aput v3, v0, v5

    const v3, 0x7f080006

    aput v3, v0, v6

    new-array v3, v7, [I

    sput-object v3, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    sget-object v3, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    aput v4, v3, v4

    sget-object v3, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    aput v5, v3, v5

    sget-object v3, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    aput v6, v3, v6

    goto :goto_0
.end method

.method static getModeStringIDArray()[I
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v5, [I

    const v1, 0x7f080016

    aput v1, v0, v2

    const v1, 0x7f080018

    aput v1, v0, v3

    const v1, 0x7f080019

    aput v1, v0, v4

    new-array v1, v5, [I

    sput-object v1, Lcom/android/soundrecorder/RecordParamsSetting;->sModeArray:[I

    sget-object v1, Lcom/android/soundrecorder/RecordParamsSetting;->sModeArray:[I

    aput v2, v1, v2

    sget-object v1, Lcom/android/soundrecorder/RecordParamsSetting;->sModeArray:[I

    aput v3, v1, v3

    sget-object v1, Lcom/android/soundrecorder/RecordParamsSetting;->sModeArray:[I

    aput v4, v1, v4

    return-object v0
.end method

.method static getRecordParams(Ljava/lang/String;II[Z)Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # I
    .param p3    # [Z

    const v7, 0x1f400

    const v6, 0xbb80

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    invoke-direct {v0}, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;-><init>()V

    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->canSelectEffect()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object p3, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEffect:[Z

    :cond_0
    const-string v1, "audio/*"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "*/*"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const-string v1, "SR/RecordParamsSetting"

    const-string v2, "<getRecordParams>, use AAC Encoder"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    const-string v1, "SR/RecordParamsSetting"

    const-string v2, "<getRecordParams> selectFormat is out of range"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    packed-switch p2, :pswitch_data_1

    const-string v1, "SR/RecordParamsSetting"

    const-string v2, "<getRecordParams> selectMode is out of range"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v0

    :pswitch_0
    iput v5, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    iput v4, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioChannels:I

    iput v7, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    iput v7, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    iput v6, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".3gpp"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/3gpp"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    iput v3, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    invoke-static {v0}, Lcom/android/soundrecorder/RecordParamsSetting;->setRecordParamsFromSystemProperties(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;)V

    goto :goto_0

    :pswitch_1
    iput v5, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    iput v4, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioChannels:I

    iput v6, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    iput v6, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    const/16 v1, 0x7d00

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".3gpp"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/3gpp"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    iput v3, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto :goto_0

    :pswitch_2
    iput v3, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    const/16 v1, 0x2fa8

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    const/16 v1, 0x2fa8

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    const/16 v1, 0x1f40

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".amr"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/amr"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    iput v5, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto :goto_0

    :cond_2
    const-string v1, "audio/amr"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x2fa8

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    const/16 v1, 0x3200

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    iput v3, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    const/16 v1, 0x1f40

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".amr"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/amr"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    iput v5, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto :goto_0

    :cond_3
    const-string v1, "audio/awb"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x6f54

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    const/16 v1, 0x6f54

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    const/16 v1, 0x3e80

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    iput v4, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    const-string v1, ".awb"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/awb"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    iput v3, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto/16 :goto_0

    :cond_4
    const-string v1, "audio/aac"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iput v4, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioChannels:I

    iput v5, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    iput v7, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    iput v7, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    iput v6, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".aac"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/aac"

    iput-object v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    const/4 v1, 0x6

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto/16 :goto_0

    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid output file type requested"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_3
    const/4 v1, 0x0

    iput v1, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mHDRecordMode:I

    const-string v1, "SR/RecordParamsSetting"

    const-string v2, "<getRecordParams> mHDRecordModeis MODE_NORMAL"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :pswitch_4
    iput v3, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mHDRecordMode:I

    const-string v1, "SR/RecordParamsSetting"

    const-string v2, "<getRecordParams> mHDRecordModeis MODE_INDOOR"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :pswitch_5
    iput v4, v0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mHDRecordMode:I

    const-string v1, "SR/RecordParamsSetting"

    const-string v2, "<getRecordParams> mHDRecordModeis MODE_OUTDOOR"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static getSelectFormat(I)I
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/android/soundrecorder/RecordParamsSetting;->sFormatArray:[I

    aget v0, v0, p0

    return v0
.end method

.method static getSelectMode(I)I
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/android/soundrecorder/RecordParamsSetting;->sModeArray:[I

    aget v0, v0, p0

    return v0
.end method

.method static isAvailableRequestType(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "audio/amr"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "audio/3gpp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "audio/*"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "*/*"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static setRecordParamsFromSystemProperties(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;)V
    .locals 6
    .param p0    # Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    const v5, 0xbb80

    const v4, 0x1f400

    const-string v1, "high_record_encoder"

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "SR/RecordParamsSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<setRecordRelatedParamsFromSystemProperties> highRecordEncoder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v1, "SR/RecordParamsSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<setRecordParamsFromSystemProperties> highRecordEncoder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", vorbis"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioChannels:I

    const/16 v1, 0x8

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    iput v4, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    iput v4, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    iput v5, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".ogg"

    iput-object v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "application/ogg"

    iput-object v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    const/16 v1, 0xa

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto :goto_0

    :pswitch_1
    const-string v1, "SR/RecordParamsSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<setRecordParamsFromSystemProperties> highRecordEncoder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", aac"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    iput v4, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    iput v4, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    iput v5, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".3gpp"

    iput-object v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/3gpp"

    iput-object v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto :goto_0

    :pswitch_2
    const-string v1, "SR/RecordParamsSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<setRecordParamsFromSystemProperties> highRecordEncoder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", adpcm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x7

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    iput v4, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    iput v4, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    iput v5, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v1, ".wav"

    iput-object v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v1, "audio/wav"

    iput-object v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    const/16 v1, 0x9

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
