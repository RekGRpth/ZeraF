.class Lcom/android/soundrecorder/SoundRecorderService$1;
.super Ljava/lang/Object;
.source "SoundRecorderService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/SoundRecorderService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/SoundRecorderService;


# direct methods
.method constructor <init>(Lcom/android/soundrecorder/SoundRecorderService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v0}, Lcom/android/soundrecorder/SoundRecorderService;->access$000(Lcom/android/soundrecorder/SoundRecorderService;)I

    move-result v0

    if-ne v4, v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v1}, Lcom/android/soundrecorder/SoundRecorderService;->access$200(Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/RemainingTimeCalculator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;->timeRemaining(Z)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/soundrecorder/SoundRecorderService;->access$102(Lcom/android/soundrecorder/SoundRecorderService;J)J

    const-string v0, "SR/SoundRecorderService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRemainingTime is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v2}, Lcom/android/soundrecorder/SoundRecorderService;->access$100(Lcom/android/soundrecorder/SoundRecorderService;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v0}, Lcom/android/soundrecorder/SoundRecorderService;->access$300(Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v0}, Lcom/android/soundrecorder/SoundRecorderService;->access$300(Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;->updateTimerView()V

    :cond_1
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v0}, Lcom/android/soundrecorder/SoundRecorderService;->access$100(Lcom/android/soundrecorder/SoundRecorderService;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v0}, Lcom/android/soundrecorder/SoundRecorderService;->access$000(Lcom/android/soundrecorder/SoundRecorderService;)I

    move-result v0

    if-ne v4, v0, :cond_2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->stopRecord()Z

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v0}, Lcom/android/soundrecorder/SoundRecorderService;->access$500(Lcom/android/soundrecorder/SoundRecorderService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorderService$1;->this$0:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-static {v1}, Lcom/android/soundrecorder/SoundRecorderService;->access$400(Lcom/android/soundrecorder/SoundRecorderService;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
