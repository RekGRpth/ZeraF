.class public Lcom/android/soundrecorder/EditViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "EditViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;
    }
.end annotation


# static fields
.field private static final NO_CHECK_POS:I = -0x1


# instance fields
.field private final mCheckStates:Landroid/util/SparseBooleanArray;

.field private final mCheckedItemId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPos:I

.field private final mDurationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIdList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mIdPos:Landroid/util/SparseIntArray;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTitleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput v1, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCurPos:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mNameList:Ljava/util/List;

    iput-object p3, p0, Lcom/android/soundrecorder/EditViewAdapter;->mPathList:Ljava/util/List;

    iput-object p4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mTitleList:Ljava/util/List;

    iput-object p5, p0, Lcom/android/soundrecorder/EditViewAdapter;->mDurationList:Ljava/util/List;

    iput-object p6, p0, Lcom/android/soundrecorder/EditViewAdapter;->mIdList:Ljava/util/List;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckedItemId:Ljava/util/List;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mIdPos:Landroid/util/SparseIntArray;

    iput p7, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCurPos:I

    iget v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCurPos:I

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    iget v1, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCurPos:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v2, -0x1

    iput v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCurPos:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mNameList:Ljava/util/List;

    iput-object p3, p0, Lcom/android/soundrecorder/EditViewAdapter;->mPathList:Ljava/util/List;

    iput-object p4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mTitleList:Ljava/util/List;

    iput-object p5, p0, Lcom/android/soundrecorder/EditViewAdapter;->mDurationList:Ljava/util/List;

    iput-object p6, p0, Lcom/android/soundrecorder/EditViewAdapter;->mIdList:Ljava/util/List;

    new-instance v2, Landroid/util/SparseBooleanArray;

    invoke-direct {v2}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckedItemId:Ljava/util/List;

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mIdPos:Landroid/util/SparseIntArray;

    if-eqz p7, :cond_0

    invoke-interface {p7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected getCheckedItemsCount()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/soundrecorder/EditViewAdapter;->getCheckedItemsList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected getCheckedItemsList()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->size()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v5, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    iget-object v5, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v1

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/soundrecorder/EditViewAdapter;->mPathList:Ljava/util/List;

    invoke-virtual {p0, v1}, Lcom/android/soundrecorder/EditViewAdapter;->getItemPos(I)I

    move-result v6

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method protected getCheckedPosList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    iget-object v4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckedItemId:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckedItemId:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckedItemId:Ljava/util/List;

    return-object v4
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected getGrayOutItems()Landroid/util/SparseBooleanArray;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1    # I

    iget-object v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mIdList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v0, v2

    iget-object v2, p0, Lcom/android/soundrecorder/EditViewAdapter;->mIdPos:Landroid/util/SparseIntArray;

    long-to-int v3, v0

    invoke-virtual {v2, v3, p1}, Landroid/util/SparseIntArray;->put(II)V

    return-wide v0
.end method

.method public getItemPos(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mIdPos:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move-object v1, p2

    if-nez v1, :cond_0

    iget-object v7, p0, Lcom/android/soundrecorder/EditViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/high16 v8, 0x7f030000

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;

    invoke-direct {v2}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;-><init>()V

    const v7, 0x7f0b0001

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v2, v7}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$002(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;Landroid/widget/TextView;)Landroid/widget/TextView;

    const/high16 v7, 0x7f0b0000

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    invoke-static {v2, v7}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$102(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    const v7, 0x7f0b0002

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v2, v7}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$202(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v7, 0x7f0b0003

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v2, v7}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$302(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;Landroid/widget/TextView;)Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    invoke-static {v2}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$000(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {v2}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$000(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v7, p0, Lcom/android/soundrecorder/EditViewAdapter;->mNameList:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v2}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$000(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/android/soundrecorder/EditViewAdapter;->mTitleList:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v2}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$200(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/android/soundrecorder/EditViewAdapter;->mDurationList:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$300(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p1}, Lcom/android/soundrecorder/EditViewAdapter;->getItemId(I)J

    move-result-wide v7

    long-to-int v4, v7

    invoke-static {v2}, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->access$100(Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;)Landroid/widget/CheckBox;

    move-result-object v7

    iget-object v8, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v8, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-object v1

    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;

    goto :goto_0
.end method

.method protected setCheckBox(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/soundrecorder/EditViewAdapter;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    return-void
.end method
