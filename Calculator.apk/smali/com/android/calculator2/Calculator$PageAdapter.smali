.class Lcom/android/calculator2/Calculator$PageAdapter;
.super Lvedroid/support/v4/view/PagerAdapter;
.source "Calculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calculator2/Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PageAdapter"
.end annotation


# instance fields
.field private mAdvancedPage:Landroid/view/View;

.field private mSimplePage:Landroid/view/View;

.field final synthetic this$0:Lcom/android/calculator2/Calculator;


# direct methods
.method public constructor <init>(Lcom/android/calculator2/Calculator;Lvedroid/support/v4/view/ViewPager;)V
    .locals 11
    .param p2    # Lvedroid/support/v4/view/ViewPager;

    const/4 v10, 0x0

    iput-object p1, p0, Lcom/android/calculator2/Calculator$PageAdapter;->this$0:Lcom/android/calculator2/Calculator;

    invoke-direct {p0}, Lvedroid/support/v4/view/PagerAdapter;-><init>()V

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v9, 0x7f040003

    invoke-virtual {v5, v9, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    const/high16 v9, 0x7f040000

    invoke-virtual {v5, v9, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v8, p0, Lcom/android/calculator2/Calculator$PageAdapter;->mSimplePage:Landroid/view/View;

    iput-object v1, p0, Lcom/android/calculator2/Calculator$PageAdapter;->mAdvancedPage:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v9, 0x7f050000

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v7

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_0

    invoke-virtual {v7, v4, v10}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Lcom/android/calculator2/Calculator;->setOnClickListener(Landroid/view/View;I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    const v9, 0x7f050001

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_1

    invoke-virtual {v0, v4, v10}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    invoke-virtual {p1, v1, v9}, Lcom/android/calculator2/Calculator;->setOnClickListener(Landroid/view/View;I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const v9, 0x7f0d0010

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {p1, v3}, Lcom/android/calculator2/Calculator;->access$002(Lcom/android/calculator2/Calculator;Landroid/view/View;)Landroid/view/View;

    :cond_2
    const v9, 0x7f0d0011

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {p1, v2}, Lcom/android/calculator2/Calculator;->access$102(Lcom/android/calculator2/Calculator;Landroid/view/View;)Landroid/view/View;

    :cond_3
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    check-cast p1, Landroid/view/ViewGroup;

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public getCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/Calculator$PageAdapter;->mSimplePage:Landroid/view/View;

    :goto_0
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calculator2/Calculator$PageAdapter;->mAdvancedPage:Landroid/view/View;

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
    .param p2    # Ljava/lang/ClassLoader;

    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method
