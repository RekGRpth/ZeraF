.class Lcom/android/keychain/KeyChainActivity$CertificateAdapter;
.super Landroid/widget/BaseAdapter;
.source "KeyChainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keychain/KeyChainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CertificateAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keychain/KeyChainActivity$CertificateAdapter$CertLoader;
    }
.end annotation


# instance fields
.field private final mAliases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSubjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/keychain/KeyChainActivity;


# direct methods
.method private constructor <init>(Lcom/android/keychain/KeyChainActivity;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->this$0:Lcom/android/keychain/KeyChainActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mSubjects:Ljava/util/List;

    iput-object p2, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;

    iget-object v1, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mSubjects:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/keychain/KeyChainActivity;Ljava/util/List;Lcom/android/keychain/KeyChainActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/keychain/KeyChainActivity;
    .param p2    # Ljava/util/List;
    .param p3    # Lcom/android/keychain/KeyChainActivity$1;

    invoke-direct {p0, p1, p2}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;-><init>(Lcom/android/keychain/KeyChainActivity;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mSubjects:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v10, 0x0

    const/4 v8, 0x0

    if-nez p2, :cond_0

    iget-object v7, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->this$0:Lcom/android/keychain/KeyChainActivity;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v7, 0x7f020003

    invoke-virtual {v3, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;

    invoke-direct {v2, v10}, Lcom/android/keychain/KeyChainActivity$ViewHolder;-><init>(Lcom/android/keychain/KeyChainActivity$1;)V

    const v7, 0x7f050002

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;->mAliasTextView:Landroid/widget/TextView;

    const v7, 0x7f050003

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;->mSubjectTextView:Landroid/widget/TextView;

    const v7, 0x7f050004

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v7, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v7, v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;->mAliasTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mSubjects:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-nez v6, :cond_1

    new-instance v7, Lcom/android/keychain/KeyChainActivity$CertificateAdapter$CertLoader;

    iget-object v9, v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;->mSubjectTextView:Landroid/widget/TextView;

    invoke-direct {v7, p0, p1, v9, v10}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter$CertLoader;-><init>(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;ILandroid/widget/TextView;Lcom/android/keychain/KeyChainActivity$1;)V

    new-array v9, v8, [Ljava/lang/Void;

    invoke-virtual {v7, v9}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    move-object v5, p3

    check-cast v5, Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/AbsListView;->getCheckedItemPosition()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    iget-object v9, v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_2

    const/4 v7, 0x1

    :goto_2
    invoke-virtual {v9, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;

    goto :goto_0

    :cond_1
    iget-object v7, v2, Lcom/android/keychain/KeyChainActivity$ViewHolder;->mSubjectTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    move v7, v8

    goto :goto_2
.end method
