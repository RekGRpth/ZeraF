.class public abstract Lcom/android/location/provider/LocationProviderBase;
.super Ljava/lang/Object;
.source "LocationProviderBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/location/provider/LocationProviderBase$1;,
        Lcom/android/location/provider/LocationProviderBase$Service;
    }
.end annotation


# static fields
.field public static final EXTRA_NO_GPS_LOCATION:Ljava/lang/String; = "noGPSLocation"

.field public static final FUSED_PROVIDER:Ljava/lang/String; = "fused"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mBinder:Landroid/os/IBinder;

.field protected final mLocationManager:Landroid/location/ILocationManager;

.field private final mProperties:Lcom/android/internal/location/ProviderProperties;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/location/provider/ProviderPropertiesUnbundled;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/location/provider/ProviderPropertiesUnbundled;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/location/provider/LocationProviderBase;->TAG:Ljava/lang/String;

    const-string v1, "location"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/location/ILocationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/ILocationManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/location/provider/LocationProviderBase;->mLocationManager:Landroid/location/ILocationManager;

    invoke-virtual {p2}, Lcom/android/location/provider/ProviderPropertiesUnbundled;->getProviderProperties()Lcom/android/internal/location/ProviderProperties;

    move-result-object v1

    iput-object v1, p0, Lcom/android/location/provider/LocationProviderBase;->mProperties:Lcom/android/internal/location/ProviderProperties;

    new-instance v1, Lcom/android/location/provider/LocationProviderBase$Service;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/location/provider/LocationProviderBase$Service;-><init>(Lcom/android/location/provider/LocationProviderBase;Lcom/android/location/provider/LocationProviderBase$1;)V

    iput-object v1, p0, Lcom/android/location/provider/LocationProviderBase;->mBinder:Landroid/os/IBinder;

    return-void
.end method

.method static synthetic access$000(Lcom/android/location/provider/LocationProviderBase;)Lcom/android/internal/location/ProviderProperties;
    .locals 1
    .param p0    # Lcom/android/location/provider/LocationProviderBase;

    iget-object v0, p0, Lcom/android/location/provider/LocationProviderBase;->mProperties:Lcom/android/internal/location/ProviderProperties;

    return-object v0
.end method


# virtual methods
.method public getBinder()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/LocationProviderBase;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public abstract onDisable()V
.end method

.method public onDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    return-void
.end method

.method public abstract onEnable()V
.end method

.method public abstract onGetStatus(Landroid/os/Bundle;)I
.end method

.method public abstract onGetStatusUpdateTime()J
.end method

.method public onSendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x0

    return v0
.end method

.method public abstract onSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V
.end method

.method public final reportLocation(Landroid/location/Location;)V
    .locals 3
    .param p1    # Landroid/location/Location;

    :try_start_0
    iget-object v1, p0, Lcom/android/location/provider/LocationProviderBase;->mLocationManager:Landroid/location/ILocationManager;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Landroid/location/ILocationManager;->reportLocation(Landroid/location/Location;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/android/location/provider/LocationProviderBase;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/android/location/provider/LocationProviderBase;->TAG:Ljava/lang/String;

    const-string v2, "Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
