.class public Lcom/mediatek/phone/GeminiConstants;
.super Ljava/lang/Object;
.source "GeminiConstants.java"


# static fields
.field public static final FDN_CONTENT:Ljava/lang/String; = "content://icc/fdn"

.field public static final FDN_CONTENT_GEMINI:[Ljava/lang/String;

.field public static final FDN_CONTENT_SIM1:Ljava/lang/String; = "content://icc/fdn1"

.field public static final FDN_CONTENT_SIM2:Ljava/lang/String; = "content://icc/fdn2"

.field public static final FDN_CONTENT_SIM3:Ljava/lang/String; = "content://icc/fdn3"

.field public static final FDN_CONTENT_SIM4:Ljava/lang/String; = "content://icc/fdn4"

.field public static final GSM_ROAMING_INDICATOR_NEEDED:Ljava/lang/String; = "gsm.roaming.indicator.needed"

.field public static final GSM_ROAMING_INDICATOR_NEEDED_2:Ljava/lang/String; = "gsm.roaming.indicator.needed.2"

.field public static final GSM_ROAMING_INDICATOR_NEEDED_3:Ljava/lang/String; = "gsm.roaming.indicator.needed.3"

.field public static final GSM_ROAMING_INDICATOR_NEEDED_4:Ljava/lang/String; = "gsm.roaming.indicator.needed.4"

.field public static final GSM_ROAMING_INDICATOR_NEEDED_GEMINI:[Ljava/lang/String;

.field public static final GSM_SIM_RETRY_PIN:Ljava/lang/String; = "gsm.sim.retry.pin"

.field public static final GSM_SIM_RETRY_PIN2:Ljava/lang/String; = "gsm.sim.retry.pin2"

.field public static final GSM_SIM_RETRY_PIN2_2:Ljava/lang/String; = "gsm.sim.retry.pin2.2"

.field public static final GSM_SIM_RETRY_PIN2_3:Ljava/lang/String; = "gsm.sim.retry.pin2.3"

.field public static final GSM_SIM_RETRY_PIN2_4:Ljava/lang/String; = "gsm.sim.retry.pin2.4"

.field public static final GSM_SIM_RETRY_PIN2_GEMINI:[Ljava/lang/String;

.field public static final GSM_SIM_RETRY_PIN_2:Ljava/lang/String; = "gsm.sim.retry.pin1.2"

.field public static final GSM_SIM_RETRY_PIN_3:Ljava/lang/String; = "gsm.sim.retry.pin1.3"

.field public static final GSM_SIM_RETRY_PIN_4:Ljava/lang/String; = "gsm.sim.retry.pin1.4"

.field public static final GSM_SIM_RETRY_PIN_GEMINI:[Ljava/lang/String;

.field public static final GSM_SIM_RETRY_PUK2:Ljava/lang/String; = "gsm.sim.retry.puk2"

.field public static final GSM_SIM_RETRY_PUK2_2:Ljava/lang/String; = "gsm.sim.retry.puk2.2"

.field public static final GSM_SIM_RETRY_PUK2_3:Ljava/lang/String; = "gsm.sim.retry.puk2.3"

.field public static final GSM_SIM_RETRY_PUK2_4:Ljava/lang/String; = "gsm.sim.retry.puk2.4"

.field public static final GSM_SIM_RETRY_PUK2_GEMINI:[Ljava/lang/String;

.field public static final NETWORK_SELECTION_KEY:Ljava/lang/String; = "network_selection_key"

.field public static final NETWORK_SELECTION_KEYS:[Ljava/lang/String;

.field public static final NETWORK_SELECTION_KEY_2:Ljava/lang/String; = "network_selection_key_2"

.field public static final NETWORK_SELECTION_KEY_3:Ljava/lang/String; = "network_selection_key_3"

.field public static final NETWORK_SELECTION_KEY_4:Ljava/lang/String; = "network_selection_key_4"

.field public static final NETWORK_SELECTION_NAME_KEY:Ljava/lang/String; = "network_selection_name_key"

.field public static final NETWORK_SELECTION_NAME_KEYS:[Ljava/lang/String;

.field public static final NETWORK_SELECTION_NAME_KEY_2:Ljava/lang/String; = "network_selection_name_key_2"

.field public static final NETWORK_SELECTION_NAME_KEY_3:Ljava/lang/String; = "network_selection_name_key_3"

.field public static final NETWORK_SELECTION_NAME_KEY_4:Ljava/lang/String; = "network_selection_name_key_4"

.field public static final PROPERTY_CS_NETWORK_TYPE:Ljava/lang/String; = "gsm.cs.network.type"

.field public static final PROPERTY_CS_NETWORK_TYPES:[Ljava/lang/String;

.field public static final PROPERTY_CS_NETWORK_TYPE_2:Ljava/lang/String; = "gsm.cs.network.type.2"

.field public static final PROPERTY_CS_NETWORK_TYPE_3:Ljava/lang/String; = "gsm.cs.network.type.3"

.field public static final PROPERTY_CS_NETWORK_TYPE_4:Ljava/lang/String; = "gsm.cs.network.type.4"

.field public static final PROPERTY_OPERATOR_ALPHA:Ljava/lang/String; = "gsm.operator.alpha"

.field public static final PROPERTY_OPERATOR_ALPHAS:[Ljava/lang/String;

.field public static final PROPERTY_OPERATOR_ALPHA_2:Ljava/lang/String; = "gsm.operator.alpha.2"

.field public static final PROPERTY_OPERATOR_ALPHA_3:Ljava/lang/String; = "gsm.operator.alpha.3"

.field public static final PROPERTY_OPERATOR_ALPHA_4:Ljava/lang/String; = "gsm.operator.alpha.4"

.field public static final PROPERTY_SIM_STATE:Ljava/lang/String; = "gsm.sim.state"

.field public static final PROPERTY_SIM_STATES:[Ljava/lang/String;

.field public static final PROPERTY_SIM_STATE_2:Ljava/lang/String; = "gsm.sim.state.2"

.field public static final PROPERTY_SIM_STATE_3:Ljava/lang/String; = "gsm.sim.state.3"

.field public static final PROPERTY_SIM_STATE_4:Ljava/lang/String; = "gsm.sim.state.4"

.field public static final SLOTS:[I

.field public static final SLOT_ID_1:I = 0x0

.field public static final SLOT_ID_2:I = 0x1

.field public static final SLOT_ID_3:I = 0x2

.field public static final SLOT_ID_4:I = 0x3

.field public static final SLOT_ID_KEY:Ljava/lang/String; = "simId"

.field public static final SOLT_NUM:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget v0, Lcom/android/internal/telephony/PhoneConstants;->GEMINI_SIM_NUM:I

    sput v0, Lcom/mediatek/phone/GeminiConstants;->SOLT_NUM:I

    sget v0, Lcom/mediatek/phone/GeminiConstants;->SOLT_NUM:I

    packed-switch v0, :pswitch_data_0

    new-array v0, v3, [I

    aput v2, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->SLOTS:[I

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "network_selection_key"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_KEYS:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "network_selection_name_key"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_NAME_KEYS:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gsm.operator.alpha"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_OPERATOR_ALPHAS:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gsm.sim.state"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_SIM_STATES:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gsm.cs.network.type"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_CS_NETWORK_TYPES:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "content://icc/fdn"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->FDN_CONTENT_GEMINI:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN_GEMINI:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin2"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN2_GEMINI:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.puk2"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PUK2_GEMINI:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "gsm.roaming.indicator.needed"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_ROAMING_INDICATOR_NEEDED_GEMINI:[Ljava/lang/String;

    :goto_0
    return-void

    :pswitch_0
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->SLOTS:[I

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "network_selection_key"

    aput-object v1, v0, v2

    const-string v1, "network_selection_key_2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_KEYS:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "network_selection_name_key"

    aput-object v1, v0, v2

    const-string v1, "network_selection_name_key_2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_NAME_KEYS:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gsm.operator.alpha"

    aput-object v1, v0, v2

    const-string v1, "gsm.operator.alpha.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_OPERATOR_ALPHAS:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gsm.sim.state"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.state.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_SIM_STATES:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gsm.cs.network.type"

    aput-object v1, v0, v2

    const-string v1, "gsm.cs.network.type.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_CS_NETWORK_TYPES:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "content://icc/fdn1"

    aput-object v1, v0, v2

    const-string v1, "content://icc/fdn2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->FDN_CONTENT_GEMINI:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.pin1.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN_GEMINI:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin2"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.pin2.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN2_GEMINI:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.puk2"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.puk2.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PUK2_GEMINI:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gsm.roaming.indicator.needed"

    aput-object v1, v0, v2

    const-string v1, "gsm.roaming.indicator.needed.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_ROAMING_INDICATOR_NEEDED_GEMINI:[Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->SLOTS:[I

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "network_selection_key"

    aput-object v1, v0, v2

    const-string v1, "network_selection_key_2"

    aput-object v1, v0, v3

    const-string v1, "network_selection_key_3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_KEYS:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "network_selection_name_key"

    aput-object v1, v0, v2

    const-string v1, "network_selection_name_key_2"

    aput-object v1, v0, v3

    const-string v1, "network_selection_name_key_3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_NAME_KEYS:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gsm.operator.alpha"

    aput-object v1, v0, v2

    const-string v1, "gsm.operator.alpha.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.operator.alpha.3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_OPERATOR_ALPHAS:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gsm.sim.state"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.state.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.state.3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_SIM_STATES:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gsm.cs.network.type"

    aput-object v1, v0, v2

    const-string v1, "gsm.cs.network.type.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.cs.network.type.3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_CS_NETWORK_TYPES:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "content://icc/fdn1"

    aput-object v1, v0, v2

    const-string v1, "content://icc/fdn2"

    aput-object v1, v0, v3

    const-string v1, "content://icc/fdn3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->FDN_CONTENT_GEMINI:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.pin1.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.retry.pin1.3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN_GEMINI:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin2"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.pin2.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.retry.pin2.3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN2_GEMINI:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.puk2"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.puk2.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.retry.puk2.3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PUK2_GEMINI:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gsm.roaming.indicator.needed"

    aput-object v1, v0, v2

    const-string v1, "gsm.roaming.indicator.needed.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.roaming.indicator.needed.3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_ROAMING_INDICATOR_NEEDED_GEMINI:[Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_2
    new-array v0, v6, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->SLOTS:[I

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "network_selection_key"

    aput-object v1, v0, v2

    const-string v1, "network_selection_key_2"

    aput-object v1, v0, v3

    const-string v1, "network_selection_key_3"

    aput-object v1, v0, v4

    const-string v1, "network_selection_key_4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_KEYS:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "network_selection_name_key"

    aput-object v1, v0, v2

    const-string v1, "network_selection_name_key_2"

    aput-object v1, v0, v3

    const-string v1, "network_selection_name_key_3"

    aput-object v1, v0, v4

    const-string v1, "network_selection_name_key_4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->NETWORK_SELECTION_NAME_KEYS:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gsm.operator.alpha"

    aput-object v1, v0, v2

    const-string v1, "gsm.operator.alpha.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.operator.alpha.3"

    aput-object v1, v0, v4

    const-string v1, "gsm.operator.alpha.4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_OPERATOR_ALPHAS:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gsm.sim.state"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.state.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.state.3"

    aput-object v1, v0, v4

    const-string v1, "gsm.sim.state.4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_SIM_STATES:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gsm.cs.network.type"

    aput-object v1, v0, v2

    const-string v1, "gsm.cs.network.type.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.cs.network.type.3"

    aput-object v1, v0, v4

    const-string v1, "gsm.cs.network.type.4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->PROPERTY_CS_NETWORK_TYPES:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "content://icc/fdn1"

    aput-object v1, v0, v2

    const-string v1, "content://icc/fdn2"

    aput-object v1, v0, v3

    const-string v1, "content://icc/fdn3"

    aput-object v1, v0, v4

    const-string v1, "content://icc/fdn4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->FDN_CONTENT_GEMINI:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.pin1.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.retry.pin1.3"

    aput-object v1, v0, v4

    const-string v1, "gsm.sim.retry.pin1.4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN_GEMINI:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.pin2"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.pin2.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.retry.pin2.3"

    aput-object v1, v0, v4

    const-string v1, "gsm.sim.retry.pin2.4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PIN2_GEMINI:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gsm.sim.retry.puk2"

    aput-object v1, v0, v2

    const-string v1, "gsm.sim.retry.puk2.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.sim.retry.puk2.3"

    aput-object v1, v0, v4

    const-string v1, "gsm.sim.retry.puk2.4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_SIM_RETRY_PUK2_GEMINI:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gsm.roaming.indicator.needed"

    aput-object v1, v0, v2

    const-string v1, "gsm.roaming.indicator.needed.2"

    aput-object v1, v0, v3

    const-string v1, "gsm.roaming.indicator.needed.3"

    aput-object v1, v0, v4

    const-string v1, "gsm.roaming.indicator.needed.4"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/phone/GeminiConstants;->GSM_ROAMING_INDICATOR_NEEDED_GEMINI:[Ljava/lang/String;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
        0x2
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
