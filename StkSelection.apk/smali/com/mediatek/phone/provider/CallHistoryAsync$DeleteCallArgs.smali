.class public Lcom/mediatek/phone/provider/CallHistoryAsync$DeleteCallArgs;
.super Ljava/lang/Object;
.source "CallHistoryAsync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/provider/CallHistoryAsync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteCallArgs"
.end annotation


# instance fields
.field public final mContext:Landroid/content/Context;

.field public final mNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$DeleteCallArgs;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$DeleteCallArgs;->mNumber:Ljava/lang/String;

    return-void
.end method
