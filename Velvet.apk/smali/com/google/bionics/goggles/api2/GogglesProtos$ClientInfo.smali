.class public final Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GogglesProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/bionics/goggles/api2/GogglesProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientInfo"
.end annotation


# instance fields
.field private applicationId_:Ljava/lang/String;

.field private applicationVersion_:Ljava/lang/String;

.field private cachedSize:I

.field private canLogImage_:Z

.field private canLogLocation_:Z

.field private deviceModel_:Ljava/lang/String;

.field private disclosedCapabilities_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private frontendName_:I

.field private hasApplicationId:Z

.field private hasApplicationVersion:Z

.field private hasCanLogImage:Z

.field private hasCanLogLocation:Z

.field private hasDeviceModel:Z

.field private hasFrontendName:Z

.field private hasInstallId:Z

.field private hasIsSpdy:Z

.field private hasIsSsl:Z

.field private hasNetworkType:Z

.field private hasPlatformId:Z

.field private hasPlatformVersion:Z

.field private hasRoundTripTimeMs:Z

.field private hasTriggerApplicationId:Z

.field private hasUserIpAddress:Z

.field private installId_:Ljava/lang/String;

.field private isSpdy_:Z

.field private isSsl_:Z

.field private networkType_:I

.field private platformId_:Ljava/lang/String;

.field private platformVersion_:Ljava/lang/String;

.field private roundTripTimeMs_:I

.field private triggerApplicationId_:Ljava/lang/String;

.field private userIpAddress_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->installId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->platformId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->platformVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->deviceModel_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->applicationId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->applicationVersion_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->disclosedCapabilities_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->triggerApplicationId_:Ljava/lang/String;

    iput v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->networkType_:I

    iput-boolean v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->isSpdy_:Z

    iput-boolean v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->isSsl_:Z

    iput v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->roundTripTimeMs_:I

    iput v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->frontendName_:I

    iput-boolean v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->canLogImage_:Z

    iput-boolean v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->canLogLocation_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->userIpAddress_:Ljava/lang/String;

    iput v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDisclosedCapabilities(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->disclosedCapabilities_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->disclosedCapabilities_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->disclosedCapabilities_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->applicationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->applicationVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->cachedSize:I

    return v0
.end method

.method public getCanLogImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->canLogImage_:Z

    return v0
.end method

.method public getCanLogLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->canLogLocation_:Z

    return v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->deviceModel_:Ljava/lang/String;

    return-object v0
.end method

.method public getDisclosedCapabilitiesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->disclosedCapabilities_:Ljava/util/List;

    return-object v0
.end method

.method public getFrontendName()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->frontendName_:I

    return v0
.end method

.method public getInstallId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->installId_:Ljava/lang/String;

    return-object v0
.end method

.method public getIsSpdy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->isSpdy_:Z

    return v0
.end method

.method public getIsSsl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->isSsl_:Z

    return v0
.end method

.method public getNetworkType()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->networkType_:I

    return v0
.end method

.method public getPlatformId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->platformId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlatformVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->platformVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getRoundTripTimeMs()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->roundTripTimeMs_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasInstallId()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getInstallId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformId()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getPlatformId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformVersion()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getPlatformVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasDeviceModel()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getDeviceModel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationId()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getApplicationId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationVersion()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getApplicationVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasTriggerApplicationId()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getTriggerApplicationId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasNetworkType()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getNetworkType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSpdy()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getIsSpdy()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSsl()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getIsSsl()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasRoundTripTimeMs()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getRoundTripTimeMs()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogImage()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getCanLogImage()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogLocation()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getCanLogLocation()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasFrontendName()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getFrontendName()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getDisclosedCapabilitiesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_e
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getDisclosedCapabilitiesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasUserIpAddress()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x11

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getUserIpAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    iput v3, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->cachedSize:I

    return v3
.end method

.method public getTriggerApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->triggerApplicationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getUserIpAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->userIpAddress_:Ljava/lang/String;

    return-object v0
.end method

.method public hasApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationId:Z

    return v0
.end method

.method public hasApplicationVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationVersion:Z

    return v0
.end method

.method public hasCanLogImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogImage:Z

    return v0
.end method

.method public hasCanLogLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogLocation:Z

    return v0
.end method

.method public hasDeviceModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasDeviceModel:Z

    return v0
.end method

.method public hasFrontendName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasFrontendName:Z

    return v0
.end method

.method public hasInstallId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasInstallId:Z

    return v0
.end method

.method public hasIsSpdy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSpdy:Z

    return v0
.end method

.method public hasIsSsl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSsl:Z

    return v0
.end method

.method public hasNetworkType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasNetworkType:Z

    return v0
.end method

.method public hasPlatformId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformId:Z

    return v0
.end method

.method public hasPlatformVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformVersion:Z

    return v0
.end method

.method public hasRoundTripTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasRoundTripTimeMs:Z

    return v0
.end method

.method public hasTriggerApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasTriggerApplicationId:Z

    return v0
.end method

.method public hasUserIpAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasUserIpAddress:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setInstallId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setPlatformId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setPlatformVersion(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setDeviceModel(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setApplicationId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setApplicationVersion(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setTriggerApplicationId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setNetworkType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setIsSpdy(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setIsSsl(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setRoundTripTimeMs(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setCanLogImage(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setCanLogLocation(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setFrontendName(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->addDisclosedCapabilities(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->setUserIpAddress(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x8a -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public setApplicationId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationId:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->applicationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setApplicationVersion(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationVersion:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->applicationVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setCanLogImage(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogImage:Z

    iput-boolean p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->canLogImage_:Z

    return-object p0
.end method

.method public setCanLogLocation(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogLocation:Z

    iput-boolean p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->canLogLocation_:Z

    return-object p0
.end method

.method public setDeviceModel(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasDeviceModel:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->deviceModel_:Ljava/lang/String;

    return-object p0
.end method

.method public setFrontendName(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasFrontendName:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->frontendName_:I

    return-object p0
.end method

.method public setInstallId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasInstallId:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->installId_:Ljava/lang/String;

    return-object p0
.end method

.method public setIsSpdy(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSpdy:Z

    iput-boolean p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->isSpdy_:Z

    return-object p0
.end method

.method public setIsSsl(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSsl:Z

    iput-boolean p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->isSsl_:Z

    return-object p0
.end method

.method public setNetworkType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasNetworkType:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->networkType_:I

    return-object p0
.end method

.method public setPlatformId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformId:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->platformId_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlatformVersion(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformVersion:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->platformVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setRoundTripTimeMs(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasRoundTripTimeMs:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->roundTripTimeMs_:I

    return-object p0
.end method

.method public setTriggerApplicationId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasTriggerApplicationId:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->triggerApplicationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setUserIpAddress(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasUserIpAddress:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->userIpAddress_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasInstallId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getInstallId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getPlatformId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasPlatformVersion()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getPlatformVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasDeviceModel()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getDeviceModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationId()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getApplicationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasApplicationVersion()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getApplicationVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasTriggerApplicationId()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getTriggerApplicationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasNetworkType()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getNetworkType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSpdy()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getIsSpdy()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasIsSsl()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getIsSsl()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasRoundTripTimeMs()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getRoundTripTimeMs()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogImage()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getCanLogImage()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasCanLogLocation()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getCanLogLocation()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasFrontendName()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getFrontendName()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getDisclosedCapabilitiesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/16 v2, 0x10

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_0

    :cond_e
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->hasUserIpAddress()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientInfo;->getUserIpAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    return-void
.end method
