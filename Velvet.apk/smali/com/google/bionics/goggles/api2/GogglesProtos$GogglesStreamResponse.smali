.class public final Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GogglesProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/bionics/goggles/api2/GogglesProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GogglesStreamResponse"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasHighestSequenceNumberComplete:Z

.field private hasHighestSequenceNumberReceived:Z

.field private hasResultSetNumber:Z

.field private hasSessionMetadata:Z

.field private highestSequenceNumberComplete_:I

.field private highestSequenceNumberReceived_:I

.field private resultSetNumber_:I

.field private results_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;"
        }
    .end annotation
.end field

.field private sessionMetadata_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->resultSetNumber_:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->sessionMetadata_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->results_:Ljava/util/List;

    iput v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->highestSequenceNumberReceived_:I

    iput v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->highestSequenceNumberComplete_:I

    iput v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addResults(Lcom/google/bionics/goggles/api2/GogglesProtos$Result;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->results_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->results_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->results_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->cachedSize:I

    return v0
.end method

.method public getHighestSequenceNumberComplete()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->highestSequenceNumberComplete_:I

    return v0
.end method

.method public getHighestSequenceNumberReceived()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->highestSequenceNumberReceived_:I

    return v0
.end method

.method public getResultSetNumber()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->resultSetNumber_:I

    return v0
.end method

.method public getResultsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->results_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasResultSetNumber()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultSetNumber()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasSessionMetadata()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getSessionMetadata()Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberReceived()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberReceived()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberComplete()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberComplete()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    iput v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->cachedSize:I

    return v2
.end method

.method public getSessionMetadata()Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->sessionMetadata_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    return-object v0
.end method

.method public hasHighestSequenceNumberComplete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberComplete:Z

    return v0
.end method

.method public hasHighestSequenceNumberReceived()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberReceived:Z

    return v0
.end method

.method public hasResultSetNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasResultSetNumber:Z

    return v0
.end method

.method public hasSessionMetadata()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasSessionMetadata:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->setResultSetNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->setSessionMetadata(Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->addResults(Lcom/google/bionics/goggles/api2/GogglesProtos$Result;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->setHighestSequenceNumberReceived(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->setHighestSequenceNumberComplete(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    move-result-object v0

    return-object v0
.end method

.method public setHighestSequenceNumberComplete(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberComplete:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->highestSequenceNumberComplete_:I

    return-object p0
.end method

.method public setHighestSequenceNumberReceived(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberReceived:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->highestSequenceNumberReceived_:I

    return-object p0
.end method

.method public setResultSetNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasResultSetNumber:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->resultSetNumber_:I

    return-object p0
.end method

.method public setSessionMetadata(Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasSessionMetadata:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->sessionMetadata_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasResultSetNumber()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultSetNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasSessionMetadata()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getSessionMetadata()Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberReceived()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberReceived()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasHighestSequenceNumberComplete()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberComplete()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
