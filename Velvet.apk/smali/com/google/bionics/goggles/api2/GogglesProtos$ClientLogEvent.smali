.class public final Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GogglesProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/bionics/goggles/api2/GogglesProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientLogEvent"
.end annotation


# instance fields
.field private cachedSize:I

.field private clientEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

.field private clientTimeMs_:J

.field private hasClientEvent:Z

.field private hasClientTimeMs:Z

.field private hasImpression:Z

.field private hasUserEvent:Z

.field private impression_:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

.field private userEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->clientTimeMs_:J

    iput-object v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->userEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    iput-object v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->clientEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    iput-object v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->impression_:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->cachedSize:I

    return v0
.end method

.method public getClientEvent()Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->clientEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    return-object v0
.end method

.method public getClientTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->clientTimeMs_:J

    return-wide v0
.end method

.method public getImpression()Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->impression_:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientTimeMs()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getClientTimeMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasUserEvent()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getUserEvent()Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientEvent()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getClientEvent()Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasImpression()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getImpression()Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->cachedSize:I

    return v0
.end method

.method public getUserEvent()Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->userEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    return-object v0
.end method

.method public hasClientEvent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientEvent:Z

    return v0
.end method

.method public hasClientTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientTimeMs:Z

    return v0
.end method

.method public hasImpression()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasImpression:Z

    return v0
.end method

.method public hasUserEvent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasUserEvent:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientTimeMs(J)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setUserEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setImpression(Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public setClientEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientEvent:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->clientEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    return-object p0
.end method

.method public setClientTimeMs(J)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientTimeMs:Z

    iput-wide p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->clientTimeMs_:J

    return-object p0
.end method

.method public setImpression(Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasImpression:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->impression_:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    return-object p0
.end method

.method public setUserEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasUserEvent:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->userEvent_:Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientTimeMs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getClientTimeMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasUserEvent()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getUserEvent()Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasClientEvent()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getClientEvent()Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->hasImpression()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->getImpression()Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    return-void
.end method
