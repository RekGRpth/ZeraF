.class public final Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GogglesProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/bionics/goggles/api2/GogglesProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pose"
.end annotation


# instance fields
.field private altitudeMeters_:F

.field private cachedSize:I

.field private hasAltitudeMeters:Z

.field private hasLatDegrees:Z

.field private hasLatLngAccuracyMeters:Z

.field private hasLngDegrees:Z

.field private latDegrees_:D

.field private latLngAccuracyMeters_:F

.field private lngDegrees_:D


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->latDegrees_:D

    iput-wide v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->lngDegrees_:D

    iput v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->latLngAccuracyMeters_:F

    iput v2, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->altitudeMeters_:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAltitudeMeters()F
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->altitudeMeters_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->cachedSize:I

    return v0
.end method

.method public getLatDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->latDegrees_:D

    return-wide v0
.end method

.method public getLatLngAccuracyMeters()F
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->latLngAccuracyMeters_:F

    return v0
.end method

.method public getLngDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->lngDegrees_:D

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatDegrees()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getLatDegrees()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLngDegrees()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getLngDegrees()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatLngAccuracyMeters()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getLatLngAccuracyMeters()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasAltitudeMeters()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getAltitudeMeters()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->cachedSize:I

    return v0
.end method

.method public hasAltitudeMeters()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasAltitudeMeters:Z

    return v0
.end method

.method public hasLatDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatDegrees:Z

    return v0
.end method

.method public hasLatLngAccuracyMeters()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatLngAccuracyMeters:Z

    return v0
.end method

.method public hasLngDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLngDegrees:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setLatDegrees(D)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setLngDegrees(D)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setLatLngAccuracyMeters(F)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setAltitudeMeters(F)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    move-result-object v0

    return-object v0
.end method

.method public setAltitudeMeters(F)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasAltitudeMeters:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->altitudeMeters_:F

    return-object p0
.end method

.method public setLatDegrees(D)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatDegrees:Z

    iput-wide p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->latDegrees_:D

    return-object p0
.end method

.method public setLatLngAccuracyMeters(F)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatLngAccuracyMeters:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->latLngAccuracyMeters_:F

    return-object p0
.end method

.method public setLngDegrees(D)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLngDegrees:Z

    iput-wide p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->lngDegrees_:D

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatDegrees()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getLatDegrees()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLngDegrees()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getLngDegrees()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasLatLngAccuracyMeters()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getLatLngAccuracyMeters()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->hasAltitudeMeters()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->getAltitudeMeters()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    return-void
.end method
