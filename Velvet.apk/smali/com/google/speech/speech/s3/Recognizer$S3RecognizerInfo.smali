.class public final Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Recognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/speech/s3/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "S3RecognizerInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;
    }
.end annotation


# instance fields
.field private alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

.field private cachedSize:I

.field private condition_:Ljava/lang/String;

.field private confidenceThreshold_:F

.field private dictationMode_:Z

.field private enableAlternates_:Z

.field private enableCombinedNbest_:Z

.field private enableEndpointerEvents_:Z

.field private enableLattice_:Z

.field private enablePartialResults_:Z

.field private enablePersonalization_:Z

.field private enableSemanticResults_:Z

.field private grammarParams_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

.field private greco2CompatMode_:Z

.field private hasAlternateParams:Z

.field private hasCondition:Z

.field private hasConfidenceThreshold:Z

.field private hasDictationMode:Z

.field private hasEnableAlternates:Z

.field private hasEnableCombinedNbest:Z

.field private hasEnableEndpointerEvents:Z

.field private hasEnableLattice:Z

.field private hasEnablePartialResults:Z

.field private hasEnablePersonalization:Z

.field private hasEnableSemanticResults:Z

.field private hasGrammarParams:Z

.field private hasGreco2CompatMode:Z

.field private hasLoggingDataRequested:Z

.field private hasMaxNbest:Z

.field private hasProfanityFilter:Z

.field private hasRecognitionContext:Z

.field private loggingDataRequested_:Z

.field private maxNbest_:I

.field private profanityFilter_:I

.field private recognitionContext_:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v2, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->recognitionContext_:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->maxNbest_:I

    iput-object v2, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enablePartialResults_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableLattice_:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->profanityFilter_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->confidenceThreshold_:F

    iput-object v2, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->grammarParams_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->condition_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->dictationMode_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableSemanticResults_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableAlternates_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableCombinedNbest_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->greco2CompatMode_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enablePersonalization_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->loggingDataRequested_:Z

    iput-boolean v1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableEndpointerEvents_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAlternateParams()Lcom/google/speech/common/Alternates$AlternateParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->cachedSize:I

    return v0
.end method

.method public getCondition()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->condition_:Ljava/lang/String;

    return-object v0
.end method

.method public getConfidenceThreshold()F
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->confidenceThreshold_:F

    return v0
.end method

.method public getDictationMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->dictationMode_:Z

    return v0
.end method

.method public getEnableAlternates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableAlternates_:Z

    return v0
.end method

.method public getEnableCombinedNbest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableCombinedNbest_:Z

    return v0
.end method

.method public getEnableEndpointerEvents()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableEndpointerEvents_:Z

    return v0
.end method

.method public getEnableLattice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableLattice_:Z

    return v0
.end method

.method public getEnablePartialResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enablePartialResults_:Z

    return v0
.end method

.method public getEnablePersonalization()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enablePersonalization_:Z

    return v0
.end method

.method public getEnableSemanticResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableSemanticResults_:Z

    return v0
.end method

.method public getGrammarParams()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->grammarParams_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

    return-object v0
.end method

.method public getGreco2CompatMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->greco2CompatMode_:Z

    return v0
.end method

.method public getLoggingDataRequested()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->loggingDataRequested_:Z

    return v0
.end method

.method public getMaxNbest()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->maxNbest_:I

    return v0
.end method

.method public getProfanityFilter()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->profanityFilter_:I

    return v0
.end method

.method public getRecognitionContext()Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->recognitionContext_:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasRecognitionContext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getRecognitionContext()Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasMaxNbest()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getMaxNbest()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasAlternateParams()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getAlternateParams()Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePartialResults()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnablePartialResults()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableLattice()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableLattice()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasProfanityFilter()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getProfanityFilter()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasConfidenceThreshold()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getConfidenceThreshold()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGrammarParams()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getGrammarParams()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasCondition()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getCondition()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasDictationMode()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getDictationMode()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableSemanticResults()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableSemanticResults()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableAlternates()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableAlternates()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableCombinedNbest()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableCombinedNbest()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGreco2CompatMode()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getGreco2CompatMode()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePersonalization()Z

    move-result v1

    if-eqz v1, :cond_e

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnablePersonalization()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasLoggingDataRequested()Z

    move-result v1

    if-eqz v1, :cond_f

    const/16 v1, 0x11

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getLoggingDataRequested()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableEndpointerEvents()Z

    move-result v1

    if-eqz v1, :cond_10

    const/16 v1, 0x12

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableEndpointerEvents()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iput v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->cachedSize:I

    return v0
.end method

.method public hasAlternateParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasAlternateParams:Z

    return v0
.end method

.method public hasCondition()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasCondition:Z

    return v0
.end method

.method public hasConfidenceThreshold()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasConfidenceThreshold:Z

    return v0
.end method

.method public hasDictationMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasDictationMode:Z

    return v0
.end method

.method public hasEnableAlternates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableAlternates:Z

    return v0
.end method

.method public hasEnableCombinedNbest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableCombinedNbest:Z

    return v0
.end method

.method public hasEnableEndpointerEvents()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableEndpointerEvents:Z

    return v0
.end method

.method public hasEnableLattice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableLattice:Z

    return v0
.end method

.method public hasEnablePartialResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePartialResults:Z

    return v0
.end method

.method public hasEnablePersonalization()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePersonalization:Z

    return v0
.end method

.method public hasEnableSemanticResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableSemanticResults:Z

    return v0
.end method

.method public hasGrammarParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGrammarParams:Z

    return v0
.end method

.method public hasGreco2CompatMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGreco2CompatMode:Z

    return v0
.end method

.method public hasLoggingDataRequested()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasLoggingDataRequested:Z

    return v0
.end method

.method public hasMaxNbest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasMaxNbest:Z

    return v0
.end method

.method public hasProfanityFilter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasProfanityFilter:Z

    return v0
.end method

.method public hasRecognitionContext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasRecognitionContext:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    invoke-direct {v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setRecognitionContext(Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setMaxNbest(I)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/speech/common/Alternates$AlternateParams;

    invoke-direct {v1}, Lcom/google/speech/common/Alternates$AlternateParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setAlternateParams(Lcom/google/speech/common/Alternates$AlternateParams;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnablePartialResults(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnableLattice(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setProfanityFilter(I)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setConfidenceThreshold(F)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setGrammarParams(Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setCondition(Ljava/lang/String;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setDictationMode(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnableSemanticResults(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnableAlternates(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnableCombinedNbest(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setGreco2CompatMode(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnablePersonalization(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setLoggingDataRequested(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnableEndpointerEvents(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x45 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x90 -> :sswitch_11
    .end sparse-switch
.end method

.method public setAlternateParams(Lcom/google/speech/common/Alternates$AlternateParams;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Lcom/google/speech/common/Alternates$AlternateParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasAlternateParams:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

    return-object p0
.end method

.method public setCondition(Ljava/lang/String;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasCondition:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->condition_:Ljava/lang/String;

    return-object p0
.end method

.method public setConfidenceThreshold(F)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasConfidenceThreshold:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->confidenceThreshold_:F

    return-object p0
.end method

.method public setDictationMode(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasDictationMode:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->dictationMode_:Z

    return-object p0
.end method

.method public setEnableAlternates(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableAlternates:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableAlternates_:Z

    return-object p0
.end method

.method public setEnableCombinedNbest(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableCombinedNbest:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableCombinedNbest_:Z

    return-object p0
.end method

.method public setEnableEndpointerEvents(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableEndpointerEvents:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableEndpointerEvents_:Z

    return-object p0
.end method

.method public setEnableLattice(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableLattice:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableLattice_:Z

    return-object p0
.end method

.method public setEnablePartialResults(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePartialResults:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enablePartialResults_:Z

    return-object p0
.end method

.method public setEnablePersonalization(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePersonalization:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enablePersonalization_:Z

    return-object p0
.end method

.method public setEnableSemanticResults(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableSemanticResults:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->enableSemanticResults_:Z

    return-object p0
.end method

.method public setGrammarParams(Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGrammarParams:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->grammarParams_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

    return-object p0
.end method

.method public setGreco2CompatMode(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGreco2CompatMode:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->greco2CompatMode_:Z

    return-object p0
.end method

.method public setLoggingDataRequested(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasLoggingDataRequested:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->loggingDataRequested_:Z

    return-object p0
.end method

.method public setMaxNbest(I)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasMaxNbest:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->maxNbest_:I

    return-object p0
.end method

.method public setProfanityFilter(I)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasProfanityFilter:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->profanityFilter_:I

    return-object p0
.end method

.method public setRecognitionContext(Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1
    .param p1    # Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasRecognitionContext:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->recognitionContext_:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasRecognitionContext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getRecognitionContext()Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasMaxNbest()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getMaxNbest()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasAlternateParams()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getAlternateParams()Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePartialResults()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnablePartialResults()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableLattice()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableLattice()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasProfanityFilter()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getProfanityFilter()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasConfidenceThreshold()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getConfidenceThreshold()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGrammarParams()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getGrammarParams()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo$GrammarSelectorParams;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasCondition()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getCondition()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasDictationMode()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getDictationMode()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableSemanticResults()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableSemanticResults()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableAlternates()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableAlternates()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableCombinedNbest()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableCombinedNbest()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasGreco2CompatMode()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getGreco2CompatMode()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnablePersonalization()Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnablePersonalization()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasLoggingDataRequested()Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x11

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getLoggingDataRequested()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->hasEnableEndpointerEvents()Z

    move-result v0

    if-eqz v0, :cond_10

    const/16 v0, 0x12

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->getEnableEndpointerEvents()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_10
    return-void
.end method
