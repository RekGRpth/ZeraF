.class public final Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VoicesearchClientLogProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/logs/VoicesearchClientLogProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatencyData"
.end annotation


# instance fields
.field private breakdown_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private durationMsec_:I

.field private factor_:I

.field private hasDurationMsec:Z

.field private hasFactor:Z

.field private hasTimeout:Z

.field private timeout_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->durationMsec_:I

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->timeout_:Z

    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->factor_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->breakdown_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addBreakdown(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->breakdown_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->breakdown_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->breakdown_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBreakdown(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->breakdown_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    return-object v0
.end method

.method public getBreakdownCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->breakdown_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBreakdownList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->breakdown_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->cachedSize:I

    return v0
.end method

.method public getDurationMsec()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->durationMsec_:I

    return v0
.end method

.method public getFactor()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->factor_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasDurationMsec()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getDurationMsec()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasTimeout()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getTimeout()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasFactor()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getFactor()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getBreakdownList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    iput v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->cachedSize:I

    return v2
.end method

.method public getTimeout()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->timeout_:Z

    return v0
.end method

.method public hasDurationMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasDurationMsec:Z

    return v0
.end method

.method public hasFactor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasFactor:Z

    return v0
.end method

.method public hasTimeout()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasTimeout:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->setDurationMsec(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->setTimeout(Z)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->setFactor(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->addBreakdown(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public setDurationMsec(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasDurationMsec:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->durationMsec_:I

    return-object p0
.end method

.method public setFactor(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasFactor:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->factor_:I

    return-object p0
.end method

.method public setTimeout(Z)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasTimeout:Z

    iput-boolean p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->timeout_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasDurationMsec()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getDurationMsec()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasTimeout()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getTimeout()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->hasFactor()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getFactor()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->getBreakdownList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    return-void
.end method
