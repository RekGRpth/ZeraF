.class public final Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/logs/RecognizerOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognizerLog"
.end annotation


# instance fields
.field private audioEncoding_:I

.field private averageConfidence_:F

.field private cachedSize:I

.field private dEPRECATEDAcousticModelVersion_:Ljava/lang/String;

.field private dEPRECATEDLanguageModelVersion_:Ljava/lang/String;

.field private dEPRECATEDLexiconVersion_:Ljava/lang/String;

.field private dEPRECATEDTextnormVersion_:Ljava/lang/String;

.field private decoderGaussianSelectionCentroids_:I

.field private decoderLmWeight_:F

.field private decoderLocalBeam_:F

.field private decoderMaxArcs_:I

.field private decoderWordPen_:F

.field private hasAudioEncoding:Z

.field private hasAverageConfidence:Z

.field private hasDEPRECATEDAcousticModelVersion:Z

.field private hasDEPRECATEDLanguageModelVersion:Z

.field private hasDEPRECATEDLexiconVersion:Z

.field private hasDEPRECATEDTextnormVersion:Z

.field private hasDecoderGaussianSelectionCentroids:Z

.field private hasDecoderLmWeight:Z

.field private hasDecoderLocalBeam:Z

.field private hasDecoderMaxArcs:Z

.field private hasDecoderWordPen:Z

.field private hasLangPack:Z

.field private hasNoiseCancelerEnabled:Z

.field private hasPersonalizationEnabled:Z

.field private hasRecognizerContext:Z

.field private hasRecognizerLanguage:Z

.field private hasRecognizerStatus:Z

.field private hasRequestDurationMs:Z

.field private hasSampleRate:Z

.field private hasServerCluster:Z

.field private hasServerMachineName:Z

.field private hasSpokenLanguage:Z

.field private hasStartTimeMs:Z

.field private hasTopHypothesis:Z

.field private hasTotalAudioDurationMs:Z

.field private hasUtteranceId:Z

.field private hasWaveform:Z

.field private langPack_:Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

.field private noiseCancelerEnabled_:Z

.field private personalizationEnabled_:Z

.field private recognizerContext_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

.field private recognizerLanguage_:Ljava/lang/String;

.field private recognizerSegment_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;",
            ">;"
        }
    .end annotation
.end field

.field private recognizerStatus_:I

.field private requestDurationMs_:I

.field private sampleRate_:F

.field private serverCluster_:Ljava/lang/String;

.field private serverMachineName_:Ljava/lang/String;

.field private spokenLanguage_:Ljava/lang/String;

.field private startTimeMs_:J

.field private topHypothesis_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

.field private totalAudioDurationMs_:I

.field private utteranceId_:Ljava/lang/String;

.field private waveform_:Lcom/google/protobuf/micro/ByteStringMicro;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->utteranceId_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->waveform_:Lcom/google/protobuf/micro/ByteStringMicro;

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->audioEncoding_:I

    iput v2, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->sampleRate_:F

    iput-object v3, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerContext_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDAcousticModelVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDLanguageModelVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDTextnormVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDLexiconVersion_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderMaxArcs_:I

    iput v2, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderLocalBeam_:F

    iput v2, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderWordPen_:F

    iput v2, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderLmWeight_:F

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderGaussianSelectionCentroids_:I

    iput-boolean v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->noiseCancelerEnabled_:Z

    iput-object v3, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->topHypothesis_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->totalAudioDurationMs_:I

    iput v2, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->averageConfidence_:F

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerStatus_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->spokenLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerLanguage_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerSegment_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->serverCluster_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->serverMachineName_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->requestDurationMs_:I

    iput-boolean v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->personalizationEnabled_:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->startTimeMs_:J

    iput-object v3, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->langPack_:Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addRecognizerSegment(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerSegment_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerSegment_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerSegment_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAudioEncoding()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->audioEncoding_:I

    return v0
.end method

.method public getAverageConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->averageConfidence_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->cachedSize:I

    return v0
.end method

.method public getDEPRECATEDAcousticModelVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDAcousticModelVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getDEPRECATEDLanguageModelVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDLanguageModelVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getDEPRECATEDLexiconVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDLexiconVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getDEPRECATEDTextnormVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDTextnormVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getDecoderGaussianSelectionCentroids()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderGaussianSelectionCentroids_:I

    return v0
.end method

.method public getDecoderLmWeight()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderLmWeight_:F

    return v0
.end method

.method public getDecoderLocalBeam()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderLocalBeam_:F

    return v0
.end method

.method public getDecoderMaxArcs()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderMaxArcs_:I

    return v0
.end method

.method public getDecoderWordPen()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderWordPen_:F

    return v0
.end method

.method public getLangPack()Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->langPack_:Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    return-object v0
.end method

.method public getNoiseCancelerEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->noiseCancelerEnabled_:Z

    return v0
.end method

.method public getPersonalizationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->personalizationEnabled_:Z

    return v0
.end method

.method public getRecognizerContext()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerContext_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

    return-object v0
.end method

.method public getRecognizerLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognizerSegmentList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerSegment_:Ljava/util/List;

    return-object v0
.end method

.method public getRecognizerStatus()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerStatus_:I

    return v0
.end method

.method public getRequestDurationMs()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->requestDurationMs_:I

    return v0
.end method

.method public getSampleRate()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->sampleRate_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasUtteranceId()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getUtteranceId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasWaveform()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getWaveform()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAudioEncoding()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getAudioEncoding()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSampleRate()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getSampleRate()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerContext()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerContext()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDAcousticModelVersion()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDAcousticModelVersion()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLanguageModelVersion()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDLanguageModelVersion()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDTextnormVersion()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDTextnormVersion()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLexiconVersion()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDLexiconVersion()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderMaxArcs()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderMaxArcs()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLocalBeam()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderLocalBeam()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderWordPen()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderWordPen()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLmWeight()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderLmWeight()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderGaussianSelectionCentroids()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderGaussianSelectionCentroids()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasNoiseCancelerEnabled()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getNoiseCancelerEnabled()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTopHypothesis()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getTopHypothesis()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTotalAudioDurationMs()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getTotalAudioDurationMs()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAverageConfidence()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getAverageConfidence()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerStatus()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerStatus()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSpokenLanguage()Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getSpokenLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerLanguage()Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerSegmentList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    const/16 v3, 0x16

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_15
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerCluster()Z

    move-result v3

    if-eqz v3, :cond_16

    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getServerCluster()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_16
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRequestDurationMs()Z

    move-result v3

    if-eqz v3, :cond_17

    const/16 v3, 0x18

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRequestDurationMs()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_17
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasPersonalizationEnabled()Z

    move-result v3

    if-eqz v3, :cond_18

    const/16 v3, 0x19

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getPersonalizationEnabled()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_18
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasStartTimeMs()Z

    move-result v3

    if-eqz v3, :cond_19

    const/16 v3, 0x1a

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getStartTimeMs()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_19
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasLangPack()Z

    move-result v3

    if-eqz v3, :cond_1a

    const/16 v3, 0x1b

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getLangPack()Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1a
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerMachineName()Z

    move-result v3

    if-eqz v3, :cond_1b

    const/16 v3, 0x1c

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getServerMachineName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1b
    iput v2, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->cachedSize:I

    return v2
.end method

.method public getServerCluster()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->serverCluster_:Ljava/lang/String;

    return-object v0
.end method

.method public getServerMachineName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->serverMachineName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSpokenLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->spokenLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->startTimeMs_:J

    return-wide v0
.end method

.method public getTopHypothesis()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->topHypothesis_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    return-object v0
.end method

.method public getTotalAudioDurationMs()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->totalAudioDurationMs_:I

    return v0
.end method

.method public getUtteranceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->utteranceId_:Ljava/lang/String;

    return-object v0
.end method

.method public getWaveform()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->waveform_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public hasAudioEncoding()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAudioEncoding:Z

    return v0
.end method

.method public hasAverageConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAverageConfidence:Z

    return v0
.end method

.method public hasDEPRECATEDAcousticModelVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDAcousticModelVersion:Z

    return v0
.end method

.method public hasDEPRECATEDLanguageModelVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLanguageModelVersion:Z

    return v0
.end method

.method public hasDEPRECATEDLexiconVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLexiconVersion:Z

    return v0
.end method

.method public hasDEPRECATEDTextnormVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDTextnormVersion:Z

    return v0
.end method

.method public hasDecoderGaussianSelectionCentroids()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderGaussianSelectionCentroids:Z

    return v0
.end method

.method public hasDecoderLmWeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLmWeight:Z

    return v0
.end method

.method public hasDecoderLocalBeam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLocalBeam:Z

    return v0
.end method

.method public hasDecoderMaxArcs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderMaxArcs:Z

    return v0
.end method

.method public hasDecoderWordPen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderWordPen:Z

    return v0
.end method

.method public hasLangPack()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasLangPack:Z

    return v0
.end method

.method public hasNoiseCancelerEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasNoiseCancelerEnabled:Z

    return v0
.end method

.method public hasPersonalizationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasPersonalizationEnabled:Z

    return v0
.end method

.method public hasRecognizerContext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerContext:Z

    return v0
.end method

.method public hasRecognizerLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerLanguage:Z

    return v0
.end method

.method public hasRecognizerStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerStatus:Z

    return v0
.end method

.method public hasRequestDurationMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRequestDurationMs:Z

    return v0
.end method

.method public hasSampleRate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSampleRate:Z

    return v0
.end method

.method public hasServerCluster()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerCluster:Z

    return v0
.end method

.method public hasServerMachineName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerMachineName:Z

    return v0
.end method

.method public hasSpokenLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSpokenLanguage:Z

    return v0
.end method

.method public hasStartTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasStartTimeMs:Z

    return v0
.end method

.method public hasTopHypothesis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTopHypothesis:Z

    return v0
.end method

.method public hasTotalAudioDurationMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTotalAudioDurationMs:Z

    return v0
.end method

.method public hasUtteranceId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasUtteranceId:Z

    return v0
.end method

.method public hasWaveform()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasWaveform:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setUtteranceId(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setWaveform(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setAudioEncoding(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setSampleRate(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

    invoke-direct {v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setRecognizerContext(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDEPRECATEDAcousticModelVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDEPRECATEDLanguageModelVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDEPRECATEDTextnormVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDEPRECATEDLexiconVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDecoderMaxArcs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDecoderLocalBeam(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDecoderWordPen(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDecoderLmWeight(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setDecoderGaussianSelectionCentroids(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setNoiseCancelerEnabled(Z)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    invoke-direct {v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setTopHypothesis(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setTotalAudioDurationMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setAverageConfidence(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setRecognizerStatus(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setSpokenLanguage(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setRecognizerLanguage(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    invoke-direct {v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->addRecognizerSegment(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setServerCluster(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setRequestDurationMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setPersonalizationEnabled(Z)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setStartTimeMs(J)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_1b
    new-instance v1, Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    invoke-direct {v1}, Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setLangPack(Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->setServerMachineName(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x95 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
    .end sparse-switch
.end method

.method public setAudioEncoding(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAudioEncoding:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->audioEncoding_:I

    return-object p0
.end method

.method public setAverageConfidence(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAverageConfidence:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->averageConfidence_:F

    return-object p0
.end method

.method public setDEPRECATEDAcousticModelVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDAcousticModelVersion:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDAcousticModelVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setDEPRECATEDLanguageModelVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLanguageModelVersion:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDLanguageModelVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setDEPRECATEDLexiconVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLexiconVersion:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDLexiconVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setDEPRECATEDTextnormVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDTextnormVersion:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->dEPRECATEDTextnormVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setDecoderGaussianSelectionCentroids(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderGaussianSelectionCentroids:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderGaussianSelectionCentroids_:I

    return-object p0
.end method

.method public setDecoderLmWeight(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLmWeight:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderLmWeight_:F

    return-object p0
.end method

.method public setDecoderLocalBeam(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLocalBeam:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderLocalBeam_:F

    return-object p0
.end method

.method public setDecoderMaxArcs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderMaxArcs:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderMaxArcs_:I

    return-object p0
.end method

.method public setDecoderWordPen(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderWordPen:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->decoderWordPen_:F

    return-object p0
.end method

.method public setLangPack(Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasLangPack:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->langPack_:Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    return-object p0
.end method

.method public setNoiseCancelerEnabled(Z)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasNoiseCancelerEnabled:Z

    iput-boolean p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->noiseCancelerEnabled_:Z

    return-object p0
.end method

.method public setPersonalizationEnabled(Z)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasPersonalizationEnabled:Z

    iput-boolean p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->personalizationEnabled_:Z

    return-object p0
.end method

.method public setRecognizerContext(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerContext:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerContext_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

    return-object p0
.end method

.method public setRecognizerLanguage(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerLanguage:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setRecognizerStatus(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerStatus:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->recognizerStatus_:I

    return-object p0
.end method

.method public setRequestDurationMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRequestDurationMs:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->requestDurationMs_:I

    return-object p0
.end method

.method public setSampleRate(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSampleRate:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->sampleRate_:F

    return-object p0
.end method

.method public setServerCluster(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerCluster:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->serverCluster_:Ljava/lang/String;

    return-object p0
.end method

.method public setServerMachineName(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerMachineName:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->serverMachineName_:Ljava/lang/String;

    return-object p0
.end method

.method public setSpokenLanguage(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSpokenLanguage:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->spokenLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setStartTimeMs(J)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasStartTimeMs:Z

    iput-wide p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->startTimeMs_:J

    return-object p0
.end method

.method public setTopHypothesis(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTopHypothesis:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->topHypothesis_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    return-object p0
.end method

.method public setTotalAudioDurationMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTotalAudioDurationMs:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->totalAudioDurationMs_:I

    return-object p0
.end method

.method public setUtteranceId(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasUtteranceId:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->utteranceId_:Ljava/lang/String;

    return-object p0
.end method

.method public setWaveform(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasWaveform:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->waveform_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasUtteranceId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getUtteranceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasWaveform()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getWaveform()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAudioEncoding()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getAudioEncoding()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSampleRate()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getSampleRate()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerContext()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerContext()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerContextLog;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDAcousticModelVersion()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDAcousticModelVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLanguageModelVersion()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDLanguageModelVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDTextnormVersion()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDTextnormVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDEPRECATEDLexiconVersion()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDEPRECATEDLexiconVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderMaxArcs()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderMaxArcs()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLocalBeam()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderLocalBeam()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderWordPen()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderWordPen()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderLmWeight()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderLmWeight()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasDecoderGaussianSelectionCentroids()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getDecoderGaussianSelectionCentroids()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasNoiseCancelerEnabled()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getNoiseCancelerEnabled()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTopHypothesis()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getTopHypothesis()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasTotalAudioDurationMs()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getTotalAudioDurationMs()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasAverageConfidence()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getAverageConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerStatus()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerStatus()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasSpokenLanguage()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getSpokenLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRecognizerLanguage()Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRecognizerSegmentList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    const/16 v2, 0x16

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_15
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerCluster()Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getServerCluster()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_16
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasRequestDurationMs()Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x18

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getRequestDurationMs()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_17
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasPersonalizationEnabled()Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x19

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getPersonalizationEnabled()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_18
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasStartTimeMs()Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x1a

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getStartTimeMs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_19
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasLangPack()Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v2, 0x1b

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getLangPack()Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1a
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->hasServerMachineName()Z

    move-result v2

    if-eqz v2, :cond_1b

    const/16 v2, 0x1c

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;->getServerMachineName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1b
    return-void
.end method
