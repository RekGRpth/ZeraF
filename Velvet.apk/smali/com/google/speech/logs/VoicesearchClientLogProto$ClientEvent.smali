.class public final Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VoicesearchClientLogProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/logs/VoicesearchClientLogProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientEvent"
.end annotation


# instance fields
.field private actionType_:I

.field private alternateCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

.field private audioInputDevice_:I

.field private bugReport_:Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

.field private cachedSize:I

.field private clientTimeMs_:J

.field private contactInfo_:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

.field private embeddedRecognizerLog_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

.field private eventSource_:I

.field private eventType_:I

.field private hasActionType:Z

.field private hasAlternateCorrection:Z

.field private hasAudioInputDevice:Z

.field private hasBugReport:Z

.field private hasClientTimeMs:Z

.field private hasContactInfo:Z

.field private hasEmbeddedRecognizerLog:Z

.field private hasEventSource:Z

.field private hasEventType:Z

.field private hasLatency:Z

.field private hasOnDeviceSource:Z

.field private hasRequestId:Z

.field private hasRequestType:Z

.field private hasScreenTransition:Z

.field private hasTypingCorrection:Z

.field private latency_:Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

.field private onDeviceSource_:Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

.field private requestId_:Ljava/lang/String;

.field private requestType_:I

.field private screenTransition_:Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

.field private typingCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->clientTimeMs_:J

    iput v4, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->eventType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->requestId_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->screenTransition_:Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    iput v4, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->actionType_:I

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->latency_:Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->alternateCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->bugReport_:Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->embeddedRecognizerLog_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    iput v3, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->audioInputDevice_:I

    iput v3, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->eventSource_:I

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->contactInfo_:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    iput v3, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->requestType_:I

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->typingCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    iput-object v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->onDeviceSource_:Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getActionType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->actionType_:I

    return v0
.end method

.method public getAlternateCorrection()Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->alternateCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    return-object v0
.end method

.method public getAudioInputDevice()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->audioInputDevice_:I

    return v0
.end method

.method public getBugReport()Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->bugReport_:Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->cachedSize:I

    return v0
.end method

.method public getClientTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->clientTimeMs_:J

    return-wide v0
.end method

.method public getContactInfo()Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->contactInfo_:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    return-object v0
.end method

.method public getEmbeddedRecognizerLog()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->embeddedRecognizerLog_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    return-object v0
.end method

.method public getEventSource()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->eventSource_:I

    return v0
.end method

.method public getEventType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->eventType_:I

    return v0
.end method

.method public getLatency()Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->latency_:Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    return-object v0
.end method

.method public getOnDeviceSource()Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->onDeviceSource_:Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

    return-object v0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->requestId_:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->requestType_:I

    return v0
.end method

.method public getScreenTransition()Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->screenTransition_:Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasClientTimeMs()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getClientTimeMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventType()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getEventType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestId()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getRequestId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasScreenTransition()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getScreenTransition()Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasActionType()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getActionType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasLatency()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getLatency()Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAlternateCorrection()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getAlternateCorrection()Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasBugReport()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getBugReport()Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEmbeddedRecognizerLog()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getEmbeddedRecognizerLog()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAudioInputDevice()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getAudioInputDevice()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventSource()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getEventSource()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasContactInfo()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getContactInfo()Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestType()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getRequestType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasTypingCorrection()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getTypingCorrection()Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasOnDeviceSource()Z

    move-result v1

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getOnDeviceSource()Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->cachedSize:I

    return v0
.end method

.method public getTypingCorrection()Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->typingCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    return-object v0
.end method

.method public hasActionType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasActionType:Z

    return v0
.end method

.method public hasAlternateCorrection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAlternateCorrection:Z

    return v0
.end method

.method public hasAudioInputDevice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAudioInputDevice:Z

    return v0
.end method

.method public hasBugReport()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasBugReport:Z

    return v0
.end method

.method public hasClientTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasClientTimeMs:Z

    return v0
.end method

.method public hasContactInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasContactInfo:Z

    return v0
.end method

.method public hasEmbeddedRecognizerLog()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEmbeddedRecognizerLog:Z

    return v0
.end method

.method public hasEventSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventSource:Z

    return v0
.end method

.method public hasEventType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventType:Z

    return v0
.end method

.method public hasLatency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasLatency:Z

    return v0
.end method

.method public hasOnDeviceSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasOnDeviceSource:Z

    return v0
.end method

.method public hasRequestId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestId:Z

    return v0
.end method

.method public hasRequestType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestType:Z

    return v0
.end method

.method public hasScreenTransition()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasScreenTransition:Z

    return v0
.end method

.method public hasTypingCorrection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasTypingCorrection:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setClientTimeMs(J)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEventType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setScreenTransition(Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setActionType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setLatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setAlternateCorrection(Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setBugReport(Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    invoke-direct {v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEmbeddedRecognizerLog(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setAudioInputDevice(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEventSource(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setContactInfo(Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setTypingCorrection(Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setOnDeviceSource(Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public setActionType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasActionType:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->actionType_:I

    return-object p0
.end method

.method public setAlternateCorrection(Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAlternateCorrection:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->alternateCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    return-object p0
.end method

.method public setAudioInputDevice(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAudioInputDevice:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->audioInputDevice_:I

    return-object p0
.end method

.method public setBugReport(Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasBugReport:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->bugReport_:Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    return-object p0
.end method

.method public setClientTimeMs(J)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasClientTimeMs:Z

    iput-wide p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->clientTimeMs_:J

    return-object p0
.end method

.method public setContactInfo(Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasContactInfo:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->contactInfo_:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    return-object p0
.end method

.method public setEmbeddedRecognizerLog(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEmbeddedRecognizerLog:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->embeddedRecognizerLog_:Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    return-object p0
.end method

.method public setEventSource(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventSource:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->eventSource_:I

    return-object p0
.end method

.method public setEventType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventType:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->eventType_:I

    return-object p0
.end method

.method public setLatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasLatency:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->latency_:Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    return-object p0
.end method

.method public setOnDeviceSource(Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasOnDeviceSource:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->onDeviceSource_:Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

    return-object p0
.end method

.method public setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestId:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->requestId_:Ljava/lang/String;

    return-object p0
.end method

.method public setRequestType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestType:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->requestType_:I

    return-object p0
.end method

.method public setScreenTransition(Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasScreenTransition:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->screenTransition_:Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    return-object p0
.end method

.method public setTypingCorrection(Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasTypingCorrection:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->typingCorrection_:Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasClientTimeMs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getClientTimeMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventType()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getEventType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestId()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasScreenTransition()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getScreenTransition()Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasActionType()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getActionType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasLatency()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getLatency()Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAlternateCorrection()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getAlternateCorrection()Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasBugReport()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getBugReport()Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEmbeddedRecognizerLog()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getEmbeddedRecognizerLog()Lcom/google/speech/logs/RecognizerOuterClass$RecognizerLog;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasAudioInputDevice()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getAudioInputDevice()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasEventSource()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getEventSource()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasContactInfo()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getContactInfo()Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestType()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getRequestType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasTypingCorrection()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getTypingCorrection()Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasOnDeviceSource()Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getOnDeviceSource()Lcom/google/speech/logs/VoicesearchClientLogProto$OnDeviceSource;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_e
    return-void
.end method
