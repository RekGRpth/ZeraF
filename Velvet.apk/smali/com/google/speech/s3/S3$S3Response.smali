.class public final Lcom/google/speech/s3/S3$S3Response;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "S3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/S3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "S3Response"
.end annotation


# instance fields
.field private cachedSize:I

.field private debugLine_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private errorCode_:I

.field private errorDescription_:Ljava/lang/String;

.field private gogglesStreamResponseExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

.field private hasErrorCode:Z

.field private hasErrorDescription:Z

.field private hasGogglesStreamResponseExtension:Z

.field private hasMajelServiceEventExtension:Z

.field private hasPinholeOutputExtension:Z

.field private hasRecognizerEventExtension:Z

.field private hasSoundSearchServiceEventExtension:Z

.field private hasStatus:Z

.field private hasTtsServiceEventExtension:Z

.field private majelServiceEventExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

.field private pinholeOutputExtension_:Lcom/google/speech/s3/PinholeStream$PinholeOutput;

.field private recognizerEventExtension_:Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

.field private soundSearchServiceEventExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

.field private status_:I

.field private ttsServiceEventExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/s3/S3$S3Response;->status_:I

    iput v0, p0, Lcom/google/speech/s3/S3$S3Response;->errorCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->errorDescription_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->debugLine_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Response;->recognizerEventExtension_:Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Response;->majelServiceEventExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Response;->ttsServiceEventExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Response;->soundSearchServiceEventExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Response;->gogglesStreamResponseExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Response;->pinholeOutputExtension_:Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/S3$S3Response;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDebugLine(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->debugLine_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->debugLine_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->debugLine_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3Response;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/S3$S3Response;->cachedSize:I

    return v0
.end method

.method public getDebugLineList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->debugLine_:Ljava/util/List;

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3Response;->errorCode_:I

    return v0
.end method

.method public getErrorDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->errorDescription_:Ljava/lang/String;

    return-object v0
.end method

.method public getGogglesStreamResponseExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->gogglesStreamResponseExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    return-object v0
.end method

.method public getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->majelServiceEventExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    return-object v0
.end method

.method public getPinholeOutputExtension()Lcom/google/speech/s3/PinholeStream$PinholeOutput;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->pinholeOutputExtension_:Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    return-object v0
.end method

.method public getRecognizerEventExtension()Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->recognizerEventExtension_:Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasStatus()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasErrorCode()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasErrorDescription()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getDebugLineList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getDebugLineList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasRecognizerEventExtension()Z

    move-result v4

    if-eqz v4, :cond_4

    const v4, 0x1320f9

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getRecognizerEventExtension()Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasMajelServiceEventExtension()Z

    move-result v4

    if-eqz v4, :cond_5

    const v4, 0x195e184

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasTtsServiceEventExtension()Z

    move-result v4

    if-eqz v4, :cond_6

    const v4, 0x1b46604

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getTtsServiceEventExtension()Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasSoundSearchServiceEventExtension()Z

    move-result v4

    if-eqz v4, :cond_7

    const v4, 0x21b6d89

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getSoundSearchServiceEventExtension()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasGogglesStreamResponseExtension()Z

    move-result v4

    if-eqz v4, :cond_8

    const v4, 0x21c774f

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getGogglesStreamResponseExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasPinholeOutputExtension()Z

    move-result v4

    if-eqz v4, :cond_9

    const v4, 0x259d705

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getPinholeOutputExtension()Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    iput v3, p0, Lcom/google/speech/s3/S3$S3Response;->cachedSize:I

    return v3
.end method

.method public getSoundSearchServiceEventExtension()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->soundSearchServiceEventExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    return-object v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3Response;->status_:I

    return v0
.end method

.method public getTtsServiceEventExtension()Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Response;->ttsServiceEventExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    return-object v0
.end method

.method public hasErrorCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasErrorCode:Z

    return v0
.end method

.method public hasErrorDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasErrorDescription:Z

    return v0
.end method

.method public hasGogglesStreamResponseExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasGogglesStreamResponseExtension:Z

    return v0
.end method

.method public hasMajelServiceEventExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasMajelServiceEventExtension:Z

    return v0
.end method

.method public hasPinholeOutputExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasPinholeOutputExtension:Z

    return v0
.end method

.method public hasRecognizerEventExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasRecognizerEventExtension:Z

    return v0
.end method

.method public hasSoundSearchServiceEventExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasSoundSearchServiceEventExtension:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasStatus:Z

    return v0
.end method

.method public hasTtsServiceEventExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasTtsServiceEventExtension:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/S3$S3Response;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3Response;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/S3$S3Response;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Response;->setStatus(I)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Response;->setErrorCode(I)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Response;->setErrorDescription(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Response;->addDebugLine(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Response;->setRecognizerEventExtension(Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Response;->setMajelServiceEventExtension(Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Response;->setTtsServiceEventExtension(Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Response;->setSoundSearchServiceEventExtension(Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Response;->setGogglesStreamResponseExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    invoke-direct {v1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Response;->setPinholeOutputExtension(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x9907ca -> :sswitch_5
        0xcaf0c22 -> :sswitch_6
        0xda33022 -> :sswitch_7
        0x10db6c4a -> :sswitch_8
        0x10e3ba7a -> :sswitch_9
        0x12ceb82a -> :sswitch_a
    .end sparse-switch
.end method

.method public setErrorCode(I)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasErrorCode:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3Response;->errorCode_:I

    return-object p0
.end method

.method public setErrorDescription(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasErrorDescription:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Response;->errorDescription_:Ljava/lang/String;

    return-object p0
.end method

.method public setGogglesStreamResponseExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasGogglesStreamResponseExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Response;->gogglesStreamResponseExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    return-object p0
.end method

.method public setMajelServiceEventExtension(Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasMajelServiceEventExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Response;->majelServiceEventExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    return-object p0
.end method

.method public setPinholeOutputExtension(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasPinholeOutputExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Response;->pinholeOutputExtension_:Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    return-object p0
.end method

.method public setRecognizerEventExtension(Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasRecognizerEventExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Response;->recognizerEventExtension_:Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    return-object p0
.end method

.method public setSoundSearchServiceEventExtension(Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasSoundSearchServiceEventExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Response;->soundSearchServiceEventExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    return-object p0
.end method

.method public setStatus(I)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasStatus:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3Response;->status_:I

    return-object p0
.end method

.method public setTtsServiceEventExtension(Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;)Lcom/google/speech/s3/S3$S3Response;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Response;->hasTtsServiceEventExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Response;->ttsServiceEventExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasErrorCode()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasErrorDescription()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getDebugLineList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasRecognizerEventExtension()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x1320f9

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getRecognizerEventExtension()Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasMajelServiceEventExtension()Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x195e184

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasTtsServiceEventExtension()Z

    move-result v2

    if-eqz v2, :cond_6

    const v2, 0x1b46604

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getTtsServiceEventExtension()Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasSoundSearchServiceEventExtension()Z

    move-result v2

    if-eqz v2, :cond_7

    const v2, 0x21b6d89

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getSoundSearchServiceEventExtension()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasGogglesStreamResponseExtension()Z

    move-result v2

    if-eqz v2, :cond_8

    const v2, 0x21c774f

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getGogglesStreamResponseExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->hasPinholeOutputExtension()Z

    move-result v2

    if-eqz v2, :cond_9

    const v2, 0x259d705

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Response;->getPinholeOutputExtension()Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    return-void
.end method
