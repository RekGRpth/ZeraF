.class public final Lcom/google/speech/s3/S3$S3ConnectionInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "S3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/S3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "S3ConnectionInfo"
.end annotation


# instance fields
.field private cachedSize:I

.field private clientIp_:Ljava/lang/String;

.field private clientPort_:I

.field private debugIp_:Z

.field private gfeFrontlineInfo_:Ljava/lang/String;

.field private hasClientIp:Z

.field private hasClientPort:Z

.field private hasDebugIp:Z

.field private hasGfeFrontlineInfo:Z

.field private hasHost:Z

.field private hasImmediateClientIp:Z

.field private hasImmediateClientPort:Z

.field private hasType:Z

.field private host_:Ljava/lang/String;

.field private immediateClientIp_:Ljava/lang/String;

.field private immediateClientPort_:I

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->clientIp_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->clientPort_:I

    iput-boolean v1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->debugIp_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->immediateClientIp_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->immediateClientPort_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->gfeFrontlineInfo_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->host_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->cachedSize:I

    return v0
.end method

.method public getClientIp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->clientIp_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientPort()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->clientPort_:I

    return v0
.end method

.method public getDebugIp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->debugIp_:Z

    return v0
.end method

.method public getGfeFrontlineInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->gfeFrontlineInfo_:Ljava/lang/String;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->host_:Ljava/lang/String;

    return-object v0
.end method

.method public getImmediateClientIp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->immediateClientIp_:Ljava/lang/String;

    return-object v0
.end method

.method public getImmediateClientPort()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->immediateClientPort_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientIp()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getClientIp()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientPort()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getClientPort()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasDebugIp()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getDebugIp()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasGfeFrontlineInfo()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getGfeFrontlineInfo()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasHost()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientIp()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getImmediateClientIp()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientPort()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getImmediateClientPort()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->cachedSize:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->type_:I

    return v0
.end method

.method public hasClientIp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientIp:Z

    return v0
.end method

.method public hasClientPort()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientPort:Z

    return v0
.end method

.method public hasDebugIp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasDebugIp:Z

    return v0
.end method

.method public hasGfeFrontlineInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasGfeFrontlineInfo:Z

    return v0
.end method

.method public hasHost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasHost:Z

    return v0
.end method

.method public hasImmediateClientIp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientIp:Z

    return v0
.end method

.method public hasImmediateClientPort()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientPort:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasType:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setType(I)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setClientIp(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setClientPort(I)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setDebugIp(Z)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setGfeFrontlineInfo(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setHost(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setImmediateClientIp(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->setImmediateClientPort(I)Lcom/google/speech/s3/S3$S3ConnectionInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public setClientIp(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientIp:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->clientIp_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientPort(I)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientPort:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->clientPort_:I

    return-object p0
.end method

.method public setDebugIp(Z)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasDebugIp:Z

    iput-boolean p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->debugIp_:Z

    return-object p0
.end method

.method public setGfeFrontlineInfo(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasGfeFrontlineInfo:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->gfeFrontlineInfo_:Ljava/lang/String;

    return-object p0
.end method

.method public setHost(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasHost:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->host_:Ljava/lang/String;

    return-object p0
.end method

.method public setImmediateClientIp(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientIp:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->immediateClientIp_:Ljava/lang/String;

    return-object p0
.end method

.method public setImmediateClientPort(I)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientPort:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->immediateClientPort_:I

    return-object p0
.end method

.method public setType(I)Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasType:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3ConnectionInfo;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientIp()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getClientIp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasClientPort()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getClientPort()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasDebugIp()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getDebugIp()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasGfeFrontlineInfo()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getGfeFrontlineInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasHost()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientIp()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getImmediateClientIp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->hasImmediateClientPort()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ConnectionInfo;->getImmediateClientPort()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    return-void
.end method
