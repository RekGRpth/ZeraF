.class public Lcom/google/speech/grammar/pumpkin/ActionFrameManager;
.super Ljava/lang/Object;
.source "ActionFrameManager.java"


# instance fields
.field private nativeActionFrameManager:J


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeCreate()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeActionFrameManager:J

    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeActionFrameManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Couldn\'t create action_frame_manager from the provided config"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private declared-synchronized delete()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeActionFrameManager:J

    invoke-static {v0, v1}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeDelete(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static native nativeCreate()J
.end method

.method private static native nativeDelete(J)V
.end method

.method private static native nativeLoadActionFrame(J[B)J
.end method


# virtual methods
.method public createActionFrame(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;)Lcom/google/speech/grammar/pumpkin/ActionFrame;
    .locals 1
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;

    invoke-virtual {p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->createActionFrame([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v0

    return-object v0
.end method

.method public createActionFrame([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;
    .locals 5
    .param p1    # [B

    iget-wide v3, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeActionFrameManager:J

    invoke-static {v3, v4, p1}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeLoadActionFrame(J[B)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Couldn\'t create action_frame from the provided ActionSetConfig"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v0, Lcom/google/speech/grammar/pumpkin/ActionFrame;

    invoke-direct {v0, v1, v2}, Lcom/google/speech/grammar/pumpkin/ActionFrame;-><init>(J)V

    return-object v0
.end method

.method protected finalize()V
    .locals 0

    invoke-direct {p0}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->delete()V

    return-void
.end method
