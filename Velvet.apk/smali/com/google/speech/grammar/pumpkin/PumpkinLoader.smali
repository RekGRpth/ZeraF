.class public abstract Lcom/google/speech/grammar/pumpkin/PumpkinLoader;
.super Ljava/lang/Object;
.source "PumpkinLoader.java"


# static fields
.field protected static actionFrameManager:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

.field protected static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected final actionPath:Ljava/lang/String;

.field protected final grmPath:Ljava/lang/String;

.field protected pumpkinConfig:Ljava/io/File;

.field protected tagger:Lcom/google/speech/grammar/pumpkin/Tagger;

.field protected userValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->actionPath:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->grmPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->pumpkinConfig:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public getTagger()Lcom/google/speech/grammar/pumpkin/Tagger;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->tagger:Lcom/google/speech/grammar/pumpkin/Tagger;

    return-object v0
.end method

.method public getUserValidators()Lcom/google/speech/grammar/pumpkin/UserValidators;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->userValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    return-object v0
.end method

.method public declared-synchronized init()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->actionFrameManager:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    invoke-direct {v1}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;-><init>()V

    sput-object v1, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->actionFrameManager:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->loadPumpkinConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->tagger:Lcom/google/speech/grammar/pumpkin/Tagger;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/speech/grammar/pumpkin/Tagger;

    invoke-direct {v1, v0}, Lcom/google/speech/grammar/pumpkin/Tagger;-><init>(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;)V

    iput-object v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->tagger:Lcom/google/speech/grammar/pumpkin/Tagger;

    :cond_1
    iget-object v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->userValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    if-nez v1, :cond_2

    new-instance v1, Lcom/google/speech/grammar/pumpkin/UserValidators;

    invoke-direct {v1, v0}, Lcom/google/speech/grammar/pumpkin/UserValidators;-><init>(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;)V

    iput-object v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->userValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected abstract loadPumpkinConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
