.class public final Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/api/RecognizerProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognitionEvent"
.end annotation


# instance fields
.field private cachedSize:I

.field private combinedResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

.field private eventType_:I

.field private generationTimeMs_:J

.field private hasCombinedResult:Z

.field private hasEventType:Z

.field private hasGenerationTimeMs:Z

.field private hasPartialResult:Z

.field private hasResult:Z

.field private hasStatus:Z

.field private partialResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

.field private result_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

.field private status_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->eventType_:I

    iput v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->status_:I

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->result_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->partialResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->combinedResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->generationTimeMs_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->cachedSize:I

    return v0
.end method

.method public getCombinedResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->combinedResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    return-object v0
.end method

.method public getEventType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->eventType_:I

    return v0
.end method

.method public getGenerationTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->generationTimeMs_:J

    return-wide v0
.end method

.method public getPartialResult()Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->partialResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    return-object v0
.end method

.method public getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->result_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasEventType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasStatus()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getStatus()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasPartialResult()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getPartialResult()Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasCombinedResult()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getCombinedResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasGenerationTimeMs()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getGenerationTimeMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->cachedSize:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->status_:I

    return v0
.end method

.method public hasCombinedResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasCombinedResult:Z

    return v0
.end method

.method public hasEventType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasEventType:Z

    return v0
.end method

.method public hasGenerationTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasGenerationTimeMs:Z

    return v0
.end method

.method public hasPartialResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasPartialResult:Z

    return v0
.end method

.method public hasResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasStatus:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setEventType(I)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setStatus(I)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setPartialResult(Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setCombinedResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setGenerationTimeMs(J)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public setCombinedResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasCombinedResult:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->combinedResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    return-object p0
.end method

.method public setEventType(I)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasEventType:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->eventType_:I

    return-object p0
.end method

.method public setGenerationTimeMs(J)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasGenerationTimeMs:Z

    iput-wide p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->generationTimeMs_:J

    return-object p0
.end method

.method public setPartialResult(Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasPartialResult:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->partialResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    return-object p0
.end method

.method public setResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->result_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    return-object p0
.end method

.method public setStatus(I)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasStatus:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->status_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasEventType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getStatus()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasPartialResult()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getPartialResult()Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasCombinedResult()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getCombinedResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasGenerationTimeMs()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getGenerationTimeMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_5
    return-void
.end method
