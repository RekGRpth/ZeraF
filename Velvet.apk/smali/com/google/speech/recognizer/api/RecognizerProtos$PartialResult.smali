.class public final Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/api/RecognizerProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PartialResult"
.end annotation


# instance fields
.field private cachedSize:I

.field private endTimeUsec_:J

.field private hasEndTimeUsec:Z

.field private hasHotwordConfFeature:Z

.field private hasHotwordConfidence:Z

.field private hasHotwordFired:Z

.field private hasPhoneAlign:Z

.field private hasStartTimeUsec:Z

.field private hasStateAlign:Z

.field private hasWordAlign:Z

.field private hotwordConfFeature_:Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

.field private hotwordConfidence_:F

.field private hotwordFired_:Z

.field private part_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;",
            ">;"
        }
    .end annotation
.end field

.field private phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

.field private startTimeUsec_:J

.field private stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

.field private wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

.field private wordConfFeature_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->part_:Ljava/util/List;

    iput-wide v2, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->startTimeUsec_:J

    iput-wide v2, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->endTimeUsec_:J

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordConfFeature_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordConfFeature_:Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordConfidence_:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordFired_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addPart(Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->part_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->part_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->part_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addWordConfFeature(Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordConfFeature_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordConfFeature_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordConfFeature_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->cachedSize:I

    return v0
.end method

.method public getEndTimeUsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->endTimeUsec_:J

    return-wide v0
.end method

.method public getHotwordConfFeature()Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordConfFeature_:Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    return-object v0
.end method

.method public getHotwordConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordConfidence_:F

    return v0
.end method

.method public getHotwordFired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordFired_:Z

    return v0
.end method

.method public getPart(I)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->part_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    return-object v0
.end method

.method public getPartCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->part_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPartList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->part_:Ljava/util/List;

    return-object v0
.end method

.method public getPhoneAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getPartList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStartTimeUsec()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getStartTimeUsec()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasEndTimeUsec()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getEndTimeUsec()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasWordAlign()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getWordAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasPhoneAlign()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getPhoneAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getWordConfFeatureList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfFeature()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getHotwordConfFeature()Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfidence()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getHotwordConfidence()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordFired()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getHotwordFired()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStateAlign()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getStateAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    iput v2, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->cachedSize:I

    return v2
.end method

.method public getStartTimeUsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->startTimeUsec_:J

    return-wide v0
.end method

.method public getStateAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object v0
.end method

.method public getWordAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object v0
.end method

.method public getWordConfFeatureList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordConfFeature_:Ljava/util/List;

    return-object v0
.end method

.method public hasEndTimeUsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasEndTimeUsec:Z

    return v0
.end method

.method public hasHotwordConfFeature()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfFeature:Z

    return v0
.end method

.method public hasHotwordConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfidence:Z

    return v0
.end method

.method public hasHotwordFired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordFired:Z

    return v0
.end method

.method public hasPhoneAlign()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasPhoneAlign:Z

    return v0
.end method

.method public hasStartTimeUsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStartTimeUsec:Z

    return v0
.end method

.method public hasStateAlign()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStateAlign:Z

    return v0
.end method

.method public hasWordAlign()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasWordAlign:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->addPart(Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setStartTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setEndTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-direct {v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setWordAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-direct {v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setPhoneAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    invoke-direct {v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->addWordConfFeature(Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setHotwordConfFeature(Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setHotwordConfidence(F)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setHotwordFired(Z)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-direct {v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->setStateAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x45 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public setEndTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasEndTimeUsec:Z

    iput-wide p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->endTimeUsec_:J

    return-object p0
.end method

.method public setHotwordConfFeature(Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfFeature:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordConfFeature_:Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    return-object p0
.end method

.method public setHotwordConfidence(F)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfidence:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordConfidence_:F

    return-object p0
.end method

.method public setHotwordFired(Z)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordFired:Z

    iput-boolean p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hotwordFired_:Z

    return-object p0
.end method

.method public setPhoneAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasPhoneAlign:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object p0
.end method

.method public setStartTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStartTimeUsec:Z

    iput-wide p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->startTimeUsec_:J

    return-object p0
.end method

.method public setStateAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStateAlign:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object p0
.end method

.method public setWordAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasWordAlign:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getPartList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStartTimeUsec()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getStartTimeUsec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasEndTimeUsec()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getEndTimeUsec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasWordAlign()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getWordAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasPhoneAlign()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getPhoneAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getWordConfFeatureList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfFeature()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getHotwordConfFeature()Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordConfidence()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getHotwordConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasHotwordFired()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getHotwordFired()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->hasStateAlign()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getStateAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    return-void
.end method
