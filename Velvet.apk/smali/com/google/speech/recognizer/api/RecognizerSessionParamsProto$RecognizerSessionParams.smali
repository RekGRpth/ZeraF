.class public final Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerSessionParamsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognizerSessionParams"
.end annotation


# instance fields
.field private alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

.field private cachedSize:I

.field private channelCount_:I

.field private enableAlternates_:Z

.field private enablePartialResults_:Z

.field private hasAlternateParams:Z

.field private hasChannelCount:Z

.field private hasEnableAlternates:Z

.field private hasEnablePartialResults:Z

.field private hasHotwordDecisionThreshold:Z

.field private hasMaskOffensiveWords:Z

.field private hasNumNbest:Z

.field private hasResetIntervalMs:Z

.field private hasSampleRate:Z

.field private hasType:Z

.field private hotwordDecisionThreshold_:F

.field private maskOffensiveWords_:Z

.field private numNbest_:I

.field private resetIntervalMs_:I

.field private sampleRate_:F

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->type_:I

    const/high16 v0, 0x45fa0000

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->sampleRate_:F

    iput-boolean v2, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->maskOffensiveWords_:Z

    iput-boolean v1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->enableAlternates_:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

    iput v1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->numNbest_:I

    iput-boolean v2, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->enablePartialResults_:Z

    iput v1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->resetIntervalMs_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hotwordDecisionThreshold_:F

    iput v2, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->channelCount_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAlternateParams()Lcom/google/speech/common/Alternates$AlternateParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->cachedSize:I

    return v0
.end method

.method public getChannelCount()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->channelCount_:I

    return v0
.end method

.method public getEnableAlternates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->enableAlternates_:Z

    return v0
.end method

.method public getEnablePartialResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->enablePartialResults_:Z

    return v0
.end method

.method public getHotwordDecisionThreshold()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hotwordDecisionThreshold_:F

    return v0
.end method

.method public getMaskOffensiveWords()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->maskOffensiveWords_:Z

    return v0
.end method

.method public getNumNbest()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->numNbest_:I

    return v0
.end method

.method public getResetIntervalMs()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->resetIntervalMs_:I

    return v0
.end method

.method public getSampleRate()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->sampleRate_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasSampleRate()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getSampleRate()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasMaskOffensiveWords()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getMaskOffensiveWords()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnableAlternates()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getEnableAlternates()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasAlternateParams()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getAlternateParams()Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasNumNbest()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getNumNbest()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnablePartialResults()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getEnablePartialResults()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasResetIntervalMs()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getResetIntervalMs()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasHotwordDecisionThreshold()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getHotwordDecisionThreshold()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasChannelCount()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getChannelCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->cachedSize:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->type_:I

    return v0
.end method

.method public hasAlternateParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasAlternateParams:Z

    return v0
.end method

.method public hasChannelCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasChannelCount:Z

    return v0
.end method

.method public hasEnableAlternates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnableAlternates:Z

    return v0
.end method

.method public hasEnablePartialResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnablePartialResults:Z

    return v0
.end method

.method public hasHotwordDecisionThreshold()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasHotwordDecisionThreshold:Z

    return v0
.end method

.method public hasMaskOffensiveWords()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasMaskOffensiveWords:Z

    return v0
.end method

.method public hasNumNbest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasNumNbest:Z

    return v0
.end method

.method public hasResetIntervalMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasResetIntervalMs:Z

    return v0
.end method

.method public hasSampleRate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasSampleRate:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasType:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setType(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setSampleRate(F)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setMaskOffensiveWords(Z)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setEnableAlternates(Z)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/common/Alternates$AlternateParams;

    invoke-direct {v1}, Lcom/google/speech/common/Alternates$AlternateParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setAlternateParams(Lcom/google/speech/common/Alternates$AlternateParams;)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setNumNbest(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setEnablePartialResults(Z)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setResetIntervalMs(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setHotwordDecisionThreshold(F)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->setChannelCount(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4d -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public setAlternateParams(Lcom/google/speech/common/Alternates$AlternateParams;)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # Lcom/google/speech/common/Alternates$AlternateParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasAlternateParams:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->alternateParams_:Lcom/google/speech/common/Alternates$AlternateParams;

    return-object p0
.end method

.method public setChannelCount(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasChannelCount:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->channelCount_:I

    return-object p0
.end method

.method public setEnableAlternates(Z)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnableAlternates:Z

    iput-boolean p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->enableAlternates_:Z

    return-object p0
.end method

.method public setEnablePartialResults(Z)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnablePartialResults:Z

    iput-boolean p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->enablePartialResults_:Z

    return-object p0
.end method

.method public setHotwordDecisionThreshold(F)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasHotwordDecisionThreshold:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hotwordDecisionThreshold_:F

    return-object p0
.end method

.method public setMaskOffensiveWords(Z)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasMaskOffensiveWords:Z

    iput-boolean p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->maskOffensiveWords_:Z

    return-object p0
.end method

.method public setNumNbest(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasNumNbest:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->numNbest_:I

    return-object p0
.end method

.method public setResetIntervalMs(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasResetIntervalMs:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->resetIntervalMs_:I

    return-object p0
.end method

.method public setSampleRate(F)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasSampleRate:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->sampleRate_:F

    return-object p0
.end method

.method public setType(I)Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasType:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasSampleRate()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getSampleRate()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasMaskOffensiveWords()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getMaskOffensiveWords()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnableAlternates()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getEnableAlternates()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasAlternateParams()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getAlternateParams()Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasNumNbest()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getNumNbest()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasEnablePartialResults()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getEnablePartialResults()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasResetIntervalMs()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getResetIntervalMs()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasHotwordDecisionThreshold()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getHotwordDecisionThreshold()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->hasChannelCount()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;->getChannelCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    return-void
.end method
