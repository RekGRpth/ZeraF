.class public final Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GstaticConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/voicesearch/proto/GstaticConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServerInfo"
.end annotation


# instance fields
.field private blacklistMccMnc_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private connectionTimeoutMsec_:I

.field private hasConnectionTimeoutMsec:Z

.field private hasHeader:Z

.field private hasHost:Z

.field private hasInBufferSize:Z

.field private hasOpenConnectionTimeoutMsec:Z

.field private hasOutBufferSize:Z

.field private hasPort:Z

.field private hasUseSsl:Z

.field private header_:Ljava/lang/String;

.field private host_:Ljava/lang/String;

.field private inBufferSize_:I

.field private openConnectionTimeoutMsec_:I

.field private outBufferSize_:I

.field private port_:I

.field private useSsl_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->host_:Ljava/lang/String;

    iput v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->port_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->header_:Ljava/lang/String;

    iput v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->outBufferSize_:I

    iput v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->inBufferSize_:I

    iput v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->openConnectionTimeoutMsec_:I

    iput v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->connectionTimeoutMsec_:I

    iput-boolean v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->useSsl_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->blacklistMccMnc_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addBlacklistMccMnc(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->blacklistMccMnc_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->blacklistMccMnc_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->blacklistMccMnc_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBlacklistMccMncList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->blacklistMccMnc_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->cachedSize:I

    return v0
.end method

.method public getConnectionTimeoutMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->connectionTimeoutMsec_:I

    return v0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->header_:Ljava/lang/String;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->host_:Ljava/lang/String;

    return-object v0
.end method

.method public getInBufferSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->inBufferSize_:I

    return v0
.end method

.method public getOpenConnectionTimeoutMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->openConnectionTimeoutMsec_:I

    return v0
.end method

.method public getOutBufferSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->outBufferSize_:I

    return v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->port_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHost()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasPort()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getPort()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHeader()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getHeader()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOutBufferSize()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getOutBufferSize()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasInBufferSize()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getInBufferSize()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOpenConnectionTimeoutMsec()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getOpenConnectionTimeoutMsec()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasConnectionTimeoutMsec()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getConnectionTimeoutMsec()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasUseSsl()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getUseSsl()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getBlacklistMccMncList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_8
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getBlacklistMccMncList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->cachedSize:I

    return v3
.end method

.method public getUseSsl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->useSsl_:Z

    return v0
.end method

.method public hasConnectionTimeoutMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasConnectionTimeoutMsec:Z

    return v0
.end method

.method public hasHeader()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHeader:Z

    return v0
.end method

.method public hasHost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHost:Z

    return v0
.end method

.method public hasInBufferSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasInBufferSize:Z

    return v0
.end method

.method public hasOpenConnectionTimeoutMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOpenConnectionTimeoutMsec:Z

    return v0
.end method

.method public hasOutBufferSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOutBufferSize:Z

    return v0
.end method

.method public hasPort()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasPort:Z

    return v0
.end method

.method public hasUseSsl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasUseSsl:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setHost(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setPort(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setHeader(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setOutBufferSize(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setInBufferSize(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setOpenConnectionTimeoutMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setConnectionTimeoutMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->setUseSsl(Z)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->addBlacklistMccMnc(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public setConnectionTimeoutMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasConnectionTimeoutMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->connectionTimeoutMsec_:I

    return-object p0
.end method

.method public setHeader(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHeader:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->header_:Ljava/lang/String;

    return-object p0
.end method

.method public setHost(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHost:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->host_:Ljava/lang/String;

    return-object p0
.end method

.method public setInBufferSize(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasInBufferSize:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->inBufferSize_:I

    return-object p0
.end method

.method public setOpenConnectionTimeoutMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOpenConnectionTimeoutMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->openConnectionTimeoutMsec_:I

    return-object p0
.end method

.method public setOutBufferSize(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOutBufferSize:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->outBufferSize_:I

    return-object p0
.end method

.method public setPort(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasPort:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->port_:I

    return-object p0
.end method

.method public setUseSsl(Z)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasUseSsl:Z

    iput-boolean p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->useSsl_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHost()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasPort()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getPort()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasHeader()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getHeader()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOutBufferSize()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getOutBufferSize()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasInBufferSize()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getInBufferSize()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasOpenConnectionTimeoutMsec()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getOpenConnectionTimeoutMsec()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasConnectionTimeoutMsec()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getConnectionTimeoutMsec()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->hasUseSsl()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getUseSsl()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getBlacklistMccMncList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_0

    :cond_8
    return-void
.end method
