.class public final Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GstaticConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/voicesearch/proto/GstaticConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Configuration"
.end annotation


# instance fields
.field private actionFeatureFlags_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

.field private auth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

.field private bluetooth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

.field private cachedSize:I

.field private debug_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

.field private dictation_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

.field private embeddedRecognitionResources_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;",
            ">;"
        }
    .end annotation
.end field

.field private embeddedRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

.field private endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

.field private hasActionFeatureFlags:Z

.field private hasAuth:Z

.field private hasBluetooth:Z

.field private hasDebug:Z

.field private hasDictation:Z

.field private hasEmbeddedRecognizer:Z

.field private hasEndpointerParams:Z

.field private hasHelp:Z

.field private hasId:Z

.field private hasIntentApi:Z

.field private hasNetworkRecognizer:Z

.field private hasPairHttpServerInfo:Z

.field private hasPersonalization:Z

.field private hasPlatform:Z

.field private hasServiceApi:Z

.field private hasSingleHttpServerInfo:Z

.field private hasSoundSearch:Z

.field private hasTcpServerInfo:Z

.field private hasVoiceSearch:Z

.field private help_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

.field private id_:Ljava/lang/String;

.field private intentApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

.field private languages_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;",
            ">;"
        }
    .end annotation
.end field

.field private localizedResources_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;",
            ">;"
        }
    .end annotation
.end field

.field private networkRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

.field private pairHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

.field private personalization_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

.field private platform_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

.field private serviceApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

.field private singleHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

.field private soundSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

.field private tcpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

.field private voiceSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->id_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->languages_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->localizedResources_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->personalization_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognitionResources_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->intentApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->dictation_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->voiceSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->serviceApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->tcpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->pairHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->singleHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->help_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->networkRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->auth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->debug_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->bluetooth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->platform_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->actionFeatureFlags_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

    iput-object v1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->soundSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->cachedSize:I

    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    return-object v0
.end method


# virtual methods
.method public addEmbeddedRecognitionResources(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognitionResources_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognitionResources_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognitionResources_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addLanguages(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->languages_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->languages_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->languages_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addLocalizedResources(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->localizedResources_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->localizedResources_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->localizedResources_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPairHttpServerInfo:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->pairHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    return-object p0
.end method

.method public clearSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSingleHttpServerInfo:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->singleHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    return-object p0
.end method

.method public clearTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasTcpServerInfo:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->tcpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    return-object p0
.end method

.method public getActionFeatureFlags()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->actionFeatureFlags_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

    return-object v0
.end method

.method public getAuth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->auth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    return-object v0
.end method

.method public getBluetooth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->bluetooth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->cachedSize:I

    return v0
.end method

.method public getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->debug_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    return-object v0
.end method

.method public getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->dictation_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    return-object v0
.end method

.method public getEmbeddedRecognitionResourcesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognitionResources_:Ljava/util/List;

    return-object v0
.end method

.method public getEmbeddedRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    return-object v0
.end method

.method public getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    return-object v0
.end method

.method public getHelp()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->help_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getIntentApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->intentApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    return-object v0
.end method

.method public getLanguages(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->languages_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;

    return-object v0
.end method

.method public getLanguagesCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->languages_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getLanguagesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->languages_:Ljava/util/List;

    return-object v0
.end method

.method public getLocalizedResourcesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->localizedResources_:Ljava/util/List;

    return-object v0
.end method

.method public getNetworkRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->networkRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    return-object v0
.end method

.method public getPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->pairHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    return-object v0
.end method

.method public getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->personalization_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    return-object v0
.end method

.method public getPlatform()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->platform_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasId()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getLanguagesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getLocalizedResourcesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPersonalization()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognitionResourcesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasIntentApi()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getIntentApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDictation()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasTcpServerInfo()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasHelp()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getHelp()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEndpointerParams()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasNetworkRecognizer()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getNetworkRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasVoiceSearch()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getVoiceSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasServiceApi()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getServiceApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasAuth()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getAuth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPairHttpServerInfo()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasBluetooth()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getBluetooth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPlatform()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPlatform()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSingleHttpServerInfo()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEmbeddedRecognizer()Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_13
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasActionFeatureFlags()Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v3, 0x16

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getActionFeatureFlags()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_14
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch()Z

    move-result v3

    if-eqz v3, :cond_15

    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_15
    iput v2, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->cachedSize:I

    return v2
.end method

.method public getServiceApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->serviceApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    return-object v0
.end method

.method public getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->singleHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    return-object v0
.end method

.method public getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->soundSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    return-object v0
.end method

.method public getTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->tcpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    return-object v0
.end method

.method public getVoiceSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->voiceSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    return-object v0
.end method

.method public hasActionFeatureFlags()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasActionFeatureFlags:Z

    return v0
.end method

.method public hasAuth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasAuth:Z

    return v0
.end method

.method public hasBluetooth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasBluetooth:Z

    return v0
.end method

.method public hasDebug()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug:Z

    return v0
.end method

.method public hasDictation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDictation:Z

    return v0
.end method

.method public hasEmbeddedRecognizer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEmbeddedRecognizer:Z

    return v0
.end method

.method public hasEndpointerParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEndpointerParams:Z

    return v0
.end method

.method public hasHelp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasHelp:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasId:Z

    return v0
.end method

.method public hasIntentApi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasIntentApi:Z

    return v0
.end method

.method public hasNetworkRecognizer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasNetworkRecognizer:Z

    return v0
.end method

.method public hasPairHttpServerInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPairHttpServerInfo:Z

    return v0
.end method

.method public hasPersonalization()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPersonalization:Z

    return v0
.end method

.method public hasPlatform()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPlatform:Z

    return v0
.end method

.method public hasServiceApi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasServiceApi:Z

    return v0
.end method

.method public hasSingleHttpServerInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSingleHttpServerInfo:Z

    return v0
.end method

.method public hasSoundSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch:Z

    return v0
.end method

.method public hasTcpServerInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasTcpServerInfo:Z

    return v0
.end method

.method public hasVoiceSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasVoiceSearch:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setId(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->addLanguages(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->addLocalizedResources(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setPersonalization(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->addEmbeddedRecognitionResources(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setIntentApi(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setDictation(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setTcpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setHelp(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setEndpointerParams(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setNetworkRecognizer(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setVoiceSearch(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setServiceApi(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setAuth(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setPairHttpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setDebug(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setBluetooth(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_12
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setPlatform(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_13
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setSingleHttpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_14
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setEmbeddedRecognizer(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_15
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setActionFeatureFlags(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setSoundSearch(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
    .end sparse-switch
.end method

.method public setActionFeatureFlags(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasActionFeatureFlags:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->actionFeatureFlags_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

    return-object p0
.end method

.method public setAuth(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasAuth:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->auth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    return-object p0
.end method

.method public setBluetooth(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasBluetooth:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->bluetooth_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

    return-object p0
.end method

.method public setDebug(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->debug_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    return-object p0
.end method

.method public setDictation(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDictation:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->dictation_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    return-object p0
.end method

.method public setEmbeddedRecognizer(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEmbeddedRecognizer:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->embeddedRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    return-object p0
.end method

.method public setEndpointerParams(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEndpointerParams:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    return-object p0
.end method

.method public setHelp(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasHelp:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->help_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasId:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setIntentApi(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasIntentApi:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->intentApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    return-object p0
.end method

.method public setNetworkRecognizer(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasNetworkRecognizer:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->networkRecognizer_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    return-object p0
.end method

.method public setPairHttpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPairHttpServerInfo:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->pairHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    return-object p0
.end method

.method public setPersonalization(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPersonalization:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->personalization_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    return-object p0
.end method

.method public setPlatform(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPlatform:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->platform_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    return-object p0
.end method

.method public setServiceApi(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasServiceApi:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->serviceApi_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    return-object p0
.end method

.method public setSingleHttpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSingleHttpServerInfo:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->singleHttpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    return-object p0
.end method

.method public setSoundSearch(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->soundSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    return-object p0
.end method

.method public setTcpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasTcpServerInfo:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->tcpServerInfo_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    return-object p0
.end method

.method public setVoiceSearch(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasVoiceSearch:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->voiceSearch_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getLanguagesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Language;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getLocalizedResourcesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LocalizedResources;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPersonalization()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognitionResourcesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasIntentApi()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getIntentApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDictation()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasTcpServerInfo()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasHelp()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getHelp()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEndpointerParams()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasNetworkRecognizer()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getNetworkRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasVoiceSearch()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getVoiceSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasServiceApi()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getServiceApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasAuth()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getAuth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPairHttpServerInfo()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasBluetooth()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getBluetooth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Bluetooth;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasPlatform()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPlatform()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSingleHttpServerInfo()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEmbeddedRecognizer()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasActionFeatureFlags()Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getActionFeatureFlags()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ActionFeatureFlags;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch()Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_15
    return-void
.end method
