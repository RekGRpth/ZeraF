.class public final Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActivityStats"
.end annotation


# instance fields
.field private activityIcon_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private activity_:Ljava/lang/String;

.field private cachedSize:I

.field private hasActivity:Z

.field private hasActivityIcon:Z

.field private stats_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->activity_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->activityIcon_:Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->stats_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addStats(Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->stats_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->stats_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->stats_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getActivity()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->activity_:Ljava/lang/String;

    return-object v0
.end method

.method public getActivityIcon()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->activityIcon_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivity()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivity()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivityIcon()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivityIcon()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStatsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->cachedSize:I

    return v2
.end method

.method public getStats(I)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->stats_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    return-object v0
.end method

.method public getStatsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->stats_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getStatsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->stats_:Ljava/util/List;

    return-object v0
.end method

.method public hasActivity()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivity:Z

    return v0
.end method

.method public hasActivityIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivityIcon:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->setActivity(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->setActivityIcon(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->addStats(Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;

    move-result-object v0

    return-object v0
.end method

.method public setActivity(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivity:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->activity_:Ljava/lang/String;

    return-object p0
.end method

.method public setActivityIcon(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivityIcon:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->activityIcon_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivity()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivityIcon()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivityIcon()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStatsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    return-void
.end method
