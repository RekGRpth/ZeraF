.class public final Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Extra"
.end annotation


# instance fields
.field private boolValue_:Z

.field private cachedSize:I

.field private hasBoolValue:Z

.field private hasKey:Z

.field private hasLongValue:Z

.field private hasStringValue:Z

.field private key_:Ljava/lang/String;

.field private longValue_:J

.field private stringValue_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->key_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->stringValue_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->longValue_:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->boolValue_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBoolValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->boolValue_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->cachedSize:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->key_:Ljava/lang/String;

    return-object v0
.end method

.method public getLongValue()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->longValue_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasKey()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasStringValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getStringValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasLongValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getLongValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasBoolValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getBoolValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->cachedSize:I

    return v0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->stringValue_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBoolValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasBoolValue:Z

    return v0
.end method

.method public hasKey()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasKey:Z

    return v0
.end method

.method public hasLongValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasLongValue:Z

    return v0
.end method

.method public hasStringValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasStringValue:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setKey(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setStringValue(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setLongValue(J)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setBoolValue(Z)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    move-result-object v0

    return-object v0
.end method

.method public setBoolValue(Z)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasBoolValue:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->boolValue_:Z

    return-object p0
.end method

.method public setKey(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasKey:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->key_:Ljava/lang/String;

    return-object p0
.end method

.method public setLongValue(J)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasLongValue:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->longValue_:J

    return-object p0
.end method

.method public setStringValue(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasStringValue:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->stringValue_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasStringValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getStringValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasLongValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getLongValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasBoolValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getBoolValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    return-void
.end method
