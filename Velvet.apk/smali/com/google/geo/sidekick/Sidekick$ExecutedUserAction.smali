.class public final Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExecutedUserAction"
.end annotation


# instance fields
.field private action_:Lcom/google/geo/sidekick/Sidekick$Action;

.field private cachedSize:I

.field private clickTarget_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

.field private customPlace_:Lcom/google/geo/sidekick/Sidekick$PlaceData;

.field private editedPlaceLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

.field private encrypted_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private executionTimeMs_:J

.field private hasAction:Z

.field private hasClickTarget:Z

.field private hasCustomPlace:Z

.field private hasEditedPlaceLocation:Z

.field private hasEncrypted:Z

.field private hasEntry:Z

.field private hasExecutionTimeMs:Z

.field private hasTargetDisplay:Z

.field private hasTimestampSeconds:Z

.field private hasTimezoneOffsetSeconds:Z

.field private inputEntry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private targetDisplay_:I

.field private timestampSeconds_:J

.field private timezoneOffsetSeconds_:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->action_:Lcom/google/geo/sidekick/Sidekick$Action;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->clickTarget_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    iput-wide v2, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->timestampSeconds_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->timezoneOffsetSeconds_:I

    iput-wide v2, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->executionTimeMs_:J

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->targetDisplay_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->inputEntry_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->customPlace_:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->editedPlaceLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->encrypted_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addInputEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->inputEntry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->inputEntry_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->inputEntry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAction()Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->action_:Lcom/google/geo/sidekick/Sidekick$Action;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->cachedSize:I

    return v0
.end method

.method public getClickTarget()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->clickTarget_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    return-object v0
.end method

.method public getCustomPlace()Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->customPlace_:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    return-object v0
.end method

.method public getEditedPlaceLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->editedPlaceLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getEncrypted()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->encrypted_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method public getExecutionTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->executionTimeMs_:J

    return-wide v0
.end method

.method public getInputEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->inputEntry_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasAction()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEntry()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimestampSeconds()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getTimestampSeconds()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTargetDisplay()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getTargetDisplay()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getInputEntryList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasCustomPlace()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getCustomPlace()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEditedPlaceLocation()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getEditedPlaceLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEncrypted()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getEncrypted()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasClickTarget()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getClickTarget()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasExecutionTimeMs()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getExecutionTimeMs()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimezoneOffsetSeconds()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getTimezoneOffsetSeconds()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->cachedSize:I

    return v2
.end method

.method public getTargetDisplay()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->targetDisplay_:I

    return v0
.end method

.method public getTimestampSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->timestampSeconds_:J

    return-wide v0
.end method

.method public getTimezoneOffsetSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->timezoneOffsetSeconds_:I

    return v0
.end method

.method public hasAction()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasAction:Z

    return v0
.end method

.method public hasClickTarget()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasClickTarget:Z

    return v0
.end method

.method public hasCustomPlace()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasCustomPlace:Z

    return v0
.end method

.method public hasEditedPlaceLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEditedPlaceLocation:Z

    return v0
.end method

.method public hasEncrypted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEncrypted:Z

    return v0
.end method

.method public hasEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEntry:Z

    return v0
.end method

.method public hasExecutionTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasExecutionTimeMs:Z

    return v0
.end method

.method public hasTargetDisplay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTargetDisplay:Z

    return v0
.end method

.method public hasTimestampSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimestampSeconds:Z

    return v0
.end method

.method public hasTimezoneOffsetSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimezoneOffsetSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Action;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setAction(Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->addInputEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setCustomPlace(Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setEditedPlaceLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setEncrypted(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setClickTarget(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setExecutionTimeMs(J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v0

    return-object v0
.end method

.method public setAction(Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasAction:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->action_:Lcom/google/geo/sidekick/Sidekick$Action;

    return-object p0
.end method

.method public setClickTarget(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasClickTarget:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->clickTarget_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    return-object p0
.end method

.method public setCustomPlace(Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasCustomPlace:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->customPlace_:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    return-object p0
.end method

.method public setEditedPlaceLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEditedPlaceLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->editedPlaceLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public setEncrypted(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEncrypted:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->encrypted_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object p0
.end method

.method public setExecutionTimeMs(J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasExecutionTimeMs:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->executionTimeMs_:J

    return-object p0
.end method

.method public setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTargetDisplay:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->targetDisplay_:I

    return-object p0
.end method

.method public setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimestampSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->timestampSeconds_:J

    return-object p0
.end method

.method public setTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimezoneOffsetSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->timezoneOffsetSeconds_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasAction()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEntry()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimestampSeconds()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getTimestampSeconds()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTargetDisplay()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getTargetDisplay()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getInputEntryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasCustomPlace()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getCustomPlace()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEditedPlaceLocation()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getEditedPlaceLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasEncrypted()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getEncrypted()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasClickTarget()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getClickTarget()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasExecutionTimeMs()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getExecutionTimeMs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->hasTimezoneOffsetSeconds()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->getTimezoneOffsetSeconds()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    return-void
.end method
