.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Places"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasNotificationPolicy:Z

.field private hasNotificationSetting:Z

.field private hasShowAll:Z

.field private hasShowAttractions:Z

.field private hasShowInterestingWhenTraveling:Z

.field private hasShowMuseumsAndArt:Z

.field private hasShowRecommendedPlaces:Z

.field private hasShowRestaurants:Z

.field private hasShowShopping:Z

.field private notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

.field private notificationSetting_:I

.field private showAll_:Z

.field private showAttractions_:Z

.field private showInterestingWhenTraveling_:Z

.field private showMuseumsAndArt_:Z

.field private showRecommendedPlaces_:Z

.field private showRestaurants_:Z

.field private showShopping_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->notificationSetting_:I

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showAll_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showRestaurants_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showAttractions_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showRecommendedPlaces_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showInterestingWhenTraveling_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showMuseumsAndArt_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showShopping_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->cachedSize:I

    return v0
.end method

.method public getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object v0
.end method

.method public getNotificationSetting()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->notificationSetting_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationPolicy()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRestaurants()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowRestaurants()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAttractions()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowAttractions()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRecommendedPlaces()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowRecommendedPlaces()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowInterestingWhenTraveling()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowInterestingWhenTraveling()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAll()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowAll()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationSetting()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getNotificationSetting()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowMuseumsAndArt()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowMuseumsAndArt()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowShopping()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowShopping()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->cachedSize:I

    return v0
.end method

.method public getShowAll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showAll_:Z

    return v0
.end method

.method public getShowAttractions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showAttractions_:Z

    return v0
.end method

.method public getShowInterestingWhenTraveling()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showInterestingWhenTraveling_:Z

    return v0
.end method

.method public getShowMuseumsAndArt()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showMuseumsAndArt_:Z

    return v0
.end method

.method public getShowRecommendedPlaces()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showRecommendedPlaces_:Z

    return v0
.end method

.method public getShowRestaurants()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showRestaurants_:Z

    return v0
.end method

.method public getShowShopping()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showShopping_:Z

    return v0
.end method

.method public hasNotificationPolicy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationPolicy:Z

    return v0
.end method

.method public hasNotificationSetting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationSetting:Z

    return v0
.end method

.method public hasShowAll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAll:Z

    return v0
.end method

.method public hasShowAttractions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAttractions:Z

    return v0
.end method

.method public hasShowInterestingWhenTraveling()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowInterestingWhenTraveling:Z

    return v0
.end method

.method public hasShowMuseumsAndArt()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowMuseumsAndArt:Z

    return v0
.end method

.method public hasShowRecommendedPlaces()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRecommendedPlaces:Z

    return v0
.end method

.method public hasShowRestaurants()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRestaurants:Z

    return v0
.end method

.method public hasShowShopping()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowShopping:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setShowRestaurants(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setShowAttractions(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setShowRecommendedPlaces(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setShowInterestingWhenTraveling(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setShowAll(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setNotificationSetting(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setShowMuseumsAndArt(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->setShowShopping(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    move-result-object v0

    return-object v0
.end method

.method public setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationPolicy:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object p0
.end method

.method public setNotificationSetting(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationSetting:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->notificationSetting_:I

    return-object p0
.end method

.method public setShowAll(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAll:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showAll_:Z

    return-object p0
.end method

.method public setShowAttractions(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAttractions:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showAttractions_:Z

    return-object p0
.end method

.method public setShowInterestingWhenTraveling(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowInterestingWhenTraveling:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showInterestingWhenTraveling_:Z

    return-object p0
.end method

.method public setShowMuseumsAndArt(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowMuseumsAndArt:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showMuseumsAndArt_:Z

    return-object p0
.end method

.method public setShowRecommendedPlaces(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRecommendedPlaces:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showRecommendedPlaces_:Z

    return-object p0
.end method

.method public setShowRestaurants(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRestaurants:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showRestaurants_:Z

    return-object p0
.end method

.method public setShowShopping(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowShopping:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->showShopping_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationPolicy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRestaurants()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowRestaurants()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAttractions()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowAttractions()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowRecommendedPlaces()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowRecommendedPlaces()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowInterestingWhenTraveling()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowInterestingWhenTraveling()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowAll()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowAll()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasNotificationSetting()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getNotificationSetting()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowMuseumsAndArt()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowMuseumsAndArt()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->hasShowShopping()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;->getShowShopping()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    return-void
.end method
