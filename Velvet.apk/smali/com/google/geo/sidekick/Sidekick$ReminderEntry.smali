.class public final Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReminderEntry"
.end annotation


# instance fields
.field private cachedSize:I

.field private dayPart_:I

.field private hasDayPart:Z

.field private hasReminderMessage:Z

.field private hasResolution:Z

.field private hasTaskId:Z

.field private hasTriggerTimeSeconds:Z

.field private hasTriggeringMessage:Z

.field private reminderMessage_:Ljava/lang/String;

.field private resolution_:I

.field private taskId_:Ljava/lang/String;

.field private triggerTimeSeconds_:J

.field private triggeringMessage_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->taskId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->reminderMessage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->triggeringMessage_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->triggerTimeSeconds_:J

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->resolution_:I

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->dayPart_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->cachedSize:I

    return v0
.end method

.method public getDayPart()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->dayPart_:I

    return v0
.end method

.method public getReminderMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->reminderMessage_:Ljava/lang/String;

    return-object v0
.end method

.method public getResolution()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->resolution_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTaskId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTaskId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasReminderMessage()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggeringMessage()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTriggeringMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTriggerTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasResolution()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getResolution()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasDayPart()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getDayPart()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->cachedSize:I

    return v0
.end method

.method public getTaskId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->taskId_:Ljava/lang/String;

    return-object v0
.end method

.method public getTriggerTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->triggerTimeSeconds_:J

    return-wide v0
.end method

.method public getTriggeringMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->triggeringMessage_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDayPart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasDayPart:Z

    return v0
.end method

.method public hasReminderMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasReminderMessage:Z

    return v0
.end method

.method public hasResolution()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasResolution:Z

    return v0
.end method

.method public hasTaskId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTaskId:Z

    return v0
.end method

.method public hasTriggerTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds:Z

    return v0
.end method

.method public hasTriggeringMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggeringMessage:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->setTaskId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->setReminderMessage(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->setTriggeringMessage(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->setTriggerTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->setResolution(I)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->setDayPart(I)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v0

    return-object v0
.end method

.method public setDayPart(I)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasDayPart:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->dayPart_:I

    return-object p0
.end method

.method public setReminderMessage(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasReminderMessage:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->reminderMessage_:Ljava/lang/String;

    return-object p0
.end method

.method public setResolution(I)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasResolution:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->resolution_:I

    return-object p0
.end method

.method public setTaskId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTaskId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->taskId_:Ljava/lang/String;

    return-object p0
.end method

.method public setTriggerTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->triggerTimeSeconds_:J

    return-object p0
.end method

.method public setTriggeringMessage(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggeringMessage:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->triggeringMessage_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTaskId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTaskId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasReminderMessage()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggeringMessage()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTriggeringMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTriggerTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasResolution()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getResolution()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasDayPart()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getDayPart()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    return-void
.end method
