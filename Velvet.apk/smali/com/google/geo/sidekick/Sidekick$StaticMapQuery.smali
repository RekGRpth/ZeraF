.class public final Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StaticMapQuery"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasHeight:Z

.field private hasPlaceEntry:Z

.field private hasStartLocation:Z

.field private hasWidth:Z

.field private height_:I

.field private placeEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

.field private startLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

.field private width_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->placeEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->width_:I

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->height_:I

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->startLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->cachedSize:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->height_:I

    return v0
.end method

.method public getPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->placeEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasPlaceEntry()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasWidth()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getWidth()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasHeight()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasStartLocation()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getStartLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->cachedSize:I

    return v0
.end method

.method public getStartLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->startLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->width_:I

    return v0
.end method

.method public hasHeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasHeight:Z

    return v0
.end method

.method public hasPlaceEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasPlaceEntry:Z

    return v0
.end method

.method public hasStartLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasStartLocation:Z

    return v0
.end method

.method public hasWidth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasWidth:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setWidth(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setHeight(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setStartLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v0

    return-object v0
.end method

.method public setHeight(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasHeight:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->height_:I

    return-object p0
.end method

.method public setPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasPlaceEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->placeEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object p0
.end method

.method public setStartLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasStartLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->startLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public setWidth(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasWidth:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->width_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasPlaceEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getWidth()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->hasStartLocation()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->getStartLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    return-void
.end method
