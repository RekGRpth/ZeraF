.class public final Lcom/google/geo/sidekick/Sidekick$Configuration;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Configuration"
.end annotation


# instance fields
.field private cachedSize:I

.field private completelyOptOutOfSidekick_:Z

.field private dasherConfiguration_:Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

.field private hasCompletelyOptOutOfSidekick:Z

.field private hasDasherConfiguration:Z

.field private hasLocaleConfiguration:Z

.field private hasOptInToSidekick:Z

.field private hasPlacevaultConfiguration:Z

.field private hasShowCardsOnBrowserEndpoints:Z

.field private hasSidekickConfiguration:Z

.field private hasWebsearchConfiguration:Z

.field private localeConfiguration_:Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

.field private optInToSidekick_:Z

.field private placevaultConfiguration_:Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

.field private showCardsOnBrowserEndpoints_:Z

.field private sidekickConfiguration_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

.field private websearchConfiguration_:Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->optInToSidekick_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->completelyOptOutOfSidekick_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->showCardsOnBrowserEndpoints_:Z

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->sidekickConfiguration_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->placevaultConfiguration_:Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->websearchConfiguration_:Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->dasherConfiguration_:Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->localeConfiguration_:Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->cachedSize:I

    return v0
.end method

.method public getCompletelyOptOutOfSidekick()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->completelyOptOutOfSidekick_:Z

    return v0
.end method

.method public getDasherConfiguration()Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->dasherConfiguration_:Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    return-object v0
.end method

.method public getLocaleConfiguration()Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->localeConfiguration_:Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    return-object v0
.end method

.method public getOptInToSidekick()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->optInToSidekick_:Z

    return v0
.end method

.method public getPlacevaultConfiguration()Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->placevaultConfiguration_:Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasSidekickConfiguration()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasPlacevaultConfiguration()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getPlacevaultConfiguration()Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasWebsearchConfiguration()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getWebsearchConfiguration()Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasDasherConfiguration()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getDasherConfiguration()Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasLocaleConfiguration()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getLocaleConfiguration()Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasCompletelyOptOutOfSidekick()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getCompletelyOptOutOfSidekick()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasOptInToSidekick()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getOptInToSidekick()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasShowCardsOnBrowserEndpoints()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getShowCardsOnBrowserEndpoints()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->cachedSize:I

    return v0
.end method

.method public getShowCardsOnBrowserEndpoints()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->showCardsOnBrowserEndpoints_:Z

    return v0
.end method

.method public getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->sidekickConfiguration_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object v0
.end method

.method public getWebsearchConfiguration()Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->websearchConfiguration_:Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;

    return-object v0
.end method

.method public hasCompletelyOptOutOfSidekick()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasCompletelyOptOutOfSidekick:Z

    return v0
.end method

.method public hasDasherConfiguration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasDasherConfiguration:Z

    return v0
.end method

.method public hasLocaleConfiguration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasLocaleConfiguration:Z

    return v0
.end method

.method public hasOptInToSidekick()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasOptInToSidekick:Z

    return v0
.end method

.method public hasPlacevaultConfiguration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasPlacevaultConfiguration:Z

    return v0
.end method

.method public hasShowCardsOnBrowserEndpoints()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasShowCardsOnBrowserEndpoints:Z

    return v0
.end method

.method public hasSidekickConfiguration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasSidekickConfiguration:Z

    return v0
.end method

.method public hasWebsearchConfiguration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasWebsearchConfiguration:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setSidekickConfiguration(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setPlacevaultConfiguration(Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setWebsearchConfiguration(Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setDasherConfiguration(Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setLocaleConfiguration(Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setCompletelyOptOutOfSidekick(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setOptInToSidekick(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setShowCardsOnBrowserEndpoints(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public setCompletelyOptOutOfSidekick(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasCompletelyOptOutOfSidekick:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->completelyOptOutOfSidekick_:Z

    return-object p0
.end method

.method public setDasherConfiguration(Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasDasherConfiguration:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->dasherConfiguration_:Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    return-object p0
.end method

.method public setLocaleConfiguration(Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasLocaleConfiguration:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->localeConfiguration_:Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    return-object p0
.end method

.method public setOptInToSidekick(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasOptInToSidekick:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->optInToSidekick_:Z

    return-object p0
.end method

.method public setPlacevaultConfiguration(Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasPlacevaultConfiguration:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->placevaultConfiguration_:Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    return-object p0
.end method

.method public setShowCardsOnBrowserEndpoints(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasShowCardsOnBrowserEndpoints:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->showCardsOnBrowserEndpoints_:Z

    return-object p0
.end method

.method public setSidekickConfiguration(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasSidekickConfiguration:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->sidekickConfiguration_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object p0
.end method

.method public setWebsearchConfiguration(Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasWebsearchConfiguration:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Configuration;->websearchConfiguration_:Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasSidekickConfiguration()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasPlacevaultConfiguration()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getPlacevaultConfiguration()Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasWebsearchConfiguration()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getWebsearchConfiguration()Lcom/google/geo/sidekick/Sidekick$WebsearchConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasDasherConfiguration()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getDasherConfiguration()Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasLocaleConfiguration()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getLocaleConfiguration()Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasCompletelyOptOutOfSidekick()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getCompletelyOptOutOfSidekick()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasOptInToSidekick()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getOptInToSidekick()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasShowCardsOnBrowserEndpoints()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getShowCardsOnBrowserEndpoints()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    return-void
.end method
