.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PublicAlerts"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasNotificationPolicy:Z

.field private hasNotificationSetting:Z

.field private hasShowWhenAlertPosted:Z

.field private notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

.field private notificationSetting_:I

.field private showWhenAlertPosted_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->notificationSetting_:I

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->showWhenAlertPosted_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->cachedSize:I

    return v0
.end method

.method public getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object v0
.end method

.method public getNotificationSetting()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->notificationSetting_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationPolicy()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasShowWhenAlertPosted()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->getShowWhenAlertPosted()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationSetting()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->getNotificationSetting()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->cachedSize:I

    return v0
.end method

.method public getShowWhenAlertPosted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->showWhenAlertPosted_:Z

    return v0
.end method

.method public hasNotificationPolicy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationPolicy:Z

    return v0
.end method

.method public hasNotificationSetting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationSetting:Z

    return v0
.end method

.method public hasShowWhenAlertPosted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasShowWhenAlertPosted:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->setShowWhenAlertPosted(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->setNotificationSetting(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    move-result-object v0

    return-object v0
.end method

.method public setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationPolicy:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object p0
.end method

.method public setNotificationSetting(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationSetting:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->notificationSetting_:I

    return-object p0
.end method

.method public setShowWhenAlertPosted(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasShowWhenAlertPosted:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->showWhenAlertPosted_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationPolicy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasShowWhenAlertPosted()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->getShowWhenAlertPosted()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->hasNotificationSetting()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;->getNotificationSetting()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    return-void
.end method
