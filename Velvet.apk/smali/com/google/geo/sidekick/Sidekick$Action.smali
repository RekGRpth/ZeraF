.class public final Lcom/google/geo/sidekick/Sidekick$Action;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# instance fields
.field private cachedSize:I

.field private displayMessage_:Ljava/lang/String;

.field private encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private hasDisplayMessage:Z

.field private hasEncodedServerPayload:Z

.field private hasInterest:Z

.field private hasInterestedItemIndex:Z

.field private hasRecordActionIfTaken:Z

.field private hasType:Z

.field private interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

.field private interestedItemIndex_:I

.field private recordActionIfTaken_:Z

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->displayMessage_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->recordActionIfTaken_:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->interestedItemIndex_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Action;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/geo/sidekick/Sidekick$Action;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    return-object v0
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->cachedSize:I

    return v0
.end method

.method public getDisplayMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->displayMessage_:Ljava/lang/String;

    return-object v0
.end method

.method public getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

    return-object v0
.end method

.method public getInterestedItemIndex()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->interestedItemIndex_:I

    return v0
.end method

.method public getRecordActionIfTaken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->recordActionIfTaken_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasDisplayMessage()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getDisplayMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasRecordActionIfTaken()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getRecordActionIfTaken()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterest()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasEncodedServerPayload()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterestedItemIndex()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getInterestedItemIndex()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->cachedSize:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->type_:I

    return v0
.end method

.method public hasDisplayMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasDisplayMessage:Z

    return v0
.end method

.method public hasEncodedServerPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasEncodedServerPayload:Z

    return v0
.end method

.method public hasInterest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterest:Z

    return v0
.end method

.method public hasInterestedItemIndex()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterestedItemIndex:Z

    return v0
.end method

.method public hasRecordActionIfTaken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasRecordActionIfTaken:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$Action;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Action;->setType(I)Lcom/google/geo/sidekick/Sidekick$Action;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Action;->setDisplayMessage(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Action;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Action;->setRecordActionIfTaken(Z)Lcom/google/geo/sidekick/Sidekick$Action;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Action;->setInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$Action;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Action;->setEncodedServerPayload(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$Action;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Action;->setInterestedItemIndex(I)Lcom/google/geo/sidekick/Sidekick$Action;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$Action;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public setDisplayMessage(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasDisplayMessage:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->displayMessage_:Ljava/lang/String;

    return-object p0
.end method

.method public setEncodedServerPayload(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasEncodedServerPayload:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterest:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

    return-object p0
.end method

.method public setInterestedItemIndex(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterestedItemIndex:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->interestedItemIndex_:I

    return-object p0
.end method

.method public setRecordActionIfTaken(Z)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasRecordActionIfTaken:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->recordActionIfTaken_:Z

    return-object p0
.end method

.method public setType(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Action;->hasType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Action;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasDisplayMessage()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getDisplayMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasRecordActionIfTaken()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getRecordActionIfTaken()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterest()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasEncodedServerPayload()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->hasInterestedItemIndex()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Action;->getInterestedItemIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    return-void
.end method
