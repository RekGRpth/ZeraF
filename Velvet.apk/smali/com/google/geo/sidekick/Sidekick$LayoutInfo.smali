.class public final Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LayoutInfo"
.end annotation


# instance fields
.field private cachedSize:I

.field private cardWidthPixelsLandscape_:I

.field private cardWidthPixelsPortrait_:I

.field private hasCardWidthPixelsLandscape:Z

.field private hasCardWidthPixelsPortrait:Z

.field private hasScreenDensity:Z

.field private hasScreenPixelsLongestEdge:Z

.field private hasScreenPixelsShortestEdge:Z

.field private screenDensity_:F

.field private screenPixelsLongestEdge_:I

.field private screenPixelsShortestEdge_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenPixelsLongestEdge_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenPixelsShortestEdge_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenDensity_:F

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cardWidthPixelsPortrait_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cardWidthPixelsLandscape_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cachedSize:I

    return v0
.end method

.method public getCardWidthPixelsLandscape()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cardWidthPixelsLandscape_:I

    return v0
.end method

.method public getCardWidthPixelsPortrait()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cardWidthPixelsPortrait_:I

    return v0
.end method

.method public getScreenDensity()F
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenDensity_:F

    return v0
.end method

.method public getScreenPixelsLongestEdge()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenPixelsLongestEdge_:I

    return v0
.end method

.method public getScreenPixelsShortestEdge()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenPixelsShortestEdge_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsLongestEdge()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getScreenPixelsLongestEdge()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsShortestEdge()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getScreenPixelsShortestEdge()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenDensity()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getScreenDensity()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsPortrait()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getCardWidthPixelsPortrait()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsLandscape()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getCardWidthPixelsLandscape()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cachedSize:I

    return v0
.end method

.method public hasCardWidthPixelsLandscape()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsLandscape:Z

    return v0
.end method

.method public hasCardWidthPixelsPortrait()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsPortrait:Z

    return v0
.end method

.method public hasScreenDensity()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenDensity:Z

    return v0
.end method

.method public hasScreenPixelsLongestEdge()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsLongestEdge:Z

    return v0
.end method

.method public hasScreenPixelsShortestEdge()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsShortestEdge:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setScreenPixelsLongestEdge(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setScreenPixelsShortestEdge(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setScreenDensity(F)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setCardWidthPixelsPortrait(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setCardWidthPixelsLandscape(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v0

    return-object v0
.end method

.method public setCardWidthPixelsLandscape(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsLandscape:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cardWidthPixelsLandscape_:I

    return-object p0
.end method

.method public setCardWidthPixelsPortrait(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsPortrait:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->cardWidthPixelsPortrait_:I

    return-object p0
.end method

.method public setScreenDensity(F)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenDensity:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenDensity_:F

    return-object p0
.end method

.method public setScreenPixelsLongestEdge(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsLongestEdge:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenPixelsLongestEdge_:I

    return-object p0
.end method

.method public setScreenPixelsShortestEdge(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsShortestEdge:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->screenPixelsShortestEdge_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsLongestEdge()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getScreenPixelsLongestEdge()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenPixelsShortestEdge()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getScreenPixelsShortestEdge()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasScreenDensity()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getScreenDensity()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsPortrait()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getCardWidthPixelsPortrait()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->hasCardWidthPixelsLandscape()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->getCardWidthPixelsLandscape()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
