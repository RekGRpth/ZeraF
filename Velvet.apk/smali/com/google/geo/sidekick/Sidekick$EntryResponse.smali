.class public final Lcom/google/geo/sidekick/Sidekick$EntryResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EntryResponse"
.end annotation


# instance fields
.field private backgroundImageDescriptor_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private backgroundImage_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Photo;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private configuration_:Lcom/google/geo/sidekick/Sidekick$Configuration;

.field private entryTree_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$EntryTree;",
            ">;"
        }
    .end annotation
.end field

.field private hasConfiguration:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->entryTree_:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->configuration_:Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImage_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImageDescriptor_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addBackgroundImage(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImage_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImage_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImage_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addBackgroundImageDescriptor(Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImageDescriptor_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImageDescriptor_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImageDescriptor_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addEntryTree(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTree;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->entryTree_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->entryTree_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->entryTree_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBackgroundImage(I)Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImage_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getBackgroundImageCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImage_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBackgroundImageDescriptor(I)Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImageDescriptor_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    return-object v0
.end method

.method public getBackgroundImageDescriptorCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImageDescriptor_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBackgroundImageDescriptorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImageDescriptor_:Ljava/util/List;

    return-object v0
.end method

.method public getBackgroundImageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Photo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->backgroundImage_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->cachedSize:I

    return v0
.end method

.method public getConfiguration()Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->configuration_:Lcom/google/geo/sidekick/Sidekick$Configuration;

    return-object v0
.end method

.method public getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->entryTree_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    return-object v0
.end method

.method public getEntryTreeCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->entryTree_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryTreeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$EntryTree;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->entryTree_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->hasConfiguration()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getConfiguration()Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Photo;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageDescriptorList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_3
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->cachedSize:I

    return v2
.end method

.method public hasConfiguration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->hasConfiguration:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->setConfiguration(Lcom/google/geo/sidekick/Sidekick$Configuration;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->addEntryTree(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->addBackgroundImage(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->addBackgroundImageDescriptor(Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v0

    return-object v0
.end method

.method public setConfiguration(Lcom/google/geo/sidekick/Sidekick$Configuration;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Configuration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->hasConfiguration:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->configuration_:Lcom/google/geo/sidekick/Sidekick$Configuration;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->hasConfiguration()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getConfiguration()Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Photo;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageDescriptorList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_3
    return-void
.end method
