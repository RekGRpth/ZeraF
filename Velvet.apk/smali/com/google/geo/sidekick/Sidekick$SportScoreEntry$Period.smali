.class public final Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Period"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasNumber:Z

.field private hasPeriodType:Z

.field private hasTimeSeconds:Z

.field private number_:I

.field private periodType_:I

.field private score_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private timeSeconds_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->score_:Ljava/util/List;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->timeSeconds_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->periodType_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->number_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addScore(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->score_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->score_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->score_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->cachedSize:I

    return v0
.end method

.method public getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->number_:I

    return v0
.end method

.method public getPeriodType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->periodType_:I

    return v0
.end method

.method public getScore(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->score_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getScoreCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->score_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getScoreList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->score_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScoreList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_0
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScoreList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasTimeSeconds()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getTimeSeconds()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasPeriodType()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getPeriodType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasNumber()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->cachedSize:I

    return v3
.end method

.method public getTimeSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->timeSeconds_:I

    return v0
.end method

.method public hasNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasNumber:Z

    return v0
.end method

.method public hasPeriodType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasPeriodType:Z

    return v0
.end method

.method public hasTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasTimeSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->addScore(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->setTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->setPeriodType(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->setNumber(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    move-result-object v0

    return-object v0
.end method

.method public setNumber(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasNumber:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->number_:I

    return-object p0
.end method

.method public setPeriodType(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasPeriodType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->periodType_:I

    return-object p0
.end method

.method public setTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasTimeSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->timeSeconds_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScoreList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasTimeSeconds()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getTimeSeconds()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasPeriodType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getPeriodType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasNumber()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    return-void
.end method
