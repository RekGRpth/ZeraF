.class public final Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Line"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private color_:I

.field private departureGroup_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;",
            ">;"
        }
    .end annotation
.end field

.field private hasColor:Z

.field private hasName:Z

.field private hasTextColor:Z

.field private hasVehicleType:Z

.field private name_:Ljava/lang/String;

.field private textColor_:I

.field private vehicleType_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->name_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->color_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->textColor_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->vehicleType_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->departureGroup_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDepartureGroup(Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->departureGroup_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->departureGroup_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->departureGroup_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->cachedSize:I

    return v0
.end method

.method public getColor()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->color_:I

    return v0
.end method

.method public getDepartureGroupList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->departureGroup_:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasName()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasColor()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getColor()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasTextColor()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getTextColor()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasVehicleType()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getVehicleType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getDepartureGroupList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->cachedSize:I

    return v2
.end method

.method public getTextColor()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->textColor_:I

    return v0
.end method

.method public getVehicleType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->vehicleType_:I

    return v0
.end method

.method public hasColor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasColor:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasName:Z

    return v0
.end method

.method public hasTextColor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasTextColor:Z

    return v0
.end method

.method public hasVehicleType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasVehicleType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->setColor(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->setTextColor(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->setVehicleType(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->addDepartureGroup(Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    move-result-object v0

    return-object v0
.end method

.method public setColor(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasColor:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->color_:I

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setTextColor(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasTextColor:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->textColor_:I

    return-object p0
.end method

.method public setVehicleType(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasVehicleType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->vehicleType_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasColor()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getColor()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasTextColor()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getTextColor()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasVehicleType()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getVehicleType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getDepartureGroupList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    return-void
.end method
