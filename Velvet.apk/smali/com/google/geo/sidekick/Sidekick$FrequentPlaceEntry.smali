.class public final Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FrequentPlaceEntry"
.end annotation


# instance fields
.field private cachedSize:I

.field private eventImage_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private eventTimeSeconds_:J

.field private eventType_:I

.field private frequentPlace_:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

.field private hasEventImage:Z

.field private hasEventTimeSeconds:Z

.field private hasEventType:Z

.field private hasFrequentPlace:Z

.field private route_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$CommuteSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->frequentPlace_:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventType_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventTimeSeconds_:J

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventImage_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearRoute()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->cachedSize:I

    return v0
.end method

.method public getEventImage()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventImage_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getEventTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventTimeSeconds_:J

    return-wide v0
.end method

.method public getEventType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventType_:I

    return v0
.end method

.method public getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->frequentPlace_:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    return-object v0
.end method

.method public getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object v0
.end method

.method public getRouteCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRouteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$CommuteSummary;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->route_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventType()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventTimeSeconds()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventTimeSeconds()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventImage()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->cachedSize:I

    return v2
.end method

.method public hasEventImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventImage:Z

    return v0
.end method

.method public hasEventTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventTimeSeconds:Z

    return v0
.end method

.method public hasEventType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventType:Z

    return v0
.end method

.method public hasFrequentPlace()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->setFrequentPlace(Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->addRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->setEventType(I)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->setEventTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->setEventImage(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    return-object v0
.end method

.method public setEventImage(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventImage:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventImage_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setEventTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventTimeSeconds_:J

    return-object p0
.end method

.method public setEventType(I)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->eventType_:I

    return-object p0
.end method

.method public setFrequentPlace(Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->frequentPlace_:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventTimeSeconds()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventTimeSeconds()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventImage()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    return-void
.end method
