.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StockData"
.end annotation


# instance fields
.field private cachedSize:I

.field private deleted_:Z

.field private description_:Ljava/lang/String;

.field private exchange_:Ljava/lang/String;

.field private gin_:J

.field private hasDeleted:Z

.field private hasDescription:Z

.field private hasExchange:Z

.field private hasGin:Z

.field private hasSymbol:Z

.field private symbol_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->exchange_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->symbol_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->description_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->gin_:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->deleted_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->cachedSize:I

    return v0
.end method

.method public getDeleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->deleted_:Z

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getExchange()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->exchange_:Ljava/lang/String;

    return-object v0
.end method

.method public getGin()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->gin_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasExchange()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getExchange()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasSymbol()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDescription()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasGin()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getGin()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDeleted()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDeleted()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->cachedSize:I

    return v0
.end method

.method public getSymbol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->symbol_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDeleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDeleted:Z

    return v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDescription:Z

    return v0
.end method

.method public hasExchange()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasExchange:Z

    return v0
.end method

.method public hasGin()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasGin:Z

    return v0
.end method

.method public hasSymbol()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasSymbol:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setExchange(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setSymbol(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setDescription(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setGin(J)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setDeleted(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    move-result-object v0

    return-object v0
.end method

.method public setDeleted(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDeleted:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->deleted_:Z

    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDescription:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setExchange(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasExchange:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->exchange_:Ljava/lang/String;

    return-object p0
.end method

.method public setGin(J)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasGin:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->gin_:J

    return-object p0
.end method

.method public setSymbol(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasSymbol:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->symbol_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasExchange()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getExchange()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasSymbol()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasGin()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getGin()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDeleted()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDeleted()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    return-void
.end method
