.class public final Lcom/google/geo/sidekick/Sidekick$Movie;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Movie"
.end annotation


# instance fields
.field private actor_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private hasImage:Z

.field private hasImageUrl:Z

.field private hasLength:Z

.field private hasMpaaRating:Z

.field private hasOpeningDateSecs:Z

.field private hasTitle:Z

.field private hasTrailerUrl:Z

.field private hasWebUrl:Z

.field private imageUrl_:Ljava/lang/String;

.field private image_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private length_:Ljava/lang/String;

.field private mpaaRating_:Ljava/lang/String;

.field private openingDateSecs_:J

.field private rating_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Rating;",
            ">;"
        }
    .end annotation
.end field

.field private showtime_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private title_:Ljava/lang/String;

.field private trailerUrl_:Ljava/lang/String;

.field private webUrl_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->title_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->actor_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->mpaaRating_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->length_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->showtime_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->imageUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->trailerUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->webUrl_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->image_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->openingDateSecs_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->rating_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addActor(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->actor_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->actor_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->actor_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addRating(Lcom/google/geo/sidekick/Sidekick$Rating;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Rating;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->rating_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->rating_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->rating_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addShowtime(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->showtime_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->showtime_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->showtime_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getActorCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->actor_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getActorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->actor_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->cachedSize:I

    return v0
.end method

.method public getImage()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->image_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->imageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getLength()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->length_:Ljava/lang/String;

    return-object v0
.end method

.method public getMpaaRating()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->mpaaRating_:Ljava/lang/String;

    return-object v0
.end method

.method public getOpeningDateSecs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->openingDateSecs_:J

    return-wide v0
.end method

.method public getRating(I)Lcom/google/geo/sidekick/Sidekick$Rating;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->rating_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Rating;

    return-object v0
.end method

.method public getRatingCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->rating_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRatingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Rating;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->rating_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getActorList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getActorList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasMpaaRating()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getMpaaRating()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasLength()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getLength()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getShowtimeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_4
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getShowtimeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImageUrl()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getImageUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTrailerUrl()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTrailerUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasWebUrl()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getWebUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImage()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasOpeningDateSecs()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getOpeningDateSecs()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getRatingList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Rating;

    const/16 v4, 0xb

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_2

    :cond_a
    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->cachedSize:I

    return v3
.end method

.method public getShowtime(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->showtime_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getShowtimeCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->showtime_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getShowtimeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->showtime_:Ljava/util/List;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getTrailerUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->trailerUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getWebUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->webUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public hasImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImage:Z

    return v0
.end method

.method public hasImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImageUrl:Z

    return v0
.end method

.method public hasLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasLength:Z

    return v0
.end method

.method public hasMpaaRating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasMpaaRating:Z

    return v0
.end method

.method public hasOpeningDateSecs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasOpeningDateSecs:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTitle:Z

    return v0
.end method

.method public hasTrailerUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTrailerUrl:Z

    return v0
.end method

.method public hasWebUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasWebUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$Movie;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->addActor(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->setMpaaRating(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->setLength(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->addShowtime(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->setImageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->setTrailerUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Movie;->setWebUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Movie;->setImage(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$Movie;->setOpeningDateSecs(J)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Rating;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Rating;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Movie;->addRating(Lcom/google/geo/sidekick/Sidekick$Rating;)Lcom/google/geo/sidekick/Sidekick$Movie;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$Movie;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Movie;

    move-result-object v0

    return-object v0
.end method

.method public setImage(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImage:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->image_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setImageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImageUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->imageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setLength(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasLength:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->length_:Ljava/lang/String;

    return-object p0
.end method

.method public setMpaaRating(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasMpaaRating:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->mpaaRating_:Ljava/lang/String;

    return-object p0
.end method

.method public setOpeningDateSecs(J)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasOpeningDateSecs:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->openingDateSecs_:J

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTitle:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public setTrailerUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTrailerUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->trailerUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setWebUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->hasWebUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Movie;->webUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getActorList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasMpaaRating()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getMpaaRating()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasLength()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getLength()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getShowtimeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImageUrl()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTrailerUrl()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTrailerUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasWebUrl()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getWebUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasOpeningDateSecs()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getOpeningDateSecs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Movie;->getRatingList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Rating;

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_a
    return-void
.end method
