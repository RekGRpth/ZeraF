.class public final Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayMediaAction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;,
        Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;,
        Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;,
        Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;,
        Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;
    }
.end annotation


# instance fields
.field private appItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

.field private bookItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

.field private cachedSize:I

.field private finskyDocid_:Ljava/lang/String;

.field private finskyFetchDocid_:Ljava/lang/String;

.field private hasAppItem:Z

.field private hasBookItem:Z

.field private hasFinskyDocid:Z

.field private hasFinskyFetchDocid:Z

.field private hasIsFromSoundSearch:Z

.field private hasItemImage:Z

.field private hasItemImageUrl:Z

.field private hasItemNumberOfRatings:Z

.field private hasItemOwnedByUser:Z

.field private hasItemPreviewUrl:Z

.field private hasItemRating:Z

.field private hasItemRemainingRentalSeconds:Z

.field private hasMediaSource:Z

.field private hasMovieItem:Z

.field private hasMusicItem:Z

.field private hasName:Z

.field private hasSuggestedQuery:Z

.field private hasTarget:Z

.field private hasUrl:Z

.field private intentFlag_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private isFromSoundSearch_:Z

.field private itemImageUrl_:Ljava/lang/String;

.field private itemImage_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private itemNumberOfRatings_:I

.field private itemOwnedByUser_:Z

.field private itemPreviewUrl_:Ljava/lang/String;

.field private itemPriceTag_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;",
            ">;"
        }
    .end annotation
.end field

.field private itemRating_:D

.field private itemRemainingRentalSeconds_:J

.field private mediaSource_:I

.field private movieItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

.field private musicItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

.field private name_:Ljava/lang/String;

.field private suggestedQuery_:Ljava/lang/String;

.field private target_:Ljava/lang/String;

.field private url_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->name_:Ljava/lang/String;

    iput v2, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->mediaSource_:I

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->musicItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->movieItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->bookItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->appItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->suggestedQuery_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->url_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemImage_:Lcom/google/protobuf/micro/ByteStringMicro;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemImageUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPreviewUrl_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemOwnedByUser_:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemRemainingRentalSeconds_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPriceTag_:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemRating_:D

    iput v2, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemNumberOfRatings_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->finskyDocid_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->finskyFetchDocid_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->isFromSoundSearch_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->target_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->intentFlag_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addIntentFlag(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->intentFlag_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->intentFlag_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->intentFlag_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addItemPriceTag(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPriceTag_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPriceTag_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPriceTag_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAppItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->appItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    return-object v0
.end method

.method public getBookItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->bookItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->cachedSize:I

    return v0
.end method

.method public getFinskyDocid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->finskyDocid_:Ljava/lang/String;

    return-object v0
.end method

.method public getFinskyFetchDocid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->finskyFetchDocid_:Ljava/lang/String;

    return-object v0
.end method

.method public getIntentFlagList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->intentFlag_:Ljava/util/List;

    return-object v0
.end method

.method public getIsFromSoundSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->isFromSoundSearch_:Z

    return v0
.end method

.method public getItemImage()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemImage_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getItemImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemImageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getItemNumberOfRatings()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemNumberOfRatings_:I

    return v0
.end method

.method public getItemOwnedByUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemOwnedByUser_:Z

    return v0
.end method

.method public getItemPreviewUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPreviewUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getItemPriceTag(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPriceTag_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    return-object v0
.end method

.method public getItemPriceTagCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPriceTag_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemPriceTagList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPriceTag_:Ljava/util/List;

    return-object v0
.end method

.method public getItemRating()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemRating_:D

    return-wide v0
.end method

.method public getItemRemainingRentalSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemRemainingRentalSeconds_:J

    return-wide v0
.end method

.method public getMediaSource()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->mediaSource_:I

    return v0
.end method

.method public getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->movieItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    return-object v0
.end method

.method public getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->musicItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasName()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMediaSource()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMediaSource()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasUrl()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasTarget()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getTarget()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMusicItem()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMovieItem()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasBookItem()Z

    move-result v4

    if-eqz v4, :cond_6

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getBookItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasSuggestedQuery()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImage()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemImage()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImageUrl()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemImageUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemPreviewUrl()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPreviewUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemOwnedByUser()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemOwnedByUser()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRemainingRentalSeconds()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemRemainingRentalSeconds()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPriceTagList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    const/16 v4, 0xf

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRating()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemRating()D

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemNumberOfRatings()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x11

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemNumberOfRatings()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyDocid()Z

    move-result v4

    if-eqz v4, :cond_10

    const/16 v4, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getFinskyDocid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyFetchDocid()Z

    move-result v4

    if-eqz v4, :cond_11

    const/16 v4, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getFinskyFetchDocid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasIsFromSoundSearch()Z

    move-result v4

    if-eqz v4, :cond_12

    const/16 v4, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIsFromSoundSearch()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasAppItem()Z

    move-result v4

    if-eqz v4, :cond_13

    const/16 v4, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getAppItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_13
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIntentFlagList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_14
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIntentFlagList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->cachedSize:I

    return v3
.end method

.method public getSuggestedQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->suggestedQuery_:Ljava/lang/String;

    return-object v0
.end method

.method public getTarget()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->target_:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAppItem()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasAppItem:Z

    return v0
.end method

.method public hasBookItem()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasBookItem:Z

    return v0
.end method

.method public hasFinskyDocid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyDocid:Z

    return v0
.end method

.method public hasFinskyFetchDocid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyFetchDocid:Z

    return v0
.end method

.method public hasIsFromSoundSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasIsFromSoundSearch:Z

    return v0
.end method

.method public hasItemImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImage:Z

    return v0
.end method

.method public hasItemImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImageUrl:Z

    return v0
.end method

.method public hasItemNumberOfRatings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemNumberOfRatings:Z

    return v0
.end method

.method public hasItemOwnedByUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemOwnedByUser:Z

    return v0
.end method

.method public hasItemPreviewUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemPreviewUrl:Z

    return v0
.end method

.method public hasItemRating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRating:Z

    return v0
.end method

.method public hasItemRemainingRentalSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRemainingRentalSeconds:Z

    return v0
.end method

.method public hasMediaSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMediaSource:Z

    return v0
.end method

.method public hasMovieItem()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMovieItem:Z

    return v0
.end method

.method public hasMusicItem()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMusicItem:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasName:Z

    return v0
.end method

.method public hasSuggestedQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasSuggestedQuery:Z

    return v0
.end method

.method public hasTarget()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasTarget:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setMediaSource(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setTarget(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setMusicItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setMovieItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setBookItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setSuggestedQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemImage(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemPreviewUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemOwnedByUser(Z)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemRemainingRentalSeconds(J)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->addItemPriceTag(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemRating(D)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemNumberOfRatings(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setFinskyDocid(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setFinskyFetchDocid(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setIsFromSoundSearch(Z)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_14
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setAppItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->addIntentFlag(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x7a -> :sswitch_e
        0x81 -> :sswitch_f
        0x88 -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa0 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb0 -> :sswitch_15
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v0

    return-object v0
.end method

.method public setAppItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasAppItem:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->appItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    return-object p0
.end method

.method public setBookItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasBookItem:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->bookItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    return-object p0
.end method

.method public setFinskyDocid(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyDocid:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->finskyDocid_:Ljava/lang/String;

    return-object p0
.end method

.method public setFinskyFetchDocid(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyFetchDocid:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->finskyFetchDocid_:Ljava/lang/String;

    return-object p0
.end method

.method public setIsFromSoundSearch(Z)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasIsFromSoundSearch:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->isFromSoundSearch_:Z

    return-object p0
.end method

.method public setItemImage(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImage:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemImage_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setItemImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImageUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemImageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setItemNumberOfRatings(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemNumberOfRatings:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemNumberOfRatings_:I

    return-object p0
.end method

.method public setItemOwnedByUser(Z)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemOwnedByUser:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemOwnedByUser_:Z

    return-object p0
.end method

.method public setItemPreviewUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemPreviewUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemPreviewUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setItemRating(D)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRating:Z

    iput-wide p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemRating_:D

    return-object p0
.end method

.method public setItemRemainingRentalSeconds(J)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRemainingRentalSeconds:Z

    iput-wide p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->itemRemainingRentalSeconds_:J

    return-object p0
.end method

.method public setMediaSource(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMediaSource:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->mediaSource_:I

    return-object p0
.end method

.method public setMovieItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMovieItem:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->movieItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    return-object p0
.end method

.method public setMusicItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMusicItem:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->musicItem_:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasName:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setSuggestedQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasSuggestedQuery:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->suggestedQuery_:Ljava/lang/String;

    return-object p0
.end method

.method public setTarget(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasTarget:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->target_:Ljava/lang/String;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->url_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMediaSource()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMediaSource()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasTarget()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getTarget()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMusicItem()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMovieItem()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasBookItem()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getBookItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasSuggestedQuery()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImage()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemImage()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImageUrl()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemPreviewUrl()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPreviewUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemOwnedByUser()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemOwnedByUser()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRemainingRentalSeconds()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemRemainingRentalSeconds()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPriceTagList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRating()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemRating()D

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemNumberOfRatings()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemNumberOfRatings()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyDocid()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getFinskyDocid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasFinskyFetchDocid()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getFinskyFetchDocid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasIsFromSoundSearch()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIsFromSoundSearch()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasAppItem()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getAppItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIntentFlagList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/16 v2, 0x16

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_1

    :cond_14
    return-void
.end method
