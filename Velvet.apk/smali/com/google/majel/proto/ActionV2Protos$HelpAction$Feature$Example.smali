.class public final Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Example"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasImageUrl:Z

.field private hasLocalizedImage:Z

.field private hasMinVersion:Z

.field private hasQuery:Z

.field private hasRelativeDays:Z

.field private hasRelativeSeconds:Z

.field private hasRetireVersion:Z

.field private hasTime:Z

.field private imageUrl_:Ljava/lang/String;

.field private localizedImage_:Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

.field private localizedQuery_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$LocalizedString;",
            ">;"
        }
    .end annotation
.end field

.field private minVersion_:I

.field private query_:Ljava/lang/String;

.field private relativeDays_:I

.field private relativeSeconds_:I

.field private requiredCapability_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private retireVersion_:I

.field private substitution_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

.field private unusedLocale_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->minVersion_:I

    iput v1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->retireVersion_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->unusedLocale_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->requiredCapability_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedQuery_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->query_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->substitution_:Ljava/util/List;

    iput-object v2, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedImage_:Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->imageUrl_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    iput v1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->relativeSeconds_:I

    iput v1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->relativeDays_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addLocalizedQuery(Lcom/google/majel/proto/ActionV2Protos$LocalizedString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$LocalizedString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedQuery_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedQuery_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedQuery_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addRequiredCapability(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->requiredCapability_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->requiredCapability_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->requiredCapability_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSubstitution(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->substitution_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->substitution_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->substitution_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addUnusedLocale(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->unusedLocale_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->unusedLocale_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->unusedLocale_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->cachedSize:I

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->imageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalizedImage()Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedImage_:Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

    return-object v0
.end method

.method public getLocalizedQueryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$LocalizedString;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedQuery_:Ljava/util/List;

    return-object v0
.end method

.method public getMinVersion()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->minVersion_:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->query_:Ljava/lang/String;

    return-object v0
.end method

.method public getRelativeDays()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->relativeDays_:I

    return v0
.end method

.method public getRelativeSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->relativeSeconds_:I

    return v0
.end method

.method public getRequiredCapabilityList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->requiredCapability_:Ljava/util/List;

    return-object v0
.end method

.method public getRetireVersion()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->retireVersion_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasMinVersion()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getMinVersion()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRetireVersion()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRetireVersion()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getUnusedLocaleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_2
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getUnusedLocaleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRequiredCapabilityList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRequiredCapabilityList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getSubstitutionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_2

    :cond_4
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getSubstitutionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasImageUrl()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getImageUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasTime()Z

    move-result v4

    if-eqz v4, :cond_6

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeSeconds()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRelativeSeconds()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeDays()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRelativeDays()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasLocalizedImage()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getLocalizedImage()Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getLocalizedQueryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/majel/proto/ActionV2Protos$LocalizedString;

    const/16 v4, 0xc

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasQuery()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getQuery()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    iput v3, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->cachedSize:I

    return v3
.end method

.method public getSubstitutionCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->substitution_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSubstitutionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->substitution_:Ljava/util/List;

    return-object v0
.end method

.method public getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    return-object v0
.end method

.method public getUnusedLocaleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->unusedLocale_:Ljava/util/List;

    return-object v0
.end method

.method public hasImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasImageUrl:Z

    return v0
.end method

.method public hasLocalizedImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasLocalizedImage:Z

    return v0
.end method

.method public hasMinVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasMinVersion:Z

    return v0
.end method

.method public hasQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasQuery:Z

    return v0
.end method

.method public hasRelativeDays()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeDays:Z

    return v0
.end method

.method public hasRelativeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeSeconds:Z

    return v0
.end method

.method public hasRetireVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRetireVersion:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasTime:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setMinVersion(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setRetireVersion(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addUnusedLocale(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addRequiredCapability(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addSubstitution(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setRelativeSeconds(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setRelativeDays(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setLocalizedImage(Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$LocalizedString;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$LocalizedString;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addLocalizedQuery(Lcom/google/majel/proto/ActionV2Protos$LocalizedString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v0

    return-object v0
.end method

.method public setImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasImageUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->imageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocalizedImage(Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasLocalizedImage:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->localizedImage_:Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

    return-object p0
.end method

.method public setMinVersion(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasMinVersion:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->minVersion_:I

    return-object p0
.end method

.method public setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasQuery:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->query_:Ljava/lang/String;

    return-object p0
.end method

.method public setRelativeDays(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeDays:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->relativeDays_:I

    return-object p0
.end method

.method public setRelativeSeconds(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeSeconds:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->relativeSeconds_:I

    return-object p0
.end method

.method public setRetireVersion(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRetireVersion:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->retireVersion_:I

    return-object p0
.end method

.method public setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasMinVersion()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getMinVersion()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRetireVersion()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRetireVersion()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getUnusedLocaleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRequiredCapabilityList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getSubstitutionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasImageUrl()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeSeconds()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRelativeSeconds()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeDays()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRelativeDays()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasLocalizedImage()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getLocalizedImage()Lcom/google/majel/proto/ActionV2Protos$LocalizedImage;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getLocalizedQueryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$LocalizedString;

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasQuery()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    return-void
.end method
