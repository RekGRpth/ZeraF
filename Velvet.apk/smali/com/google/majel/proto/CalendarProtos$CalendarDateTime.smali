.class public final Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CalendarProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/CalendarProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CalendarDateTime"
.end annotation


# instance fields
.field private cachedSize:I

.field private dateUnspecified_:Z

.field private date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

.field private hasDate:Z

.field private hasDateUnspecified:Z

.field private hasOffsetMs:Z

.field private hasTime:Z

.field private hasTimeMs:Z

.field private hasTimeUnspecified:Z

.field private offsetMs_:I

.field private timeMs_:J

.field private timeUnspecified_:Z

.field private time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->timeMs_:J

    iput v2, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->offsetMs_:I

    iput-object v3, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    iput-boolean v2, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->dateUnspecified_:Z

    iput-object v3, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    iput-boolean v2, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->timeUnspecified_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->cachedSize:I

    return v0
.end method

.method public getDate()Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    return-object v0
.end method

.method public getDateUnspecified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->dateUnspecified_:Z

    return v0
.end method

.method public getOffsetMs()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->offsetMs_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeMs()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTimeMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasOffsetMs()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getOffsetMs()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDate()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getDate()Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDateUnspecified()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getDateUnspecified()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTime()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeUnspecified()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTimeUnspecified()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->cachedSize:I

    return v0
.end method

.method public getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    return-object v0
.end method

.method public getTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->timeMs_:J

    return-wide v0
.end method

.method public getTimeUnspecified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->timeUnspecified_:Z

    return v0
.end method

.method public hasDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDate:Z

    return v0
.end method

.method public hasDateUnspecified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDateUnspecified:Z

    return v0
.end method

.method public hasOffsetMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasOffsetMs:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTime:Z

    return v0
.end method

.method public hasTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeMs:Z

    return v0
.end method

.method public hasTimeUnspecified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeUnspecified:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->setTimeMs(J)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->setOffsetMs(I)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->setDate(Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->setDateUnspecified(Z)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->setTimeUnspecified(Z)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    move-result-object v0

    return-object v0
.end method

.method public setDate(Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDate:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    return-object p0
.end method

.method public setDateUnspecified(Z)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDateUnspecified:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->dateUnspecified_:Z

    return-object p0
.end method

.method public setOffsetMs(I)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasOffsetMs:Z

    iput p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->offsetMs_:I

    return-object p0
.end method

.method public setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    return-object p0
.end method

.method public setTimeMs(J)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeMs:Z

    iput-wide p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->timeMs_:J

    return-object p0
.end method

.method public setTimeUnspecified(Z)Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeUnspecified:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->timeUnspecified_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeMs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTimeMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasOffsetMs()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getOffsetMs()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getDate()Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasDateUnspecified()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getDateUnspecified()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->hasTimeUnspecified()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTimeUnspecified()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    return-void
.end method
