.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BaseballMatch"
.end annotation


# instance fields
.field private cachedSize:I

.field private firstTeamErrors_:I

.field private firstTeamHits_:I

.field private firstTeamRuns_:I

.field private hasFirstTeamErrors:Z

.field private hasFirstTeamHits:Z

.field private hasFirstTeamRuns:Z

.field private hasSecondTeamErrors:Z

.field private hasSecondTeamHits:Z

.field private hasSecondTeamRuns:Z

.field private secondTeamErrors_:I

.field private secondTeamHits_:I

.field private secondTeamRuns_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamRuns_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamHits_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamErrors_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamHits_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamRuns_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamErrors_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->cachedSize:I

    return v0
.end method

.method public getFirstTeamErrors()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamErrors_:I

    return v0
.end method

.method public getFirstTeamHits()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamHits_:I

    return v0
.end method

.method public getFirstTeamRuns()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamRuns_:I

    return v0
.end method

.method public getSecondTeamErrors()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamErrors_:I

    return v0
.end method

.method public getSecondTeamHits()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamHits_:I

    return v0
.end method

.method public getSecondTeamRuns()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamRuns_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamRuns()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamRuns()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamHits()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamHits()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamErrors()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamErrors()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamHits()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamHits()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamRuns()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamRuns()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamErrors()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamErrors()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->cachedSize:I

    return v0
.end method

.method public hasFirstTeamErrors()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamErrors:Z

    return v0
.end method

.method public hasFirstTeamHits()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamHits:Z

    return v0
.end method

.method public hasFirstTeamRuns()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamRuns:Z

    return v0
.end method

.method public hasSecondTeamErrors()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamErrors:Z

    return v0
.end method

.method public hasSecondTeamHits()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamHits:Z

    return v0
.end method

.method public hasSecondTeamRuns()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamRuns:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->setFirstTeamRuns(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->setFirstTeamHits(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->setFirstTeamErrors(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->setSecondTeamHits(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->setSecondTeamRuns(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->setSecondTeamErrors(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    move-result-object v0

    return-object v0
.end method

.method public setFirstTeamErrors(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamErrors:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamErrors_:I

    return-object p0
.end method

.method public setFirstTeamHits(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamHits:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamHits_:I

    return-object p0
.end method

.method public setFirstTeamRuns(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamRuns:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->firstTeamRuns_:I

    return-object p0
.end method

.method public setSecondTeamErrors(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamErrors:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamErrors_:I

    return-object p0
.end method

.method public setSecondTeamHits(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamHits:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamHits_:I

    return-object p0
.end method

.method public setSecondTeamRuns(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamRuns:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->secondTeamRuns_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamRuns()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamRuns()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamHits()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamHits()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasFirstTeamErrors()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamErrors()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamHits()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamHits()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamRuns()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamRuns()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->hasSecondTeamErrors()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamErrors()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    return-void
.end method
