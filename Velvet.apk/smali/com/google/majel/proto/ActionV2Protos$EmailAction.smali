.class public final Lcom/google/majel/proto/ActionV2Protos$EmailAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmailAction"
.end annotation


# instance fields
.field private bcc_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;",
            ">;"
        }
    .end annotation
.end field

.field private bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

.field private body_:Ljava/lang/String;

.field private cachedSize:I

.field private cc_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;",
            ">;"
        }
    .end annotation
.end field

.field private hasBody:Z

.field private hasBodySpan:Z

.field private hasSubject:Z

.field private hasSubjectSpan:Z

.field private subjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

.field private subject_:Ljava/lang/String;

.field private to_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->to_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cc_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bcc_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->subject_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->subjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->body_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    return-object v0
.end method


# virtual methods
.method public addBcc(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bcc_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bcc_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bcc_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addCc(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cc_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cc_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cc_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addTo(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->to_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->to_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->to_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBccList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bcc_:Ljava/util/List;

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->body_:Ljava/lang/String;

    return-object v0
.end method

.method public getBodySpan()Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cachedSize:I

    return v0
.end method

.method public getCcList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cc_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getToList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getCcList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBccList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubject()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBody()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubjectSpan()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubjectSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBodySpan()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBodySpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    iput v2, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->cachedSize:I

    return v2
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->subject_:Ljava/lang/String;

    return-object v0
.end method

.method public getSubjectSpan()Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->subjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object v0
.end method

.method public getTo(I)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->to_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    return-object v0
.end method

.method public getToCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->to_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getToList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->to_:Ljava/util/List;

    return-object v0
.end method

.method public hasBody()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBody:Z

    return v0
.end method

.method public hasBodySpan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBodySpan:Z

    return v0
.end method

.method public hasSubject()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubject:Z

    return v0
.end method

.method public hasSubjectSpan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubjectSpan:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->addTo(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->addCc(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->addBcc(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->setSubject(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->setBody(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/SpanProtos$Span;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$Span;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->setSubjectSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/SpanProtos$Span;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$Span;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->setBodySpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    move-result-object v0

    return-object v0
.end method

.method public setBody(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBody:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->body_:Ljava/lang/String;

    return-object p0
.end method

.method public setBodySpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$Span;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBodySpan:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object p0
.end method

.method public setSubject(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubject:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->subject_:Ljava/lang/String;

    return-object p0
.end method

.method public setSubjectSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$Span;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubjectSpan:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->subjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getToList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getCcList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBccList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubject()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBody()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubjectSpan()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubjectSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBodySpan()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBodySpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    return-void
.end method
