.class public final Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ArgValue"
.end annotation


# instance fields
.field private argName_:Ljava/lang/String;

.field private boolValue_:Z

.field private cachedSize:I

.field private doubleValue_:D

.field private hasArgName:Z

.field private hasBoolValue:Z

.field private hasDoubleValue:Z

.field private hasIntValue:Z

.field private hasStringValue:Z

.field private intValue_:I

.field private stringValue_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->argName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->stringValue_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->boolValue_:Z

    iput v1, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->intValue_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->doubleValue_:D

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getArgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->argName_:Ljava/lang/String;

    return-object v0
.end method

.method public getBoolValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->boolValue_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->cachedSize:I

    return v0
.end method

.method public getDoubleValue()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->doubleValue_:D

    return-wide v0
.end method

.method public getIntValue()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->intValue_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasArgName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getArgName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasStringValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getStringValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasBoolValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getBoolValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasIntValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getIntValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasDoubleValue()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getDoubleValue()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->cachedSize:I

    return v0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->stringValue_:Ljava/lang/String;

    return-object v0
.end method

.method public hasArgName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasArgName:Z

    return v0
.end method

.method public hasBoolValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasBoolValue:Z

    return v0
.end method

.method public hasDoubleValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasDoubleValue:Z

    return v0
.end method

.method public hasIntValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasIntValue:Z

    return v0
.end method

.method public hasStringValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasStringValue:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->setArgName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->setStringValue(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->setBoolValue(Z)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->setIntValue(I)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->setDoubleValue(D)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x29 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;

    move-result-object v0

    return-object v0
.end method

.method public setArgName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasArgName:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->argName_:Ljava/lang/String;

    return-object p0
.end method

.method public setBoolValue(Z)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasBoolValue:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->boolValue_:Z

    return-object p0
.end method

.method public setDoubleValue(D)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasDoubleValue:Z

    iput-wide p1, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->doubleValue_:D

    return-object p0
.end method

.method public setIntValue(I)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasIntValue:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->intValue_:I

    return-object p0
.end method

.method public setStringValue(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasStringValue:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->stringValue_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasArgName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getArgName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasStringValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasBoolValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getBoolValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasIntValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getIntValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->hasDoubleValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction$ArgValue;->getDoubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_4
    return-void
.end method
