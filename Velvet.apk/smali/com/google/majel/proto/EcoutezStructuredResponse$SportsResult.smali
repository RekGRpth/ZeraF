.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SportsResult"
.end annotation


# instance fields
.field private associationData_:Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

.field private cachedSize:I

.field private hasAssociationData:Z

.field private hasSportType:Z

.field private hasTeamData:Z

.field private hasTeamVsTeamData:Z

.field private hasTitle:Z

.field private sportType_:I

.field private teamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

.field private teamVsTeamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

.field private title_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->sportType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->title_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->teamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->teamVsTeamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->associationData_:Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAssociationData()Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->associationData_:Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasSportType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getSportType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamData()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamVsTeamData()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamVsTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasAssociationData()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getAssociationData()Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->cachedSize:I

    return v0
.end method

.method public getSportType()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->sportType_:I

    return v0
.end method

.method public getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->teamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    return-object v0
.end method

.method public getTeamVsTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->teamVsTeamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAssociationData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasAssociationData:Z

    return v0
.end method

.method public hasSportType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasSportType:Z

    return v0
.end method

.method public hasTeamData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamData:Z

    return v0
.end method

.method public hasTeamVsTeamData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamVsTeamData:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->setSportType(I)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->setTitle(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->setTeamData(Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->setTeamVsTeamData(Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->setAssociationData(Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    move-result-object v0

    return-object v0
.end method

.method public setAssociationData(Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasAssociationData:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->associationData_:Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

    return-object p0
.end method

.method public setSportType(I)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasSportType:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->sportType_:I

    return-object p0
.end method

.method public setTeamData(Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamData:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->teamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    return-object p0
.end method

.method public setTeamVsTeamData(Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamVsTeamData:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->teamVsTeamData_:Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTitle:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasSportType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getSportType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamData()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamVsTeamData()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamVsTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasAssociationData()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getAssociationData()Lcom/google/majel/proto/EcoutezStructuredResponse$AssociationData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    return-void
.end method
