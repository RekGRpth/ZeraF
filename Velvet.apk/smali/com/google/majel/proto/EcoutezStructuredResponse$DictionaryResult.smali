.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DictionaryResult"
.end annotation


# instance fields
.field private attributionLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

.field private cachedSize:I

.field private dictionaryWord_:Ljava/lang/String;

.field private externalDictionaryLink_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;",
            ">;"
        }
    .end annotation
.end field

.field private googleDictionaryLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

.field private hasAttributionLink:Z

.field private hasDictionaryWord:Z

.field private hasGoogleDictionaryLink:Z

.field private hasNormalForm:Z

.field private hasPartOfSpeech:Z

.field private hasPronunciation:Z

.field private hasSound:Z

.field private hasSynonymsHeader:Z

.field private hasVariationType:Z

.field private normalForm_:Ljava/lang/String;

.field private partOfSpeechMeaning_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;",
            ">;"
        }
    .end annotation
.end field

.field private partOfSpeech_:Ljava/lang/String;

.field private pronunciation_:Ljava/lang/String;

.field private sound_:Ljava/lang/String;

.field private synonym_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;",
            ">;"
        }
    .end annotation
.end field

.field private synonymsHeader_:Ljava/lang/String;

.field private variationType_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->dictionaryWord_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->variationType_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->normalForm_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeech_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->pronunciation_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->sound_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeechMeaning_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonymsHeader_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonym_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->googleDictionaryLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->attributionLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->externalDictionaryLink_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-direct {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    return-object v0
.end method


# virtual methods
.method public addExternalDictionaryLink(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->externalDictionaryLink_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->externalDictionaryLink_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->externalDictionaryLink_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPartOfSpeechMeaning(Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeechMeaning_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeechMeaning_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeechMeaning_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSynonym(Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonym_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonym_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonym_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAttributionLink()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->attributionLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->cachedSize:I

    return v0
.end method

.method public getDictionaryWord()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->dictionaryWord_:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalDictionaryLinkList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->externalDictionaryLink_:Ljava/util/List;

    return-object v0
.end method

.method public getGoogleDictionaryLink()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->googleDictionaryLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    return-object v0
.end method

.method public getNormalForm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->normalForm_:Ljava/lang/String;

    return-object v0
.end method

.method public getPartOfSpeech()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeech_:Ljava/lang/String;

    return-object v0
.end method

.method public getPartOfSpeechMeaningList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeechMeaning_:Ljava/util/List;

    return-object v0
.end method

.method public getPronunciation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->pronunciation_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasDictionaryWord()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getDictionaryWord()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasVariationType()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getVariationType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasNormalForm()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getNormalForm()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPartOfSpeech()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPronunciation()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPronunciation()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSound()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSound()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPartOfSpeechMeaningList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSynonymsHeader()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSynonymsHeader()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSynonymList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;

    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasGoogleDictionaryLink()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getGoogleDictionaryLink()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasAttributionLink()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getAttributionLink()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getExternalDictionaryLinkList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    const/16 v3, 0xc

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_b
    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->cachedSize:I

    return v2
.end method

.method public getSound()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->sound_:Ljava/lang/String;

    return-object v0
.end method

.method public getSynonymList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonym_:Ljava/util/List;

    return-object v0
.end method

.method public getSynonymsHeader()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonymsHeader_:Ljava/lang/String;

    return-object v0
.end method

.method public getVariationType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->variationType_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAttributionLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasAttributionLink:Z

    return v0
.end method

.method public hasDictionaryWord()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasDictionaryWord:Z

    return v0
.end method

.method public hasGoogleDictionaryLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasGoogleDictionaryLink:Z

    return v0
.end method

.method public hasNormalForm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasNormalForm:Z

    return v0
.end method

.method public hasPartOfSpeech()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPartOfSpeech:Z

    return v0
.end method

.method public hasPronunciation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPronunciation:Z

    return v0
.end method

.method public hasSound()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSound:Z

    return v0
.end method

.method public hasSynonymsHeader()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSynonymsHeader:Z

    return v0
.end method

.method public hasVariationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasVariationType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setDictionaryWord(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setVariationType(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setNormalForm(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setPartOfSpeech(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setPronunciation(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setSound(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->addPartOfSpeechMeaning(Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setSynonymsHeader(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->addSynonym(Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setGoogleDictionaryLink(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->setAttributionLink(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->addExternalDictionaryLink(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    move-result-object v0

    return-object v0
.end method

.method public setAttributionLink(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasAttributionLink:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->attributionLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    return-object p0
.end method

.method public setDictionaryWord(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasDictionaryWord:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->dictionaryWord_:Ljava/lang/String;

    return-object p0
.end method

.method public setGoogleDictionaryLink(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasGoogleDictionaryLink:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->googleDictionaryLink_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    return-object p0
.end method

.method public setNormalForm(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasNormalForm:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->normalForm_:Ljava/lang/String;

    return-object p0
.end method

.method public setPartOfSpeech(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPartOfSpeech:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->partOfSpeech_:Ljava/lang/String;

    return-object p0
.end method

.method public setPronunciation(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPronunciation:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->pronunciation_:Ljava/lang/String;

    return-object p0
.end method

.method public setSound(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSound:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->sound_:Ljava/lang/String;

    return-object p0
.end method

.method public setSynonymsHeader(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSynonymsHeader:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->synonymsHeader_:Ljava/lang/String;

    return-object p0
.end method

.method public setVariationType(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasVariationType:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->variationType_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasDictionaryWord()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getDictionaryWord()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasVariationType()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getVariationType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasNormalForm()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getNormalForm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPartOfSpeech()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasPronunciation()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPronunciation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSound()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSound()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPartOfSpeechMeaningList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasSynonymsHeader()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSynonymsHeader()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSynonymList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasGoogleDictionaryLink()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getGoogleDictionaryLink()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->hasAttributionLink()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getAttributionLink()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getExternalDictionaryLinkList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_b
    return-void
.end method
