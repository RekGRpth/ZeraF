.class public final Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CommonStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/CommonStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CurrencyConversionResult"
.end annotation


# instance fields
.field private baseAmount_:F

.field private baseCurrency_:Ljava/lang/String;

.field private baseSymbol_:Ljava/lang/String;

.field private cachedSize:I

.field private chartImageUrl_:Ljava/lang/String;

.field private exchangeRate_:F

.field private hasBaseAmount:Z

.field private hasBaseCurrency:Z

.field private hasBaseSymbol:Z

.field private hasChartImageUrl:Z

.field private hasExchangeRate:Z

.field private hasLhs:Z

.field private hasRhs:Z

.field private hasTargetAmount:Z

.field private hasTargetCurrency:Z

.field private hasTargetSymbol:Z

.field private lhs_:Ljava/lang/String;

.field private rhs_:Ljava/lang/String;

.field private targetAmount_:F

.field private targetCurrency_:Ljava/lang/String;

.field private targetSymbol_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseAmount_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseSymbol_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseCurrency_:Ljava/lang/String;

    iput v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->exchangeRate_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetSymbol_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetCurrency_:Ljava/lang/String;

    iput v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetAmount_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->lhs_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->rhs_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->chartImageUrl_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBaseAmount()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseAmount_:F

    return v0
.end method

.method public getBaseCurrency()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseCurrency_:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseSymbol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseSymbol_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->cachedSize:I

    return v0
.end method

.method public getChartImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->chartImageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getExchangeRate()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->exchangeRate_:F

    return v0
.end method

.method public getLhs()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->lhs_:Ljava/lang/String;

    return-object v0
.end method

.method public getRhs()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->rhs_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseAmount()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getBaseAmount()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseSymbol()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getBaseSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasExchangeRate()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getExchangeRate()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetSymbol()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getTargetSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetAmount()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getTargetAmount()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseCurrency()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getBaseCurrency()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetCurrency()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getTargetCurrency()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasChartImageUrl()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getChartImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasLhs()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getLhs()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasRhs()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getRhs()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->cachedSize:I

    return v0
.end method

.method public getTargetAmount()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetAmount_:F

    return v0
.end method

.method public getTargetCurrency()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetCurrency_:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetSymbol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetSymbol_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBaseAmount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseAmount:Z

    return v0
.end method

.method public hasBaseCurrency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseCurrency:Z

    return v0
.end method

.method public hasBaseSymbol()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseSymbol:Z

    return v0
.end method

.method public hasChartImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasChartImageUrl:Z

    return v0
.end method

.method public hasExchangeRate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasExchangeRate:Z

    return v0
.end method

.method public hasLhs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasLhs:Z

    return v0
.end method

.method public hasRhs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasRhs:Z

    return v0
.end method

.method public hasTargetAmount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetAmount:Z

    return v0
.end method

.method public hasTargetCurrency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetCurrency:Z

    return v0
.end method

.method public hasTargetSymbol()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetSymbol:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setBaseAmount(F)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setBaseSymbol(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setExchangeRate(F)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setTargetSymbol(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setTargetAmount(F)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setBaseCurrency(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setTargetCurrency(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setChartImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setLhs(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->setRhs(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2d -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    move-result-object v0

    return-object v0
.end method

.method public setBaseAmount(F)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseAmount:Z

    iput p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseAmount_:F

    return-object p0
.end method

.method public setBaseCurrency(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseCurrency:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseCurrency_:Ljava/lang/String;

    return-object p0
.end method

.method public setBaseSymbol(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseSymbol:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->baseSymbol_:Ljava/lang/String;

    return-object p0
.end method

.method public setChartImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasChartImageUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->chartImageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setExchangeRate(F)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasExchangeRate:Z

    iput p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->exchangeRate_:F

    return-object p0
.end method

.method public setLhs(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasLhs:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->lhs_:Ljava/lang/String;

    return-object p0
.end method

.method public setRhs(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasRhs:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->rhs_:Ljava/lang/String;

    return-object p0
.end method

.method public setTargetAmount(F)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetAmount:Z

    iput p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetAmount_:F

    return-object p0
.end method

.method public setTargetCurrency(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetCurrency:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetCurrency_:Ljava/lang/String;

    return-object p0
.end method

.method public setTargetSymbol(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetSymbol:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->targetSymbol_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseAmount()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getBaseAmount()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseSymbol()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getBaseSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasExchangeRate()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getExchangeRate()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetSymbol()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getTargetSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetAmount()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getTargetAmount()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasBaseCurrency()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getBaseCurrency()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasTargetCurrency()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getTargetCurrency()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasChartImageUrl()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getChartImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasLhs()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getLhs()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->hasRhs()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;->getRhs()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    return-void
.end method
