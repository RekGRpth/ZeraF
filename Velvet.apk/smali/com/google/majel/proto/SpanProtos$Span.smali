.class public final Lcom/google/majel/proto/SpanProtos$Span;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "SpanProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/SpanProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Span"
.end annotation


# instance fields
.field private alternateSpanExtension_:Lcom/google/majel/proto/SpanProtos$AlternateSpan;

.field private cachedSize:I

.field private hasAlternateSpanExtension:Z

.field private hasLength:Z

.field private hasStart:Z

.field private length_:I

.field private start_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->start_:I

    iput v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->length_:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->alternateSpanExtension_:Lcom/google/majel/proto/SpanProtos$AlternateSpan;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAlternateSpanExtension()Lcom/google/majel/proto/SpanProtos$AlternateSpan;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->alternateSpanExtension_:Lcom/google/majel/proto/SpanProtos$AlternateSpan;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->cachedSize:I

    return v0
.end method

.method public getLength()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->length_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->hasStart()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->getStart()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->hasLength()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->getLength()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->hasAlternateSpanExtension()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x19b2283

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->getAlternateSpanExtension()Lcom/google/majel/proto/SpanProtos$AlternateSpan;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->cachedSize:I

    return v0
.end method

.method public getStart()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->start_:I

    return v0
.end method

.method public hasAlternateSpanExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->hasAlternateSpanExtension:Z

    return v0
.end method

.method public hasLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->hasLength:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->hasStart:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/SpanProtos$Span;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/SpanProtos$Span;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/SpanProtos$Span;->setStart(I)Lcom/google/majel/proto/SpanProtos$Span;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/SpanProtos$Span;->setLength(I)Lcom/google/majel/proto/SpanProtos$Span;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/SpanProtos$AlternateSpan;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$AlternateSpan;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/SpanProtos$Span;->setAlternateSpanExtension(Lcom/google/majel/proto/SpanProtos$AlternateSpan;)Lcom/google/majel/proto/SpanProtos$Span;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0xcd9141a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/SpanProtos$Span;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v0

    return-object v0
.end method

.method public setAlternateSpanExtension(Lcom/google/majel/proto/SpanProtos$AlternateSpan;)Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$AlternateSpan;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->hasAlternateSpanExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/SpanProtos$Span;->alternateSpanExtension_:Lcom/google/majel/proto/SpanProtos$AlternateSpan;

    return-object p0
.end method

.method public setLength(I)Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->hasLength:Z

    iput p1, p0, Lcom/google/majel/proto/SpanProtos$Span;->length_:I

    return-object p0
.end method

.method public setStart(I)Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/SpanProtos$Span;->hasStart:Z

    iput p1, p0, Lcom/google/majel/proto/SpanProtos$Span;->start_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->getStart()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->hasLength()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->getLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->hasAlternateSpanExtension()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x19b2283

    invoke-virtual {p0}, Lcom/google/majel/proto/SpanProtos$Span;->getAlternateSpanExtension()Lcom/google/majel/proto/SpanProtos$AlternateSpan;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    return-void
.end method
