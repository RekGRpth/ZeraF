.class public final Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientInfoProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ClientInfoProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreviewParams"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasMapHeightPixels:Z

.field private hasMapWidthPixels:Z

.field private hasUrlHeightPixels:Z

.field private hasUrlPreviewType:Z

.field private hasUrlWidthPixels:Z

.field private mapHeightPixels_:I

.field private mapWidthPixels_:I

.field private urlHeightPixels_:I

.field private urlPreviewType_:I

.field private urlWidthPixels_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x190

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlWidthPixels_:I

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlHeightPixels_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlPreviewType_:I

    const/16 v0, 0x280

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->mapWidthPixels_:I

    const/16 v0, 0x140

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->mapHeightPixels_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->cachedSize:I

    return v0
.end method

.method public getMapHeightPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->mapHeightPixels_:I

    return v0
.end method

.method public getMapWidthPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->mapWidthPixels_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlWidthPixels()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getUrlWidthPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlHeightPixels()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getUrlHeightPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlPreviewType()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getUrlPreviewType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapWidthPixels()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getMapWidthPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapHeightPixels()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getMapHeightPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->cachedSize:I

    return v0
.end method

.method public getUrlHeightPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlHeightPixels_:I

    return v0
.end method

.method public getUrlPreviewType()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlPreviewType_:I

    return v0
.end method

.method public getUrlWidthPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlWidthPixels_:I

    return v0
.end method

.method public hasMapHeightPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapHeightPixels:Z

    return v0
.end method

.method public hasMapWidthPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapWidthPixels:Z

    return v0
.end method

.method public hasUrlHeightPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlHeightPixels:Z

    return v0
.end method

.method public hasUrlPreviewType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlPreviewType:Z

    return v0
.end method

.method public hasUrlWidthPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlWidthPixels:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setUrlWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setUrlHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setUrlPreviewType(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setMapWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setMapHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v0

    return-object v0
.end method

.method public setMapHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapHeightPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->mapHeightPixels_:I

    return-object p0
.end method

.method public setMapWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapWidthPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->mapWidthPixels_:I

    return-object p0
.end method

.method public setUrlHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlHeightPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlHeightPixels_:I

    return-object p0
.end method

.method public setUrlPreviewType(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlPreviewType:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlPreviewType_:I

    return-object p0
.end method

.method public setUrlWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlWidthPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->urlWidthPixels_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlWidthPixels()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getUrlWidthPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlHeightPixels()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getUrlHeightPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasUrlPreviewType()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getUrlPreviewType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapWidthPixels()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getMapWidthPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->hasMapHeightPixels()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->getMapHeightPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
