.class public final Lcom/google/majel/proto/PeanutProtos$Peanut;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PeanutProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/PeanutProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Peanut"
.end annotation


# instance fields
.field private actionResponse_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$ClientSideAction;",
            ">;"
        }
    .end annotation
.end field

.field private actionV2_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionV2;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private confidence_:F

.field private debug_:Ljava/lang/String;

.field private finalScore_:F

.field private hasConfidence:Z

.field private hasDebug:Z

.field private hasFinalScore:Z

.field private hasHighConfidenceResponse:Z

.field private hasImageResponseHeader:Z

.field private hasIsLoggable:Z

.field private hasIsQuestion:Z

.field private hasOnlyShowPeanutImageResponse:Z

.field private hasPrimaryType:Z

.field private hasSearchResultsUnnecessary:Z

.field private hasServerName:Z

.field private hasStructuredResponse:Z

.field private hasTextResponse:Z

.field private hasWebSearchType:Z

.field private highConfidenceResponse_:Z

.field private imageResponseHeader_:Ljava/lang/String;

.field private imageResponse_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$Image;",
            ">;"
        }
    .end annotation
.end field

.field private isLoggable_:Z

.field private isQuestion_:Z

.field private onlyShowPeanutImageResponse_:Z

.field private placeResponse_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/LatLngProtos$LatLng;",
            ">;"
        }
    .end annotation
.end field

.field private primaryType_:I

.field private searchResultsUnnecessary_:Z

.field private serverName_:Ljava/lang/String;

.field private structuredResponse_:Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

.field private textResponse_:Lcom/google/majel/proto/PeanutProtos$Text;

.field private urlResponse_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$Url;",
            ">;"
        }
    .end annotation
.end field

.field private videoResponse_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$Video;",
            ">;"
        }
    .end annotation
.end field

.field private webSearchType_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->serverName_:Ljava/lang/String;

    iput v2, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->finalScore_:F

    iput v2, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->confidence_:F

    iput-object v3, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->textResponse_:Lcom/google/majel/proto/PeanutProtos$Text;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponse_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponseHeader_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->urlResponse_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->placeResponse_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->videoResponse_:Ljava/util/List;

    iput-object v3, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->structuredResponse_:Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionResponse_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionV2_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->isQuestion_:Z

    iput v1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->primaryType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->webSearchType_:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->searchResultsUnnecessary_:Z

    iput-boolean v1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->highConfidenceResponse_:Z

    iput-boolean v1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->onlyShowPeanutImageResponse_:Z

    iput-boolean v1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->isLoggable_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->debug_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addActionResponse(Lcom/google/majel/proto/PeanutProtos$ClientSideAction;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionResponse_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionResponse_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionResponse_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addActionV2(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionV2_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionV2_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionV2_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addImageResponse(Lcom/google/majel/proto/PeanutProtos$Image;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponse_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponse_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponse_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPlaceResponse(Lcom/google/majel/proto/LatLngProtos$LatLng;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/LatLngProtos$LatLng;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->placeResponse_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->placeResponse_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->placeResponse_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addUrlResponse(Lcom/google/majel/proto/PeanutProtos$Url;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Url;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->urlResponse_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->urlResponse_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->urlResponse_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addVideoResponse(Lcom/google/majel/proto/PeanutProtos$Video;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Video;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->videoResponse_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->videoResponse_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->videoResponse_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearWebSearchType()Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasWebSearchType:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->webSearchType_:Ljava/lang/String;

    return-object p0
.end method

.method public getActionResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$ClientSideAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionResponse_:Ljava/util/List;

    return-object v0
.end method

.method public getActionV2(I)Lcom/google/majel/proto/ActionV2Protos$ActionV2;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionV2_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    return-object v0
.end method

.method public getActionV2Count()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionV2_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getActionV2List()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionV2;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->actionV2_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->confidence_:F

    return v0
.end method

.method public getDebug()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->debug_:Ljava/lang/String;

    return-object v0
.end method

.method public getFinalScore()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->finalScore_:F

    return v0
.end method

.method public getHighConfidenceResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->highConfidenceResponse_:Z

    return v0
.end method

.method public getImageResponse(I)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponse_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Image;

    return-object v0
.end method

.method public getImageResponseCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponse_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getImageResponseHeader()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponseHeader_:Ljava/lang/String;

    return-object v0
.end method

.method public getImageResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$Image;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponse_:Ljava/util/List;

    return-object v0
.end method

.method public getIsLoggable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->isLoggable_:Z

    return v0
.end method

.method public getIsQuestion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->isQuestion_:Z

    return v0
.end method

.method public getOnlyShowPeanutImageResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->onlyShowPeanutImageResponse_:Z

    return v0
.end method

.method public getPlaceResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/LatLngProtos$LatLng;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->placeResponse_:Ljava/util/List;

    return-object v0
.end method

.method public getPrimaryType()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->primaryType_:I

    return v0
.end method

.method public getSearchResultsUnnecessary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->searchResultsUnnecessary_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasServerName()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getServerName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasConfidence()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getConfidence()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasTextResponse()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Image;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getUrlResponseList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Url;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getPlaceResponseList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/LatLngProtos$LatLng;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsQuestion()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getIsQuestion()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasPrimaryType()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getPrimaryType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasDebug()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getDebug()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionResponseList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    const/16 v3, 0xb

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_3

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasFinalScore()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getFinalScore()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasStructuredResponse()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getVideoResponseList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Video;

    const/16 v3, 0xe

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_4

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2List()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    const/16 v3, 0xf

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_5

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasWebSearchType()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getWebSearchType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasSearchResultsUnnecessary()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getSearchResultsUnnecessary()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasHighConfidenceResponse()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getHighConfidenceResponse()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsLoggable()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getIsLoggable()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasOnlyShowPeanutImageResponse()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getOnlyShowPeanutImageResponse()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasImageResponseHeader()Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseHeader()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_13
    iput v2, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->cachedSize:I

    return v2
.end method

.method public getServerName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->serverName_:Ljava/lang/String;

    return-object v0
.end method

.method public getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->structuredResponse_:Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    return-object v0
.end method

.method public getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->textResponse_:Lcom/google/majel/proto/PeanutProtos$Text;

    return-object v0
.end method

.method public getUrlResponse(I)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->urlResponse_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Url;

    return-object v0
.end method

.method public getUrlResponseCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->urlResponse_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getUrlResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$Url;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->urlResponse_:Ljava/util/List;

    return-object v0
.end method

.method public getVideoResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/PeanutProtos$Video;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->videoResponse_:Ljava/util/List;

    return-object v0
.end method

.method public getWebSearchType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->webSearchType_:Ljava/lang/String;

    return-object v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasConfidence:Z

    return v0
.end method

.method public hasDebug()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasDebug:Z

    return v0
.end method

.method public hasFinalScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasFinalScore:Z

    return v0
.end method

.method public hasHighConfidenceResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasHighConfidenceResponse:Z

    return v0
.end method

.method public hasImageResponseHeader()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasImageResponseHeader:Z

    return v0
.end method

.method public hasIsLoggable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsLoggable:Z

    return v0
.end method

.method public hasIsQuestion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsQuestion:Z

    return v0
.end method

.method public hasOnlyShowPeanutImageResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasOnlyShowPeanutImageResponse:Z

    return v0
.end method

.method public hasPrimaryType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasPrimaryType:Z

    return v0
.end method

.method public hasSearchResultsUnnecessary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasSearchResultsUnnecessary:Z

    return v0
.end method

.method public hasServerName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasServerName:Z

    return v0
.end method

.method public hasStructuredResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasStructuredResponse:Z

    return v0
.end method

.method public hasTextResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasTextResponse:Z

    return v0
.end method

.method public hasWebSearchType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasWebSearchType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setServerName(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setConfidence(F)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Text;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Text;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setTextResponse(Lcom/google/majel/proto/PeanutProtos$Text;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Image;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addImageResponse(Lcom/google/majel/proto/PeanutProtos$Image;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Url;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addUrlResponse(Lcom/google/majel/proto/PeanutProtos$Url;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/LatLngProtos$LatLng;

    invoke-direct {v1}, Lcom/google/majel/proto/LatLngProtos$LatLng;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addPlaceResponse(Lcom/google/majel/proto/LatLngProtos$LatLng;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setIsQuestion(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setPrimaryType(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setDebug(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addActionResponse(Lcom/google/majel/proto/PeanutProtos$ClientSideAction;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setFinalScore(F)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    invoke-direct {v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setStructuredResponse(Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Video;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Video;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addVideoResponse(Lcom/google/majel/proto/PeanutProtos$Video;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addActionV2(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setWebSearchType(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setSearchResultsUnnecessary(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setHighConfidenceResponse(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setIsLoggable(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setOnlyShowPeanutImageResponse(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setImageResponseHeader(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x65 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x88 -> :sswitch_10
        0x90 -> :sswitch_11
        0x98 -> :sswitch_12
        0xa0 -> :sswitch_13
        0xaa -> :sswitch_14
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v0

    return-object v0
.end method

.method public setConfidence(F)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasConfidence:Z

    iput p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->confidence_:F

    return-object p0
.end method

.method public setDebug(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasDebug:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->debug_:Ljava/lang/String;

    return-object p0
.end method

.method public setFinalScore(F)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasFinalScore:Z

    iput p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->finalScore_:F

    return-object p0
.end method

.method public setHighConfidenceResponse(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasHighConfidenceResponse:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->highConfidenceResponse_:Z

    return-object p0
.end method

.method public setImageResponseHeader(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasImageResponseHeader:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->imageResponseHeader_:Ljava/lang/String;

    return-object p0
.end method

.method public setIsLoggable(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsLoggable:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->isLoggable_:Z

    return-object p0
.end method

.method public setIsQuestion(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsQuestion:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->isQuestion_:Z

    return-object p0
.end method

.method public setOnlyShowPeanutImageResponse(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasOnlyShowPeanutImageResponse:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->onlyShowPeanutImageResponse_:Z

    return-object p0
.end method

.method public setPrimaryType(I)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasPrimaryType:Z

    iput p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->primaryType_:I

    return-object p0
.end method

.method public setSearchResultsUnnecessary(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasSearchResultsUnnecessary:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->searchResultsUnnecessary_:Z

    return-object p0
.end method

.method public setServerName(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasServerName:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->serverName_:Ljava/lang/String;

    return-object p0
.end method

.method public setStructuredResponse(Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasStructuredResponse:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->structuredResponse_:Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    return-object p0
.end method

.method public setTextResponse(Lcom/google/majel/proto/PeanutProtos$Text;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Text;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasTextResponse:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->textResponse_:Lcom/google/majel/proto/PeanutProtos$Text;

    return-object p0
.end method

.method public setWebSearchType(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasWebSearchType:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Peanut;->webSearchType_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasServerName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getServerName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasConfidence()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasTextResponse()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Image;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getUrlResponseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Url;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getPlaceResponseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/LatLngProtos$LatLng;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsQuestion()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getIsQuestion()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasPrimaryType()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getPrimaryType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasDebug()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getDebug()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionResponseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_3

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasFinalScore()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getFinalScore()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasStructuredResponse()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getVideoResponseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Video;

    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_4

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2List()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_5

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasWebSearchType()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getWebSearchType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasSearchResultsUnnecessary()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getSearchResultsUnnecessary()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasHighConfidenceResponse()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getHighConfidenceResponse()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasIsLoggable()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getIsLoggable()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasOnlyShowPeanutImageResponse()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getOnlyShowPeanutImageResponse()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasImageResponseHeader()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseHeader()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_13
    return-void
.end method
