.class public final Lcom/google/majel/proto/PeanutProtos$Url;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PeanutProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/PeanutProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Url"
.end annotation


# instance fields
.field private cachedSize:I

.field private cookie_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CookieProtos$MajelCookie;",
            ">;"
        }
    .end annotation
.end field

.field private displayLink_:Ljava/lang/String;

.field private hasDisplayLink:Z

.field private hasHtml:Z

.field private hasLink:Z

.field private hasRenderedLink:Z

.field private hasRenderedPage:Z

.field private hasSearchResultsInfo:Z

.field private hasTitle:Z

.field private html_:Ljava/lang/String;

.field private link_:Ljava/lang/String;

.field private renderedLink_:Ljava/lang/String;

.field private renderedPage_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private searchResultsInfo_:Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

.field private title_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->link_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->displayLink_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->renderedLink_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->renderedPage_:Lcom/google/protobuf/micro/ByteStringMicro;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->html_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->searchResultsInfo_:Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cookie_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addCookie(Lcom/google/majel/proto/CookieProtos$MajelCookie;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CookieProtos$MajelCookie;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cookie_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cookie_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cookie_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cachedSize:I

    return v0
.end method

.method public getCookieList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CookieProtos$MajelCookie;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cookie_:Ljava/util/List;

    return-object v0
.end method

.method public getDisplayLink()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->displayLink_:Ljava/lang/String;

    return-object v0
.end method

.method public getHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->html_:Ljava/lang/String;

    return-object v0
.end method

.method public getLink()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->link_:Ljava/lang/String;

    return-object v0
.end method

.method public getRenderedLink()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->renderedLink_:Ljava/lang/String;

    return-object v0
.end method

.method public getRenderedPage()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->renderedPage_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getSearchResultsInfo()Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->searchResultsInfo_:Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasLink()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getLink()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedLink()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getRenderedLink()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedPage()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getRenderedPage()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasTitle()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasHtml()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getHtml()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasSearchResultsInfo()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getSearchResultsInfo()Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasDisplayLink()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getDisplayLink()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getCookieList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/CookieProtos$MajelCookie;

    const/16 v3, 0x8

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_7
    iput v2, p0, Lcom/google/majel/proto/PeanutProtos$Url;->cachedSize:I

    return v2
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDisplayLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasDisplayLink:Z

    return v0
.end method

.method public hasHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasHtml:Z

    return v0
.end method

.method public hasLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasLink:Z

    return v0
.end method

.method public hasRenderedLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedLink:Z

    return v0
.end method

.method public hasRenderedPage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedPage:Z

    return v0
.end method

.method public hasSearchResultsInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasSearchResultsInfo:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/PeanutProtos$Url;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setRenderedLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setRenderedPage(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setTitle(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setHtml(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Url;->setSearchResultsInfo(Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setDisplayLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/majel/proto/CookieProtos$MajelCookie;

    invoke-direct {v1}, Lcom/google/majel/proto/CookieProtos$MajelCookie;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Url;->addCookie(Lcom/google/majel/proto/CookieProtos$MajelCookie;)Lcom/google/majel/proto/PeanutProtos$Url;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/PeanutProtos$Url;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v0

    return-object v0
.end method

.method public setDisplayLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasDisplayLink:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Url;->displayLink_:Ljava/lang/String;

    return-object p0
.end method

.method public setHtml(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasHtml:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Url;->html_:Ljava/lang/String;

    return-object p0
.end method

.method public setLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasLink:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Url;->link_:Ljava/lang/String;

    return-object p0
.end method

.method public setRenderedLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedLink:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Url;->renderedLink_:Ljava/lang/String;

    return-object p0
.end method

.method public setRenderedPage(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedPage:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Url;->renderedPage_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setSearchResultsInfo(Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasSearchResultsInfo:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Url;->searchResultsInfo_:Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Url;->hasTitle:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Url;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasLink()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getLink()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedLink()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getRenderedLink()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasRenderedPage()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getRenderedPage()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasHtml()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasSearchResultsInfo()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getSearchResultsInfo()Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->hasDisplayLink()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getDisplayLink()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Url;->getCookieList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/CookieProtos$MajelCookie;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_7
    return-void
.end method
