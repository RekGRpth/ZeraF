.class public final Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EarsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/audio/ears/proto/EarsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EarsResultsResponse"
.end annotation


# instance fields
.field private cachedSize:I

.field private detectedCountryCode_:Ljava/lang/String;

.field private hasDetectedCountryCode:Z

.field private hasStatusCode:Z

.field private result_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$EarsResult;",
            ">;"
        }
    .end annotation
.end field

.field private statusCode_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->result_:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->statusCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->detectedCountryCode_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addResult(Lcom/google/audio/ears/proto/EarsService$EarsResult;)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->result_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->result_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->result_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->cachedSize:I

    return v0
.end method

.method public getDetectedCountryCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->detectedCountryCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getResultCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->result_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getResultList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$EarsResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->result_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getResultList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$EarsResult;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasStatusCode()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getStatusCode()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasDetectedCountryCode()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getDetectedCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    iput v2, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->cachedSize:I

    return v2
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->statusCode_:I

    return v0
.end method

.method public hasDetectedCountryCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasDetectedCountryCode:Z

    return v0
.end method

.method public hasStatusCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasStatusCode:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$EarsResult;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->addResult(Lcom/google/audio/ears/proto/EarsService$EarsResult;)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->setStatusCode(I)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->setDetectedCountryCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    move-result-object v0

    return-object v0
.end method

.method public setDetectedCountryCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasDetectedCountryCode:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->detectedCountryCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setStatusCode(I)Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasStatusCode:Z

    iput p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->statusCode_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getResultList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$EarsResult;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasStatusCode()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getStatusCode()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->hasDetectedCountryCode()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getDetectedCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    return-void
.end method
