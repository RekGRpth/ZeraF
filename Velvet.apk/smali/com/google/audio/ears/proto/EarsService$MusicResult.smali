.class public final Lcom/google/audio/ears/proto/EarsService$MusicResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EarsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/audio/ears/proto/EarsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MusicResult"
.end annotation


# instance fields
.field private albumArtUrl_:Ljava/lang/String;

.field private albumArt_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private album_:Ljava/lang/String;

.field private artistId_:Ljava/lang/String;

.field private artist_:Ljava/lang/String;

.field private cachedSize:I

.field private hasAlbum:Z

.field private hasAlbumArt:Z

.field private hasAlbumArtUrl:Z

.field private hasArtist:Z

.field private hasArtistId:Z

.field private hasIsExplicit:Z

.field private hasIsrc:Z

.field private hasLabelCode:Z

.field private hasPopularityScore:Z

.field private hasPrerelease:Z

.field private hasSignedInAlbumArtUrl:Z

.field private hasTrack:Z

.field private isExplicit_:Z

.field private isrc_:Ljava/lang/String;

.field private labelCode_:Ljava/lang/String;

.field private offer_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$ProductOffer;",
            ">;"
        }
    .end annotation
.end field

.field private popularityScore_:D

.field private prerelease_:Z

.field private signedInAlbumArtUrl_:Ljava/lang/String;

.field private track_:Ljava/lang/String;

.field private video_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->artist_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->artistId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->track_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->album_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->isExplicit_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->isrc_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->labelCode_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->video_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->offer_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->albumArt_:Lcom/google/protobuf/micro/ByteStringMicro;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->albumArtUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->signedInAlbumArtUrl_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->popularityScore_:D

    iput-boolean v2, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->prerelease_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addOffer(Lcom/google/audio/ears/proto/EarsService$ProductOffer;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->offer_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->offer_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->offer_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addVideo(Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->video_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->video_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->video_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAlbum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->album_:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumArt()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->albumArt_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getAlbumArtUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->albumArtUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->artist_:Ljava/lang/String;

    return-object v0
.end method

.method public getArtistId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->artistId_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->cachedSize:I

    return v0
.end method

.method public getIsExplicit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->isExplicit_:Z

    return v0
.end method

.method public getIsrc()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->isrc_:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->labelCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getOfferList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$ProductOffer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->offer_:Ljava/util/List;

    return-object v0
.end method

.method public getPopularityScore()D
    .locals 2

    iget-wide v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->popularityScore_:D

    return-wide v0
.end method

.method public getPrerelease()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->prerelease_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtist()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtist()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasTrack()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getTrack()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbum()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbum()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsrc()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getIsrc()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getVideoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getOfferList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArt()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbumArt()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasLabelCode()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getLabelCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArtUrl()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbumArtUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPopularityScore()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getPopularityScore()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasSignedInAlbumArtUrl()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getSignedInAlbumArtUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtistId()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtistId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPrerelease()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getPrerelease()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsExplicit()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getIsExplicit()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    iput v2, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->cachedSize:I

    return v2
.end method

.method public getSignedInAlbumArtUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->signedInAlbumArtUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->track_:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->video_:Ljava/util/List;

    return-object v0
.end method

.method public hasAlbum()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbum:Z

    return v0
.end method

.method public hasAlbumArt()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArt:Z

    return v0
.end method

.method public hasAlbumArtUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArtUrl:Z

    return v0
.end method

.method public hasArtist()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtist:Z

    return v0
.end method

.method public hasArtistId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtistId:Z

    return v0
.end method

.method public hasIsExplicit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsExplicit:Z

    return v0
.end method

.method public hasIsrc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsrc:Z

    return v0
.end method

.method public hasLabelCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasLabelCode:Z

    return v0
.end method

.method public hasPopularityScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPopularityScore:Z

    return v0
.end method

.method public hasPrerelease()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPrerelease:Z

    return v0
.end method

.method public hasSignedInAlbumArtUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasSignedInAlbumArtUrl:Z

    return v0
.end method

.method public hasTrack()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasTrack:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setArtist(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setTrack(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setAlbum(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setIsrc(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->addVideo(Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->addOffer(Lcom/google/audio/ears/proto/EarsService$ProductOffer;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setAlbumArt(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setLabelCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setAlbumArtUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setPopularityScore(D)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setSignedInAlbumArtUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setArtistId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setPrerelease(Z)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setIsExplicit(Z)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x51 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    move-result-object v0

    return-object v0
.end method

.method public setAlbum(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbum:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->album_:Ljava/lang/String;

    return-object p0
.end method

.method public setAlbumArt(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArt:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->albumArt_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setAlbumArtUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArtUrl:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->albumArtUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setArtist(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtist:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->artist_:Ljava/lang/String;

    return-object p0
.end method

.method public setArtistId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtistId:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->artistId_:Ljava/lang/String;

    return-object p0
.end method

.method public setIsExplicit(Z)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsExplicit:Z

    iput-boolean p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->isExplicit_:Z

    return-object p0
.end method

.method public setIsrc(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsrc:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->isrc_:Ljava/lang/String;

    return-object p0
.end method

.method public setLabelCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasLabelCode:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->labelCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setPopularityScore(D)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPopularityScore:Z

    iput-wide p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->popularityScore_:D

    return-object p0
.end method

.method public setPrerelease(Z)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPrerelease:Z

    iput-boolean p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->prerelease_:Z

    return-object p0
.end method

.method public setSignedInAlbumArtUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasSignedInAlbumArtUrl:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->signedInAlbumArtUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setTrack(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasTrack:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$MusicResult;->track_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtist()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtist()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasTrack()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getTrack()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbum()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbum()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsrc()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getIsrc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getVideoList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$YouTubeVideo;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getOfferList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArt()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbumArt()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasLabelCode()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getLabelCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArtUrl()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbumArtUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPopularityScore()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getPopularityScore()D

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasSignedInAlbumArtUrl()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getSignedInAlbumArtUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtistId()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtistId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasPrerelease()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getPrerelease()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasIsExplicit()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getIsExplicit()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_d
    return-void
.end method
