.class public final Lcom/google/audio/ears/proto/EarsService$TvResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EarsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/audio/ears/proto/EarsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TvResult"
.end annotation


# instance fields
.field private cachedScreenshot_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private cachedSize:I

.field private channelId_:Ljava/lang/String;

.field private channelLogoUrl_:Ljava/lang/String;

.field private channelName_:Ljava/lang/String;

.field private hasCachedScreenshot:Z

.field private hasChannelId:Z

.field private hasChannelLogoUrl:Z

.field private hasChannelName:Z

.field private hasProgramEndMs:Z

.field private hasProgramId:Z

.field private hasProgramImageUrl:Z

.field private hasProgramSecondaryTitle:Z

.field private hasProgramStartMs:Z

.field private hasProgramSynopsis:Z

.field private hasProgramTitle:Z

.field private programEndMs_:J

.field private programId_:Ljava/lang/String;

.field private programImageUrl_:Ljava/lang/String;

.field private programSecondaryTitle_:Ljava/lang/String;

.field private programStartMs_:J

.field private programSynopsis_:Ljava/lang/String;

.field private programTitle_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelLogoUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programTitle_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programSecondaryTitle_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programImageUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programSynopsis_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programStartMs_:J

    iput-wide v1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programEndMs_:J

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->cachedScreenshot_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedScreenshot()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->cachedScreenshot_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->cachedSize:I

    return v0
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelId_:Ljava/lang/String;

    return-object v0
.end method

.method public getChannelLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelLogoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getChannelName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelName_:Ljava/lang/String;

    return-object v0
.end method

.method public getProgramEndMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programEndMs_:J

    return-wide v0
.end method

.method public getProgramId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programId_:Ljava/lang/String;

    return-object v0
.end method

.method public getProgramImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programImageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getProgramSecondaryTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programSecondaryTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getProgramStartMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programStartMs_:J

    return-wide v0
.end method

.method public getProgramSynopsis()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programSynopsis_:Ljava/lang/String;

    return-object v0
.end method

.method public getProgramTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getChannelId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelName()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getChannelName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelLogoUrl()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getChannelLogoUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramTitle()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramImageUrl()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSynopsis()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramSynopsis()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramStartMs()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramStartMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramEndMs()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramEndMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasCachedScreenshot()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getCachedScreenshot()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSecondaryTitle()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramSecondaryTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramId()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->cachedSize:I

    return v0
.end method

.method public hasCachedScreenshot()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasCachedScreenshot:Z

    return v0
.end method

.method public hasChannelId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelId:Z

    return v0
.end method

.method public hasChannelLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelLogoUrl:Z

    return v0
.end method

.method public hasChannelName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelName:Z

    return v0
.end method

.method public hasProgramEndMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramEndMs:Z

    return v0
.end method

.method public hasProgramId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramId:Z

    return v0
.end method

.method public hasProgramImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramImageUrl:Z

    return v0
.end method

.method public hasProgramSecondaryTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSecondaryTitle:Z

    return v0
.end method

.method public hasProgramStartMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramStartMs:Z

    return v0
.end method

.method public hasProgramSynopsis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSynopsis:Z

    return v0
.end method

.method public hasProgramTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setChannelId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setChannelName(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setChannelLogoUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setProgramTitle(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setProgramImageUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setProgramSynopsis(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setProgramStartMs(J)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setProgramEndMs(J)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setCachedScreenshot(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setProgramSecondaryTitle(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->setProgramId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/audio/ears/proto/EarsService$TvResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$TvResult;

    move-result-object v0

    return-object v0
.end method

.method public setCachedScreenshot(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasCachedScreenshot:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->cachedScreenshot_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setChannelId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelId:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelId_:Ljava/lang/String;

    return-object p0
.end method

.method public setChannelLogoUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelLogoUrl:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelLogoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setChannelName(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelName:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->channelName_:Ljava/lang/String;

    return-object p0
.end method

.method public setProgramEndMs(J)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramEndMs:Z

    iput-wide p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programEndMs_:J

    return-object p0
.end method

.method public setProgramId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramId:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programId_:Ljava/lang/String;

    return-object p0
.end method

.method public setProgramImageUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramImageUrl:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programImageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setProgramSecondaryTitle(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSecondaryTitle:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programSecondaryTitle_:Ljava/lang/String;

    return-object p0
.end method

.method public setProgramStartMs(J)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramStartMs:Z

    iput-wide p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programStartMs_:J

    return-object p0
.end method

.method public setProgramSynopsis(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSynopsis:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programSynopsis_:Ljava/lang/String;

    return-object p0
.end method

.method public setProgramTitle(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramTitle:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$TvResult;->programTitle_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getChannelId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelName()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getChannelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasChannelLogoUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getChannelLogoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramTitle()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramImageUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSynopsis()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramSynopsis()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramStartMs()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramStartMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramEndMs()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramEndMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasCachedScreenshot()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getCachedScreenshot()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramSecondaryTitle()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramSecondaryTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->hasProgramId()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$TvResult;->getProgramId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    return-void
.end method
