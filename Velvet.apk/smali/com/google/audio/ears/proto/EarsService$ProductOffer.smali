.class public final Lcom/google/audio/ears/proto/EarsService$ProductOffer;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EarsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/audio/ears/proto/EarsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductOffer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private hasIdentifier:Z

.field private hasParentIdentifier:Z

.field private hasPreviewUrl:Z

.field private hasUrl:Z

.field private hasVendor:Z

.field private identifier_:Ljava/lang/String;

.field private parentIdentifier_:Ljava/lang/String;

.field private parentPricingInfo_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private previewUrl_:Ljava/lang/String;

.field private pricingInfo_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private url_:Ljava/lang/String;

.field private vendor_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->vendor_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->identifier_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentIdentifier_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->pricingInfo_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentPricingInfo_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->url_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->previewUrl_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addParentPricingInfo(Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentPricingInfo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentPricingInfo_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentPricingInfo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPricingInfo(Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->pricingInfo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->pricingInfo_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->pricingInfo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->cachedSize:I

    return v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->identifier_:Ljava/lang/String;

    return-object v0
.end method

.method public getParentIdentifier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentIdentifier_:Ljava/lang/String;

    return-object v0
.end method

.method public getParentPricingInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentPricingInfo_:Ljava/util/List;

    return-object v0
.end method

.method public getPreviewUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->previewUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getPricingInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->pricingInfo_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasVendor()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getVendor()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasIdentifier()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasParentIdentifier()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getParentIdentifier()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getPricingInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasUrl()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getParentPricingInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasPreviewUrl()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getPreviewUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    iput v2, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->cachedSize:I

    return v2
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public getVendor()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->vendor_:I

    return v0
.end method

.method public hasIdentifier()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasIdentifier:Z

    return v0
.end method

.method public hasParentIdentifier()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasParentIdentifier:Z

    return v0
.end method

.method public hasPreviewUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasPreviewUrl:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasUrl:Z

    return v0
.end method

.method public hasVendor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasVendor:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->setVendor(I)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->setIdentifier(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->setParentIdentifier(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->addPricingInfo(Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->setUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->addParentPricingInfo(Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->setPreviewUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    move-result-object v0

    return-object v0
.end method

.method public setIdentifier(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasIdentifier:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->identifier_:Ljava/lang/String;

    return-object p0
.end method

.method public setParentIdentifier(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasParentIdentifier:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->parentIdentifier_:Ljava/lang/String;

    return-object p0
.end method

.method public setPreviewUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasPreviewUrl:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->previewUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasUrl:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->url_:Ljava/lang/String;

    return-object p0
.end method

.method public setVendor(I)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasVendor:Z

    iput p1, p0, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->vendor_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasVendor()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getVendor()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasIdentifier()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasParentIdentifier()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getParentIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getPricingInfoList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getParentPricingInfoList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasPreviewUrl()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getPreviewUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    return-void
.end method
