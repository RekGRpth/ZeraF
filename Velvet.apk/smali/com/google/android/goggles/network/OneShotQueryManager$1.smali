.class Lcom/google/android/goggles/network/OneShotQueryManager$1;
.super Ljava/lang/Object;
.source "OneShotQueryManager.java"

# interfaces
.implements Lcom/google/android/goggles/network/QueryManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/network/OneShotQueryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/network/OneShotQueryManager;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/network/OneShotQueryManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 0

    return-void
.end method

.method public onConnectionError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$100(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/GogglesController$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$Callback;->onError()V

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$100(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/GogglesController$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$Callback;->onError()V

    return-void
.end method

.method public onNewSession(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "goggles.OneShotQueryManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New session: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$000(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultRanker;->onNewSession(Ljava/lang/String;)V

    return-void
.end method

.method public onQuerySent(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$000(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultRanker;->onQuerySent(I)V

    return-void
.end method

.method public onReconnected()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegData:[B
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$200(Lcom/google/android/goggles/network/OneShotQueryManager;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$400(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/network/QueryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegData:[B
    invoke-static {v1}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$200(Lcom/google/android/goggles/network/OneShotQueryManager;)[B

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mTextRestrict:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$300(Lcom/google/android/goggles/network/OneShotQueryManager;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/goggles/network/QueryManager;->query([BILjava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$1;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$000(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultRanker;->onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V

    return-void
.end method
