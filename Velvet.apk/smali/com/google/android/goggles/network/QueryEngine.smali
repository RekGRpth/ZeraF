.class public Lcom/google/android/goggles/network/QueryEngine;
.super Ljava/lang/Object;
.source "QueryEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/network/QueryEngine$Listener;,
        Lcom/google/android/goggles/network/QueryEngine$ServerMessages;,
        Lcom/google/android/goggles/network/QueryEngine$State;
    }
.end annotation


# instance fields
.field private mConnection:Lcom/google/android/speech/network/S3Connection;

.field private final mFactory:Lcom/google/android/velvet/VelvetFactory;

.field private final mListener:Lcom/google/android/goggles/network/QueryEngine$Listener;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field protected final mServerMessages:Lcom/google/android/goggles/network/QueryEngine$ServerMessages;

.field private final mSettings:Lcom/google/android/goggles/GogglesSettings;

.field private final mState:Lcom/google/android/searchcommon/util/StateMachine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<",
            "Lcom/google/android/goggles/network/QueryEngine$State;",
            ">;"
        }
    .end annotation
.end field

.field private final mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method public constructor <init>(Lcom/google/android/goggles/network/QueryEngine$Listener;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 4
    .param p1    # Lcom/google/android/goggles/network/QueryEngine$Listener;
    .param p2    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p3    # Lcom/google/android/velvet/VelvetFactory;
    .param p4    # Lcom/google/android/goggles/GogglesSettings;
    .param p5    # Lcom/google/android/velvet/presenter/QueryState;

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "goggles.QueryEngine"

    sget-object v1, Lcom/google/android/goggles/network/QueryEngine$State;->DISCONNECTED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->newBuilder(Ljava/lang/String;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/network/QueryEngine$State;->DISCONNECTED:Lcom/google/android/goggles/network/QueryEngine$State;

    sget-object v2, Lcom/google/android/goggles/network/QueryEngine$State;->INITIALIZED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/network/QueryEngine$State;->INITIALIZED:Lcom/google/android/goggles/network/QueryEngine$State;

    sget-object v2, Lcom/google/android/goggles/network/QueryEngine$State;->DISCONNECTED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/network/QueryEngine$State;->INITIALIZED:Lcom/google/android/goggles/network/QueryEngine$State;

    sget-object v2, Lcom/google/android/goggles/network/QueryEngine$State;->FINISHED_UPLOADING:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/network/QueryEngine$State;->FINISHED_UPLOADING:Lcom/google/android/goggles/network/QueryEngine$State;

    sget-object v2, Lcom/google/android/goggles/network/QueryEngine$State;->DISCONNECTED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setSingleThreadOnly(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setStrictMode(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->build()Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    new-instance v0, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;-><init>(Lcom/google/android/goggles/network/QueryEngine;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mServerMessages:Lcom/google/android/goggles/network/QueryEngine$ServerMessages;

    iput-object p1, p0, Lcom/google/android/goggles/network/QueryEngine;->mListener:Lcom/google/android/goggles/network/QueryEngine$Listener;

    iput-object p2, p0, Lcom/google/android/goggles/network/QueryEngine;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    iput-object p3, p0, Lcom/google/android/goggles/network/QueryEngine;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p4, p0, Lcom/google/android/goggles/network/QueryEngine;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    iput-object p5, p0, Lcom/google/android/goggles/network/QueryEngine;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/network/QueryEngine;)Lcom/google/android/goggles/network/QueryEngine$Listener;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/QueryEngine;

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mListener:Lcom/google/android/goggles/network/QueryEngine$Listener;

    return-object v0
.end method

.method private static createGogglesRequest(Lcom/google/android/speech/params/RecognizerParams;)Lcom/google/speech/s3/S3$S3Request;
    .locals 6
    .param p0    # Lcom/google/android/speech/params/RecognizerParams;

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParams;->getService()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/speech/s3/S3$S3Request;->setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParams;->waitForS3ClientInfo()Lcom/google/speech/s3/S3$S3ClientInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2, v0}, Lcom/google/speech/s3/S3$S3Request;->setS3ClientInfoExtension(Lcom/google/speech/s3/S3$S3ClientInfo;)Lcom/google/speech/s3/S3$S3Request;

    new-instance v4, Lcom/google/speech/s3/S3$S3SessionInfo;

    invoke-direct {v4}, Lcom/google/speech/s3/S3$S3SessionInfo;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParams;->getRequestId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/speech/s3/S3$S3SessionInfo;->setSessionId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3SessionInfo;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/speech/s3/S3$S3Request;->setS3SessionInfoExtension(Lcom/google/speech/s3/S3$S3SessionInfo;)Lcom/google/speech/s3/S3$S3Request;

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParams;->waitForS3UserInfo()Lcom/google/speech/s3/S3$S3UserInfo;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParams;->waitForMobileUserInfo()Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    move-result-object v1

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Lcom/google/speech/s3/S3$S3Request;->setS3UserInfoExtension(Lcom/google/speech/s3/S3$S3UserInfo;)Lcom/google/speech/s3/S3$S3Request;

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Lcom/google/speech/s3/S3$S3Request;->setMobileUserInfoExtension(Lcom/google/speech/s3/MobileUser$MobileUserInfo;)Lcom/google/speech/s3/S3$S3Request;

    :cond_1
    :goto_0
    return-object v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public connect(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V
    .locals 8
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/io/IOException;

    const-string v6, "Current query is not a goggles query."

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v6, Lcom/google/android/goggles/network/QueryEngine$State;->DISCONNECTED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v5, v6}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    const-string v5, "goggles.QueryEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Connecting to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;->getDown()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/goggles/network/QueryEngine;->getRecognizerParams()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetFactory;->createGogglesConnectionFactory()Lcom/google/android/speech/network/ConnectionFactory;

    move-result-object v1

    new-instance v5, Lcom/google/android/speech/network/PairHttpConnection;

    invoke-direct {v5, p1, v1}, Lcom/google/android/speech/network/PairHttpConnection;-><init>(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Lcom/google/android/speech/network/ConnectionFactory;)V

    iput-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mConnection:Lcom/google/android/speech/network/S3Connection;

    new-instance v4, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    invoke-direct {v4}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->setCanLogImage(Z)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->setCanLogLocation(Z)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v5}, Lcom/google/android/goggles/GogglesSettings;->getAnnotation()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v5}, Lcom/google/android/goggles/GogglesSettings;->getAnnotation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->setAnnotation(Ljava/lang/String;)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    :cond_1
    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/Query;->getGogglesDisclosedCapability()I

    move-result v0

    const/4 v5, -0x1

    if-eq v0, v5, :cond_2

    invoke-virtual {v4, v0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->addDisclosedCapabilities(I)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    :cond_2
    invoke-static {v3}, Lcom/google/android/goggles/network/QueryEngine;->createGogglesRequest(Lcom/google/android/speech/params/RecognizerParams;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v5, Ljava/io/IOException;

    const-string v6, "Unable to create request"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3
    invoke-virtual {v2, v4}, Lcom/google/speech/s3/S3$S3Request;->setGogglesS3SessionOptionsExtension(Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;)Lcom/google/speech/s3/S3$S3Request;

    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mConnection:Lcom/google/android/speech/network/S3Connection;

    iget-object v6, p0, Lcom/google/android/goggles/network/QueryEngine;->mServerMessages:Lcom/google/android/goggles/network/QueryEngine$ServerMessages;

    invoke-interface {v5, v6, v2}, Lcom/google/android/speech/network/S3Connection;->connect(Lcom/google/android/speech/callback/Callback;Lcom/google/speech/s3/S3$S3Request;)V

    iget-object v5, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v6, Lcom/google/android/goggles/network/QueryEngine$State;->INITIALIZED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v5, v6}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    return-void
.end method

.method public disconnect()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/goggles/network/QueryEngine$State;->DISCONNECTED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "goggles.QueryEngine"

    const-string v1, "Ignoring disconnect request: Already disconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mConnection:Lcom/google/android/speech/network/S3Connection;

    invoke-interface {v0}, Lcom/google/android/speech/network/S3Connection;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mConnection:Lcom/google/android/speech/network/S3Connection;

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/goggles/network/QueryEngine$State;->DISCONNECTED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    goto :goto_0
.end method

.method getRecognizerParams()Lcom/google/android/speech/params/RecognizerParams;
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryEngine;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizerParamsFactory()Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    const-string v1, "en-US"

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->GOGGLES:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    return-object v0
.end method

.method public sendLastRequest()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/goggles/network/QueryEngine$State;->INITIALIZED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    new-instance v0, Lcom/google/speech/s3/S3$S3Request;

    invoke-direct {v0}, Lcom/google/speech/s3/S3$S3Request;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setEndOfData(Z)Lcom/google/speech/s3/S3$S3Request;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine;->mConnection:Lcom/google/android/speech/network/S3Connection;

    invoke-interface {v1, v0}, Lcom/google/android/speech/network/S3Connection;->send(Lcom/google/speech/s3/S3$S3Request;)V

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/goggles/network/QueryEngine$State;->FINISHED_UPLOADING:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    return-void
.end method

.method public sendQuery(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)V
    .locals 3
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/goggles/network/QueryEngine$State;->INITIALIZED:Lcom/google/android/goggles/network/QueryEngine$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    new-instance v0, Lcom/google/speech/s3/S3$S3Request;

    invoke-direct {v0}, Lcom/google/speech/s3/S3$S3Request;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/speech/s3/S3$S3Request;->setGogglesStreamRequestExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)Lcom/google/speech/s3/S3$S3Request;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine;->mConnection:Lcom/google/android/speech/network/S3Connection;

    invoke-interface {v1, v0}, Lcom/google/android/speech/network/S3Connection;->send(Lcom/google/speech/s3/S3$S3Request;)V

    return-void
.end method
