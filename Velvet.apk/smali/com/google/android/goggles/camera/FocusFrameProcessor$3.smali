.class Lcom/google/android/goggles/camera/FocusFrameProcessor$3;
.super Ljava/lang/Object;
.source "FocusFrameProcessor.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusMoveCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/FocusFrameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$3;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoFocusMoving(ZLandroid/hardware/Camera;)V
    .locals 1
    .param p1    # Z
    .param p2    # Landroid/hardware/Camera;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$3;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # invokes: Lcom/google/android/goggles/camera/FocusFrameProcessor;->onFocusMoving(Z)V
    invoke-static {v0, p1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$000(Lcom/google/android/goggles/camera/FocusFrameProcessor;Z)V

    return-void
.end method
