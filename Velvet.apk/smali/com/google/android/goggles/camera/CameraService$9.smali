.class Lcom/google/android/goggles/camera/CameraService$9;
.super Ljava/lang/Object;
.source "CameraService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraService;->cancelAutoFocus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraService;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraService$9;->this$0:Lcom/google/android/goggles/camera/CameraService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$9;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraService;->access$800(Lcom/google/android/goggles/camera/CameraService;)Landroid/hardware/Camera;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$9;->this$0:Lcom/google/android/goggles/camera/CameraService;

    const-string v1, "cancelAutoFocus skipped because camera is null."

    # invokes: Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->access$900(Lcom/google/android/goggles/camera/CameraService;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$9;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraService;->access$800(Lcom/google/android/goggles/camera/CameraService;)Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    goto :goto_0
.end method
