.class Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;
.super Ljava/lang/Object;
.source "PreviewLooper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/PreviewLooper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackgroundFrameProcessors"
.end annotation


# instance fields
.field private mCamera:Landroid/hardware/Camera;

.field final synthetic this$0:Lcom/google/android/goggles/camera/PreviewLooper;


# direct methods
.method private constructor <init>(Lcom/google/android/goggles/camera/PreviewLooper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->this$0:Lcom/google/android/goggles/camera/PreviewLooper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/goggles/camera/PreviewLooper;Lcom/google/android/goggles/camera/PreviewLooper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/camera/PreviewLooper;
    .param p2    # Lcom/google/android/goggles/camera/PreviewLooper$1;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;-><init>(Lcom/google/android/goggles/camera/PreviewLooper;)V

    return-void
.end method


# virtual methods
.method public bindCamera(Landroid/hardware/Camera;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->mCamera:Landroid/hardware/Camera;

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput-object p1, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->mCamera:Landroid/hardware/Camera;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 5

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    invoke-static {}, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->getFakePreviewFrame()Lcom/google/android/goggles/camera/PreviewFrame;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->this$0:Lcom/google/android/goggles/camera/PreviewLooper;

    # getter for: Lcom/google/android/goggles/camera/PreviewLooper;->mFrameProcessors:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/goggles/camera/PreviewLooper;->access$100(Lcom/google/android/goggles/camera/PreviewLooper;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/goggles/camera/FrameProcessor;

    if-eqz v0, :cond_0

    move-object v3, v0

    :goto_1
    invoke-virtual {v2, v3}, Lcom/google/android/goggles/camera/FrameProcessor;->onBackgroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->this$0:Lcom/google/android/goggles/camera/PreviewLooper;

    # getter for: Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;
    invoke-static {v3}, Lcom/google/android/goggles/camera/PreviewLooper;->access$200(Lcom/google/android/goggles/camera/PreviewLooper;)Lcom/google/android/goggles/camera/PreviewFrame;

    move-result-object v3

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->mCamera:Landroid/hardware/Camera;

    iget-object v4, p0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->this$0:Lcom/google/android/goggles/camera/PreviewLooper;

    # getter for: Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;
    invoke-static {v4}, Lcom/google/android/goggles/camera/PreviewLooper;->access$200(Lcom/google/android/goggles/camera/PreviewLooper;)Lcom/google/android/goggles/camera/PreviewFrame;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    invoke-virtual {v3, v4}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    return-void
.end method
