.class Lcom/google/android/goggles/camera/CameraService$8;
.super Ljava/lang/Object;
.source "CameraService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraService;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraService;

.field final synthetic val$callback:Landroid/hardware/Camera$AutoFocusCallback;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraService;Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraService$8;->this$0:Lcom/google/android/goggles/camera/CameraService;

    iput-object p2, p0, Lcom/google/android/goggles/camera/CameraService$8;->val$callback:Landroid/hardware/Camera$AutoFocusCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$8;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/google/android/goggles/camera/CameraService;->access$800(Lcom/google/android/goggles/camera/CameraService;)Landroid/hardware/Camera;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$8;->this$0:Lcom/google/android/goggles/camera/CameraService;

    const-string v2, "autoFocus skipped because camera is null."

    # invokes: Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/goggles/camera/CameraService;->access$900(Lcom/google/android/goggles/camera/CameraService;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$8;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/google/android/goggles/camera/CameraService;->access$800(Lcom/google/android/goggles/camera/CameraService;)Landroid/hardware/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$8;->val$callback:Landroid/hardware/Camera$AutoFocusCallback;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "goggles.CameraService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Focus failure: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$8;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/goggles/camera/CameraService;->access$100(Lcom/google/android/goggles/camera/CameraService;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/goggles/camera/CameraService$8$1;

    invoke-direct {v2, p0}, Lcom/google/android/goggles/camera/CameraService$8$1;-><init>(Lcom/google/android/goggles/camera/CameraService$8;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
