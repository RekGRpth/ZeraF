.class Lcom/google/android/goggles/camera/CameraService$4;
.super Ljava/lang/Object;
.source "CameraService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraService;->setParameters(Lcom/google/android/goggles/camera/CameraParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraService;

.field final synthetic val$oldParams:Lcom/google/android/goggles/camera/CameraParameters;

.field final synthetic val$parameters:Lcom/google/android/goggles/camera/CameraParameters;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraService;Lcom/google/android/goggles/camera/CameraParameters;Lcom/google/android/goggles/camera/CameraParameters;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraService$4;->this$0:Lcom/google/android/goggles/camera/CameraService;

    iput-object p2, p0, Lcom/google/android/goggles/camera/CameraService$4;->val$oldParams:Lcom/google/android/goggles/camera/CameraParameters;

    iput-object p3, p0, Lcom/google/android/goggles/camera/CameraService$4;->val$parameters:Lcom/google/android/goggles/camera/CameraParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$4;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCameraPreviewing:Z
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraService;->access$300(Lcom/google/android/goggles/camera/CameraService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$4;->val$oldParams:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v0, v0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$4;->val$parameters:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v1, v1, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraManager$Size;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to change preview size while preview is active. camera previewing is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$4;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCameraPreviewing:Z
    invoke-static {v2}, Lcom/google/android/goggles/camera/CameraService;->access$300(Lcom/google/android/goggles/camera/CameraService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", preview size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$4;->val$oldParams:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v2, v2, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {v2}, Lcom/google/android/goggles/camera/CameraManager$Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$4;->val$parameters:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v2, v2, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {v2}, Lcom/google/android/goggles/camera/CameraManager$Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$4;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # invokes: Lcom/google/android/goggles/camera/CameraService;->updateParametersInBackground()V
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraService;->access$400(Lcom/google/android/goggles/camera/CameraService;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
