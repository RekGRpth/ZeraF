.class Lcom/google/android/goggles/ui/CameraButton;
.super Landroid/widget/ImageButton;
.source "CameraButton.java"


# instance fields
.field private drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

.field private mDefaultMarginPx:I

.field private mDescribeImageText:Landroid/view/View;

.field private mMode:I

.field private mModeContentDescriptionMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mModeLayerMap:Landroid/util/SparseIntArray;

.field private mResponseModeRightMarginPx:I

.field private mTextEndpointRightMarginPx:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v8, 0x7f0d003c

    const/4 v7, 0x4

    const/high16 v6, 0x41200000

    const v4, 0x7f1002a9

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v5, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v5, v4}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/4 v2, 0x2

    const v3, 0x7f1002aa

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/4 v2, 0x3

    const v3, 0x7f1002ab

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const v2, 0x7f1002ac

    invoke-virtual {v1, v7, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/4 v2, 0x7

    const v3, 0x7f1002ad

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/16 v2, 0x8

    const v3, 0x7f1002ad

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    const/16 v2, 0x9

    const v3, 0x7f1002ad

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeContentDescriptionMap:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeContentDescriptionMap:Landroid/util/SparseArray;

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d003a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeContentDescriptionMap:Landroid/util/SparseArray;

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d003b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeContentDescriptionMap:Landroid/util/SparseArray;

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeContentDescriptionMap:Landroid/util/SparseArray;

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeContentDescriptionMap:Landroid/util/SparseArray;

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->CameraButton:[I

    invoke-virtual {p1, p2, v1, v5, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mTextEndpointRightMarginPx:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mResponseModeRightMarginPx:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mDefaultMarginPx:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v7}, Lcom/google/android/goggles/ui/CameraButton;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method adjustMargins()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mDescribeImageText:Landroid/view/View;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const v2, 0x7f100110

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mDescribeImageText:Landroid/view/View;

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mDescribeImageText:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    :goto_0
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v1, v1, -0x71

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mDefaultMarginPx:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v1, v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    :goto_1
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mDefaultMarginPx:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mTextEndpointRightMarginPx:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v1, v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_1

    :pswitch_3
    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mResponseModeRightMarginPx:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v1, v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/ImageButton;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->adjustMargins()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd8

    :goto_0
    const/16 v2, 0xc

    invoke-virtual {p1, v1, v0, v2}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_0
    const/16 v0, 0xf2

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/goggles/ui/CameraButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method updateMode(I)V
    .locals 7
    .param p1    # I

    const v6, 0x7f1002a8

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput p1, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeContentDescriptionMap:Landroid/util/SparseArray;

    iget v4, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/google/android/goggles/ui/CameraButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mModeLayerMap:Landroid/util/SparseIntArray;

    iget v4, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    invoke-virtual {v1, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    const v4, 0x7f1002a7

    iget-object v5, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/TransitionDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/drawable/TransitionDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    iget-object v4, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/TransitionDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v6, v4}, Landroid/graphics/drawable/TransitionDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v4, 0x7f1002ad

    if-eq v1, v4, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v4, 0x7f1002a9

    if-ne v1, v4, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    const/16 v4, 0xfa

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    const/4 v4, 0x3

    if-eq v1, v4, :cond_1

    iget v1, p0, Lcom/google/android/goggles/ui/CameraButton;->mMode:I

    const/4 v4, 0x4

    if-ne v1, v4, :cond_3

    :cond_1
    move v1, v3

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/goggles/ui/CameraButton;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/CameraButton;->adjustMargins()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/goggles/ui/CameraButton;->drawableLayers:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method
