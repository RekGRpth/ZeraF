.class Lcom/google/android/goggles/ui/QueryBar$EventHandler;
.super Ljava/lang/Object;
.source "QueryBar.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/QueryBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/ui/QueryBar;


# direct methods
.method private constructor <init>(Lcom/google/android/goggles/ui/QueryBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/goggles/ui/QueryBar;Lcom/google/android/goggles/ui/QueryBar$1;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/ui/QueryBar;
    .param p2    # Lcom/google/android/goggles/ui/QueryBar$1;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/ui/QueryBar$EventHandler;-><init>(Lcom/google/android/goggles/ui/QueryBar;)V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x3

    if-eq p2, v2, :cond_0

    invoke-static {p3}, Lcom/google/android/searchcommon/util/Util;->isEnterKey(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    # getter for: Lcom/google/android/goggles/ui/QueryBar;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;
    invoke-static {v2}, Lcom/google/android/goggles/ui/QueryBar;->access$500(Lcom/google/android/goggles/ui/QueryBar;)Lcom/google/android/goggles/ui/GogglesPlate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/goggles/ui/GogglesPlate;->getLastPreviewFrame()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v1, "goggles.QueryBar"

    const-string v2, "onEditorAction() with null mQueryBitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    # getter for: Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/google/android/goggles/ui/QueryBar;->access$600(Lcom/google/android/goggles/ui/QueryBar;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    const-string v1, "goggles.QueryBar"

    const-string v2, "Can\'t search for empty string, ignoring."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/QueryBar;->adjustLogo(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    # getter for: Lcom/google/android/goggles/ui/QueryBar;->mCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;
    invoke-static {v0}, Lcom/google/android/goggles/ui/QueryBar;->access$400(Lcom/google/android/goggles/ui/QueryBar;)Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    # getter for: Lcom/google/android/goggles/ui/QueryBar;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;
    invoke-static {v2}, Lcom/google/android/goggles/ui/QueryBar;->access$500(Lcom/google/android/goggles/ui/QueryBar;)Lcom/google/android/goggles/ui/GogglesPlate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/goggles/ui/GogglesPlate;->getLastPreviewFrame()Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    # getter for: Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/google/android/goggles/ui/QueryBar;->access$600(Lcom/google/android/goggles/ui/QueryBar;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;->this$0:Lcom/google/android/goggles/ui/QueryBar;

    # getter for: Lcom/google/android/goggles/ui/QueryBar;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;
    invoke-static {v4}, Lcom/google/android/goggles/ui/QueryBar;->access$500(Lcom/google/android/goggles/ui/QueryBar;)Lcom/google/android/goggles/ui/GogglesPlate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/goggles/ui/GogglesPlate;->getMode()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/goggles/ui/GogglesPlate;->shouldRequestSimilarImages(I)Z

    move-result v4

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;->onTextEdited(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Z)V

    move v0, v1

    goto :goto_0
.end method
