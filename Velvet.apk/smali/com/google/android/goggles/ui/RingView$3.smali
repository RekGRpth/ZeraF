.class Lcom/google/android/goggles/ui/RingView$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "RingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/ui/RingView;->updateMode(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCanceled:Z

.field final synthetic this$0:Lcom/google/android/goggles/ui/RingView;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/ui/RingView;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/goggles/ui/RingView$3;->this$0:Lcom/google/android/goggles/ui/RingView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/ui/RingView$3;->mCanceled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/goggles/ui/RingView$3;->mCanceled:Z

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView$3;->this$0:Lcom/google/android/goggles/ui/RingView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/goggles/ui/RingView;->access$402(Lcom/google/android/goggles/ui/RingView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6
    .param p1    # Landroid/animation/Animator;

    iget-boolean v0, p0, Lcom/google/android/goggles/ui/RingView$3;->mCanceled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView$3;->this$0:Lcom/google/android/goggles/ui/RingView;

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView$3;->this$0:Lcom/google/android/goggles/ui/RingView;

    # getter for: Lcom/google/android/goggles/ui/RingView;->MODESTATE:Landroid/util/Property;
    invoke-static {}, Lcom/google/android/goggles/ui/RingView;->access$600()Landroid/util/Property;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    # setter for: Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/goggles/ui/RingView;->access$502(Lcom/google/android/goggles/ui/RingView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView$3;->this$0:Lcom/google/android/goggles/ui/RingView;

    # getter for: Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/goggles/ui/RingView;->access$500(Lcom/google/android/goggles/ui/RingView;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView$3;->this$0:Lcom/google/android/goggles/ui/RingView;

    # getter for: Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/goggles/ui/RingView;->access$500(Lcom/google/android/goggles/ui/RingView;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void
.end method
