.class Lcom/google/android/goggles/ui/GogglesPlate$4;
.super Ljava/lang/Object;
.source "GogglesPlate.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/ui/GogglesPlate;->doFinalFocus(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/ui/GogglesPlate;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/ui/GogglesPlate;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlate$4;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    iput-object p2, p0, Lcom/google/android/goggles/ui/GogglesPlate$4;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$4;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate$4;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v1}, Lcom/google/android/goggles/ui/GogglesPlate;->getLastPreviewFrame()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->setQueryImage(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$4;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    const/4 v1, 0x6

    const/4 v2, 0x1

    # invokes: Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->access$400(Lcom/google/android/goggles/ui/GogglesPlate;IZ)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$4;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mRingView:Lcom/google/android/goggles/ui/RingView;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$500(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/RingView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/RingView;->flash()V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$4;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method
