.class public interface abstract Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;
.super Ljava/lang/Object;
.source "GogglesPlate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/GogglesPlate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CameraCallback"
.end annotation


# virtual methods
.method public abstract onCameraCreated(Lcom/google/android/goggles/camera/CameraManager;)V
.end method

.method public abstract onCameraOpened(Z)V
.end method

.method public abstract onCameraReleased()V
.end method

.method public abstract onCaptureButtonClicked()V
.end method

.method public abstract onFlashButtonClicked()V
.end method
