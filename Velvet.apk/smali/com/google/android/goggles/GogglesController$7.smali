.class Lcom/google/android/goggles/GogglesController$7;
.super Ljava/lang/Object;
.source "GogglesController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/GogglesController;->onCaptureButtonClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/GogglesController;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/GogglesController$7;->this$0:Lcom/google/android/goggles/GogglesController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$7;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$100(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/QueryFrameProcessor;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/QueryFrameProcessor;->setLastQueryImportant(Z)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$7;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$100(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/QueryFrameProcessor;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/QueryFrameProcessor;->requestDoneStreamingImages(I)V

    return-void
.end method
