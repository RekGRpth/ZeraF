.class public Lcom/google/android/goggles/VisionGyro;
.super Ljava/lang/Object;
.source "VisionGyro.java"


# instance fields
.field private final mMatrix:[F

.field private mNativeVisionGyro:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "goggles_clientvision"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/goggles/VisionGyro;->mMatrix:[F

    invoke-virtual {p0}, Lcom/google/android/goggles/VisionGyro;->constructNative()I

    move-result v0

    iput v0, p0, Lcom/google/android/goggles/VisionGyro;->mNativeVisionGyro:I

    return-void
.end method


# virtual methods
.method protected native constructNative()I
.end method

.method public declared-synchronized destroy()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/goggles/VisionGyro;->mNativeVisionGyro:I

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/VisionGyro;->destroyNative(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected native destroyNative(I)V
.end method

.method public declared-synchronized nextFrame([BIILandroid/graphics/Matrix;)V
    .locals 3
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Matrix;

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/goggles/VisionGyro;->mNativeVisionGyro:I

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/goggles/VisionGyro;->nextFrameNative(I[BII)V

    iget v0, p0, Lcom/google/android/goggles/VisionGyro;->mNativeVisionGyro:I

    iget-object v1, p0, Lcom/google/android/goggles/VisionGyro;->mMatrix:[F

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/goggles/VisionGyro;->obtainVgTransformNative(I[FZ)V

    iget-object v0, p0, Lcom/google/android/goggles/VisionGyro;->mMatrix:[F

    invoke-virtual {p4, v0}, Landroid/graphics/Matrix;->setValues([F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected native nextFrameNative(I[BII)V
.end method

.method protected native obtainVgTransformNative(I[FZ)V
.end method
