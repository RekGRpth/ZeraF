.class Lcom/google/android/goggles/GogglesController$4;
.super Ljava/lang/Object;
.source "GogglesController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/GogglesController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/GogglesController;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/GogglesController$4;->this$0:Lcom/google/android/goggles/GogglesController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$4;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$200(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$GogglesUi;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$GogglesUi;->pauseUi()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$4;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$300(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/goggles/ResultRanker;->waitForResults(J)V

    return-void
.end method
