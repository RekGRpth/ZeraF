.class public Lcom/google/android/goggles/QueryFrameProcessor;
.super Lcom/google/android/goggles/camera/FrameProcessor;
.source "QueryFrameProcessor.java"

# interfaces
.implements Lcom/google/android/goggles/SceneFrameProcessor$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    }
.end annotation


# static fields
.field private static instance:Lcom/google/android/goggles/QueryFrameProcessor;


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

.field private mFakeResponseList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;"
        }
    .end annotation
.end field

.field private mFlags:I

.field private final mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

.field private final mListener:Lcom/google/android/goggles/network/QueryManager$Listener;

.field private mQueryAllowed:I

.field private final mQueryManager:Lcom/google/android/goggles/network/QueryManager;


# direct methods
.method public constructor <init>(Lcom/google/android/goggles/QueryFrameProcessor$EventListener;Ljava/util/concurrent/Executor;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 8
    .param p1    # Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p4    # Lcom/google/android/velvet/VelvetFactory;
    .param p5    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p6    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p7    # Lcom/google/android/goggles/GogglesSettings;
    .param p8    # Lcom/google/android/velvet/presenter/QueryState;

    const-string v0, "Query processor"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FrameProcessor;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    new-instance v0, Lcom/google/android/goggles/QueryFrameProcessor$1;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/QueryFrameProcessor$1;-><init>(Lcom/google/android/goggles/QueryFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mListener:Lcom/google/android/goggles/network/QueryManager$Listener;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFakeResponseList:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mListener:Lcom/google/android/goggles/network/QueryManager$Listener;

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/goggles/QueryFrameProcessor;->createQueryManager(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/network/QueryManager$Listener;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/goggles/network/QueryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    iput-object p1, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    iput-object p2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {p7}, Lcom/google/android/goggles/GogglesSettings;->shouldShowDebugView()Z

    move-result v0

    if-eqz v0, :cond_0

    sput-object p0, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/QueryFrameProcessor;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFakeResponseList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    .locals 1
    .param p0    # Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/QueryFrameProcessor;)I
    .locals 1
    .param p0    # Lcom/google/android/goggles/QueryFrameProcessor;

    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/goggles/QueryFrameProcessor;I)I
    .locals 0
    .param p0    # Lcom/google/android/goggles/QueryFrameProcessor;
    .param p1    # I

    iput p1, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/goggles/QueryFrameProcessor;I)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/QueryFrameProcessor;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/goggles/QueryFrameProcessor;->checkAndClear(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400()Lcom/google/android/goggles/QueryFrameProcessor;
    .locals 1

    sget-object v0, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/network/QueryManager$Listener;
    .locals 1
    .param p0    # Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mListener:Lcom/google/android/goggles/network/QueryManager$Listener;

    return-object v0
.end method

.method private declared-synchronized checkAndClear(I)Z
    .locals 2
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static insertFakeTriggers(Landroid/view/Menu;)V
    .locals 2
    .param p0    # Landroid/view/Menu;

    const-string v0, "Zero Results"

    invoke-interface {p0, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/goggles/QueryFrameProcessor$3;

    invoke-direct {v1}, Lcom/google/android/goggles/QueryFrameProcessor$3;-><init>()V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "One Result"

    invoke-interface {p0, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/goggles/QueryFrameProcessor$4;

    invoke-direct {v1}, Lcom/google/android/goggles/QueryFrameProcessor$4;-><init>()V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "Two Results"

    invoke-interface {p0, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/goggles/QueryFrameProcessor$5;

    invoke-direct {v1}, Lcom/google/android/goggles/QueryFrameProcessor$5;-><init>()V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method

.method public static updateFakeResponseList(I)V
    .locals 3
    .param p0    # I

    sget-object v1, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v1, v1, Lcom/google/android/goggles/QueryFrameProcessor;->mFakeResponseList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x1

    if-lt p0, v1, :cond_1

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;-><init>()V

    const-string v1, "Fake Result 1"

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->setTitle(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    const-string v1, "This is the query for Fake web search 1"

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->setSearchQuery(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    const-string v1, "Fake Subtitle 1"

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->setSubtitle(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    sget-object v1, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v1, v1, Lcom/google/android/goggles/QueryFrameProcessor;->mFakeResponseList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v1, 0x2

    if-lt p0, v1, :cond_2

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;-><init>()V

    const-string v1, "Fake Result 2"

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->setTitle(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    const-string v1, "Fake web search 2"

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->setSearchQuery(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    const-string v1, "Fake Subtitle 2"

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->setSubtitle(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    sget-object v1, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v1, v1, Lcom/google/android/goggles/QueryFrameProcessor;->mFakeResponseList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-object v1, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v1, v1, Lcom/google/android/goggles/QueryFrameProcessor;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/goggles/QueryFrameProcessor$2;

    invoke-direct {v2}, Lcom/google/android/goggles/QueryFrameProcessor$2;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    invoke-interface {v0}, Lcom/google/android/goggles/network/QueryManager;->close()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected createQueryManager(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/network/QueryManager$Listener;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/goggles/network/QueryManager;
    .locals 8
    .param p1    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p2    # Lcom/google/android/velvet/VelvetFactory;
    .param p3    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p4    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p5    # Lcom/google/android/goggles/network/QueryManager$Listener;
    .param p6    # Lcom/google/android/goggles/GogglesSettings;
    .param p7    # Lcom/google/android/velvet/presenter/QueryState;

    new-instance v0, Lcom/google/android/goggles/network/QueryManagerImpl;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/goggles/network/QueryManagerImpl;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/network/QueryManager$Listener;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)V

    return-object v0
.end method

.method protected onBackgroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V
    .locals 6
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget v3, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    if-nez v3, :cond_3

    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/google/android/goggles/QueryFrameProcessor;->checkAndClear(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_0
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/goggles/QueryFrameProcessor;->checkAndClear(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    invoke-interface {v2}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onImageFetchingDone()V

    :cond_1
    monitor-exit p0

    :cond_2
    :goto_0
    return-void

    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_1
    iget v3, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    if-ne v3, v2, :cond_4

    const/4 v3, 0x4

    invoke-direct {p0, v3}, Lcom/google/android/goggles/QueryFrameProcessor;->checkAndClear(I)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->reset()V

    iget-object v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1, v2}, Lcom/google/android/goggles/camera/PreviewFrame;->toJpeg(Ljava/io/OutputStream;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "goggles.QueryFrameProcessor"

    const-string v3, "Failed to convert preview into jpeg data."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_4
    :try_start_3
    iget-object v3, p1, Lcom/google/android/goggles/camera/PreviewFrame;->newBarcode:Lcom/google/android/goggles/Barcode;

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    invoke-interface {v3}, Lcom/google/android/goggles/network/QueryManager;->hasOutstandingQuery()Z

    move-result v3

    if-nez v3, :cond_6

    move v0, v2

    :goto_2
    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :cond_7
    monitor-enter p0

    :try_start_4
    iget v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    if-lez v2, :cond_8

    iget v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    :cond_8
    iget v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    if-nez v2, :cond_9

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/goggles/QueryFrameProcessor;->checkAndClear(I)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    invoke-interface {v2}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onImageFetchingDone()V

    :cond_9
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    sget-object v2, Lcom/google/android/goggles/ui/DebugView;->frames:Lcom/google/android/goggles/ui/DebugView$Frames;

    iget-object v3, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    invoke-interface {v3}, Lcom/google/android/goggles/network/QueryManager;->getNextSequenceNumber()I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/google/android/goggles/ui/DebugView$Frames;->add(Lcom/google/android/goggles/camera/PreviewFrame;I)V

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    iget-object v2, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    iget-object v3, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iget v4, p1, Lcom/google/android/goggles/camera/PreviewFrame;->rotation:I

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/google/android/goggles/network/QueryManager;->query([BILjava/lang/String;Z)V

    goto :goto_0

    :catchall_2
    move-exception v2

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2
.end method

.method public onNewScene()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    iget-object v1, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    invoke-interface {v1}, Lcom/google/android/goggles/network/QueryManager;->getNextSequenceNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onNewScene(I)V

    return-void
.end method

.method public open(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    invoke-interface {v0, p1}, Lcom/google/android/goggles/network/QueryManager;->open(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V

    sget-object v0, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    if-eqz v0, :cond_0

    sput-object p0, Lcom/google/android/goggles/QueryFrameProcessor;->instance:Lcom/google/android/goggles/QueryFrameProcessor;

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized requestDoneStreamingImages(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    if-gez p1, :cond_0

    const/4 p1, -0x1

    :cond_0
    :try_start_0
    iput p1, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I

    or-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I

    and-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setLastQueryImportant(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/android/goggles/QueryFrameProcessor;->mFlags:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
