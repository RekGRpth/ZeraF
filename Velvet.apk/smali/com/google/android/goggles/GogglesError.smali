.class public Lcom/google/android/goggles/GogglesError;
.super Lcom/google/android/velvet/presenter/SearchError;
.source "GogglesError.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchError;-><init>()V

    return-void
.end method


# virtual methods
.method public getErrorImageResId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getErrorMessageResId()I
    .locals 1

    const v0, 0x7f0d0041

    return v0
.end method

.method public retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;
    .param p2    # Lcom/google/android/velvet/Query;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->newSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventStartSearch(I)V

    invoke-super {p0, p1, p2}, Lcom/google/android/velvet/presenter/SearchError;->retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V

    return-void
.end method
