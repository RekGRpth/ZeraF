.class public Lcom/google/android/goggles/TraceTracker$RealSession;
.super Ljava/lang/Object;
.source "TraceTracker.java"

# interfaces
.implements Lcom/google/android/goggles/TraceTracker$Session;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/TraceTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RealSession"
.end annotation


# instance fields
.field private mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

.field final synthetic this$0:Lcom/google/android/goggles/TraceTracker;


# direct methods
.method private constructor <init>(Lcom/google/android/goggles/TraceTracker;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->this$0:Lcom/google/android/goggles/TraceTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/goggles/TraceTracker;Lcom/google/android/goggles/TraceTracker$1;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/TraceTracker;
    .param p2    # Lcom/google/android/goggles/TraceTracker$1;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/TraceTracker$RealSession;-><init>(Lcom/google/android/goggles/TraceTracker;)V

    return-void
.end method

.method private addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V
    .locals 2
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientTimeMs(J)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    iget-object v1, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;->addEvents(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public addClientEventLocalBarcodeDetected()V
    .locals 2

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;-><init>()V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addClientEventRecvResponse(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setResultSetNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addClientEventSceneChangeDetected()V
    .locals 2

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addClientEventSendRequest(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setSequenceNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addClientEventThumbnailGet(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setFetchUrl(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    invoke-virtual {v0, p2}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;->setFetchSucceeded(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setClientEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addImpressionDisambiguation(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;
    .locals 2
    .param p1    # I

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;->setResultSetNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setImpression(Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-object v0
.end method

.method public addImpressionSingleResult(ILjava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;->setResultSetNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    invoke-virtual {v0, p2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;->setResultId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setImpression(Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-object v0
.end method

.method public addUserEventDisambigClick(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;->setResultSetNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-virtual {v0, p2}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;->setResultId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setUserEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addUserEventRequestResults()V
    .locals 2

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setUserEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addUserEventStartSearch(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;->setEntryPoint(I)Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setUserEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public addUserEventTextRefinement()V
    .locals 2

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;-><init>()V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;->setType(I)Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;->setUserEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$UserEvent;)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/goggles/TraceTracker$RealSession;->addEvent(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientLogEvent;)V

    return-void
.end method

.method public build()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    goto :goto_0
.end method

.method public end()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->this$0:Lcom/google/android/goggles/TraceTracker;

    # invokes: Lcom/google/android/goggles/TraceTracker;->endSession(Lcom/google/android/goggles/TraceTracker$Session;)V
    invoke-static {v0, p0}, Lcom/google/android/goggles/TraceTracker;->access$100(Lcom/google/android/goggles/TraceTracker;Lcom/google/android/goggles/TraceTracker$Session;)V

    return-void
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;->getSessionId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setSessionId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$RealSession;->mClientLog:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-virtual {v0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;->setSessionId(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    return-void
.end method
