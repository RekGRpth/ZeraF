.class final Lcom/google/android/goggles/GogglesController$1;
.super Ljava/lang/Object;
.source "GogglesController.java"

# interfaces
.implements Lcom/google/android/goggles/GogglesController$GogglesUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/GogglesController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doFinalFocus(Ljava/lang/Runnable;)V
    .locals 0
    .param p1    # Ljava/lang/Runnable;

    return-void
.end method

.method public isGogglesCapturing()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isTorchOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public notifyGogglesQuerySent()V
    .locals 0

    return-void
.end method

.method public notifyGogglesResult()V
    .locals 0

    return-void
.end method

.method public notifyNoGogglesResult()V
    .locals 0

    return-void
.end method

.method public pauseUi()V
    .locals 0

    return-void
.end method

.method public setTorch(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public startCamera()V
    .locals 0

    return-void
.end method

.method public stopSpinner()V
    .locals 0

    return-void
.end method
