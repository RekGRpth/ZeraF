.class Lcom/google/android/goggles/RecognizedContactCard$6;
.super Ljava/lang/Object;
.source "RecognizedContactCard.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/RecognizedContactCard;->populateActions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/RecognizedContactCard;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/RecognizedContactCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/RecognizedContactCard$6;->this$0:Lcom/google/android/goggles/RecognizedContactCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard$6;->this$0:Lcom/google/android/goggles/RecognizedContactCard;

    invoke-virtual {v1}, Lcom/google/android/goggles/RecognizedContactCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "clipboard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    iget-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard$6;->this$0:Lcom/google/android/goggles/RecognizedContactCard;

    invoke-virtual {v1}, Lcom/google/android/goggles/RecognizedContactCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0049

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard$6;->this$0:Lcom/google/android/goggles/RecognizedContactCard;

    invoke-virtual {v1}, Lcom/google/android/goggles/RecognizedContactCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/RecognizedContactController;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->getDataString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    iget-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard$6;->this$0:Lcom/google/android/goggles/RecognizedContactCard;

    invoke-virtual {v1}, Lcom/google/android/goggles/RecognizedContactCard;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/RecognizedContactCard$6;->this$0:Lcom/google/android/goggles/RecognizedContactCard;

    invoke-virtual {v2}, Lcom/google/android/goggles/RecognizedContactCard;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method
