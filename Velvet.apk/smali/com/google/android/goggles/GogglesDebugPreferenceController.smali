.class public Lcom/google/android/goggles/GogglesDebugPreferenceController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "GogglesDebugPreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final mSettings:Lcom/google/android/goggles/GogglesSettings;


# direct methods
.method public constructor <init>(Lcom/google/android/goggles/GogglesSettings;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/GogglesSettings;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/GogglesDebugPreferenceController;)Lcom/google/android/goggles/GogglesSettings;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesDebugPreferenceController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/GogglesDebugPreferenceController;Landroid/preference/ListPreference;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/GogglesDebugPreferenceController;
    .param p1    # Landroid/preference/ListPreference;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleFrontend(Landroid/preference/ListPreference;)V

    return-void
.end method

.method private handleAnnotation(Landroid/preference/EditTextPreference;)V
    .locals 1
    .param p1    # Landroid/preference/EditTextPreference;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesSettings;->getAnnotation()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "None"

    invoke-virtual {p1, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesSettings;->getAnnotation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private handleCustomFrontend(Landroid/preference/ListPreference;)V
    .locals 7
    .param p1    # Landroid/preference/ListPreference;

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Landroid/widget/EditText;

    invoke-direct {v4, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const-string v5, "https://<your-host>.sandbox.google.com/m/voice-search"

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/16 v5, 0x8

    const/16 v6, 0x13

    invoke-virtual {v4, v5, v6}, Landroid/widget/EditText;->setSelection(II)V

    new-instance v2, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;

    invoke-direct {v2, p0, v4, p1}, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;-><init>(Lcom/google/android/goggles/GogglesDebugPreferenceController;Landroid/widget/EditText;Landroid/preference/ListPreference;)V

    new-instance v1, Lcom/google/android/goggles/GogglesDebugPreferenceController$2;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesDebugPreferenceController$2;-><init>(Lcom/google/android/goggles/GogglesDebugPreferenceController;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0d0050

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    const v5, 0x7f0d0365

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v5, 0x7f0d0366

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    new-instance v5, Lcom/google/android/goggles/GogglesDebugPreferenceController$3;

    invoke-direct {v5, p0, v3, v4}, Lcom/google/android/goggles/GogglesDebugPreferenceController$3;-><init>(Lcom/google/android/goggles/GogglesDebugPreferenceController;Landroid/content/Context;Landroid/widget/EditText;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private handleFrontend(Landroid/preference/ListPreference;)V
    .locals 4
    .param p1    # Landroid/preference/ListPreference;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->getAllServers()[Ljava/lang/CharSequence;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->getAllServers()[Ljava/lang/CharSequence;

    move-result-object v1

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->getCurrentServer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "goggles.frontend"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/preference/ListPreference;

    invoke-direct {p0, v0}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleFrontend(Landroid/preference/ListPreference;)V

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "goggles.annotation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/preference/EditTextPreference;

    invoke-direct {p0, v0}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleAnnotation(Landroid/preference/EditTextPreference;)V

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "goggles.frontend"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Landroid/preference/ListPreference;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleCustomFrontend(Landroid/preference/ListPreference;)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/GogglesSettings;->setServerInfo(Ljava/lang/String;)V

    check-cast p1, Landroid/preference/ListPreference;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleFrontend(Landroid/preference/ListPreference;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "goggles.showDebugView"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/GogglesSettings;->showDebugView(Z)V

    :cond_2
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "goggles.annotation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/google/android/goggles/GogglesSettings;->setAnnotation(Ljava/lang/String;)V

    check-cast p1, Landroid/preference/EditTextPreference;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleAnnotation(Landroid/preference/EditTextPreference;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "goggles.fakeFocus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/GogglesSettings;->forceFakeFocus(Z)V

    goto :goto_2
.end method
