.class Lcom/google/android/searchcommon/GlobalSearchServicesImpl$1;
.super Landroid/database/DataSetObserver;
.source "GlobalSearchServicesImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSources()Lcom/google/android/searchcommon/summons/Sources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/GlobalSearchServicesImpl;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/GlobalSearchServicesImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$1;->this$0:Lcom/google/android/searchcommon/GlobalSearchServicesImpl;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$1;->this$0:Lcom/google/android/searchcommon/GlobalSearchServicesImpl;

    iget-object v5, v5, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$1;->this$0:Lcom/google/android/searchcommon/GlobalSearchServicesImpl;

    # getter for: Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;
    invoke-static {v3}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->access$000(Lcom/google/android/searchcommon/GlobalSearchServicesImpl;)Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/summons/Sources;->getSources()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v2}, Lcom/google/android/searchcommon/summons/Source;->getIconPackage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$1;->this$0:Lcom/google/android/searchcommon/GlobalSearchServicesImpl;

    # getter for: Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mImageLoadersManager:Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;
    invoke-static {v3}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->access$100(Lcom/google/android/searchcommon/GlobalSearchServicesImpl;)Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->clean(Ljava/util/Set;)V

    return-void
.end method
