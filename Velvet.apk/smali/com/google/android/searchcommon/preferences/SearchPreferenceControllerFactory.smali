.class public Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;
.super Ljava/lang/Object;
.source "SearchPreferenceControllerFactory.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/searchcommon/preferences/PreferenceController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory$UnknownPreferenceException;
    }
.end annotation


# static fields
.field private static final USER_ACCOUNT_SETTINGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final mActivity:Landroid/app/Activity;

.field private final mBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

.field private final mControllers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/preferences/PreferenceController;",
            ">;"
        }
    .end annotation
.end field

.field protected final mDeviceClassSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected final mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

.field protected final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field protected final mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

.field protected final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field protected final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field protected final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field protected final mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

.field protected final mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

.field private mScreen:Landroid/preference/PreferenceScreen;

.field protected final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field protected final mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

.field protected final mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

.field protected final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field protected final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field protected final mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "google_account"

    const-string v1, "use_google_com"

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->USER_ACCOUNT_SETTINGS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/app/Activity;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/searchcommon/GlobalSearchServices;Landroid/database/DataSetObservable;Lcom/google/common/base/Supplier;Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/velvet/VelvetBackgroundTasks;Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/searchcommon/MarinerOptInSettings;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/SearchSettings;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;
    .param p4    # Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;
    .param p5    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p6    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p7    # Landroid/app/Activity;
    .param p8    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p9    # Lcom/google/android/searchcommon/GlobalSearchServices;
    .param p10    # Landroid/database/DataSetObservable;
    .param p12    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p13    # Lcom/google/android/goggles/GogglesSettings;
    .param p14    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p15    # Lcom/google/android/velvet/VelvetBackgroundTasks;
    .param p16    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p17    # Lcom/google/android/speech/utils/NetworkInformation;
    .param p18    # Lcom/google/android/searchcommon/MarinerOptInSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/SearchConfig;",
            "Lcom/google/android/searchcommon/SearchSettings;",
            "Lcom/google/android/voicesearch/settings/Settings;",
            "Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;",
            "Lcom/google/android/searchcommon/google/gaia/LoginHelper;",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper;",
            "Landroid/app/Activity;",
            "Lcom/google/android/searchcommon/google/LocationSettings;",
            "Lcom/google/android/searchcommon/GlobalSearchServices;",
            "Landroid/database/DataSetObservable;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/speech/embedded/Greco3DataManager;",
            "Lcom/google/android/goggles/GogglesSettings;",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Lcom/google/android/velvet/VelvetBackgroundTasks;",
            "Lcom/google/android/searchcommon/history/SearchHistoryHelper;",
            "Lcom/google/android/speech/utils/NetworkInformation;",
            "Lcom/google/android/searchcommon/MarinerOptInSettings;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p4, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    iput-object p5, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p6, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p7, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    iput-object p8, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    iput-object p9, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    iput-object p10, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    iput-object p11, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mDeviceClassSupplier:Lcom/google/common/base/Supplier;

    iput-object p12, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iput-object p13, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1, p0}, Lcom/google/android/searchcommon/SearchSettings;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    return-void
.end method

.method private getControllerFor(Landroid/preference/Preference;)Lcom/google/android/searchcommon/preferences/PreferenceController;
    .locals 3
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->getControllerId(Landroid/preference/Preference;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PreferenceController;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->createControllerFor(Landroid/preference/Preference;)Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->addController(Ljava/lang/String;Lcom/google/android/searchcommon/preferences/PreferenceController;)V

    :cond_0
    return-object v0
.end method

.method private handlePrefenceGroup(Landroid/preference/PreferenceGroup;)V
    .locals 3
    .param p1    # Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->filterPreference(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->handlePreference(Landroid/preference/Preference;)V

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method protected addController(Ljava/lang/String;Lcom/google/android/searchcommon/preferences/PreferenceController;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/preferences/PreferenceController;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected createControllerFor(Landroid/preference/Preference;)Lcom/google/android/searchcommon/preferences/PreferenceController;
    .locals 11
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->getControllerId(Landroid/preference/Preference;)Ljava/lang/String;

    move-result-object v10

    const-string v0, "google_location_settings"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/LocationSettingsController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/android/searchcommon/google/LocationSettings;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "search_sources"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SearchableItemsController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/GlobalSearchServices;Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    const-string v0, "manage_search_history"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    new-instance v8, Lcom/google/android/searchcommon/util/IntentUtilsImpl;

    invoke-direct {v8}, Lcom/google/android/searchcommon/util/IntentUtilsImpl;-><init>()V

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/SearchConfig;Landroid/app/Activity;Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/database/DataSetObservable;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/searchcommon/history/SearchHistoryHelper;)V

    goto :goto_0

    :cond_2
    const-string v0, "use_google_com"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/app/Activity;)V

    goto :goto_0

    :cond_3
    const-string v0, "clear_shortcuts"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/clicklog/ClickLog;Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;)V

    goto :goto_0

    :cond_4
    const-string v0, "language"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/searchcommon/preferences/LanguageSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/LanguageSettingController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;)V

    goto :goto_0

    :cond_5
    const-string v0, "ttsMode"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, "profanityFilter"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/searchcommon/preferences/DefaultSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/preferences/DefaultSettingController;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "downloadLanguagePacks"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mDeviceClassSupplier:Lcom/google/common/base/Supplier;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/common/base/Supplier;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "managePersonalization"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "personalizedResults"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;)V

    goto/16 :goto_0

    :cond_a
    const-string v0, "bluetoothHeadset"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v0, Lcom/google/android/searchcommon/preferences/BluetoothHeadsetSettingController;

    invoke-direct {v0}, Lcom/google/android/searchcommon/preferences/BluetoothHeadsetSettingController;-><init>()V

    goto/16 :goto_0

    :cond_b
    const-string v0, "hotwordDetector"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lcom/google/android/searchcommon/preferences/HotwordSettingController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/HotwordSettingController;-><init>(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/embedded/Greco3DataManager;)V

    goto/16 :goto_0

    :cond_c
    const-string v0, "location_tos"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Lcom/google/android/searchcommon/preferences/LocationTosPreferenceController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/preferences/LocationTosPreferenceController;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_d
    const-string v0, "personalized_search_bool"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    new-instance v0, Lcom/google/android/searchcommon/preferences/PersonalizedSearchSettingsController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/PersonalizedSearchSettingsController;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/google/gaia/LoginHelper;)V

    goto/16 :goto_0

    :cond_e
    const-string v0, "safe_search_settings"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    new-instance v0, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    goto/16 :goto_0

    :cond_f
    const-string v0, "debugS3Server"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "debugPersonalization"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "debug_force_hands_free"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "debugConfigurationDate"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "debugConfigurationExperiment"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "audioLoggingEnabled"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "debugSendLoggedAudio"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    new-instance v0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mActivity:Landroid/app/Activity;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;-><init>(Lcom/google/android/searchcommon/debug/DebugFeatures;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;Lcom/google/android/speech/utils/NetworkInformation;Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_11
    const-string v0, "debug_search_domain_override"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "debug_search_scheme_override"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_12
    new-instance v0, Lcom/google/android/searchcommon/preferences/DebugSearchController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/DebugSearchController;-><init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/debug/DebugFeatures;)V

    goto/16 :goto_0

    :cond_13
    const-string v0, "goggles.frontend"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "goggles.showDebugView"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "goggles.annotation"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "goggles.fakeFocus"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_14
    new-instance v0, Lcom/google/android/goggles/GogglesDebugPreferenceController;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-direct {v0, v1}, Lcom/google/android/goggles/GogglesDebugPreferenceController;-><init>(Lcom/google/android/goggles/GogglesSettings;)V

    goto/16 :goto_0

    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    const/4 v0, 0x0

    instance-of v2, p1, Landroid/preference/PreferenceGroup;

    if-nez v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->getControllerFor(Landroid/preference/Preference;)Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v0

    :cond_0
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->filterPreference(Landroid/preference/Preference;)Z

    move-result v1

    :cond_1
    return v1
.end method

.method protected getControllerId(Landroid/preference/Preference;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "manage_search_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "manage_location_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "google_account"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "cloud_search_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const-string v0, "manage_search_history"

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->getControllerFor(Landroid/preference/Preference;)Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->handlePreference(Landroid/preference/Preference;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v1, p1, Landroid/preference/PreferenceGroup;

    if-eqz v1, :cond_2

    check-cast p1, Landroid/preference/PreferenceGroup;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->handlePrefenceGroup(Landroid/preference/PreferenceGroup;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory$UnknownPreferenceException;

    invoke-direct {v1, p1}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory$UnknownPreferenceException;-><init>(Landroid/preference/Preference;)V

    throw v1
.end method

.method public onCreateComplete(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onCreateComplete(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onDestroy()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2, p0}, Lcom/google/android/searchcommon/SearchSettings;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onPause()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onPause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PreferenceController;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mScreen:Landroid/preference/PreferenceScreen;

    invoke-interface {v0, v2}, Lcom/google/android/searchcommon/preferences/PreferenceController;->setScreen(Landroid/preference/PreferenceScreen;)V

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onResume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->USER_ACCOUNT_SETTINGS:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasks;->forceRunInterruptingOngoing(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mControllers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onStop()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setScreen(Landroid/preference/PreferenceScreen;)V
    .locals 0
    .param p1    # Landroid/preference/PreferenceScreen;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;->mScreen:Landroid/preference/PreferenceScreen;

    return-void
.end method
