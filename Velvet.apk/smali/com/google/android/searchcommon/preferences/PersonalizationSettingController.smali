.class public Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "PersonalizationSettingController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

.field private mPreference:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mActivity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    return-void
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-interface {v0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;->isPersonalizationAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Landroid/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPreference:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-interface {v1}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    move-object v1, p2

    check-cast v1, Ljava/lang/Boolean;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.voicesearch.action.PERSONALIZATION_OPT_IN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "PERSONALIZATION_OPT_IN_ENABLE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "PersonalizationSettingController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t start personalization opt-in: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/PersonalizationSettingController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-interface {v1}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    return-void
.end method
