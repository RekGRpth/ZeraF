.class public Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "DownloadLanguagePacksSettingController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mDeviceClassSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPreference:Landroid/preference/Preference;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/SearchSettings;",
            "Landroid/app/Activity;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;->mActivity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;->mDeviceClassSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;->mDeviceClassSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;->mPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;->mPreference:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;->mPreference:Landroid/preference/Preference;

    invoke-virtual {v1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DownloadLanguagePacksSettingController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0
.end method
