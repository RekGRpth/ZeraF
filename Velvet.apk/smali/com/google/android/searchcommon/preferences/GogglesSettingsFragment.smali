.class public Lcom/google/android/searchcommon/preferences/GogglesSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;
.source "GogglesSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getPreferencesResourceId()I
    .locals 1

    const v0, 0x7f070007

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GogglesSettingsFragment;->getController()Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GogglesSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->setScreen(Landroid/preference/PreferenceScreen;)V

    return-void
.end method
