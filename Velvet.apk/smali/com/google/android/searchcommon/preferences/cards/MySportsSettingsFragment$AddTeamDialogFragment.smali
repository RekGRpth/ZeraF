.class public Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;
.super Landroid/app/DialogFragment;
.source "MySportsSettingsFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddTeamDialogFragment"
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

.field private mTeamsFilter:Landroid/widget/EditText;

.field private mTeamsListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private createSortedSportsList(Lcom/google/geo/sidekick/Sidekick$SportsTeams;)Ljava/util/List;
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportsTeams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$SportsTeams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportsTeams;->getSportTeamPlayerCount()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportsTeams;->getSportTeamPlayerList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-static {v3}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->sportTeamToLabel(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;

    invoke-direct {v4, v3, v1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;-><init>(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v2
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1    # Landroid/text/Editable;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->filterBy(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "sports_entries_extra"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v7

    invoke-static {v7}, Lcom/google/geo/sidekick/Sidekick$SportsTeams;->parseFrom([B)Lcom/google/geo/sidekick/Sidekick$SportsTeams;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$SportsTeams;->getSportTeamPlayerCount()I

    move-result v7

    if-nez v7, :cond_0

    :cond_0
    invoke-direct {p0, v5}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->createSortedSportsList(Lcom/google/geo/sidekick/Sidekick$SportsTeams;)Ljava/util/List;

    move-result-object v4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v7, 0x7f040005

    invoke-virtual {v3, v7, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f100024

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsFilter:Landroid/widget/EditText;

    const v7, 0x7f100025

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    iput-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    const v8, 0x1020004

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    new-instance v7, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v7, v8, v4}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    new-instance v8, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment$1;

    invoke-direct {v8, p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;)V

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsFilter:Landroid/widget/EditText;

    invoke-virtual {v7, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$200()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Failed to parse sports team list: "

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    const/4 v3, -0x1

    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method updateSportsEntities(Lcom/google/geo/sidekick/Sidekick$SportsTeams;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->createSortedSportsList(Lcom/google/geo/sidekick/Sidekick$SportsTeams;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->mTeamsFilter:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->afterTextChanged(Landroid/text/Editable;)V

    :cond_0
    return-void
.end method
