.class Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;
.super Ljava/lang/Object;
.source "MySportsSettingsFragment.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->loadSportsFromLocalStorage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<[B",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

.field final synthetic val$updateSportsHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->val$updateSportsHandler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->apply([B)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public apply([B)Ljava/lang/Void;
    .locals 13
    .param p1    # [B
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    const/4 v12, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_1

    :try_start_0
    new-instance v1, Lcom/google/android/apps/sidekick/StaticEntitiesData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasStaticEntities()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getStaticEntities()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;->hasSportsEntities()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getStaticEntities()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;->getSportsEntities()Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    monitor-enter v9
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    # setter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;
    invoke-static {v8, v7}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$002(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;Lcom/google/geo/sidekick/Sidekick$SportsTeams;)Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->val$updateSportsHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v4, 0x0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasLastRefreshMillis()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getLastRefreshMillis()J

    move-result-wide v2

    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v8}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$100(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J
    :try_end_2
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-wide v5

    sub-long v8, v5, v2

    const-wide/32 v10, 0x240c8400

    cmp-long v8, v8, v10

    if-lez v8, :cond_1

    const/4 v4, 0x1

    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    new-instance v8, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    invoke-direct {v8, v9}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;-><init>(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)V

    new-array v9, v12, [Ljava/lang/Void;

    invoke-virtual {v8, v9}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    const/4 v8, 0x0

    return-object v8

    :catchall_0
    move-exception v8

    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v8
    :try_end_4
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$200()Ljava/lang/String;

    move-result-object v8

    const-string v9, "File storage contained invalid data"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
