.class public Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;
.source "AtPlaceCardSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getPreferenceResourceId()I
    .locals 1

    const/high16 v0, 0x7f070000

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    const v4, 0x7f0d0093

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    const v3, 0x7f0d017c

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    const v4, 0x7f0d0094

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    const v4, 0x7f0d0095

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method
