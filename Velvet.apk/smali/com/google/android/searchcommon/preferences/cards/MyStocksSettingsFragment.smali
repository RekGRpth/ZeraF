.class public Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;
.source "MyStocksSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;,
        Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;,
        Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;,
        Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mStockInterestsPreferenceKey:Ljava/lang/String;

.field private mStocksCategory:Landroid/preference/PreferenceCategory;

.field private mWorkerFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->addStock(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->removeStock(Ljava/lang/String;)V

    return-void
.end method

.method private addStock(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)V
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->keyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStocksCategory:Landroid/preference/PreferenceCategory;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->keyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->createStockPreference(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Landroid/preference/Preference;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStocksCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->editConfiguration()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStockInterestsPreferenceKey:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->addMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->apply()V

    goto :goto_0
.end method

.method private createStockPreference(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Landroid/preference/Preference;
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->keyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setPersistent(Z)V

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const v2, 0x7f0400bb

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setLayoutResource(I)V

    invoke-virtual {v0}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "stock_preference_key"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "stock_preference_label"

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->stockToLabel(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static getStockDataFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getMessages(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$1;

    invoke-direct {v2}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$1;-><init>()V

    invoke-static {v1, v2}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$2;

    invoke-direct {v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$2;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private keyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    sget-object v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->REPEATED_MESSAGE_INFO:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    invoke-interface {v0, p1}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private removeStock(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStocksCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, p1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStocksCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStockInterestsPreferenceKey:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setDeleted(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->editConfiguration()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStockInterestsPreferenceKey:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->updateMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->apply()V

    :cond_0
    return-void
.end method

.method private declared-synchronized showAddStockDialog()Z
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;

    invoke-direct {v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "add_stock_dialog_tag"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private showRemoveStockDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;->newInstance(Landroid/app/Fragment;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "remove_team_dialog_tag"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private static stockToLabel(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    const v0, 0x7f0d024e

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSymbol()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getPreferenceResourceId()I
    .locals 1

    const v0, 0x7f07000e

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f0d006c

    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStockInterestsPreferenceKey:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    const v7, 0x7f0d005b

    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/PreferenceCategory;

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStocksCategory:Landroid/preference/PreferenceCategory;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStocksCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v6, v8}, Landroid/preference/PreferenceCategory;->setOrderingAsAdded(Z)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStockInterestsPreferenceKey:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getStockDataFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->createStockPreference(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Landroid/preference/Preference;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mStocksCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v6, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0d022f

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setTitle(I)V

    const-string v6, "add_stock_preference_key"

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Landroid/preference/Preference;->setPersistent(Z)V

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v6, "work"

    invoke-virtual {v1, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v6

    check-cast v6, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mWorkerFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mWorkerFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    if-nez v6, :cond_1

    new-instance v6, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-direct {v6}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;-><init>()V

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mWorkerFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->mWorkerFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    const-string v8, "work"

    invoke-virtual {v6, v7, v8}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentTransaction;->commit()I

    :cond_1
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    const-string v1, "add_stock_preference_key"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->showAddStockDialog()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "stock_preference_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "stock_preference_label"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->showRemoveStockDialog(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
