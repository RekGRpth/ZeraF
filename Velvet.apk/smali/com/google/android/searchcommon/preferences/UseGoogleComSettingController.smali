.class public Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "UseGoogleComSettingController.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/app/Activity;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/SearchSettings;
    .param p3    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p4    # Landroid/app/Activity;

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p4, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mActivity:Landroid/app/Activity;

    invoke-interface {p2, p0}, Lcom/google/android/searchcommon/SearchSettings;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method private getSummary(Z)Ljava/lang/String;
    .locals 5
    .param p1    # Z

    if-eqz p1, :cond_0

    const v0, 0x7f0d035a

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getLocalSearchDomain()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const v0, 0x7f0d0359

    goto :goto_0
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isDotComAnyway()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getFixedSearchDomain()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Landroid/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->getSummary(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/SearchSettings;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d035b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-string v0, "search_domain_apply_time"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->mUseGoogleComPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/UseGoogleComSettingController;->getSummary(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
