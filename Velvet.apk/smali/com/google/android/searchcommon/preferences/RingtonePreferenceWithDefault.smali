.class public Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;
.super Landroid/preference/RingtonePreference;
.source "RingtonePreferenceWithDefault.java"


# instance fields
.field private mHasCustomDefault:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->init()V

    return-void
.end method

.method private init()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->getShowDefault()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->mHasCustomDefault:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->getRingtoneType()I

    move-result v1

    invoke-static {v1}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->setDefaultValue(Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/RingtonePreferenceWithDefault;->mHasCustomDefault:Z

    invoke-super {p0, p1, p2}, Landroid/preference/RingtonePreference;->onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
