.class public final Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;
.super Ljava/lang/Object;
.source "NowConfigurationPreferences.java"

# interfaces
.implements Landroid/content/SharedPreferences$Editor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ConfigurationEditor"
.end annotation


# instance fields
.field final backingEditor:Landroid/content/SharedPreferences$Editor;

.field hasDirtyEdits:Z

.field final mAdds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;>;"
        }
    .end annotation
.end field

.field final mDeletes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mEditorLock:Ljava/lang/Object;

.field final mModified:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mAdds:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mDeletes:Ljava/util/Map;

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;
    invoke-static {p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$300(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
    .param p2    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;-><init>(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)V

    return-void
.end method

.method private doCommit()V
    .locals 28

    const/4 v12, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;
    invoke-static/range {v24 .. v24}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$600(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Ljava/lang/Object;

    move-result-object v25

    monitor-enter v25

    const/4 v4, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBackingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    move-object/from16 v26, v0

    monitor-enter v26
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mAdds:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mDeletes:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_1

    :cond_0
    const/4 v5, 0x1

    :goto_0
    if-eqz v5, :cond_2

    if-nez v22, :cond_2

    new-instance v24, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$NoBackingConfigurationException;

    invoke-direct/range {v24 .. v24}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$NoBackingConfigurationException;-><init>()V

    throw v24

    :catchall_0
    move-exception v24

    monitor-exit v26
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v24

    :catchall_1
    move-exception v24

    monitor-exit v25
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v24

    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    move-object/from16 v0, v22

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->traverseTo(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;
    invoke-static {v0, v11}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$700(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;

    move-result-object v8

    if-nez v8, :cond_4

    new-instance v24, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-direct {v0, v11, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;)V

    throw v24

    :cond_4
    invoke-virtual {v8}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->has()Z

    move-result v24

    if-eqz v24, :cond_5

    invoke-virtual {v8}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->get()Ljava/lang/Object;

    move-result-object v18

    :goto_2
    iget-object v0, v8, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v24

    sget-object v27, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v24

    move-object/from16 v0, v24

    instance-of v0, v0, Ljava/lang/String;

    move/from16 v24, v0

    if-eqz v24, :cond_6

    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    :goto_3
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_3

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->set(Ljava/lang/Object;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    const/16 v18, 0x0

    goto :goto_2

    :cond_6
    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mDeletes:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_8
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    move-object/from16 v0, v22

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->traverseTo(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;
    invoke-static {v0, v11}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$700(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;

    move-result-object v8

    if-nez v8, :cond_9

    new-instance v24, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-direct {v0, v11, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;)V

    throw v24

    :cond_9
    iget-object v0, v8, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mTarget:Lcom/google/protobuf/micro/MessageMicro;

    move-object/from16 v24, v0

    iget-object v0, v8, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    move-object/from16 v27, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getRepeatedFieldAsList(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/util/List;

    move-result-object v21

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v20

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/protobuf/micro/MessageMicro;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    invoke-static/range {v24 .. v24}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$500(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v0, v14}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-interface {v0, v1, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_a
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/util/Set;

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_b
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/protobuf/micro/MessageMicro;

    if-eqz v23, :cond_b

    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mAdds:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_d
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v22

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->traverseTo(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;
    invoke-static {v0, v11}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$700(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;

    move-result-object v8

    if-nez v8, :cond_e

    new-instance v24, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-direct {v0, v11, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;)V

    throw v24

    :cond_e
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/util/List;

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/protobuf/micro/MessageMicro;

    iget-object v0, v8, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mTarget:Lcom/google/protobuf/micro/MessageMicro;

    move-object/from16 v24, v0

    iget-object v0, v8, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    move-object/from16 v27, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-static {v0, v1, v14}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->addRepeatedField(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_6

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->clear()V

    monitor-exit v26
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_14

    const/4 v4, 0x1

    :goto_7
    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mListeners:Ljava/util/Set;
    invoke-static/range {v24 .. v24}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$800(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_10

    new-instance v13, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mListeners:Ljava/util/Set;
    invoke-static/range {v24 .. v24}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$800(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Ljava/util/Set;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v12, v13

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    move/from16 v24, v0

    if-eqz v24, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    move-object/from16 v24, v0

    const-string v26, "NowConfigurationPreferences.dirty"

    const/16 v27, 0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_11
    if-eqz v4, :cond_12

    invoke-static/range {v22 .. v22}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->encodeAsString(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-object/from16 v26, v0

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingConfigurationKey:Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$900(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_12
    monitor-exit v25
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v12, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->notifyListeners(Ljava/util/Collection;Ljava/util/Set;)V
    invoke-static {v0, v12, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$200(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/util/Collection;Ljava/util/Set;)V

    :cond_13
    return-void

    :cond_14
    const/4 v4, 0x0

    goto :goto_7
.end method


# virtual methods
.method public addMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/protobuf/micro/MessageMicro;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mAdds:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mAdds:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    monitor-exit v3

    return-object p0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public apply()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->doCommit()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public clear()Landroid/content/SharedPreferences$Editor;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public commit()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->doCommit()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-object p0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # F

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-object p0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-object p0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-object p0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-object p0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/SharedPreferences$Editor;"
        }
    .end annotation

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-object p0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mModified:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->backingEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    return-object p0

    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1
.end method

.method public removeMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/protobuf/micro/MessageMicro;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$500(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->removeMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v0

    return-object v0
.end method

.method public removeMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p1, v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$400(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mEditorLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mDeletes:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->mDeletes:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->hasDirtyEdits:Z

    monitor-exit v3

    return-object p0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public updateMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/protobuf/micro/MessageMicro;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->this$0:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    # getter for: Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    invoke-static {v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->access$500(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->removeMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->addMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    return-object p0
.end method
