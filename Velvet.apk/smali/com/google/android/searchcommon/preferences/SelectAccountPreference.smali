.class public Lcom/google/android/searchcommon/preferences/SelectAccountPreference;
.super Landroid/preference/ListPreference;
.source "SelectAccountPreference.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method setNetworkClient(Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-void
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f0d00d4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->showDialog(Landroid/os/Bundle;)V

    goto :goto_0
.end method
