.class public Lcom/google/android/searchcommon/preferences/SearchableItemsController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "SearchableItemsController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

.field protected mSourcePreferences:Landroid/preference/PreferenceGroup;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/GlobalSearchServices;Landroid/content/Context;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Lcom/google/android/searchcommon/GlobalSearchServices;
    .param p3    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->mContext:Landroid/content/Context;

    return-void
.end method

.method private createSourcePreference(Lcom/google/android/searchcommon/summons/Source;)Landroid/preference/Preference;
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    new-instance v1, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getSourceEnabledPreference(Lcom/google/android/searchcommon/summons/Source;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;->setKey(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->isEnabledByDefault()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;->setDefaultValue(Ljava/lang/Object;)V

    invoke-virtual {v1, p0}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->getSettingsDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;->setSummaryOn(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;->setSummaryOff(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->getSourceIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SearchableItemPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    return-object v1
.end method

.method private getSources()Lcom/google/android/searchcommon/summons/Sources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 0
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Landroid/preference/PreferenceGroup;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->mSourcePreferences:Landroid/preference/PreferenceGroup;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->populateSourcePreference()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->broadcastSettingsChanged()V

    const/4 v0, 0x1

    return v0
.end method

.method protected populateSourcePreference()V
    .locals 5

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->mSourcePreferences:Landroid/preference/PreferenceGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceGroup;->setOrderingAsAdded(Z)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/summons/Sources;->getSources()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/summons/Source;

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->createSourcePreference(Lcom/google/android/searchcommon/summons/Source;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SearchableItemsController;->mSourcePreferences:Landroid/preference/PreferenceGroup;

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    return-void
.end method
