.class public abstract Lcom/google/android/searchcommon/ui/BaseSuggestionView;
.super Landroid/widget/RelativeLayout;
.source "BaseSuggestionView.java"

# interfaces
.implements Lcom/google/android/searchcommon/ui/SuggestionView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/ui/BaseSuggestionView$1;,
        Lcom/google/android/searchcommon/ui/BaseSuggestionView$ClickListener;
    }
.end annotation


# instance fields
.field private mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

.field private mClickLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

.field protected mDivider:Landroid/view/View;

.field private mFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

.field protected mIcon1:Landroid/widget/ImageView;

.field protected mIcon1Mode:I

.field protected mIcon2:Landroid/widget/ImageView;

.field protected mIcon2Mode:I

.field private mListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

.field protected mText1:Landroid/widget/TextView;

.field protected mText2:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    if-eq v0, p1, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/ui/BaseSuggestionView$ClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/ui/BaseSuggestionView$ClickListener;-><init>(Lcom/google/android/searchcommon/ui/BaseSuggestionView;Lcom/google/android/searchcommon/ui/BaseSuggestionView$1;)V

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBoundSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    return-object v0
.end method

.method protected getSuggestionFormatter()Lcom/google/android/searchcommon/suggest/SuggestionFormatter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    return-object v0
.end method

.method public getText1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mText1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f100072

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mText1:Landroid/widget/TextView;

    const v0, 0x7f100073

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mText2:Landroid/widget/TextView;

    const v0, 0x7f10006f

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon1:Landroid/widget/ImageView;

    const v0, 0x7f100070

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon2:Landroid/widget/ImageView;

    const v0, 0x7f100074

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mDivider:Landroid/view/View;

    return-void
.end method

.method protected onRemoveFromHistoryClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mClickLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/ui/SuggestionClickListener;->onSuggestionRemoveFromHistoryClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V

    return-void
.end method

.method protected onSuggestionClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mClickLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/ui/SuggestionClickListener;->onSuggestionClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V

    return-void
.end method

.method protected onSuggestionQueryRefineClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mClickLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/ui/SuggestionClickListener;->onSuggestionQueryRefineClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V

    return-void
.end method

.method protected onSuggestionQuickContactClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mBoundSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mClickLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/ui/SuggestionClickListener;->onSuggestionQuickContactClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V

    return-void
.end method

.method public setClickListener(Lcom/google/android/searchcommon/ui/SuggestionClickListener;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/ui/SuggestionClickListener;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    iput-object p2, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mClickLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    return-void
.end method

.method public setDividerVisibility(I)V
    .locals 1
    .param p1    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mDivider:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1    # Z

    const/high16 v2, 0x3f800000

    const/high16 v1, 0x3f000000

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon1:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon2:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon1:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon2:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method public setIconModes(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon1Mode:I

    iput p2, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mIcon2Mode:I

    return-void
.end method

.method public setSuggestionFormatter(Lcom/google/android/searchcommon/suggest/SuggestionFormatter;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    return-void
.end method

.method protected setText1(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mText1:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setText2(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mText2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mText2:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->mText2:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
