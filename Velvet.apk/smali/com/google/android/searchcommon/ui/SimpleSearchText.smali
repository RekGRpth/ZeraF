.class public Lcom/google/android/searchcommon/ui/SimpleSearchText;
.super Landroid/widget/EditText;
.source "SimpleSearchText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/ui/SimpleSearchText$1;,
        Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;
    }
.end annotation


# instance fields
.field mCallbacksSuspended:Z

.field private mEditStartedInTouchMode:Z

.field private mIsVoiceQueryMode:Z

.field mSelectedQueryCorrectionSpan:Lcom/google/android/searchcommon/suggest/CorrectionSpan;

.field mSpannedQuery:Landroid/text/SpannedString;

.field mTextChangeWatcher:Lcom/google/android/searchcommon/util/TextChangeWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mCallbacksSuspended:Z

    return-void
.end method

.method private getTouchedCorrectionSpan(Landroid/view/MotionEvent;)Lcom/google/android/searchcommon/suggest/CorrectionSpan;
    .locals 4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getOffsetForPosition(FF)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v2

    const-class v3, Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    invoke-interface {v2, v0, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public addQueryTextWatcher(Lcom/google/android/searchcommon/util/TextChangeWatcher;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/TextChangeWatcher;

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mTextChangeWatcher:Lcom/google/android/searchcommon/util/TextChangeWatcher;

    return-void
.end method

.method public getQuery()Ljava/lang/CharSequence;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public hasFocusFromKeyboard()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mEditStartedInTouchMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3
    .param p1    # Landroid/view/inputmethod/EditorInfo;

    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, -0x40000001

    and-int/2addr v1, v2

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    and-int/lit16 v1, v1, -0x100

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v1, v1, 0x3

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    return-object v0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # Landroid/graphics/Rect;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->isInTouchMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mEditStartedInTouchMode:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mTextChangeWatcher:Lcom/google/android/searchcommon/util/TextChangeWatcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mTextChangeWatcher:Lcom/google/android/searchcommon/util/TextChangeWatcher;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/TextChangeWatcher;->onTextEditStarted()V

    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/EditText;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    instance-of v1, p1, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;

    if-nez v1, :cond_1

    invoke-super {p0, p1}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v1, v0, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;->mSpannedText:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    new-instance v1, Landroid/text/SpannedString;

    iget-object v2, v0, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;->mSpannedText:Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSpannedQuery:Landroid/text/SpannedString;

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/widget/EditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSpannedQuery:Landroid/text/SpannedString;

    iput-object v2, v0, Lcom/google/android/searchcommon/ui/SimpleSearchText$SavedState;->mSpannedText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mTextChangeWatcher:Lcom/google/android/searchcommon/util/TextChangeWatcher;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mCallbacksSuspended:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mTextChangeWatcher:Lcom/google/android/searchcommon/util/TextChangeWatcher;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/TextChangeWatcher;->onTextChanged(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getTouchedCorrectionSpan(Landroid/view/MotionEvent;)Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSelectedQueryCorrectionSpan:Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getTouchedCorrectionSpan(Landroid/view/MotionEvent;)Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSelectedQueryCorrectionSpan:Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    if-ne v1, v6, :cond_0

    invoke-virtual {v6}, Lcom/google/android/searchcommon/suggest/CorrectionSpan;->getCorrection()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v6}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    invoke-interface {v0, v6}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    :cond_0
    iput-object v7, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSelectedQueryCorrectionSpan:Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    goto :goto_0

    :pswitch_3
    iput-object v7, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSelectedQueryCorrectionSpan:Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setQuery(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    new-instance v0, Landroid/text/SpannedString;

    invoke-direct {v0, p1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSpannedQuery:Landroid/text/SpannedString;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public setTextQueryCorrections(Landroid/text/Spanned;)V
    .locals 8

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v1, Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    invoke-interface {p1, v2, v0, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v3, Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    invoke-interface {v4, v2, v1, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    array-length v5, v1

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v1, v3

    invoke-interface {v4, v6}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_2

    aget-object v2, v0, v1

    invoke-interface {p1, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {p1, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/CorrectionSpan;->getCorrection()Ljava/lang/String;

    invoke-interface {p1, v3, v5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v7

    if-lt v7, v5, :cond_1

    invoke-interface {v4, v3, v5}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/16 v6, 0x21

    invoke-interface {v4, v2, v3, v5, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public showTextQueryMode()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mIsVoiceQueryMode:Z

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mCallbacksSuspended:Z

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setSingleLine(Z)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getInputType()I

    move-result v1

    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setInputType(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setTextQueryCorrections(Landroid/text/Spanned;)V

    iput-boolean v3, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mCallbacksSuspended:Z

    iput-boolean v3, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mIsVoiceQueryMode:Z

    :cond_0
    return-void
.end method

.method public showVoiceQueryMode()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mIsVoiceQueryMode:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mCallbacksSuspended:Z

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setSingleLine(Z)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setLines(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getInputType()I

    move-result v0

    const v1, -0x80001

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setInputType(I)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mSpannedQuery:Landroid/text/SpannedString;

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v2, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mCallbacksSuspended:Z

    iput-boolean v3, p0, Lcom/google/android/searchcommon/ui/SimpleSearchText;->mIsVoiceQueryMode:Z

    :cond_0
    return-void
.end method
