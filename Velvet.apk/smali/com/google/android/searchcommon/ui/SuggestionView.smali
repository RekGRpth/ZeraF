.class public interface abstract Lcom/google/android/searchcommon/ui/SuggestionView;
.super Ljava/lang/Object;
.source "SuggestionView.java"


# virtual methods
.method public abstract bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z
.end method

.method public abstract getBoundSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;
.end method

.method public abstract setClickListener(Lcom/google/android/searchcommon/ui/SuggestionClickListener;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
.end method

.method public abstract setDividerVisibility(I)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setIconModes(II)V
.end method

.method public abstract setSuggestionFormatter(Lcom/google/android/searchcommon/suggest/SuggestionFormatter;)V
.end method
