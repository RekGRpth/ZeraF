.class public interface abstract Lcom/google/android/searchcommon/ui/SuggestionClickListener;
.super Ljava/lang/Object;
.source "SuggestionClickListener.java"


# virtual methods
.method public abstract onSuggestionClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
.end method

.method public abstract onSuggestionQueryRefineClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
.end method

.method public abstract onSuggestionQuickContactClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
.end method

.method public abstract onSuggestionRemoveFromHistoryClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
.end method
