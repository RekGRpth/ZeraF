.class public Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;
.super Lcom/google/android/searchcommon/ui/BaseSuggestionView;
.source "WebSearchSuggestionView.java"


# instance fields
.field private mBoundSuggestionForQuery:Ljava/lang/String;

.field private mIconAndText1:Landroid/view/View;

.field private mRemoveFromHistoryButton:Landroid/widget/ImageView;

.field private mRemoveFromHistoryButtonAnimation:Lcom/google/android/searchcommon/ui/util/DisablingAnimator;

.field private mShowingRemoveFromHistory:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private setIsHistorySuggestion(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    const/16 v1, 0x8

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mShowingRemoveFromHistory:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->startRemoveFromHistoryButtonAnimation()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->stopRemoveFromHistoryButtonAnimation()V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->stopRemoveFromHistoryButtonAnimation()V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private startRemoveFromHistoryButtonAnimation()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButtonAnimation:Lcom/google/android/searchcommon/ui/util/DisablingAnimator;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/ui/util/DisablingAnimator;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    const/high16 v2, 0x7f060000

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/ui/util/DisablingAnimator;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButtonAnimation:Lcom/google/android/searchcommon/ui/util/DisablingAnimator;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButtonAnimation:Lcom/google/android/searchcommon/ui/util/DisablingAnimator;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/util/DisablingAnimator;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mShowingRemoveFromHistory:Z

    return-void
.end method

.method private stopRemoveFromHistoryButtonAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButtonAnimation:Lcom/google/android/searchcommon/ui/util/DisablingAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButtonAnimation:Lcom/google/android/searchcommon/ui/util/DisablingAnimator;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/util/DisablingAnimator;->end()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mShowingRemoveFromHistory:Z

    return-void
.end method


# virtual methods
.method public bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z
    .locals 7
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mBoundSuggestionForQuery:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p0, v1, p2}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->formatSuggestion(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->setText1(Ljava/lang/CharSequence;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mBoundSuggestionForQuery:Ljava/lang/String;

    :cond_1
    if-nez v0, :cond_2

    iget-boolean v2, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mShowingRemoveFromHistory:Z

    if-eq v2, p3, :cond_3

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isHistorySuggestion()Z

    move-result v2

    invoke-direct {p0, v2, p3}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->setIsHistorySuggestion(ZZ)V

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d035f

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_3
    return v0
.end method

.method protected formatSuggestion(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/text/Spanned;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->getSuggestionFormatter()Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/searchcommon/suggest/SuggestionFormatter;->formatSuggestion(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->onFinishInflate()V

    const v0, 0x7f100290

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    const v0, 0x7f100291

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView$1;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView$1;-><init>(Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    new-instance v1, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView$2;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView$2;-><init>(Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mRemoveFromHistoryButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method

.method public setLongClickable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setLongClickable(Z)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnFocusChangeListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnKeyListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnLongClickListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/WebSearchSuggestionView;->mIconAndText1:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method
