.class Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;
.super Ljava/lang/Object;
.source "SuggestedTextController.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/ui/SuggestedTextController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BufferTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/ui/SuggestedTextController;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/ui/SuggestedTextController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;->this$0:Lcom/google/android/searchcommon/ui/SuggestedTextController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/ui/SuggestedTextController;Lcom/google/android/searchcommon/ui/SuggestedTextController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/ui/SuggestedTextController;
    .param p2    # Lcom/google/android/searchcommon/ui/SuggestedTextController$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;-><init>(Lcom/google/android/searchcommon/ui/SuggestedTextController;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1    # Landroid/text/Editable;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;->this$0:Lcom/google/android/searchcommon/ui/SuggestedTextController;

    # invokes: Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V
    invoke-static {v0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->access$200(Lcom/google/android/searchcommon/ui/SuggestedTextController;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;->this$0:Lcom/google/android/searchcommon/ui/SuggestedTextController;

    # invokes: Lcom/google/android/searchcommon/ui/SuggestedTextController;->handleTextChanged(Landroid/text/Editable;)V
    invoke-static {v0, p1}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->access$300(Lcom/google/android/searchcommon/ui/SuggestedTextController;Landroid/text/Editable;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;->this$0:Lcom/google/android/searchcommon/ui/SuggestedTextController;

    # invokes: Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V
    invoke-static {v0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->access$200(Lcom/google/android/searchcommon/ui/SuggestedTextController;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;->this$0:Lcom/google/android/searchcommon/ui/SuggestedTextController;

    # getter for: Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;
    invoke-static {v0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->access$400(Lcom/google/android/searchcommon/ui/SuggestedTextController;)Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;->this$0:Lcom/google/android/searchcommon/ui/SuggestedTextController;

    new-instance v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    invoke-direct {v1, p2, p4, p3}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;-><init>(III)V

    # setter for: Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;
    invoke-static {v0, v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->access$402(Lcom/google/android/searchcommon/ui/SuggestedTextController;Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;)Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
