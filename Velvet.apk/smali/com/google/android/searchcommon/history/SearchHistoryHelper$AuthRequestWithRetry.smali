.class abstract Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;
.super Ljava/lang/Object;
.source "SearchHistoryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/history/SearchHistoryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "AuthRequestWithRetry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/searchcommon/history/SearchHistoryHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p2    # Lcom/google/android/searchcommon/history/SearchHistoryHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;-><init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/Account;)Ljava/lang/Object;
    .locals 7
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v4, 0x2

    if-ge v2, v4, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    # invokes: Lcom/google/android/searchcommon/history/SearchHistoryHelper;->getHeaders(Landroid/accounts/Account;)Ljava/util/Map;
    invoke-static {v4, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->access$800(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;)Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_1
    return-object v3

    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;->runRequest(Ljava/util/Map;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/HttpHelper$HttpException;->getStatusCode()I

    move-result v4

    const/16 v5, 0x193

    if-ne v4, v5, :cond_2

    const-string v4, "Search.SearchHistoryHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Authorization exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    # getter for: Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    invoke-static {v4}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->access$900(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v4

    const-string v5, "oauth2:https://www.googleapis.com/auth/webhistory"

    invoke-virtual {v4, v5}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->invalidateAuthToken(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    throw v0
.end method

.method public abstract runRequest(Ljava/util/Map;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation
.end method
