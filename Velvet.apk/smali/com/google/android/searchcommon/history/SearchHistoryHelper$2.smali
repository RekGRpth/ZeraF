.class Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;
.super Lcom/google/android/searchcommon/history/SearchHistoryHelper$BgTask;
.source "SearchHistoryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/history/SearchHistoryHelper;->getHistoryEnabled(Landroid/accounts/Account;Lcom/google/android/searchcommon/util/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/history/SearchHistoryHelper$BgTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$consumer:Lcom/google/android/searchcommon/util/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$BgTask;-><init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v4, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->val$account:Landroid/accounts/Account;

    # invokes: Lcom/google/android/searchcommon/history/SearchHistoryHelper;->getHistoryEnabled(Landroid/accounts/Account;)Ljava/lang/Boolean;
    invoke-static {v3, v4}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->access$400(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v3, "Search.SearchHistoryHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get search history setting: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "Search.SearchHistoryHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get search history setting: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
