.class public Lcom/google/android/searchcommon/debug/DebugFeatures;
.super Ljava/lang/Object;
.source "DebugFeatures.java"


# static fields
.field private static final sInstance:Lcom/google/android/searchcommon/debug/DebugFeatures;


# instance fields
.field private mDebugLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-direct {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/debug/DebugFeatures;->sInstance:Lcom/google/android/searchcommon/debug/DebugFeatures;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/debug/DebugFeatures;->mDebugLevel:I

    return-void
.end method

.method public static getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;
    .locals 2

    sget-object v0, Lcom/google/android/searchcommon/debug/DebugFeatures;->sInstance:Lcom/google/android/searchcommon/debug/DebugFeatures;

    iget v0, v0, Lcom/google/android/searchcommon/debug/DebugFeatures;->mDebugLevel:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot get instance because the debug level is not yet set."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    sget-object v0, Lcom/google/android/searchcommon/debug/DebugFeatures;->sInstance:Lcom/google/android/searchcommon/debug/DebugFeatures;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setDebugLevel(I)V
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/google/android/searchcommon/debug/DebugFeatures;->sInstance:Lcom/google/android/searchcommon/debug/DebugFeatures;

    iput p0, v0, Lcom/google/android/searchcommon/debug/DebugFeatures;->mDebugLevel:I

    return-void
.end method


# virtual methods
.method public dogfoodDebugEnabled()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/searchcommon/debug/DebugFeatures;->mDebugLevel:I

    if-lt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public teamDebugEnabled()Z
    .locals 2

    iget v0, p0, Lcom/google/android/searchcommon/debug/DebugFeatures;->mDebugLevel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
