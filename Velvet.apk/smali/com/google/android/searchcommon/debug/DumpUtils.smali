.class public Lcom/google/android/searchcommon/debug/DumpUtils;
.super Ljava/lang/Object;
.source "DumpUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V
    .locals 6
    .param p0    # Ljava/io/PrintWriter;
    .param p1    # [Ljava/lang/Object;

    if-nez p0, :cond_0

    const-string v4, "Search.DumpUtils"

    const-string v5, "Null PrintWriter"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    if-nez v3, :cond_1

    const-string v4, "null"

    :goto_2
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Ljava/io/PrintWriter;->println()V

    goto :goto_0
.end method
