.class public abstract Lcom/google/android/searchcommon/util/SynchronousLoader;
.super Ljava/lang/Object;
.source "SynchronousLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/UriLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/UriLoader",
        "<TA;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<TA;>;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/SynchronousLoader;->loadNow(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/util/Now;->returnThis(Ljava/lang/Object;)Lcom/google/android/searchcommon/util/Now;

    move-result-object v0

    return-object v0
.end method

.method public abstract loadNow(Landroid/net/Uri;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")TA;"
        }
    .end annotation
.end method
