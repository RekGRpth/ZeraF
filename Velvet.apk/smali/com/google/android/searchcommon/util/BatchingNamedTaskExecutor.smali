.class public Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;
.super Ljava/lang/Object;
.source "BatchingNamedTaskExecutor.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/NamedTaskExecutor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;
    }
.end annotation


# instance fields
.field private final mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private final mQueuedTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/searchcommon/util/NamedTask;",
            ">;"
        }
    .end annotation
.end field

.field private final mRunningTasks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;

    return-object v0
.end method

.method private dispatch(Lcom/google/android/searchcommon/util/NamedTask;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/util/NamedTask;

    new-instance v0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;-><init>(Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;Lcom/google/android/searchcommon/util/NamedTask;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private static getNextBatch(Ljava/util/List;I[Lcom/google/android/searchcommon/util/NamedTask;)[Lcom/google/android/searchcommon/util/NamedTask;
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/searchcommon/util/NamedTask;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I[TT;)[TT;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v3, 0x0

    invoke-interface {p0, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, [Lcom/google/android/searchcommon/util/NamedTask;

    move-object p2, v0

    invoke-interface {v2}, Ljava/util/List;->clear()V

    monitor-exit p0

    return-object p2

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method


# virtual methods
.method public cancelPendingTasks()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->cancelPendingTasks()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public cancelRunningTasks()V
    .locals 4

    iget-object v3, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/NamedTask;->cancelExecution()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public close()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->cancelPendingTasks()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->cancelRunningTasks()V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->close()V

    return-void
.end method

.method public execute(Lcom/google/android/searchcommon/util/NamedTask;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/NamedTask;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public executeNextBatch(I)I
    .locals 7
    .param p1    # I

    iget-object v5, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/google/android/searchcommon/util/NamedTask;

    invoke-static {v5, p1, v6}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->getNextBatch(Ljava/util/List;I[Lcom/google/android/searchcommon/util/NamedTask;)[Lcom/google/android/searchcommon/util/NamedTask;

    move-result-object v1

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    invoke-direct {p0, v4}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->dispatch(Lcom/google/android/searchcommon/util/NamedTask;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    array-length v5, v1

    return v5
.end method

.method public submit(Lcom/google/android/searchcommon/util/NamedTask;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/NamedTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/NamedTask;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "submit() method not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
