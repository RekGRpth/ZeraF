.class public final Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;
.super Lcom/google/android/searchcommon/util/ChunkProducer$DataChunk;
.source "ChunkProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/ChunkProducer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ByteDataChunk"
.end annotation


# instance fields
.field private final mData:[B

.field private final mLength:I


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1    # [B

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;-><init>([BI)V

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 3
    .param p1    # [B
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/ChunkProducer$DataChunk;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, -0x1

    if-lt p2, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    array-length v0, p1

    if-gt p2, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mData:[B

    iput p2, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mLength:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mData:[B

    check-cast p1, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;

    iget-object v1, p1, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mData:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDataLength()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mData:[B

    array-length v0, v0

    return v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 4

    iget v0, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mLength:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mData:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mData:[B

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;->mLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    goto :goto_0
.end method
