.class public Lcom/google/android/searchcommon/util/StateMachine$Builder;
.super Ljava/lang/Object;
.source "StateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/StateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Enum",
        "<TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mDebug:Z

.field private final mInitialState:Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mLegalTransitions:Lcom/google/common/collect/HashMultimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/HashMultimap",
            "<TT;TT;>;"
        }
    .end annotation
.end field

.field private mOneThread:Z

.field private mStrictMode:Z

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Enum;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mLegalTransitions:Lcom/google/common/collect/HashMultimap;

    iput-boolean v1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mStrictMode:Z

    iput-boolean v1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mOneThread:Z

    iput-boolean v1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mDebug:Z

    iput-object p2, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mInitialState:Ljava/lang/Enum;

    iput-object p1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)",
            "Lcom/google/android/searchcommon/util/StateMachine$Builder",
            "<TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mLegalTransitions:Lcom/google/common/collect/HashMultimap;

    invoke-virtual {v0, p1, p2}, Lcom/google/common/collect/HashMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/searchcommon/util/StateMachine;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/StateMachine;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mTag:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mInitialState:Ljava/lang/Enum;

    iget-boolean v3, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mStrictMode:Z

    iget-boolean v4, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mOneThread:Z

    iget-object v5, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mLegalTransitions:Lcom/google/common/collect/HashMultimap;

    iget-boolean v6, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mDebug:Z

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/searchcommon/util/StateMachine;-><init>(Ljava/lang/String;Ljava/lang/Enum;ZZLcom/google/common/collect/HashMultimap;ZLcom/google/android/searchcommon/util/StateMachine$1;)V

    return-object v0
.end method

.method public setDebug(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;
    .locals 0
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/searchcommon/util/StateMachine$Builder",
            "<TT;>;"
        }
    .end annotation

    iput-boolean p1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mDebug:Z

    return-object p0
.end method

.method public setSingleThreadOnly(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;
    .locals 0
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/searchcommon/util/StateMachine$Builder",
            "<TT;>;"
        }
    .end annotation

    iput-boolean p1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mOneThread:Z

    return-object p0
.end method

.method public setStrictMode(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;
    .locals 0
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/searchcommon/util/StateMachine$Builder",
            "<TT;>;"
        }
    .end annotation

    iput-boolean p1, p0, Lcom/google/android/searchcommon/util/StateMachine$Builder;->mStrictMode:Z

    return-object p0
.end method
