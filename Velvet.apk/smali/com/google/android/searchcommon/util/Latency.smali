.class public Lcom/google/android/searchcommon/util/Latency;
.super Ljava/lang/Object;
.source "Latency.java"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mStartTime:J


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/Clock;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/Latency;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/Latency;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/searchcommon/util/Latency;->mStartTime:J

    return-void
.end method


# virtual methods
.method public getLatency()I
    .locals 4

    iget-object v2, p0, Lcom/google/android/searchcommon/util/Latency;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/searchcommon/util/Latency;->mStartTime:J

    sub-long v2, v0, v2

    long-to-int v2, v2

    return v2
.end method

.method protected getStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/searchcommon/util/Latency;->mStartTime:J

    return-wide v0
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/Latency;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/searchcommon/util/Latency;->mStartTime:J

    return-void
.end method
