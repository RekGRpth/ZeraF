.class public interface abstract Lcom/google/android/searchcommon/util/IntentStarter;
.super Ljava/lang/Object;
.source "IntentStarter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;
    }
.end annotation


# virtual methods
.method public abstract getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation
.end method

.method public abstract startActivity(Landroid/content/Intent;)Z
.end method

.method public abstract startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z
.end method
