.class public interface abstract Lcom/google/android/searchcommon/util/NamedTaskExecutor;
.super Ljava/lang/Object;
.source "NamedTaskExecutor.java"


# virtual methods
.method public abstract cancelPendingTasks()V
.end method

.method public abstract close()V
.end method

.method public abstract execute(Lcom/google/android/searchcommon/util/NamedTask;)V
.end method

.method public abstract submit(Lcom/google/android/searchcommon/util/NamedTask;)Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/NamedTask;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end method
