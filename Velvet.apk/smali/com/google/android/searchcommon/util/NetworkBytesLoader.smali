.class public Lcom/google/android/searchcommon/util/NetworkBytesLoader;
.super Lcom/google/android/searchcommon/util/SynchronousLoader;
.source "NetworkBytesLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/SynchronousLoader",
        "<[B>;"
    }
.end annotation


# instance fields
.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mTrafficTag:I


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/HttpHelper;I)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p2    # I

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/SynchronousLoader;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/NetworkBytesLoader;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput p2, p0, Lcom/google/android/searchcommon/util/NetworkBytesLoader;->mTrafficTag:I

    return-void
.end method


# virtual methods
.method public bridge synthetic loadNow(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/NetworkBytesLoader;->loadNow(Landroid/net/Uri;)[B

    move-result-object v0

    return-object v0
.end method

.method public loadNow(Landroid/net/Uri;)[B
    .locals 5
    .param p1    # Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v3, "http"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v3, "https"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/NetworkBytesLoader;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    new-instance v3, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/searchcommon/util/NetworkBytesLoader;->mTrafficTag:I

    invoke-interface {v1, v3, v4}, Lcom/google/android/searchcommon/util/HttpHelper;->rawGet(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)[B
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    :goto_1
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Search.NetworkLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "Search.NetworkLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    goto :goto_1
.end method
