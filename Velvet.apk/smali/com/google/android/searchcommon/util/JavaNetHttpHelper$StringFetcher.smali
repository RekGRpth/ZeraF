.class Lcom/google/android/searchcommon/util/JavaNetHttpHelper$StringFetcher;
.super Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
.source "JavaNetHttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/JavaNetHttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StringFetcher"
.end annotation


# instance fields
.field public mResponse:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/util/JavaNetHttpHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$1;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$StringFetcher;-><init>()V

    return-void
.end method


# virtual methods
.method public fetchResponse(Ljava/net/HttpURLConnection;[B)V
    .locals 9
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$StringFetcher;->sendRequest(Ljava/net/HttpURLConnection;[B)V

    # invokes: Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->extractCharset(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->access$200(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v3

    const/16 v6, 0x200

    const/16 v7, 0x2000

    mul-int/lit8 v8, v3, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/common/io/ByteStreams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    # invokes: Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->makeConnectionReusable(Ljava/io/InputStream;)V
    invoke-static {v4}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->access$300(Ljava/io/InputStream;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v6, v7, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v6, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$StringFetcher;->mResponse:Ljava/lang/String;

    return-void

    :catchall_0
    move-exception v6

    # invokes: Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->makeConnectionReusable(Ljava/io/InputStream;)V
    invoke-static {v4}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->access$300(Ljava/io/InputStream;)V

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v6
.end method
