.class public Lcom/google/android/searchcommon/util/LevenshteinDistance;
.super Ljava/lang/Object;
.source "LevenshteinDistance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;,
        Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;
    }
.end annotation


# instance fields
.field private final mDistanceTable:[[I

.field private final mEditTypeTable:[[I

.field private final mSource:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

.field private final mTarget:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;


# direct methods
.method public constructor <init>([Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;)V
    .locals 8
    .param p1    # [Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;
    .param p2    # [Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    const/4 v7, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v3, p1

    array-length v4, p2

    add-int/lit8 v5, v3, 0x1

    add-int/lit8 v6, v4, 0x1

    filled-new-array {v5, v6}, [I

    move-result-object v5

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v6, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[I

    add-int/lit8 v5, v3, 0x1

    add-int/lit8 v6, v4, 0x1

    filled-new-array {v5, v6}, [I

    move-result-object v5

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v6, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    aget-object v5, v1, v7

    const/4 v6, 0x3

    aput v6, v5, v7

    aget-object v5, v0, v7

    aput v7, v5, v7

    const/4 v2, 0x1

    :goto_0
    if-gt v2, v3, :cond_0

    aget-object v5, v1, v2

    aput v7, v5, v7

    aget-object v5, v0, v2

    aput v2, v5, v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-gt v2, v4, :cond_1

    aget-object v5, v1, v7

    const/4 v6, 0x1

    aput v6, v5, v2

    aget-object v5, v0, v7

    aput v2, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iput-object v1, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mEditTypeTable:[[I

    iput-object v0, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mDistanceTable:[[I

    iput-object p1, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mSource:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mTarget:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    return-void
.end method


# virtual methods
.method public calculate()I
    .locals 17

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mSource:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mTarget:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    array-length v7, v9

    array-length v11, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mDistanceTable:[[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mEditTypeTable:[[I

    const/4 v6, 0x1

    :goto_0
    if-gt v6, v7, :cond_5

    add-int/lit8 v15, v6, -0x1

    aget-object v8, v9, v15

    const/4 v10, 0x1

    :goto_1
    if-gt v10, v11, :cond_4

    add-int/lit8 v15, v10, -0x1

    aget-object v12, v13, v15

    invoke-virtual {v8, v12}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->prefixOf(Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;)Z

    move-result v15

    if-eqz v15, :cond_2

    const/4 v1, 0x0

    :goto_2
    add-int/lit8 v15, v6, -0x1

    aget-object v15, v3, v15

    aget v15, v15, v10

    add-int/lit8 v4, v15, 0x1

    const/4 v14, 0x0

    aget-object v15, v3, v6

    add-int/lit8 v16, v10, -0x1

    aget v2, v15, v16

    add-int/lit8 v15, v2, 0x1

    if-ge v15, v4, :cond_0

    add-int/lit8 v4, v2, 0x1

    const/4 v14, 0x1

    :cond_0
    add-int/lit8 v15, v6, -0x1

    aget-object v15, v3, v15

    add-int/lit8 v16, v10, -0x1

    aget v2, v15, v16

    add-int v15, v2, v1

    if-ge v15, v4, :cond_1

    add-int v4, v2, v1

    if-nez v1, :cond_3

    const/4 v14, 0x3

    :cond_1
    :goto_3
    aget-object v15, v3, v6

    aput v4, v15, v10

    aget-object v15, v5, v6

    aput v14, v15, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_2

    :cond_3
    const/4 v14, 0x2

    goto :goto_3

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_5
    aget-object v15, v3, v7

    aget v15, v15, v11

    return v15
.end method

.method public getTargetOperations()[Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;
    .locals 7

    iget-object v6, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mTarget:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    array-length v5, v6

    new-array v2, v5, [Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;

    move v4, v5

    iget-object v6, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mSource:[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    array-length v3, v6

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance;->mEditTypeTable:[[I

    :goto_0
    if-lez v4, :cond_0

    aget-object v6, v0, v3

    aget v1, v6, v4

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :pswitch_1
    add-int/lit8 v4, v4, -0x1

    new-instance v6, Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;

    invoke-direct {v6, v1, v3}, Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;-><init>(II)V

    aput-object v6, v2, v4

    goto :goto_0

    :pswitch_2
    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v3, v3, -0x1

    new-instance v6, Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;

    invoke-direct {v6, v1, v3}, Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;-><init>(II)V

    aput-object v6, v2, v4

    goto :goto_0

    :cond_0
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
