.class final Lcom/google/android/searchcommon/util/Consumers$5;
.super Ljava/lang/Object;
.source "Consumers.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/util/Consumers;->createAsyncConsumer(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<TA;>;"
    }
.end annotation


# instance fields
.field final synthetic val$consumer:Lcom/google/android/searchcommon/util/Consumer;

.field final synthetic val$executor:Ljava/util/concurrent/Executor;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/util/Consumers$5;->val$executor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/Consumers$5;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/Consumers$5;->val$executor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/Consumers$5;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-static {v0, v1, p1}, Lcom/google/android/searchcommon/util/Consumers;->consumeAsync(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method
