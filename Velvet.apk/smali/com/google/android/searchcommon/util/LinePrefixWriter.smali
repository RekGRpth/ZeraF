.class public Lcom/google/android/searchcommon/util/LinePrefixWriter;
.super Ljava/io/FilterWriter;
.source "LinePrefixWriter.java"


# instance fields
.field private mNextWriteNeedsPrefix:Z

.field private mPrefix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/Writer;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/Writer;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/io/FilterWriter;-><init>(Ljava/io/Writer;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mNextWriteNeedsPrefix:Z

    iput-object p2, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mPrefix:Ljava/lang/String;

    return-void
.end method

.method private writePrefixIfNeeded()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mNextWriteNeedsPrefix:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->out:Ljava/io/Writer;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mNextWriteNeedsPrefix:Z

    return-void
.end method


# virtual methods
.method public addToPrefix(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mPrefix:Ljava/lang/String;

    return-void
.end method

.method public write(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/LinePrefixWriter;->writePrefixIfNeeded()V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->out:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(I)V

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mNextWriteNeedsPrefix:Z

    :cond_0
    return-void
.end method

.method public write(Ljava/lang/String;II)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v5, 0xa

    move v1, p2

    add-int v0, p2, p3

    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    :goto_0
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/LinePrefixWriter;->writePrefixIfNeeded()V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->out:Ljava/io/Writer;

    sub-int v4, v2, v1

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, p1, v1, v4}, Ljava/io/Writer;->write(Ljava/lang/String;II)V

    add-int/lit8 v1, v2, 0x1

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mNextWriteNeedsPrefix:Z

    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    goto :goto_0

    :cond_0
    if-ge v1, v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/LinePrefixWriter;->writePrefixIfNeeded()V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->out:Ljava/io/Writer;

    sub-int v4, v0, v1

    invoke-virtual {v3, p1, v1, v4}, Ljava/io/Writer;->write(Ljava/lang/String;II)V

    :cond_1
    return-void
.end method

.method public write([CII)V
    .locals 5
    .param p1    # [C
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move v1, p2

    move v2, p2

    add-int v0, p2, p3

    :goto_0
    if-ge v2, v0, :cond_1

    const/16 v3, 0xa

    aget-char v4, p1, v2

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/LinePrefixWriter;->writePrefixIfNeeded()V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->out:Ljava/io/Writer;

    sub-int v4, v2, v1

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, p1, v1, v4}, Ljava/io/Writer;->write([CII)V

    add-int/lit8 v1, v2, 0x1

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->mNextWriteNeedsPrefix:Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-ge v1, v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/LinePrefixWriter;->writePrefixIfNeeded()V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/LinePrefixWriter;->out:Ljava/io/Writer;

    sub-int v4, v0, v1

    invoke-virtual {v3, p1, v1, v4}, Ljava/io/Writer;->write([CII)V

    :cond_2
    return-void
.end method
