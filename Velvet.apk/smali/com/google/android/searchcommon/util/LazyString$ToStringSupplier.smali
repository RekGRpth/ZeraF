.class Lcom/google/android/searchcommon/util/LazyString$ToStringSupplier;
.super Ljava/lang/Object;
.source "LazyString.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/LazyString;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ToStringSupplier"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final mArguments:[Ljava/lang/Object;

.field private final mFormat:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/LazyString$ToStringSupplier;->mFormat:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/LazyString$ToStringSupplier;->mArguments:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/LazyString$ToStringSupplier;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LazyString$ToStringSupplier;->mFormat:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/LazyString$ToStringSupplier;->mArguments:[Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
