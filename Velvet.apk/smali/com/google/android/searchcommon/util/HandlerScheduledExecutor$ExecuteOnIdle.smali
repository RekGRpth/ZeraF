.class Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;
.super Ljava/lang/Object;
.source "HandlerScheduledExecutor.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExecuteOnIdle"
.end annotation


# instance fields
.field private final mCommand:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;Ljava/lang/Runnable;)V
    .locals 0
    .param p2    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;->this$0:Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;->mCommand:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public queueIdle()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;->this$0:Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;->mCommand:Ljava/lang/Runnable;

    # invokes: Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->dequeueIdleHandler(Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;
    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->access$000(Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;->this$0:Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;->mCommand:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    return v0
.end method
