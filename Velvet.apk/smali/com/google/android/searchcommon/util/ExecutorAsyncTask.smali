.class public abstract Lcom/google/android/searchcommon/util/ExecutorAsyncTask;
.super Ljava/lang/Object;
.source "ExecutorAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mMainThread:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    new-instance v0, Lcom/google/android/searchcommon/util/NamingTaskExecutor;

    invoke-direct {v0, p2, p3}, Lcom/google/android/searchcommon/util/NamingTaskExecutor;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;->mMainThread:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;->mExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/util/ExecutorAsyncTask;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/util/ExecutorAsyncTask;
    .param p1    # Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;->postResult(Ljava/lang/Object;)V

    return-void
.end method

.method private postResult(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;->mMainThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/searchcommon/util/ExecutorAsyncTask$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/util/ExecutorAsyncTask$2;-><init>(Lcom/google/android/searchcommon/util/ExecutorAsyncTask;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected varargs abstract doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method public varargs execute([Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/searchcommon/util/ExecutorAsyncTask$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/util/ExecutorAsyncTask$1;-><init>(Lcom/google/android/searchcommon/util/ExecutorAsyncTask;[Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method
