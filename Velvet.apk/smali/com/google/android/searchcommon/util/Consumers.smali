.class public Lcom/google/android/searchcommon/util/Consumers;
.super Ljava/lang/Object;
.source "Consumers.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static consumeAsync(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TA;>;TA;)V"
        }
    .end annotation

    if-nez p0, :cond_0

    invoke-interface {p1, p2}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/searchcommon/util/Consumers$1;

    invoke-direct {v0, p1, p2}, Lcom/google/android/searchcommon/util/Consumers$1;-><init>(Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static consumePartialAsync(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/ConsumerWithProgress;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<TA;>;TA;)V"
        }
    .end annotation

    if-nez p0, :cond_0

    invoke-interface {p1, p2}, Lcom/google/android/searchcommon/util/ConsumerWithProgress;->consumePartial(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/searchcommon/util/Consumers$2;

    invoke-direct {v0, p1, p2}, Lcom/google/android/searchcommon/util/Consumers$2;-><init>(Lcom/google/android/searchcommon/util/ConsumerWithProgress;Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static createAsyncConsumer(Landroid/os/Handler;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;
    .locals 1
    .param p0    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Handler;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TA;>;)",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TA;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/searchcommon/util/Util;->asExecutor(Landroid/os/Handler;)Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/searchcommon/util/Consumers;->createAsyncConsumer(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v0

    return-object v0
.end method

.method public static createAsyncConsumer(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;
    .locals 2
    .param p0    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TA;>;)",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TA;>;"
        }
    .end annotation

    instance-of v1, p1, Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    new-instance v1, Lcom/google/android/searchcommon/util/Consumers$4;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/util/Consumers$4;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/ConsumerWithProgress;)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/searchcommon/util/Consumers$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/util/Consumers$5;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_0
.end method

.method public static progressChangedAsync(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/ConsumerWithProgress;I)V
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<TA;>;I)V"
        }
    .end annotation

    if-nez p0, :cond_0

    invoke-interface {p1, p2}, Lcom/google/android/searchcommon/util/ConsumerWithProgress;->progressChanged(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/searchcommon/util/Consumers$3;

    invoke-direct {v0, p1, p2}, Lcom/google/android/searchcommon/util/Consumers$3;-><init>(Lcom/google/android/searchcommon/util/ConsumerWithProgress;I)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
