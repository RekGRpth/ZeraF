.class public Lcom/google/android/searchcommon/util/PriorityThreadFactory;
.super Ljava/lang/Object;
.source "PriorityThreadFactory.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private final mPriority:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/searchcommon/util/PriorityThreadFactory;->mPriority:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/util/PriorityThreadFactory;)I
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/util/PriorityThreadFactory;

    iget v0, p0, Lcom/google/android/searchcommon/util/PriorityThreadFactory;->mPriority:I

    return v0
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/util/PriorityThreadFactory$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/PriorityThreadFactory$1;-><init>(Lcom/google/android/searchcommon/util/PriorityThreadFactory;Ljava/lang/Runnable;)V

    return-object v0
.end method
