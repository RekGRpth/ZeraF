.class public Lcom/google/android/searchcommon/util/LatencyTracker$EventList;
.super Ljava/lang/Object;
.source "LatencyTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/LatencyTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventList"
.end annotation


# instance fields
.field public final baseTimestamp:J

.field public final events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/util/LatencyTracker$Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(JLjava/util/List;)V
    .locals 0
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/util/LatencyTracker$Event;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;->baseTimestamp:J

    iput-object p3, p0, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;->events:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(JLjava/util/List;Lcom/google/android/searchcommon/util/LatencyTracker$1;)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/util/List;
    .param p4    # Lcom/google/android/searchcommon/util/LatencyTracker$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;-><init>(JLjava/util/List;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "EventList "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;->events:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
