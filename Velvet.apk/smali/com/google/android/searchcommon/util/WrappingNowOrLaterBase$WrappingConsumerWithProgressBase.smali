.class public abstract Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;
.super Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;
.source "WrappingNowOrLaterBase.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/ConsumerWithProgress;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "WrappingConsumerWithProgressBase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase",
        "<TA;TB;>.WrappingConsumerBase;",
        "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
        "<TA;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;Lcom/google/android/searchcommon/util/ConsumerWithProgress;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->this$0:Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;-><init>(Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method


# virtual methods
.method public consumePartial(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->consumer()Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->doConsumePartial(Lcom/google/android/searchcommon/util/ConsumerWithProgress;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method protected bridge synthetic consumer()Lcom/google/android/searchcommon/util/Consumer;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->consumer()Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    move-result-object v0

    return-object v0
.end method

.method protected consumer()Lcom/google/android/searchcommon/util/ConsumerWithProgress;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;"
        }
    .end annotation

    invoke-super {p0}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->consumer()Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    return-object v0
.end method

.method protected abstract doConsumePartial(Lcom/google/android/searchcommon/util/ConsumerWithProgress;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;TA;)Z"
        }
    .end annotation
.end method

.method protected abstract doProgressChanged(Lcom/google/android/searchcommon/util/ConsumerWithProgress;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;I)V"
        }
    .end annotation
.end method

.method public progressChanged(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->consumer()Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->doProgressChanged(Lcom/google/android/searchcommon/util/ConsumerWithProgress;I)V

    goto :goto_0
.end method
