.class public interface abstract Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;
.super Ljava/lang/Object;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HttpResponse"
.end annotation


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract getContentTypeIfAvailable()[Ljava/lang/String;
.end method

.method public abstract getHeaders()Lcom/google/android/searchcommon/util/NowOrLater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/NowOrLater",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end method

.method public abstract getInputStream()Ljava/io/InputStream;
.end method

.method public abstract isComplete()Z
.end method

.method public abstract isFailed()Z
.end method

.method public abstract reset()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation
.end method
