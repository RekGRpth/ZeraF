.class public abstract Lcom/google/android/searchcommon/util/NonCancellableNamedTask;
.super Ljava/lang/Object;
.source "NonCancellableNamedTask.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/NamedTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelExecution()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "NonCancellableNamedTask can\'t be canceled."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
