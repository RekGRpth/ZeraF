.class Lcom/google/android/searchcommon/util/InputStreamChunkProducer$2;
.super Ljava/lang/Object;
.source "InputStreamChunkProducer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->close()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

.field final synthetic val$source:Ljava/io/InputStream;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/InputStreamChunkProducer;Ljava/io/InputStream;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$2;->this$0:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$2;->val$source:Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$2;->val$source:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Exception when trying to close source"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
