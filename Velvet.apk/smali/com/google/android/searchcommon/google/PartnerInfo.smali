.class public Lcom/google/android/searchcommon/google/PartnerInfo;
.super Ljava/lang/Object;
.source "PartnerInfo.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/PartnerInfo;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getClientId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PartnerInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "client_id"

    invoke-static {v0, v1}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchClientId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PartnerInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "search_client_id"

    invoke-static {v0, v1}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
