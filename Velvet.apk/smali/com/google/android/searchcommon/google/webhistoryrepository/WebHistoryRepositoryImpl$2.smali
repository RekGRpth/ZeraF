.class Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;
.super Lcom/google/android/searchcommon/util/SQLiteTransaction;
.source "WebHistoryRepositoryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->insertLocalHistory(Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

.field final synthetic val$insertTime:J

.field final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;->val$query:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;->val$insertTime:J

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/SQLiteTransaction;-><init>()V

    return-void
.end method


# virtual methods
.method protected performTransaction(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 4
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;->val$query:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;->val$insertTime:J

    # invokes: Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->insertQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z
    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->access$200(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    # getter for: Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDataSetObservable:Landroid/database/DataSetObservable;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->access$300(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)Landroid/database/DataSetObservable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
