.class final Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;
.super Lcom/google/android/searchcommon/util/SQLiteTransaction;
.source "WebHistoryRepositoryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DeleteTransaction"
.end annotation


# instance fields
.field private final mLowerCaseQuery:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/SQLiteTransaction;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;->mLowerCaseQuery:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected performTransaction(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 8
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "history"

    # getter for: Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->DELETE_WHERE_CLAUSE:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->access$600()Ljava/lang/String;

    move-result-object v5

    new-array v6, v2, [Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;->mLowerCaseQuery:Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-virtual {p1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    int-to-long v0, v4

    const-wide/16 v4, 0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    # getter for: Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDataSetObservable:Landroid/database/DataSetObservable;
    invoke-static {v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->access$300(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)Landroid/database/DataSetObservable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/DataSetObservable;->notifyChanged()V

    :goto_0
    return v2

    :cond_0
    const-string v2, "Search.HistoryRepositoryImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DB Transaction (delete) unsuccessful, Return value :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto :goto_0
.end method
