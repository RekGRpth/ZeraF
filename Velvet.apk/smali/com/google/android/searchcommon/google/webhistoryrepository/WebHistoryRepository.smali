.class public interface abstract Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;
.super Ljava/lang/Object;
.source "WebHistoryRepository.java"


# virtual methods
.method public abstract deleteAllLocalHistory()V
.end method

.method public abstract deleteLocalHistoryItem(Ljava/lang/String;Z)V
.end method

.method public abstract getLocalHistory(Lcom/google/android/velvet/Query;ILcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "I",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract hasLocalHistory(Lcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract insertLocalHistory(Ljava/lang/String;J)V
.end method

.method public abstract registerDataSetObserver(Landroid/database/DataSetObserver;)V
.end method

.method public abstract unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
.end method
