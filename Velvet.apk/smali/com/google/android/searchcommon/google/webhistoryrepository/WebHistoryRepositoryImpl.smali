.class public Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;
.super Ljava/lang/Object;
.source "WebHistoryRepositoryImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;,
        Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;,
        Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$LocalHistoryTask;,
        Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final DB_NAME:Ljava/lang/String; = "qsb-history.db"

.field private static final DB_VERSION:I = 0x3

.field private static final DELETE_WHERE_CLAUSE:Ljava/lang/String;

.field public static final LOCAL_HISTORY_SOURCE_NAME:Ljava/lang/String; = "local_history_source"

.field private static final TAG:Ljava/lang/String; = "Search.HistoryRepositoryImpl"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mDataSetObservable:Landroid/database/DataSetObservable;

.field private final mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

.field private final mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private final mHasHistoryQueryString:Ljava/lang/String;

.field private final mHistoryQueryString:Ljava/lang/String;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mZeroQueryHistoryQueryString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->QUERY_LOWER:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->DELETE_WHERE_CLAUSE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/NamedTaskExecutor;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/SearchSettings;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;

    const-string v5, "qsb-history.db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/NamedTaskExecutor;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Ljava/lang/String;Lcom/google/android/searchcommon/util/Clock;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/NamedTaskExecutor;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Ljava/lang/String;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/SearchSettings;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDataSetObservable:Landroid/database/DataSetObservable;

    new-instance v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    const/4 v1, 0x3

    invoke-direct {v0, p1, p5, v1, p3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/searchcommon/SearchConfig;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->buildHistoryQueryString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mHistoryQueryString:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->buildZeroQueryHistoryQueryString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mZeroQueryHistoryQueryString:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->buildHasHistoryQueryString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mHasHistoryQueryString:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    iput-object p4, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {p4}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->deleteAllLocalHistory()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mHistoryQueryString:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mZeroQueryHistoryQueryString:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mHasHistoryQueryString:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->insertQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)Landroid/database/DataSetObservable;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDataSetObservable:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mHasHistoryQueryString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->DELETE_WHERE_CLAUSE:Ljava/lang/String;

    return-object v0
.end method

.method private buildHasHistoryQueryString()Ljava/lang/String;
    .locals 9

    const/4 v2, 0x0

    const/4 v0, 0x0

    const-string v1, "history"

    const-string v7, "1"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private buildHistoryQueryString()Ljava/lang/String;
    .locals 10

    const/4 v0, 0x0

    const/4 v4, 0x0

    const-string v8, "insert_time DESC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->QUERY_LOWER:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIKE ?1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v1, "history"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v5, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->QUERY:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    const-string v6, "insert_time DESC"

    move-object v5, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT  ?2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildZeroQueryHistoryQueryString()Ljava/lang/String;
    .locals 10

    const/4 v0, 0x0

    const/4 v3, 0x0

    const-string v8, "insert_time DESC"

    const-string v1, "history"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v4, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->QUERY:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    const-string v6, "insert_time DESC"

    move-object v4, v3

    move-object v5, v3

    move-object v7, v3

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT  ?1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHistoryQueryParams(Ljava/lang/String;I)[Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method private insertQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z
    .locals 6
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sget-object v3, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->QUERY_LOWER:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->QUERY:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->INSERT_TIME:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$HistoryTable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "history"

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {p1, v3, v4, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const-string v3, "Search.HistoryRepositoryImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DB Transaction (insert) unsuccessful, Return value :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_0
.end method

.method private runTransactionOnExecutor(Lcom/google/android/searchcommon/util/SQLiteTransaction;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/SQLiteTransaction;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    new-instance v1, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$5;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Lcom/google/android/searchcommon/util/SQLiteTransaction;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    return-void
.end method

.method private suggestionFromCurrentCursor(Landroid/database/Cursor;)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v2

    sget-object v3, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_DEVICE_HISTORY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->logType(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v2

    const-string v3, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentAction(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;->close()V

    return-void
.end method

.method public deleteAllLocalHistory()V
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$3;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$3;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)V

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->runTransactionOnExecutor(Lcom/google/android/searchcommon/util/SQLiteTransaction;)V

    return-void
.end method

.method deleteDatabase()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;->deleteDatabase()V

    return-void
.end method

.method public deleteLocalHistoryItem(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;->run(Landroid/database/sqlite/SQLiteDatabase;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DeleteTransaction;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->runTransactionOnExecutor(Lcom/google/android/searchcommon/util/SQLiteTransaction;)V

    goto :goto_0
.end method

.method public getLocalHistory(Lcom/google/android/velvet/Query;ILcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "I",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    new-instance v1, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Lcom/google/android/velvet/Query;ILcom/google/android/searchcommon/util/Consumer;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    return-void
.end method

.method getLocalHistoryForQuery(Lcom/google/android/velvet/Query;I)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 8
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # I

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isEmptySuggestQuery()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mZeroQueryHistoryQueryString:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_0
    new-instance v0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;

    const-string v3, "local_history_source"

    invoke-direct {v0, v3, p1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDbOpenHelper:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$DbOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mHistoryQueryString:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, p2}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->getHistoryQueryParams(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->suggestionFromCurrentCursor(Landroid/database/Cursor;)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v2}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v3

    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v3
.end method

.method public hasLocalHistory(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    new-instance v1, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$4;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Lcom/google/android/searchcommon/util/Consumer;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    return-void
.end method

.method public insertLocalHistory(Ljava/lang/String;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    new-instance v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$2;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Ljava/lang/String;J)V

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->runTransactionOnExecutor(Lcom/google/android/searchcommon/util/SQLiteTransaction;)V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method
