.class final Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;
.super Ljava/lang/Object;
.source "WebSuggestSourceWithLocalHistory.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CombinedConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">;"
    }
.end annotation


# instance fields
.field mHistorySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private mRemainingLists:I

.field private final mWebSourceName:Ljava/lang/String;

.field mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mRemainingLists:I

    iput-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mHistorySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mWebSourceName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public declared-synchronized await()Z
    .locals 2

    monitor-enter p0

    :goto_0
    :try_start_0
    iget v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mRemainingLists:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :goto_1
    monitor-exit p0

    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    monitor-enter p0

    if-eqz p1, :cond_3

    :try_start_0
    const-string v0, "local_history_source"

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mHistorySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received history suggestions twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mHistorySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mRemainingLists:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mRemainingLists:I

    iget v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mRemainingLists:I

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :cond_3
    if-eqz p1, :cond_1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mWebSourceName:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received web suggestions twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iput-object p1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v0

    return v0
.end method
