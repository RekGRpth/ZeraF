.class public Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;
.super Ljava/lang/Object;
.source "CompleteServerConstants.java"


# static fields
.field public static final LOG_TYPE_DEVICE_HISTORY:Ljava/lang/String;

.field public static final LOG_TYPE_NAV:Ljava/lang/String;

.field public static final LOG_TYPE_QUERY:Ljava/lang/String;

.field public static final LOG_TYPE_SEARCH_HISTORY:Ljava/lang/String;

.field public static final SPELLING_CORRECTION_TAG_PATTERN:Ljava/util/regex/Pattern;

.field public static final SPELLING_ERROR_TAG_PATTERN:Ljava/util/regex/Pattern;

.field public static final SPELLING_ESCAPE_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_QUERY:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_NAV:Ljava/lang/String;

    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_SEARCH_HISTORY:Ljava/lang/String;

    const/16 v0, 0x29

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_DEVICE_HISTORY:Ljava/lang/String;

    const-string v0, "<sc>(.*?)</sc>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->SPELLING_CORRECTION_TAG_PATTERN:Ljava/util/regex/Pattern;

    const-string v0, "<se>(.*?)</se>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->SPELLING_ERROR_TAG_PATTERN:Ljava/util/regex/Pattern;

    const-string v0, "<(/?((se)|(sc)))>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->SPELLING_ESCAPE_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
