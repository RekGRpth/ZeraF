.class public Lcom/google/android/searchcommon/google/complete/SuggestionCache;
.super Landroid/util/LruCache;
.source "SuggestionCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">;"
    }
.end annotation


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mTimeout:I


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/CoreSearchServices;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {p1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getSuggestionCacheMaxValues()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/util/LruCache;-><init>(I)V

    invoke-interface {p1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getSuggestionCacheTimeout()I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->mTimeout:I

    invoke-interface {p1}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {p1}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/google/complete/SuggestionCache$1;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/google/complete/SuggestionCache$1;-><init>(Lcom/google/android/searchcommon/google/complete/SuggestionCache;)V

    invoke-virtual {v0, v1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public purgeOldData()V
    .locals 8

    iget-object v4, p0, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v4

    iget v6, p0, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->mTimeout:I

    int-to-long v6, v6

    sub-long v2, v4, v6

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->snapshot()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v4}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCreationTime()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-gez v4, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method
