.class public Lcom/google/android/searchcommon/google/complete/CompleteServerClient;
.super Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;
.source "CompleteServerClient.java"


# instance fields
.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mSuggestionCache:Lcom/google/android/searchcommon/google/complete/SuggestionCache;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/apps/sidekick/inject/LocationOracle;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/CoreSearchServices;
    .param p3    # Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;)V

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v1, "mobilepersonalfeeds"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->requireAuthTokenType(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-direct {v0, p2}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;-><init>(Lcom/google/android/searchcommon/CoreSearchServices;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSuggestionCache:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    return-void
.end method

.method private addAuthHeader(Ljava/util/Map;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getAuthToken()Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GoogleLogin auth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addSuggestionFromJson(Lorg/json/JSONArray;ZLjava/util/List;)V
    .locals 11
    .param p1    # Lorg/json/JSONArray;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v8, 0x4

    const/4 v10, 0x0

    const/4 v7, 0x2

    invoke-virtual {p1, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x3

    invoke-virtual {p1, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v7, "p"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v3}, Lcom/google/android/common/base/StringUtil;->unescapeHTML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v1, p2}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->createWebSuggestion(Ljava/lang/CharSequence;ZZ)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v7

    invoke-interface {p3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    const/4 v7, 0x5

    if-ne v5, v7, :cond_3

    invoke-virtual {p1, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-le v7, v8, :cond_1

    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getNavSuggestUrl(Lorg/json/JSONArray;)Landroid/net/Uri;

    move-result-object v6

    :cond_1
    if-nez v6, :cond_2

    invoke-static {v0}, Lcom/google/android/common/base/StringUtil;->unescapeHTML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/webkit/URLUtil;->guessUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    :cond_2
    invoke-static {v0}, Lcom/google/android/common/base/StringUtil;->unescapeHTML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4}, Lcom/google/android/common/base/StringUtil;->unescapeHTML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v7, v8, v6, v10, v9}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->createNavSuggestion(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/net/Uri;ZLandroid/content/Context;)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v7

    invoke-interface {p3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const/16 v7, 0x32

    if-ne v5, v7, :cond_4

    invoke-direct {p0, p1, p3}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->addWordByWordSuggestionsFromJson(Lorg/json/JSONArray;Ljava/util/List;)V

    goto :goto_0

    :cond_4
    const-string v7, "Search.CompleteServerClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown suggestion type "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addWordByWordSuggestionsFromJson(Lorg/json/JSONArray;Ljava/util/List;)V
    .locals 10
    .param p1    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x5

    :try_start_0
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v8, "e"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v3, v8, :cond_0

    invoke-virtual {v7, v3}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v6

    const-string v8, "a"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "c"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v8, 0x0

    invoke-virtual {v1, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/common/base/StringUtil;->unescapeHTML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/searchcommon/util/Util;->invertBold(Landroid/text/Spanned;)V

    const/4 v8, 0x0

    invoke-static {v4, v1, v8}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->createWordByWordSuggestion(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v8

    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v8, "Search.CompleteServerClient"

    const-string v9, "Failed to parse JSON word by word suggestions"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private buildBaseUri(Ljava/lang/String;Z)Landroid/net/Uri$Builder;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    if-eqz p2, :cond_0

    const-string v0, "https"

    :goto_0
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getCompleteServerDomainName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "client"

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getCompleteServerClientId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "http"

    goto :goto_0
.end method

.method private extractCorrections(Lorg/json/JSONArray;Ljava/util/List;Ljava/lang/String;)V
    .locals 15
    .param p1    # Lorg/json/JSONArray;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v13, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    new-instance v10, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p3

    invoke-direct {v10, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v13, "o"

    invoke-virtual {v1, v13}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string v13, "p"

    invoke-virtual {v1, v13}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string v13, "o"

    invoke-virtual {v1, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->unescapeForSpelling(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v13, "p"

    invoke-virtual {v1, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->unescapeForSpelling(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    sget-object v13, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->SPELLING_ERROR_TAG_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v13, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    sget-object v13, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->SPELLING_CORRECTION_TAG_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v13, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    :goto_0
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v13

    if-eqz v13, :cond_0

    const/4 v13, 0x1

    invoke-virtual {v6, v13}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->start()I

    move-result v13

    sub-int v12, v13, v9

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    add-int v5, v12, v13

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->end()I

    move-result v13

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->start()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v14

    sub-int/2addr v13, v14

    add-int/2addr v9, v13

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v13

    invoke-static {v13}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    new-instance v11, Lcom/google/android/searchcommon/suggest/CorrectionSpan;

    invoke-direct {v11, v3}, Lcom/google/android/searchcommon/suggest/CorrectionSpan;-><init>(Ljava/lang/String;)V

    const/16 v13, 0x21

    invoke-virtual {v10, v11, v12, v5, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_0
    invoke-static {v10}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->createCorrectionSuggestion(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private extractSuggestions(Lcom/google/android/velvet/Query;Lorg/json/JSONArray;Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 12
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lorg/json/JSONArray;
    .param p3    # Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSuggestionCache:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-virtual {v0, v10}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v11

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getPrefetchIndex(Lorg/json/JSONArray;)I

    move-result v9

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v7, v0, :cond_1

    :try_start_0
    invoke-virtual {v11, v7}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v1

    if-ne v7, v9, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v1, v0, v3}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->addSuggestionFromJson(Lorg/json/JSONArray;ZLjava/util/List;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v6

    const-string v0, "Search.CompleteServerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not parse suggestion at position "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isCorrectionsEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_1
    invoke-direct {p0, p2, v3, v10}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->extractCorrections(Lorg/json/JSONArray;Ljava/util/List;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_3
    invoke-virtual {p1, v10}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v2

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;

    const-string v1, "complete-server"

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;J)V

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->getMutableCopy()Lcom/google/android/searchcommon/suggest/MutableSuggestionList;

    move-result-object v8

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setAccount(Ljava/lang/String;)V

    :cond_3
    return-object v8

    :catch_1
    move-exception v6

    const-string v0, "Search.CompleteServerClient"

    const-string v1, "Error parsing JSON correction span data"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private getAuthToken()Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v1, "mobilepersonalfeeds"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAuthToken(Ljava/lang/String;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    move-result-object v0

    return-object v0
.end method

.method private getNavSuggestUrl(Lorg/json/JSONArray;)Landroid/net/Uri;
    .locals 5
    .param p1    # Lorg/json/JSONArray;

    if-eqz p1, :cond_1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/url"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Search.CompleteServerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t parse nav suggest destination: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getPrefetchIndex(Lorg/json/JSONArray;)I
    .locals 6
    .param p1    # Lorg/json/JSONArray;

    const/4 v2, -0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->isNativeIgEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x2

    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "n"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "n"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v1

    const-string v3, "Search.CompleteServerClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception when looking for prefetch index: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private parseJson(Lcom/google/android/velvet/Query;Ljava/lang/String;Lcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;)V
    .locals 9
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .param p4    # Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v1, p4}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->extractSuggestions(Lcom/google/android/velvet/Query;Lorg/json/JSONArray;Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v6

    invoke-interface {p3, v6}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->addAll(Ljava/lang/Iterable;)I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/searchcommon/SearchConfig;->isSuggestLookAheadEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    :try_start_0
    iget-object v7, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSuggestionCache:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-interface {v6}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v7, 0x2

    invoke-virtual {v1, v7}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_1

    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v5

    invoke-direct {p0, p1, v5, p4}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->extractSuggestions(Lcom/google/android/velvet/Query;Lorg/json/JSONArray;Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSuggestionCache:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-interface {v6}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    const-string v7, "m"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v7, "Search.CompleteServerClient"

    const-string v8, "Error parsing JSON look ahead suggestion data"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private unescapeForSpelling(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->SPELLING_ESCAPE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "&lt;$1&gt;"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->isSuggestLookAheadEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isEmptySuggestQuery()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSuggestionCache:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->purgeOldData()V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSuggestionCache:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->setFromCache(Z)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getClientNameForLogging()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getCompleteServerClientId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSourceName()Ljava/lang/String;
    .locals 1

    const-string v0, "complete-server"

    return-object v0
.end method

.method public isLikelyToReturnZeroQueryResults()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getAuthToken()Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public logClick(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    return-void
.end method

.method protected query(Lcom/google/android/velvet/Query;ZLcom/google/android/searchcommon/suggest/MutableSuggestionList;)V
    .locals 21
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Z
    .param p3    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v11

    move/from16 v10, p2

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v7

    const/4 v14, 0x0

    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->addAuthHeader(Ljava/util/Map;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    move-result-object v14

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/searchcommon/SearchConfig;->getCompleteServerPath()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v10}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->buildBaseUri(Ljava/lang/String;Z)Landroid/net/Uri$Builder;

    move-result-object v15

    const-string v16, "hl"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getHlParameter()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getGeoLocation()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    const-string v16, "gl"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    if-eqz p2, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForSearch()Z

    move-result v16

    if-eqz v16, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/searchcommon/SearchConfig;->getLocationExpiryTimeSeconds()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x3e8

    mul-long v17, v17, v19

    invoke-interface/range {v16 .. v18}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->requestRecentLocation(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v9

    if-eqz v9, :cond_2

    const-string v16, "X-Geo"

    invoke-static {v9}, Lcom/google/android/searchcommon/google/XGeoEncoder;->encodeLocation(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_0
    const-string v16, "oe"

    const-string v17, "UTF-8"

    invoke-virtual/range {v15 .. v17}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/searchcommon/SearchConfig;->isSuggestLookAheadEnabled()Z

    move-result v16

    if-eqz v16, :cond_3

    const-string v16, "sla"

    const-string v17, "1"

    invoke-virtual/range {v15 .. v17}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/searchcommon/SearchConfig;->isWordByWordEnabled()Z

    move-result v16

    if-eqz v16, :cond_4

    const-string v16, "sugexp"

    const-string v17, "wbw"

    invoke-virtual/range {v15 .. v17}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v16, "cp"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/searchcommon/SearchConfig;->getCompleteServerExtraParams()[Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    :goto_1
    array-length v0, v4

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-ge v8, v0, :cond_6

    aget-object v16, v4, v8

    add-int/lit8 v17, v8, 0x1

    aget-object v17, v4, v17

    invoke-virtual/range {v15 .. v17}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    add-int/lit8 v8, v8, 0x2

    goto :goto_1

    :cond_5
    const-string v16, "devloc"

    const-string v17, "0"

    invoke-virtual/range {v15 .. v17}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_6
    const/4 v12, 0x0

    :try_start_0
    const-string v16, "q"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const/4 v13, 0x1

    if-nez p2, :cond_7

    const/high16 v16, 0x10000000

    or-int v13, v13, v16

    :cond_7
    new-instance v5, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {v15}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v5, v0, v7}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->setUseCaches(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v5, v13}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v12, v2, v14}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->parseJson(Lcom/google/android/velvet/Query;Ljava/lang/String;Lcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;)V

    if-eqz v14, :cond_8

    invoke-virtual {v14}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->getAccount()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setAccount(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_2
    return-void

    :catch_0
    move-exception v3

    const-string v16, "Search.CompleteServerClient"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Error parsing suggestions \'"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\'"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    const/16 v16, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setRequestFailed(Z)V

    goto :goto_2

    :catch_1
    move-exception v16

    goto :goto_3

    :catch_2
    move-exception v16

    goto :goto_3
.end method

.method public removeFromHistory(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/searchcommon/SearchConfig;->getRemoveHistoryPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, v5}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->buildBaseUri(Ljava/lang/String;Z)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v6, "delq"

    invoke-virtual {v3, v6, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v6, "callback"

    const-string v7, "google.sbox.d0"

    invoke-virtual {v3, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->addAuthHeader(Ljava/util/Map;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return v4

    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6, v1}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->setUseCaches(Z)V

    iget-object v6, p0, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    const/4 v7, 0x2

    invoke-interface {v6, v0, v7}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move v4, v5

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_0

    :catch_1
    move-exception v5

    goto :goto_0
.end method
