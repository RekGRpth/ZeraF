.class Lcom/google/android/searchcommon/google/LocationOptIn$2;
.super Landroid/database/ContentObserver;
.source "LocationOptIn.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/LocationOptIn;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/LocationOptIn;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/LocationOptIn;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$2;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn$2;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mBackgroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$300(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$2;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mGetGoogleLocationSetting:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$400(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
