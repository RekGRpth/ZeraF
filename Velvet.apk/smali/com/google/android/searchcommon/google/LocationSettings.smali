.class public interface abstract Lcom/google/android/searchcommon/google/LocationSettings;
.super Ljava/lang/Object;
.source "LocationSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/LocationSettings$Observer;
    }
.end annotation


# virtual methods
.method public abstract addUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V
.end method

.method public abstract canUseLocationForGoogleApps()Z
.end method

.method public abstract canUseLocationForSearch()Z
.end method

.method public abstract dispose()V
.end method

.method public abstract getGoogleSettingIntent(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract isGoogleSettingForAllApps()Z
.end method

.method public abstract maybeShowLegacyOptIn()V
.end method

.method public abstract removeUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V
.end method
