.class Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;
.super Ljava/lang/Object;
.source "RefreshSearchDomainAndCookiesTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->maybeDestroyWebview()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    # getter for: Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->access$200(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    # getter for: Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->access$300(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewDestroyDone:Z
    invoke-static {v0, v2}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->access$402(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;Z)Z

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    # getter for: Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->access$200(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
