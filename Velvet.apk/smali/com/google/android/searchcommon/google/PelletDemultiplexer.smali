.class public Lcom/google/android/searchcommon/google/PelletDemultiplexer;
.super Ljava/lang/Object;
.source "PelletDemultiplexer.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;,
        Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/searchcommon/google/PelletParser$Pellet;",
        ">;"
    }
.end annotation


# instance fields
.field private final mBaseUri:Ljava/lang/String;

.field private final mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

.field private final mCards:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;",
            ">;"
        }
    .end annotation
.end field

.field private final mSrp:Ljava/lang/StringBuilder;

.field private final mSrpConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;"
        }
    .end annotation
.end field

.field private final mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private noMoreCards:Z


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/Consumer;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mCards:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Consumer;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mSrpConsumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Consumer;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mBaseUri:Ljava/lang/String;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/StringBuilder;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mSrp:Ljava/lang/StringBuilder;

    return-void
.end method

.method static makeAction(Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;)Lcom/google/android/velvet/ActionServerResult;
    .locals 6
    .param p0    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;->getData()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x8

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    new-instance v3, Lcom/google/android/velvet/ActionServerResult;

    invoke-static {v2}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->stripWebSearchType(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;->getCardLoggingUrlProto()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->makeCardMetadata(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/velvet/ActionServerResult;-><init>(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;)V

    return-object v3
.end method

.method private static makeAnswer(Ljava/lang/String;Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;)Lcom/google/android/velvet/ActionServerResult;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;->getData()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-direct {v2}, Lcom/google/majel/proto/PeanutProtos$Url;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/majel/proto/PeanutProtos$Url;->setHtml(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {v2, p0}, Lcom/google/majel/proto/PeanutProtos$Url;->setRenderedLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setPrimaryType(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addUrlResponse(Lcom/google/majel/proto/PeanutProtos$Url;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    new-instance v3, Lcom/google/android/velvet/ActionServerResult;

    invoke-static {v1}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->stripWebSearchType(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;->getCardLoggingUrlProto()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->makeCardMetadata(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/velvet/ActionServerResult;-><init>(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;)V

    return-object v3
.end method

.method static makeCardMetadata(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;
    .locals 4
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v1, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;-><init>()V

    if-eqz p0, :cond_0

    const/16 v2, 0x8

    invoke-static {p0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    :goto_0
    return-object v1

    :cond_0
    const-string v2, "Search.PelletDemultiplexer"

    const-string v3, "No card metadata received"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private noMoreCards()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->noMoreCards:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->noMoreCards:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;->noMoreCards()V

    :cond_0
    return-void
.end method

.method private produceCard(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;

    const-string v4, "ans"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    const-string v4, "act"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v2, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    if-eqz v2, :cond_2

    :try_start_0
    invoke-static {p1, p3}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->makeAnswer(Ljava/lang/String;Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;)Lcom/google/android/velvet/ActionServerResult;

    move-result-object v3

    :goto_0
    iget-object v4, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    invoke-interface {v4, v3}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;->consume(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-static {p3}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->makeAction(Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;)Lcom/google/android/velvet/ActionServerResult;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Search.PelletDemultiplexer"

    const-string v5, "Could not make card"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v4, "Search.PelletDemultiplexer"

    const-string v5, "Could not make card"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static stripWebSearchType(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 0
    .param p0    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->clearWebSearchType()Lcom/google/majel/proto/PeanutProtos$Peanut;

    return-object p0
.end method


# virtual methods
.method public consume(Lcom/google/android/searchcommon/google/PelletParser$Pellet;)Z
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/google/PelletParser$Pellet;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "ect"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;

    new-instance v4, Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;

    iget-object v5, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mId:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mSrpConsumer:Lcom/google/android/searchcommon/util/Consumer;

    new-instance v4, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;

    iget-object v5, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mData:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;-><init>([B)V

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    :cond_1
    const-string v3, "eoc"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->noMoreCards()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mCards:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;

    iget-object v3, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mData:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mMetadata:Ljava/util/Map;

    invoke-direct {v0, v3, v4}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mCards:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-boolean v3, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mMorePelletsToFollow:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mBaseUri:Ljava/lang/String;

    invoke-direct {p0, v3, v1, v0}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->produceCard(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->mCards:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iget-object v3, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mData:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;->mMetadata:Ljava/util/Map;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/searchcommon/google/PelletDemultiplexer$ChunkedCard;->append(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/google/PelletParser$Pellet;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->consume(Lcom/google/android/searchcommon/google/PelletParser$Pellet;)Z

    move-result v0

    return v0
.end method

.method onEndOfResponse()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->noMoreCards()V

    return-void
.end method
