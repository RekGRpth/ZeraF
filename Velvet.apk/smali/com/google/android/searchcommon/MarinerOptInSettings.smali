.class public interface abstract Lcom/google/android/searchcommon/MarinerOptInSettings;
.super Ljava/lang/Object;
.source "MarinerOptInSettings.java"


# static fields
.field public static final CAN_USE_TG_NO:I = 0x2

.field public static final CAN_USE_TG_NO_ASSUMED:I = 0x3

.field public static final CAN_USE_TG_NO_DASHER_BLOCKED:I = 0x4

.field public static final CAN_USE_TG_PENDING:I = 0x0

.field public static final CAN_USE_TG_YES:I = 0x1

.field public static final SUCCESS:I = 0x0

.field public static final TIMEOUT:I = -0x1


# virtual methods
.method public abstract canAccountRunTheGoogle(Landroid/accounts/Account;)I
.end method

.method public abstract disableForAccount(Landroid/accounts/Account;)V
.end method

.method public abstract domainIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)Z
.end method

.method public abstract fetchAccountConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;
.end method

.method public abstract getSavedConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;
.end method

.method public abstract getSavedConfigurationTimestamp(Landroid/accounts/Account;)J
.end method

.method public abstract isAccountOptedIn(Landroid/accounts/Account;)Z
.end method

.method public abstract isUserOptedIn()Z
.end method

.method public abstract localeIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;)Z
.end method

.method public abstract optAccountIn(Landroid/accounts/Account;)V
.end method

.method public abstract optAccountOut(Landroid/accounts/Account;)V
.end method

.method public abstract optIntoLatitude()V
.end method

.method public abstract registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
.end method

.method public abstract saveConfiguration(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)V
.end method

.method public abstract sendLocationReportingOptInIntent()V
.end method

.method public abstract setFirstRunScreensShown()V
.end method

.method public abstract stopServicesIfUserOptedOut()Z
.end method

.method public abstract unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
.end method

.method public abstract updateConfigurationForAccount(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;)Z
.end method

.method public abstract userCanRunTheGoogle(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)I
.end method

.method public abstract userHasSeenFirstRunScreens()Z
.end method
