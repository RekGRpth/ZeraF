.class public interface abstract Lcom/google/android/searchcommon/SearchSettings;
.super Ljava/lang/Object;
.source "SearchSettings.java"


# virtual methods
.method public abstract addMenuItems(Landroid/view/Menu;Z)V
.end method

.method public abstract broadcastSettingsChanged()V
.end method

.method public abstract getBackgroundTaskEarliestNextRun(Ljava/lang/String;)J
.end method

.method public abstract getBackgroundTaskForcedRun(Ljava/lang/String;)J
.end method

.method public abstract getCachedUserAgentBase()Ljava/lang/String;
.end method

.method public abstract getCachedZeroQueryWebResults()Ljava/lang/String;
.end method

.method public abstract getDebugFeaturesLevel()I
.end method

.method public abstract getDebugSearchDomainOverride()Ljava/lang/String;
.end method

.method public abstract getDebugSearchSchemeOverride()Ljava/lang/String;
.end method

.method public abstract getGoogleAccountToUse()Ljava/lang/String;
.end method

.method public abstract getLastApplicationLaunch()J
.end method

.method public abstract getLastRunVersion()I
.end method

.method public abstract getPersonalizedSearchEnabled()Z
.end method

.method public abstract getRefreshWebViewCookiesAt()J
.end method

.method public abstract getSafeSearch()Ljava/lang/String;
.end method

.method public abstract getSearchDomain()Ljava/lang/String;
.end method

.method public abstract getSearchDomainCountryCode()Ljava/lang/String;
.end method

.method public abstract getSearchDomainLanguage()Ljava/lang/String;
.end method

.method public abstract getSearchDomainScheme()Ljava/lang/String;
.end method

.method public abstract getSignedOut()Z
.end method

.method public abstract getSourceClickStats(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getVoiceSearchInstallId()Ljava/lang/String;
.end method

.method public abstract getWebCorpora()[B
.end method

.method public abstract getWebCorporaConfigUrl()Ljava/lang/String;
.end method

.method public abstract getWebViewLoggedInAccount()Ljava/lang/String;
.end method

.method public abstract getWebViewLoggedInDomain()Ljava/lang/String;
.end method

.method public abstract isSingleRequestArchitectureEnabled()Z
.end method

.method public abstract isSourceEnabled(Lcom/google/android/searchcommon/summons/Source;)Z
.end method

.method public abstract registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
.end method

.method public abstract setBackgroundTaskEarliestNextRun(Ljava/lang/String;J)V
.end method

.method public abstract setBackgroundTaskForcedRun(Ljava/lang/String;J)V
.end method

.method public abstract setCachedUserAgentBase(Ljava/lang/String;)V
.end method

.method public abstract setCachedZeroQueryWebResults(Ljava/lang/String;)V
.end method

.method public abstract setDebugFeaturesLevel(I)V
.end method

.method public abstract setGoogleAccountToUse(Ljava/lang/String;)V
.end method

.method public abstract setGservicesOverridesJson(Ljava/lang/String;)V
.end method

.method public abstract setLastApplicationLaunch(J)V
.end method

.method public abstract setLastRunVersion(I)V
.end method

.method public abstract setRefreshWebViewCookiesAt(J)V
.end method

.method public abstract setSearchDomain(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setSignedOut(Z)V
.end method

.method public abstract setSourceClickStats(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setUseGoogleCom(Z)V
.end method

.method public abstract setWebCorpora([B)V
.end method

.method public abstract setWebCorporaConfigUrl(Ljava/lang/String;)V
.end method

.method public abstract setWebViewLoggedInAccount(Ljava/lang/String;)V
.end method

.method public abstract setWebViewLoggedInDomain(Ljava/lang/String;)V
.end method

.method public abstract shouldForceHandsFreeDebugFlag()Z
.end method

.method public abstract shouldUseGoogleCom()Z
.end method

.method public abstract unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
.end method
