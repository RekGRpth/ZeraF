.class public interface abstract Lcom/google/android/searchcommon/clicklog/ClickLog;
.super Ljava/lang/Object;
.source "ClickLog.java"


# virtual methods
.method public abstract clearHistory()V
.end method

.method public abstract getSourceScores(Lcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract hasHistory(Lcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract reportClick(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)V
.end method
