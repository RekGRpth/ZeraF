.class public Lcom/google/android/searchcommon/AsyncServicesImpl;
.super Ljava/lang/Object;
.source "AsyncServicesImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/AsyncServices;


# instance fields
.field private final THREAD_POOL_SIZE:I

.field private final mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mPerNameExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private final mScheduledBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mThreadPool:Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

.field private final mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v4, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->THREAD_POOL_SIZE:I

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;-><init>(Landroid/os/Handler;Landroid/os/MessageQueue;)V

    invoke-static {v2}, Lcom/google/android/velvet/VelvetStrictMode;->maybeTrackUiExecutor(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v0, Lcom/google/android/searchcommon/util/PriorityThreadFactory;

    const/16 v2, 0xa

    invoke-direct {v0, v2}, Lcom/google/android/searchcommon/util/PriorityThreadFactory;-><init>(I)V

    new-instance v2, Lcom/google/android/searchcommon/util/PerNameExecutor;

    invoke-static {v0}, Lcom/google/android/searchcommon/util/SingleThreadNamedTaskExecutor;->factory(Ljava/util/concurrent/ThreadFactory;)Lcom/google/android/searchcommon/util/Factory;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/searchcommon/util/PerNameExecutor;-><init>(Lcom/google/android/searchcommon/util/Factory;)V

    iput-object v2, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mPerNameExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    new-instance v2, Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;

    invoke-direct {v2, v4, v0}, Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;)V

    iput-object v2, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mThreadPool:Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x1

    invoke-static {v2, v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mScheduledBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method


# virtual methods
.method public getExclusiveBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mPerNameExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    return-object v0
.end method

.method public getExclusiveNamedExecutor(Ljava/lang/String;)Ljava/util/concurrent/Executor;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/util/NamingTaskExecutor;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/AsyncServicesImpl;->getExclusiveBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/searchcommon/util/NamingTaskExecutor;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    return-object v0
.end method

.method public getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mThreadPool:Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

    return-object v0
.end method

.method public bridge synthetic getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/AsyncServicesImpl;->getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

    move-result-object v0

    return-object v0
.end method

.method public getPooledBackgroundExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/AsyncServicesImpl;->getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;)V

    return-object v0
.end method

.method public getScheduledBackgroundExecutorService()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mScheduledBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/AsyncServicesImpl;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method
