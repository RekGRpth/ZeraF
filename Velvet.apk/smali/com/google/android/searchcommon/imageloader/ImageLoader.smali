.class public Lcom/google/android/searchcommon/imageloader/ImageLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/UriLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/UriLoader",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDataUriLoader:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mFallbackUriLoader:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mNetworkLoader:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/searchcommon/util/UriLoader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoader;->mNetworkLoader:Lcom/google/android/searchcommon/util/UriLoader;

    iput-object p2, p0, Lcom/google/android/searchcommon/imageloader/ImageLoader;->mDataUriLoader:Lcom/google/android/searchcommon/util/UriLoader;

    iput-object p3, p0, Lcom/google/android/searchcommon/imageloader/ImageLoader;->mFallbackUriLoader:Lcom/google/android/searchcommon/util/UriLoader;

    return-void
.end method


# virtual methods
.method public load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/searchcommon/util/Util;->isEmpty(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/searchcommon/util/Now;->returnNull()Lcom/google/android/searchcommon/util/Now;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "https"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoader;->mNetworkLoader:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v1, "data"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoader;->mDataUriLoader:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoader;->mFallbackUriLoader:Lcom/google/android/searchcommon/util/UriLoader;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoader;->mFallbackUriLoader:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/google/android/searchcommon/util/Now;->returnNull()Lcom/google/android/searchcommon/util/Now;

    move-result-object v1

    goto :goto_0
.end method
