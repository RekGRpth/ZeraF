.class public Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;
.super Lcom/google/android/searchcommon/util/SynchronousLoader;
.source "NetworkImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/SynchronousLoader",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/HttpHelper;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p2    # Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/SynchronousLoader;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;->mResources:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public loadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1    # Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    const-string v7, "http"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    const-string v7, "https"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    const/4 v4, 0x1

    :cond_1
    invoke-static {v4}, Ljunit/framework/Assert;->assertTrue(Z)V

    :try_start_0
    new-instance v3, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;)V

    const v4, 0x15180

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->setMaxStaleSecs(I)V

    iget-object v4, p0, Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    const/4 v6, 0x7

    invoke-interface {v4, v3, v6}, Lcom/google/android/searchcommon/util/HttpHelper;->rawGet(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)[B

    move-result-object v1

    if-nez v1, :cond_2

    move-object v4, v5

    :goto_0
    return-object v4

    :cond_2
    const/4 v4, 0x0

    array-length v6, v1

    invoke-static {v1, v4, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v4, "Search.NetworkImageLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to decode "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    goto :goto_0

    :cond_3
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;->mResources:Landroid/content/res/Resources;

    invoke-direct {v4, v6, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v4, "Search.NetworkImageLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to load "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v4, "Search.NetworkImageLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to load "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    goto :goto_0
.end method

.method public bridge synthetic loadNow(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;->loadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
