.class public Lcom/google/android/searchcommon/summons/SummonsFilter;
.super Ljava/lang/Object;
.source "SummonsFilter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
