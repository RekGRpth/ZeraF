.class public abstract Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;
.super Ljava/lang/Object;
.source "CursorSuggestionBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# instance fields
.field protected final mCursor:Landroid/database/Cursor;

.field private final mFormatCol:I

.field private final mIcon1Col:I

.field private final mIcon2Col:I

.field private final mIconLargeCol:I

.field private final mRefreshSpinnerCol:I

.field private final mText1Col:I

.field private final mText2Col:I

.field private final mText2UrlCol:I

.field private final mUserQuery:Lcom/google/android/velvet/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_text_2_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_icon_2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "suggest_intent_query"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "suggest_format"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "suggest_spinner_while_refreshing"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/velvet/Query;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mUserQuery:Lcom/google/android/velvet/Query;

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    const-string v0, "suggest_format"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mFormatCol:I

    const-string v0, "suggest_text_1"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mText1Col:I

    const-string v0, "suggest_text_2"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mText2Col:I

    const-string v0, "suggest_text_2_url"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mText2UrlCol:I

    const-string v0, "suggest_icon_1"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mIcon1Col:I

    const-string v0, "suggest_icon_2"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mIcon2Col:I

    const-string v0, "suggest_icon_large"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mIconLargeCol:I

    const-string v0, "suggest_spinner_while_refreshing"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mRefreshSpinnerCol:I

    return-void
.end method

.method private formatString(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "html"

    iget v1, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mFormatCol:I

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private getIntentExtraData()Ljava/lang/String;
    .locals 1

    const-string v0, "suggest_intent_extra_data"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getQuery()Ljava/lang/String;
    .locals 1

    const-string v0, "suggest_intent_query"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSuggestionLogType()Ljava/lang/String;
    .locals 1

    const-string v0, "suggest_log_type"

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public build(I)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TS;"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mUserQuery:Lcom/google/android/velvet/Query;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-virtual {p0, v5, v6, v7}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->createSuggestionList(Lcom/google/android/velvet/Query;Ljava/util/List;Z)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v5

    :goto_0
    return-object v5

    :cond_0
    iget-object v5, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v5, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v5, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    const-string v8, "suggest_last_access_hint"

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    const/4 v8, -0x1

    if-eq v5, v8, :cond_3

    move v0, v6

    :goto_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_4

    iget-object v5, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getCurrentSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->shouldKeepSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_2
    move v5, v7

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mUserQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p0, v5, v4, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->createSuggestionList(Lcom/google/android/velvet/Query;Ljava/util/List;Z)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v5

    goto :goto_0
.end method

.method protected abstract createSuggestionList(Lcom/google/android/velvet/Query;Ljava/util/List;Z)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;Z)TS;"
        }
    .end annotation
.end method

.method protected getColumnIndex(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "QSB.CursorSuggestionBuilder"

    const-string v3, "getColumnIndex() failed, "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected getCurrentSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getSuggestionSource()Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->source(Lcom/google/android/searchcommon/summons/Source;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getText1()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getText2()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text2(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getText2Url()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text2Url(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getIcon1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->icon1(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getIcon2()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->icon2(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getIconLarge()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->iconLarge(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getLastAccessTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->lastAccessTime(J)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->isSpinnerWhileRefreshing()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->spinnerWhileRefreshing(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getIntentAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentAction(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getIntentDataString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentData(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getIntentExtraData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentExtraData(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getSuggestionSource()Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getIntentComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentComponent(Landroid/content/ComponentName;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->logType(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->isHistory()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getExtras()Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->extras(Lcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getExtras()Lcom/google/android/searchcommon/suggest/SuggestionExtras;
.end method

.method protected getIcon1()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mIcon1Col:I

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getIcon2()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mIcon2Col:I

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getIconLarge()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mIconLargeCol:I

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getIntentAction()Ljava/lang/String;
    .locals 3

    const-string v2, "suggest_intent_action"

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getSuggestionSource()Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v0, v2

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getDefaultIntentAction()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method protected getIntentDataString()Ljava/lang/String;
    .locals 5

    const-string v3, "suggest_intent_data"

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getSuggestionSource()Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v2

    if-nez v0, :cond_0

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getDefaultIntentData()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    const-string v3, "suggest_intent_data_id"

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method protected getLastAccessTime()J
    .locals 3

    const-string v0, "suggest_last_access_hint"

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getLongOrDefault(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected getLongOrDefault(IJ)J
    .locals 3
    .param p1    # I
    .param p2    # J

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-wide p2

    :cond_1
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "QSB.CursorSuggestionBuilder"

    const-string v2, "getLong() failed, "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected getLongOrDefault(Ljava/lang/String;J)J
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getLongOrDefault(IJ)J

    move-result-wide v1

    return-wide v1
.end method

.method protected getStringOrNull(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "QSB.CursorSuggestionBuilder"

    const-string v3, "getString() failed, "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected getStringOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected abstract getSuggestionSource()Lcom/google/android/searchcommon/summons/ContentProviderSource;
.end method

.method protected getText1()Ljava/lang/CharSequence;
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mText1Col:I

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->formatString(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected getText2()Ljava/lang/CharSequence;
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mText2Col:I

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->formatString(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected getText2Url()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mText2UrlCol:I

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract isHistory()Z
.end method

.method protected isSpinnerWhileRefreshing()Z
    .locals 2

    const-string v0, "true"

    iget v1, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mRefreshSpinnerCol:I

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected shouldKeepSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->mUserQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
