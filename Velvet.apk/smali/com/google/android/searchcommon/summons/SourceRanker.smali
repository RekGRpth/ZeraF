.class public interface abstract Lcom/google/android/searchcommon/summons/SourceRanker;
.super Ljava/lang/Object;
.source "SourceRanker.java"


# virtual methods
.method public abstract getSourcesForQuerying(Lcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract getSourcesForUi(Lcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;>;)V"
        }
    .end annotation
.end method
