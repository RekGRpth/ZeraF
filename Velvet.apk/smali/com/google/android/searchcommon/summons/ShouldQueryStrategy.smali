.class public Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;
.super Ljava/lang/Object;
.source "ShouldQueryStrategy.java"


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mEmptySources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/searchcommon/summons/Source;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

.field private mLastQuery:Ljava/lang/String;

.field private volatile mSummonsQueryStrategy:I


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/GlobalSearchServices;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mLastQuery:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mEmptySources:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mSummonsQueryStrategy:I

    return-void
.end method

.method private updateQuery(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mLastQuery:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mLastQuery:Ljava/lang/String;

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mLastQuery:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mEmptySources:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mEmptySources:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method


# virtual methods
.method public getSummonsQueryStrategy()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mSummonsQueryStrategy:I

    return v0
.end method

.method public isSingleSourceMode()Z
    .locals 2

    iget v0, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mSummonsQueryStrategy:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onZeroResults(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/summons/Sources;->getContentProviderSource(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mLastQuery:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->queryAfterZeroResults()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mEmptySources:Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setSummonsQueryStrategy(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput p1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mSummonsQueryStrategy:I

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldQuerySource(Lcom/google/android/searchcommon/summons/ContentProviderSource;Ljava/lang/String;)Z
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mSummonsQueryStrategy:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/SearchConfig;->isSourceEnabledInSuggestMode(Lcom/google/android/searchcommon/summons/ContentProviderSource;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->updateQuery(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getQueryThreshold()I

    move-result v2

    if-lt v1, v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->queryAfterZeroResults()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mEmptySources:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public shouldQuerySummons(Lcom/google/android/velvet/Query;)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->mSummonsQueryStrategy:I

    if-eq v1, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isEmptySuggestQuery()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
