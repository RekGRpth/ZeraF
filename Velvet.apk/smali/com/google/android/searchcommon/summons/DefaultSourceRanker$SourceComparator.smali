.class Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;
.super Ljava/lang/Object;
.source "DefaultSourceRanker.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/summons/DefaultSourceRanker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SourceComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/searchcommon/summons/Source;",
        ">;"
    }
.end annotation


# instance fields
.field private final mClickScores:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;->mClickScores:Ljava/util/Map;

    return-void
.end method

.method private getClickScore(Lcom/google/android/searchcommon/summons/Source;)I
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;->mClickScores:Ljava/util/Map;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;->mClickScores:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method private getSourceScore(Lcom/google/android/searchcommon/summons/Source;)I
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;->getClickScore(Lcom/google/android/searchcommon/summons/Source;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public compare(Lcom/google/android/searchcommon/summons/Source;Lcom/google/android/searchcommon/summons/Source;)I
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/summons/Source;
    .param p2    # Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->isEnabledByDefault()Z

    move-result v1

    invoke-interface {p2}, Lcom/google/android/searchcommon/summons/Source;->isEnabledByDefault()Z

    move-result v2

    if-eq v1, v2, :cond_1

    if-eqz v1, :cond_0

    const/4 v3, -0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;->getSourceScore(Lcom/google/android/searchcommon/summons/Source;)I

    move-result v3

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;->getSourceScore(Lcom/google/android/searchcommon/summons/Source;)I

    move-result v4

    sub-int v0, v3, v4

    if-eqz v0, :cond_2

    move v3, v0

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/searchcommon/summons/Source;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/summons/Source;

    check-cast p2, Lcom/google/android/searchcommon/summons/Source;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;->compare(Lcom/google/android/searchcommon/summons/Source;Lcom/google/android/searchcommon/summons/Source;)I

    move-result v0

    return v0
.end method
