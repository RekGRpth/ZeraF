.class Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;
.super Ljava/lang/Object;
.source "TopSourceSummonsPromoter.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SourceState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;",
        ">;"
    }
.end annotation


# instance fields
.field final mName:Ljava/lang/String;

.field mResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field mScore:I

.field mState:I

.field final synthetic this$0:Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->this$0:Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;)I
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    iget v0, p1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mScore:I

    iget v1, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mScore:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->compareTo(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;)I

    move-result v0

    return v0
.end method

.method expectResults()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mState:I

    return-void
.end method

.method haveResults()Z
    .locals 2

    iget v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method resetState()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-void
.end method

.method setResults(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mState:I

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method stillWaiting()Z
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
