.class public Lcom/google/android/searchcommon/suggest/SuggestionListFactory;
.super Ljava/lang/Object;
.source "SuggestionListFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;
    }
.end annotation


# static fields
.field private static final LAST_ACCESS_TIME_COMP:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static final NON_LAST_ACCESS_TIME_COMP:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;-><init>(Z)V

    sput-object v0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory;->LAST_ACCESS_TIME_COMP:Ljava/util/Comparator;

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;-><init>(Z)V

    sput-object v0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory;->NON_LAST_ACCESS_TIME_COMP:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createSuggestionList(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;Z)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/velvet/Query;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;Z)",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;"
        }
    .end annotation

    if-eqz p3, :cond_0

    :cond_0
    if-eqz p3, :cond_2

    sget-object v0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory;->LAST_ACCESS_TIME_COMP:Ljava/util/Comparator;

    :goto_0
    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "Search.SuggestionListFactory"

    const-string v1, "null in Suggestion list"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;)V

    return-object v0

    :cond_2
    sget-object v0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory;->NON_LAST_ACCESS_TIME_COMP:Ljava/util/Comparator;

    goto :goto_0
.end method
