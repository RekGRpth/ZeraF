.class public interface abstract Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;
.super Ljava/lang/Object;
.source "SuggestionsUi.java"


# virtual methods
.method public abstract getQuery()Lcom/google/android/velvet/Query;
.end method

.method public abstract indicateRemoveFromHistoryFailed()V
.end method

.method public abstract setWebSuggestionsEnabled(Z)V
.end method

.method public abstract showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
.end method
