.class Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$1;
.super Ljava/lang/Object;
.source "RemoveFromHistoryButtonsPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$1;->this$0:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$1;->this$0:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mShowRemoveFromHistoryButtons:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$1;->this$0:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mView:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->access$000(Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;)Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;->updateHistoryButtonsView()V

    return-void
.end method
