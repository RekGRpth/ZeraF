.class final Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;
.super Ljava/lang/Object;
.source "SuggestionListFactory.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/SuggestionListFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SuggestionsComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/searchcommon/suggest/Suggestion;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContainsLastAccessTime:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;->mContainsLastAccessTime:Z

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/Suggestion;)I
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    if-nez p1, :cond_2

    if-nez p2, :cond_1

    const/4 v1, 0x0

    :goto_0
    move v0, v1

    :cond_0
    :goto_1
    return v0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    const/4 v0, -0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;->mContainsLastAccessTime:Z

    if-eqz v1, :cond_4

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getLastAccessTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getLastAccessTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    long-to-int v0, v1

    if-nez v0, :cond_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/Util;->compareAsStrings(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/Util;->compareAsStrings(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/suggest/Suggestion;

    check-cast p2, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/suggest/SuggestionListFactory$SuggestionsComparator;->compare(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/Suggestion;)I

    move-result v0

    return v0
.end method
