.class public Lcom/google/android/searchcommon/suggest/SuggestionListImpl;
.super Ljava/lang/Object;
.source "SuggestionListImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionList;


# instance fields
.field protected mAccount:Ljava/lang/String;

.field private final mCreationTime:J

.field protected mExtraColumns:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mIsFinal:Z

.field private mIsFromCache:Z

.field protected mLatency:I

.field protected mRequestFailed:Z

.field private final mSourceName:Ljava/lang/String;

.field protected mSourceSuggestionCount:I

.field private mSourceSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

.field protected final mSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserQuery:Lcom/google/android/velvet/Query;

.field protected mWasRequestMade:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mLatency:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mWasRequestMade:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mUserQuery:Lcom/google/android/velvet/Query;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mCreationTime:J

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;IJ)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # I
    .param p4    # J

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p3}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;J)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mLatency:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mWasRequestMade:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mUserQuery:Lcom/google/android/velvet/Query;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mCreationTime:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mLatency:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mWasRequestMade:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mUserQuery:Lcom/google/android/velvet/Query;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    iput-wide p4, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mCreationTime:J

    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;[Lcom/google/android/searchcommon/suggest/Suggestion;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # [Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected copyInto(Lcom/google/android/searchcommon/suggest/MutableSuggestionList;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;

    invoke-interface {p1, p0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->addAll(Ljava/lang/Iterable;)I

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mAccount:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setAccount(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mLatency:I

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setLatency(I)V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mRequestFailed:Z

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setRequestFailed(Z)V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mWasRequestMade:Z

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setRequestMade(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setSourceSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mIsFromCache:Z

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setFromCache(Z)V

    return-void
.end method

.method public get(I)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/suggest/Suggestion;

    return-object v0
.end method

.method public getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCreationTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mCreationTime:J

    return-wide v0
.end method

.method public getExtraColumns()Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    if-nez v7, :cond_2

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    iget-object v7, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getExtras()Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v2, v6

    :goto_0
    if-eqz v2, :cond_0

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionExtras;->getExtraColumnNames()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionExtras;->getExtraColumnNames()Ljava/util/Collection;

    move-result-object v2

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    :goto_2
    return-object v6

    :cond_3
    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    goto :goto_2
.end method

.method public getLatency()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mLatency:I

    return v0
.end method

.method public getMutableCopy()Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .locals 5

    new-instance v0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mUserQuery:Lcom/google/android/velvet/Query;

    iget-wide v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mCreationTime:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->copyInto(Lcom/google/android/searchcommon/suggest/MutableSuggestionList;)V

    return-object v0
.end method

.method public getSourceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceName:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceSuggestionCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceSuggestionCount:I

    return v0
.end method

.method public getSourceSuggestions()Lcom/google/android/searchcommon/suggest/Suggestions;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    return-object v0
.end method

.method public getUserQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mUserQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method public indexOf(Lcom/google/android/searchcommon/suggest/Suggestion;)I
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isFinal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mIsFinal:Z

    return v0
.end method

.method public isFromCache()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mIsFromCache:Z

    return v0
.end method

.method public isRequestFailed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mRequestFailed:Z

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Iterators;->unmodifiableIterator(Ljava/util/Iterator;)Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v0

    return-object v0
.end method

.method public setFromCache(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mIsFromCache:Z

    return-void
.end method

.method public setLatency(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mLatency:I

    return-void
.end method

.method public setRequestFailed(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mRequestFailed:Z

    return-void
.end method

.method public setRequestMade(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mWasRequestMade:Z

    return-void
.end method

.method public setSourceSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSourceSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wasRequestMade()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->mWasRequestMade:Z

    return v0
.end method
