.class public Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;
.super Landroid/content/BroadcastReceiver;
.source "SearchWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static getSearchWidgetState(Landroid/content/Context;I)Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    new-instance v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;

    invoke-direct {v0, p1}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;-><init>(I)V

    const-string v1, "launcher-widget"

    invoke-static {p0, v1}, Lcom/google/android/velvet/util/IntentUtils;->createSearchIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->setQueryTextViewIntent(Landroid/content/Intent;)V

    const-string v1, "launcher-widget"

    invoke-static {p0, v1}, Lcom/google/android/velvet/util/IntentUtils;->createVoiceSearchIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->setVoiceSearchIntent(Landroid/content/Intent;)V

    return-object v0
.end method

.method private static getSearchWidgetStates(Landroid/content/Context;)[Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;
    .locals 5
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->myComponentName(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    array-length v4, v0

    new-array v3, v4, [Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_0

    aget v4, v0, v2

    invoke-static {p0, v4}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->getSearchWidgetState(Landroid/content/Context;I)Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;

    move-result-object v4

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private static myComponentName(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public static updateSearchWidgets(Landroid/content/Context;)V
    .locals 6
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->getSearchWidgetStates(Landroid/content/Context;)[Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;

    move-result-object v4

    move-object v0, v4

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    invoke-virtual {v3, p0, v5}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.appwidget.action.APPWIDGET_ENABLED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->updateSearchWidgets(Landroid/content/Context;)V

    goto :goto_0
.end method
