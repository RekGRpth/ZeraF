.class public Lcom/google/android/googlequicksearchbox/DebugIntent;
.super Ljava/lang/Object;
.source "DebugIntent.java"


# static fields
.field private static final AUTOMATE_INTENT_DATA:Ljava/lang/String;

.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final SKIP_SETUP_EXTRA:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/googlequicksearchbox/DebugIntent;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/googlequicksearchbox/DebugIntent;->CLASS_NAME:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/googlequicksearchbox/DebugIntent;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".automate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/googlequicksearchbox/DebugIntent;->AUTOMATE_INTENT_DATA:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/googlequicksearchbox/DebugIntent;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".skipSetup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/googlequicksearchbox/DebugIntent;->SKIP_SETUP_EXTRA:Ljava/lang/String;

    return-void
.end method

.method public static handleIntent(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Landroid/app/Activity;
    .param p1    # Landroid/content/Intent;

    return-void
.end method

.method public static shouldSkipSetup(Landroid/content/Intent;)Z
    .locals 1
    .param p0    # Landroid/content/Intent;

    const/4 v0, 0x0

    return v0
.end method
