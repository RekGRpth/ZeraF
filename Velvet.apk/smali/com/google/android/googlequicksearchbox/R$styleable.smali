.class public final Lcom/google/android/googlequicksearchbox/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/googlequicksearchbox/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final BitmapSoundLevelView:[I

.field public static final CameraButton:[I

.field public static final CardTableLayout:[I

.field public static final CoScrollContainer_Layout:[I

.field public static final CrossfadingWebImageView:[I

.field public static final DragHandle:[I

.field public static final DrawSoundLevelView:[I

.field public static final Histogram:[I

.field public static final ProportionalLayout:[I

.field public static final QuotedTextView:[I

.field public static final ResultCard:[I

.field public static final RingView:[I

.field public static final RoundedCornerWebImageView:[I

.field public static final ScrollBinder:[I

.field public static final SuggestionGridLayout:[I

.field public static final SuggestionGridLayout_Layout:[I

.field public static final SuggestionListView:[I

.field public static final TruncatedTextView:[I

.field public static final VelvetFragment:[I

.field public static final WebImageView:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->BitmapSoundLevelView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->CameraButton:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->CardTableLayout:[I

    new-array v0, v4, [I

    const v1, 0x7f01000f

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->CoScrollContainer_Layout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->CrossfadingWebImageView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->DragHandle:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->DrawSoundLevelView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->Histogram:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->ProportionalLayout:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->QuotedTextView:[I

    new-array v0, v4, [I

    const v1, 0x7f010005

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->ResultCard:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->RingView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->RoundedCornerWebImageView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->ScrollBinder:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->SuggestionGridLayout:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->SuggestionGridLayout_Layout:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->SuggestionListView:[I

    new-array v0, v4, [I

    const v1, 0x7f010038

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->TruncatedTextView:[I

    new-array v0, v4, [I

    const v1, 0x7f010008

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->VelvetFragment:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->WebImageView:[I

    return-void

    :array_0
    .array-data 4
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
    .end array-data

    :array_1
    .array-data 4
        0x7f01002f
        0x7f010030
        0x7f010031
    .end array-data

    :array_2
    .array-data 4
        0x7f010036
        0x7f010037
    .end array-data

    :array_3
    .array-data 4
        0x7f010023
        0x7f010024
        0x7f010025
    .end array-data

    :array_4
    .array-data 4
        0x7f01001c
        0x7f01001d
    .end array-data

    :array_5
    .array-data 4
        0x7f010013
        0x7f010014
        0x7f01001a
        0x7f01001b
    .end array-data

    :array_6
    .array-data 4
        0x7f010026
        0x7f010027
    .end array-data

    :array_7
    .array-data 4
        0x7f010021
        0x7f010022
    .end array-data

    :array_8
    .array-data 4
        0x7f010006
        0x7f010007
    .end array-data

    :array_9
    .array-data 4
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
    .end array-data

    :array_a
    .array-data 4
        0x7f010034
        0x7f010035
    .end array-data

    :array_b
    .array-data 4
        0x7f01001e
        0x7f01001f
        0x7f010020
    .end array-data

    :array_c
    .array-data 4
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
    .end array-data

    :array_d
    .array-data 4
        0x7f010000
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010011
        0x7f010012
    .end array-data

    :array_e
    .array-data 4
        0x7f010001
        0x7f010010
    .end array-data

    :array_f
    .array-data 4
        0x7f010032
        0x7f010033
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
