.class public Lcom/google/android/apps/sidekick/CalendarEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "CalendarEntryAdapter.java"


# instance fields
.field private final mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

.field private final mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

.field private final mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

.field private final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field private final mEntryLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .param p3    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p6    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p7    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p8    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p7, p8}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCalendarEntry()Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getHash()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->getCalendarDataByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mEntryLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/geo/sidekick/Sidekick$Location;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLat()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLng()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$Location;->setLat(D)Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$Location;->setLng(D)Lcom/google/geo/sidekick/Sidekick$Location;

    :cond_2
    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$Location;->setLat(D)Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$Location;->setLng(D)Lcom/google/geo/sidekick/Sidekick$Location;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    return-object v0
.end method

.method private addEmailButton(Landroid/content/Context;Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getNumberOfAttendees()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    :goto_0
    return-void

    :cond_0
    const v3, 0x7f10002c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    const v3, 0x7f1000ef

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const v3, 0x7f100199

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v3, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private createExampleCalendarData(Landroid/content/Context;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 4
    .param p1    # Landroid/content/Context;

    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;-><init>()V

    const v2, 0x7f0d01d9

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v1

    const v2, 0x7f0d01da

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setWhereField(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v1

    const-wide/32 v2, 0x4f60f668

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setStartTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setEventData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v1

    return-object v1
.end method

.method private createNextAppointmentCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    const v3, 0x7f040082

    invoke-virtual {p2, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f100031

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f100197

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getFormattedStartTime(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getWhereField()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const v3, 0x7f100198

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getIsExample()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->addEmailButton(Landroid/content/Context;Landroid/view/View;)V

    :cond_1
    return-object v1
.end method

.method private createTimeToLeaveCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v10, 0x7f0400ca

    const/4 v11, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v10, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->showSample()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->createExampleCalendarData(Landroid/content/Context;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v2

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v10

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v10, v11}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->formatTime(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v4

    const v10, 0x7f100031

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    const v11, 0x7f0d0164

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getTitle()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v4, v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v10, 0x7f100198

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v10, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-direct {v10}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;-><init>()V

    iget-object v11, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v10, v11}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v5

    iget-object v10, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v10

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v8

    :goto_0
    new-instance v10, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-direct {v10}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;-><init>()V

    invoke-virtual {v10, v5}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->setFrequentPlace(Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v6

    if-eqz v8, :cond_1

    invoke-virtual {v6, v8}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->addRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->showSample()Z

    move-result v10

    if-eqz v10, :cond_7

    const v10, 0x7f100028

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/sidekick/StaticMapView;

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/StaticMapView;->showSampleRoute()V

    :goto_1
    if-eqz v8, :cond_9

    iget-object v10, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v12

    invoke-interface {v10, v11, v12}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->checkNavigationAvailability(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Z

    move-result v10

    if-eqz v10, :cond_9

    const v10, 0x7f10002c

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelTimeWithoutDelayInMinutes()Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v10, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v8, v10}, Lcom/google/android/apps/sidekick/TimeUtilities;->getEtaString(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Z)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v3, :cond_8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary()Z

    move-result v10

    if-eqz v10, :cond_8

    const v10, 0x7f0d010a

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_2
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v10, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;

    invoke-direct {v10, p0, v8}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->showSample()Z

    move-result v10

    if-nez v10, :cond_5

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->addEmailButton(Landroid/content/Context;Landroid/view/View;)V

    :cond_5
    return-object v9

    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_7
    const v10, 0x7f100028

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/sidekick/StaticMapView;

    iget-object v11, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mEntryLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    invoke-virtual {v10, v11, v6, v12, v13}, Lcom/google/android/apps/sidekick/StaticMapView;->setLocations(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLcom/google/android/apps/sidekick/inject/StaticMapCache;)V

    goto :goto_1

    :cond_8
    if-eqz v3, :cond_3

    const v10, 0x7f0d0109

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->showSample()Z

    move-result v10

    if-eqz v10, :cond_4

    const v10, 0x7f10002c

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method private formatTime(Landroid/content/Context;J)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # J

    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p2

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/apps/sidekick/TimeUtilities;->formatDisplayTime(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private showSample()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getIsExample()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createEmailAttendeesIntent(J)Landroid/content/Intent;
    .locals 2
    .param p1    # J

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.calendar.MAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.calendar/com.android.calendar.alerts.AlertReceiver"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "eventid"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    return-object v0
.end method

.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getCalendarData()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0207

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0175

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0176

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormattedStartTime(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->formatTime(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d02c8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d02c7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TimeToLeaveCalendar"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "NextAppointment"

    goto :goto_0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 7

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->showSample()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->createTimeToLeaveCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->createTimeToLeaveCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->createNextAppointmentCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f10005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getWhereField()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasWhereField()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 12
    .param p1    # Landroid/content/Context;

    const-wide/16 v10, 0x3e8

    const/4 v9, 0x0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getWaypoints(I)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v5

    invoke-direct {v4, p1, p0, v3, v5}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;I)V

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->run()V

    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->logDetailsInteraction(Landroid/content/Context;)V

    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    if-eqz v4, :cond_0

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "content://com.android.calendar/events/%1$s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEventId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    const-string v4, "beginTime"

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v5

    mul-long/2addr v5, v10

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v4, "endTime"

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEndTimeSeconds()J

    move-result-wide v5

    mul-long/2addr v5, v10

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v4

    const v5, 0x7f0d039b

    invoke-interface {v4, p1, v1, v5}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeStartActivityWithMessage(Landroid/content/Context;Landroid/content/Intent;I)Z

    goto :goto_0
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    new-instance v0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->addClientRunnable(Ljava/lang/Runnable;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    return-void
.end method

.method public registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLat()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLng()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f10019a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getIsExample()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
