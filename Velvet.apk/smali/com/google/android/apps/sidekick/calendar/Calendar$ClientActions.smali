.class public final Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/Calendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientActions"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasIsDismissed:Z

.field private hasIsNotificationDismissed:Z

.field private hasIsNotified:Z

.field private isDismissed_:Z

.field private isNotificationDismissed_:Z

.field private isNotified_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isNotified_:Z

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isDismissed_:Z

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isNotificationDismissed_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->cachedSize:I

    return v0
.end method

.method public getIsDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isDismissed_:Z

    return v0
.end method

.method public getIsNotificationDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isNotificationDismissed_:Z

    return v0
.end method

.method public getIsNotified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isNotified_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotified()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotified()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsDismissed()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsDismissed()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotificationDismissed()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotificationDismissed()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->cachedSize:I

    return v0
.end method

.method public hasIsDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsDismissed:Z

    return v0
.end method

.method public hasIsNotificationDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotificationDismissed:Z

    return v0
.end method

.method public hasIsNotified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotified:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->setIsNotified(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->setIsDismissed(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->setIsNotificationDismissed(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v0

    return-object v0
.end method

.method public setIsDismissed(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsDismissed:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isDismissed_:Z

    return-object p0
.end method

.method public setIsNotificationDismissed(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotificationDismissed:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isNotificationDismissed_:Z

    return-object p0
.end method

.method public setIsNotified(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotified:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->isNotified_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotified()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotified()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsDismissed()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsDismissed()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->hasIsNotificationDismissed()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotificationDismissed()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    return-void
.end method
