.class public final Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/Calendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServerData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private dataClearedBecauseEventChanged_:Z

.field private geocodedLatLng_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

.field private hasDataClearedBecauseEventChanged:Z

.field private hasGeocodedLatLng:Z

.field private hasIsGeocodable:Z

.field private hasNotifyTimeSeconds:Z

.field private hasServerHash:Z

.field private hasTravelTimeMinutes:Z

.field private isGeocodable_:Z

.field private notifyTimeSeconds_:J

.field private serverHash_:Ljava/lang/String;

.field private travelTimeMinutes_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->serverHash_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->geocodedLatLng_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    iput-boolean v2, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->isGeocodable_:Z

    iput v2, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->travelTimeMinutes_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->notifyTimeSeconds_:J

    iput-boolean v2, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->dataClearedBecauseEventChanged_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearServerHash()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearIsGeocodable()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearTravelTimeMinutes()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearNotifyTimeSeconds()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearDataClearedBecauseEventChanged()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->cachedSize:I

    return-object p0
.end method

.method public clearDataClearedBecauseEventChanged()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasDataClearedBecauseEventChanged:Z

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->dataClearedBecauseEventChanged_:Z

    return-object p0
.end method

.method public clearGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->geocodedLatLng_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    return-object p0
.end method

.method public clearIsGeocodable()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable:Z

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->isGeocodable_:Z

    return-object p0
.end method

.method public clearNotifyTimeSeconds()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->notifyTimeSeconds_:J

    return-object p0
.end method

.method public clearServerHash()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasServerHash:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->serverHash_:Ljava/lang/String;

    return-object p0
.end method

.method public clearTravelTimeMinutes()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes:Z

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->travelTimeMinutes_:I

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->cachedSize:I

    return v0
.end method

.method public getDataClearedBecauseEventChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->dataClearedBecauseEventChanged_:Z

    return v0
.end method

.method public getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->geocodedLatLng_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    return-object v0
.end method

.method public getIsGeocodable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->isGeocodable_:Z

    return v0
.end method

.method public getNotifyTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->notifyTimeSeconds_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasServerHash()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getTravelTimeMinutes()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getNotifyTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasDataClearedBecauseEventChanged()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getDataClearedBecauseEventChanged()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->cachedSize:I

    return v0
.end method

.method public getServerHash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->serverHash_:Ljava/lang/String;

    return-object v0
.end method

.method public getTravelTimeMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->travelTimeMinutes_:I

    return v0
.end method

.method public hasDataClearedBecauseEventChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasDataClearedBecauseEventChanged:Z

    return v0
.end method

.method public hasGeocodedLatLng()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng:Z

    return v0
.end method

.method public hasIsGeocodable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable:Z

    return v0
.end method

.method public hasNotifyTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds:Z

    return v0
.end method

.method public hasServerHash()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasServerHash:Z

    return v0
.end method

.method public hasTravelTimeMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setGeocodedLatLng(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setIsGeocodable(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setTravelTimeMinutes(I)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setNotifyTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setDataClearedBecauseEventChanged(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v0

    return-object v0
.end method

.method public setDataClearedBecauseEventChanged(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasDataClearedBecauseEventChanged:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->dataClearedBecauseEventChanged_:Z

    return-object p0
.end method

.method public setGeocodedLatLng(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->geocodedLatLng_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    return-object p0
.end method

.method public setIsGeocodable(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->isGeocodable_:Z

    return-object p0
.end method

.method public setNotifyTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->notifyTimeSeconds_:J

    return-object p0
.end method

.method public setServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasServerHash:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->serverHash_:Ljava/lang/String;

    return-object p0
.end method

.method public setTravelTimeMinutes(I)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes:Z

    iput p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->travelTimeMinutes_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasServerHash()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getTravelTimeMinutes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getNotifyTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasDataClearedBecauseEventChanged()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getDataClearedBecauseEventChanged()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    return-void
.end method
