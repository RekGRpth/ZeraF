.class Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;
.super Ljava/lang/Object;
.source "MovieTicketEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->maybeAddNavigateButton(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

.field final synthetic val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

.field final synthetic val$theaterLocation:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/geo/sidekick/Sidekick$Location;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$theaterLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointCount()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointList()Ljava/util/List;

    move-result-object v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelMode()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;)Lcom/google/android/apps/sidekick/DirectionsLauncher;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->val$theaterLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-interface {v3, v2, v4, v1, v0}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->start(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/util/List;I)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v2

    const-string v3, "CARD_BUTTON_PRESS"

    iget-object v4, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    const-string v5, "NAVIGATE"

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method
