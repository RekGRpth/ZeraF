.class public Lcom/google/android/apps/sidekick/sync/StateDifference;
.super Ljava/lang/Object;
.source "StateDifference.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/protobuf/micro/MessageMicro;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final ADD_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

.field private static final DELETE_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

.field private static final UPDATE_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;


# instance fields
.field private final mFromMessage:Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mMessageVisitor:Lcom/google/android/apps/sidekick/sync/MessageVisitor;

.field private final mToMessage:Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/sync/StateDifference$1;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/sync/StateDifference$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/sidekick/sync/StateDifference;->UPDATE_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

    new-instance v0, Lcom/google/android/apps/sidekick/sync/StateDifference$2;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/sync/StateDifference$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/sidekick/sync/StateDifference;->ADD_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

    new-instance v0, Lcom/google/android/apps/sidekick/sync/StateDifference$3;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/sync/StateDifference$3;-><init>()V

    sput-object v0, Lcom/google/android/apps/sidekick/sync/StateDifference;->DELETE_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;)V
    .locals 2
    .param p3    # Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;",
            "Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mFromMessage:Lcom/google/protobuf/micro/MessageMicro;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mToMessage:Lcom/google/protobuf/micro/MessageMicro;

    new-instance v0, Lcom/google/android/apps/sidekick/sync/MessageVisitor;

    const/4 v1, 0x1

    invoke-direct {v0, p3, v1}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;-><init>(Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;Z)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mMessageVisitor:Lcom/google/android/apps/sidekick/sync/MessageVisitor;

    return-void
.end method

.method private static newMessageMicro(Ljava/lang/Class;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static newStateDifference(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;)Lcom/google/android/apps/sidekick/sync/StateDifference;
    .locals 1
    .param p2    # Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(TT;TT;",
            "Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;",
            ")",
            "Lcom/google/android/apps/sidekick/sync/StateDifference",
            "<TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/sidekick/sync/StateDifference;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/sidekick/sync/StateDifference;-><init>(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;)V

    return-object v0
.end method


# virtual methods
.method public compareForAdds()Lcom/google/protobuf/micro/MessageMicro;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mFromMessage:Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/sync/StateDifference;->newMessageMicro(Ljava/lang/Class;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mToMessage:Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {v1, v0}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mMessageVisitor:Lcom/google/android/apps/sidekick/sync/MessageVisitor;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mFromMessage:Lcom/google/protobuf/micro/MessageMicro;

    sget-object v3, Lcom/google/android/apps/sidekick/sync/StateDifference;->ADD_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->visit(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;)V

    return-object v0
.end method

.method public compareForDeletes()Lcom/google/protobuf/micro/MessageMicro;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mFromMessage:Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/sync/StateDifference;->newMessageMicro(Ljava/lang/Class;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mToMessage:Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {v1, v0}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mMessageVisitor:Lcom/google/android/apps/sidekick/sync/MessageVisitor;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mFromMessage:Lcom/google/protobuf/micro/MessageMicro;

    sget-object v3, Lcom/google/android/apps/sidekick/sync/StateDifference;->DELETE_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->visit(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;)V

    return-object v0
.end method

.method public compareForUpdates()Lcom/google/protobuf/micro/MessageMicro;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mFromMessage:Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/sync/StateDifference;->newMessageMicro(Ljava/lang/Class;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mToMessage:Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {v1, v0}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mMessageVisitor:Lcom/google/android/apps/sidekick/sync/MessageVisitor;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/sync/StateDifference;->mFromMessage:Lcom/google/protobuf/micro/MessageMicro;

    sget-object v3, Lcom/google/android/apps/sidekick/sync/StateDifference;->UPDATE_VISITOR:Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->visit(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;)V

    return-object v0
.end method
