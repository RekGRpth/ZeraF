.class final Lcom/google/android/apps/sidekick/sync/StateDifference$1;
.super Ljava/lang/Object;
.source "StateDifference.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/sync/StateDifference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visitRepeated(Ljava/lang/reflect/Field;Ljava/util/SortedMap;Ljava/util/SortedMap;)Ljava/util/SortedMap;
    .locals 7
    .param p1    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;)",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v4

    invoke-interface {p2}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {p3}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/common/collect/Sets;->intersection(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/collect/Sets$SetView;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p2, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/micro/MessageMicro;

    invoke-interface {p3, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v0}, Lcom/google/protobuf/micro/MessageMicro;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/protobuf/micro/MessageMicro;->toByteArray()[B

    move-result-object v6

    invoke-static {v5, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {p3, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method public visitScalar(Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 3
    .param p1    # Ljava/lang/reflect/Field;
    .param p2    # Lcom/google/protobuf/micro/MessageMicro;
    .param p3    # Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {p2, p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p3, p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p3, p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->clearFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)V

    :cond_0
    return-void
.end method
