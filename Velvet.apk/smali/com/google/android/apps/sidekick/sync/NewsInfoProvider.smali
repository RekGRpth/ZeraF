.class Lcom/google/android/apps/sidekick/sync/NewsInfoProvider;
.super Ljava/lang/Object;
.source "NewsInfoProvider.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public generateKeyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;->getQuery()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/sync/NewsInfoProvider;->generateKeyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
