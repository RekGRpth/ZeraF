.class public Lcom/google/android/apps/sidekick/sync/MessageVisitor;
.super Ljava/lang/Object;
.source "MessageVisitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;
    }
.end annotation


# instance fields
.field private final mMessageInfo:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

.field private final mPruneEmptyMessages:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;Z)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->mMessageInfo:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    iput-boolean p2, p0, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->mPruneEmptyMessages:Z

    return-void
.end method

.method private mapFromRepeatedField(Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;)Ljava/util/SortedMap;
    .locals 5
    .param p1    # Ljava/lang/reflect/Field;
    .param p2    # Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ")",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v2

    if-eqz p2, :cond_1

    invoke-static {p2, p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getRepeatedFieldAsList(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isMessageField(Ljava/lang/reflect/Field;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->mMessageInfo:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    invoke-interface {v4, v3}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-interface {v2, v1, v3}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method private mapToRepeatedField(Ljava/util/Map;Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 3
    .param p2    # Ljava/lang/reflect/Field;
    .param p3    # Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;",
            "Ljava/lang/reflect/Field;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ")V"
        }
    .end annotation

    invoke-static {p3, p2}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->clearRepeatedField(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)V

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {p3, p2, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->addRepeatedField(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public visit(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;)V
    .locals 9
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;
    .param p2    # Lcom/google/protobuf/micro/MessageMicro;
    .param p3    # Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;

    invoke-static {p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getProtoFields(Lcom/google/protobuf/micro/MessageMicro;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Field;

    invoke-static {v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isRepeatedField(Ljava/lang/reflect/Field;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {p1, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getRepeatedFieldCount(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)I

    move-result v8

    if-lez v8, :cond_0

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->mapFromRepeatedField(Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;)Ljava/util/SortedMap;

    move-result-object v6

    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->mapFromRepeatedField(Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {p3, v1, v6, v0}, Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;->visitRepeated(Ljava/lang/reflect/Field;Ljava/util/SortedMap;Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v5

    if-eqz p2, :cond_0

    invoke-direct {p0, v5, v1, p2}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->mapToRepeatedField(Ljava/util/Map;Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    invoke-static {p1, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->hasField(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isMessageField(Ljava/lang/reflect/Field;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-static {p2, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->hasField(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {p1, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/micro/MessageMicro;

    const/4 v3, 0x0

    if-eqz p2, :cond_2

    invoke-static {p2, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getFieldBuilder(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/micro/MessageMicro;

    :cond_2
    invoke-virtual {p0, v4, v3, p3}, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->visit(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;)V

    iget-boolean v8, p0, Lcom/google/android/apps/sidekick/sync/MessageVisitor;->mPruneEmptyMessages:Z

    if-eqz v8, :cond_0

    if-eqz p2, :cond_0

    invoke-static {v3}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isEmpty(Lcom/google/protobuf/micro/MessageMicro;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-static {p2, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->clearFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)V

    goto :goto_0

    :cond_3
    invoke-interface {p3, v1, p1, p2}, Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;->visitScalar(Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    return-void
.end method
