.class public Lcom/google/android/apps/sidekick/TransitEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "TransitEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;
    }
.end annotation


# static fields
.field private static final MAPS_PLACE_URI:Landroid/net/Uri;


# instance fields
.field private final mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://maps.google.com/maps/place"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->MAPS_PLACE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getTransitStationEntry()Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    return-void
.end method

.method private getVehicleType()I
    .locals 6

    const/4 v4, -0x1

    const/4 v3, -0x1

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->getLineList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getVehicleType()I

    move-result v2

    if-ne v3, v4, :cond_1

    move v3, v2

    goto :goto_0

    :cond_1
    if-eq v3, v2, :cond_0

    move v3, v4

    :cond_2
    return v3
.end method

.method private getVehicleTypeString(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->getVehicleType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    const v1, 0x7f0d010f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0d0110

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    const v1, 0x7f0d0111

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    const v1, 0x7f0d010e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$2;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/TransitEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/TransitEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v1
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d01a1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d01a2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->getVehicleType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v1, 0x7f0d02dd

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    const v1, 0x7f0d02db

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0d02dc

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 34
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v3, 0x7f0400cc

    const/16 v30, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, v30

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v29

    const v3, 0x7f100031

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->getStationName()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->getVehicleTypeString(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v27

    if-eqz v27, :cond_0

    const v3, 0x7f100267

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->getResponseEpochSeconds()J

    move-result-wide v10

    invoke-static/range {p1 .. p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->getLineList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasColor()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getColor()I

    move-result v8

    :goto_0
    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->hasTextColor()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getTextColor()I

    move-result v9

    :goto_1
    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line;->getDepartureGroupList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;->getHeadsign()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;->getDepartureList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :cond_3
    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeDepartureTimeSeconds()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->getRelativeDepartureTimeSeconds()I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v17, v0

    add-long v30, v17, v10

    const-wide/16 v32, 0x3e8

    mul-long v4, v30, v32

    new-instance v3, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;-><init>(JLjava/lang/String;Ljava/lang/String;II)V

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    goto :goto_0

    :cond_5
    const/4 v9, 0x0

    goto :goto_1

    :cond_6
    invoke-static/range {v19 .. v19}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const v3, 0x7f100268

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TableLayout;

    const/16 v20, 0x0

    :goto_3
    const/4 v3, 0x5

    move/from16 v0, v20

    if-ge v0, v3, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v20

    if-ge v0, v3, :cond_8

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;

    const v3, 0x7f0400cb

    const/16 v30, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v16

    iget v3, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSignColor:I

    if-eqz v3, :cond_7

    move-object/from16 v0, v16

    iget v3, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSignTextColor:I

    if-eqz v3, :cond_7

    const v3, 0x7f100265

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSignColor:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    const v3, 0x7f100265

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSignTextColor:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_7
    const v3, 0x7f100265

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mLine:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f1000dc

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSign:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f100266

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->getDepartureTimeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_3

    :cond_8
    const v3, 0x7f100035

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    new-instance v30, Lcom/google/android/apps/sidekick/TransitEntryAdapter$1;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/TransitEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/TransitEntryAdapter;Landroid/content/Context;)V

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v29
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f10005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->hasStationId()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TransitEntryAdapter"

    const-string v1, "Missing station identifier"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "0x%1$x:0x%2$x"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->getStationId()Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;->getCellId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->getStationId()Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;->getFprint()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->MAPS_PLACE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ftid"

    invoke-virtual {v0, v1, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const-string v4, "DETAILS"

    const v5, 0x7f0d0399

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->openUrlWithMessage(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;->mTransitStation:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;->getLineCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
