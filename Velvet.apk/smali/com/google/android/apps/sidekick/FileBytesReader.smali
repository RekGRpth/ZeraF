.class public Lcom/google/android/apps/sidekick/FileBytesReader;
.super Ljava/lang/Object;
.source "FileBytesReader.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mSignedCipherHelper:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/FileBytesReader;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/FileBytesReader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/FileBytesReader;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/FileBytesReader;->mSignedCipherHelper:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    return-void
.end method


# virtual methods
.method public readEncryptedFileBytes(Ljava/lang/String;I)[B
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/FileBytesReader;->readFileBytes(Ljava/lang/String;I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/FileBytesReader;->mSignedCipherHelper:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-interface {v1, v0}, Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;->decryptBytes([B)[B

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/sidekick/FileBytesReader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to read file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " crypto failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method public readFileBytes(Ljava/lang/String;I)[B
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v7, 0x0

    const/4 v8, 0x0

    :try_start_0
    new-instance v6, Ljava/io/File;

    iget-object v10, p0, Lcom/google/android/apps/sidekick/FileBytesReader;->mAppContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-direct {v6, v10, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->isFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    if-nez v10, :cond_0

    const/4 v10, 0x0

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_0
    return-object v10

    :cond_0
    :try_start_1
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v4

    int-to-long v10, p2

    cmp-long v10, v4, v10

    if-ltz v10, :cond_1

    sget-object v10, Lcom/google/android/apps/sidekick/FileBytesReader;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Data is too large ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " bytes) to read to disk: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v10, 0x0

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v10, p0, Lcom/google/android/apps/sidekick/FileBytesReader;->mAppContext:Landroid/content/Context;

    invoke-virtual {v10, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v7

    long-to-int v10, v4

    new-array v2, v10, [B

    const/4 v9, 0x0

    long-to-int v1, v4

    :cond_2
    invoke-virtual {v7, v2, v9, v1}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    const/4 v10, 0x1

    if-ge v0, v10, :cond_3

    :goto_1
    move-object v8, v2

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_2
    move-object v10, v8

    goto :goto_0

    :cond_3
    sub-int/2addr v1, v0

    add-int/2addr v9, v0

    if-gtz v1, :cond_2

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_3
    sget-object v10, Lcom/google/android/apps/sidekick/FileBytesReader;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to read file: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v10

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v10
.end method
