.class public Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;
.super Ljava/lang/Object;
.source "SystemLocationManagerInjectable.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;


# instance fields
.field private final mLocationManager:Landroid/location/LocationManager;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationManager:Landroid/location/LocationManager;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    return-void
.end method


# virtual methods
.method public getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SystemLocationManagerInjectable"

    const-string v1, "Location access is not permitted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method public isProviderEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeUpdates(Landroid/app/PendingIntent;)V
    .locals 1
    .param p1    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeUpdates(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # F
    .param p5    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SystemLocationManagerInjectable"

    const-string v1, "Location access is not permitted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;->mLocationManager:Landroid/location/LocationManager;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V

    goto :goto_0
.end method
