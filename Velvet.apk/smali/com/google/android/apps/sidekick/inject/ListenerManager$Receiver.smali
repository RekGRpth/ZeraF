.class Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;
.super Landroid/content/BroadcastReceiver;
.source "ListenerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/inject/ListenerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Receiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/inject/ListenerManager;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/inject/ListenerManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;->this$0:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/inject/ListenerManager;Lcom/google/android/apps/sidekick/inject/ListenerManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/ListenerManager;
    .param p2    # Lcom/google/android/apps/sidekick/inject/ListenerManager$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;-><init>(Lcom/google/android/apps/sidekick/inject/ListenerManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;->this$0:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    # getter for: Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->access$100(Lcom/google/android/apps/sidekick/inject/ListenerManager;)Ljava/util/Set;

    move-result-object v4

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;->this$0:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    # getter for: Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->access$100(Lcom/google/android/apps/sidekick/inject/ListenerManager;)Ljava/util/Set;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;->this$0:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    # getter for: Lcom/google/android/apps/sidekick/inject/ListenerManager;->mDispatcher:Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->access$200(Lcom/google/android/apps/sidekick/inject/ListenerManager;)Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;->dispatch(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_0
    return-void
.end method
