.class public final Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;
.super Ljava/lang/Object;
.source "SessionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/inject/SessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SessionKey"
.end annotation


# instance fields
.field public final expirationSeconds:J

.field public final jitteredExpirationSeconds:J

.field public final key:Ljava/util/UUID;


# direct methods
.method public constructor <init>(Ljava/util/UUID;JJ)V
    .locals 4
    .param p1    # Ljava/util/UUID;
    .param p2    # J
    .param p4    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v2

    invoke-virtual {p1}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->key:Ljava/util/UUID;

    iput-wide p2, p0, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->expirationSeconds:J

    iput-wide p4, p0, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->jitteredExpirationSeconds:J

    return-void
.end method
