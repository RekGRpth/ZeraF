.class public Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;
.super Ljava/lang/Object;
.source "ExecutedUserActionStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;
.implements Lcom/google/android/velvet/ActivityLifecycleObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl$WriteService;
    }
.end annotation


# static fields
.field static final ACTION_DELETE:Ljava/lang/String; = "delete"

.field static final ACTION_WRITE:Ljava/lang/String; = "write"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivityLifecycleNotifier:Lcom/google/android/velvet/ActivityLifecycleNotifier;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mContext:Landroid/content/Context;

.field private final mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

.field private final mFileLock:Ljava/lang/Object;

.field private final mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

.field private final mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/ActivityLifecycleNotifier;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/FileBytesReader;
    .param p3    # Lcom/google/android/apps/sidekick/FileBytesWriter;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;
    .param p5    # Lcom/google/android/velvet/ActivityLifecycleNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mActivityLifecycleNotifier:Lcom/google/android/velvet/ActivityLifecycleNotifier;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mActivityLifecycleNotifier:Lcom/google/android/velvet/ActivityLifecycleNotifier;

    invoke-interface {v0, p0}, Lcom/google/android/velvet/ActivityLifecycleNotifier;->addActivityLifecycleObserver(Lcom/google/android/velvet/ActivityLifecycleObserver;)V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method deleteStore()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    const-string v2, "executed_user_action_log"

    invoke-virtual {v0, v2}, Lcom/google/android/apps/sidekick/FileBytesWriter;->deleteFile(Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public flush()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    const-string v6, "executed_user_action_log"

    const/high16 v7, 0x80000

    invoke-virtual {v4, v6, v7}, Lcom/google/android/apps/sidekick/FileBytesReader;->readEncryptedFileBytes(Ljava/lang/String;I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-direct {v2}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->getExecutedUserActionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    const-string v6, "executed_user_action_log"

    invoke-virtual {v4, v6}, Lcom/google/android/apps/sidekick/FileBytesWriter;->deleteFile(Ljava/lang/String;)V

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    monitor-enter v5

    :try_start_3
    iget-object v4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->getExecutedUserActionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->clear()Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-object v3

    :catch_0
    move-exception v1

    :try_start_4
    sget-object v4, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->TAG:Ljava/lang/String;

    const-string v6, "File storage contained invalid data"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    :catchall_1
    move-exception v4

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4
.end method

.method handleWriteIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const-string v4, "actions"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v4, v3

    if-lez v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    const-string v6, "executed_user_action_log"

    const/high16 v7, 0x80000

    invoke-virtual {v4, v6, v7}, Lcom/google/android/apps/sidekick/FileBytesReader;->readEncryptedFileBytes(Ljava/lang/String;I)[B

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-direct {v2}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    array-length v4, v0

    if-eqz v4, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    invoke-virtual {v2, v3}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_2
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    const-string v6, "executed_user_action_log"

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->toByteArray()[B

    move-result-object v7

    const/high16 v8, 0x80000

    invoke-virtual {v4, v6, v7, v8}, Lcom/google/android/apps/sidekick/FileBytesWriter;->writeEncryptedFileBytes(Ljava/lang/String;[BI)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->TAG:Ljava/lang/String;

    const-string v6, "Failed to write actions"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    monitor-exit v5

    :goto_1
    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v4, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->TAG:Ljava/lang/String;

    const-string v6, "File storage contained invalid data"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v5

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    :catch_1
    move-exception v1

    :try_start_4
    sget-object v4, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->TAG:Ljava/lang/String;

    const-string v6, "Received intent with invalid action data"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public onActivityStart()V
    .locals 0

    return-void
.end method

.method public onActivityStop()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->getExecutedUserActionCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "write"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    const-class v5, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl$WriteService;

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "actions"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->startService(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->clear()Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public postDeleteStore()V
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "delete"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl$WriteService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->startService(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->clear()Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public saveAction(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;J)V
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # J

    new-instance v1, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;J)V

    invoke-virtual {v1, p3, p4}, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->withExecutionTimeMs(J)Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mExecutedUserActions:Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->build()Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->addExecutedUserAction(Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;)Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method startService(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
