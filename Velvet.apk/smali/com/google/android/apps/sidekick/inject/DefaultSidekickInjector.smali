.class public Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;
.super Ljava/lang/Object;
.source "DefaultSidekickInjector.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/SidekickInjector;


# instance fields
.field private final mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

.field private final mAlarmUtils:Lcom/google/android/apps/sidekick/AlarmUtils;

.field private final mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

.field private final mCalendarController:Lcom/google/android/apps/sidekick/calendar/CalendarController;

.field private final mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

.field private final mEntryInvalidator:Lcom/google/android/apps/sidekick/inject/EntryInvalidator;

.field private final mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

.field private final mGmmLocationManager:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

.field private final mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

.field private final mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

.field private final mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

.field private final mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

.field private final mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 52
    .param p1    # Landroid/content/Context;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static/range {p1 .. p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v50

    new-instance v3, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/apps/sidekick/inject/SystemLocationManagerInjectable;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    new-instance v3, Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mGmmLocationManager:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v6

    new-instance v13, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-direct {v13, v3}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;-><init>(Lcom/google/android/searchcommon/GsaPreferenceController;)V

    new-instance v7, Lcom/google/android/apps/sidekick/FileBytesReader;

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v13}, Lcom/google/android/apps/sidekick/FileBytesReader;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;)V

    new-instance v8, Lcom/google/android/apps/sidekick/FileBytesWriter;

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v13}, Lcom/google/android/apps/sidekick/FileBytesWriter;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;)V

    new-instance v3, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    invoke-direct {v3, v7, v8}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;-><init>(Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    invoke-static/range {p1 .. p1}, Lvedroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Lvedroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v30

    new-instance v3, Lcom/google/android/apps/sidekick/LocationOracleImpl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mGmmLocationManager:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v9

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getPendingIntentFactory()Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/sidekick/LocationOracleImpl;-><init>(Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/searchcommon/debug/DebugFeatures;Lcom/google/android/searchcommon/GsaPreferenceController;Lvedroid/support/v4/content/LocalBroadcastManager;Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    const-string v3, "connectivity"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/net/ConnectivityManager;

    new-instance v3, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;

    new-instance v14, Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v17, v0

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-object/from16 v19, v0

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v20

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    invoke-direct/range {v14 .. v20}, Lcom/google/android/apps/sidekick/SensorSignalsOracle;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/DeviceCapabilityManager;Lcom/google/android/apps/sidekick/inject/WidgetManager;Lcom/google/android/searchcommon/debug/DebugFeatures;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    new-instance v14, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;

    move-object/from16 v15, v51

    move-object/from16 v16, v7

    move-object/from16 v17, v8

    move-object/from16 v18, v6

    move-object/from16 v19, v51

    invoke-direct/range {v14 .. v19}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/ActivityLifecycleNotifier;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    new-instance v24, Lcom/google/android/apps/sidekick/SessionManagerImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    move-object/from16 v0, v24

    invoke-direct {v0, v3, v6, v13}, Lcom/google/android/apps/sidekick/SessionManagerImpl;-><init>(Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;)V

    new-instance v14, Lcom/google/android/apps/sidekick/VelvetNetworkClient;

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v17

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getVersionName()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v19

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    move-object/from16 v23, v0

    move-object/from16 v15, v50

    move-object/from16 v16, v30

    invoke-direct/range {v14 .. v24}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;-><init>(Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/debug/DebugFeatures;Ljava/lang/String;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/apps/sidekick/SensorSignalsOracle;Landroid/net/ConnectivityManager;Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;Lcom/google/android/apps/sidekick/inject/SessionManager;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    new-instance v25, Lcom/google/android/apps/sidekick/EntryProviderImpl;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v29, v0

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/AsyncServices;->getPooledBackgroundExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v31

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/AsyncServices;->getScheduledBackgroundExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v33

    move-object/from16 v26, v6

    move-object/from16 v27, p1

    move-object/from16 v32, p0

    invoke-direct/range {v25 .. v33}, Lcom/google/android/apps/sidekick/EntryProviderImpl;-><init>(Lcom/google/android/searchcommon/util/Clock;Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/SearchConfig;Ljava/util/concurrent/Executor;Lcom/google/android/apps/sidekick/inject/SidekickInjector;Ljava/util/concurrent/ScheduledExecutorService;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    new-instance v35, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/searchcommon/SearchConfig;->getStaticMapCacheMaxSize()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object/from16 v0, v35

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;-><init>(ILcom/google/android/apps/sidekick/inject/EntryProvider;)V

    new-instance v3, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v6}, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mCalendarController:Lcom/google/android/apps/sidekick/calendar/CalendarController;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mCalendarController:Lcom/google/android/apps/sidekick/calendar/CalendarController;

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/calendar/CalendarController;->newCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    new-instance v3, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v4

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    new-instance v49, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    move-object/from16 v0, v49

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;)V

    new-instance v38, Lcom/google/android/apps/sidekick/DirectionsLauncherImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object/from16 v0, v38

    move-object/from16 v1, p1

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/DirectionsLauncherImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    const-string v3, "wifi"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Landroid/net/wifi/WifiManager;

    new-instance v31, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-object/from16 v32, v0

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-object/from16 v37, v0

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v40

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSearchHistoryHelper()Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object/from16 v42, v0

    invoke-virtual/range {v51 .. v51}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v43

    move-object/from16 v34, v6

    invoke-direct/range {v31 .. v43}, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/android/apps/sidekick/DirectionsLauncher;Landroid/net/wifi/WifiManager;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/apps/sidekick/inject/ActivityHelper;Lcom/google/android/searchcommon/SearchConfig;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v45

    new-instance v3, Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v0, p1

    invoke-direct {v3, v7, v8, v6, v0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;-><init>(Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;Lcom/google/android/searchcommon/util/Clock;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    new-instance v46, Lcom/google/android/apps/sidekick/inject/SystemNotificationManagerInjectable;

    move-object/from16 v0, v46

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/inject/SystemNotificationManagerInjectable;-><init>(Landroid/content/Context;)V

    new-instance v40, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-object/from16 v47, v0

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getPendingIntentFactory()Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    move-result-object v48

    move-object/from16 v41, p1

    move-object/from16 v42, v6

    invoke-direct/range {v40 .. v48}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V

    move-object/from16 v0, v40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    new-instance v4, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v5}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/notifications/NotificationStore;)V

    invoke-interface {v3, v4}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->addLightweightGeofencer(Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;)V

    new-instance v14, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v17

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    move-object/from16 v19, v0

    invoke-direct/range {v14 .. v19}, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;-><init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Landroid/content/SharedPreferences;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryInvalidator:Lcom/google/android/apps/sidekick/inject/EntryInvalidator;

    new-instance v3, Lcom/google/android/apps/sidekick/AlarmUtils;

    invoke-interface/range {v50 .. v50}, Lcom/google/android/searchcommon/CoreSearchServices;->getAlarmManager()Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    new-instance v9, Ljava/util/Random;

    invoke-direct {v9}, Ljava/util/Random;-><init>()V

    invoke-direct {v3, v4, v6, v5, v9}, Lcom/google/android/apps/sidekick/AlarmUtils;-><init>(Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/GsaPreferenceController;Ljava/util/Random;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mAlarmUtils:Lcom/google/android/apps/sidekick/AlarmUtils;

    return-void
.end method


# virtual methods
.method public getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    return-object v0
.end method

.method public getAlarmUtils()Lcom/google/android/apps/sidekick/AlarmUtils;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mAlarmUtils:Lcom/google/android/apps/sidekick/AlarmUtils;

    return-object v0
.end method

.method public getAsyncFileStorage()Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    return-object v0
.end method

.method public getCalendarController()Lcom/google/android/apps/sidekick/calendar/CalendarController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mCalendarController:Lcom/google/android/apps/sidekick/calendar/CalendarController;

    return-object v0
.end method

.method public getCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    return-object v0
.end method

.method public getEntryInvalidator()Lcom/google/android/apps/sidekick/inject/EntryInvalidator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryInvalidator:Lcom/google/android/apps/sidekick/inject/EntryInvalidator;

    return-object v0
.end method

.method public getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    return-object v0
.end method

.method public getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-object v0
.end method

.method public getExecutedUserActionStore()Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    return-object v0
.end method

.method public getGmmLocationProvider()Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mGmmLocationManager:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    return-object v0
.end method

.method public getLocalBroadcastManager()Lvedroid/support/v4/content/LocalBroadcastManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

    return-object v0
.end method

.method public getLocationManager()Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    return-object v0
.end method

.method public getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    return-object v0
.end method

.method public getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-object v0
.end method

.method public getNotificationStore()Lcom/google/android/apps/sidekick/notifications/NotificationStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    return-object v0
.end method

.method public getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    return-object v0
.end method

.method public getSensorSignalsOracle()Lcom/google/android/apps/sidekick/SensorSignalsOracle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    return-object v0
.end method

.method public getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;

    return-object v0
.end method
