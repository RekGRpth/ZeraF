.class public interface abstract Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;
.super Ljava/lang/Object;
.source "PendingIntentFactory.java"


# virtual methods
.method public abstract createComponentName(Ljava/lang/Class;)Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/ComponentName;"
        }
    .end annotation
.end method

.method public abstract getBroadcast(ILandroid/content/Intent;I)Landroid/app/PendingIntent;
.end method

.method public abstract getService(ILandroid/content/Intent;I)Landroid/app/PendingIntent;
.end method
