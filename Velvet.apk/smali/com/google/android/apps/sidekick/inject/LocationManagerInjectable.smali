.class public interface abstract Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
.super Ljava/lang/Object;
.source "LocationManagerInjectable.java"


# virtual methods
.method public abstract getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract isProviderEnabled(Ljava/lang/String;)Z
.end method

.method public abstract removeUpdates(Landroid/app/PendingIntent;)V
.end method

.method public abstract requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V
.end method
