.class public interface abstract Lcom/google/android/apps/sidekick/inject/StaticMapCache;
.super Ljava/lang/Object;
.source "StaticMapCache.java"


# virtual methods
.method public abstract get(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;Z)Landroid/graphics/Bitmap;
.end method

.method public abstract put(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLandroid/graphics/Bitmap;)V
.end method
