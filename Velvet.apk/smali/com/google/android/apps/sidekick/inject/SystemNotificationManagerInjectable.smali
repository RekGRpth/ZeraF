.class public Lcom/google/android/apps/sidekick/inject/SystemNotificationManagerInjectable;
.super Ljava/lang/Object;
.source "SystemNotificationManagerInjectable.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;


# instance fields
.field private final mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemNotificationManagerInjectable;->mNotificationManager:Landroid/app/NotificationManager;

    return-void
.end method


# virtual methods
.method public cancel(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemNotificationManagerInjectable;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public cancelAll()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemNotificationManagerInjectable;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    return-void
.end method

.method public notify(ILandroid/app/Notification;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/app/Notification;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemNotificationManagerInjectable;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
