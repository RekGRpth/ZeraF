.class public interface abstract Lcom/google/android/apps/sidekick/EntryItemAdapter;
.super Ljava/lang/Object;
.source "EntryItemAdapter.java"


# virtual methods
.method public abstract examplify()V
.end method

.method public abstract findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
.end method

.method public abstract getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
.end method

.method public abstract getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.end method

.method public abstract getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;
.end method

.method public abstract getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
.end method

.method public abstract getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
.end method

.method public abstract getJustification(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
.end method

.method public abstract getLoggingName()Ljava/lang/String;
.end method

.method public abstract getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
.end method

.method public abstract getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract isDismissed()Z
.end method

.method public abstract maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
.end method

.method public abstract prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
.end method

.method public abstract registerActions(Landroid/app/Activity;Landroid/view/View;)V
.end method

.method public abstract registerBackOfCardMenuListener(Landroid/app/Activity;Landroid/view/View;)V
.end method

.method public abstract registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
.end method

.method public abstract setDismissed(Z)V
.end method

.method public abstract shouldDisplay()Z
.end method

.method public abstract supportsDismissTrail(Landroid/content/Context;)Z
.end method
