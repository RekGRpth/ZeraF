.class Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;
.super Ljava/lang/Object;
.source "ResearchTopicEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

.field final synthetic val$pagesList:Lcom/google/android/apps/sidekick/CardTableLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;Lcom/google/android/apps/sidekick/CardTableLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;->val$pagesList:Lcom/google/android/apps/sidekick/CardTableLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;->val$pagesList:Lcom/google/android/apps/sidekick/CardTableLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/CardTableLayout;->setExpanded(Z)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_EXPAND"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapter(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    return-void
.end method
