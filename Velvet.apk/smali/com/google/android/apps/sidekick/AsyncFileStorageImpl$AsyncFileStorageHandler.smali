.class Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;
.super Landroid/os/Handler;
.source "AsyncFileStorageImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncFileStorageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->this$0:Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private deleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->this$0:Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->access$400(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;)Lcom/google/android/apps/sidekick/FileBytesWriter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/FileBytesWriter;->deleteFile(Ljava/lang/String;)V

    return-void
.end method

.method private readFile(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;

    const/4 v0, 0x0

    :try_start_0
    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->access$500(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mEncrypted:Z
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->access$600(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)Z

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->readFileBytes(Ljava/lang/String;Z)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mCallback:Lcom/google/common/base/Function;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->access$700(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)Lcom/google/common/base/Function;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :catchall_0
    move-exception v1

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mCallback:Lcom/google/common/base/Function;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->access$700(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)Lcom/google/common/base/Function;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    throw v1
.end method

.method private readFileBytes(Ljava/lang/String;Z)[B
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/high16 v1, 0x80000

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->this$0:Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->access$1100(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;)Lcom/google/android/apps/sidekick/FileBytesReader;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/sidekick/FileBytesReader;->readEncryptedFileBytes(Ljava/lang/String;I)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->this$0:Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->access$1100(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;)Lcom/google/android/apps/sidekick/FileBytesReader;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/sidekick/FileBytesReader;->readFileBytes(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_0
.end method

.method private updateFile(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->mFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->access$800(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->mEncrypted:Z
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->access$900(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)Z

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->readFileBytes(Ljava/lang/String;Z)[B

    move-result-object v0

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->mCallback:Lcom/google/common/base/Function;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->access$1000(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)Lcom/google/common/base/Function;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->mFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->access$800(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->deleteFile(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->mFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->access$800(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->mEncrypted:Z
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;->access$900(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)Z

    move-result v4

    invoke-direct {v2, v3, v1, v4}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;-><init>(Ljava/lang/String;[BZ)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->writeFile(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)V

    goto :goto_0
.end method

.method private writeFile(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;

    const/high16 v3, 0x80000

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mEncrypted:Z
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$100(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->this$0:Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->access$400(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;)Lcom/google/android/apps/sidekick/FileBytesWriter;

    move-result-object v0

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$200(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mData:[B
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$300(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/FileBytesWriter;->writeEncryptedFileBytes(Ljava/lang/String;[BI)Z

    :goto_0
    return-void

    :cond_0
    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mData:[B
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$300(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)[B

    move-result-object v0

    array-length v0, v0

    if-gt v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->this$0:Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->access$400(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;)Lcom/google/android/apps/sidekick/FileBytesWriter;

    move-result-object v0

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$200(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mData:[B
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$300(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/FileBytesWriter;->writeFileBytes(Ljava/lang/String;[B)Z

    goto :goto_0

    :cond_1
    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Data is too large ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mData:[B
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$300(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes) to write to disk: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mFilename:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->access$200(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    # getter for: Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unknown message sent to AsyncFileStorageHandler"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->writeFile(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->readFile(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->deleteFile(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;->updateFile(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
