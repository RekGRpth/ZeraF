.class Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PurgeLocationsTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p2    # Lcom/google/android/apps/sidekick/LocationOracleImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->notIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    const-wide/16 v1, 0x0

    # setter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocationReceivedMillis:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2902(Lcom/google/android/apps/sidekick/LocationOracleImpl;J)J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocation:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3200(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithAndroidProviders()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithGmmProvider()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    goto :goto_0
.end method
