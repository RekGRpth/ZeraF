.class public Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;
.super Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;
.source "GenericPlaceActionAdapter.java"


# instance fields
.field private final mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;-><init>(Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;)Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    return-object v0
.end method


# virtual methods
.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 13
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super/range {p0 .. p3}, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v11}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v11

    invoke-interface {v11}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v8

    iget-object v11, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v11, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrDeleteAction(Landroid/app/Activity;)Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v11, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v11}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEditHomeOrWorkAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v5

    const/4 v0, 0x0

    if-eqz v3, :cond_3

    const v11, 0x7f0d00e5

    invoke-virtual {p1, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, p2, v10, v11}, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->addButton(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/widget/Button;

    move-result-object v4

    const-string v0, "RENAME"

    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    move-object v7, v3

    move-object v6, v0

    new-instance v11, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;

    invoke-direct {v11, p0, v7, v8, v6}, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;-><init>(Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;Lcom/google/android/apps/sidekick/actions/EntryAction;Lcom/google/android/searchcommon/google/UserInteractionLogger;Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v11, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v11, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getDeleteAction(Landroid/content/Context;)Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;

    move-result-object v1

    if-eqz v1, :cond_2

    const v11, 0x7f0d0212

    invoke-virtual {p1, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, p2, v10, v11}, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->addButton(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/widget/Button;

    move-result-object v2

    new-instance v11, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$2;

    invoke-direct {v11, p0, v1, v8}, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$2;-><init>(Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;Lcom/google/android/apps/sidekick/actions/EntryAction;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    invoke-virtual {v2, v11}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-object v10

    :cond_3
    if-eqz v5, :cond_0

    const v9, 0x7f0d01e9

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v11

    const/16 v12, 0x11

    if-ne v11, v12, :cond_5

    const v9, 0x7f0d02fd

    :cond_4
    :goto_1
    invoke-virtual {p1, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, p2, v10, v11}, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->addButton(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/widget/Button;

    move-result-object v4

    new-instance v3, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;

    iget-object v11, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v11}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v11

    const/4 v12, 0x1

    invoke-direct {v3, p1, v5, v11, v12}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;-><init>(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Z)V

    const-string v0, "EDIT_HOME_WORK"

    goto :goto_0

    :cond_5
    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v11

    const/16 v12, 0x12

    if-ne v11, v12, :cond_4

    const v9, 0x7f0d02fe

    goto :goto_1
.end method
