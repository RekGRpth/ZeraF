.class Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;
.super Ljava/lang/Object;
.source "WeatherActionAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-static {}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;->newInstance()Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "change_weather_units"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;)Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_BACK_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;)Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    move-result-object v3

    const-string v4, "CHANGE_WEATHER_UNITS"

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
