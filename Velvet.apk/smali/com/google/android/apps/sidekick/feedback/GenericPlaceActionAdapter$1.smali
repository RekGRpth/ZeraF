.class Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;
.super Ljava/lang/Object;
.source "GenericPlaceActionAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;

.field final synthetic val$finalActionType:Ljava/lang/String;

.field final synthetic val$finalEditAction:Lcom/google/android/apps/sidekick/actions/EntryAction;

.field final synthetic val$logger:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;Lcom/google/android/apps/sidekick/actions/EntryAction;Lcom/google/android/searchcommon/google/UserInteractionLogger;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->val$finalEditAction:Lcom/google/android/apps/sidekick/actions/EntryAction;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->val$logger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->val$finalActionType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->val$finalEditAction:Lcom/google/android/apps/sidekick/actions/EntryAction;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/actions/EntryAction;->run()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->val$logger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v1, "CARD_BACK_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;)Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter$1;->val$finalActionType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
