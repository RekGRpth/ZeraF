.class Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;
.super Ljava/lang/Object;
.source "VisualSearchListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

.field final synthetic val$visualSearchButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;->val$visualSearchButton:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;->val$visualSearchButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->startVisualSearch(Lcom/google/android/apps/sidekick/TgPresenterAccessor;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;I)V

    return-void
.end method
