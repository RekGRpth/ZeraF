.class Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2$1;
.super Ljava/lang/Object;
.source "CurrencyExchangeEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2$1;->this$1:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2$1;->this$1:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;

    iget-object v0, v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->showDisclaimer(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2$1;->this$1:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;

    iget-object v0, v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_MENU_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2$1;->this$1:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;

    iget-object v2, v2, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    const-string v3, "DISCLAIMER"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
