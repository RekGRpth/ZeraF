.class public final enum Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;
.super Ljava/lang/Enum;
.source "FlightStatusEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

.field public static final enum CANCELLED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

.field public static final enum IN_AIR:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

.field public static final enum LAST_LANDED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

.field public static final enum NOT_DEPARTED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->CANCELLED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    new-instance v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    const-string v1, "NOT_DEPARTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->NOT_DEPARTED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    new-instance v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    const-string v1, "IN_AIR"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->IN_AIR:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    new-instance v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    const-string v1, "LAST_LANDED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->LAST_LANDED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    sget-object v1, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->CANCELLED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->NOT_DEPARTED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->IN_AIR:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->LAST_LANDED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->$VALUES:[Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->$VALUES:[Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    invoke-virtual {v0}, [Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    return-object v0
.end method
