.class Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;
.super Ljava/lang/Object;
.source "BarcodeEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->addBoardingPassToCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$manageFlightUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;->val$manageFlightUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;->val$manageFlightUrl:Ljava/lang/String;

    const-string v4, "BOARDING_PASS_MANAGE_FLIGHT_URL"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
