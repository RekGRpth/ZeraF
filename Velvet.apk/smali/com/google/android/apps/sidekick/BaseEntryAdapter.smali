.class abstract Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.super Ljava/lang/Object;
.source "BaseEntryAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/EntryItemAdapter;


# instance fields
.field private final mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

.field private mDismissed:Z

.field private final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private final mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

.field private final mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mDismissed:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    return-void
.end method

.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mDismissed:Z

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/BaseEntryAdapter;)Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/BaseEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # Landroid/view/View;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->recordFeedback(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V

    return-void
.end method

.method protected static findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getDismissAction()Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const-string v2, "delete"

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getDisplayMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private recordFeedback(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v1, v0, p1, v2, p2}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/16 v1, 0x8

    invoke-virtual {p3, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method protected addFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public examplify()V
    .locals 0

    return-void
.end method

.method public findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const v5, 0x7f100016

    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-eqz v3, :cond_1

    new-instance v1, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v1, p2}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    const v5, 0x7f100015

    invoke-virtual {v2, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    if-eqz v0, :cond_0

    new-instance v5, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v5, v0}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v1, v5}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v0
.end method

.method public final getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method public final getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasType()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/GenericLowPriorityNotification;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const v2, 0x7f030001

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/notifications/GenericLowPriorityNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;I)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    return-object v0
.end method

.method public getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Adapter"

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/Util;->removeTrailingSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Entry"

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/Util;->removeTrailingSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->getGenericEntryType(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    return-object v0
.end method

.method getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    return-object v0
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    return-object v0
.end method

.method public isDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mDismissed:Z

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    return-void
.end method

.method logDetailsInteraction(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->logDetailsInteractionWithLabel(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method logDetailsInteractionWithLabel(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_DETAILS"

    invoke-virtual {v0, v1, p0, p2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method

.method public maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 7
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasUserPrompt()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v5, 0x19

    invoke-virtual {p0, v5}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    const/16 v5, 0x1a

    invoke-virtual {p0, v5}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    const v5, 0x7f040016

    const/4 v6, 0x0

    invoke-virtual {p2, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f100056

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->getUserPrompt()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f100057

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Action;->getDisplayMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;

    invoke-direct {v5, p0, p1, v3, v2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f100058

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getDisplayMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/google/android/apps/sidekick/BaseEntryAdapter$4;

    invoke-direct {v5, p0, p1, v0, v2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->addFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_0
.end method

.method protected openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;

    const v5, 0x7f0d0370

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->openUrlWithMessage(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;I)V

    return-void
.end method

.method protected openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method protected openUrlWithMessage(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;
    .param p5    # I

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-interface {v2, p1, p3, v4, p5}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeViewUriWithMessage(Landroid/content/Context;Landroid/net/Uri;ZI)Z

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v2

    const-string v3, "CARD_BUTTON_PRESS"

    invoke-virtual {v2, v3, p0, p4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    const/16 v2, 0x14

    invoke-static {p2, v2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    invoke-direct {v2, v1, p1, p2, v0}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getDismissAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p2, v1, v0}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->addEntry(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    :cond_0
    return-void
.end method

.method public registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    return-void
.end method

.method public final registerBackOfCardMenuListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    const v1, 0x7f10005c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/BaseEntryAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setDismissed(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mDismissed:Z

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected startVisualSearch(Lcom/google/android/apps/sidekick/TgPresenterAccessor;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;I)V
    .locals 6
    .param p1    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # I

    invoke-interface {p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->startVisualSearch(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v3

    const-string v4, "CARD_BUTTON_PRESS"

    const-string v5, "VISUAL_SEARCH"

    invoke-virtual {v3, v4, p0, v5}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/goggles/TraceTracker;->newSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v3

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventStartSearch(I)V

    if-eqz p3, :cond_0

    const/16 v3, 0x16

    invoke-static {p3, v3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    invoke-direct {v3, v1, p2, p3, v0}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
