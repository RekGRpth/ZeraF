.class Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter$1;
.super Ljava/lang/Object;
.source "SwipeTutorialCardAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;

    # getter for: Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;->access$000(Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;)Lcom/google/android/velvet/presenter/CardDismissalHandler;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;->SWIPE_TUTORIAL:Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->recordDismissedFirstUseCard(Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;)V

    return-void
.end method
