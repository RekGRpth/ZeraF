.class Lcom/google/android/apps/sidekick/LocationOracleImpl$2;
.super Landroid/os/HandlerThread;
.source "LocationOracleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;->startLocked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/lang/String;I)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0, p2, p3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected onLooperPrepared()V
    .locals 4

    invoke-super {p0}, Landroid/os/HandlerThread;->onLooperPrepared()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1000(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    # setter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;
    invoke-static {v0, v2}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1102(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/os/Handler;)Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    new-instance v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/os/Looper;)V

    # setter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGmmLocationHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1202(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/os/Handler;)Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->startInternal()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1300(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
