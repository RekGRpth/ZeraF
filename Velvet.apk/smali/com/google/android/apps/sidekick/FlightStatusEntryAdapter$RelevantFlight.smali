.class public Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;
.super Ljava/lang/Object;
.source "FlightStatusEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RelevantFlight"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;
    }
.end annotation


# instance fields
.field private final mFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

.field private final mStatus:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .param p2    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->mFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->mStatus:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    return-void
.end method


# virtual methods
.method public getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->mFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    return-object v0
.end method

.method public getStatus()Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->mStatus:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    return-object v0
.end method
