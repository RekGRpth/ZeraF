.class Lcom/google/android/apps/sidekick/LocationOracleImpl$1;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;->newRunningLock(Ljava/lang/String;)Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

.field final synthetic val$tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->val$tag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public acquire()V
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->val$tag:Ljava/lang/String;

    invoke-virtual {v0, p0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->startLocked()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    :cond_0
    monitor-exit v2

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->release()V

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$900()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forgot to release lock from: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->val$tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->stop()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$800(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
