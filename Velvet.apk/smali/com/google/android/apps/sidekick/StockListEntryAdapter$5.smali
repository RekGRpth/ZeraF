.class Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;
.super Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;
.source "StockListEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-void
.end method


# virtual methods
.method public getNoRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    new-instance v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$2;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;Landroid/content/Context;I)V

    return-object v0
.end method

.method public getQuestionCount(Landroid/view/View;)I
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f100253

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v1

    goto :goto_0
.end method

.method public getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/StockListEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntry(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const v0, 0x7f0d02e9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    new-instance v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;Landroid/content/Context;I)V

    return-object v0
.end method
