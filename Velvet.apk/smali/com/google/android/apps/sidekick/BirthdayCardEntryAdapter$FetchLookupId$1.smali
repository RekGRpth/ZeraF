.class Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;
.super Ljava/lang/Object;
.source "BirthdayCardEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->onPostExecute(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;

.field final synthetic val$lookupId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;->this$1:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;->val$lookupId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;->val$lookupId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;->this$1:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;

    # getter for: Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->access$100(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {v1, p1, v0, v2, v3}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;->this$1:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;

    iget-object v1, v1, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;->this$1:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;

    iget-object v3, v3, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    const-string v4, "VIEW_CONTACT"

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
