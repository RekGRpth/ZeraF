.class Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;
.super Landroid/os/AsyncTask;
.source "RemindersListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchRemindersTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$Entry;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->this$0:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;Lcom/google/android/apps/sidekick/RemindersListFragment$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;
    .param p2    # Lcom/google/android/apps/sidekick/RemindersListFragment$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;-><init>(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 15
    .param p1    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    iget-object v13, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->this$0:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    invoke-virtual {v13}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v13

    invoke-interface {v13}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v5

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    new-instance v13, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v13}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    const/4 v14, 0x6

    invoke-virtual {v13, v14}, Lcom/google/geo/sidekick/Sidekick$Interest;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v13

    const/16 v14, 0x2b

    invoke-virtual {v13, v14}, Lcom/google/geo/sidekick/Sidekick$Interest;->addEntryTypeRestrict(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v9

    new-instance v13, Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    invoke-direct {v13}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;-><init>()V

    invoke-virtual {v13, v9}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->addInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v10

    new-instance v13, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v13}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v13, v10}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v7

    invoke-interface {v5, v7}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeList()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildList()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v8
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->this$0:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    # setter for: Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mReminders:Ljava/util/List;
    invoke-static {v0, p1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->access$302(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->this$0:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    # invokes: Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->updateTargetReminders()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->access$000(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->this$0:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mFetchRemindersTask:Landroid/os/AsyncTask;
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->access$402(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    return-void
.end method
