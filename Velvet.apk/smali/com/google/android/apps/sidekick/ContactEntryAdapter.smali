.class Lcom/google/android/apps/sidekick/ContactEntryAdapter;
.super Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;
.source "ContactEntryAdapter.java"


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;
    .param p6    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p7    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ContactEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ContactEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ContactEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasContactData()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "entry was expected to have hasContactData()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldDisplay()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
