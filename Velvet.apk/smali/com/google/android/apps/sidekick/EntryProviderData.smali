.class public final Lcom/google/android/apps/sidekick/EntryProviderData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EntryProviderData.java"


# instance fields
.field private cachedSize:I

.field private entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

.field private hasEntryResponse:Z

.field private hasIncludesMoreCards:Z

.field private hasLastRefreshMillis:Z

.field private hasLocale:Z

.field private hasLocation:Z

.field private includesMoreCards_:Z

.field private lastRefreshMillis_:J

.field private locale_:Ljava/lang/String;

.field private location_:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->lastRefreshMillis_:J

    iput-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->includesMoreCards_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->locale_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->cachedSize:I

    return v0
.end method

.method public getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    return-object v0
.end method

.method public getIncludesMoreCards()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->includesMoreCards_:Z

    return v0
.end method

.method public getLastRefreshMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->lastRefreshMillis_:J

    return-wide v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->locale_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLastRefreshMillis()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLastRefreshMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocation()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasEntryResponse()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasIncludesMoreCards()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getIncludesMoreCards()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocale()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->cachedSize:I

    return v0
.end method

.method public hasEntryResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasEntryResponse:Z

    return v0
.end method

.method public hasIncludesMoreCards()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasIncludesMoreCards:Z

    return v0
.end method

.method public hasLastRefreshMillis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLastRefreshMillis:Z

    return v0
.end method

.method public hasLocale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocale:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocation:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/EntryProviderData;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/EntryProviderData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->setLastRefreshMillis(J)Lcom/google/android/apps/sidekick/EntryProviderData;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/EntryProviderData;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryProviderData;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/EntryProviderData;->setEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Lcom/google/android/apps/sidekick/EntryProviderData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/EntryProviderData;->setIncludesMoreCards(Z)Lcom/google/android/apps/sidekick/EntryProviderData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/EntryProviderData;->setLocale(Ljava/lang/String;)Lcom/google/android/apps/sidekick/EntryProviderData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/EntryProviderData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/EntryProviderData;

    move-result-object v0

    return-object v0
.end method

.method public setEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Lcom/google/android/apps/sidekick/EntryProviderData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasEntryResponse:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    return-object p0
.end method

.method public setIncludesMoreCards(Z)Lcom/google/android/apps/sidekick/EntryProviderData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasIncludesMoreCards:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->includesMoreCards_:Z

    return-object p0
.end method

.method public setLastRefreshMillis(J)Lcom/google/android/apps/sidekick/EntryProviderData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLastRefreshMillis:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->lastRefreshMillis_:J

    return-object p0
.end method

.method public setLocale(Ljava/lang/String;)Lcom/google/android/apps/sidekick/EntryProviderData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocale:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->locale_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryProviderData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocation:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderData;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLastRefreshMillis()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLastRefreshMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasEntryResponse()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasIncludesMoreCards()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getIncludesMoreCards()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocale()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
