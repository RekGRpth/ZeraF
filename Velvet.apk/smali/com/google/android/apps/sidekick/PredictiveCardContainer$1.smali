.class Lcom/google/android/apps/sidekick/PredictiveCardContainer$1;
.super Ljava/lang/Object;
.source "PredictiveCardContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/PredictiveCardContainer;->createMenuButtonOverlay(Lcom/google/android/velvet/presenter/TgPresenter;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

.field final synthetic val$presenter:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Lcom/google/android/velvet/presenter/TgPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$1;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$1;->val$presenter:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$1;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    const v2, 0x7f100011

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$1;->val$presenter:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->toggleBackOfCard(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_INFO_BUTTON_PRESS"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapter(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    return-void
.end method
