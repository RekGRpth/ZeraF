.class Lcom/google/android/apps/sidekick/LocationOracleImpl$5;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;->blockingUpdateBestLocation()Landroid/location/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/location/Location;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/location/Location;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->getNetworkLocation()Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/location/Location;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2200(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    move-result-object v2

    const-string v3, "gps"

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2300(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;Landroid/location/Location;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;->call()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
