.class public Lcom/google/android/apps/sidekick/StaticMapView;
.super Landroid/widget/ImageView;
.source "StaticMapView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/StaticMapView$1;,
        Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;,
        Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;
    }
.end annotation


# instance fields
.field private mMapHeight:I

.field private mMapWidth:I

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/StaticMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/StaticMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/util/LayoutUtils;->getCardWidth(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/sidekick/StaticMapView;->mMapWidth:I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/sidekick/StaticMapView;->mMapHeight:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/StaticMapView;->setBackgroundColor(I)V

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/StaticMapView;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/StaticMapView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/StaticMapView;

    iget v0, p0, Lcom/google/android/apps/sidekick/StaticMapView;->mMapWidth:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/StaticMapView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/StaticMapView;

    iget v0, p0, Lcom/google/android/apps/sidekick/StaticMapView;->mMapHeight:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/StaticMapView;)Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/StaticMapView;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StaticMapView;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/StaticMapView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/StaticMapView;
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/StaticMapView;->setImageBitmapAndFadeIn(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private setImageBitmapAndFadeIn(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/StaticMapView;->setAlpha(F)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/StaticMapView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/StaticMapView;->setBackgroundColor(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticMapView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method


# virtual methods
.method public setLocations(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLcom/google/android/apps/sidekick/inject/StaticMapCache;)V
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .param p3    # Z
    .param p4    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    const/4 v7, 0x0

    invoke-interface {p4, p1, p2, p3}, Lcom/google/android/apps/sidekick/inject/StaticMapCache;->get(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p0, v6}, Lcom/google/android/apps/sidekick/StaticMapView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v7}, Lcom/google/android/apps/sidekick/StaticMapView;->setBackgroundColor(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;-><init>(Lcom/google/android/apps/sidekick/StaticMapView;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLcom/google/android/apps/sidekick/inject/StaticMapCache;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public showSampleRoute()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;-><init>(Lcom/google/android/apps/sidekick/StaticMapView;Lcom/google/android/apps/sidekick/StaticMapView$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
