.class Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;
.super Ljava/lang/Object;
.source "ResearchTopicEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field final synthetic val$linkUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->val$linkUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;->val$linkUri:Landroid/net/Uri;

    const-string v4, "VIEW_RESEARCH_PAGE"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method
