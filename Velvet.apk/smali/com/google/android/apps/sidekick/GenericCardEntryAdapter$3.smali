.class Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;
.super Ljava/lang/Object;
.source "GenericCardEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getLatitudeOptInOnClick(Landroid/content/Context;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    const-string v3, "LatitudeOptIn"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->access$300(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->optIntoLatitude()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->dismissEntry(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    return-void
.end method
