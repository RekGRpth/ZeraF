.class Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;
.super Ljava/lang/Object;
.source "LocationHistoryReminderEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;

.field final synthetic val$actedKey:Ljava/lang/String;

.field final synthetic val$enabledKey:Ljava/lang/String;

.field final synthetic val$nowPrefs:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/view/View;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$view:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$nowPrefs:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$actedKey:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$enabledKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$view:Landroid/view/View;

    # invokes: Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->isPromptBubbleHidden(Landroid/view/View;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$nowPrefs:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$actedKey:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$enabledKey:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;->val$view:Landroid/view/View;

    # invokes: Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->hidePromptBubble(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/view/View;)V

    goto :goto_0
.end method
