.class public Lcom/google/android/apps/sidekick/SportsEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "SportsEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;
    }
.end annotation


# static fields
.field private static final SUPPORTED_SPORTS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->TAG:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->SUPPORTED_SPORTS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getSportScoreEntry()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/SportsEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/SportsEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/SportsEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->updateTeamInSettings(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;Z)V

    return-void
.end method

.method private varargs addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/cards/SportsMatchCard;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # [Ljava/lang/Object;

    invoke-virtual {p1, p3, p6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;

    invoke-direct {v1, p0, p1, p4, p5}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->addAction(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addActionButtons(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/cards/SportsMatchCard;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasPreviewUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    const v3, 0x7f0d014e

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPreviewUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PREVIEW"

    new-array v6, v7, [Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasTicketsUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const v3, 0x7f0d014f

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getTicketsUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "TICKETS"

    new-array v6, v7, [Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveUpdateUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    const v3, 0x7f0d0150

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLiveUpdateUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PLAY_BY_PLAY"

    new-array v6, v7, [Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveStreamUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    const v3, 0x7f0d0151

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLiveStreamUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STREAM"

    new-array v6, v7, [Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasRecapUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    const v3, 0x7f0d014c

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getRecapUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "RECAP"

    new-array v6, v7, [Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasBoxScoresUrl()Z

    move-result v0

    if-eqz v0, :cond_5

    const v3, 0x7f0d014d

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getBoxScoresUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BOX_SCORE"

    new-array v6, v7, [Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    return-void
.end method

.method private anyScoresExist(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;)Z
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScoreList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getMatchDetails()Lcom/google/android/velvet/cards/MatchDetailsView$Builder;
    .locals 14

    const/4 v13, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntityCount()I

    move-result v9

    if-ne v9, v13, :cond_0

    move v9, v10

    :goto_0
    invoke-static {v9}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9, v11}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v8

    const/4 v0, 0x0

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriodCount()I

    move-result v4

    new-array v6, v4, [I

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9, v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriod(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasNumber()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9, v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriod(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getNumber()I

    move-result v9

    :goto_2
    aput v9, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move v9, v11

    goto :goto_0

    :cond_1
    add-int/lit8 v9, v3, 0x1

    goto :goto_2

    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v9

    if-ne v9, v13, :cond_5

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasBaseballScore()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasBaseballScore()Z

    move-result v9

    if-eqz v9, :cond_5

    const/4 v0, 0x1

    const/4 v9, 0x3

    new-array v2, v9, [I

    fill-array-data v2, :array_0

    new-instance v1, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;

    invoke-direct {v1, v6, v2}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;-><init>([I[I)V

    :goto_3
    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->firstContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->secondContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v4, :cond_6

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v9, v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriod(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScoreCount()I

    move-result v9

    if-lez v9, :cond_3

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->firstContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v5, v11}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScore(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v3, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setPeriodScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    :cond_3
    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScoreCount()I

    move-result v9

    if-le v9, v10, :cond_4

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->secondContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v5, v10}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getScore(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v3, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setPeriodScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    new-instance v1, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;

    invoke-direct {v1, v6}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;-><init>([I)V

    goto :goto_3

    :cond_6
    if-eqz v0, :cond_7

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->firstContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;->getRuns()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v11, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;->getHits()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v10, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;->getErrors()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v13, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->secondContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;->getRuns()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v11, v12}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;->getHits()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;->getErrors()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v13, v10}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    :goto_5
    return-object v1

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->firstContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->secondContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    goto :goto_5

    nop

    :array_0
    .array-data 4
        0x7f0d0147
        0x7f0d0148
        0x7f0d0149
    .end array-data
.end method

.method private getPeriodString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const v1, 0x7f0d014b

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0d014a

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f0d0146

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    packed-switch p2, :pswitch_data_1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0d0159

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0d015a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0d015b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0d015c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    packed-switch p2, :pswitch_data_2

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0d015d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0d015e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0d015f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_a
    packed-switch p2, :pswitch_data_3

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_b
    const v0, 0x7f0d0160

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_c
    const v0, 0x7f0d0161

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method private isGameToday()Z
    .locals 12

    const/4 v11, 0x6

    iget-object v7, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeSeconds()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long v5, v7, v9

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v2, v11}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v0, v4}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {v0, v11}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    if-ne v3, v1, :cond_0

    const/4 v7, 0x1

    :goto_0
    return v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private periodIsInProgressOrDone(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;)Z
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getTimeSeconds()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->anyScoresExist(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateContestant(Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;Z)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .param p3    # Z

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-static {v1, p2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->fromSportEntry(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;)Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->access$000(Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;)Landroid/net/Uri;

    move-result-object v1

    # getter for: Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mClipRect:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->access$100(Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setLogo(Landroid/net/Uri;Landroid/graphics/Rect;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setScore(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    :cond_1
    return-void
.end method

.method private updateTeamInSettings(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .param p3    # Z

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasPrimaryKey()Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Team to update does not have primary key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    const v5, 0x7f0d0082

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getPrimaryKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    if-eqz v4, :cond_1

    invoke-virtual {v4, p3}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->setHide(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->editConfiguration()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v5

    invoke-virtual {v5, v0, v4}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->updateMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->apply()V

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Team to update was not found in settings: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method detailsWebQuery(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p1    # Landroid/content/Context;

    const/16 v7, 0x20

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getWebSearchQuery()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getWebSearchQuery()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_1
    if-eqz v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3, v5}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsUserInterest()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3, v6}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsUserInterest()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4, v6}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    const v3, 0x7f0d01f7

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_1
    const v3, 0x7f0d01f8

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_2
    const v3, 0x7f0d01f9

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_3
    const v3, 0x7f0d01fa

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_4
    const v3, 0x7f0d01fb

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 6

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsUserInterest()Z

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsUserInterest()Z

    move-result v3

    new-instance v1, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-direct {v1, p0, v4, v2, v3}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;ZZ)V

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v4

    const v5, 0x7f0d02fc

    invoke-direct {v0, p0, v4, v5}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/inject/ActivityHelper;I)V

    new-instance v4, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v4, p0, v1, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V

    return-object v4
.end method

.method getCurrentPeriodIndex()I
    .locals 4

    const/4 v0, -0x1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriodList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->periodIsInProgressOrDone(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d016c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d016d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getFormattedStartTime(Landroid/content/Context;)Ljava/lang/String;
    .locals 12
    .param p1    # Landroid/content/Context;

    const/4 v11, 0x0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeSeconds()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long v2, v7, v9

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->isGameToday()Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "z"

    invoke-direct {v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    const v7, 0x7f0d0141

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v11

    const/4 v9, 0x1

    aput-object v5, v8, v9

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    :goto_0
    return-object v7

    :cond_0
    invoke-static {p1, v2, v3, v11}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSource()I

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0d02d2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d02d1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/notifications/SportsNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-object v0
.end method

.method public getStatus(Landroid/content/Context;I)Ljava/lang/String;
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getFormattedStartTime(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    :cond_1
    :goto_0
    return-object v5

    :pswitch_0
    iget-object v8, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeSeconds()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long v6, v8, v10

    const/4 v8, 0x0

    invoke-static {p1, v6, v7, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d0143

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v3, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getCurrentPeriodIndex()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriodCount()I

    move-result v8

    if-ge v0, v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v8, v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriod(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasNumber()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getNumber()I

    move-result v8

    :goto_1
    invoke-direct {p0, p1, v8}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getPeriodString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->hasTimeSeconds()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;->getTimeSeconds()I

    move-result v1

    int-to-long v8, v1

    invoke-static {v8, v9}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    const v8, 0x7f0d0144

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    const/4 v10, 0x1

    aput-object v2, v9, v10

    invoke-virtual {p1, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_2
    add-int/lit8 v8, v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v7, 0x1

    new-instance v0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;

    invoke-direct {v0}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatusCode()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->shouldShowScore(I)Z

    move-result v1

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getStatus(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->setStatus(Ljava/lang/CharSequence;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntityCount()I

    move-result v4

    if-lez v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->leftContestant()Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v5

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->updateContestant(Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;Z)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntityCount()I

    move-result v4

    if-le v4, v7, :cond_1

    invoke-virtual {v0}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->rightContestant()Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v5, v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v5

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->updateContestant(Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;Z)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatusCode()I

    move-result v4

    if-ne v4, v7, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntityCount()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriodCount()I

    move-result v4

    if-lez v4, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getMatchDetails()Lcom/google/android/velvet/cards/MatchDetailsView$Builder;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->setMatchDetailsBuilder(Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;

    :cond_2
    invoke-virtual {v0, p1}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->build(Landroid/content/Context;)Lcom/google/android/velvet/cards/SportsMatchCard;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButtons(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;)V

    return-object v3
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f100240

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->detailsWebQuery(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v3}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->startWebSearch(Landroid/content/Context;Ljava/lang/String;Landroid/location/Location;)Z

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->logDetailsInteraction(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntityCount()I

    move-result v3

    if-eq v3, v5, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v3, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->SUPPORTED_SPORTS:Ljava/util/Set;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatusCode()I

    move-result v0

    if-eq v0, v5, :cond_2

    if-eq v0, v2, :cond_2

    if-nez v0, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public shouldShowScore(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
