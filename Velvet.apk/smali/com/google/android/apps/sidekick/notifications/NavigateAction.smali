.class public Lcom/google/android/apps/sidekick/notifications/NavigateAction;
.super Ljava/lang/Object;
.source "NavigateAction.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/notifications/NotificationAction;


# instance fields
.field private final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field private final mRoute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mRoute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-void
.end method


# virtual methods
.method public getActionIcon()I
    .locals 1

    const v0, 0x7f020067

    return v0
.end method

.method public getActionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d00db

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCallbackIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v4, :cond_2

    const/4 v1, 0x0

    const/4 v2, -0x1

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mRoute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mRoute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointCount()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mRoute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointList()Ljava/util/List;

    move-result-object v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mRoute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v2

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-interface {v3, v4, v5, v1, v2}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->getDrivingLauncherIntent(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/util/List;I)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "callback_type"

    const-string v4, "activity"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    return-object v0

    :cond_1
    move-object v1, v3

    goto :goto_0

    :cond_2
    move-object v0, v3

    goto :goto_1
.end method

.method public getCallbackType()Ljava/lang/String;
    .locals 1

    const-string v0, "activity"

    return-object v0
.end method

.method public getLogString()Ljava/lang/String;
    .locals 1

    const-string v0, "NAVIGATE"

    return-object v0
.end method

.method public isActive()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;->mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->checkNavigationAvailability(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
