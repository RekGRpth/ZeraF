.class public interface abstract Lcom/google/android/apps/sidekick/notifications/NotificationAction;
.super Ljava/lang/Object;
.source "NotificationAction.java"


# virtual methods
.method public abstract getActionIcon()I
.end method

.method public abstract getActionString(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getCallbackIntent(Landroid/content/Context;)Landroid/content/Intent;
.end method

.method public abstract getCallbackType()Ljava/lang/String;
.end method

.method public abstract getLogString()Ljava/lang/String;
.end method

.method public abstract isActive()Z
.end method
