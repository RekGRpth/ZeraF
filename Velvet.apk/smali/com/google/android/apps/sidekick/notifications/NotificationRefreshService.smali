.class public Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;
.super Landroid/app/IntentService;
.source "NotificationRefreshService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$1;,
        Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final VALID_ACTIONS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;


# instance fields
.field private mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

.field private mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

.field private mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field private mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

.field private mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

.field private mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const-class v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    const-string v0, "com.google.android.apps.sidekick.notifications.REFRESH"

    const-string v1, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    const-string v2, "com.google.android.apps.sidekick.notifications.SHOW_NOTIFICATIONS"

    const-string v3, "com.google.android.apps.sidekick.notifications.EXPIRE_NOTIFICATIONS"

    const-string v4, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DISMISS_ACTION"

    const-string v5, "com.google.android.apps.sidekick.notifications.NOTIFICATION_SNOOZE_ACTION"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DELETE_ACTION"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "com.google.android.apps.sidekick.notifications.NOTIFICATION_TRIGGER_ACTION"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "com.google.android.apps.sidekick.notifications.INITIALIZE"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "com.google.android.apps.sidekick.notifications.SHUTDOWN"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->VALID_ACTIONS:Ljava/util/Set;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;-><init>(Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$1;)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private deleteNotification(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    const-string v4, "notification_entries"

    invoke-static {p1, v4}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getStoredEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->deleteNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->expireNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextExpirationAlarm()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextUserNotifyAlarm()V

    :cond_2
    return-void
.end method

.method private dismissNotification(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const-string v2, "notification_entries"

    invoke-static {p1, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->markEntryNotificationDismissed(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private expireNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    invoke-interface {v2, p1, v3, v3}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    const-string v3, "unable to find the notification!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    const-string v3, "unable to find the adapter!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private expireNotifications()V
    .locals 8

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getExpiredNotifications()Ljava/util/Collection;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getUnexpiredNotifications()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getNotificationTypes(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getNotificationTypes(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/google/common/collect/Sets;->difference(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v7, v4}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextExpirationAlarm()V

    return-void
.end method

.method public static getDeleteNotificationIntent(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DELETE_ACTION"

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "notification_entries"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getExpirationPendingIntent()Landroid/app/PendingIntent;
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.EXPIRE_NOTIFICATIONS"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getNotificationDismissIntent(Ljava/util/Collection;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)Landroid/app/PendingIntent;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;",
            ")",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "notification_refresh_dismiss"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->getNotificationId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DISMISS_ACTION"

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v2, v3, v0, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "notification_entries"

    invoke-static {v2, v0, p1}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v3, 0x10000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static getNotificationSnoozeIntent(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.NOTIFICATION_SNOOZE_ACTION"

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "notification_entries"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNotificationTypes(Ljava/util/Collection;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    invoke-interface {v5, v1, v6, v6}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method private getNotifyPendingIntent()Landroid/app/PendingIntent;
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.SHOW_NOTIFICATIONS"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getRefreshPendingIntent()Landroid/app/PendingIntent;
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.REFRESH"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static getTriggerIntent(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.NOTIFICATION_TRIGGER_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "triggered_notification_entries"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "concluded_notification_entries"

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    :cond_1
    return-object v0
.end method

.method private presentPendingNotifications()V
    .locals 19

    new-instance v5, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    :goto_0
    invoke-static {}, Lcom/google/common/collect/LinkedListMultimap;->create()Lcom/google/common/collect/LinkedListMultimap;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getEntriesToNotify()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->markEntryNotified(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v15

    const/16 v17, 0x2b

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v16

    if-eqz v16, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->size()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    new-instance v8, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;

    move-object/from16 v0, v16

    invoke-direct {v8, v0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;-><init>(Ljava/util/Collection;)V

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v15, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v17, 0x2b

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Lcom/google/common/collect/Multimap;->removeAll(Ljava/lang/Object;)Ljava/util/Collection;

    :cond_2
    invoke-interface {v6}, Lcom/google/common/collect/Multimap;->values()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/geo/sidekick/Sidekick$Entry;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v7, v4, v4}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v14

    invoke-interface {v15, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    invoke-interface {v15, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    invoke-interface {v15}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v17, v0

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v9}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getNotificationDismissIntent(Ljava/util/Collection;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)Landroid/app/PendingIntent;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v8, v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->createNotification(Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v13

    if-nez v13, :cond_5

    sget-object v17, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    const-string v18, "createNotification surprisingly returned null"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v13, v9}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->showNotification(Landroid/app/Notification;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/geo/sidekick/Sidekick$Entry;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->sendDeliverActiveNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->setLastNotificationTime()V

    goto :goto_3

    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextExpirationAlarm()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextUserNotifyAlarm()V

    return-void
.end method

.method private requestNotifications()V
    .locals 26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getPendingRefreshInterests()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_0

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v20

    if-eqz v20, :cond_1

    :cond_0
    sget-object v20, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    const-string v21, "Skipping notification refresh, no interests to query."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    new-instance v7, Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    invoke-direct {v7}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;-><init>()V

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {v7, v12}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->addInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    goto :goto_1

    :cond_2
    new-instance v20, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct/range {v20 .. v20}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v19

    if-eqz v19, :cond_3

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v20

    if-nez v20, :cond_4

    :cond_3
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v21

    sget-object v23, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->getRetryAlarmOffsetMillis()J

    move-result-wide v23

    add-long v21, v21, v23

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setCheckNotificationAlarm(IJ)V

    sget-object v20, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    const-string v21, "Failed to get response for notification query from server"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    sget-object v20, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->clearBackoff()V

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v20

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_6

    sget-object v20, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "got back "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " entry trees for "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " interests"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setCheckNotificationAlarm()V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v20

    if-eqz v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->remindersEnabled(Lcom/google/android/searchcommon/SearchConfig;)Z

    move-result v17

    const/4 v10, 0x0

    :goto_3
    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v20

    move/from16 v0, v20

    if-ge v10, v0, :cond_b

    invoke-interface {v13, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {v8, v10}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v9

    if-nez v17, :cond_9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v20

    if-eqz v20, :cond_9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v20

    if-lez v20, :cond_9

    const/4 v14, 0x0

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v20

    const/16 v21, 0x2b

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7

    const/4 v14, 0x1

    :cond_8
    if-eqz v14, :cond_9

    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_9
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasExpirationTimestampSeconds()Z

    move-result v20

    if-eqz v20, :cond_a

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getExpirationTimestampSeconds()J

    move-result-wide v20

    const-wide/32 v22, 0x493e0

    add-long v22, v22, v15

    const-wide/16 v24, 0x3e8

    div-long v22, v22, v24

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v4, v5, v12, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->updateNextRefreshTime(JLcom/google/geo/sidekick/Sidekick$Interest;Z)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v12}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->updatePendingNotifications(Lcom/google/geo/sidekick/Sidekick$EntryTree;Lcom/google/geo/sidekick/Sidekick$Interest;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x3e8

    div-long v21, v15, v21

    move-object/from16 v0, v20

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2, v12}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->recordRefresh(JLcom/google/geo/sidekick/Sidekick$Interest;)V

    goto :goto_4

    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextUserNotifyAlarm()V

    goto/16 :goto_2
.end method

.method private setCheckNotificationAlarm()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getNextRefreshTimeMillis()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setCheckNotificationAlarm(IJ)V

    :cond_0
    return-void
.end method

.method private setCheckNotificationAlarm(IJ)V
    .locals 2
    .param p1    # I
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getRefreshPendingIntent()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private setNextExpirationAlarm()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getNextExpirationTimeMillis()Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getExpirationPendingIntent()Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v2, v0}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5, v0}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private setNextUserNotifyAlarm()V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getNotifyPendingIntent()Landroid/app/PendingIntent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getNextNotificationTimeMillis()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v2, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private snoozeNotification(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v6, "notification_entries"

    invoke-static {p1, v6}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->getStoredEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v6, v5}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->snooze(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->expireNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v6

    const/16 v7, 0x1e

    if-ne v6, v7, :cond_1

    new-instance v6, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v9

    invoke-direct {v6, v7, v8, v5, v9}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;)V

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextExpirationAlarm()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextUserNotifyAlarm()V

    :cond_4
    return-void
.end method

.method private updateNextRefreshTime(Lcom/google/geo/sidekick/Sidekick$EntryTree;)V
    .locals 7
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getCallbackWithInterestList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;->getCallbackTimeSeconds()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->updateNextRefreshTime(JLcom/google/geo/sidekick/Sidekick$Interest;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateNotificationTriggers(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const-string v4, "triggered_notification_entries"

    invoke-static {p1, v4}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v3

    const-string v4, "concluded_notification_entries"

    invoke-static {p1, v4}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->expireNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->resetNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->presentPendingNotifications()V

    :cond_1
    return-void
.end method


# virtual methods
.method public cancelAlarms()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getNotifyPendingIntent()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getRefreshPendingIntent()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getExpirationPendingIntent()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNotificationStore()Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getAlarmManager()Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->isTimeToLeaveNotificationsEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v5}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isUserOptedIn()Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->VALID_ACTIONS:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "power"

    invoke-virtual {p0, v5}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    const/4 v5, 0x1

    const-string v6, "NotificationRefresh_wakelock"

    invoke-virtual {v3, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isInitialized()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->initialize()V

    :cond_2
    const-string v5, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    new-instance v5, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-direct {v5}, Lcom/google/geo/sidekick/Sidekick$EntryTree;-><init>()V

    invoke-static {v5, v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    if-eqz v2, :cond_3

    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->updateNextRefreshTime(Lcom/google/geo/sidekick/Sidekick$EntryTree;)V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setCheckNotificationAlarm()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :goto_1
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_5
    :try_start_1
    const-string v5, "com.google.android.apps.sidekick.notifications.REFRESH"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->requestNotifications()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v5

    :cond_6
    :try_start_2
    const-string v5, "com.google.android.apps.sidekick.notifications.SHOW_NOTIFICATIONS"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->presentPendingNotifications()V

    goto :goto_1

    :cond_7
    const-string v5, "com.google.android.apps.sidekick.notifications.INITIALIZE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->clearNotifiedMarkers()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->presentPendingNotifications()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setCheckNotificationAlarm()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextUserNotifyAlarm()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->setNextExpirationAlarm()V

    goto :goto_1

    :cond_8
    const-string v5, "com.google.android.apps.sidekick.notifications.EXPIRE_NOTIFICATIONS"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->expireNotifications()V

    goto :goto_1

    :cond_9
    const-string v5, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DISMISS_ACTION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->dismissNotification(Landroid/content/Intent;)V

    goto :goto_1

    :cond_a
    const-string v5, "com.google.android.apps.sidekick.notifications.NOTIFICATION_SNOOZE_ACTION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->snoozeNotification(Landroid/content/Intent;)V

    goto :goto_1

    :cond_b
    const-string v5, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DELETE_ACTION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->deleteNotification(Landroid/content/Intent;)V

    goto :goto_1

    :cond_c
    const-string v5, "com.google.android.apps.sidekick.notifications.NOTIFICATION_TRIGGER_ACTION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->updateNotificationTriggers(Landroid/content/Intent;)V

    goto :goto_1

    :cond_d
    const-string v5, "com.google.android.apps.sidekick.notifications.SHUTDOWN"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->cancelAlarms()V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->clearAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method
