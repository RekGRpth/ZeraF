.class public Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;
.source "MultiReminderNotification.java"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;-><init>(Ljava/util/Collection;)V

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/SnoozeAction;

    invoke-direct {v0, p1}, Lcom/google/android/apps/sidekick/notifications/SnoozeAction;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->addAction(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)V

    return-void
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const-string v0, ""

    return-object v0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11000d

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->getEntries()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->getEntries()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->isLowPriorityNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->REMINDER_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    goto :goto_0
.end method

.method public getNotificationSmallIcon()I
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->getEntries()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    const v5, 0x7f020176

    :goto_1
    return v5

    :cond_3
    if-eqz v2, :cond_4

    if-nez v1, :cond_4

    const v5, 0x7f020177

    goto :goto_1

    :cond_4
    const v5, 0x7f020175

    goto :goto_1
.end method

.method public getNotificationStyle()Landroid/app/Notification$Style;
    .locals 5

    new-instance v2, Landroid/app/Notification$InboxStyle;

    invoke-direct {v2}, Landroid/app/Notification$InboxStyle;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->getEntries()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasReminderMessage()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->getEntries()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_1
    const-string v3, ""

    goto :goto_0
.end method

.method public getNotificationType()I
    .locals 7

    const/4 v4, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/MultiReminderNotification;->getEntries()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v3

    if-ne v3, v5, :cond_2

    move v4, v5

    :cond_1
    :goto_1
    return v4

    :cond_2
    if-eq v3, v4, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    if-nez v2, :cond_1

    const/4 v4, 0x3

    goto :goto_1
.end method
