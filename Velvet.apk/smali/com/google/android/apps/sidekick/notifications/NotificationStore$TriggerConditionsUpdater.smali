.class public abstract Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;
.super Ljava/lang/Object;
.source "NotificationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/NotificationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TriggerConditionsUpdater"
.end annotation


# instance fields
.field private final mConcluded:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mTriggered:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mTriggered:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mConcluded:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->update(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;Lcom/google/android/searchcommon/util/Clock;)V

    return-void
.end method

.method private update(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasLastTriggerTimeSeconds()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->areTriggerConditionsSatisfied(Lcom/google/geo/sidekick/Sidekick$Entry;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v1, :cond_0

    invoke-interface {p2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setLastTriggerTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mTriggered:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->clearLastTriggerTimeSeconds()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mConcluded:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method protected abstract areTriggerConditionsSatisfied(Lcom/google/geo/sidekick/Sidekick$Entry;Z)Z
.end method

.method public final getConcludedNotifications()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mConcluded:Ljava/util/Set;

    return-object v0
.end method

.method public final getTriggeredNotifications()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mTriggered:Ljava/util/Set;

    return-object v0
.end method

.method public final hasAffectedNotifications()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mTriggered:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->mConcluded:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
