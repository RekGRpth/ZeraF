.class public Lcom/google/android/apps/sidekick/notifications/NotificationStore;
.super Ljava/lang/Object;
.source "NotificationStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/notifications/NotificationStore$1;,
        Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;,
        Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;
    }
.end annotation


# static fields
.field public static final FILE_NAME:Ljava/lang/String; = "notifications_store"

.field static final SNOOZE_DURATION_SECS:J = 0x258L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private volatile mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mFileBytesReader:Lcom/google/android/apps/sidekick/FileBytesReader;

.field private final mFileBytesWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

.field private final mInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

.field private final mWriteLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;Lcom/google/android/searchcommon/util/Clock;Landroid/content/Context;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/sidekick/FileBytesReader;
    .param p2    # Lcom/google/android/apps/sidekick/FileBytesWriter;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mFileBytesReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mFileBytesWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mAppContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$300(Lcom/google/geo/sidekick/Sidekick$Notification;)Z
    .locals 1
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Notification;

    invoke-static {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->hasLocationTriggerConditions(Lcom/google/geo/sidekick/Sidekick$Notification;)Z

    move-result v0

    return v0
.end method

.method private flush()V
    .locals 4

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mFileBytesWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    const-string v1, "notifications_store"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->toByteArray()[B

    move-result-object v2

    const/high16 v3, 0x80000

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/FileBytesWriter;->writeEncryptedFileBytes(Ljava/lang/String;[BI)Z

    return-void
.end method

.method private static hasLocationTriggerConditions(Lcom/google/geo/sidekick/Sidekick$Notification;)Z
    .locals 5
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Notification;

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getConditionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_0

    :cond_1
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z
    .locals 12
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v8

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v9

    if-eq v8, v9, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    move v7, v6

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v8

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v9

    if-ne v8, v9, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    :goto_1
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v3

    :goto_2
    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v8

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v9

    if-ne v8, v9, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v8

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v9

    if-ne v8, v9, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventTimeSeconds()J

    move-result-wide v8

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventTimeSeconds()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-nez v8, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v8

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v9

    if-ne v8, v9, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v8

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v9

    if-ne v8, v9, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mAppContext:Landroid/content/Context;

    invoke-static {v8, v0}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mAppContext:Landroid/content/Context;

    invoke-static {v9, v1}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    goto :goto_1

    :cond_4
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v3

    goto :goto_2

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry()Z

    move-result v8

    if-nez v8, :cond_6

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry()Z

    move-result v8

    if-nez v8, :cond_5

    :goto_3
    move v7, v6

    goto/16 :goto_0

    :cond_5
    move v6, v7

    goto :goto_3

    :cond_6
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTaskId()Z

    move-result v8

    if-nez v8, :cond_8

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTaskId()Z

    move-result v8

    if-nez v8, :cond_7

    :goto_4
    move v7, v6

    goto/16 :goto_0

    :cond_7
    move v6, v7

    goto :goto_4

    :cond_8
    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTaskId()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTaskId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTaskId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2b -> :sswitch_1
    .end sparse-switch
.end method

.method private isExpired(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Z
    .locals 8
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v1, v4, v6

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getExpirationTimestampSeconds()J

    move-result-wide v4

    cmp-long v4, v4, v1

    if-gez v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isSnoozed(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Z
    .locals 7
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long v0, v3, v5

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getSnoozeTimeSeconds()J

    move-result-wide v3

    sub-long v3, v0, v3

    const-wide/16 v5, 0x258

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private readFromDisk()V
    .locals 6

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    new-instance v1, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mFileBytesReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    const-string v4, "notifications_store"

    const/high16 v5, 0x80000

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/sidekick/FileBytesReader;->readEncryptedFileBytes(Ljava/lang/String;I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->pruneOldData(Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    return-void

    :catch_0
    move-exception v2

    sget-object v3, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->TAG:Ljava/lang/String;

    const-string v4, "Error reading notifications from disk"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private waitForInitialization()Z
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->TAG:Ljava/lang/String;

    const-string v2, "Initialization latch wait interrupted"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearAll()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mFileBytesWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    const-string v2, "notifications_store"

    invoke-virtual {v0, v2}, Lcom/google/android/apps/sidekick/FileBytesWriter;->deleteFile(Ljava/lang/String;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearNotifiedMarkers()V
    .locals 7

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v6, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v4, v6}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setNotified(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->clearSnoozeTimeSeconds()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public deleteNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)Z
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v7, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v7}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v5, v7}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method getClientData()Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    return-object v0
.end method

.method public getEntriesToNotify()Ljava/util/Collection;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v3, v8, v10

    iget-object v8, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v8}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isSnoozed(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isExpired(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->hasLocationTriggerConditions(Lcom/google/geo/sidekick/Sidekick$Notification;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasLastTriggerTimeSeconds()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getTimeSeconds()J

    move-result-wide v6

    cmp-long v8, v6, v3

    if-gtz v8, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getExpiredNotifications()Ljava/util/Collection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long v3, v6, v8

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->getExpirationTimestampSeconds()J

    move-result-wide v6

    cmp-long v6, v6, v3

    if-gtz v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getNextExpirationTimeMillis()Ljava/lang/Long;
    .locals 12

    const-wide/16 v10, 0x3e8

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    const-wide v1, 0x7fffffffffffffffL

    iget-object v8, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v8

    div-long v4, v8, v10

    iget-object v8, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v8}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->getExpirationTimestampSeconds()J

    move-result-wide v8

    cmp-long v8, v8, v4

    if-lez v8, :cond_2

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->getExpirationTimestampSeconds()J

    move-result-wide v8

    invoke-static {v1, v2, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    goto :goto_1

    :cond_3
    const-wide v8, 0x7fffffffffffffffL

    cmp-long v8, v1, v8

    if-eqz v8, :cond_0

    mul-long v7, v1, v10

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_0
.end method

.method public getNextNotificationTimeMillis()Ljava/lang/Long;
    .locals 10

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-direct {p0, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isSnoozed(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getSnoozeTimeSeconds()J

    move-result-wide v6

    const-wide/16 v8, 0x258

    add-long/2addr v6, v8

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_1

    :cond_3
    invoke-static {v3}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->hasLocationTriggerConditions(Lcom/google/geo/sidekick/Sidekick$Notification;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->hasTimeSeconds()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getTimeSeconds()J

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_1

    :cond_4
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v6, v0, v6

    if-eqz v6, :cond_0

    const-wide/16 v5, 0x3e8

    mul-long/2addr v5, v0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0
.end method

.method public getNextRefreshTimeMillis()Ljava/lang/Long;
    .locals 11

    const-wide/16 v9, 0x3e8

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    const-wide v4, 0x7fffffffffffffffL

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v7}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v7

    div-long v1, v7, v9

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingRefreshList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getRefreshTimeSeconds()J

    move-result-wide v7

    cmp-long v7, v7, v1

    if-ltz v7, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getRefreshTimeSeconds()J

    move-result-wide v7

    invoke-static {v4, v5, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_1

    :cond_3
    const-wide v7, 0x7fffffffffffffffL

    cmp-long v7, v4, v7

    if-eqz v7, :cond_0

    mul-long v6, v4, v9

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto :goto_0
.end method

.method public getPendingRefreshInterests()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Interest;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    :cond_0
    return-object v1

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v5}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long v2, v5, v7

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingRefreshList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getRefreshTimeSeconds()J

    move-result-wide v5

    cmp-long v5, v5, v2

    if-gtz v5, :cond_2

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getStoredEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getUnexpiredNotifications()Ljava/util/Collection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long v3, v6, v8

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->getExpirationTimestampSeconds()J

    move-result-wide v6

    cmp-long v6, v6, v3

    if-lez v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public initialize()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->readFromDisk()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    return-void
.end method

.method public isInitialized()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public markEntryNotificationDismissed(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v7, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v7}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v5, v7}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setNotificationDismissed(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public markEntryNotified(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v7, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v7}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v5, v7}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setNotified(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method pruneOldData(Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;)V
    .locals 14
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const-wide/16 v12, 0x3e8

    iget-object v10, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v10}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v10, 0x5265c00

    sub-long v8, v3, v10

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->hasLocationTriggerConditions(Lcom/google/geo/sidekick/Sidekick$Notification;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getTimeSeconds()J

    move-result-wide v10

    mul-long v1, v10, v12

    cmp-long v10, v1, v8

    if-gez v10, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingRefreshList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getRefreshTimeSeconds()J

    move-result-wide v10

    mul-long/2addr v10, v12

    cmp-long v10, v10, v3

    if-gez v10, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public recordRefresh(JLcom/google/geo/sidekick/Sidekick$Interest;)V
    .locals 10
    .param p1    # J
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v8, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v8}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v6, v8}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v0, 0x0

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingRefreshList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    new-instance v1, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v1, p3}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    new-instance v3, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getRefreshTimeSeconds()J

    move-result-wide v8

    cmp-long v6, v8, p1

    if-gtz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public resetNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 7
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v6, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v4, v6}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setNotified(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->clearSnoozeTimeSeconds()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->clearNotificationDismissed()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method setClientDataForTest(Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public snooze(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 11
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v7, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v7}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v5, v7}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v5}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    invoke-virtual {v3, v7, v8}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setSnoozeTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->clearNotified()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public updateLocationTriggerConditions(Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v6, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v6}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v4, v6}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->hasLocationTriggerConditions(Lcom/google/geo/sidekick/Sidekick$Notification;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    # invokes: Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->update(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;Lcom/google/android/searchcommon/util/Clock;)V
    invoke-static {p1, v3, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->access$200(Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;Lcom/google/android/searchcommon/util/Clock;)V

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;->hasAffectedNotifications()Z

    move-result v4

    if-eqz v4, :cond_3

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_3
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public updateNextRefreshTime(JLcom/google/geo/sidekick/Sidekick$Interest;Z)V
    .locals 15
    .param p1    # J
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Interest;
    .param p4    # Z

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v11

    if-nez v11, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v5, Lcom/google/android/apps/sidekick/ProtoKey;

    move-object/from16 v0, p3

    invoke-direct {v5, v0}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iget-object v11, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v11}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v11

    const-wide/16 v13, 0x3e8

    div-long v8, v11, v13

    iget-object v12, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v12

    :try_start_0
    iget-object v11, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v13, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v13}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v11, v13}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingRefreshList()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    new-instance v6, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v11

    invoke-direct {v6, v11}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v3, 0x1

    if-nez p4, :cond_2

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getRefreshTimeSeconds()J

    move-result-wide v13

    cmp-long v11, p1, v13

    if-ltz v11, :cond_2

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->getRefreshTimeSeconds()J

    move-result-wide v13

    cmp-long v11, v13, v8

    if-gez v11, :cond_1

    :cond_2
    move-wide/from16 v0, p1

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->setRefreshTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    if-nez v3, :cond_4

    new-instance v11, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    invoke-direct {v11}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->setInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    move-result-object v11

    move-wide/from16 v0, p1

    invoke-virtual {v11, v0, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;->setRefreshTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->addPendingRefresh(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_5

    iput-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    :cond_5
    monitor-exit v12

    goto :goto_0

    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11
.end method

.method public updatePendingNotifications(Lcom/google/geo/sidekick/Sidekick$EntryTree;Lcom/google/geo/sidekick/Sidekick$Interest;)V
    .locals 12
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTree;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->waitForInitialization()Z

    move-result v9

    if-nez v9, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v9

    if-eqz v9, :cond_3

    new-instance v3, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;

    const/4 v9, 0x0

    invoke-direct {v3, v9}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;-><init>(Lcom/google/android/apps/sidekick/notifications/NotificationStore$1;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;->visit(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    # getter for: Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;->entriesWithNotifications:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;->access$100(Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;)Ljava/util/List;

    move-result-object v0

    :goto_1
    iget-object v10, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mWriteLock:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    iget-object v9, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    new-instance v11, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {v11}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;-><init>()V

    invoke-static {v9, v11}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    new-instance v5, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v5, p2}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    new-instance v9, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v8}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v11

    invoke-direct {v9, v11}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v5, v9}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v8}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v9

    invoke-direct {p0, v1, v9}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->isEquivalent(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Entry;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v2, v1

    invoke-virtual {v8, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    goto :goto_3

    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    :cond_3
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_1

    :cond_4
    if-nez v2, :cond_5

    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_5
    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    new-instance v9, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-direct {v9}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;-><init>()V

    invoke-virtual {v9, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    move-result-object v9

    invoke-virtual {v9, p2}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->addPendingNotification(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    goto :goto_4

    :cond_7
    iput-object v7, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->mClientData:Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->flush()V

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
