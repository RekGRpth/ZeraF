.class public final enum Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
.super Ljava/lang/Enum;
.source "NowNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum CALENDAR_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum EVENT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum FLIGHT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum PUBLIC_ALERT_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum REMINDER_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum RESTAURANT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

.field public static final enum TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;


# instance fields
.field private final mNotificationId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x7

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x3

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "TRAFFIC_NOTIFICATION"

    invoke-direct {v0, v1, v8, v4}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "CALENDAR_TIME_TO_LEAVE_NOTIFICATION"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->CALENDAR_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "LOW_PRIORITY_NOTIFICATION"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "PUBLIC_ALERT_NOTIFICATION"

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->PUBLIC_ALERT_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "RESTAURANT_TIME_TO_LEAVE_NOTIFICATION"

    const/4 v2, 0x4

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->RESTAURANT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "EVENT_TIME_TO_LEAVE_NOTIFICATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->EVENT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "FLIGHT_TIME_TO_LEAVE_NOTIFICATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->FLIGHT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const-string v1, "REMINDER_NOTIFICATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->REMINDER_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v1, v0, v8

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->CALENDAR_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->PUBLIC_ALERT_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v1, v0, v4

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->RESTAURANT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->EVENT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->FLIGHT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->REMINDER_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->$VALUES:[Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->mNotificationId:I

    return-void
.end method

.method public static typeFromIntent(Landroid/content/Intent;)Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 8
    .param p0    # Landroid/content/Intent;

    const/4 v5, 0x0

    const-string v6, "com.google.android.apps.sidekick.FROM_NOTIFICATION"

    const/4 v7, -0x1

    invoke-virtual {p0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-gez v2, :cond_1

    move-object v4, v5

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-static {}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->values()[Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v0

    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->getNotificationId()I

    move-result v6

    if-eq v6, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->$VALUES:[Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-virtual {v0}, [Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-object v0
.end method


# virtual methods
.method public addToIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "com.google.android.apps.sidekick.FROM_NOTIFICATION"

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->getNotificationId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-void
.end method

.method public getNotificationId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->mNotificationId:I

    return v0
.end method
