.class public final Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Notifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/Notifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientData"
.end annotation


# instance fields
.field private cachedSize:I

.field private pendingNotification_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;",
            ">;"
        }
    .end annotation
.end field

.field private pendingRefresh_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingNotification_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingRefresh_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addPendingNotification(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingNotification_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingNotification_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingNotification_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPendingRefresh(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingRefresh_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingRefresh_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingRefresh_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->cachedSize:I

    return v0
.end method

.method public getPendingNotification(I)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingNotification_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    return-object v0
.end method

.method public getPendingNotificationCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingNotification_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPendingNotificationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingNotification_:Ljava/util/List;

    return-object v0
.end method

.method public getPendingRefresh(I)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingRefresh_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    return-object v0
.end method

.method public getPendingRefreshCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingRefresh_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPendingRefreshList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->pendingRefresh_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingRefreshList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_1
    iput v2, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->cachedSize:I

    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->addPendingNotification(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->addPendingRefresh(Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingNotificationList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$ClientData;->getPendingRefreshList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingRefresh;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_1
    return-void
.end method
