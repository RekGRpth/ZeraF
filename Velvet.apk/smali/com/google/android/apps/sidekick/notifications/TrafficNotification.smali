.class public Lcom/google/android/apps/sidekick/notifications/TrafficNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;
.source "TrafficNotification.java"


# instance fields
.field private final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-void
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/TravelReport;->buildCommuteString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->isLowPriorityNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    goto :goto_0
.end method

.method public getNotificationSmallIcon()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/TravelReport;->getTravelMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7f020174

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/sidekick/IconSet;->HOME_NOTIFICATION:Lcom/google/android/apps/sidekick/IconSet;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/IconSet;->getIcon(I)I

    move-result v0

    goto :goto_0

    :cond_1
    const v0, 0x7f02017b

    goto :goto_0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasNotificationBarText()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->getNotificationBarText()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f0d00eb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
