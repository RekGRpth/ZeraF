.class Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;
.super Ljava/lang/Object;
.source "GenericPlaceEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->renamePlaceOnClick(Landroid/app/Activity;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

.field final synthetic val$confirmRenameAction:Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;->val$confirmRenameAction:Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;->val$confirmRenameAction:Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;->run()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    const-string v3, "RENAME_PLACE"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
