.class Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationWatchDog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p2    # Lcom/google/android/apps/sidekick/LocationOracleImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    return-void
.end method


# virtual methods
.method reset()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public run()V
    .locals 10

    const-wide/32 v8, 0x116520

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->reset()V

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v6}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocationReceivedMillis:J
    invoke-static {v6}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2900(Lcom/google/android/apps/sidekick/LocationOracleImpl;)J

    move-result-wide v6

    sub-long v4, v0, v6

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastAndroidLocationReceivedMillis:J
    invoke-static {v6}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3000(Lcom/google/android/apps/sidekick/LocationOracleImpl;)J

    move-result-wide v6

    sub-long v2, v0, v6

    cmp-long v6, v4, v8

    if-lez v6, :cond_0

    cmp-long v6, v2, v8

    if-lez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithAndroidProviders()V
    invoke-static {v6}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    :cond_0
    const-wide/32 v6, 0xd0bd8

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithGmmProvider()V
    invoke-static {v6}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    :cond_1
    return-void
.end method
