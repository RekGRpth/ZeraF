.class Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;
.super Ljava/lang/Object;
.source "DismissableLinearLayout.java"

# interfaces
.implements Lcom/google/android/velvet/tg/SwipeHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/DismissableLinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwipeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Lcom/google/android/apps/sidekick/DismissableLinearLayout$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/DismissableLinearLayout;
    .param p2    # Lcom/google/android/apps/sidekick/DismissableLinearLayout$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;-><init>(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)V

    return-void
.end method


# virtual methods
.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    return v0
.end method

.method dragEnd(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$102(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->invalidate()V

    return-void
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    # getter for: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z
    invoke-static {v1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$100(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    # invokes: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getEventCoordsInDismissableContainer(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$200(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    # invokes: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getChildAtXYPosition(II)Landroid/view/View;
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$300(Lcom/google/android/apps/sidekick/DismissableLinearLayout;II)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    # setter for: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$102(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->invalidate()V

    return-void
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    # getter for: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$400(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$102(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    # getter for: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissListener:Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$500(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    # getter for: Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissListener:Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->access$500(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;->onViewDismissed(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public onSnapBackCompleted(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;->dragEnd(Landroid/view/View;)V

    return-void
.end method
