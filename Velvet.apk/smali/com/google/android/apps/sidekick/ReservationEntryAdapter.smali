.class public Lcom/google/android/apps/sidekick/ReservationEntryAdapter;
.super Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;
.source "ReservationEntryAdapter.java"


# instance fields
.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

.field private final mRelativeDays:I


# direct methods
.method protected constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 13
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p6    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;
    .param p7    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p8    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v9}, Landroid/text/format/Time;->setToNow()V

    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventTimeSeconds()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long v11, v1, v3

    invoke-virtual {v10, v11, v12}, Landroid/text/format/Time;->set(J)V

    invoke-static {v9, v10}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->relativeDays(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mRelativeDays:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/ReservationEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-object v0
.end method

.method private createEventView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040034

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->populateEventView(Landroid/content/Context;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method private getLocationName()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasDisplayName()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getReservationString(Landroid/content/Context;II)Ljava/lang/CharSequence;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventTimeSeconds()J

    move-result-wide v3

    const-wide/16 v7, 0x3e8

    mul-long v1, v3, v7

    move-object v0, p1

    move-wide v3, v1

    move v5, p3

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v0, v3

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isNavigationShowing(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getTravelReport()Lcom/google/android/apps/sidekick/TravelReport;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mightShowNavigateButtonFor(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->shouldShowNavigation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSourcedFromGmail()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private populateEventView(Landroid/content/Context;Landroid/view/View;)Landroid/view/View;
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v7

    const v9, 0x7f100031

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$Location;->hasAddress()Z

    move-result v9

    if-eqz v9, :cond_0

    const v9, 0x7f1000b3

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$Location;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->reservationIsToday()Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v4, 0x1

    :goto_0
    const v9, 0x7f100026

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    const v10, 0x7f0d0290

    invoke-direct {p0, p1, v10, v4}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getReservationString(Landroid/content/Context;II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventImage()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v6

    const v9, 0x7f1000ac

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasPhotoAttribution()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Photo;->getPhotoAttribution()Lcom/google/geo/sidekick/Sidekick$Attribution;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attribution;->hasTitle()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attribution;->getTitle()Ljava/lang/String;

    move-result-object v2

    :goto_1
    const v9, 0x7f1000ad

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attribution;->hasUrl()Z

    move-result v9

    if-eqz v9, :cond_1

    new-instance v9, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;

    invoke-direct {v9, p0, p1, v1}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/ReservationEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Attribution;)V

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v9, 0x7f1000ab

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    instance-of v9, p1, Landroid/app/Activity;

    if-eqz v9, :cond_3

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->updateActionButtons(Landroid/app/Activity;Landroid/view/View;)V

    :cond_3
    return-object p2

    :cond_4
    const/16 v4, 0x13

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attribution;->getUrl()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method static relativeDays(Landroid/text/format/Time;Landroid/text/format/Time;)I
    .locals 7
    .param p0    # Landroid/text/format/Time;
    .param p1    # Landroid/text/format/Time;

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    invoke-virtual {p1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    sub-int v2, v1, v0

    return v2
.end method

.method private reservationIsToday()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mRelativeDays:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reservationIsTomorrow()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mRelativeDays:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateHotelContext(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->reservationIsToday()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0d028d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0d028c

    const/16 v1, 0x12

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getReservationString(Landroid/content/Context;II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->reservationIsToday()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d028e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->reservationIsTomorrow()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d028f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateRestaurantContext(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->reservationIsToday()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const v1, 0x7f0d0290

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getReservationString(Landroid/content/Context;II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x13

    goto :goto_0
.end method


# virtual methods
.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 6

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getLocationName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const v3, 0x7f0d02ec

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;I[Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    :goto_0
    return-object v2

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v2

    goto :goto_0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f0d02bf

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0d02c3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0d02c1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "GmailEventReservation"

    goto :goto_0

    :pswitch_1
    const-string v0, "GmailRestaurantReservation"

    goto :goto_0

    :pswitch_2
    const-string v0, "GmailHotelReservation"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    const-class v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, ":android:show_fragment"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v1

    :pswitch_0
    const-class v0, Lcom/google/android/searchcommon/preferences/cards/GmailEventsCardSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-class v0, Lcom/google/android/searchcommon/preferences/cards/GmailRestaurantsCardSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const-class v0, Lcom/google/android/searchcommon/preferences/cards/GmailHotelsCardSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 4

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->isSourcedFromGmail()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->createEventView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v6

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->isNavigationShowing(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->launchDetails(Landroid/content/Context;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v2

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;-><init>(Landroid/content/Context;JLcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->run()V

    const-string v0, "PLACE_PAGE"

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->logDetailsInteractionWithLabel(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public shouldDisplay()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected updateActionButtons(Landroid/app/Activity;Landroid/view/View;)V
    .locals 20
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->updateActionButtons(Landroid/app/Activity;Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v18

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->isNavigationShowing(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f10002e

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/Button;

    invoke-virtual/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v15

    const/4 v2, 0x5

    if-ne v15, v2, :cond_2

    const v2, 0x7f0d0284

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    :goto_0
    new-instance v2, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v1, v13, v14}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/ReservationEntryAdapter;Landroid/app/Activity;J)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    const v2, 0x7f100031

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const v2, 0x7f10002f

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    invoke-virtual/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getGmailReferenceList()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/MoonshineUtilities;->getEffectiveGmailReferenceAndSetText(Landroid/content/Context;Landroid/widget/Button;Ljava/util/List;)Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v17

    if-eqz v17, :cond_1

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v2, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    invoke-virtual/range {v17 .. v17}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getEmailUrl()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const-string v8, "RESERVATION_EMAIL"

    const-string v9, "mail"

    sget-object v10, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->GMAIL_URL_PREFIXES:[Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v12

    move-object/from16 v3, p1

    move-object/from16 v7, p0

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void

    :cond_2
    const v2, 0x7f0d0281

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method protected updateContextMessage(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const v1, 0x7f100026

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->updateRestaurantContext(Landroid/content/Context;Landroid/widget/TextView;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->updateHotelContext(Landroid/content/Context;Landroid/widget/TextView;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected updateMapOrImage(Landroid/content/Context;Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getTravelReport()Lcom/google/android/apps/sidekick/TravelReport;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->showMap(Landroid/content/Context;Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f100029

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateTitle(Landroid/content/Context;Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getLocationName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const v3, 0x7f100031

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected updateTravelTime(Landroid/content/Context;Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getTravelReport()Lcom/google/android/apps/sidekick/TravelReport;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/TravelReport;->getTotalEtaMinutes()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v4, p1}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficColorForHtml(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/TravelReport;->getTravelMode()I

    move-result v5

    if-ne v5, v9, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/TravelReport;->getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/TravelReport;->getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const v5, 0x7f100027

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f0d0291

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4, p1}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficColorForHtml(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    aput-object v1, v6, v9

    const/4 v7, 0x2

    aput-object v0, v6, v7

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/TravelReport;->getRouteSummary()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
