.class Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;
.super Ljava/lang/Object;
.source "AsyncFileStorageImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReadData"
.end annotation


# instance fields
.field private final mCallback:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function",
            "<[B",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mEncrypted:Z

.field private final mFilename:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/base/Function;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mFilename:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mCallback:Lcom/google/common/base/Function;

    iput-boolean p3, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mEncrypted:Z

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mEncrypted:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;)Lcom/google/common/base/Function;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;->mCallback:Lcom/google/common/base/Function;

    return-object v0
.end method
