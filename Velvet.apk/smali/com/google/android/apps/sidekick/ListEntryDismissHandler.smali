.class public Lcom/google/android/apps/sidekick/ListEntryDismissHandler;
.super Ljava/lang/Object;
.source "ListEntryDismissHandler.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mContext:Landroid/content/Context;

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mEntryViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mGroupEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/util/Map;Landroid/content/Context;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p4    # Landroid/content/Context;
    .param p5    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p6    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p7    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p8    # Lcom/google/android/searchcommon/util/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/sidekick/TgPresenterAccessor;",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Lcom/google/android/apps/sidekick/inject/EntryProvider;",
            "Lcom/google/android/searchcommon/util/Clock;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mGroupEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mEntryViews:Ljava/util/Map;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mContext:Landroid/content/Context;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method public onViewDismissed(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mEntryViews:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    if-eqz v1, :cond_1

    const/4 v5, 0x1

    invoke-static {v1, v5}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v5}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-static {v5, v6}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->newBuilder(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->setTimestamp(J)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->addEntry(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->build()Lcom/google/android/apps/sidekick/actions/DismissEntryTask;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Void;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mGroupEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-static {v1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->removeGroupChildEntries(Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;)V

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mEntryViews:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;->mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v5, v6, v7}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->dismissEntry(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    :cond_2
    return-void
.end method
