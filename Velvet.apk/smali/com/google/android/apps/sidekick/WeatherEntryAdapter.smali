.class public Lcom/google/android/apps/sidekick/WeatherEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "WeatherEntryAdapter.java"


# instance fields
.field private final mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWeatherEntry()Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    return-void
.end method

.method private getDensityDpiString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const-string v0, "xhdpi"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "hdpi"

    goto :goto_0

    :sswitch_1
    const-string v0, "mdpi"

    goto :goto_0

    :sswitch_2
    const-string v0, "mdpi"

    goto :goto_0

    :sswitch_3
    const-string v0, "hdpi"

    goto :goto_0

    :sswitch_4
    const-string v0, "xhdpi"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_1
        0xa0 -> :sswitch_2
        0xd5 -> :sswitch_3
        0xf0 -> :sswitch_0
        0x140 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 3

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;

    invoke-direct {v2, p0}, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;-><init>(Lcom/google/android/apps/sidekick/WeatherEntryAdapter;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0209

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0166

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0167

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageUrl(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-direct {p0, v3}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getDensityDpiString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocationType()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocationType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0d02e2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    const v0, 0x7f0d02e3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0d02e4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0d02e5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocationType()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocationType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Home"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Work"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "TripDestination"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/WeatherCardSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/WeatherNotification;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/notifications/WeatherNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocation()Z

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v10

    const/16 v15, 0x2c

    invoke-virtual {v10, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-lez v5, :cond_0

    const/4 v15, 0x0

    invoke-virtual {v10, v15, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    :cond_0
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    new-instance v3, Lcom/google/android/velvet/cards/WeatherCard$Builder;

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v15

    invoke-direct {v3, v10, v15}, Lcom/google/android/velvet/cards/WeatherCard$Builder;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getDescription()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v15}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setConditions(Ljava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getDescription()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v15, 0x7f0d013b

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasChanceOfPrecipitation()Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getChanceOfPrecipitation()I

    move-result v15

    invoke-virtual {v3, v15}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setChanceOfPrecipitation(I)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    :cond_2
    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v8, v1}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getImageUrl(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v3, v15}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setIcon(Landroid/net/Uri;)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    :cond_3
    :goto_0
    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindSpeed()Z

    move-result v15

    if-eqz v15, :cond_4

    const-string v14, ""

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWindUnit()I

    move-result v15

    packed-switch v15, :pswitch_data_0

    :goto_1
    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWindSpeed()I

    move-result v15

    invoke-virtual {v3, v15, v14}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setWindSpeed(ILjava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    :cond_4
    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointCount()I

    move-result v15

    if-ge v5, v15, :cond_7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v15, v5}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v15, v1}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getImageUrl(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    :goto_3
    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLabel()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLowTemperature()I

    move-result v16

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v3, v15, v7, v0, v1}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->addForecast(Ljava/lang/CharSequence;Landroid/net/Uri;II)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLabel()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getDescription()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v15, 0x7f0d013c

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v15, 0x7f0d013d

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLowTemperature()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointCount()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-virtual {v13}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v3, v15}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setIcon(Landroid/net/Uri;)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    goto/16 :goto_0

    :pswitch_0
    const v15, 0x7f0d0139

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_1

    :pswitch_1
    const v15, 0x7f0d0138

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_1

    :pswitch_2
    const v15, 0x7f0d013a

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_1

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getChangeDescriptionsList()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLabel()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getDescription()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->addLaterConditionChange(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;

    goto :goto_4

    :cond_8
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->create(Landroid/content/Context;)Lcom/google/android/velvet/cards/WeatherCard;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f10005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocation()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/location/Location;

    const-string v1, "weather"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v1

    const v2, 0x7f0d01f4

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2, v0}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->startWebSearch(Landroid/content/Context;Ljava/lang/String;Landroid/location/Location;)Z

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->logDetailsInteraction(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
