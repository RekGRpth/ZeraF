.class Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;
.super Ljava/lang/Object;
.source "FlightStatusEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->populateView(Landroid/content/Context;Lcom/google/android/velvet/cards/FlightCard;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

.field final synthetic val$location:Lcom/google/geo/sidekick/Sidekick$Location;

.field final synthetic val$travelMode:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Location;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->val$location:Lcom/google/geo/sidekick/Sidekick$Location;

    iput p3, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->val$travelMode:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)Lcom/google/android/apps/sidekick/DirectionsLauncher;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->val$location:Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->val$travelMode:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->start(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/util/List;I)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    const-string v3, "NAVIGATE"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
