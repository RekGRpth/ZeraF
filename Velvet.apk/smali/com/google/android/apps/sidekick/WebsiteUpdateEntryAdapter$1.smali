.class Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;
.super Ljava/lang/Object;
.source "WebsiteUpdateEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->addUpdateRow(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/google/android/apps/sidekick/CardTableLayout;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$websiteUpdateEntry:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;->val$websiteUpdateEntry:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;->val$websiteUpdateEntry:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getClickUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "WEBSITE_UPDATE_READ_FULL_STORY"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
