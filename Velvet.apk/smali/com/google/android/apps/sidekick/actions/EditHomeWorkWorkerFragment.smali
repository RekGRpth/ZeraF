.class public Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;
.super Landroid/app/Fragment;
.source "EditHomeWorkWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;
    }
.end annotation


# instance fields
.field private mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private mTask:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->handleResponse(Z)V

    return-void
.end method

.method public static buildArguments(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "entry_key"

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v1, "action_key"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    if-eqz p2, :cond_0

    const-string v1, "edited_location_key"

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Location;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_0
    const-string v1, "old_name_key"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "old_address_key"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getTgPresenter()Lcom/google/android/velvet/presenter/TgPresenter;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/VelvetActivity;->getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/velvet/presenter/TgPresenter;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/velvet/presenter/TgPresenter;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleResponse(Z)V
    .locals 8
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v0, "entry_key"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const-string v0, "action_key"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    const-string v0, "old_name_key"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "old_address_key"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    const-string v1, "edited_location_key"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$Location;

    move-object v0, p0

    move v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->editPlaceTaskFinished(ZLcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->hide(Landroid/app/FragmentManager;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method public static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;
    .locals 2
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;-><init>()V

    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->buildArguments(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method protected editPlaceTaskFinished(ZLcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Z
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v2, p2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->findAdapterForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    new-instance v3, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;

    invoke-direct {v3, p2, p4}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->mutateEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getTgPresenter()Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v2, v0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->handlePlaceEdit(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v7}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v0, "entry_key"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const-string v0, "action_key"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    const-string v1, "edited_location_key"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->show(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    new-instance v0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;-><init>(Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->cancel(Z)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method
