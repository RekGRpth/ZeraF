.class public Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;
.super Ljava/lang/Object;
.source "ViewInMapsAction.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/actions/EntryAction;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

.field private final mContext:Landroid/content/Context;

.field private final mShowRoute:Z

.field private final mSource:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mTravelMode:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mTravelMode:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mShowRoute:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-static {p4}, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->fromSidekickProtoTravelMode(I)Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mTravelMode:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mShowRoute:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mShowRoute:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mTravelMode:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/MapsLauncher;->start(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->mAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/MapsLauncher;->start(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    goto :goto_0
.end method
