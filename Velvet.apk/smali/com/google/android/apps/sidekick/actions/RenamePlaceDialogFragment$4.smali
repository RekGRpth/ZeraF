.class Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;
.super Ljava/lang/Object;
.source "RenamePlaceDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

.field final synthetic val$dialog:Landroid/app/Dialog;

.field final synthetic val$editText:Landroid/widget/EditText;

.field final synthetic val$items:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Ljava/util/List;Landroid/widget/EditText;Landroid/app/Dialog;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->val$items:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->val$editText:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->val$dialog:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->val$items:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->val$editText:Landroid/widget/EditText;

    # getter for: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->access$300(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    # getter for: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->access$300(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v2

    # invokes: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->startEditPlaceTask(Lcom/google/geo/sidekick/Sidekick$PlaceData;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->access$000(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Lcom/google/geo/sidekick/Sidekick$PlaceData;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->hideSoftKeyboard(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method
