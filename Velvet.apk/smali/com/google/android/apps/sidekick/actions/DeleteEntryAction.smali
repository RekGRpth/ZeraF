.class public Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;
.super Lcom/google/android/apps/sidekick/actions/EntryActionBase;
.source "DeleteEntryAction.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/actions/EntryActionBase;-><init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "delete_place_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
