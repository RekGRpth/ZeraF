.class public Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;
.super Lcom/google/android/apps/sidekick/actions/BaseEditDialogFragment;
.source "EditHomeWorkDialogFragment.java"


# instance fields
.field private mAction:Lcom/google/geo/sidekick/Sidekick$Action;

.field private mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private mLocation:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/actions/BaseEditDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;Lcom/google/geo/sidekick/Sidekick$Location;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->startServerAction(Lcom/google/geo/sidekick/Sidekick$Location;)V

    return-void
.end method

.method private static getFrequentPlaceLocation(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;
    .locals 4
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry_key"

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "action_key"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    new-instance v1, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private startServerAction(Lcom/google/geo/sidekick/Sidekick$Location;)V
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "editPlaceWorkerFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Location;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, p1, v4, v5}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "editPlaceWorkerFragment"

    invoke-virtual {v2, v1, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method


# virtual methods
.method createEditDialog(ILandroid/view/View;Landroid/widget/EditText;Landroid/widget/EditText;)Landroid/app/AlertDialog;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/widget/EditText;
    .param p4    # Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getAddress()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p4, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;

    invoke-direct {v3, p0, p4, p3}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;-><init>(Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$1;

    invoke-direct {v3, p0, p4}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$1;-><init>(Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;

    invoke-direct {v1, p0, p4}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;-><init>(Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v0

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method editHomeAddressDialog()Landroid/app/AlertDialog;
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040030

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f1000a3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    const v3, 0x7f0d01e3

    const v2, 0x7f1000a4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    invoke-virtual {p0, v3, v0, v5, v2}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->createEditDialog(ILandroid/view/View;Landroid/widget/EditText;Landroid/widget/EditText;)Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method editWorkLabelAndAddressDialog()Landroid/app/AlertDialog;
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040030

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v3, 0x7f1000a3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-static {v3}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->getFrequentPlaceLocation(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0d01e7

    const v3, 0x7f1000a4

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {p0, v4, v0, v1, v3}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->createEditDialog(ILandroid/view/View;Landroid/widget/EditText;Landroid/widget/EditText;)Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3

    :cond_0
    const-string v3, ""

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const-string v1, "action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-static {v1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->getFrequentPlaceLocation(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v1

    const/16 v2, 0x11

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->editHomeAddressDialog()Landroid/app/AlertDialog;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->editWorkLabelAndAddressDialog()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0
.end method
