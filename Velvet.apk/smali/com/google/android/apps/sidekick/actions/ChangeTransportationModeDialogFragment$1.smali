.class Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;
.super Ljava/lang/Object;
.source "ChangeTransportationModeDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;

.field final synthetic val$entryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field final synthetic val$preferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

.field final synthetic val$travelModeSetting:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;ILcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;

    iput p2, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->val$travelModeSetting:I

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->val$preferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->val$entryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget v1, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->val$travelModeSetting:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const v0, 0x7f0d0075

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->val$preferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->val$entryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;->dismiss()V

    return-void

    :cond_0
    const v0, 0x7f0d0076

    goto :goto_0
.end method
