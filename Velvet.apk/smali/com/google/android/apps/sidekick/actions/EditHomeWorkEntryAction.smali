.class public Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;
.super Lcom/google/android/apps/sidekick/actions/EntryActionBase;
.source "EditHomeWorkEntryAction.java"


# instance fields
.field private final mPromptToEdit:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Z)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/actions/EntryActionBase;-><init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;)V

    iput-boolean p4, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;->mPromptToEdit:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;->mPromptToEdit:Z

    if-nez v3, :cond_1

    const-string v3, "editPlaceWorkerFragment"

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-static {v3, v4, v5, v5, v5}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    move-result-object v2

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    const-string v4, "editPlaceWorkerFragment"

    invoke-virtual {v3, v2, v4}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-static {v3, v4}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

    move-result-object v0

    const-string v3, "edit_home_work"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
