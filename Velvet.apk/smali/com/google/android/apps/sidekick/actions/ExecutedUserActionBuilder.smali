.class public Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;
.super Ljava/lang/Object;
.source "ExecutedUserActionBuilder.java"


# instance fields
.field private final mAction:Lcom/google/geo/sidekick/Sidekick$Action;

.field private mCustomPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

.field private final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private mExecutionTimeMs:J

.field private final mTimestampMs:J


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;J)V
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mExecutionTimeMs:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mCustomPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    iput-wide p3, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mTimestampMs:J

    return-void
.end method

.method static stripCommuteSummayWaypoints(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 4
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteCount()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->parseFrom([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->clearWaypoints()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object p0, v2

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 7

    const-wide/16 v5, 0x3e8

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setAction(Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-static {v2}, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->stripCommuteSummayWaypoints(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mTimestampMs:J

    div-long/2addr v2, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mTimestampMs:J

    invoke-virtual {v2, v3, v4}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v2, v5

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mExecutionTimeMs:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mExecutionTimeMs:J

    invoke-virtual {v0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setExecutionTimeMs(J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mCustomPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mCustomPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setCustomPlace(Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    :cond_1
    return-object v0
.end method

.method public withCustomPlace(Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mCustomPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    return-object p0
.end method

.method public withExecutionTimeMs(J)Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->mExecutionTimeMs:J

    return-object p0
.end method
