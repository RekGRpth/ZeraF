.class public Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;
.super Ljava/lang/Object;
.source "RecordActionTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/RecordActionTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final mActions:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation
.end field

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mNewPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$PlaceData;",
            "Lcom/google/android/searchcommon/util/Clock;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mActions:Ljava/util/Collection;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mNewPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method public create()Lcom/google/android/apps/sidekick/actions/RecordActionTask;
    .locals 6

    new-instance v0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mActions:Ljava/util/Collection;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mNewPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V

    return-object v0
.end method
