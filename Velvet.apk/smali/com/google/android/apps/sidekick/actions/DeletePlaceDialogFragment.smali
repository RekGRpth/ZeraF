.class public Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;
.super Landroid/app/DialogFragment;
.source "DeletePlaceDialogFragment.java"


# instance fields
.field private mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

.field private mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->startDeletePlaceTask()V

    return-void
.end method

.method public static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;
    .locals 4
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry_key"

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "delete_action_key"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    new-instance v1, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private startDeletePlaceTask()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "deletePlaceWorkerFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-static {v2, v3}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "deletePlaceWorkerFragment"

    invoke-virtual {v2, v1, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "entry_key"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const-string v3, "delete_action_key"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0d01ef

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x104000a

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment$2;-><init>(Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment$1;-><init>(Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v1
.end method
