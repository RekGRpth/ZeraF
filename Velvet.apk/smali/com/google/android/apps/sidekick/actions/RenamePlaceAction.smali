.class public Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;
.super Lcom/google/android/apps/sidekick/actions/EntryActionBase;
.source "RenamePlaceAction.java"


# instance fields
.field private final mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Action;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Action;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, p2, v0}, Lcom/google/android/apps/sidekick/actions/EntryActionBase;-><init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;)V

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "rename_place_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
