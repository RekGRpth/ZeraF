.class public Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "RealEstateEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;
    }
.end annotation


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p4    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p6}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method

.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p4    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p6}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method private addListing(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 18
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const v14, 0x7f0400a0

    const/4 v15, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v14, v1, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v3

    const v14, 0x7f1001f3

    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    const/4 v15, 0x0

    invoke-virtual {v3, v15}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddress(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasSubtype()Z

    move-result v14

    if-nez v14, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasPrice()Z

    move-result v14

    if-eqz v14, :cond_6

    :cond_0
    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasSubtype()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getSubtype()Ljava/lang/String;

    move-result-object v11

    :goto_0
    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasPrice()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPrice()Ljava/lang/String;

    move-result-object v6

    :goto_1
    const-string v14, " - "

    invoke-static {v14}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/common/base/Joiner;->skipNulls()Lcom/google/common/base/Joiner;

    move-result-object v14

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-virtual {v14, v6, v11, v15}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const v14, 0x7f1001f4

    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    invoke-virtual {v14, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNumBedroomsAndBathrooms()Z

    move-result v14

    if-nez v14, :cond_1

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLivingArea()Z

    move-result v14

    if-eqz v14, :cond_9

    :cond_1
    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNumBedroomsAndBathrooms()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNumBedroomsAndBathrooms()Ljava/lang/String;

    move-result-object v7

    :goto_3
    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLivingArea()Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getLivingArea()Ljava/lang/String;

    move-result-object v10

    :goto_4
    const-string v14, " - "

    invoke-static {v14}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/common/base/Joiner;->skipNulls()Lcom/google/common/base/Joiner;

    move-result-object v14

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-virtual {v14, v7, v10, v15}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v14, 0x7f1001f5

    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    invoke-virtual {v14, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNextOpenHouseTimestamp()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNextOpenHouseTimestamp()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    mul-long v4, v14, v16

    const v14, 0x7f1001ee

    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    const v14, 0x7f1001ef

    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    const v15, 0x80013

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v15}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9, v3}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->showListingPhoto(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;)V

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasDetailsUrl()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getDetailsUrl()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v14, v0, v1, v2, v13}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;-><init>(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;)V

    invoke-virtual {v9, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v9

    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_6
    const v14, 0x7f1001f4

    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_4

    :cond_9
    const v14, 0x7f1001f5

    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5
.end method

.method private buildMultipleListingCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v9, 0x7f1001f2

    const/4 v8, 0x0

    const v5, 0x7f04009f

    invoke-virtual {p2, v5, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v5, 0x7f100031

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0d02b1

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getTitle()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getIsExpanded()Z

    move-result v2

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/CardTableLayout;

    if-eqz v2, :cond_0

    invoke-direct {p0, p1, v0, v3, p2}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    :goto_0
    move-object v5, v0

    check-cast v5, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setDismissableContainer(Landroid/view/ViewGroup;)V

    return-object v0

    :cond_0
    new-instance v5, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$3;

    invoke-direct {v5, p0, p1, v0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;Landroid/content/Context;Landroid/view/ViewGroup;)V

    invoke-virtual {v3, v5}, Lcom/google/android/apps/sidekick/CardTableLayout;->setAdapter(Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;)V

    new-instance v1, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$4;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;Lcom/google/android/apps/sidekick/CardTableLayout;)V

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private buildSingleListingCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 26
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const v21, 0x7f04009e

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v21

    move-object/from16 v2, p3

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v9

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v21

    const/16 v22, 0x27

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    const v21, 0x7f1001e9

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddressCount()I

    move-result v21

    if-lez v21, :cond_1

    const v21, 0x7f1001ea

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddress(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddressCount()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_2

    const v21, 0x7f1001eb

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddress(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v9}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->showListingPhoto(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;)V

    const v21, 0x7f1001ed

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TableLayout;

    new-instance v20, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    move-object/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/widget/TableLayout;Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$1;)V

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasPrice()Z

    move-result v21

    if-eqz v21, :cond_3

    const v21, 0x7f0d02a4

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPrice()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->access$100(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_3
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLivingArea()Z

    move-result v21

    if-eqz v21, :cond_4

    const v21, 0x7f0d02a5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getLivingArea()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->access$100(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNumBedroomsAndBathrooms()Z

    move-result v21

    if-eqz v21, :cond_5

    const v21, 0x7f0d02a6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNumBedroomsAndBathrooms()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->access$100(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasListingTimestamp()Z

    move-result v21

    if-eqz v21, :cond_6

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getListingTimestamp()J

    move-result-wide v21

    const-wide/16 v23, 0x3e8

    mul-long v10, v21, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v15

    sub-long v21, v15, v10

    const-wide/32 v23, 0x5265c00

    div-long v7, v21, v23

    const-wide/16 v21, 0x0

    cmp-long v21, v7, v21

    if-lez v21, :cond_6

    const v21, 0x7f0d02a7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f0d02a8

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, p1

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->access$100(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_6
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasSubtype()Z

    move-result v21

    if-eqz v21, :cond_7

    const v21, 0x7f0d02a9

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getSubtype()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->access$100(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_7
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasYearBuilt()Z

    move-result v21

    if-eqz v21, :cond_8

    const v21, 0x7f0d02aa

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getYearBuilt()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->access$100(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_8
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNextOpenHouseTimestamp()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNextOpenHouseTimestamp()J

    move-result-wide v21

    const-wide/16 v23, 0x3e8

    mul-long v13, v21, v23

    const v21, 0x7f1001ee

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    const v21, 0x7f1001ef

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    const v22, 0x80013

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v13, v14, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasForSaleSimilarListingsUrl()Z

    move-result v21

    if-eqz v21, :cond_a

    const v21, 0x7f1001f0

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/Button;

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v21, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v9}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasRecentlySoldSimilarListingsUrl()Z

    move-result v21

    if-eqz v21, :cond_b

    const v21, 0x7f1001f1

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v21, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$2;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v9}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    return-object v6
.end method

.method private expandCard(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    const/4 v12, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v12, v2, :cond_0

    const/16 v2, 0xa

    if-gt v12, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/geo/sidekick/Sidekick$Entry;

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-direct {p0, p1, v0, v1, v11}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->addListing(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v13

    invoke-interface {v5, v13, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->addListCardTags(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;)V

    check-cast p2, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    new-instance v2, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v9, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v10, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/util/Map;Landroid/content/Context;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setOnDismissListener(Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;)V

    return-void
.end method

.method private getRealEstateLocation()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddressCount()I

    move-result v1

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddress(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddressCount()I

    move-result v1

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddress(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private showListingPhoto(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    const/4 v7, 0x0

    invoke-virtual {p3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPhotoCount()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {p3, v7}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPhoto(I)Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v0

    const v4, 0x7f1001ec

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0073

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0c0074

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->setImageUrlSmartCrop(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 6

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    const v2, 0x7f0d02f1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getRealEstateLocation()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;I[Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v1
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getRealEstateLocation()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0d02cf

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const v1, 0x7f0d02d0

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->buildSingleListingCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->buildSingleListingCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->buildMultipleListingCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f10005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    :cond_0
    :goto_0
    if-nez v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasDetailsUrl()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getDetailsUrl()Ljava/lang/String;

    move-result-object v2

    const-string v3, "REAL_ESTATE_LEARN_MORE"

    invoke-virtual {p0, p1, v0, v2, v3}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasRealEstateEntry()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
