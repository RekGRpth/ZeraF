.class public Lcom/google/android/apps/sidekick/widget/CurrencyExchangeRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "CurrencyExchangeRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/CurrencyExchangeRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 4
    .param p1    # Landroid/content/Context;

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f040026

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/widget/CurrencyExchangeRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCurrencyExchangeEntry()Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    move-result-object v0

    const v2, 0x7f100083

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-object v1
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 12
    .param p1    # Landroid/content/Context;

    const v11, 0x7f10029d

    const/4 v10, 0x2

    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0400df

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/widget/CurrencyExchangeRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCurrencyExchangeEntry()Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    move-result-object v0

    const v4, 0x7f10029e

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/high16 v4, 0x41200000

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalToHomeRate()F

    move-result v5

    div-float v2, v4, v5

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    invoke-virtual {v1, v10}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d02a2

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v8, 0xa

    invoke-virtual {v1, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getHomeCurrency()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    float-to-double v7, v2

    invoke-virtual {v1, v7, v8}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    const/4 v7, 0x3

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalCurrency()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v11, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090065

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v11, v4}, Landroid/widget/RemoteViews;->setTextColor(II)V

    return-object v3
.end method
