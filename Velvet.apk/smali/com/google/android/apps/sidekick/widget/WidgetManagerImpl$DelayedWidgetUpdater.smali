.class Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;
.super Ljava/lang/Object;
.source "WidgetManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DelayedWidgetUpdater"
.end annotation


# instance fields
.field private mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic this$0:Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;->this$0:Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;->mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;
    .param p2    # Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;-><init>(Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;->mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;->mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;->this$0:Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;

    # invokes: Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->updateWidgetInternal()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->access$100(Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;)V

    :cond_0
    return-void
.end method
