.class public Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "StockQuoteRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mStockListEntryAdapter:Lcom/google/android/apps/sidekick/StockListEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;->mStockListEntryAdapter:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    return-void
.end method

.method private createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const v10, 0x7f10024b

    const v9, 0x7f10024a

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0400be

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;->mStockListEntryAdapter:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getStockQuoteListEntry()Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntry(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    move-result-object v1

    const v3, 0x7f100248

    iget-object v4, p0, Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;->mStockListEntryAdapter:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    invoke-virtual {v4, p1, v1}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getTitle(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastPrice()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f100249

    const-string v4, "%.2f"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastPrice()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariation()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "%.2f"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariation()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariation()F

    move-result v3

    float-to-double v3, v3

    invoke-static {p1, v3, v4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getColorForPriceVariation(Landroid/content/Context;D)I

    move-result v3

    invoke-virtual {v2, v9, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    invoke-virtual {v2, v9, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_1
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariationPercent()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "%.2f%%"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariationPercent()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v10, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariationPercent()F

    move-result v3

    float-to-double v3, v3

    invoke-static {p1, v3, v4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getColorForPriceVariation(Landroid/content/Context;D)I

    move-result v3

    invoke-virtual {v2, v10, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    invoke-virtual {v2, v10, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_2
    return-object v2
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method
