.class public Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;
.super Ljava/lang/Object;
.source "WidgetImageLoader.java"


# instance fields
.field private final mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/apps/sidekick/inject/WidgetManager;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/sidekick/inject/WidgetManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Lcom/google/android/apps/sidekick/inject/WidgetManager;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)Lcom/google/android/apps/sidekick/inject/WidgetManager;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;

    return-object v0
.end method


# virtual methods
.method loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/RemoteViews;
    .param p3    # I
    .param p4    # Landroid/net/Uri;
    .param p5    # Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v5, p4}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->haveNow()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz p5, :cond_1

    iget v5, p5, Landroid/graphics/Rect;->left:I

    iget v6, p5, Landroid/graphics/Rect;->top:I

    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {p5}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-static {v0, v5, v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p2, p3, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :goto_0
    const/4 v5, 0x0

    invoke-virtual {p2, p3, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p2, p3, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    new-instance v4, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader$1;-><init>(Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_1
.end method
