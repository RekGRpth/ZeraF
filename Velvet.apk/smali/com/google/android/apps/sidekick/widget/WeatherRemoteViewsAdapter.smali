.class public Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "WeatherRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

.field private final mWeatherEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/WeatherEntryAdapter;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/WeatherEntryAdapter;
    .param p2    # Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mWeatherEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    return-void
.end method

.method private createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v12, 0x2

    const/4 v5, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0400d6

    invoke-direct {v2, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mWeatherEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWeatherEntry()Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {v6, v10}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v8

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f1000a2

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_2
    const v0, 0x7f100285

    const v1, 0x7f0d0134

    new-array v3, v11, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v3, v10

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mWeatherEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getImageUrl(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    const v3, 0x7f100279

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V

    :cond_3
    if-eqz p2, :cond_0

    const v0, 0x7f100286

    invoke-virtual {v2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointCount()I

    move-result v0

    if-le v0, v11, :cond_5

    invoke-virtual {v6, v11}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f100288

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mWeatherEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getImageUrl(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    const v3, 0x7f100289

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V

    :cond_4
    const v0, 0x7f100287

    invoke-virtual {v2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_5
    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointCount()I

    move-result v0

    if-le v0, v12, :cond_0

    const v0, 0x7f10028a

    invoke-virtual {v2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v6, v12}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f10028c

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mWeatherEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;->getImageUrl(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    const v3, 0x7f10028d

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V

    :cond_6
    const v0, 0x7f10028b

    invoke-virtual {v2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_0
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method
