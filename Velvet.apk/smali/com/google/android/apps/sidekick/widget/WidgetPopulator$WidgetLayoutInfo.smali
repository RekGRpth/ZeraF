.class Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;
.super Ljava/lang/Object;
.source "WidgetPopulator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/widget/WidgetPopulator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WidgetLayoutInfo"
.end annotation


# instance fields
.field public final mIncludePadding:Z

.field public final mRows:I

.field public final mShowHalfWidthCards:Z


# direct methods
.method public constructor <init>(IZZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mRows:I

    iput-boolean p2, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mShowHalfWidthCards:Z

    iput-boolean p3, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mIncludePadding:Z

    return-void
.end method
