.class Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;
.super Ljava/lang/Object;
.source "VisualSearchListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->addPlace(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

.field final synthetic val$clickAction:Lcom/google/geo/sidekick/Sidekick$Action;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field final synthetic val$website:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$website:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$clickAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$website:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->openUrl(Landroid/content/Context;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->val$clickAction:Lcom/google/geo/sidekick/Sidekick$Action;

    const-string v4, "DETAILS"

    iget-object v5, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;->this$0:Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;
    invoke-static {v5}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;)Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v5

    # invokes: Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->recordClickAction(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->access$300(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    return-void
.end method
