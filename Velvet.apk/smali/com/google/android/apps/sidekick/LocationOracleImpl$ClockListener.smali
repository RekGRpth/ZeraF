.class Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Clock$TimeResetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClockListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p2    # Lcom/google/android/apps/sidekick/LocationOracleImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    return-void
.end method


# virtual methods
.method public onTimeReset()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1900(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mPurgeLocationsTask:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3300(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
