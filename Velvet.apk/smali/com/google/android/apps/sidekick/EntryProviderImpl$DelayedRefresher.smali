.class Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;
.super Ljava/lang/Object;
.source "EntryProviderImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/EntryProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DelayedRefresher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;Lcom/google/android/apps/sidekick/EntryProviderImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/EntryProviderImpl;
    .param p2    # Lcom/google/android/apps/sidekick/EntryProviderImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;-><init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$800(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;
    invoke-static {v0, v2}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$802(Lcom/google/android/apps/sidekick/EntryProviderImpl;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$900(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$400(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.USER_REFRESH"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$400(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
