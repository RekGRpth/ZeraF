.class public Lcom/google/android/apps/sidekick/GmmLocationProviderImpl$LocationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GmmLocationProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocationReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "location"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "location"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    # invokes: Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;->gmmLocationProviderFromContext(Landroid/content/Context;)Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;->access$000(Landroid/content/Context;)Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;->setLocation(Landroid/location/Location;)V

    goto :goto_0
.end method
