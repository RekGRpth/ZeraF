.class Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;
.super Ljava/lang/Object;
.source "AbstractPlaceEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->updateActionButtons(Landroid/app/Activity;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

.field final synthetic val$userInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->val$userInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    iget-object v2, v2, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointCount()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointList()Ljava/util/List;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    iget-object v2, v2, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    iget-object v3, v3, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v5

    invoke-interface {v2, v3, v4, v1, v5}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->start(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/util/List;I)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->val$userInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v3, "CARD_BUTTON_PRESS"

    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    const-string v5, "NAVIGATE_ALT"

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
