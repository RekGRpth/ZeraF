.class public Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "LocalAttractionsListEntryAdapter.java"


# instance fields
.field private final mAttractionList:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p3    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p4    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getAttractionListEntry()Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mAttractionList:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mAttractionList:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/View;
    .param p4    # I
    .param p5    # I

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->addAttractions(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->hideBottomDivider(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-object v0
.end method

.method private addAttractions(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/View;
    .param p4    # I
    .param p5    # I

    const v12, 0x7f10015b

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    move/from16 v4, p4

    :goto_0
    move/from16 v0, p5

    if-ge v4, v0, :cond_6

    iget-object v12, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mAttractionList:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;->getAttractionCount()I

    move-result v12

    if-ge v4, v12, :cond_6

    iget-object v12, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mAttractionList:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    invoke-virtual {v12, v4}, Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;->getAttraction(I)Lcom/google/geo/sidekick/Sidekick$Attraction;

    move-result-object v1

    const v12, 0x7f040069

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v7, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v2

    const v12, 0x7f100157

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v8, 0x0

    const v12, 0x7f100158

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasRoute()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasDistanceInMeters()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getDistanceInMeters()I

    move-result v12

    invoke-static {v12}, Lcom/google/android/apps/sidekick/LocationUtilities;->toLocalDistanceUnits(I)D

    move-result-wide v12

    invoke-static {p1, v12, v13}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->formatDistance(Landroid/content/Context;D)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v8, v8, 0x1

    :goto_1
    const v12, 0x7f10015a

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenUntil()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenUntil()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v8, v8, 0x1

    :goto_2
    const v12, 0x7f100159

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    const/4 v12, 0x2

    if-ne v8, v12, :cond_5

    const-string v12, "\u22c5"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getKnownForTermCount()I

    move-result v12

    if-lez v12, :cond_0

    const-string v12, "\u22c5"

    invoke-static {v12}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v12

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getKnownForTermList()Ljava/util/List;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    const v12, 0x7f100033

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v12

    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v12, 0x0

    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto()Z

    move-result v12

    if-eqz v12, :cond_1

    const v12, 0x7f1000ed

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setImageUrl(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v12

    if-eqz v12, :cond_2

    new-instance v12, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;

    invoke-direct {v12, p0, p1, v2}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V

    invoke-virtual {v10, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_3
    const/16 v12, 0x8

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    const/16 v12, 0x8

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_5
    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_6
    return-void
.end method

.method private static formatDistance(Landroid/content/Context;D)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # D

    invoke-static {}, Lcom/google/android/apps/sidekick/LocationUtilities;->getLocalDistanceUnits()Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;->KILOMETERS:Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    if-ne v1, v2, :cond_0

    const v0, 0x7f0d0263

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const v0, 0x7f0d0262

    goto :goto_0
.end method

.method private hideBottomDivider(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f10015b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0236

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0234

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0235

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02de

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/TravelCardsSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x3

    const/4 v4, 0x0

    const v0, 0x7f04006a

    invoke-virtual {p2, v0, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f100031

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d0234

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->addAttractions(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mAttractionList:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;->getAttractionCount()I

    move-result v0

    if-lt v0, v5, :cond_0

    const v0, 0x7f10015c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    new-instance v5, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;

    move-object v6, p0

    move-object v8, p1

    move-object v9, p2

    move-object v10, v3

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/widget/Button;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V

    invoke-virtual {v7, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v4}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-object v3

    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->hideBottomDivider(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
