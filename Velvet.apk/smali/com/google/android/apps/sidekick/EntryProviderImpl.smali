.class public Lcom/google/android/apps/sidekick/EntryProviderImpl;
.super Ljava/lang/Object;
.source "EntryProviderImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/EntryProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private mBackgroundImagePhotos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/inject/BackgroundImage;",
            ">;"
        }
    .end annotation
.end field

.field private mCardListEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation
.end field

.field private mCardListLastRefreshTimeMillis:J

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfiguration:Lcom/google/android/searchcommon/SearchConfig;

.field private mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private mEntryIds:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mIncludesMoreEntries:Z

.field private mLastRefreshLocale:Ljava/util/Locale;

.field private mLastRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private mMainEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation
.end field

.field private mMainEntriesLastRefreshTimeMillis:J

.field private final mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

.field private final mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/util/Clock;Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/SearchConfig;Ljava/util/concurrent/Executor;Lcom/google/android/apps/sidekick/inject/SidekickInjector;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/Clock;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;
    .param p4    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p5    # Lcom/google/android/searchcommon/SearchConfig;
    .param p6    # Ljava/util/concurrent/Executor;
    .param p7    # Lcom/google/android/apps/sidekick/inject/SidekickInjector;
    .param p8    # Ljava/util/concurrent/ScheduledExecutorService;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/sidekick/EntryProviderObservable;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/EntryProviderObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mEntryIds:Lcom/google/common/collect/BiMap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mIncludesMoreEntries:Z

    iput-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mCardListEntries:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mBackgroundImagePhotos:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mConfiguration:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/EntryProviderImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->doInitializeFromStorage()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/EntryProviderImpl;Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;JZLjava/util/Locale;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # J
    .param p5    # Z
    .param p6    # Ljava/util/Locale;

    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->updateFromEntryResponseInternal(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;JZLjava/util/Locale;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/sidekick/EntryProviderImpl;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntriesLastRefreshTimeMillis:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/sidekick/EntryProviderImpl;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;
    .param p1    # Ljava/util/concurrent/ScheduledFuture;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Lcom/google/android/apps/sidekick/inject/SidekickInjector;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/EntryProviderImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    return-object v0
.end method

.method private declared-synchronized appendToMainEntries(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList$Builder;->addAll(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList$Builder;

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList$Builder;->addAll(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList$Builder;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList$Builder;->build()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private doInitializeFromStorage()V
    .locals 4

    new-instance v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->isSavedConfigurationVersionCurrent()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->invalidate()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    const-string v2, "entry_provider"

    new-instance v3, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;-><init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;->readFromEncryptedFile(Ljava/lang/String;Lcom/google/common/base/Function;)V

    goto :goto_0
.end method

.method private getCurrentLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized handleEntriesForInterest(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Interest;)V
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Interest;

    monitor-enter p0

    :try_start_0
    new-instance v2, Lcom/google/android/apps/sidekick/EntryTreeConverter;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getCurrentLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v4

    invoke-direct {v2, v3, p2, v4}, Lcom/google/android/apps/sidekick/EntryTreeConverter;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/EntryItemFactory;)V

    invoke-virtual {v2, p1}, Lcom/google/android/apps/sidekick/EntryTreeConverter;->apply(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasTargetDisplay()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p3}, Lcom/google/geo/sidekick/Sidekick$Interest;->getTargetDisplay()I

    move-result v1

    :goto_0
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mCardListEntries:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mCardListLastRefreshTimeMillis:J

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/EntryProviderObservable;->notifyCardListEntriesRefreshed()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private showNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)Z
    .locals 2
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    const/4 v1, 0x0

    invoke-interface {p1, p2, v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->createNotification(Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->showNotification(Landroid/app/Notification;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized updateFromEntryResponseInternal(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;JZLjava/util/Locale;)V
    .locals 9
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # J
    .param p5    # Z
    .param p6    # Ljava/util/Locale;

    monitor-enter p0

    :try_start_0
    new-instance v6, Lcom/google/android/apps/sidekick/EntryTreeConverter;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getCurrentLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v7

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v8

    invoke-direct {v6, v7, p2, v8}, Lcom/google/android/apps/sidekick/EntryTreeConverter;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/EntryItemFactory;)V

    invoke-virtual {v6, p1}, Lcom/google/android/apps/sidekick/EntryTreeConverter;->apply(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageCount()I

    move-result v6

    if-lez v6, :cond_2

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mBackgroundImagePhotos:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageDescriptorCount()I

    move-result v6

    if-lez v6, :cond_0

    const/4 v2, 0x1

    :goto_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageCount()I

    move-result v6

    if-ge v3, v6, :cond_2

    invoke-virtual {p1, v3}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImage(I)Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v5

    if-eqz v2, :cond_1

    invoke-virtual {p1, v3}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getBackgroundImageDescriptor(I)Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    move-result-object v0

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mBackgroundImagePhotos:Ljava/util/List;

    new-instance v7, Lcom/google/android/apps/sidekick/inject/BackgroundImage;

    invoke-direct {v7, v5, v0}, Lcom/google/android/apps/sidekick/inject/BackgroundImage;-><init>(Lcom/google/geo/sidekick/Sidekick$Photo;Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :cond_2
    :try_start_1
    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;

    invoke-direct {v4}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;-><init>()V

    invoke-virtual {v4, p1}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->visit(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->getEntryKeyMap()Lcom/google/common/collect/BiMap;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mEntryIds:Lcom/google/common/collect/BiMap;

    iput-boolean p5, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mIncludesMoreEntries:Z

    iput-wide p3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntriesLastRefreshTimeMillis:J

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocale:Ljava/util/Locale;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/EntryProviderObservable;->notifyRefreshed()V

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private updateStoredEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/sidekick/EntryTreeVisitor;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    const-string v1, "entry_provider"

    new-instance v2, Lcom/google/android/apps/sidekick/EntryProviderImpl$3;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/sidekick/EntryProviderImpl$3;-><init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;->updateEncryptedFile(Ljava/lang/String;Lcom/google/common/base/Function;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized appendMoreCardEntries(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Landroid/location/Location;)V
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .param p2    # Landroid/location/Location;

    const/4 v2, 0x0

    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-static {p2}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    :cond_0
    new-instance v3, Lcom/google/android/apps/sidekick/EntryTreeConverter;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getCurrentLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v5

    invoke-direct {v3, v4, v2, v5}, Lcom/google/android/apps/sidekick/EntryTreeConverter;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/EntryItemFactory;)V

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/EntryTreeConverter;->apply(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->appendToMainEntries(Ljava/util/List;)V

    new-instance v1, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mEntryIds:Lcom/google/common/collect/BiMap;

    invoke-direct {v1, v3}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;-><init>(Lcom/google/common/collect/BiMap;)V

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->visit(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->getEntryKeyMap()Lcom/google/common/collect/BiMap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mEntryIds:Lcom/google/common/collect/BiMap;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mIncludesMoreEntries:Z

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/sidekick/EntryProviderObservable;->notifyEntriesAdded(Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized cancelDelayedRefresh()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized entriesIncludeMore()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mIncludesMoreEntries:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public findAdapterForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    move-object v1, v6

    :goto_0
    return-object v1

    :cond_1
    new-instance v2, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v2, p1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    new-instance v7, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v7, v0}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v2, v7}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_0

    :cond_4
    move-object v1, v6

    goto :goto_0
.end method

.method public declared-synchronized getBackgroundImagePhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/inject/BackgroundImage;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mBackgroundImagePhotos:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCardListEntries()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mCardListLastRefreshTimeMillis:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mCardListEntries:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getIdForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mEntryIds:Lcom/google/common/collect/BiMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v0, p1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mEntryIds:Lcom/google/common/collect/BiMap;

    invoke-interface {v1}, Lcom/google/common/collect/BiMap;->inverse()Lcom/google/common/collect/BiMap;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getLastRefreshTimeMillis()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntriesLastRefreshTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getStalenessTimeoutMs()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getMarinerStalenessTimeoutMinutes()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getTotalEntryCount()I
    .locals 4

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public handleDismissedEntries(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v5, 0x1

    invoke-interface {v0, v5}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->setDismissed(Z)V

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    if-eqz v3, :cond_3

    new-instance v5, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v5, v3}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V

    new-instance v5, Lcom/google/android/apps/sidekick/EntryRemover;

    invoke-direct {v5, v2}, Lcom/google/android/apps/sidekick/EntryRemover;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->updateStoredEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V

    goto :goto_0
.end method

.method public declared-synchronized hasLocationChangedSignificantlySinceRefresh()Z
    .locals 5

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v3

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-static {v4}, Lcom/google/android/apps/sidekick/LocationUtilities;->sidekickLocationToAndroidLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Landroid/location/Location;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/apps/sidekick/LocationUtilities;->distanceBetween(Landroid/location/Location;Landroid/location/Location;)F

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mConfiguration:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getMarinerMaximumStaleDataRefreshDistanceMeters()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    int-to-float v4, v2

    cmpl-float v4, v1, v4

    if-lez v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public initializeFromStorage()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/sidekick/EntryProviderImpl$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl$1;-><init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public declared-synchronized invalidate()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    const-string v1, "entry_provider"

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;->deleteFile(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntriesLastRefreshTimeMillis:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mCardListLastRefreshTimeMillis:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mCardListEntries:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mBackgroundImagePhotos:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/EntryProviderObservable;->notifyInvalidated()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized invalidateWithDelayedRefresh()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->invalidate()V

    new-instance v0, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl$DelayedRefresher;-><init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;Lcom/google/android/apps/sidekick/EntryProviderImpl$1;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->cancelDelayedRefresh()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x2710

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mDelayedRefresher:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isDataForLocale(Ljava/util/Locale;)Z
    .locals 1
    .param p1    # Ljava/util/Locale;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocale:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isInitializedFromStorage()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public mutateEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/sidekick/EntryTreeVisitor;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    new-instance v5, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v5, v1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p1, v5, v1}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->updateStoredEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V

    return-void
.end method

.method notifyForCards(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Ljava/util/List;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    sget-object v6, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {p1, v6}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    const/4 v4, 0x0

    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->isLowPriorityNotification()Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v4, :cond_1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->showNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)Z

    move-result v4

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->showNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)Z

    goto :goto_0

    :cond_3
    return-void
.end method

.method public registerEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/EntryProviderObserver;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/EntryProviderObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public removeGroupChildEntries(Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;)V
    .locals 11
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v9

    invoke-static {v9}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$Entry;

    if-eqz v2, :cond_0

    new-instance v9, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v9, v2}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-interface {v1, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v3, p1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    new-instance v7, Lcom/google/android/apps/sidekick/ChildEntryRemover;

    invoke-direct {v7, v3, v1}, Lcom/google/android/apps/sidekick/ChildEntryRemover;-><init>(Lcom/google/android/apps/sidekick/ProtoKey;Ljava/util/Collection;)V

    iget-object v9, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v8}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v6

    if-eqz v6, :cond_3

    new-instance v9, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v3, v9}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v7, v6}, Lcom/google/android/apps/sidekick/ChildEntryRemover;->visit(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0, v7}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->updateStoredEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V

    return-void
.end method

.method declared-synchronized setLastRefreshLocationForTest(Landroid/location/Location;)V
    .locals 1
    .param p1    # Landroid/location/Location;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mLastRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method setMainEntries(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntries:Ljava/util/List;

    return-void
.end method

.method public unregisterEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/EntryProviderObserver;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mObservable:Lcom/google/android/apps/sidekick/EntryProviderObservable;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/EntryProviderObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public declared-synchronized updateFromEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Interest;Landroid/location/Location;ZLjava/util/Locale;)V
    .locals 9
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Interest;
    .param p3    # Landroid/location/Location;
    .param p4    # Z
    .param p5    # Ljava/util/Locale;

    monitor-enter p0

    if-eqz p3, :cond_0

    :try_start_0
    invoke-static {p3}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    :goto_0
    if-eqz p2, :cond_1

    invoke-direct {p0, p1, v2, p2}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->handleEntriesForInterest(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Interest;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    if-eqz p3, :cond_3

    new-instance v0, Lcom/google/android/apps/sidekick/EntryProviderData;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/EntryProviderData;-><init>()V

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/sidekick/EntryProviderData;->setLastRefreshMillis(J)Lcom/google/android/apps/sidekick/EntryProviderData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/EntryProviderData;->setEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Lcom/google/android/apps/sidekick/EntryProviderData;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/apps/sidekick/EntryProviderData;->setIncludesMoreCards(Z)Lcom/google/android/apps/sidekick/EntryProviderData;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/EntryProviderData;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryProviderData;

    move-result-object v7

    if-eqz p5, :cond_2

    invoke-virtual {p5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/apps/sidekick/EntryProviderData;->setLocale(Ljava/lang/String;)Lcom/google/android/apps/sidekick/EntryProviderData;

    :cond_2
    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/EntryProviderData;->toByteArray()[B

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    const-string v1, "entry_provider"

    invoke-interface {v0, v1, v8}, Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;->writeToEncryptedFile(Ljava/lang/String;[B)V

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->updateFromEntryResponseInternal(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;JZLjava/util/Locale;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->notifyForCards(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
