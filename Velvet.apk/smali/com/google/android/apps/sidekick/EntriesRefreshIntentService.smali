.class public Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;
.super Landroid/app/IntentService;
.source "EntriesRefreshIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$RefreshReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private mGmmPrecacher:Lcom/google/android/searchcommon/GmmPrecacher;

.field private mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

.field private mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->setIntentRedelivery(Z)V

    return-void
.end method

.method private buildQuery(Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;
    .locals 5
    .param p1    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$ClientUserData;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$ClientUserData;-><init>()V

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->getCalendarDataForRefresh()Ljava/lang/Iterable;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    invoke-virtual {v0, v2}, Lcom/google/geo/sidekick/Sidekick$ClientUserData;->addCalendarData(Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;)Lcom/google/geo/sidekick/Sidekick$ClientUserData;

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/geo/sidekick/Sidekick$ClientUserData;->setSidekickConfigurationHashId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientUserData;

    new-instance v4, Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->setClientUserData(Lcom/google/geo/sidekick/Sidekick$ClientUserData;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v4

    return-object v4
.end method

.method private getInterestToSend(Lcom/google/geo/sidekick/Sidekick$Interest;ZZ)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;
    .param p2    # Z
    .param p3    # Z

    const/4 v2, 0x1

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Interest;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/geo/sidekick/Sidekick$Interest;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setConstraintLevel(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    if-nez p3, :cond_2

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setIncludeStricterConstraintResponses(Z)Lcom/google/geo/sidekick/Sidekick$Interest;

    :cond_1
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Interest;->clear()Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static getRefreshPendingIntent(Landroid/content/Context;Z)Landroid/app/PendingIntent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const/high16 v0, 0x8000000

    :goto_0
    invoke-static {p0, v2, v1, v0}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    const/high16 v0, 0x20000000

    goto :goto_0
.end method

.method private hasPartialFailure(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Z
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasError()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getError()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private initializeDependencies()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    new-instance v0, Lcom/google/android/searchcommon/GmmPrecacher;

    invoke-direct {v0}, Lcom/google/android/searchcommon/GmmPrecacher;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mGmmPrecacher:Lcom/google/android/searchcommon/GmmPrecacher;

    :cond_0
    return-void
.end method

.method private isTimeToRefresh(ZZJ)Z
    .locals 9
    .param p1    # Z
    .param p2    # Z
    .param p3    # J

    const/4 v4, 0x1

    if-eqz p2, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->getMarinerBackgroundRefreshRateLimitMinutes()I

    move-result v5

    int-to-long v5, v5

    const-wide/32 v7, 0xea60

    mul-long v0, v5, v7

    sub-long v5, v2, p3

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(J)J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-gtz v5, :cond_0

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private maybeGetDisabledLocationCard(Z)Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .locals 11
    .param p1    # Z

    const/4 v7, 0x0

    if-eqz p1, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/sidekick/LocationDisabledCardHelper;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/LocationDisabledCardHelper;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocationManager()Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/LocationDisabledCardHelper;->createDisabledLocationCard(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v8

    if-eqz v8, :cond_0

    new-instance v6, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-virtual {v6, v8}, Lcom/google/geo/sidekick/Sidekick$Entry;->setGenericCardEntry(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    new-instance v7, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-direct {v7}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;-><init>()V

    invoke-virtual {v7, v6}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->addEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    const-string v1, "location_service_disabled"

    invoke-interface {v2, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v9, "location_service_disabled"

    const/4 v10, 0x1

    invoke-interface {v1, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private refreshEntries(Lcom/google/geo/sidekick/Sidekick$Interest;ZZ)V
    .locals 11

    const/4 v2, 0x0

    const/4 v10, 0x0

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->cancelDelayedRefresh()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v3, "REFRESH"

    invoke-virtual {v1, v3, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logTimingSometimes(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->blockingUpdateBestLocation()Landroid/location/Location;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v9

    if-nez v9, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v3, :cond_2

    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.google.android.apps.sidekick.STARTING_REFRESH"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_3

    const-string v4, "com.google.android.apps.sidekick.INTEREST"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Interest;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocalBroadcastManager()Lvedroid/support/v4/content/LocalBroadcastManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lvedroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    new-instance v4, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    const-string v1, ""

    invoke-virtual {v4, v9}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->hasWorkingConfigurationFor(Landroid/accounts/Account;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v8, v9}, Lcom/google/android/searchcommon/MarinerOptInSettings;->getSavedConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getHashId()Ljava/lang/String;

    move-result-object v1

    :cond_4
    invoke-direct {p0, v7, v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->buildQuery(Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getInterestToSend(Lcom/google/geo/sidekick/Sidekick$Interest;ZZ)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->addInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    new-instance v5, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v5}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v5, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v1

    invoke-virtual {v4, v9}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->hasWorkingConfigurationFor(Landroid/accounts/Account;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v4, v9}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getSettingsChanges(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$StateChanges;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v1, v4}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setStateChangeQuery(Lcom/google/geo/sidekick/Sidekick$StateChanges;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v4

    if-nez v4, :cond_8

    :cond_6
    sget-object v0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->TAG:Ljava/lang/String;

    const-string v1, "Error sending request to the server"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.REFRESH_SERVER_UNREACHABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_7

    const-string v1, "com.google.android.apps.sidekick.INTEREST"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Interest;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocalBroadcastManager()Lvedroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lvedroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;->timingComplete()V

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v4

    const-string v5, "REFRESH_SUCCESS"

    invoke-virtual {v4, v5, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logInternalActionOncePerDay(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->hasPartialFailure(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->TAG:Ljava/lang/String;

    const-string v4, "Partial entry source failure from the server"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-direct {p0, p3}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->maybeGetDisabledLocationCard(Z)Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v4

    if-lez v4, :cond_a

    invoke-virtual {v1, v10}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v1, v10}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->addChild(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    :cond_a
    if-eqz p3, :cond_c

    invoke-interface {v0, v1, v3}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->appendMoreCardEntries(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Landroid/location/Location;)V

    :goto_2
    invoke-direct {p0, v1, v7}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->updateAnyCalendarData(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->hasConfiguration()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getConfiguration()Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v0

    invoke-interface {v8, v9, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->updateConfigurationForAccount(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;)Z

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasSidekickConfiguration()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNowCardsDisabled()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNowCardsDisabled()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;

    invoke-direct {v2, p0, v8, v9}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;-><init>(Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;Lcom/google/android/searchcommon/MarinerOptInSettings;Landroid/accounts/Account;)V

    invoke-interface {v0, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_b
    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->maybePrecacheMaps(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v0

    if-lez v0, :cond_e

    invoke-virtual {v1, v10}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getCallbackWithInterestCount()I

    move-result v0

    if-lez v0, :cond_e

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$EntryTree;-><init>()V

    invoke-virtual {v1, v10}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getCallbackWithInterestList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;

    invoke-virtual {v3, v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->addCallbackWithInterest(Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    goto :goto_3

    :cond_c
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    move-object v2, p1

    move v4, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->updateFromEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Interest;Landroid/location/Location;ZLjava/util/Locale;)V

    goto/16 :goto_2

    :cond_d
    const-string v0, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_e
    if-nez p3, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getAlarmUtils()Lcom/google/android/apps/sidekick/AlarmUtils;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->registerRefreshAlarm(Landroid/content/Context;Lcom/google/android/apps/sidekick/AlarmUtils;Z)V

    :cond_f
    move-object v0, v6

    goto/16 :goto_1
.end method

.method public static registerRefreshAlarm(Landroid/content/Context;Lcom/google/android/apps/sidekick/AlarmUtils;Z)V
    .locals 6

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Context must be the application context."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getMarinerBackgroundRefreshIntervalMinutes()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long v1, v0, v2

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getRefreshPendingIntent(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.google.android.apps.sidekick.notifications.INITIALIZE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getRefreshPendingIntent(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v3

    const-string v4, "refresh_alarm"

    move-object v0, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/AlarmUtils;->scheduleRepeatingAlarm(JLandroid/app/PendingIntent;Ljava/lang/String;Z)V

    return-void
.end method

.method public static unregisterRefreshAlarm(Landroid/content/Context;Lcom/google/android/apps/sidekick/AlarmUtils;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/sidekick/AlarmUtils;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "Context must be the application context."

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getRefreshPendingIntent(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "refresh_alarm"

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/sidekick/AlarmUtils;->cancel(Landroid/app/PendingIntent;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    :cond_0
    return-void
.end method

.method private updateAnyCalendarData(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .param p2    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-interface {p2, v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->updateWithNewEntryTreeFromServer(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method injectDependeciesForTest(Lcom/google/android/velvet/VelvetApplication;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/apps/sidekick/inject/SidekickInjector;Lcom/google/android/searchcommon/GmmPrecacher;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/VelvetApplication;
    .param p2    # Lcom/google/android/searchcommon/CoreSearchServices;
    .param p3    # Lcom/google/android/apps/sidekick/inject/SidekickInjector;
    .param p4    # Lcom/google/android/searchcommon/GmmPrecacher;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mGmmPrecacher:Lcom/google/android/searchcommon/GmmPrecacher;

    return-void
.end method

.method maybePrecacheMaps(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V
    .locals 11
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPrecacheDirectiveList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;

    const/4 v6, 0x0

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;->hasCentroid()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;->getCentroid()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/sidekick/LocationUtilities;->sidekickLocationToAndroidLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Landroid/location/Location;

    move-result-object v9

    invoke-static {v9}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    :cond_4
    :goto_1
    if-eqz v6, :cond_3

    iget-object v9, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mGmmPrecacher:Lcom/google/android/searchcommon/GmmPrecacher;

    invoke-virtual {v9, p0, v6}, Lcom/google/android/searchcommon/GmmPrecacher;->precache(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;->getBoundsCount()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_4

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;->getBounds(I)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/sidekick/LocationUtilities;->sidekickLocationToAndroidLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Landroid/location/Location;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;->getBounds(I)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/sidekick/LocationUtilities;->sidekickLocationToAndroidLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Landroid/location/Location;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    goto :goto_1

    :cond_6
    return-void
.end method

.method public onCreate()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->initializeDependencies()V

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->maybeRegisterSidekickAlarms()V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Wakelock"
        }
    .end annotation

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v9, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "power"

    invoke-virtual {p0, v9}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PowerManager;

    const/4 v9, 0x1

    const-string v10, "EntriesRefresh_wakelock"

    invoke-virtual {v5, v9, v10}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    :try_start_0
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v9, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v9}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/searchcommon/MarinerOptInSettings;->stopServicesIfUserOptedOut()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v9, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v9}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->isInitializedFromStorage()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_3
    :try_start_2
    const-string v9, "com.google.android.apps.sidekick.INTEREST"

    invoke-static {p1, v9}, Lcom/google/android/apps/sidekick/ProtoUtils;->getInterestFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v2

    const-string v9, "com.google.android.apps.sidekick.MORE"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v9, "com.google.android.apps.sidekick.INCREMENTAL"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v9, "com.google.android.apps.sidekick.USER_REFRESH"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v3

    invoke-direct {p0, v6, v7, v3, v4}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->isTimeToRefresh(ZZJ)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-direct {p0, v2, v6, v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->refreshEntries(Lcom/google/geo/sidekick/Sidekick$Interest;ZZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v9

    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v9
.end method
