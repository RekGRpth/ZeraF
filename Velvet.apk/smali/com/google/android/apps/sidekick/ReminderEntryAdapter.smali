.class public Lcom/google/android/apps/sidekick/ReminderEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "ReminderEntryAdapter.java"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method

.method public static getTriggerMessage(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$ReminderEntry;J)Ljava/lang/CharSequence;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .param p2    # J

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggeringMessage()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTriggeringMessage()Ljava/lang/String;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getTriggerTimeSeconds()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long v4, v6, v8

    sub-long v2, p2, v4

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasResolution()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getResolution()I

    move-result v1

    :goto_1
    const/4 v6, 0x2

    if-ne v1, v6, :cond_4

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasDayPart()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getDayPart()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    :pswitch_0
    const v0, 0x7f0d0313

    goto :goto_2

    :pswitch_1
    const v0, 0x7f0d0314

    goto :goto_2

    :pswitch_2
    const v0, 0x7f0d0315

    goto :goto_2

    :pswitch_3
    const v0, 0x7f0d0316

    goto :goto_2

    :cond_2
    invoke-static {v4, v5, p2, p3}, Lcom/google/android/apps/sidekick/TimeUtilities;->isTomorrow(JJ)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getDayPart()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    :goto_3
    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0d0318

    goto :goto_3

    :pswitch_5
    const v0, 0x7f0d0319

    goto :goto_3

    :pswitch_6
    const v0, 0x7f0d031a

    goto :goto_3

    :pswitch_7
    const v0, 0x7f0d031b

    goto :goto_3

    :cond_3
    invoke-static {v4, v5, p2, p3}, Lcom/google/android/apps/sidekick/TimeUtilities;->wasYesterday(JJ)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getDayPart()I

    move-result v6

    packed-switch v6, :pswitch_data_2

    :goto_4
    if-eqz v0, :cond_4

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    :pswitch_8
    const v0, 0x7f0d031d

    goto :goto_4

    :pswitch_9
    const v0, 0x7f0d031e

    goto :goto_4

    :pswitch_a
    const v0, 0x7f0d031f

    goto :goto_4

    :pswitch_b
    const v0, 0x7f0d0320

    goto :goto_4

    :cond_4
    const/4 v6, 0x3

    if-ne v1, v6, :cond_8

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v6

    if-eqz v6, :cond_5

    const v6, 0x7f0d0305

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    :cond_5
    invoke-static {v4, v5, p2, p3}, Lcom/google/android/apps/sidekick/TimeUtilities;->isTomorrow(JJ)Z

    move-result v6

    if-eqz v6, :cond_6

    const v6, 0x7f0d0317

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    :cond_6
    invoke-static {v4, v5, p2, p3}, Lcom/google/android/apps/sidekick/TimeUtilities;->wasYesterday(JJ)Z

    move-result v6

    if-eqz v6, :cond_7

    const v6, 0x7f0d031c

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    :cond_7
    const/16 v6, 0x12

    invoke-static {p0, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    :cond_8
    const/4 v6, 0x0

    invoke-static {p0, v4, v5, v6}, Lcom/google/android/apps/sidekick/TimeUtilities;->formatDisplayTime(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_0

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static remindersEnabled(Lcom/google/android/searchcommon/SearchConfig;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/SearchConfig;

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d02e7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d02e8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 3

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/Clock;)V

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v2, 0x7f0400a5

    const/4 v3, 0x0

    invoke-virtual {p2, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasReminderMessage()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f100031

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {p1, v2, v3, v4}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->getTriggerMessage(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$ReminderEntry;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const v2, 0x7f1001fd

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    new-instance v0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/ReminderEntryAdapter;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->addClientRunnable(Ljava/lang/Runnable;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
