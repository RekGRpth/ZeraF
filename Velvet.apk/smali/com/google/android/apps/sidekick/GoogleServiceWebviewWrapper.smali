.class public Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;
.super Landroid/app/Activity;
.source "GoogleServiceWebviewWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;
    }
.end annotation


# static fields
.field public static final ALL_URL_PREFIXES:[Ljava/lang/String;

.field public static final GMAIL_URL_PREFIXES:[Ljava/lang/String;

.field private static final SCHEMES_TO_OPEN_IN_AN_APP:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;

.field private mUrlPrefixesStayInWebView:[Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->TAG:Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "*"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->ALL_URL_PREFIXES:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "mail.google."

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->GMAIL_URL_PREFIXES:[Ljava/lang/String;

    const-string v0, "mailto"

    const-string v1, "market"

    const-string v2, "tel"

    invoke-static {v0, v1, v2}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->SCHEMES_TO_OPEN_IN_AN_APP:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->SCHEMES_TO_OPEN_IN_AN_APP:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->sendViewIntent(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUrlPrefixesStayInWebView:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method private sendViewIntent(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x4

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->finish()V

    sget-object v3, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->TAG:Ljava/lang/String;

    const-string v4, "Uri required"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v3, "webview_title"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "webview_title"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    iput-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mTitle:Ljava/lang/String;

    const-string v3, "webview_url_prefixes"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUrlPrefixesStayInWebView:[Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mTitle:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_2
    new-instance v3, Landroid/webkit/WebView;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {v3, p0, v4, v6, v5}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    iput-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    const-string v3, "enable_javascript"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    new-instance v4, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;

    invoke-direct {v4, p0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;-><init>(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->setContentView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v3, ""

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
