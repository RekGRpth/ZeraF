.class public Lcom/google/android/apps/sidekick/DismissableLinearLayout;
.super Landroid/widget/LinearLayout;
.source "DismissableLinearLayout.java"

# interfaces
.implements Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/DismissableLinearLayout$1;,
        Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;,
        Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;
    }
.end annotation


# instance fields
.field private mDismissListener:Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;

.field private mDismissableContainer:Landroid/view/ViewGroup;

.field private mHitRect:Landroid/graphics/Rect;

.field private mIsDragging:Z

.field private mSwipeCallback:Lcom/google/android/velvet/tg/SwipeHelper$Callback;

.field private mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mHitRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mHitRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mHitRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->init()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/DismissableLinearLayout;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mIsDragging:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/DismissableLinearLayout;
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getEventCoordsInDismissableContainer(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/DismissableLinearLayout;II)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/DismissableLinearLayout;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getChildAtXYPosition(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/sidekick/DismissableLinearLayout;)Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissListener:Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;

    return-object v0
.end method

.method private getChildAtXYPosition(II)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-object v3, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return-object v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getEventCoordsInDismissableContainer(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;
    .locals 6
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/MotionEvent;

    const/high16 v4, 0x3f000000

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    add-float/2addr v3, v4

    float-to-int v1, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    add-float/2addr v3, v4

    float-to-int v2, v3

    if-ne p1, p0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    if-nez v3, :cond_0

    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    :goto_0
    return-object v3

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    invoke-virtual {p1, v3, v0}, Landroid/view/ViewGroup;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    :goto_1
    new-instance v3, Landroid/graphics/Point;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p0, v0}, Landroid/view/ViewGroup;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_1
.end method

.method private init()V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    new-instance v4, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/google/android/apps/sidekick/DismissableLinearLayout$SwipeCallback;-><init>(Lcom/google/android/apps/sidekick/DismissableLinearLayout;Lcom/google/android/apps/sidekick/DismissableLinearLayout$1;)V

    iput-object v4, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mSwipeCallback:Lcom/google/android/velvet/tg/SwipeHelper$Callback;

    new-instance v4, Lcom/google/android/velvet/tg/SwipeHelper;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mSwipeCallback:Lcom/google/android/velvet/tg/SwipeHelper$Callback;

    int-to-float v7, v2

    invoke-direct {v4, v5, v6, v1, v7}, Lcom/google/android/velvet/tg/SwipeHelper;-><init>(ILcom/google/android/velvet/tg/SwipeHelper$Callback;FF)V

    iput-object v4, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    iput-object p0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    return-void
.end method


# virtual methods
.method public isDismissableViewAtPosition(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p2    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getEventCoordsInDismissableContainer(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;

    move-result-object v0

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getChildAtXYPosition(II)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mSwipeCallback:Lcom/google/android/velvet/tg/SwipeHelper$Callback;

    invoke-interface {v2, v1}, Lcom/google/android/velvet/tg/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/velvet/tg/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/tg/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDismissableContainer(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissableContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method public setOnDismissListener(Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->mDismissListener:Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;

    return-void
.end method
