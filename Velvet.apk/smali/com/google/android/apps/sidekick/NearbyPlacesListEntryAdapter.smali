.class public Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "NearbyPlacesListEntryAdapter.java"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p4    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p7    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p7}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    iput-object p3, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;)Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getPlaceData(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    return-object v0
.end method

.method private addPlaceViewFromEntry(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, p4}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getPlaceData(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v0

    const v4, 0x7f04007f

    const/4 v5, 0x0

    invoke-virtual {p2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasDisplayName()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f100193

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;

    invoke-direct {v4, p0, p1, v0, p4}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BusinessData;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-static {p1, v3, v0}, Lcom/google/android/apps/sidekick/PlaceDataHelper;->populateBusinessData(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V

    const v4, 0x7f1001ba

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/sidekick/PlaceDataHelper;->populatePlaceReview(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V

    invoke-virtual {p3, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v3
.end method

.method private getPlaceData(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlaceEntry()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "NearbyPlacesListEntryAdapter"

    const-string v5, "Unexepcted Entry without nearby place entry."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "NearbyPlacesListEntryAdapter"

    const-string v5, "Unexepcted nearby place entry without frequent place."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "NearbyPlacesListEntryAdapter"

    const-string v5, "Unexepcted frequent place without place data."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "NearbyPlacesListEntryAdapter"

    const-string v5, "Unexepcted place data without business data."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0
.end method

.method private getSearchForMoreAction(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlacesListEntry()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlacesListEntry()Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;->hasSearchForMore()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;->getSearchForMore()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$3;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v1
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d017a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d017b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02cb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    const-class v0, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, ":android:show_fragment"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v1
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 25
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v5, 0x7f04007e

    const/4 v6, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v5, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    const v5, 0x7f100191

    invoke-virtual {v15, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/sidekick/CardTableLayout;

    new-instance v5, Landroid/animation/LayoutTransition;

    invoke-direct {v5}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v15, v5}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v17

    const v5, 0x7f100031

    invoke-virtual {v15, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual/range {v17 .. v17}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlacesListEntry()Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/geo/sidekick/Sidekick$Entry;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    move-object/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->addPlaceViewFromEntry(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v21

    if-eqz v21, :cond_0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-static {v15, v0, v1}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->addListCardTags(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;)V

    move-object/from16 v24, v15

    check-cast v24, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    new-instance v5, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object/from16 v7, p0

    move-object/from16 v9, p1

    invoke-direct/range {v5 .. v13}, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/util/Map;Landroid/content/Context;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setOnDismissListener(Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getSearchForMoreAction(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    move-result-object v14

    if-eqz v14, :cond_2

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasLabel()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasUri()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getUri()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v23

    if-eqz v23, :cond_2

    const v5, 0x7f100192

    invoke-virtual {v15, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getLabel()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Landroid/content/Context;Landroid/net/Uri;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/sidekick/CardTableLayout;->getShowDividers()I

    move-result v5

    or-int/lit8 v5, v5, 0x4

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/google/android/apps/sidekick/CardTableLayout;->setShowDividers(I)V

    :cond_2
    move-object v5, v15

    check-cast v5, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    const v6, 0x7f100191

    invoke-virtual {v15, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setDismissableContainer(Landroid/view/ViewGroup;)V

    return-object v15
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 5

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlacesListEntry()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlacesListEntry()Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
