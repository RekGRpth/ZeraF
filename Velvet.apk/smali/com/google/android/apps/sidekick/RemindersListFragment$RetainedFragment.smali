.class public Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;
.super Landroid/app/Fragment;
.source "RemindersListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/RemindersListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RetainedFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;
    }
.end annotation


# instance fields
.field private mFetchRemindersTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;"
        }
    .end annotation
.end field

.field private mReminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->updateTargetReminders()V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mReminders:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;
    .param p1    # Landroid/os/AsyncTask;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mFetchRemindersTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method private updateTargetReminders()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/RemindersListFragment;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mReminders:Ljava/util/List;

    # invokes: Lcom/google/android/apps/sidekick/RemindersListFragment;->setReminders(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/RemindersListFragment;->access$200(Lcom/google/android/apps/sidekick/RemindersListFragment;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->setRetainInstance(Z)V

    new-instance v0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;-><init>(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;Lcom/google/android/apps/sidekick/RemindersListFragment$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment$FetchRemindersTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mFetchRemindersTask:Landroid/os/AsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mFetchRemindersTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->mFetchRemindersTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method
