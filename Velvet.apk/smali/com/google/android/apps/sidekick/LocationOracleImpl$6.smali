.class Lcom/google/android/apps/sidekick/LocationOracleImpl$6;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;->requestRecentLocation(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

.field final synthetic val$maxAgeMillis:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iput-wide p2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->val$maxAgeMillis:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->notIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->getNetworkLocation()Landroid/location/Location;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/location/Location;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2200(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    move-result-object v3

    const-string v4, "gps"

    invoke-interface {v3, v4}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v3

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2300(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;Landroid/location/Location;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/LocationQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/LocationQueue;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-wide v3, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->val$maxAgeMillis:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithAndroidProviders()V
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithGmmProvider()V
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    goto :goto_0
.end method
