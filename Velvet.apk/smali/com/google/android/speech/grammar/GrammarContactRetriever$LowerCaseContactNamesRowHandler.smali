.class Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;
.super Ljava/lang/Object;
.source "GrammarContactRetriever.java"

# interfaces
.implements Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/grammar/GrammarContactRetriever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LowerCaseContactNamesRowHandler"
.end annotation


# instance fields
.field private final contactNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;->contactNames:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/speech/grammar/GrammarContactRetriever$1;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/grammar/GrammarContactRetriever$1;

    invoke-direct {p0}, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;

    iget-object v0, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;->contactNames:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public handleCurrentRow(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;->contactNames:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
