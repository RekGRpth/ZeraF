.class public Lcom/google/android/speech/grammar/ContactsGrammarBuilder;
.super Lcom/google/android/speech/grammar/GrammarBuilder;
.source "ContactsGrammarBuilder.java"


# instance fields
.field private final mGrammarContactList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/speech/grammar/GrammarContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/speech/grammar/GrammarBuilder;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->mGrammarContactList:Ljava/util/ArrayList;

    return-void
.end method

.method private calculateWeight()V
    .locals 9

    iget-object v0, p0, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->mGrammarContactList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->mGrammarContactList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/grammar/GrammarContact;

    iget v8, v0, Lcom/google/android/speech/grammar/GrammarContact;->timesContacted:I

    iget-object v0, p0, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->mGrammarContactList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/speech/grammar/GrammarContact;

    iget v0, v6, Lcom/google/android/speech/grammar/GrammarContact;->timesContacted:I

    int-to-double v0, v0

    int-to-double v2, v8

    iget-wide v4, v6, Lcom/google/android/speech/grammar/GrammarContact;->lastTimeContactedElapsed:J

    long-to-double v4, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->getWeight(DDD)D

    move-result-wide v0

    iput-wide v0, v6, Lcom/google/android/speech/grammar/GrammarContact;->weight:D

    goto :goto_0
.end method

.method public static getWeight(DDD)D
    .locals 10
    .param p0    # D
    .param p2    # D
    .param p4    # D

    const-wide/high16 v6, 0x3ff0000000000000L

    add-double/2addr v6, p0

    const-wide/high16 v8, 0x3ff0000000000000L

    add-double/2addr v8, p2

    div-double v2, v6, v8

    const-wide v6, 0x420cf7c580000000L

    invoke-static {p4, p5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide p4

    const-wide/high16 v6, 0x3fe0000000000000L

    const-wide v8, 0x41c9bfcc00000000L

    div-double v8, p4, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v6, 0x3fe0000000000000L

    add-double v8, v0, v2

    mul-double v4, v6, v8

    return-wide v4
.end method


# virtual methods
.method public addContact(Lcom/google/android/speech/grammar/GrammarContact;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/grammar/GrammarContact;

    iget-object v0, p0, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->mGrammarContactList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected appendBeforeDisjunctionRules(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1    # Ljava/lang/StringBuilder;

    const-string v0, "$TARGET = $CONTACT;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "$VOICE_DIALING = $CONTACT_AND_DIGIT_DIALING;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method protected appendDisjunctionAssignment(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1    # Ljava/lang/StringBuilder;

    const-string v0, "$CONTACT = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method protected appendEmptyTokensRules(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1    # Ljava/lang/StringBuilder;

    const-string v0, "$TARGET = $VOID;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "$VOICE_DIALING = $DIGIT_DIALING;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method protected getGrammarTokens()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/speech/grammar/GrammarToken;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->calculateWeight()V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->mGrammarContactList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/grammar/GrammarContact;

    invoke-virtual {v0, v2}, Lcom/google/android/speech/grammar/GrammarContact;->addTokens(Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    return-object v3
.end method
