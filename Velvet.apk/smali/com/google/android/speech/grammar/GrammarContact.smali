.class public Lcom/google/android/speech/grammar/GrammarContact;
.super Ljava/lang/Object;
.source "GrammarContact.java"


# static fields
.field private static final SPACE_JOINER:Lcom/google/common/base/Joiner;


# instance fields
.field final displayName:Ljava/lang/String;

.field final lastTimeContactedElapsed:J

.field final timesContacted:I

.field weight:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, " "

    invoke-static {v0}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v0

    sput-object v0, Lcom/google/android/speech/grammar/GrammarContact;->SPACE_JOINER:Lcom/google/common/base/Joiner;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/speech/grammar/GrammarContact;->displayName:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/speech/grammar/GrammarContact;->timesContacted:I

    iput-wide p3, p0, Lcom/google/android/speech/grammar/GrammarContact;->lastTimeContactedElapsed:J

    return-void
.end method

.method private addToken(Ljava/lang/String;DLjava/util/Map;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "D",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/grammar/GrammarToken;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/grammar/GrammarToken;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/speech/grammar/GrammarToken;->add(D)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/speech/grammar/GrammarToken;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/speech/grammar/GrammarToken;-><init>(Ljava/lang/String;D)V

    invoke-interface {p4, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public addTokens(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/grammar/GrammarToken;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/speech/grammar/GrammarContact;->displayName:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/speech/grammar/GrammarBuilder;->stripAbnfTokens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/speech/grammar/GrammarBuilder;->getWords(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iget-wide v3, p0, Lcom/google/android/speech/grammar/GrammarContact;->weight:D

    invoke-direct {p0, v2, v3, v4, p1}, Lcom/google/android/speech/grammar/GrammarContact;->addToken(Ljava/lang/String;DLjava/util/Map;)V

    array-length v2, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v1, v2

    iget-wide v3, p0, Lcom/google/android/speech/grammar/GrammarContact;->weight:D

    invoke-direct {p0, v2, v3, v4, p1}, Lcom/google/android/speech/grammar/GrammarContact;->addToken(Ljava/lang/String;DLjava/util/Map;)V

    sget-object v2, Lcom/google/android/speech/grammar/GrammarContact;->SPACE_JOINER:Lcom/google/common/base/Joiner;

    invoke-virtual {v2, v1}, Lcom/google/common/base/Joiner;->join([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/speech/grammar/GrammarContact;->weight:D

    invoke-direct {p0, v2, v3, v4, p1}, Lcom/google/android/speech/grammar/GrammarContact;->addToken(Ljava/lang/String;DLjava/util/Map;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GrammarContact["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarContact;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/speech/grammar/GrammarContact;->timesContacted:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",last-time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/speech/grammar/GrammarContact;->lastTimeContactedElapsed:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",weigth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/speech/grammar/GrammarContact;->weight:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
