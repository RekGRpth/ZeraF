.class public Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;
.super Ljava/lang/Object;
.source "HandsFreeGrammarCompiler.java"


# instance fields
.field private final mContactRetriever:Lcom/google/android/speech/grammar/GrammarContactRetriever;

.field private final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private final mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

.field private final mTextGrammarLoader:Lcom/google/android/speech/grammar/TextGrammarLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/grammar/GrammarContactRetriever;Lcom/google/android/speech/grammar/TextGrammarLoader;Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p2    # Lcom/google/android/speech/grammar/GrammarContactRetriever;
    .param p3    # Lcom/google/android/speech/grammar/TextGrammarLoader;
    .param p4    # Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iput-object p2, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mContactRetriever:Lcom/google/android/speech/grammar/GrammarContactRetriever;

    iput-object p3, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mTextGrammarLoader:Lcom/google/android/speech/grammar/TextGrammarLoader;

    iput-object p4, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    return-void
.end method

.method private buildAbnfGrammar(Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;)Ljava/lang/String;
    .locals 11
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    const/4 v7, 0x0

    invoke-interface {p1}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getLanguageMetadata()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v0

    const-string v8, "en-US"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    :try_start_0
    iget-object v8, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mTextGrammarLoader:Lcom/google/android/speech/grammar/TextGrammarLoader;

    iget-object v9, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-virtual {v9, v0}, Lcom/google/android/speech/embedded/Greco3Grammar;->getApkFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/high16 v10, 0x80000

    invoke-virtual {v8, v9, v10}, Lcom/google/android/speech/grammar/TextGrammarLoader;->get(Ljava/lang/String;I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v8, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-virtual {v8}, Lcom/google/android/speech/embedded/Greco3Grammar;->isAddContacts()Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mContactRetriever:Lcom/google/android/speech/grammar/GrammarContactRetriever;

    invoke-virtual {v8}, Lcom/google/android/speech/grammar/GrammarContactRetriever;->getContacts()Ljava/util/List;

    move-result-object v2

    const/16 v8, 0xe

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    new-instance v3, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;

    invoke-direct {v3}, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/grammar/GrammarContact;

    invoke-virtual {v3, v1}, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->addContact(Lcom/google/android/speech/grammar/GrammarContact;)V

    goto :goto_1

    :catch_0
    move-exception v6

    const-string v8, "VS.G3ContactsCompiler"

    const-string v9, "I/O Exception reading ABNF grammar: "

    invoke-static {v8, v9, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v4}, Lcom/google/android/speech/grammar/ContactsGrammarBuilder;->append(Ljava/lang/StringBuilder;)V

    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method private compileGrammar(Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;Ljava/lang/String;Ljava/io/File;)Z
    .locals 4
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    new-instance v0, Lcom/google/android/speech/embedded/Greco3GrammarCompiler;

    sget-object v2, Lcom/google/android/speech/embedded/Greco3Mode;->COMPILER:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-interface {p1, v2}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getResourcePaths()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/speech/embedded/Greco3GrammarCompiler;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3GrammarCompiler;->init()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, v2}, Lcom/google/android/speech/embedded/Greco3GrammarCompiler;->compile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3GrammarCompiler;->delete()V

    goto :goto_0
.end method


# virtual methods
.method public buildGrammar(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v1}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v1, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "VS.G3ContactsCompiler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Grammar compilation failed, no resources for locale :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->buildAbnfGrammar(Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public declared-synchronized compileGrammar(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v3}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v2, Lcom/google/android/searchcommon/util/StopWatch;

    invoke-direct {v2}, Lcom/google/android/searchcommon/util/StopWatch;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/StopWatch;->start()V

    iget-object v3, p0, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v3, p2}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v3, "VS.G3ContactsCompiler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Grammar compilation failed, no resources for locale :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/StopWatch;->start()V

    invoke-direct {p0, v1, p1, p3}, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->compileGrammar(Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;Ljava/lang/String;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v3, "VS.G3ContactsCompiler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Compiled grammar : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/StopWatch;->getElapsedTime()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
