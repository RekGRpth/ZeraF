.class public interface abstract Lcom/google/android/speech/helper/AccountHelper;
.super Ljava/lang/Object;
.source "AccountHelper.java"

# interfaces
.implements Lcom/google/android/speech/helper/AuthTokenHelper;


# virtual methods
.method public abstract getMainGmailAccount(Lcom/google/android/speech/callback/SimpleCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Landroid/accounts/Account;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract hasGoogleAccount()Z
.end method

.method public abstract promptForPermissions(Landroid/app/Activity;)V
.end method
