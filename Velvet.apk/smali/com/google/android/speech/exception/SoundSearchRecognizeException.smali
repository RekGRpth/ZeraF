.class public Lcom/google/android/speech/exception/SoundSearchRecognizeException;
.super Lcom/google/android/speech/exception/RecognizeException;
.source "SoundSearchRecognizeException.java"


# instance fields
.field private final mOriginalException:Lcom/google/android/speech/exception/RecognizeException;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    invoke-direct {p0}, Lcom/google/android/speech/exception/RecognizeException;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/exception/SoundSearchRecognizeException;->mOriginalException:Lcom/google/android/speech/exception/RecognizeException;

    return-void
.end method


# virtual methods
.method public getOriginalException()Lcom/google/android/speech/exception/RecognizeException;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/exception/SoundSearchRecognizeException;->mOriginalException:Lcom/google/android/speech/exception/RecognizeException;

    return-object v0
.end method
