.class public Lcom/google/android/speech/audio/AudioEncoderInputStream;
.super Ljava/io/InputStream;
.source "AudioEncoderInputStream.java"


# instance fields
.field private final mAdtsHeader:Ljava/nio/ByteBuffer;

.field private final mChannels:I

.field private mCodec:Landroid/media/MediaCodec;

.field private mCodecInputBuffers:[Ljava/nio/ByteBuffer;

.field private mCodecOutputBuffers:[Ljava/nio/ByteBuffer;

.field private mCurrentOutputBufferIndex:I

.field private final mDataIn:Ljava/nio/ByteBuffer;

.field private mEof:Z

.field private final mIsAac:Z

.field private final mReadSize:I

.field private final mSampleRate:I

.field private final mStream:Ljava/io/InputStream;

.field private mTotalRead:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;IIII)V
    .locals 6
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCurrentOutputBufferIndex:I

    iput p4, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mReadSize:I

    iput-object p1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mStream:Ljava/io/InputStream;

    iput p3, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mSampleRate:I

    iput p6, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mChannels:I

    const-string v2, "audio/mp4a-latm"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mIsAac:Z

    iget-boolean v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mIsAac:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mSampleRate:I

    const/16 v5, 0x2b11

    if-ne v2, v5, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mChannels:I

    if-ne v2, v3, :cond_2

    :goto_1
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    :cond_0
    iget v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mReadSize:I

    new-array v2, v2, [B

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    iget v3, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mReadSize:I

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v2, 0x7

    new-array v2, v2, [B

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mAdtsHeader:Ljava/nio/ByteBuffer;

    new-instance v1, Landroid/media/MediaFormat;

    invoke-direct {v1}, Landroid/media/MediaFormat;-><init>()V

    const-string v2, "mime"

    invoke-virtual {v1, v2, p2}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "sample-rate"

    iget v3, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mSampleRate:I

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    const-string v2, "bitrate"

    invoke-virtual {v1, v2, p5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    const-string v2, "channel-count"

    iget v3, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mChannels:I

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    iget-boolean v2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mIsAac:Z

    if-eqz v2, :cond_3

    :try_start_0
    const-string v2, "OMX.google.aac.encoder"

    invoke-direct {p0, v2, v1}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->startCodecByName(Ljava/lang/String;Landroid/media/MediaFormat;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-direct {p0, p2, v1}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->startCodecByMimeType(Ljava/lang/String;Landroid/media/MediaFormat;)V

    goto :goto_2

    :cond_3
    invoke-direct {p0, p2, v1}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->startCodecByMimeType(Ljava/lang/String;Landroid/media/MediaFormat;)V

    goto :goto_2
.end method

.method private encodeStream()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v5, 0x2710

    const/4 v0, 0x0

    const/4 v4, -0x1

    iget v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCurrentOutputBufferIndex:I

    if-le v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    iget v3, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCurrentOutputBufferIndex:I

    invoke-virtual {v1, v3, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1, v5, v6}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v2

    if-eq v2, v4, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-direct {p0, v1, v2}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->onInputBufferReady(Landroid/media/MediaCodec;I)V

    :cond_1
    new-instance v8, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v8}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1, v8, v5, v6}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v2

    const/4 v1, -0x2

    if-eq v2, v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-ne v2, v4, :cond_3

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Timed out while dequeuing output buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/4 v0, -0x3

    if-ne v2, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecOutputBuffers:[Ljava/nio/ByteBuffer;

    :cond_4
    :goto_0
    return-void

    :cond_5
    if-eq v2, v4, :cond_4

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    iget v3, v8, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v4, v8, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget-wide v5, v8, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget v7, v8, Landroid/media/MediaCodec$BufferInfo;->flags:I

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->onOutputBufferReady(Landroid/media/MediaCodec;IIIJI)V

    goto :goto_0
.end method

.method private onInputBufferReady(Landroid/media/MediaCodec;I)V
    .locals 10
    .param p1    # Landroid/media/MediaCodec;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecInputBuffers:[Ljava/nio/ByteBuffer;

    aget-object v7, v0, p2

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    :goto_0
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mReadSize:I

    if-ge v0, v1, :cond_1

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mEof:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    iget v3, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mReadSize:I

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    sub-int/2addr v3, v6

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v9

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v7, v0, v1, v9}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/2addr v1, v9

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mStream:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v0, -0x1

    if-ne v8, v0, :cond_2

    :cond_1
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    move-object v0, p1

    move v1, p2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mTotalRead:I

    add-int/2addr v0, v8

    iput v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mTotalRead:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mEof:Z

    const/4 v6, 0x4

    move-object v0, p1

    move v1, p2

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    goto :goto_1
.end method

.method private onOutputBufferReady(Landroid/media/MediaCodec;IIIJI)V
    .locals 2
    .param p1    # Landroid/media/MediaCodec;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # J
    .param p7    # I

    iput p2, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCurrentOutputBufferIndex:I

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mAdtsHeader:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mAdtsHeader:Ljava/nio/ByteBuffer;

    invoke-direct {p0, p4, v1}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->setAdtsHeaderBytes(ILjava/nio/ByteBuffer;)V

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mAdtsHeader:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecOutputBuffers:[Ljava/nio/ByteBuffer;

    aget-object v0, v1, p2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    add-int v1, p3, p4

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    return-void
.end method

.method private release()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    return-void
.end method

.method private setAdtsHeaderBytes(ILjava/nio/ByteBuffer;)V
    .locals 9
    .param p1    # I
    .param p2    # Ljava/nio/ByteBuffer;

    const/4 v8, 0x2

    const-wide/16 v6, 0xff

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/4 v5, 0x7

    if-lt v2, v5, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const-wide/16 v0, 0x0

    const/16 v2, 0xc

    const/16 v5, 0xfff

    invoke-static {v0, v1, v2, v5}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v8, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v3, v3}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v8, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    const/4 v2, 0x4

    const/16 v5, 0xa

    invoke-static {v0, v1, v2, v5}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    const/4 v2, 0x3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    const/16 v2, 0xd

    add-int/lit8 v3, p1, 0x7

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    const/16 v2, 0xb

    const/16 v3, 0x7ff

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    invoke-static {v0, v1, v8, v4}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->writeBits(JII)J

    move-result-wide v0

    const/16 v2, 0x30

    ushr-long v2, v0, v2

    and-long/2addr v2, v6

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x28

    ushr-long v2, v0, v2

    and-long/2addr v2, v6

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    and-long/2addr v2, v6

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x18

    ushr-long v2, v0, v2

    and-long/2addr v2, v6

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x10

    ushr-long v2, v0, v2

    and-long/2addr v2, v6

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x8

    ushr-long v2, v0, v2

    and-long/2addr v2, v6

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    long-to-int v2, v0

    int-to-byte v2, v2

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    return-void

    :cond_0
    move v2, v4

    goto/16 :goto_0
.end method

.method private startAndConfigureCodec(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 6
    .param p1    # Landroid/media/MediaCodec;
    .param p2    # Landroid/media/MediaFormat;

    const/4 v5, 0x0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, p2, v2, v3, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecInputBuffers:[Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecOutputBuffers:[Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Could not create codec"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v0

    iput-object v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    iput-object v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecInputBuffers:[Ljava/nio/ByteBuffer;

    iput-object v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecOutputBuffers:[Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private startCodecByMimeType(Ljava/lang/String;Landroid/media/MediaFormat;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/media/MediaFormat;

    invoke-static {p1}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->startAndConfigureCodec(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V

    return-void
.end method

.method private startCodecByName(Ljava/lang/String;Landroid/media/MediaFormat;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/media/MediaFormat;

    invoke-static {p1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->startAndConfigureCodec(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V

    return-void
.end method

.method private stop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    :cond_0
    return-void
.end method

.method private static writeBits(JII)J
    .locals 6
    .param p0    # J
    .param p2    # I
    .param p3    # I

    const-wide/16 v2, -0x1

    rsub-int/lit8 v4, p2, 0x40

    ushr-long v0, v2, v4

    shl-long v2, p0, p2

    int-to-long v4, p3

    and-long/2addr v4, v0

    or-long/2addr v2, v4

    return-wide v2
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mStream:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    invoke-direct {p0}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->stop()V

    invoke-direct {p0}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->release()V

    return-void
.end method

.method public finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodec:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->close()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no one closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public getTotalRead()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mTotalRead:I

    return v0
.end method

.method public read()I
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Single-byte read not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([B)I
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 7
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, -0x1

    iget-boolean v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mEof:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mDataIn:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    iget v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCurrentOutputBufferIndex:I

    if-eq v5, v4, :cond_0

    iget-object v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecOutputBuffers:[Ljava/nio/ByteBuffer;

    iget v6, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCurrentOutputBufferIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/speech/audio/AudioEncoderInputStream;->encodeStream()V

    :cond_1
    iget-boolean v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mEof:Z

    if-eqz v5, :cond_3

    :goto_1
    return v4

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mAdtsHeader:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mAdtsHeader:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-static {p3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mAdtsHeader:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, p1, p2, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    add-int/2addr p2, v2

    sub-int/2addr p3, v2

    :cond_4
    iget-object v4, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCodecOutputBuffers:[Ljava/nio/ByteBuffer;

    iget v5, p0, Lcom/google/android/speech/audio/AudioEncoderInputStream;->mCurrentOutputBufferIndex:I

    aget-object v0, v4, v5

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-static {p3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v0, p1, p2, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    add-int v4, v3, v2

    goto :goto_1
.end method
