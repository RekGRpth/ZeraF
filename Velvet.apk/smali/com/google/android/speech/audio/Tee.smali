.class public Lcom/google/android/speech/audio/Tee;
.super Ljava/lang/Object;
.source "Tee.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/audio/Tee$TeeSecondaryInputStream;,
        Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;
    }
.end annotation


# instance fields
.field private mBasePos:I

.field private final mBuffer:[B

.field private mBufferBegin:I

.field private mBufferEnd:I

.field private final mDelegate:Ljava/io/InputStream;

.field private mEof:Z

.field private mException:Ljava/io/IOException;

.field private final mKeepSize:I

.field private final mLeader:Ljava/io/InputStream;

.field private final mReadPositions:[I

.field private final mReadSize:I

.field private mStartMark:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;IIII)V
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ge p3, p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput-object p1, p0, Lcom/google/android/speech/audio/Tee;->mDelegate:Ljava/io/InputStream;

    mul-int v0, p4, p2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/speech/audio/Tee;->mBuffer:[B

    mul-int v0, p3, p2

    iput v0, p0, Lcom/google/android/speech/audio/Tee;->mKeepSize:I

    iput v1, p0, Lcom/google/android/speech/audio/Tee;->mBufferBegin:I

    iput v1, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    iput-boolean v1, p0, Lcom/google/android/speech/audio/Tee;->mEof:Z

    iput p2, p0, Lcom/google/android/speech/audio/Tee;->mReadSize:I

    new-array v0, p5, [I

    iput-object v0, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    iget-object v0, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    const v2, 0x7fffffff

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    new-instance v0, Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;

    invoke-direct {v0, p0}, Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;-><init>(Lcom/google/android/speech/audio/Tee;)V

    iput-object v0, p0, Lcom/google/android/speech/audio/Tee;->mLeader:Ljava/io/InputStream;

    iget-object v0, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aput v1, v0, v1

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private doRead(I[BII)V
    .locals 7
    .param p1    # I
    .param p2    # [B
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/google/android/speech/audio/Tee;->mBuffer:[B

    array-length v2, v1

    add-int v0, p1, p4

    if-gt v0, v2, :cond_0

    invoke-static {v1, p1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    return-void

    :cond_0
    array-length v5, v1

    if-lt p1, v5, :cond_1

    sub-int v5, p1, v2

    invoke-static {v1, v5, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_1
    sub-int v3, v2, p1

    sub-int v4, p4, v3

    invoke-static {v1, p1, p2, p3, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v5, 0x0

    add-int v6, p3, v3

    invoke-static {v1, v5, p2, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private findSlowestReaderLocked()I
    .locals 4

    const v1, 0x7fffffff

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aget v2, v3, v0

    if-ge v2, v1, :cond_0

    move v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    if-gt v1, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    return v1

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private readFromDelegate(I)I
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/speech/audio/Tee;->mBuffer:[B

    array-length v0, v3

    if-ge p1, v0, :cond_0

    move v1, p1

    :goto_0
    sub-int v3, v0, v1

    iget v4, p0, Lcom/google/android/speech/audio/Tee;->mReadSize:I

    if-lt v3, v4, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    :try_start_0
    iget-object v3, p0, Lcom/google/android/speech/audio/Tee;->mDelegate:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mBuffer:[B

    iget v5, p0, Lcom/google/android/speech/audio/Tee;->mReadSize:I

    invoke-static {v3, v4, v1, v5}, Lcom/google/common/io/ByteStreams;->read(Ljava/io/InputStream;[BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    return v3

    :cond_0
    sub-int v1, p1, v0

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :catch_0
    move-exception v2

    monitor-enter p0

    :try_start_1
    iput-object v2, p0, Lcom/google/android/speech/audio/Tee;->mException:Ljava/io/IOException;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method private rewindBuffersLocked()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const v7, 0x7fffffff

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aget v4, v4, v5

    iget v6, p0, Lcom/google/android/speech/audio/Tee;->mKeepSize:I

    if-lt v4, v6, :cond_3

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aget v4, v4, v5

    iget v5, p0, Lcom/google/android/speech/audio/Tee;->mKeepSize:I

    sub-int v2, v4, v5

    invoke-direct {p0}, Lcom/google/android/speech/audio/Tee;->findSlowestReaderLocked()I

    move-result v4

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mBuffer:[B

    array-length v0, v4

    iget v4, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    iget v5, p0, Lcom/google/android/speech/audio/Tee;->mReadSize:I

    add-int/2addr v4, v5

    sub-int/2addr v4, v3

    if-gt v4, v0, :cond_6

    iget v4, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I

    if-ge v4, v3, :cond_0

    iput v7, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I

    :cond_0
    if-lt v3, v0, :cond_5

    iget v4, p0, Lcom/google/android/speech/audio/Tee;->mBasePos:I

    add-int/2addr v4, v0

    iput v4, p0, Lcom/google/android/speech/audio/Tee;->mBasePos:I

    iget v4, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I

    if-eq v4, v7, :cond_1

    iget v4, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I

    sub-int/2addr v4, v0

    iput v4, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    array-length v4, v4

    if-eq v1, v4, :cond_4

    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aget v4, v4, v1

    if-eq v4, v7, :cond_2

    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aget v5, v4, v1

    sub-int/2addr v5, v0

    aput v5, v4, v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v4, v5

    goto :goto_0

    :cond_4
    sub-int/2addr v3, v0

    iget v4, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    sub-int/2addr v4, v0

    iput v4, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    :cond_5
    iput v3, p0, Lcom/google/android/speech/audio/Tee;->mBufferBegin:I

    return-void

    :cond_6
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Buffer overflow, no available space."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/speech/audio/Tee;->mException:Ljava/io/IOException;

    iget-object v4, p0, Lcom/google/android/speech/audio/Tee;->mException:Ljava/io/IOException;

    throw v4
.end method


# virtual methods
.method close()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/audio/Tee;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/speech/audio/Tee;->mEof:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "Tee"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException closing audio track: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getLeader()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/audio/Tee;->mLeader:Ljava/io/InputStream;

    return-object v0
.end method

.method readLeader([BII)I
    .locals 12
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, -0x1

    iget-object v9, p0, Lcom/google/android/speech/audio/Tee;->mBuffer:[B

    array-length v2, v9

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v0, -0x1

    const/4 v5, -0x1

    :goto_0
    monitor-enter p0

    :try_start_0
    iget-object v9, p0, Lcom/google/android/speech/audio/Tee;->mException:Ljava/io/IOException;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/speech/audio/Tee;->mException:Ljava/io/IOException;

    throw v9

    :catchall_0
    move-exception v9

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    :cond_0
    :try_start_1
    iget-object v9, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    const/4 v10, 0x0

    aget v7, v9, v10

    const v9, 0x7fffffff

    if-ne v7, v9, :cond_2

    if-eq v5, v11, :cond_1

    sub-int/2addr v8, v4

    monitor-exit p0

    :goto_1
    return v8

    :cond_1
    monitor-exit p0

    goto :goto_1

    :cond_2
    iget v3, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    if-eq v5, v11, :cond_4

    add-int/2addr v3, v5

    iput v3, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    iget v9, p0, Lcom/google/android/speech/audio/Tee;->mReadSize:I

    if-ge v5, v9, :cond_3

    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/speech/audio/Tee;->mEof:Z

    monitor-exit p0

    goto :goto_1

    :cond_3
    const/4 v5, -0x1

    :cond_4
    if-eqz v4, :cond_5

    add-int/2addr v7, v4

    iget-object v9, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    const/4 v10, 0x0

    aput v7, v9, v10

    const/4 v4, 0x0

    :cond_5
    if-ne v8, p3, :cond_6

    monitor-exit p0

    move v8, p3

    goto :goto_1

    :cond_6
    if-ne v3, v7, :cond_8

    iget-boolean v9, p0, Lcom/google/android/speech/audio/Tee;->mEof:Z

    if-eqz v9, :cond_7

    monitor-exit p0

    goto :goto_1

    :cond_7
    iget v9, p0, Lcom/google/android/speech/audio/Tee;->mReadSize:I

    add-int/2addr v9, v3

    iget v10, p0, Lcom/google/android/speech/audio/Tee;->mBufferBegin:I

    sub-int/2addr v9, v10

    if-le v9, v2, :cond_8

    invoke-direct {p0}, Lcom/google/android/speech/audio/Tee;->rewindBuffersLocked()V

    iget-object v9, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    const/4 v10, 0x0

    aget v7, v9, v10

    move v3, v7

    :cond_8
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v3, v7, :cond_9

    invoke-direct {p0, v3}, Lcom/google/android/speech/audio/Tee;->readFromDelegate(I)I

    move-result v5

    add-int/2addr v3, v5

    :cond_9
    sub-int v1, v3, v7

    sub-int v6, p3, v8

    if-ge v1, v6, :cond_a

    move v4, v1

    :goto_2
    add-int v9, p2, v8

    invoke-direct {p0, v7, p1, v9, v4}, Lcom/google/android/speech/audio/Tee;->doRead(I[BII)V

    add-int/2addr v8, v4

    goto :goto_0

    :cond_a
    move v4, v6

    goto :goto_2
.end method

.method readSecondary(I[BII)I
    .locals 10
    .param p1    # I
    .param p2    # [B
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v3, 0x0

    :goto_0
    monitor-enter p0

    :goto_1
    :try_start_0
    iget-object v7, p0, Lcom/google/android/speech/audio/Tee;->mException:Ljava/io/IOException;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/speech/audio/Tee;->mException:Ljava/io/IOException;

    throw v7

    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    :cond_0
    :try_start_1
    iget-object v7, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aget v5, v7, p1

    const v7, 0x7fffffff

    if-ne v5, v7, :cond_1

    const/4 p4, 0x0

    monitor-exit p0

    :goto_2
    return p4

    :cond_1
    if-eqz v3, :cond_2

    add-int/2addr v5, v3

    iget-object v7, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aput v5, v7, p1

    const/4 v3, 0x0

    :cond_2
    if-ne v6, p4, :cond_3

    monitor-exit p0

    goto :goto_2

    :cond_3
    iget v1, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    if-eq v1, v5, :cond_4

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-int v0, v1, v5

    sub-int v4, p4, v6

    if-ge v0, v4, :cond_6

    move v3, v0

    :goto_3
    add-int v7, p3, v6

    invoke-direct {p0, v5, p2, v7, v3}, Lcom/google/android/speech/audio/Tee;->doRead(I[BII)V

    add-int/2addr v6, v3

    goto :goto_0

    :cond_4
    :try_start_2
    iget-boolean v7, p0, Lcom/google/android/speech/audio/Tee;->mEof:Z

    if-eqz v7, :cond_5

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move p4, v6

    goto :goto_2

    :cond_5
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v2

    :try_start_4
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->interrupt()V

    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Interrupted waiting for buffers: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_6
    move v3, v4

    goto :goto_3
.end method

.method declared-synchronized remove(I)V
    .locals 2
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    const v1, 0x7fffffff

    aput v1, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setStartAtDelegatePos(J)V
    .locals 2
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/speech/audio/Tee;->mBasePos:I

    iget v1, p0, Lcom/google/android/speech/audio/Tee;->mBufferBegin:I

    add-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/speech/audio/Tee;->mBasePos:I

    iget v1, p0, Lcom/google/android/speech/audio/Tee;->mBufferEnd:I

    add-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget v0, p0, Lcom/google/android/speech/audio/Tee;->mBasePos:I

    int-to-long v0, v0

    sub-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized split()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const v3, 0x7fffffff

    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I

    if-ne v2, v3, :cond_0

    new-instance v2, Ljava/io/IOException;

    const-string v3, "No splits possible, buffers rewound."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    array-length v2, v2

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    aget v2, v2, v0

    if-eq v2, v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    array-length v2, v2

    if-ne v0, v2, :cond_2

    new-instance v2, Ljava/io/IOException;

    const-string v3, "No splits possible, too many siblings."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    new-instance v1, Lcom/google/android/speech/audio/Tee$TeeSecondaryInputStream;

    invoke-direct {v1, p0, v0}, Lcom/google/android/speech/audio/Tee$TeeSecondaryInputStream;-><init>(Lcom/google/android/speech/audio/Tee;I)V

    iget-object v2, p0, Lcom/google/android/speech/audio/Tee;->mReadPositions:[I

    iget v3, p0, Lcom/google/android/speech/audio/Tee;->mStartMark:I

    aput v3, v2, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method
