.class public Lcom/google/android/speech/alternates/Suggestions;
.super Ljava/lang/Object;
.source "Suggestions.java"


# static fields
.field private static sEasyEditSpanConstructor:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<",
            "Landroid/text/style/EasyEditSpan;",
            ">;"
        }
    .end annotation
.end field

.field private static sEasyEditSpanConstructorCreated:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static declared-synchronized createEasyEditSpan(Landroid/content/Context;Ljava/lang/String;I)Landroid/text/style/EasyEditSpan;
    .locals 7

    const-class v2, Lcom/google/android/speech/alternates/Suggestions;

    monitor-enter v2

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.speech.NOTIFY_TEXT_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/google/android/speech/alternates/SuggestionSpanBroadcastReceiver;

    invoke-virtual {v0, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v3, "com.google.android.speech.REQUEST_ID"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.speech.SEGMENT_ID"

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sget-boolean v3, Lcom/google/android/speech/alternates/Suggestions;->sEasyEditSpanConstructorCreated:Z

    if-nez v3, :cond_0

    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/speech/alternates/Suggestions;->sEasyEditSpanConstructorCreated:Z

    const-class v3, Landroid/text/style/EasyEditSpan;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    sput-object v3, Lcom/google/android/speech/alternates/Suggestions;->sEasyEditSpanConstructor:Ljava/lang/reflect/Constructor;

    :cond_0
    sget-object v3, Lcom/google/android/speech/alternates/Suggestions;->sEasyEditSpanConstructor:Ljava/lang/reflect/Constructor;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/google/android/speech/alternates/Suggestions;->sEasyEditSpanConstructor:Ljava/lang/reflect/Constructor;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/style/EasyEditSpan;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-nez v0, :cond_1

    :try_start_1
    new-instance v0, Landroid/text/style/EasyEditSpan;

    invoke-direct {v0}, Landroid/text/style/EasyEditSpan;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit v2

    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_4
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static getSuggestionSpan(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/google/android/speech/logger/SuggestionLogger;)Ljava/lang/CharSequence;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p5    # Lcom/google/android/speech/logger/SuggestionLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Lcom/google/android/speech/logger/SuggestionLogger;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/speech/alternates/Suggestions;->getSuggestionSpan(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/google/android/speech/logger/SuggestionLogger;Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static getSuggestionSpan(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/google/android/speech/logger/SuggestionLogger;Z)Ljava/lang/CharSequence;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Lcom/google/android/speech/logger/SuggestionLogger;",
            "Z)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const-string v0, "UTF-8"

    invoke-virtual {p3, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8, p3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    if-eqz p6, :cond_0

    invoke-static {p0, p1, p2}, Lcom/google/android/speech/alternates/Suggestions;->createEasyEditSpan(Landroid/content/Context;Ljava/lang/String;I)Landroid/text/style/EasyEditSpan;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    if-eqz p4, :cond_3

    if-eqz p1, :cond_3

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/speech/common/Alternates$AlternateSpan;

    invoke-virtual {v6}, Lcom/google/speech/common/Alternates$AlternateSpan;->getStart()I

    move-result v0

    invoke-static {v7, v0}, Lcom/google/android/speech/alternates/UtfUtils;->getOffsetUtf16([BI)I

    move-result v10

    invoke-virtual {v6}, Lcom/google/speech/common/Alternates$AlternateSpan;->getStart()I

    move-result v0

    invoke-virtual {v6}, Lcom/google/speech/common/Alternates$AlternateSpan;->getLength()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v7, v0}, Lcom/google/android/speech/alternates/UtfUtils;->getOffsetUtf16([BI)I

    move-result v11

    invoke-virtual {v6}, Lcom/google/speech/common/Alternates$AlternateSpan;->getAlternatesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    array-length v0, v3

    if-ge v1, v0, :cond_2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$Alternate;

    invoke-virtual {v0}, Lcom/google/speech/common/Alternates$Alternate;->getText()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/text/style/SuggestionSpan;

    const/4 v2, 0x0

    const/4 v4, 0x1

    const-class v5, Lcom/google/android/speech/alternates/SuggestionSpanBroadcastReceiver;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/text/style/SuggestionSpan;-><init>(Landroid/content/Context;Ljava/util/Locale;[Ljava/lang/String;ILjava/lang/Class;)V

    const/16 v1, 0x21

    invoke-virtual {v8, v0, v10, v11, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    if-eqz p5, :cond_1

    invoke-virtual {v0}, Landroid/text/style/SuggestionSpan;->hashCode()I

    move-result v1

    invoke-virtual {v6}, Lcom/google/speech/common/Alternates$AlternateSpan;->getStart()I

    move-result v4

    invoke-virtual {v6}, Lcom/google/speech/common/Alternates$AlternateSpan;->getLength()I

    move-result v5

    move-object/from16 v0, p5

    move-object v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/speech/logger/SuggestionLogger;->addSuggestion(ILjava/lang/String;III)V

    goto :goto_0

    :cond_3
    return-object v8
.end method

.method public static getSuggestionSpanForSearchBox(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/speech/logger/SuggestionLogger;)Ljava/lang/CharSequence;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/speech/logger/SuggestionLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/logger/SuggestionLogger;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move v6, v2

    invoke-static/range {v0 .. v6}, Lcom/google/android/speech/alternates/Suggestions;->getSuggestionSpan(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/google/android/speech/logger/SuggestionLogger;Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
