.class public Lcom/google/android/speech/params/RecognizerParamsBuilder;
.super Ljava/lang/Object;
.source "RecognizerParamsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/params/RecognizerParamsBuilder$5;
    }
.end annotation


# instance fields
.field private final mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

.field private mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

.field private mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

.field private mLocationOverride:Landroid/location/Location;

.field private mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field private mNoSpeechDetectedEnabled:Z

.field private mPlayBeepEnabled:Z

.field private mRecognitionContext:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

.field private final mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

.field private mRecordedAudio:Z

.field private final mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

.field private mSamplingRate:I

.field private mSoundSearchTtsEnabled:Z

.field private mSpokenBcp47Locale:Ljava/lang/String;

.field private mStreamRewindTimeUsec:J

.field private mSuggestionEnabled:Z

.field private mTriggerApplication:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/params/RecognizerBuilderData;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/params/RecognizerBuilderData;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSuggestionEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mPlayBeepEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mNoSpeechDetectedEnabled:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSoundSearchTtsEnabled:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mStreamRewindTimeUsec:J

    const/16 v0, 0x1f40

    iput v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSamplingRate:I

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    new-instance v0, Lcom/google/speech/s3/S3$S3AudioInfo;

    invoke-direct {v0}, Lcom/google/speech/s3/S3$S3AudioInfo;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3AudioInfo;->setEncoding(I)Lcom/google/speech/s3/S3$S3AudioInfo;

    move-result-object v0

    iget v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSamplingRate:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3AudioInfo;->setSampleRateHz(F)Lcom/google/speech/s3/S3$S3AudioInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->ENDPOINTER_VOICESEARCH:Lcom/google/android/speech/embedded/Greco3Mode;

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSuggestionEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mPlayBeepEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mNoSpeechDetectedEnabled:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSoundSearchTtsEnabled:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mStreamRewindTimeUsec:J

    const/16 v0, 0x1f40

    iput v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSamplingRate:I

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getBuildParams()Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    iput-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    iput-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mTriggerApplication:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getGreco3Grammar()Lcom/google/android/speech/embedded/Greco3Grammar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/embedded/Greco3Mode;
    .locals 1
    .param p0    # Lcom/google/android/speech/params/RecognizerParamsBuilder;

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;
    .locals 1
    .param p0    # Lcom/google/android/speech/params/RecognizerParamsBuilder;

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Z
    .locals 1
    .param p0    # Lcom/google/android/speech/params/RecognizerParamsBuilder;

    invoke-direct {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->isSoundSearchEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerParams$Mode;
    .locals 1
    .param p0    # Lcom/google/android/speech/params/RecognizerParamsBuilder;

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Z
    .locals 1
    .param p0    # Lcom/google/android/speech/params/RecognizerParamsBuilder;

    iget-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mNoSpeechDetectedEnabled:Z

    return v0
.end method

.method private createNewRequestId()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/params/RecognizerParamsBuilder$4;

    invoke-direct {v0, p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder$4;-><init>(Lcom/google/android/speech/params/RecognizerParamsBuilder;)V

    invoke-static {v0}, Lcom/google/common/base/Suppliers;->memoize(Lcom/google/common/base/Supplier;)Lcom/google/common/base/Supplier;

    move-result-object v0

    return-object v0
.end method

.method private getApplicationId()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/google/android/speech/params/RecognizerParamsBuilder$5;->$SwitchMap$com$google$android$speech$params$RecognizerParams$Mode:[I

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerParams$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string v0, "intent-api"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "service-api"

    goto :goto_0

    :pswitch_2
    const-string v0, "voice-search"

    goto :goto_0

    :pswitch_3
    const-string v0, "voice-ime"

    goto :goto_0

    :pswitch_4
    const-string v0, "hands-free"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private getAudioInputParams()Lcom/google/android/speech/params/AudioInputParams;
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/speech/params/AudioInputParams$Builder;

    invoke-direct {v0}, Lcom/google/android/speech/params/AudioInputParams$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v4, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v1, v4, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/AudioInputParams$Builder;->setNoiseSuppressionEnabled(Z)Lcom/google/android/speech/params/AudioInputParams$Builder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->isPlayBeepEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/speech/params/AudioInputParams$Builder;->setPlayBeepEnabled(Z)Lcom/google/android/speech/params/AudioInputParams$Builder;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v5, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v4, v5, :cond_1

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/android/speech/params/AudioInputParams$Builder;->setStoreCompleteAudio(Z)Lcom/google/android/speech/params/AudioInputParams$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSamplingRate:I

    invoke-virtual {v1, v2}, Lcom/google/android/speech/params/AudioInputParams$Builder;->setSamplingRate(I)Lcom/google/android/speech/params/AudioInputParams$Builder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mStreamRewindTimeUsec:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/speech/params/AudioInputParams$Builder;->setStreamRewindTimeUsec(J)Lcom/google/android/speech/params/AudioInputParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/AudioInputParams$Builder;->build()Lcom/google/android/speech/params/AudioInputParams;

    move-result-object v1

    return-object v1

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private getEmbeddedRecognizerFallbackTimeout()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;

    invoke-direct {v0, p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;-><init>(Lcom/google/android/speech/params/RecognizerParamsBuilder;)V

    return-object v0
.end method

.method private getEmbeddedRecognizerParams()Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 5

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/speech/network/request/RecognizerSessionParamsBuilderTask;

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v4, Lcom/google/android/speech/params/RecognizerParams$Mode;->DICTATION:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v1, v4, :cond_0

    iget-boolean v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSuggestionEnabled:Z

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v4, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v1, v4, :cond_2

    :cond_1
    move v1, v2

    :goto_0
    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/speech/network/request/RecognizerSessionParamsBuilderTask;-><init>(Lcom/google/android/speech/SpeechSettings;ZZ)V

    invoke-virtual {v0}, Lcom/google/android/speech/network/request/RecognizerSessionParamsBuilderTask;->call()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getEndpointerParams()Lcom/google/common/base/Supplier;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;

    invoke-direct {v0, p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;-><init>(Lcom/google/android/speech/params/RecognizerParamsBuilder;)V

    goto :goto_0
.end method

.method private getMajelClientInfoFuture()Ljava/util/concurrent/Future;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/Majel$MajelClientInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v2}, Lcom/google/android/speech/params/RecognizerBuilderData;->getDeviceParams()Lcom/google/android/speech/params/DeviceParams;

    move-result-object v2

    new-instance v3, Lcom/google/android/speech/params/RecognizerParamsBuilder$2;

    invoke-direct {v3, p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder$2;-><init>(Lcom/google/android/speech/params/RecognizerParamsBuilder;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;-><init>(Lcom/google/android/speech/params/DeviceParams;Lcom/google/common/base/Supplier;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method private getMobileUserInfoFuture()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/MobileUser$MobileUserInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v2}, Lcom/google/android/speech/params/RecognizerBuilderData;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;-><init>(Lcom/google/android/speech/utils/NetworkInformation;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPinholeParamsFuture(Lcom/google/common/base/Supplier;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/PinholeStream$PinholeParams;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getPinholeParamsBuilder()Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->getPinholeParamsCallable(Lcom/google/common/base/Supplier;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    goto :goto_0
.end method

.method private getRecognizerInfo()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 9

    const/4 v1, 0x0

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v8, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v2, v8, :cond_0

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v8, Lcom/google/android/speech/params/RecognizerParams$Mode;->SERVICE_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v2, v8, :cond_0

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v8, Lcom/google/android/speech/params/RecognizerParams$Mode;->INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v2, v8, :cond_3

    :cond_0
    move v4, v3

    :goto_0
    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v8, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v2, v8, :cond_1

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v8, Lcom/google/android/speech/params/RecognizerParams$Mode;->DICTATION:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v2, v8, :cond_4

    iget-boolean v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSuggestionEnabled:Z

    if-eqz v2, :cond_4

    :cond_1
    move v5, v3

    :goto_1
    const/4 v6, 0x5

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasIntentApi()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getIntentApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;->hasMaxNbest()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getIntentApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;->getMaxNbest()I

    move-result v6

    :cond_2
    new-instance v0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognitionContext:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v2}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;-><init>(Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;Lcom/google/android/speech/SpeechSettings;ZZZI)V

    invoke-virtual {v0}, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->call()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    return-object v1

    :cond_3
    move v4, v1

    goto :goto_0

    :cond_4
    move v5, v1

    goto :goto_1
.end method

.method private getS3AudioInfo()Lcom/google/speech/s3/S3$S3AudioInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    goto :goto_0
.end method

.method private getS3ClientInfoFuture()Ljava/util/concurrent/Future;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3ClientInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    new-instance v0, Lcom/google/android/speech/network/request/S3ClientInfoBuilderTask;

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getApplicationId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v3}, Lcom/google/android/speech/params/RecognizerBuilderData;->getDeviceParams()Lcom/google/android/speech/params/DeviceParams;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mTriggerApplication:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v5}, Lcom/google/android/speech/params/RecognizerBuilderData;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/network/request/S3ClientInfoBuilderTask;-><init>(Lcom/google/android/speech/SpeechSettings;Ljava/lang/String;Lcom/google/android/speech/params/DeviceParams;Ljava/lang/String;Landroid/view/WindowManager;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getS3UserInfoFuture()Ljava/util/concurrent/Future;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v2}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v3}, Lcom/google/android/speech/params/RecognizerBuilderData;->getLocationHelper()Lcom/google/android/speech/helper/SpeechLocationHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSpokenBcp47Locale:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mLocationOverride:Landroid/location/Location;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/speech/network/request/S3UserInfoBuilderTask;->getBuilder(Lcom/google/android/speech/helper/AuthTokenHelper;Lcom/google/android/speech/SpeechSettings;Lcom/google/android/speech/helper/SpeechLocationHelper;Ljava/lang/String;Landroid/location/Location;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method private getService()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/google/android/speech/params/RecognizerParamsBuilder$5;->$SwitchMap$com$google$android$speech$params$RecognizerParams$Mode:[I

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerParams$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string v0, "recognizer"

    :goto_0
    return-object v0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getVoiceActionsService()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const-string v0, "voicesearch"

    goto :goto_0

    :pswitch_3
    const-string v0, "voicesearch-text"

    goto :goto_0

    :pswitch_4
    const-string v0, "recognizer"

    goto :goto_0

    :pswitch_5
    const-string v0, "visualsearch"

    goto :goto_0

    :pswitch_6
    const-string v0, "sound-search"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private getSoundSearchInfoFuture()Ljava/util/concurrent/Future;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;

    iget-boolean v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSoundSearchTtsEnabled:Z

    iget-object v3, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v3}, Lcom/google/android/speech/params/RecognizerBuilderData;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v4}, Lcom/google/android/speech/params/RecognizerBuilderData;->getDeviceParams()Lcom/google/android/speech/params/DeviceParams;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;-><init>(ZLcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/speech/params/DeviceParams;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTimeoutTimestamp()J
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private getVoiceActionsService()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSearchConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getS3ServiceVoiceActionsSingleRequest()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "voicesearch"

    goto :goto_0
.end method

.method private isBlacklistedSoundSearchDevice()Z
    .locals 5

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v4}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;->getBlacklistedDevicesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isPlayBeepEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mPlayBeepEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSoundSearchEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSearchConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getSoundSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->isBlacklistedSoundSearchDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rebuild()Lcom/google/android/speech/params/RecognizerParams;
    .locals 30

    new-instance v2, Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v3}, Lcom/google/android/speech/params/RecognizerParams;->getBuildParams()Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v4}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v5}, Lcom/google/android/speech/params/RecognizerParams;->getService()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognizerParams;->getSpokenLanguage()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v7}, Lcom/google/android/speech/params/RecognizerParams;->waitForS3ClientInfo()Lcom/google/speech/s3/S3$S3ClientInfo;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v8}, Lcom/google/android/speech/params/RecognizerParams;->waitForS3UserInfo()Lcom/google/speech/s3/S3$S3UserInfo;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->refreshAuthTokensForS3UserInfo(Lcom/google/speech/s3/S3$S3UserInfo;)Ljava/util/concurrent/Future;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v9}, Lcom/google/android/speech/params/RecognizerParams;->waitForMobileUserInfo()Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    move-result-object v9

    invoke-static {v9}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v10}, Lcom/google/android/speech/params/RecognizerParams;->getS3AudioInfo()Lcom/google/speech/s3/S3$S3AudioInfo;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognizerParams;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v11

    invoke-static {v11}, Lcom/google/common/base/Suppliers;->ofInstance(Ljava/lang/Object;)Lcom/google/common/base/Supplier;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v12}, Lcom/google/android/speech/params/RecognizerParams;->getS3RecognizerInfo()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v13}, Lcom/google/android/speech/params/RecognizerParams;->waitForMajelClientInfo()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v13

    invoke-static {v13}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v14}, Lcom/google/android/speech/params/RecognizerParams;->waitForSoundSearchInfo()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    move-result-object v14

    invoke-static {v14}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v15}, Lcom/google/android/speech/params/RecognizerParams;->waitForPinholeParams()Lcom/google/speech/s3/PinholeStream$PinholeParams;

    move-result-object v15

    invoke-static {v15}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v15

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getTimeoutTimestamp()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/speech/params/RecognizerParams;->getGreco3Grammar()Lcom/google/android/speech/embedded/Greco3Grammar;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/speech/params/RecognizerParams;->getGreco3Mode()Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/speech/params/RecognizerParams;->getRequestIdSupplierForRebuild()Lcom/google/common/base/Supplier;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/speech/params/RecognizerParams;->isRecordedAudio()Z

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/speech/params/RecognizerParams;->getEmbeddedRecognizerParams()Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/speech/params/RecognizerParams;->getEmbeddedRecognizerFallbackTimeout()J

    move-result-wide v23

    move-wide/from16 v0, v23

    long-to-int v0, v0

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/google/common/base/Suppliers;->ofInstance(Ljava/lang/Object;)Lcom/google/common/base/Supplier;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMergerStrategy()Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/speech/params/RecognizerParams;->getAudioInputParams()Lcom/google/android/speech/params/AudioInputParams;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/speech/params/RecognizerParams;->getLocationOverride()Landroid/location/Location;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/speech/params/RecognizerParams;->getTriggerApplication()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/speech/params/RecognizerParams;->getApplicationId()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/speech/params/RecognizerParams;->isSoundSearchTtsEnabled()Z

    move-result v29

    invoke-direct/range {v2 .. v29}, Lcom/google/android/speech/params/RecognizerParams;-><init>(Lcom/google/android/speech/params/RecognizerBuilderData;Lcom/google/android/speech/params/RecognizerParams$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/google/speech/s3/S3$S3AudioInfo;Lcom/google/common/base/Supplier;Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;JLcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/common/base/Supplier;ZLcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;Lcom/google/android/speech/params/AudioInputParams;Landroid/location/Location;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v2
.end method

.method private refreshAuthTokensForS3UserInfo(Lcom/google/speech/s3/S3$S3UserInfo;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/google/speech/s3/S3$S3UserInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const-string v0, "RecognizerParamsBuilder"

    const-string v1, "Attempting refreshed with failed user info."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getS3UserInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/speech/network/request/S3UserInfoBuilderTask;->getAuthTokenRefreshingBuilder(Lcom/google/android/speech/helper/AuthTokenHelper;Lcom/google/speech/s3/S3$S3UserInfo;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/google/android/speech/params/RecognizerParams;
    .locals 30

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSpokenBcp47Locale:Ljava/lang/String;

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    sget-object v4, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne v3, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    if-eqz v3, :cond_3

    :cond_0
    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v4, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v3, v4, :cond_4

    new-instance v2, Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getService()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSpokenBcp47Locale:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEndpointerParams()Lcom/google/common/base/Supplier;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getTimeoutTimestamp()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    move-object/from16 v19, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->createNewRequestId()Lcom/google/common/base/Supplier;

    move-result-object v20

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecordedAudio:Z

    move/from16 v21, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEmbeddedRecognizerParams()Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    move-result-object v22

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEmbeddedRecognizerFallbackTimeout()Lcom/google/common/base/Supplier;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMergerStrategy()Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    move-result-object v24

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getAudioInputParams()Lcom/google/android/speech/params/AudioInputParams;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mLocationOverride:Landroid/location/Location;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mTriggerApplication:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getApplicationId()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSoundSearchTtsEnabled:Z

    move/from16 v29, v0

    invoke-direct/range {v2 .. v29}, Lcom/google/android/speech/params/RecognizerParams;-><init>(Lcom/google/android/speech/params/RecognizerBuilderData;Lcom/google/android/speech/params/RecognizerParams$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/google/speech/s3/S3$S3AudioInfo;Lcom/google/common/base/Supplier;Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;JLcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/common/base/Supplier;ZLcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;Lcom/google/android/speech/params/AudioInputParams;Landroid/location/Location;Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_3
    return-object v2

    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v4, Lcom/google/android/speech/params/RecognizerParams$Mode;->GOGGLES:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v3, v4, :cond_5

    new-instance v2, Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getService()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSpokenBcp47Locale:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getS3ClientInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v7

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getS3UserInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v8

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMobileUserInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/google/common/base/Suppliers;->ofInstance(Ljava/lang/Object;)Lcom/google/common/base/Supplier;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMajelClientInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v14}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getTimeoutTimestamp()J

    move-result-wide v16

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->createNewRequestId()Lcom/google/common/base/Supplier;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/google/common/base/Suppliers;->ofInstance(Ljava/lang/Object;)Lcom/google/common/base/Supplier;

    move-result-object v23

    const/16 v24, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getAudioInputParams()Lcom/google/android/speech/params/AudioInputParams;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mLocationOverride:Landroid/location/Location;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mTriggerApplication:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getApplicationId()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSoundSearchTtsEnabled:Z

    move/from16 v29, v0

    invoke-direct/range {v2 .. v29}, Lcom/google/android/speech/params/RecognizerParams;-><init>(Lcom/google/android/speech/params/RecognizerBuilderData;Lcom/google/android/speech/params/RecognizerParams$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/google/speech/s3/S3$S3AudioInfo;Lcom/google/common/base/Supplier;Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;JLcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/common/base/Supplier;ZLcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;Lcom/google/android/speech/params/AudioInputParams;Landroid/location/Location;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3

    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->createNewRequestId()Lcom/google/common/base/Supplier;

    move-result-object v20

    new-instance v2, Lcom/google/android/speech/params/RecognizerParams;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getService()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSpokenBcp47Locale:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getS3ClientInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v7

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getS3UserInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v8

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMobileUserInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v9

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getS3AudioInfo()Lcom/google/speech/s3/S3$S3AudioInfo;

    move-result-object v10

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEndpointerParams()Lcom/google/common/base/Supplier;

    move-result-object v11

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getRecognizerInfo()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    move-result-object v12

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMajelClientInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v13

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getSoundSearchInfoFuture()Ljava/util/concurrent/Future;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getPinholeParamsFuture(Lcom/google/common/base/Supplier;)Ljava/util/concurrent/Future;

    move-result-object v15

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getTimeoutTimestamp()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecordedAudio:Z

    move/from16 v21, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEmbeddedRecognizerParams()Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    move-result-object v22

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEmbeddedRecognizerFallbackTimeout()Lcom/google/common/base/Supplier;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMergerStrategy()Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    move-result-object v24

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getAudioInputParams()Lcom/google/android/speech/params/AudioInputParams;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mLocationOverride:Landroid/location/Location;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mTriggerApplication:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->getApplicationId()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSoundSearchTtsEnabled:Z

    move/from16 v29, v0

    invoke-direct/range {v2 .. v29}, Lcom/google/android/speech/params/RecognizerParams;-><init>(Lcom/google/android/speech/params/RecognizerBuilderData;Lcom/google/android/speech/params/RecognizerParams$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/google/speech/s3/S3$S3AudioInfo;Lcom/google/common/base/Supplier;Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;JLcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/common/base/Supplier;ZLcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;Lcom/google/android/speech/params/AudioInputParams;Landroid/location/Location;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->rebuild()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public getMergerStrategy()Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;
    .locals 2

    sget-object v0, Lcom/google/android/speech/params/RecognizerParamsBuilder$5;->$SwitchMap$com$google$android$speech$params$RecognizerParams$Mode:[I

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerParams$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;->PREFER_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;->EMBEDDED_ONLY:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;->EMBEDDED_IGNORE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;->EMBEDDED_MERGE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setGrammarType(Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGreco3Mode(Lcom/google/android/speech/embedded/Greco3Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Mode;

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLocationOverride(Landroid/location/Location;)V
    .locals 1
    .param p1    # Landroid/location/Location;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mLocationOverride:Landroid/location/Location;

    return-void
.end method

.method public setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 1
    .param p1    # Lcom/google/android/speech/params/RecognizerParams$Mode;

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNoSpeechDetected(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mNoSpeechDetectedEnabled:Z

    return-object p0
.end method

.method public setPlayBeep(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-boolean p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mPlayBeepEnabled:Z

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRecognitionContext(Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 1
    .param p1    # Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognitionContext:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRecordedAudio(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecordedAudio:Z

    return-object p0
.end method

.method public setSamplingRate(I)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSamplingRate:I

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    iget v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSamplingRate:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3AudioInfo;->setSampleRateHz(F)Lcom/google/speech/s3/S3$S3AudioInfo;

    return-object p0
.end method

.method public setSoundSearchTtsEnabled(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSoundSearchTtsEnabled:Z

    return-object p0
.end method

.method public setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x5f

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    :goto_1
    const-string v0, "This is not a bcp47 locale"

    invoke-static {v1, v0}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSpokenBcp47Locale:Ljava/lang/String;

    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public setSuggestionEnabled(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mSuggestionEnabled:Z

    return-object p0
.end method

.method public setTriggerApplicationId(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder;->mTriggerApplication:Ljava/lang/String;

    return-object p0
.end method
