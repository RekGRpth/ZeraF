.class public final enum Lcom/google/android/speech/params/RecognizerParams$Mode;
.super Ljava/lang/Enum;
.source "RecognizerParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/params/RecognizerParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/speech/params/RecognizerParams$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum DICTATION:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum GOGGLES:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum HANDS_FREE_COMMANDS:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum HANDS_FREE_CONTACTS:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum SERVICE_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field public static final enum VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "INTENT_API"

    invoke-direct {v0, v1, v3}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "SERVICE_API"

    invoke-direct {v0, v1, v4}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->SERVICE_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "VOICE_ACTIONS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "TEXT_ACTIONS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "DICTATION"

    invoke-direct {v0, v1, v7}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->DICTATION:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "HANDS_FREE_CONTACTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_CONTACTS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "HANDS_FREE_COMMANDS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "HOTWORD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "GOGGLES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->GOGGLES:Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    const-string v1, "SOUND_SEARCH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/params/RecognizerParams$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SERVICE_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->DICTATION:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_CONTACTS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->GOGGLES:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->$VALUES:[Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParams$Mode;
    .locals 1

    const-class v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/speech/params/RecognizerParams$Mode;
    .locals 1

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->$VALUES:[Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v0}, [Lcom/google/android/speech/params/RecognizerParams$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-object v0
.end method
