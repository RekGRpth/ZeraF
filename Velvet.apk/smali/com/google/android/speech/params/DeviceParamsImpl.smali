.class public Lcom/google/android/speech/params/DeviceParamsImpl;
.super Ljava/lang/Object;
.source "DeviceParamsImpl.java"

# interfaces
.implements Lcom/google/android/speech/params/DeviceParams;


# instance fields
.field private final mApplicationVersion:Ljava/lang/String;

.field private final mBrowserParamsSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreviewParamsSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;",
            ">;"
        }
    .end annotation
.end field

.field private final mScreenParamsSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;",
            ">;"
        }
    .end annotation
.end field

.field private final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mUserAgentSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p6    # Lcom/google/android/searchcommon/SearchConfig;
    .param p7    # Lcom/google/android/searchcommon/SearchSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;",
            ">;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;",
            ">;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;",
            ">;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/searchcommon/SearchConfig;",
            "Lcom/google/android/searchcommon/SearchSettings;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mApplicationVersion:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mBrowserParamsSupplier:Lcom/google/common/base/Supplier;

    iput-object p3, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mPreviewParamsSupplier:Lcom/google/common/base/Supplier;

    iput-object p4, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mScreenParamsSupplier:Lcom/google/common/base/Supplier;

    iput-object p5, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mUserAgentSupplier:Lcom/google/common/base/Supplier;

    iput-object p6, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p7, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-void
.end method


# virtual methods
.method public getApplicationVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mApplicationVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getBrowserParams()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mBrowserParamsSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    return-object v0
.end method

.method public getDeviceCountry()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getDeviceCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewParams()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mPreviewParamsSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    return-object v0
.end method

.method public getScreenParams()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mScreenParamsSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    return-object v0
.end method

.method public getSearchDomainCountryCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getSearchDomainCountryCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/DeviceParamsImpl;->mUserAgentSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
