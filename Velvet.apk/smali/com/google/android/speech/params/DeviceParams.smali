.class public interface abstract Lcom/google/android/speech/params/DeviceParams;
.super Ljava/lang/Object;
.source "DeviceParams.java"


# virtual methods
.method public abstract getApplicationVersion()Ljava/lang/String;
.end method

.method public abstract getBrowserParams()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
.end method

.method public abstract getDeviceCountry()Ljava/lang/String;
.end method

.method public abstract getPreviewParams()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
.end method

.method public abstract getScreenParams()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
.end method

.method public abstract getSearchDomainCountryCode()Ljava/lang/String;
.end method

.method public abstract getUserAgent()Ljava/lang/String;
.end method
