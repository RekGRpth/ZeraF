.class public Lcom/google/android/speech/RecognitionResponse;
.super Ljava/lang/Object;
.source "RecognitionResponse.java"


# instance fields
.field private final mEmbeddedResponse:Lcom/google/speech/s3/S3$S3Response;

.field private final mEngine:I

.field private final mNetworkResponse:Lcom/google/speech/s3/S3$S3Response;

.field private final mSoundSearchResponse:Lcom/google/speech/s3/S3$S3Response;


# direct methods
.method private constructor <init>(ILcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/speech/s3/S3$S3Response;
    .param p3    # Lcom/google/speech/s3/S3$S3Response;
    .param p4    # Lcom/google/speech/s3/S3$S3Response;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/speech/RecognitionResponse;->mEngine:I

    iput-object p2, p0, Lcom/google/android/speech/RecognitionResponse;->mEmbeddedResponse:Lcom/google/speech/s3/S3$S3Response;

    iput-object p3, p0, Lcom/google/android/speech/RecognitionResponse;->mNetworkResponse:Lcom/google/speech/s3/S3$S3Response;

    iput-object p4, p0, Lcom/google/android/speech/RecognitionResponse;->mSoundSearchResponse:Lcom/google/speech/s3/S3$S3Response;

    return-void
.end method

.method public static createEmbeddedResponse(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/android/speech/RecognitionResponse;
    .locals 3
    .param p0    # Lcom/google/speech/s3/S3$S3Response;

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/speech/RecognitionResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0, v2, v2}, Lcom/google/android/speech/RecognitionResponse;-><init>(ILcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;)V

    return-object v0
.end method

.method public static createNetworkResponse(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/android/speech/RecognitionResponse;
    .locals 3
    .param p0    # Lcom/google/speech/s3/S3$S3Response;

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/speech/RecognitionResponse;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v2, p0, v2}, Lcom/google/android/speech/RecognitionResponse;-><init>(ILcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;)V

    return-object v0
.end method

.method public static createRecognitionResponse(Lcom/google/speech/s3/S3$S3Response;I)Lcom/google/android/speech/RecognitionResponse;
    .locals 1
    .param p0    # Lcom/google/speech/s3/S3$S3Response;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {p0}, Lcom/google/android/speech/RecognitionResponse;->createEmbeddedResponse(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/android/speech/RecognitionResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0}, Lcom/google/android/speech/RecognitionResponse;->createNetworkResponse(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/android/speech/RecognitionResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p0}, Lcom/google/android/speech/RecognitionResponse;->createSoundSearchResponse(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/android/speech/RecognitionResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static createSoundSearchResponse(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/android/speech/RecognitionResponse;
    .locals 3
    .param p0    # Lcom/google/speech/s3/S3$S3Response;

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/speech/RecognitionResponse;

    const/4 v1, 0x3

    invoke-direct {v0, v1, v2, v2, p0}, Lcom/google/android/speech/RecognitionResponse;-><init>(ILcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;Lcom/google/speech/s3/S3$S3Response;)V

    return-object v0
.end method


# virtual methods
.method public getEngine()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/RecognitionResponse;->mEngine:I

    return v0
.end method

.method public getS3Response()Lcom/google/speech/s3/S3$S3Response;
    .locals 1

    iget v0, p0, Lcom/google/android/speech/RecognitionResponse;->mEngine:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/speech/RecognitionResponse;->mEmbeddedResponse:Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/speech/RecognitionResponse;->mNetworkResponse:Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/speech/RecognitionResponse;->mSoundSearchResponse:Lcom/google/speech/s3/S3$S3Response;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
