.class public Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;
.super Ljava/lang/Object;
.source "CompositeRecognitionEventListener.java"

# interfaces
.implements Lcom/google/android/speech/listeners/RecognitionEventListener;


# instance fields
.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/listeners/RecognitionEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBeginningOfSpeech()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onBeginningOfSpeech()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onDone()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onDone()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onEndOfSpeech()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onMediaDataResult([B)V
    .locals 3
    .param p1    # [B

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMediaDataResult([B)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onMusicDetected()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMusicDetected()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onNoSpeechDetected()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V
    .locals 3
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 3
    .param p1    # F
    .param p2    # F

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1, p1, p2}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onReadyForSpeech(FF)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onRecognitionCancelled()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onRecognitionCancelled()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 3
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V
    .locals 3
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    iget-object v2, p0, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v1, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V

    goto :goto_0

    :cond_0
    return-void
.end method
