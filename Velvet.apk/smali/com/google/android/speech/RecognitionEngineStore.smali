.class public Lcom/google/android/speech/RecognitionEngineStore;
.super Ljava/lang/Object;
.source "RecognitionEngineStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/RecognitionEngineStore$1;
    }
.end annotation


# instance fields
.field private final mEngineParams:Lcom/google/android/speech/params/RecognitionEngineParams;

.field private final mHybridEngine:Lcom/google/android/speech/engine/RecognitionEngine;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/params/RecognitionEngineParams;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/params/RecognitionEngineParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/RecognitionEngineStore;->mEngineParams:Lcom/google/android/speech/params/RecognitionEngineParams;

    invoke-direct {p0}, Lcom/google/android/speech/RecognitionEngineStore;->createHybridEngine()Lcom/google/android/speech/engine/RecognitionEngine;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/speech/RecognitionEngineStore;->log(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/engine/RecognitionEngine;

    iput-object v0, p0, Lcom/google/android/speech/RecognitionEngineStore;->mHybridEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    return-void
.end method

.method private createEmbeddedEngine()Lcom/google/android/speech/engine/RecognitionEngine;
    .locals 9

    iget-object v0, p0, Lcom/google/android/speech/RecognitionEngineStore;->mEngineParams:Lcom/google/android/speech/params/RecognitionEngineParams;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognitionEngineParams;->getEmbeddedParams()Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;

    move-result-object v8

    new-instance v0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;

    invoke-virtual {v8}, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->getGreco3EngineManager()Lcom/google/android/speech/embedded/Greco3EngineManager;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;

    move-result-object v2

    invoke-virtual {v8}, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->getSamplingRate()I

    move-result v3

    invoke-virtual {v8}, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->getBytesPerSample()I

    move-result v4

    invoke-virtual {v8}, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->getCallbackFactory()Lcom/google/android/speech/embedded/Greco3CallbackFactory;

    move-result-object v5

    invoke-virtual {v8}, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->getModeSelector()Lcom/google/android/speech/embedded/Greco3ModeSelector;

    move-result-object v6

    new-instance v7, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory;

    invoke-direct {v7}, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory;-><init>()V

    invoke-direct/range {v0 .. v7}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;-><init>(Lcom/google/android/speech/embedded/Greco3EngineManager;Lcom/google/android/speech/SpeechLevelSource;IILcom/google/android/speech/embedded/Greco3CallbackFactory;Lcom/google/android/speech/embedded/Greco3ModeSelector;Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;)V

    return-object v0
.end method

.method private createHybridEngine()Lcom/google/android/speech/engine/RecognitionEngine;
    .locals 12

    iget-object v0, p0, Lcom/google/android/speech/RecognitionEngineStore;->mEngineParams:Lcom/google/android/speech/params/RecognitionEngineParams;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognitionEngineParams;->getHybridParams()Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;

    move-result-object v11

    new-instance v0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;

    invoke-direct {p0}, Lcom/google/android/speech/RecognitionEngineStore;->createEmbeddedEngine()Lcom/google/android/speech/engine/RecognitionEngine;

    move-result-object v1

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->getLocalExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/speech/RecognitionEngineStore;->createNetworkEngine()Lcom/google/android/speech/engine/RecognitionEngine;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->getNetworkExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/speech/RecognitionEngineStore;->createMusicDetectorEngine()Lcom/google/android/speech/engine/RecognitionEngine;

    move-result-object v5

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->getMusicExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->getNetworkInformation()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v7

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v9

    invoke-virtual {v11}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->getSoundSearchEnabledSupplier()Lcom/google/common/base/Supplier;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;-><init>(Lcom/google/android/speech/engine/RecognitionEngine;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/engine/RecognitionEngine;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/engine/RecognitionEngine;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/utils/NetworkInformation;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/speech/SpeechSettings;Lcom/google/common/base/Supplier;)V

    return-object v0
.end method

.method private createMusicDetectorEngine()Lcom/google/android/speech/engine/RecognitionEngine;
    .locals 3

    iget-object v1, p0, Lcom/google/android/speech/RecognitionEngineStore;->mEngineParams:Lcom/google/android/speech/params/RecognitionEngineParams;

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognitionEngineParams;->getMusicDetectorParams()Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;

    move-result-object v0

    new-instance v1, Lcom/google/android/ears/MusicDetectorRecognitionEngine;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/ears/MusicDetectorRecognitionEngine;-><init>(Lcom/google/android/speech/SpeechSettings;)V

    return-object v1
.end method

.method private createNetworkEngine()Lcom/google/android/speech/engine/RecognitionEngine;
    .locals 7

    iget-object v0, p0, Lcom/google/android/speech/RecognitionEngineStore;->mEngineParams:Lcom/google/android/speech/params/RecognitionEngineParams;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognitionEngineParams;->getNetworkParams()Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;

    move-result-object v6

    new-instance v5, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;

    new-instance v0, Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getNetworkExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getNetworkRequestProducerParams()Lcom/google/android/speech/params/NetworkRequestProducerParams;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/params/NetworkRequestProducerParams;)V

    new-instance v1, Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getNetworkExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getNetworkRequestProducerParams()Lcom/google/android/speech/params/NetworkRequestProducerParams;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/params/NetworkRequestProducerParams;)V

    invoke-direct {v5, v0, v1}, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;-><init>(Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;)V

    new-instance v0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getPrimaryConnectionFactory()Lcom/google/android/speech/network/S3ConnectionFactory;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getFallbackConnectionFactory()Lcom/google/android/speech/network/S3ConnectionFactory;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getRetryPolicy()Lcom/google/android/speech/engine/RetryPolicy;

    move-result-object v3

    invoke-virtual {v6}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;->getNetworkExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/engine/NetworkRecognitionEngine;-><init>(Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/engine/RetryPolicy;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/network/producers/RequestProducerFactory;)V

    return-object v0
.end method

.method public static final log(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method public getEnginesForParams(Lcom/google/android/speech/params/RecognizerParams;)Ljava/util/Collection;
    .locals 3
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/params/RecognizerParams;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/speech/engine/RecognitionEngine;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/RecognitionEngineStore$1;->$SwitchMap$com$google$android$speech$params$RecognizerParams$Mode:[I

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/params/RecognizerParams$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    iget-object v1, p0, Lcom/google/android/speech/RecognitionEngineStore;->mHybridEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
