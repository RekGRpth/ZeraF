.class public Lcom/google/android/speech/contacts/Contact;
.super Ljava/lang/Object;
.source "Contact.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mGrammarWeight:D

.field private final mId:J

.field private final mMerged:Z

.field private final mName:Ljava/lang/String;

.field private final mPrimary:Z

.field private final mTimesContacted:I

.field private final mType:I

.field private final mTypeString:Ljava/lang/String;

.field private final mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/speech/contacts/Contact$1;

    invoke-direct {v0}, Lcom/google/android/speech/contacts/Contact$1;-><init>()V

    sput-object v0, Lcom/google/android/speech/contacts/Contact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IZDZ)V
    .locals 4
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .param p8    # Z
    .param p9    # D
    .param p11    # Z

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v2, -0x1

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz p3, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    const-string v1, "Only number-only contacts may have null names."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput-wide p1, p0, Lcom/google/android/speech/contacts/Contact;->mId:J

    iput-object p3, p0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/speech/contacts/Contact;->mValue:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/speech/contacts/Contact;->mTypeString:Ljava/lang/String;

    iput p7, p0, Lcom/google/android/speech/contacts/Contact;->mType:I

    iput p4, p0, Lcom/google/android/speech/contacts/Contact;->mTimesContacted:I

    iput-boolean p8, p0, Lcom/google/android/speech/contacts/Contact;->mPrimary:Z

    iput-wide p9, p0, Lcom/google/android/speech/contacts/Contact;->mGrammarWeight:D

    iput-boolean p11, p0, Lcom/google/android/speech/contacts/Contact;->mMerged:Z

    return-void

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static newPhoneNumberOnlyContact(Ljava/lang/String;)Lcom/google/android/speech/contacts/Contact;
    .locals 12
    .param p0    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/speech/contacts/Contact;

    const-wide/16 v1, -0x1

    const/4 v7, -0x1

    const-wide/16 v9, 0x0

    move-object v5, p0

    move-object v6, v3

    move v8, v4

    move v11, v4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/speech/contacts/Contact;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IZDZ)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/speech/contacts/Contact;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    iget-object v2, p0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/speech/contacts/Contact;->mType:I

    iget v3, v0, Lcom/google/android/speech/contacts/Contact;->mType:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public getGrammarWeight()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/speech/contacts/Contact;->mGrammarWeight:D

    return-wide v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/speech/contacts/Contact;->mId:J

    return-wide v0
.end method

.method public getLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/res/Resources;

    iget v0, p0, Lcom/google/android/speech/contacts/Contact;->mType:I

    iget-object v1, p0, Lcom/google/android/speech/contacts/Contact;->mTypeString:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 4

    iget-wide v0, p0, Lcom/google/android/speech/contacts/Contact;->mId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot get name of number-only contact."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "tel"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->opaquePart(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/contacts/Contact;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/speech/contacts/Contact;->mType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isMerged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/contacts/Contact;->mMerged:Z

    return v0
.end method

.method public isNumberOnlyContact()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/speech/contacts/Contact;->mId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/contacts/Contact;->mPrimary:Z

    return v0
.end method

.method public toRfc822Token()Ljava/lang/String;
    .locals 4

    new-instance v0, Landroid/text/util/Rfc822Token;

    iget-object v1, p0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/speech/contacts/Contact;->mValue:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Contact [mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/speech/contacts/Contact;->mId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/contacts/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTypeString="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/contacts/Contact;->mTypeString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/speech/contacts/Contact;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimesContacted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/speech/contacts/Contact;->mTimesContacted:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsPrimary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/speech/contacts/Contact;->mPrimary:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/speech/contacts/Contact;->mId:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/speech/contacts/Contact;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/speech/contacts/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/speech/contacts/Contact;->mTypeString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/speech/contacts/Contact;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/speech/contacts/Contact;->mTimesContacted:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/speech/contacts/Contact;->mPrimary:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-wide v3, p0, Lcom/google/android/speech/contacts/Contact;->mGrammarWeight:D

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeDouble(D)V

    iget-boolean v0, p0, Lcom/google/android/speech/contacts/Contact;->mMerged:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
