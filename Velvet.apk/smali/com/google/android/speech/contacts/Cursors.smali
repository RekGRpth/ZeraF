.class public Lcom/google/android/speech/contacts/Cursors;
.super Ljava/lang/Object;
.source "Cursors.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static iterateCursor(Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;Landroid/database/Cursor;)V
    .locals 1
    .param p0    # Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    :goto_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0, p1}, Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;->handleCurrentRow(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    return-void
.end method
