.class public final enum Lcom/google/android/speech/contacts/ContactLookup$Mode;
.super Ljava/lang/Enum;
.source "ContactLookup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/contacts/ContactLookup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/speech/contacts/ContactLookup$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/speech/contacts/ContactLookup$Mode;

.field public static final enum CONTACT:Lcom/google/android/speech/contacts/ContactLookup$Mode;

.field public static final enum EMAIL:Lcom/google/android/speech/contacts/ContactLookup$Mode;

.field public static final enum PHONE_NUMBER:Lcom/google/android/speech/contacts/ContactLookup$Mode;


# instance fields
.field private mCols:[Ljava/lang/String;

.field private mContentFilterUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;

    const-string v1, "EMAIL"

    # getter for: Lcom/google/android/speech/contacts/ContactLookup;->EMAIL_COLS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/speech/contacts/ContactLookup;->access$000()[Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/speech/contacts/ContactLookup$Mode;-><init>(Ljava/lang/String;I[Ljava/lang/String;Landroid/net/Uri;)V

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->EMAIL:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    new-instance v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;

    const-string v1, "PHONE_NUMBER"

    # getter for: Lcom/google/android/speech/contacts/ContactLookup;->PHONE_COLS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/speech/contacts/ContactLookup;->access$100()[Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "search_phone_number"

    const-string v5, "0"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/speech/contacts/ContactLookup$Mode;-><init>(Ljava/lang/String;I[Ljava/lang/String;Landroid/net/Uri;)V

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->PHONE_NUMBER:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    new-instance v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;

    const-string v1, "CONTACT"

    # getter for: Lcom/google/android/speech/contacts/ContactLookup;->CONTACT_COLS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/speech/contacts/ContactLookup;->access$200()[Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/speech/contacts/ContactLookup$Mode;-><init>(Ljava/lang/String;I[Ljava/lang/String;Landroid/net/Uri;)V

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->CONTACT:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/speech/contacts/ContactLookup$Mode;

    sget-object v1, Lcom/google/android/speech/contacts/ContactLookup$Mode;->EMAIL:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/speech/contacts/ContactLookup$Mode;->PHONE_NUMBER:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/speech/contacts/ContactLookup$Mode;->CONTACT:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->$VALUES:[Lcom/google/android/speech/contacts/ContactLookup$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I[Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p3    # [Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->mCols:[Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->mContentFilterUri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/speech/contacts/ContactLookup$Mode;)Z
    .locals 1
    .param p0    # Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-direct {p0}, Lcom/google/android/speech/contacts/ContactLookup$Mode;->hasValueColumns()Z

    move-result v0

    return v0
.end method

.method private hasValueColumns()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->mCols:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/speech/contacts/ContactLookup$Mode;
    .locals 1

    const-class v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/speech/contacts/ContactLookup$Mode;
    .locals 1

    sget-object v0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->$VALUES:[Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-virtual {v0}, [Lcom/google/android/speech/contacts/ContactLookup$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/speech/contacts/ContactLookup$Mode;

    return-object v0
.end method


# virtual methods
.method public getCols()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->mCols:[Ljava/lang/String;

    return-object v0
.end method

.method public getContentFilterUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Mode;->mContentFilterUri:Landroid/net/Uri;

    return-object v0
.end method
