.class public Lcom/google/android/speech/logger/SpeechLoggerImpl;
.super Ljava/lang/Object;
.source "SpeechLoggerImpl.java"

# interfaces
.implements Lcom/google/android/speech/logger/SpeechLogger;


# static fields
.field public static final INSTANCE:Lcom/google/android/speech/logger/SpeechLoggerImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/speech/logger/SpeechLoggerImpl;

    invoke-direct {v0}, Lcom/google/android/speech/logger/SpeechLoggerImpl;-><init>()V

    sput-object v0, Lcom/google/android/speech/logger/SpeechLoggerImpl;->INSTANCE:Lcom/google/android/speech/logger/SpeechLoggerImpl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public recordSpeechEvent(I)V
    .locals 0
    .param p1    # I

    invoke-static {p1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    return-void
.end method
