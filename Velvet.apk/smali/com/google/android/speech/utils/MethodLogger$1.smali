.class final Lcom/google/android/speech/utils/MethodLogger$1;
.super Ljava/lang/Object;
.source "MethodLogger.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/utils/MethodLogger;->createLoggingProxy(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$delegate:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/utils/MethodLogger$1;->val$delegate:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/reflect/Method;
    .param p3    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    # getter for: Lcom/google/android/speech/utils/MethodLogger;->SKIP_THESE_METHODS:Ljava/util/Set;
    invoke-static {}, Lcom/google/android/speech/utils/MethodLogger;->access$000()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MethodLogger"

    # invokes: Lcom/google/android/speech/utils/MethodLogger;->simpleString(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {p2, p3}, Lcom/google/android/speech/utils/MethodLogger;->access$100(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/utils/MethodLogger$1;->val$delegate:Ljava/lang/Object;

    invoke-virtual {p2, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
