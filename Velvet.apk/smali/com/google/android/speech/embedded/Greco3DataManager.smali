.class public Lcom/google/android/speech/embedded/Greco3DataManager;
.super Ljava/lang/Object;
.source "Greco3DataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;,
        Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;
    }
.end annotation


# static fields
.field private static final DIRECTORY_FILTER:Ljava/io/FileFilter;

.field static final SYSTEM_DATA_DIR:Ljava/io/File;


# instance fields
.field private mAvailableLanguages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/embedded/LocaleResourcesImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final mCompiledGrammarRoot:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private final mEngineVersion:I

.field private final mGreco3Prefs:Lcom/google/android/speech/embedded/Greco3Preferences;

.field final mInitializationCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNumUpdatesInProgress:I

.field private mPathDeleter:Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;

.field private final mSearchPaths:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpdateExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/system/usr/srec"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/speech/embedded/Greco3DataManager;->SYSTEM_DATA_DIR:Ljava/io/File;

    new-instance v0, Lcom/google/android/speech/embedded/Greco3DataManager$4;

    invoke-direct {v0}, Lcom/google/android/speech/embedded/Greco3DataManager$4;-><init>()V

    sput-object v0, Lcom/google/android/speech/embedded/Greco3DataManager;->DIRECTORY_FILTER:Ljava/io/FileFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;ILcom/google/common/collect/ImmutableList;Ljava/io/File;Ljava/util/concurrent/Executor;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Preferences;
    .param p3    # I
    .param p5    # Ljava/io/File;
    .param p6    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/speech/embedded/Greco3Preferences;",
            "I",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mGreco3Prefs:Lcom/google/android/speech/embedded/Greco3Preferences;

    iput p3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mEngineVersion:I

    iput-object p4, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mSearchPaths:Lcom/google/common/collect/ImmutableList;

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mSearchPaths:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isAbsolute()Z

    move-result v2

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    goto :goto_0

    :cond_0
    iput-object p5, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mCompiledGrammarRoot:Ljava/io/File;

    iput-object p6, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mUpdateExecutor:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mAvailableLanguages:Ljava/util/HashMap;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mInitializationCallbacks:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;ILjava/util/concurrent/Executor;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Preferences;
    .param p3    # I
    .param p4    # Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/File;

    const-string v1, "g3_models"

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/speech/embedded/Greco3DataManager;->SYSTEM_DATA_DIR:Ljava/io/File;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->getSearchPathList([Ljava/io/File;)Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "g3_grammars"

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/speech/embedded/Greco3DataManager;-><init>(Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;ILcom/google/common/collect/ImmutableList;Ljava/io/File;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/embedded/Greco3DataManager;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-direct {p0}, Lcom/google/android/speech/embedded/Greco3DataManager;->updateResourceListAndNotifyCallback()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/speech/embedded/Greco3DataManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/speech/embedded/Greco3DataManager;

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->doLanguageDelete(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    return-void
.end method

.method static deleteSingleLevelTree(Ljava/io/File;)V
    .locals 8
    .param p0    # Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "VS.G3DataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error deleting resource file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "VS.G3DataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error deleting directory: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private doLanguageDelete(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 3
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/speech/embedded/Greco3DataManager;->getOutputDirForLocale(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mPathDeleter:Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;

    invoke-interface {v1, v0, v2}, Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;->delete(Ljava/io/File;Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->blockingUpdateResources(Z)V

    return-void
.end method

.method private getOutputDirForLocale(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mContext:Landroid/content/Context;

    const-string v2, "g3_models"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private static varargs getSearchPathList([Ljava/io/File;)Lcom/google/common/collect/ImmutableList;
    .locals 6
    .param p0    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    if-eqz v1, :cond_0

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    return-object v5
.end method

.method private handleLocale(Ljava/io/File;Ljava/util/HashMap;)V
    .locals 11
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/embedded/LocaleResourcesImpl;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/speech/embedded/Greco3DataManager;->isValidLocale(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/speech/embedded/LocaleResourcesImpl;

    if-nez v5, :cond_2

    new-instance v5, Lcom/google/android/speech/embedded/LocaleResourcesImpl;

    iget v9, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mEngineVersion:I

    invoke-direct {v5, v9}, Lcom/google/android/speech/embedded/LocaleResourcesImpl;-><init>(I)V

    invoke-virtual {p2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v0, v2

    array-length v6, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v1, v0, v3

    invoke-static {v1}, Lcom/google/android/speech/embedded/Greco3Mode;->valueOf(Ljava/io/File;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v8

    const-string v9, "metadata"

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v8, :cond_4

    invoke-virtual {v5, v8, v1}, Lcom/google/android/speech/embedded/LocaleResourcesImpl;->addConfig(Lcom/google/android/speech/embedded/Greco3Mode;Ljava/io/File;)V

    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    if-eqz v4, :cond_3

    invoke-virtual {v5, v1}, Lcom/google/android/speech/embedded/LocaleResourcesImpl;->addMetadata(Ljava/io/File;)V

    goto :goto_1
.end method

.method private hasDictationOrGrammarResources(Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;)Z
    .locals 2
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getResourcePaths()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-interface {p1, v1}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-interface {p1, v1}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isValidLocale(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v2, 0x2d

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processGrammar(Ljava/io/File;Lcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/LocaleResourcesImpl;)V
    .locals 9
    .param p1    # Ljava/io/File;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;
    .param p3    # Lcom/google/android/speech/embedded/LocaleResourcesImpl;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v7, v1

    sget v8, Lcom/google/android/speech/embedded/Greco3GrammarCompiler;->NUM_GENERATED_FILES:I

    if-ge v7, v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x0

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    const-string v7, "metadata"

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move-object v4, v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    if-eqz v4, :cond_0

    invoke-virtual {p3, p2, v6, p1, v4}, Lcom/google/android/speech/embedded/LocaleResourcesImpl;->addGrammar(Lcom/google/android/speech/embedded/Greco3Grammar;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)V

    goto :goto_0
.end method

.method private processLocaleData(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/embedded/LocaleResourcesImpl;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/embedded/LocaleResourcesImpl;

    invoke-virtual {v1}, Lcom/google/android/speech/embedded/LocaleResourcesImpl;->processLocaleData()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateGrammars(Ljava/util/HashMap;)V
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/embedded/LocaleResourcesImpl;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/embedded/Greco3DataManager;->mCompiledGrammarRoot:Ljava/io/File;

    move-object/from16 v22, v0

    sget-object v23, Lcom/google/android/speech/embedded/Greco3DataManager;->DIRECTORY_FILTER:Ljava/io/FileFilter;

    invoke-virtual/range {v22 .. v23}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v22, v0

    if-nez v22, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object/from16 v3, v18

    array-length v13, v3

    const/4 v10, 0x0

    move v12, v10

    :goto_0
    if-ge v12, v13, :cond_0

    aget-object v16, v3, v12

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/speech/embedded/Greco3DataManager;->isValidLocale(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/speech/embedded/LocaleResourcesImpl;

    if-eqz v19, :cond_0

    sget-object v22, Lcom/google/android/speech/embedded/Greco3DataManager;->DIRECTORY_FILTER:Ljava/io/FileFilter;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_2

    array-length v0, v9

    move/from16 v22, v0

    if-nez v22, :cond_3

    :cond_2
    add-int/lit8 v10, v12, 0x1

    move v12, v10

    goto :goto_0

    :cond_3
    move-object v4, v9

    array-length v14, v4

    const/4 v10, 0x0

    move v11, v10

    :goto_1
    if-ge v11, v14, :cond_2

    aget-object v7, v4, v11

    invoke-static {v7}, Lcom/google/android/speech/embedded/Greco3Grammar;->valueOf(Ljava/io/File;)Lcom/google/android/speech/embedded/Greco3Grammar;

    move-result-object v8

    if-nez v8, :cond_5

    :cond_4
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_1

    :cond_5
    sget-object v22, Lcom/google/android/speech/embedded/Greco3DataManager;->DIRECTORY_FILTER:Ljava/io/FileFilter;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v21

    if-eqz v21, :cond_4

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    if-eqz v22, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/embedded/Greco3DataManager;->mGreco3Prefs:Lcom/google/android/speech/embedded/Greco3Preferences;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/google/android/speech/embedded/Greco3Preferences;->getCompiledGrammarRevisionId(Lcom/google/android/speech/embedded/Greco3Grammar;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v5, v21

    array-length v15, v5

    const/4 v10, 0x0

    :goto_2
    if-ge v10, v15, :cond_4

    aget-object v20, v5, v10

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/speech/embedded/Greco3DataManager;->mPathDeleter:Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;->delete(Ljava/io/File;Z)V

    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->processGrammar(Ljava/io/File;Lcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/LocaleResourcesImpl;)V

    goto :goto_3
.end method

.method private updateResourceListAndNotifyCallback()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/speech/embedded/Greco3DataManager;->doUpdateResourceList()Ljava/util/HashMap;

    move-result-object v0

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mInitializationCallbacks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/callback/SimpleCallback;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mInitializationCallbacks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mAvailableLanguages:Ljava/util/HashMap;

    iget v3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I

    iget v3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I

    if-ltz v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private updateResources(Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/embedded/LocaleResourcesImpl;",
            ">;)V"
        }
    .end annotation

    iget-object v7, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mSearchPaths:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v7}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    sget-object v7, Lcom/google/android/speech/embedded/Greco3DataManager;->DIRECTORY_FILTER:Ljava/io/FileFilter;

    invoke-virtual {v6, v7}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object v0, v5

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    invoke-direct {p0, v4, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->handleLocale(Ljava/io/File;Ljava/util/HashMap;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateResourcesLocked(Lcom/google/android/speech/callback/SimpleCallback;Z)V
    .locals 2
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mPathDeleter:Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mInitializationCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I

    if-lez v0, :cond_2

    if-nez p2, :cond_2

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mUpdateExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/speech/embedded/Greco3DataManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/speech/embedded/Greco3DataManager$1;-><init>(Lcom/google/android/speech/embedded/Greco3DataManager;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized blockingUpdateResources(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->updateResourcesLocked(Lcom/google/android/speech/callback/SimpleCallback;Z)V

    invoke-virtual {p0}, Lcom/google/android/speech/embedded/Greco3DataManager;->waitForPendingUpdates()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createOutputPathForGrammar(Lcom/google/android/speech/embedded/Greco3Grammar;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mCompiledGrammarRoot:Ljava/io/File;

    invoke-direct {v1, v3, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/google/android/speech/embedded/Greco3Grammar;->getDirectoryName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    :cond_0
    return-object v2
.end method

.method public deleteLanguage(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Ljava/util/concurrent/Executor;Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/speech/embedded/Greco3DataManager$3;

    invoke-direct {v0, p0, p1, p3}, Lcom/google/android/speech/embedded/Greco3DataManager$3;-><init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Ljava/lang/Runnable;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/speech/embedded/Greco3DataManager$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected doUpdateResourceList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/speech/embedded/LocaleResourcesImpl;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->updateResources(Ljava/util/HashMap;)V

    invoke-direct {p0, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->updateGrammars(Ljava/util/HashMap;)V

    invoke-direct {p0, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->processLocaleData(Ljava/util/HashMap;)V

    return-object v0
.end method

.method getInitializationCallbacksForTesting()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mInitializationCallbacks:Ljava/util/List;

    return-object v0
.end method

.method public getInstalledLanguages()Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v4

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v4, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mAvailableLanguages:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/speech/embedded/LocaleResourcesImpl;

    invoke-virtual {v3}, Lcom/google/android/speech/embedded/LocaleResourcesImpl;->getLanguageMetadata()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasDictationOrGrammarResources(Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public getModelsDirSupplier()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/embedded/Greco3DataManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/speech/embedded/Greco3DataManager$2;-><init>(Lcom/google/android/speech/embedded/Greco3DataManager;)V

    invoke-static {v0}, Lcom/google/common/base/Suppliers;->memoize(Lcom/google/common/base/Supplier;)Lcom/google/common/base/Supplier;

    move-result-object v0

    return-object v0
.end method

.method public getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mAvailableLanguages:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getRevisionForGrammar(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v1

    if-nez v1, :cond_1

    move-object v0, v2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mGreco3Prefs:Lcom/google/android/speech/embedded/Greco3Preferences;

    invoke-virtual {v3, p2}, Lcom/google/android/speech/embedded/Greco3Preferences;->getCompiledGrammarRevisionId(Lcom/google/android/speech/embedded/Greco3Grammar;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v1, p2, v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getGrammarPath(Lcom/google/android/speech/embedded/Greco3Grammar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public hasCompiledGrammar(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/speech/embedded/Greco3DataManager;->getRevisionForGrammar(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Mode;

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-interface {v0, p2}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hasResourcesForCompilation(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v2, Lcom/google/android/speech/embedded/Greco3Mode;->COMPILER:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-interface {v0, v2}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-interface {v0, v2}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public declared-synchronized isInitialized()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mAvailableLanguages:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isInstalledInSystemPartition(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getResourcePaths()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    invoke-interface {v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->isInstalledInSystemPartition()Z

    move-result v1

    goto :goto_0
.end method

.method public isUsingDownloadedData(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getResourcePaths()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    invoke-interface {v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->isUsingDownloadedData()Z

    move-result v1

    goto :goto_0
.end method

.method public declared-synchronized maybeInitialize(Lcom/google/android/speech/callback/SimpleCallback;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->updateResourcesLocked(Lcom/google/android/speech/callback/SimpleCallback;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :cond_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setPathDeleter(Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;

    iput-object p1, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mPathDeleter:Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;

    return-void
.end method

.method public declared-synchronized waitForInitialization()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mAvailableLanguages:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected declared-synchronized waitForPendingUpdates()V
    .locals 3

    monitor-enter p0

    :goto_0
    :try_start_0
    iget v1, p0, Lcom/google/android/speech/embedded/Greco3DataManager;->mNumUpdatesInProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    const-string v1, "VS.G3DataManager"

    const-string v2, "Interrupted waiting for resource update."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    monitor-exit p0

    return-void
.end method
