.class public Lcom/google/android/speech/internal/DefaultModeSelector;
.super Ljava/lang/Object;
.source "DefaultModeSelector.java"

# interfaces
.implements Lcom/google/android/speech/embedded/Greco3ModeSelector;


# instance fields
.field private final mIsTelephoneCapable:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/speech/internal/DefaultModeSelector;->mIsTelephoneCapable:Z

    return-void
.end method


# virtual methods
.method public getFallbackMode(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3Mode;
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->ENDPOINTER_DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->ENDPOINTER_VOICESEARCH:Lcom/google/android/speech/embedded/Greco3Mode;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMode(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3Mode;
    .locals 2
    .param p1    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;

    move-object v0, p1

    invoke-virtual {p0}, Lcom/google/android/speech/internal/DefaultModeSelector;->isEndpointerOnly()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/speech/internal/DefaultModeSelector;->getFallbackMode(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/speech/internal/DefaultModeSelector;->mIsTelephoneCapable:Z

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->ENDPOINTER_VOICESEARCH:Lcom/google/android/speech/embedded/Greco3Mode;

    :cond_1
    return-object v0
.end method

.method protected isEndpointerOnly()Z
    .locals 1

    invoke-static {}, Lcom/google/android/speech/embedded/Greco3Recognizer;->isEndpointerOnly()Z

    move-result v0

    return v0
.end method
