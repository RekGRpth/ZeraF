.class public Lcom/google/android/speech/internal/DefaultCallbackFactory;
.super Ljava/lang/Object;
.source "DefaultCallbackFactory.java"

# interfaces
.implements Lcom/google/android/speech/embedded/Greco3CallbackFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)Lcom/google/android/speech/embedded/Greco3Callback;
    .locals 3
    .param p2    # Lcom/google/android/speech/audio/EndpointerListener;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p4    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/audio/EndpointerListener;",
            "Lcom/google/android/speech/embedded/Greco3Mode;",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;",
            ")",
            "Lcom/google/android/speech/embedded/Greco3Callback;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/internal/EndpointerEventProcessor;

    invoke-direct {v0, p2, p4}, Lcom/google/android/speech/internal/EndpointerEventProcessor;-><init>(Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)V

    new-instance v1, Lcom/google/android/speech/RecognitionResponseWrapper;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/google/android/speech/RecognitionResponseWrapper;-><init>(Lcom/google/android/speech/callback/Callback;I)V

    new-instance v2, Lcom/google/android/speech/internal/Greco3CallbackImpl;

    invoke-direct {v2, p3, v1, v0}, Lcom/google/android/speech/internal/Greco3CallbackImpl;-><init>(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/internal/EndpointerEventProcessor;)V

    return-object v2
.end method
