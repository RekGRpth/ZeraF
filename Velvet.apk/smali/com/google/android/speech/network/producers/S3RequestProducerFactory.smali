.class public Lcom/google/android/speech/network/producers/S3RequestProducerFactory;
.super Ljava/lang/Object;
.source "S3RequestProducerFactory.java"

# interfaces
.implements Lcom/google/android/speech/network/producers/RequestProducerFactory;


# instance fields
.field private mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field private final mSoundSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;

.field private final mVoiceSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;
    .param p2    # Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mSoundSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;

    iput-object p2, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mVoiceSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;

    return-void
.end method


# virtual methods
.method public init(Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mSoundSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;->init(Lcom/google/android/speech/params/RecognizerParams;)V

    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mVoiceSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;->init(Lcom/google/android/speech/params/RecognizerParams;)V

    return-void
.end method

.method public newRequestProducer(Ljava/io/InputStream;)Lcom/google/android/speech/network/producers/S3RequestProducer;
    .locals 2
    .param p1    # Ljava/io/InputStream;

    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mSoundSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;->newRequestProducer(Ljava/io/InputStream;)Lcom/google/android/speech/network/producers/S3RequestProducer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mVoiceSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;->newRequestProducer(Ljava/io/InputStream;)Lcom/google/android/speech/network/producers/S3RequestProducer;

    move-result-object v0

    goto :goto_0
.end method

.method public refresh()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mSoundSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;

    invoke-virtual {v0}, Lcom/google/android/speech/network/producers/SoundSearchRequestProducerFactory;->refresh()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/speech/network/producers/S3RequestProducerFactory;->mVoiceSearchRequestProducerFactory:Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;

    invoke-virtual {v0}, Lcom/google/android/speech/network/producers/VoiceSearchRequestProducerFactory;->refresh()V

    goto :goto_0
.end method
