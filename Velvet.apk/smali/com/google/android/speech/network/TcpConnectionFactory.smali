.class public Lcom/google/android/speech/network/TcpConnectionFactory;
.super Ljava/lang/Object;
.source "TcpConnectionFactory.java"

# interfaces
.implements Lcom/google/android/speech/network/S3ConnectionFactory;


# instance fields
.field private final mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

.field private final mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mTcpServerInfo:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/base/Supplier;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/speech/utils/NetworkInformation;)V
    .locals 0
    .param p2    # Lcom/google/android/speech/network/ConnectionFactory;
    .param p3    # Lcom/google/android/speech/utils/NetworkInformation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;",
            ">;",
            "Lcom/google/android/speech/network/ConnectionFactory;",
            "Lcom/google/android/speech/utils/NetworkInformation;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mTcpServerInfo:Lcom/google/common/base/Supplier;

    iput-object p2, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    iput-object p3, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    return-void
.end method

.method private getMccMnc()I
    .locals 6

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v3}, Lcom/google/android/speech/utils/NetworkInformation;->getConnectionId()I

    move-result v0

    if-eq v0, v4, :cond_0

    if-ne v0, v5, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v3}, Lcom/google/android/speech/utils/NetworkInformation;->getNetworkMccMnc()[I

    move-result-object v1

    sget-object v3, Lcom/google/android/speech/utils/NetworkInformation;->MISSING_TELEPHONY_RESULT:[I

    if-eq v1, v3, :cond_0

    aget v3, v1, v2

    if-eq v3, v4, :cond_0

    aget v2, v1, v2

    mul-int/lit16 v2, v2, 0x3e8

    aget v3, v1, v5

    add-int/2addr v2, v3

    goto :goto_0
.end method

.method private isTcpUsable(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mTcpServerInfo:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getBlacklistMccMncList()Ljava/util/List;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public create()Lcom/google/android/speech/network/S3Connection;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/speech/network/TcpConnectionFactory;->getMccMnc()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/speech/network/TcpConnectionFactory;->isTcpUsable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/speech/network/DirectTcpConnection;

    iget-object v0, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mTcpServerInfo:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    iget-object v2, p0, Lcom/google/android/speech/network/TcpConnectionFactory;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    invoke-direct {v1, v0, v2}, Lcom/google/android/speech/network/DirectTcpConnection;-><init>(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;Lcom/google/android/speech/network/ConnectionFactory;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
