.class public Lcom/google/android/speech/network/request/PreviewParamsSupplier;
.super Ljava/lang/Object;
.source "PreviewParamsSupplier.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;",
        ">;"
    }
.end annotation


# instance fields
.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/network/request/PreviewParamsSupplier;->mResources:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public get()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 4

    const v3, 0x7f0c0033

    new-instance v0, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    invoke-direct {v0}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/network/request/PreviewParamsSupplier;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0c0035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setMapHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/network/request/PreviewParamsSupplier;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0c0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setMapWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/network/request/PreviewParamsSupplier;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setUrlHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/network/request/PreviewParamsSupplier;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setUrlWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/network/request/PreviewParamsSupplier;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0b0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;->setUrlPreviewType(I)Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/network/request/PreviewParamsSupplier;->get()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v0

    return-object v0
.end method
