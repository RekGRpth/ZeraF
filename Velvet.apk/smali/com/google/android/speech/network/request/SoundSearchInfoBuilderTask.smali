.class public Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;
.super Lcom/google/android/speech/network/request/BaseRequestBuilderTask;
.source "SoundSearchInfoBuilderTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/speech/network/request/BaseRequestBuilderTask",
        "<",
        "Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

.field private final mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mTtsEnabled:Z


# direct methods
.method public constructor <init>(ZLcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/speech/params/DeviceParams;)V
    .locals 1
    .param p1    # Z
    .param p2    # Lcom/google/android/speech/utils/NetworkInformation;
    .param p3    # Lcom/google/android/speech/params/DeviceParams;

    const-string v0, "SoundSearchInfoBuilderTask"

    invoke-direct {p0, v0}, Lcom/google/android/speech/network/request/BaseRequestBuilderTask;-><init>(Ljava/lang/String;)V

    iput-boolean p1, p0, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;->mTtsEnabled:Z

    iput-object p2, p0, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    iput-object p3, p0, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    return-void
.end method


# virtual methods
.method protected build()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
    .locals 7

    const/4 v6, 0x4

    new-instance v0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    invoke-direct {v0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;-><init>()V

    iget-object v5, p0, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v5}, Lcom/google/android/speech/utils/NetworkInformation;->getDeviceCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0, v2}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setClientCountryCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    :cond_0
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->addDesiredResultType(I)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    new-instance v4, Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    invoke-direct {v4}, Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;-><init>()V

    invoke-virtual {v4, v6}, Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;->setAudioContainer(I)Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    invoke-virtual {v4, v6}, Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;->setAudioEncoding(I)Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    new-instance v3, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    invoke-direct {v3}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->setLookupRequest(Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    invoke-virtual {v3, v4}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->setStreamRequest(Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    iget-boolean v5, p0, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;->mTtsEnabled:Z

    invoke-virtual {v3, v5}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->setTtsOutputEnabled(Z)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    return-object v3

    :cond_1
    iget-object v5, p0, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    invoke-interface {v5}, Lcom/google/android/speech/params/DeviceParams;->getSearchDomainCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setClientCountryCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0
.end method

.method protected bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/network/request/SoundSearchInfoBuilderTask;->build()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    move-result-object v0

    return-object v0
.end method
