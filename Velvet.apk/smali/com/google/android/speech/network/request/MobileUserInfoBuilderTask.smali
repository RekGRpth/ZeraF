.class public Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;
.super Lcom/google/android/speech/network/request/BaseRequestBuilderTask;
.source "MobileUserInfoBuilderTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/speech/network/request/BaseRequestBuilderTask",
        "<",
        "Lcom/google/speech/s3/MobileUser$MobileUserInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/utils/NetworkInformation;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/utils/NetworkInformation;

    const-string v0, "MobileUserInfoBuilderTask"

    invoke-direct {p0, v0}, Lcom/google/android/speech/network/request/BaseRequestBuilderTask;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    return-void
.end method


# virtual methods
.method protected build()Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    invoke-direct {v0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;-><init>()V

    iget-object v4, p0, Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v4}, Lcom/google/android/speech/utils/NetworkInformation;->getNetworkMccMnc()[I

    move-result-object v1

    sget-object v4, Lcom/google/android/speech/utils/NetworkInformation;->MISSING_TELEPHONY_RESULT:[I

    if-eq v1, v4, :cond_0

    aget v4, v1, v5

    invoke-virtual {v0, v4}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setNetworkMcc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    aget v4, v1, v6

    invoke-virtual {v0, v4}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setNetworkMnc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    :cond_0
    iget-object v4, p0, Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v4}, Lcom/google/android/speech/utils/NetworkInformation;->getSimMccMnc()[I

    move-result-object v3

    sget-object v4, Lcom/google/android/speech/utils/NetworkInformation;->MISSING_TELEPHONY_RESULT:[I

    if-eq v3, v4, :cond_1

    aget v4, v3, v5

    invoke-virtual {v0, v4}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setSimMcc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    aget v4, v3, v6

    invoke-virtual {v0, v4}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setSimMnc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    :cond_1
    iget-object v4, p0, Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v4}, Lcom/google/android/speech/utils/NetworkInformation;->getConnectionId()I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    invoke-virtual {v0, v2}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setNetworkType(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    :cond_2
    return-object v0
.end method

.method protected bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/network/request/MobileUserInfoBuilderTask;->build()Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    move-result-object v0

    return-object v0
.end method
