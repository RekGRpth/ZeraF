.class public Lcom/google/android/speech/network/request/BrowserParamsSupplier;
.super Ljava/lang/Object;
.source "BrowserParamsSupplier.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;",
        ">;"
    }
.end annotation


# instance fields
.field private mBaseParams:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

.field private final mPredictiveCardsPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mUserAgentHelper:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/common/base/Supplier;Landroid/view/WindowManager;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;
    .param p2    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p4    # Landroid/view/WindowManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/view/WindowManager;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mPredictiveCardsPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    iput-object p2, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p3, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mUserAgentHelper:Lcom/google/common/base/Supplier;

    iput-object p4, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mWindowManager:Landroid/view/WindowManager;

    return-void
.end method

.method private declared-synchronized maybeInitBaseParams()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mBaseParams:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    if-nez v1, :cond_0

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    new-instance v1, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-direct {v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;-><init>()V

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    move-result-object v1

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mBaseParams:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public get()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->maybeInitBaseParams()V

    new-instance v0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-direct {v0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;-><init>()V

    :try_start_0
    iget-object v3, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mBaseParams:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-virtual {v3}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getHlParameter()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setSearchLanguage(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    iget-object v3, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mUserAgentHelper:Lcom/google/common/base/Supplier;

    invoke-interface {v3}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setUserAgent(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    iget-object v3, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mPredictiveCardsPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getUnits()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    if-nez v2, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v0, v3}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setUseMetricUnits(Z)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    :cond_0
    iget-object v3, p0, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getMajelDomain(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setGoogleDomain(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clear()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/network/request/BrowserParamsSupplier;->get()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    move-result-object v0

    return-object v0
.end method
