.class public Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;
.super Ljava/lang/Object;
.source "DirectTcpConnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/network/DirectTcpConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ReaderRunnable"
.end annotation


# instance fields
.field private volatile mRunning:Z

.field final synthetic this$0:Lcom/google/android/speech/network/DirectTcpConnection;


# direct methods
.method protected constructor <init>(Lcom/google/android/speech/network/DirectTcpConnection;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    return-void
.end method

.method public run()V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    iput-boolean v4, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    :goto_0
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    # getter for: Lcom/google/android/speech/network/DirectTcpConnection;->mInput:Lcom/google/android/speech/message/S3ResponseStream;
    invoke-static {v2}, Lcom/google/android/speech/network/DirectTcpConnection;->access$000(Lcom/google/android/speech/network/DirectTcpConnection;)Lcom/google/android/speech/message/S3ResponseStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/message/S3ResponseStream;->read()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    if-nez v2, :cond_0

    const-string v2, "DirectTcpConnection"

    const-string v3, "Reader - exit"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    invoke-virtual {v2}, Lcom/google/android/speech/network/DirectTcpConnection;->close()V

    iput-boolean v5, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    :goto_1
    return-void

    :cond_0
    :try_start_1
    invoke-virtual {v1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    invoke-virtual {v1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v2

    if-ne v2, v4, :cond_2

    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    :cond_2
    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    iget-object v2, v2, Lcom/google/android/speech/network/DirectTcpConnection;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v2, v1}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    if-eqz v2, :cond_3

    const-string v2, "DirectTcpConnection"

    const-string v3, "Reader - exception - exit"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    iget-object v2, v2, Lcom/google/android/speech/network/DirectTcpConnection;->mCallback:Lcom/google/android/speech/callback/Callback;

    new-instance v3, Lcom/google/android/speech/exception/NetworkRecognizeException;

    const-string v4, "Error while reading"

    invoke-direct {v3, v4, v0}, Lcom/google/android/speech/exception/NetworkRecognizeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v2, v3}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    invoke-virtual {v2}, Lcom/google/android/speech/network/DirectTcpConnection;->close()V

    iput-boolean v5, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    invoke-virtual {v2}, Lcom/google/android/speech/network/DirectTcpConnection;->close()V

    iput-boolean v5, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    goto :goto_1

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->this$0:Lcom/google/android/speech/network/DirectTcpConnection;

    invoke-virtual {v3}, Lcom/google/android/speech/network/DirectTcpConnection;->close()V

    iput-boolean v5, p0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->mRunning:Z

    throw v2
.end method
