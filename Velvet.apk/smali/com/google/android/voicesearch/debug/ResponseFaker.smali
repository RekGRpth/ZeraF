.class public Lcom/google/android/voicesearch/debug/ResponseFaker;
.super Ljava/lang/Object;
.source "ResponseFaker.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ResponseFaker"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createActionContact(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v0

    return-object v0
.end method

.method private static varargs createActionContactGroup([Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;
    .locals 6
    .param p0    # [Ljava/lang/String;

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;-><init>()V

    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    invoke-static {v4}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createActionContact(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static createEmailAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;-><init>()V

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createActionContactGroup([Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->addTo(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->setSubject(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v0, p2}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->setBody(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    :cond_2
    return-object v0
.end method

.method private static createImageResponsePeanut(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 9
    .param p0    # Ljava/lang/String;

    const/16 v8, 0x12c

    new-instance v6, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v6}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    const-string v7, "The Origin of Dogs"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageTitle(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v6

    const-string v7, "http://www.scientificamerican.com/article.cfm?id=the-origin-of-dogs"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageUrl(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v6

    const-string v7, "scientificamerican.com"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageDomain(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v0

    new-instance v6, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v6}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    const-string v7, "Pictures of Dogs"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageTitle(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v6

    const-string v7, "http://picturesofpets.onsugar.com/Pictures-Dogs-18794228"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageUrl(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v6

    const-string v7, "onsugar.com"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageDomain(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v1

    new-instance v6, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v6}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    const-string v7, "Dogs world"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageTitle(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v6

    const-string v7, "http://norhanakkad.wordpress.com/2009/03/11/61/"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageUrl(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v6

    const-string v7, "wordpress.com"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->setPageDomain(Ljava/lang/String;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v2

    new-instance v6, Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-direct {v6}, Lcom/google/majel/proto/PeanutProtos$Image;-><init>()V

    const-string v7, "http://www.google.com/images?q=tbn:ANd9GcRKR_t9u9YozSre8J5c0Q2DQ-hB39RecHmI3j9PFZVV4GYAeWbxwaJg-FQ"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setThumbUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    const-string v7, "http://www.scientificamerican.com/media/inline/the-origin-of-dogs_1.jpg"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/google/majel/proto/PeanutProtos$Image;->setHeight(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/google/majel/proto/PeanutProtos$Image;->setWidth(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/majel/proto/PeanutProtos$Image;->addAttribution(Lcom/google/majel/proto/AttributionProtos$Attribution;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v3

    new-instance v6, Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-direct {v6}, Lcom/google/majel/proto/PeanutProtos$Image;-><init>()V

    const-string v7, "http://www.google.com/images?q=tbn:ANd9GcT_AMZG26G_hk_0b5AUkqWi0mhMQ9ug4ZbNM2YNdzPrFGKJuchkRjjm"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setThumbUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    const-string v7, "http://media19.onsugar.com/files/2011/08/33/3/1873/18733178/4d1341f8241cf7bf_Pictures_Of_Dogs_A.jpg"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    const/16 v7, 0x1f4

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setHeight(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    const/16 v7, 0x1c2

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setWidth(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/google/majel/proto/PeanutProtos$Image;->addAttribution(Lcom/google/majel/proto/AttributionProtos$Attribution;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v4

    new-instance v6, Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-direct {v6}, Lcom/google/majel/proto/PeanutProtos$Image;-><init>()V

    const-string v7, "/images?q=tbn:ANd9GcRw2gmVG6mHknKKriXemiJpDRQ_b0smb66qY0w5EB3mdldTfsI8gnxbXkU"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setThumbUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    const-string v7, "http://norhanakkad.files.wordpress.com/2009/03/dfr.jpg"

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    const/16 v7, 0x190

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setHeight(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    const/16 v7, 0x258

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Image;->setWidth(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->addAttribution(Lcom/google/majel/proto/AttributionProtos$Attribution;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v5

    new-instance v6, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v6}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setPrimaryType(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addImageResponse(Lcom/google/majel/proto/PeanutProtos$Image;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addImageResponse(Lcom/google/majel/proto/PeanutProtos$Image;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addImageResponse(Lcom/google/majel/proto/PeanutProtos$Image;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v6

    return-object v6
.end method

.method public static createPhoneAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;-><init>()V

    invoke-virtual {v2, p1}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->setNumber(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->setType(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    invoke-virtual {v2, p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->addPhone(Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v0

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v2

    return-object v2
.end method

.method public static createSMSAction(Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SMSAction;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createActionContact(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->setContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;-><init>()V

    invoke-static {p0}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createActionContact(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->addRecipient(Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;)Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, p1}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->setMessageBody(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    :cond_1
    return-object v1
.end method

.method private static createWebSearchPeanut(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 5
    .param p0    # Ljava/lang/String;

    new-instance v2, Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    invoke-direct {v2}, Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;-><init>()V

    const-string v3, "query"

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;

    move-result-object v0

    new-instance v2, Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-direct {v2}, Lcom/google/majel/proto/PeanutProtos$Url;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/majel/proto/PeanutProtos$Url;->setSearchResultsInfo(Lcom/google/majel/proto/PeanutProtos$SearchResultsInfo;)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<html><body><b>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</b></body></html>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/PeanutProtos$Url;->setHtml(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addUrlResponse(Lcom/google/majel/proto/PeanutProtos$Url;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v2

    return-object v2
.end method

.method private static fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 2
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    new-instance v0, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-direct {v0}, Lcom/google/majel/proto/MajelProtos$MajelResponse;-><init>()V

    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    invoke-virtual {v1, p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addActionV2(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->addPeanut(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v0

    return-object v0
.end method

.method public static fakeEmailAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v1, "ResponseFaker"

    const-string v2, "#fakeEmailAction"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1, p2}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createEmailAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    move-result-object v0

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setEmailActionExtension(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    return-object v1
.end method

.method public static fakeHelpAction()Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 8

    const/4 v7, 0x5

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;-><init>()V

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;-><init>()V

    const-string v3, "Discover what you can say"

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->setText(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->setTitle(Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;-><init>()V

    const-string v3, "Indeed, discover it!"

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->setText(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->setIntroduction(Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;-><init>()V

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;-><init>()V

    const-string v4, "Check your calendar"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->setText(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->setHeadline(Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Show my calendar tomorrow"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setRelativeDays(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Add event on %2 %1"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    const/16 v4, 0x37

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setRelativeDays(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addSubstitution(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addSubstitution(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Add event at 5pm"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    new-instance v4, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    invoke-direct {v4}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;-><init>()V

    invoke-virtual {v4, v7}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setHour(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setMinute(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->addFeature(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;)Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;-><init>()V

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;-><init>()V

    const-string v4, "Set alarms"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->setText(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->setHeadline(Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Set alarm in 30 minutes"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    const/16 v4, 0x708

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setRelativeSeconds(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Set alarm at 8pm"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    new-instance v4, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    invoke-direct {v4}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;-><init>()V

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setHour(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setMinute(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "When is my next alarm?"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    new-instance v4, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    invoke-direct {v4}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;-><init>()V

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setHour(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v4

    const/16 v5, 0x1e

    invoke-virtual {v4, v5}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setMinute(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->addFeature(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;)Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;-><init>()V

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;-><init>()V

    const-string v4, "Do cool stuff"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->setText(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->setHeadline(Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Where am I?"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    const-string v4, "http://venturebeat.files.wordpress.com/2010/12/google-maps-android-update.jpg?w=558&h=9999&crop=0"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Scan barcodes"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    const-string v4, "http://www.centralbooks.com/images/cabinet_barcode.gif"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Show my calendar on %1"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setRelativeDays(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addSubstitution(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Research digital cameras"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    const-string v4, "http://cdn.pocket-lint.com/images/4Wt8/samsung-galaxy-camera-ek-gc100-android-wi-fi-compact-camera-review-0.jpg?20121127-205910"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    const-string v4, "Call Adele"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    const-string v4, "http://o.scdn.co/300/20aa06be383da7292e0e4f69c0fa13c64470e631"

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->setImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->addRequiredCapability(I)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->addFeature(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;)Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setHelpActionExtension(Lcom/google/majel/proto/ActionV2Protos$HelpAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v0

    return-object v0
.end method

.method public static fakeImageSearch()Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 2

    const-string v0, "ResponseFaker"

    const-string v1, "#fakeImageSearch"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-direct {v0}, Lcom/google/majel/proto/MajelProtos$MajelResponse;-><init>()V

    const-string v1, "fake pictures of dogs"

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createImageResponsePeanut(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->addPeanut(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v0

    return-object v0
.end method

.method public static fakeMessageAction(Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const-string v1, "ResponseFaker"

    const-string v2, "#fakeMessageAction"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createSMSAction(Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    move-result-object v0

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setSMSActionExtension(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    return-object v1
.end method

.method public static fakeOpenAppAction(Ljava/lang/String;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "ResponseFaker"

    const-string v2, "#fakeOpenAppAction"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;-><init>()V

    invoke-virtual {v1, p0}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;

    move-result-object v0

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setOpenApplicationActionExtension(Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    return-object v1
.end method

.method public static fakePhoneAction()Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 5

    const-string v2, "ResponseFaker"

    const-string v3, "#fakePhoneAction"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "Zanolin Luca"

    const-string v3, "07931447370"

    const-string v4, "home"

    invoke-static {v2, v3, v4}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createPhoneAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v1

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setPhoneActionExtension(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    new-instance v2, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-direct {v2}, Lcom/google/majel/proto/MajelProtos$MajelResponse;-><init>()V

    new-instance v3, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addActionV2(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->addPeanut(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v2

    return-object v2
.end method

.method public static fakePhoneActionNotInAddressBook()Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 4

    const-string v1, "ResponseFaker"

    const-string v2, "#fakePhoneAction"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;-><init>()V

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    const-string v3, "Zanolin Luca"

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setParsedName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v0

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setPhoneActionExtension(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    return-object v1
.end method

.method public static fakeRecognitionEvent(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 4
    .param p0    # Ljava/lang/String;

    new-instance v2, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    invoke-direct {v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;-><init>()V

    invoke-virtual {v2, p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v2

    const/high16 v3, 0x3f800000

    invoke-virtual {v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setConfidence(F)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v0

    new-instance v2, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    invoke-direct {v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->addHypothesis(Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v1

    new-instance v2, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-direct {v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setCombinedResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setEventType(I)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v2

    return-object v2
.end method

.method public static fakeRecognitionEventPhoneAction()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1

    const-string v0, "Call Simon"

    invoke-static {v0}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeRecognitionEvent(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    return-object v0
.end method

.method public static fakeRecognitionEventShowContactInformationAction()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1

    const-string v0, "Show me Peter\'s contact details"

    invoke-static {v0}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeRecognitionEvent(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    return-object v0
.end method

.method public static fakeSelfNoteAction(Ljava/lang/String;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "ResponseFaker"

    const-string v2, "#fakeSelfNoteAction"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;-><init>()V

    invoke-virtual {v1, p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->setBody(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    move-result-object v0

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setSelfNoteActionExtension(Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    return-object v1
.end method

.method public static fakeShowContactInformationAction()Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 4

    const-string v1, "ResponseFaker"

    const-string v2, "#fakeShowContactInformationAction"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;-><init>()V

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    const-string v3, "Peter"

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    move-result-object v0

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setShowContactInformationActionExtension(Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->fakeAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    return-object v1
.end method

.method public static fakeWebSearch()Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 2

    const-string v0, "ResponseFaker"

    const-string v1, "#fakeWebSearch"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-direct {v0}, Lcom/google/majel/proto/MajelProtos$MajelResponse;-><init>()V

    const-string v1, "golden gate bridge"

    invoke-static {v1}, Lcom/google/android/voicesearch/debug/ResponseFaker;->createWebSearchPeanut(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->addPeanut(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v0

    return-object v0
.end method
