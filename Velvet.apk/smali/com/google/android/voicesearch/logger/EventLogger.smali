.class public final Lcom/google/android/voicesearch/logger/EventLogger;
.super Ljava/lang/Object;
.source "EventLogger.java"


# static fields
.field private static volatile sEventLoggerStore:Lcom/google/android/searchcommon/EventLoggerStore;

.field private static sOneOffEvents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/logger/EventLogger;->sOneOffEvents:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getStore()Lcom/google/android/searchcommon/EventLoggerStore;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/logger/EventLogger;->sEventLoggerStore:Lcom/google/android/searchcommon/EventLoggerStore;

    return-object v0
.end method

.method public static init()V
    .locals 1

    invoke-static {}, Lcom/google/android/voicesearch/logger/store/EventLoggerStores;->createEventStore()Lcom/google/android/searchcommon/EventLoggerStore;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->setStore(Lcom/google/android/searchcommon/EventLoggerStore;)V

    return-void
.end method

.method public static logTextSearchStart(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const/16 v0, 0x9

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    const/4 v0, 0x3

    invoke-static {v0, p0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method private static record(IIILjava/lang/Object;Z)V
    .locals 4
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/Object;
    .param p4    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    const v1, 0xfffffff

    and-int/2addr v1, p0

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const v1, -0xf000001

    and-int/2addr v1, p1

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const/high16 v1, -0x1000000

    and-int/2addr v1, p2

    if-nez v1, :cond_2

    :goto_2
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    or-int v1, p0, p1

    or-int v0, v1, p2

    if-eqz p4, :cond_3

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->shouldLog(I)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_3
    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    sget-object v1, Lcom/google/android/voicesearch/logger/EventLogger;->sEventLoggerStore:Lcom/google/android/searchcommon/EventLoggerStore;

    invoke-interface {v1, v0, p3}, Lcom/google/android/searchcommon/EventLoggerStore;->recordEvent(ILjava/lang/Object;)V

    goto :goto_3
.end method

.method public static recordBreakdownEvent(I)V
    .locals 3
    .param p0    # I

    const/4 v2, 0x0

    const/high16 v0, 0x50000000

    const/4 v1, 0x0

    invoke-static {v0, v2, p0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->record(IIILjava/lang/Object;Z)V

    return-void
.end method

.method public static recordClientEvent(I)V
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    return-void
.end method

.method public static recordClientEvent(ILjava/lang/Object;)V
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    const/high16 v0, 0x10000000

    invoke-static {v0, v1, p0, p1, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->record(IIILjava/lang/Object;Z)V

    return-void
.end method

.method public static recordClientEventWithSource(IILjava/lang/Object;)V
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/high16 v0, 0x10000000

    const/4 v1, 0x0

    invoke-static {v0, p1, p0, p2, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->record(IIILjava/lang/Object;Z)V

    return-void
.end method

.method public static recordLatencyStart(I)V
    .locals 3
    .param p0    # I

    const/4 v2, 0x0

    const/high16 v0, 0x40000000

    const/4 v1, 0x0

    invoke-static {v0, v2, p0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->record(IIILjava/lang/Object;Z)V

    return-void
.end method

.method public static recordOneOffBreakdownEvent(I)V
    .locals 4
    .param p0    # I

    const/high16 v0, 0x50000000

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v1, p0, v2, v3}, Lcom/google/android/voicesearch/logger/EventLogger;->record(IIILjava/lang/Object;Z)V

    return-void
.end method

.method public static recordScreen(I)V
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordScreen(ILjava/lang/Object;)V

    return-void
.end method

.method public static recordScreen(ILjava/lang/Object;)V
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    const/high16 v0, 0x30000000

    invoke-static {v0, v1, p0, p1, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->record(IIILjava/lang/Object;Z)V

    return-void
.end method

.method public static recordSpeechEvent(I)V
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    return-void
.end method

.method public static recordSpeechEvent(ILjava/lang/Object;)V
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    const/high16 v0, 0x20000000

    invoke-static {v0, v1, p0, p1, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->record(IIILjava/lang/Object;Z)V

    return-void
.end method

.method public static resetOneOff()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/logger/EventLogger;->sOneOffEvents:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method public static setStore(Lcom/google/android/searchcommon/EventLoggerStore;)V
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/EventLoggerStore;

    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/EventLoggerStore;

    sput-object v0, Lcom/google/android/voicesearch/logger/EventLogger;->sEventLoggerStore:Lcom/google/android/searchcommon/EventLoggerStore;

    return-void
.end method

.method private static declared-synchronized shouldLog(I)Z
    .locals 3
    .param p0    # I

    const-class v1, Lcom/google/android/voicesearch/logger/EventLogger;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/voicesearch/logger/EventLogger;->sOneOffEvents:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/voicesearch/logger/EventLogger;->sOneOffEvents:Landroid/util/SparseArray;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
