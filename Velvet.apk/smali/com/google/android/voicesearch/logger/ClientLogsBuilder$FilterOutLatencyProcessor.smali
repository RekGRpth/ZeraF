.class public Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FilterOutLatencyProcessor"
.end annotation


# instance fields
.field private mActivityCreateTimestamp:J

.field private mActivityRestartTimestamp:J

.field private mApplicationCreateTimestamp:J

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 0
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method


# virtual methods
.method internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 10
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/4 v0, 0x1

    const-wide/16 v8, 0x1388

    const-wide/16 v6, 0x0

    const/high16 v5, 0x40000000

    const/4 v1, 0x3

    invoke-virtual {p1, v5, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mApplicationCreateTimestamp:J

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p1, v5, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_1

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityCreateTimestamp:J

    iget-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mApplicationCreateTimestamp:J

    add-long/2addr v1, v8

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iput-wide v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mApplicationCreateTimestamp:J

    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x7

    invoke-virtual {p1, v5, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_2

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityRestartTimestamp:J

    iget-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityCreateTimestamp:J

    add-long/2addr v1, v8

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    iput-wide v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityCreateTimestamp:J

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    invoke-virtual {p1, v5, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityCreateTimestamp:J

    add-long/2addr v1, v8

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_3

    iget-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityRestartTimestamp:J

    add-long/2addr v1, v8

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_4

    :cond_3
    iput-wide v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityCreateTimestamp:J

    iput-wide v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;->mActivityRestartTimestamp:J

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic process(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-super {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->process(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z

    move-result v0

    return v0
.end method
