.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactInfoProcessor"
.end annotation


# instance fields
.field private mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 0
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 7
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/16 v6, 0xd

    const/high16 v5, 0x10000000

    const/high16 v1, 0x20000000

    invoke-virtual {p1, v1, v6}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/speech/contacts/Contact;

    if-eqz v1, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mContactSyncLookup:Lcom/google/android/speech/contacts/ContactSyncLookup;
    invoke-static {v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Lcom/google/android/speech/contacts/ContactSyncLookup;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/speech/contacts/ContactSyncLookup;->isSynced(J)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;->setSyncedContact(Z)Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getGrammarWeight()D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getGrammarWeight()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;->setGrammarWeight(D)Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->isMerged()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;->setSource(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    :cond_0
    :goto_0
    const/16 v1, 0xe

    invoke-virtual {p1, v5, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1, v5, v6}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x32

    invoke-virtual {p1, v5, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    invoke-virtual {p2, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setContactInfo(Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    :cond_2
    const/4 v1, 0x0

    return v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;->setSource(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;->mContactInfo:Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;->setSource(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ContactInfo;

    goto :goto_0
.end method
