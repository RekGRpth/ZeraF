.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenProcessor"
.end annotation


# instance fields
.field private mLastScreen:I

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 1
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->mLastScreen:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
    .param p3    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 6
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/high16 v5, 0x10000000

    invoke-virtual {p1, v5, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x2

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x23

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x29

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x37

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x3b

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x3c

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x3e

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x3d

    invoke-virtual {p1, v5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    iput v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->mLastScreen:I

    :cond_1
    :goto_0
    return v2

    :cond_2
    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v4

    const/high16 v5, 0x30000000

    if-ne v4, v5, :cond_1

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {p2, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEventType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v2

    new-instance v4, Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    invoke-direct {v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;-><init>()V

    iget v5, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->mLastScreen:I

    invoke-virtual {v4, v5}, Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;->setFrom(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;->setTo(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setScreenTransition(Lcom/google/speech/logs/VoicesearchClientLogProto$ScreenTransitionData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v2

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setClientTimeMs(J)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Integer;

    if-eqz v2, :cond_3

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setActionType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_3
    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1200(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1200(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_4
    iput v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->mLastScreen:I

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v2, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    move v2, v3

    goto :goto_0
.end method
