.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplicationNameProcessor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 0
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
    .param p3    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/4 v3, 0x0

    const/high16 v2, 0x10000000

    const/4 v1, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    const-string v2, "voice-search"

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v2, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEventType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setClientTimeMs(J)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_1
    const/16 v0, 0x23

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    const-string v3, "voice-ime"

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v0, 0x29

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    const/16 v0, 0x37

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    const-string v3, "service-api"

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    move v0, v1

    goto :goto_0

    :cond_4
    const/16 v0, 0x3b

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x3c

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0

    :cond_6
    const/16 v0, 0x3d

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    const-string v3, "intent-api"

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    move v0, v1

    goto/16 :goto_0

    :cond_7
    const/16 v0, 0x3e

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0x4d

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    const-string v3, "hands-free"

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    move v0, v1

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0x4e

    invoke-virtual {p1, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v0, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
