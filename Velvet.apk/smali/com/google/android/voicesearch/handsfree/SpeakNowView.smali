.class Lcom/google/android/voicesearch/handsfree/SpeakNowView;
.super Landroid/widget/FrameLayout;
.source "SpeakNowView.java"

# interfaces
.implements Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;


# instance fields
.field private mController:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

.field private final mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

.field private final mText:Landroid/widget/TextView;

.field private final mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x7f040055

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f100059

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mTitle:Landroid/widget/TextView;

    const v2, 0x7f10011b

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mText:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-direct {v2, v1}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;-><init>(Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    return-void
.end method

.method private setText(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private setTitle(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method


# virtual methods
.method public setController(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mController:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mController:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->setLanguage(Ljava/lang/String;)V

    return-void
.end method

.method public showListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->showListening()V

    return-void
.end method

.method public showNotListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->showNotListening()V

    return-void
.end method

.method public showStart()V
    .locals 1

    const v0, 0x7f0d0470

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->setTitle(I)V

    const v0, 0x7f0d0471

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowView;->setText(I)V

    return-void
.end method
