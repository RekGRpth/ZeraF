.class public Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;
.super Ljava/lang/Object;
.source "RecognizerViewHelper.java"


# instance fields
.field private final mLanguage:Landroid/widget/TextView;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f100119

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->mView:Landroid/view/View;

    const v0, 0x7f10011a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->mLanguage:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->mLanguage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showListening()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->mView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->mView:Landroid/view/View;

    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method public showNotListening()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->mView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->mView:Landroid/view/View;

    const v1, 0x7f090063

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method
