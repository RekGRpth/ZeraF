.class Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;
.super Landroid/os/AsyncTask;
.source "AsyncContactRetriever.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RetrieveContactTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/speech/contacts/Contact;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mActionContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;"
        }
    .end annotation
.end field

.field private final mListener:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;

.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;Ljava/util/List;Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;)V
    .locals 0
    .param p3    # Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;",
            "Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->this$0:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->mActionContacts:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->mListener:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 4
    .param p1    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->this$0:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

    # getter for: Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;->mContactRetriever:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;->access$000(Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/contacts/ContactLookup$Mode;->PHONE_NUMBER:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->mActionContacts:Ljava/util/List;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/speech/contacts/ContactLookup;->findAllByDisplayName(Lcom/google/android/speech/contacts/ContactLookup$Mode;Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->mListener:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;->onNoContact()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->mListener:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;->onContacts(Ljava/util/List;)V

    goto :goto_0
.end method
