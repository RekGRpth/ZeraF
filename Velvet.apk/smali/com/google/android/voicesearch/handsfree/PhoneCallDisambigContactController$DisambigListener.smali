.class Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "PhoneCallDisambigContactController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DisambigListener"
.end annotation


# instance fields
.field private mResponseDispatched:Z

.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->mResponseDispatched:Z

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->mResponseDispatched:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->mResponseDispatched:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->handleVoiceErrror()V

    :cond_0
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mListening:Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$102(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Z)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$200(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;->showNotListening()V

    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->handleVoiceErrror()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->mResponseDispatched:Z

    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mListening:Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$102(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Z)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->handleVoiceErrror()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->mResponseDispatched:Z

    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mListening:Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$102(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Z)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$200(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;->showListening()V

    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->mResponseDispatched:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$300(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->handleRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;->mResponseDispatched:Z

    :cond_0
    return-void
.end method
