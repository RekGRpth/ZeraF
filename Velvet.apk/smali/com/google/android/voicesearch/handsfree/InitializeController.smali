.class Lcom/google/android/voicesearch/handsfree/InitializeController;
.super Ljava/lang/Object;
.source "InitializeController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/InitializeController$Ui;,
        Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;,
        Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;,
        Lcom/google/android/voicesearch/handsfree/InitializeController$Init;
    }
.end annotation


# instance fields
.field private final mAudioRouter:Lcom/google/android/voicesearch/handsfree/AudioRouter;

.field private final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<",
            "Lcom/google/android/voicesearch/handsfree/InitializeController$Init;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field private mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

.field private final mMainExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

.field private mPlayingTts:Z

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private final mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

.field private mUi:Lcom/google/android/voicesearch/handsfree/InitializeController$Ui;

.field private final mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/AudioRouter;Landroid/content/res/Resources;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/embedded/OfflineActionsManager;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/handsfree/AudioRouter;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p4    # Lcom/google/android/voicesearch/handsfree/ViewDisplayer;
    .param p5    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p6    # Lcom/google/android/speech/embedded/OfflineActionsManager;
    .param p7    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p8    # Ljava/util/concurrent/ScheduledExecutorService;
    .param p9    # Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .param p10    # Lcom/google/android/voicesearch/settings/Settings;

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "InitializeController"

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->NOTHING:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->newBuilder(Ljava/lang/String;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->NOTHING:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->AUDIO_ROUTE:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->NOTHING:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->AUDIO_ROUTE:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->AUDIO_ROUTE:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRAMMAR_COMPILED:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setDebug(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setSingleThreadOnly(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setStrictMode(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->build()Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mAudioRouter:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    iput-object p7, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p5, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iput-object p6, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iput-object p10, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p9, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/handsfree/InitializeController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->showInitialization()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/handsfree/InitializeController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->handleTtsInitializationCompletion()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/handsfree/InitializeController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->handleGrammarCompilationSuccess()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/handsfree/InitializeController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->handleGrammarCompilationError()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/voicesearch/handsfree/InitializeController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->handleAudioRouteEstablished()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/handsfree/InitializeController;)Lcom/google/android/voicesearch/handsfree/MainController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/InitializeController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-object v0
.end method

.method private compileGrammar()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v3, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v2, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResourcesForCompilation(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "en-US"

    :cond_0
    new-instance v1, Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;-><init>(Lcom/google/android/voicesearch/handsfree/InitializeController;Lcom/google/android/voicesearch/handsfree/InitializeController$1;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-static {v2, v1}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/callback/SimpleCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/speech/embedded/Greco3Grammar;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/speech/embedded/Greco3Grammar;->CONTACT_DIALING:Lcom/google/android/speech/embedded/Greco3Grammar;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/speech/embedded/Greco3Grammar;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/embedded/Greco3Grammar;

    aput-object v5, v3, v4

    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/speech/embedded/OfflineActionsManager;->startOfflineDataCheck(Lcom/google/android/speech/callback/SimpleCallback;Ljava/lang/String;[Lcom/google/android/speech/embedded/Greco3Grammar;)V

    return-void
.end method

.method private handleAudioRouteEstablished()V
    .locals 4

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->AUDIO_ROUTE:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->compileGrammar()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/InitializeController$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/handsfree/InitializeController$1;-><init>(Lcom/google/android/voicesearch/handsfree/InitializeController;)V

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private handleGrammarCompilationError()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mPlayingTts:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playErrorSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    :cond_0
    return-void
.end method

.method private handleGrammarCompilationSuccess()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRAMMAR_COMPILED:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mPlayingTts:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->startSpeakNow()V

    :cond_0
    return-void
.end method

.method private handleTtsInitializationCompletion()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mPlayingTts:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playErrorSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRAMMAR_COMPILED:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->startSpeakNow()V

    goto :goto_0
.end method

.method private showInitialization()V
    .locals 5

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRAMMAR_COMPILED:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mUi:Lcom/google/android/voicesearch/handsfree/InitializeController$Ui;

    const v2, 0x7f0d0433

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/handsfree/InitializeController$Ui;->setMessage(I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mPlayingTts:Z

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v2, Lcom/google/android/voicesearch/handsfree/InitializeController$2;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/handsfree/InitializeController$2;-><init>(Lcom/google/android/voicesearch/handsfree/InitializeController;)V

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    const v2, 0x7f0d0465

    const-string v3, "init-tts"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(ILjava/lang/String;ILjava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/MainController;

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-void
.end method

.method public start()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mInitMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->NOTHING:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->showInitialize()Lcom/google/android/voicesearch/handsfree/InitializeController$Ui;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mUi:Lcom/google/android/voicesearch/handsfree/InitializeController$Ui;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController;->mAudioRouter:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;-><init>(Lcom/google/android/voicesearch/handsfree/InitializeController;Lcom/google/android/voicesearch/handsfree/InitializeController$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/handsfree/AudioRouter;->establishRoute(Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;)V

    return-void
.end method
