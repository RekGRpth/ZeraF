.class Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
.super Ljava/lang/Object;
.source "PhoneCallDisambigContactController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$1;,
        Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;,
        Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;,
        Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;
    }
.end annotation


# instance fields
.field private mContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

.field private mListening:Z

.field private mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

.field private final mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

.field private final mResources:Landroid/content/res/Resources;

.field private mSameContact:Z

.field private final mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

.field private mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

.field private final mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Landroid/content/res/Resources;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Lcom/google/android/voicesearch/handsfree/ViewDisplayer;
    .param p4    # Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mResources:Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$1;)V

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;-><init>(Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    iput-object p3, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    iput-object p4, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mListening:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;

    return-object v0
.end method

.method private buildDifferentContactsChoiceText(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0d037e

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    add-int/lit8 v7, v1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, v7}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0d0381

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private buildOneContactChoiceText(Ljava/util/List;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v8, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v3}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    iget-object v3, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0d037f

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, v1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, v7}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0d0381

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private trim(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public cancelByTouch()V
    .locals 3

    const/16 v0, 0x2d

    const/high16 v1, 0x1000000

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method

.method public cancelByVoice()V
    .locals 3

    const/16 v0, 0x2d

    const/high16 v1, 0x2000000

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playHandsFreeShutDownSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method

.method public createView()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->showPhoneCallDisambigContact()Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;

    iget-boolean v2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mSameContact:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;->setContacts(Ljava/util/List;Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    invoke-interface {v0, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;->setController(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/handsfree/MainController;->getSpokenLanguageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;->setLanguage(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mSameContact:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;->setTitle(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mListening:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;->showListening()V

    :cond_1
    return-void
.end method

.method public handleVoiceErrror()V
    .locals 4

    const v3, 0x7f0d0464

    const/16 v0, 0x2d

    const/high16 v1, 0x3000000

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/voicesearch/handsfree/MainController;->showError(II)V

    return-void
.end method

.method public pickContactByTouch(Lcom/google/android/speech/contacts/Contact;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    const/high16 v3, 0x1000000

    const/16 v2, 0xa

    const/16 v0, 0x2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    const/16 v0, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getCallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/handsfree/MainController;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method

.method public pickContactByVoice(Lcom/google/android/speech/contacts/Contact;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    const/16 v0, 0x2c

    const/high16 v1, 0x2000000

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/MainController;->callContact(Lcom/google/android/speech/contacts/Contact;)V

    return-void
.end method

.method public setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/handsfree/MainController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/MainController;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->trim(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mListening:Z

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->containsSameContactName(Ljava/util/List;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mSameContact:Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->createView()V

    iget-boolean v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mSameContact:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->buildOneContactChoiceText(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    new-instance v2, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$DisambigListener;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)V

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startCommandRecognitionNoUi(Lcom/google/android/speech/listeners/RecognitionEventListener;ILjava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->buildDifferentContactsChoiceText(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
