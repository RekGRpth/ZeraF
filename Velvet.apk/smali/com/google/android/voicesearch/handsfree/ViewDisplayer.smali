.class public Lcom/google/android/voicesearch/handsfree/ViewDisplayer;
.super Ljava/lang/Object;
.source "ViewDisplayer.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mViewGroup:Landroid/view/ViewGroup;

    return-void
.end method

.method private addView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public showInitialize()Lcom/google/android/voicesearch/handsfree/InitializeController$Ui;
    .locals 2

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InitializeView;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/InitializeView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public showNoMatch()Lcom/google/android/voicesearch/handsfree/ErrorController$Ui;
    .locals 2

    new-instance v0, Lcom/google/android/voicesearch/handsfree/ErrorView;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/ErrorView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public showPhoneCallContact()Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;
    .locals 2

    const/16 v1, 0xe

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordScreen(I)V

    new-instance v0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactView;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public showPhoneCallDisambigContact()Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;
    .locals 2

    const/16 v1, 0x13

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordScreen(I)V

    new-instance v0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public showPhoneCallNumber()Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;
    .locals 2

    new-instance v0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x20

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordScreen(I)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public showSpeakNow()Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;
    .locals 2

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordScreen(I)V

    new-instance v0, Lcom/google/android/voicesearch/handsfree/SpeakNowView;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/SpeakNowView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->addView(Landroid/view/View;)V

    return-object v0
.end method
