.class Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;
.super Ljava/lang/Object;
.source "PhoneCallContactController.java"

# interfaces
.implements Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InterpretationProcessorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;
    .param p2    # Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->cancelByVoice()V

    return-void
.end method

.method public onConfirm()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->callContactByVoice()V

    return-void
.end method

.method public onSelect(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->handleVoiceError()V

    return-void
.end method
