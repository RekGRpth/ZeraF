.class Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "PhoneCallContactController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecognizerListener"
.end annotation


# instance fields
.field private mResponseDispatched:Z

.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->mResponseDispatched:Z

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->mResponseDispatched:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->mResponseDispatched:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->handleVoiceError()V

    :cond_0
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->access$100(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;->showNotListening()V

    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->handleVoiceError()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->mResponseDispatched:Z

    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->handleVoiceError()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->mResponseDispatched:Z

    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->access$100(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;->showListening()V

    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->mResponseDispatched:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->access$200(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->handleRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;->mResponseDispatched:Z

    :cond_0
    return-void
.end method
