.class Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;
.super Ljava/lang/Object;
.source "AsyncContactRetriever.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;,
        Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mContactRetriever:Lcom/google/android/speech/contacts/ContactLookup;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/contacts/ContactLookup;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/contacts/ContactLookup;
    .param p2    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;->mContactRetriever:Lcom/google/android/speech/contacts/ContactLookup;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;)Lcom/google/android/speech/contacts/ContactLookup;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;->mContactRetriever:Lcom/google/android/speech/contacts/ContactLookup;

    return-object v0
.end method


# virtual methods
.method public start(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;)V
    .locals 4
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PhoneAction;
    .param p2    # Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    invoke-static {p1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getSpokenNumber(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/speech/contacts/Contact;->newPhoneNumberOnlyContact(Ljava/lang/String;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/speech/contacts/Contact;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;->onContacts(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, p0, v2, p2}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;-><init>(Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;Ljava/util/List;Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$RetrieveContactTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
