.class public Lcom/google/android/voicesearch/hotword/HotwordDetector;
.super Ljava/lang/Object;
.source "HotwordDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/hotword/HotwordDetector$1;,
        Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;,
        Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;
    }
.end annotation


# instance fields
.field private final mAccessibilityService:Landroid/view/accessibility/AccessibilityManager;

.field private mActive:Z

.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

.field private final mMainThreadExecutor:Ljava/util/concurrent/Executor;

.field private mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private final mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStarted:Z

.field private final mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

.field private final mVss:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;Landroid/content/Context;Lcom/google/android/voicesearch/settings/Settings;Ljava/util/concurrent/Executor;Lcom/google/common/base/Supplier;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;
    .param p4    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/VoiceSearchServices;",
            "Landroid/content/Context;",
            "Lcom/google/android/voicesearch/settings/Settings;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mVss:Lcom/google/android/voicesearch/VoiceSearchServices;

    iput-object p2, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p4, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mContext:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mAccessibilityService:Landroid/view/accessibility/AccessibilityManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/hotword/HotwordDetector;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/hotword/HotwordDetector;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/hotword/HotwordDetector;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/voicesearch/hotword/HotwordDetector;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/hotword/HotwordDetector;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mActive:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/hotword/HotwordDetector;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/hotword/HotwordDetector;

    invoke-direct {p0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->internalStop()V

    return-void
.end method

.method private canStartHotword()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->isHotwordDetectorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->isSpokenFeedbackEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createHotwordRecognizerParams(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParams;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->HOTWORD:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setGreco3Mode(Lcom/google/android/speech/embedded/Greco3Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    return-object v0
.end method

.method private ensureRecognizerParamsFactory()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mVss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizerParamsFactory()Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    :cond_0
    return-void
.end method

.method private internalStart()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-boolean v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mActive:Z

    if-nez v1, :cond_0

    move v3, v2

    :cond_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->canStartHotword()Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v4, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    :goto_1
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    iput-boolean v2, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->ensureRecognizerParamsFactory()V

    iget-object v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mVss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->createHotwordRecognizerParams(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v2

    new-instance v3, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;

    invoke-direct {v3, p0, v4}, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;-><init>(Lcom/google/android/voicesearch/hotword/HotwordDetector;Lcom/google/android/voicesearch/hotword/HotwordDetector$1;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/speech/Recognizer;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method private internalStop()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    iget-object v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mVss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/Recognizer;->cancel()V

    iput-boolean v2, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mActive:Z

    iput-boolean v2, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;->onHotwordDetectorStopped()V

    :cond_0
    return-void
.end method

.method private isMusicActive()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    return v0
.end method

.method private isSpokenFeedbackEnabled()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mAccessibilityService:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mAccessibilityService:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public start(Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    if-ne v0, p1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    invoke-direct {p0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->internalStart()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->internalStop()V

    :cond_0
    return-void
.end method
