.class Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;
.super Landroid/os/AsyncTask;
.source "ContactSelectItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/contacts/ContactSelectItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchThumbnailForBadgeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Landroid/graphics/drawable/Drawable$ConstantState;",
        ">;"
    }
.end annotation


# instance fields
.field private final mBadge:Landroid/widget/QuickContactBadge;

.field final synthetic this$0:Lcom/google/android/voicesearch/contacts/ContactSelectItem;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/contacts/ContactSelectItem;Landroid/widget/QuickContactBadge;)V
    .locals 0
    .param p2    # Landroid/widget/QuickContactBadge;

    iput-object p1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->mBadge:Landroid/widget/QuickContactBadge;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Long;)Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 7
    .param p1    # [Ljava/lang/Long;

    const/4 v6, 0x0

    aget-object v3, p1, v6

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v3}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$000(Lcom/google/android/voicesearch/contacts/ContactSelectItem;)Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchPhotoBitmap(Landroid/content/ContentResolver;JZ)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->sContactIdToThumbnail:Landroid/util/LruCache;
    invoke-static {}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$200()Landroid/util/LruCache;

    move-result-object v3

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->NULL_THUMBNAIL:Landroid/graphics/drawable/Drawable$ConstantState;
    invoke-static {}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$100()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->NULL_THUMBNAIL:Landroid/graphics/drawable/Drawable$ConstantState;
    invoke-static {}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$100()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mResources:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$300(Lcom/google/android/voicesearch/contacts/ContactSelectItem;)Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->sContactIdToThumbnail:Landroid/util/LruCache;
    invoke-static {}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$200()Landroid/util/LruCache;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->doInBackground([Ljava/lang/Long;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable$ConstantState;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->mBadge:Landroid/widget/QuickContactBadge;

    # invokes: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setThumbnail(Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$400(Lcom/google/android/voicesearch/contacts/ContactSelectItem;Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mOutstandingTask:Landroid/os/AsyncTask;
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->access$502(Lcom/google/android/voicesearch/contacts/ContactSelectItem;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->onPostExecute(Landroid/graphics/drawable/Drawable$ConstantState;)V

    return-void
.end method
