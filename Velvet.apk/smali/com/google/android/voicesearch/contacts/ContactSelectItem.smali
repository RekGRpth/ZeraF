.class public Lcom/google/android/voicesearch/contacts/ContactSelectItem;
.super Landroid/widget/RelativeLayout;
.source "ContactSelectItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;
    }
.end annotation


# static fields
.field private static final NULL_THUMBNAIL:Landroid/graphics/drawable/Drawable$ConstantState;

.field private static final sContactIdToThumbnail:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContact:Lcom/google/android/speech/contacts/Contact;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private mOutstandingTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->sContactIdToThumbnail:Landroid/util/LruCache;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectItem$2;

    invoke-direct {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectItem$2;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->NULL_THUMBNAIL:Landroid/graphics/drawable/Drawable$ConstantState;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/contacts/ContactSelectItem;)Landroid/content/ContentResolver;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$100()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->NULL_THUMBNAIL:Landroid/graphics/drawable/Drawable$ConstantState;

    return-object v0
.end method

.method static synthetic access$200()Landroid/util/LruCache;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->sContactIdToThumbnail:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/contacts/ContactSelectItem;)Landroid/content/res/Resources;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/contacts/ContactSelectItem;Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectItem;
    .param p1    # Landroid/widget/QuickContactBadge;
    .param p2    # Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setThumbnail(Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V

    return-void
.end method

.method static synthetic access$502(Lcom/google/android/voicesearch/contacts/ContactSelectItem;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectItem;
    .param p1    # Landroid/os/AsyncTask;

    iput-object p1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mOutstandingTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method private cancelOutstandingTask()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mOutstandingTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mOutstandingTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mOutstandingTask:Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method private setQuickContactBadgeImage(Landroid/widget/QuickContactBadge;Lcom/google/android/speech/contacts/Contact;)V
    .locals 6
    .param p1    # Landroid/widget/QuickContactBadge;
    .param p2    # Lcom/google/android/speech/contacts/Contact;

    sget-object v1, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->sContactIdToThumbnail:Landroid/util/LruCache;

    invoke-virtual {p2}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setThumbnail(Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/widget/QuickContactBadge;->setImageToDefault()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->cancelOutstandingTask()V

    new-instance v1, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;

    invoke-direct {v1, p0, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;-><init>(Lcom/google/android/voicesearch/contacts/ContactSelectItem;Landroid/widget/QuickContactBadge;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/contacts/ContactSelectItem$FetchThumbnailForBadgeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mOutstandingTask:Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private setTextViewText(ILjava/lang/CharSequence;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private setThumbnail(Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 1
    .param p1    # Landroid/widget/QuickContactBadge;
    .param p2    # Landroid/graphics/drawable/Drawable$ConstantState;

    sget-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->NULL_THUMBNAIL:Landroid/graphics/drawable/Drawable$ConstantState;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/QuickContactBadge;->setImageToDefault()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/QuickContactBadge;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public getContact()Lcom/google/android/speech/contacts/Contact;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mContact:Lcom/google/android/speech/contacts/Contact;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->cancelOutstandingTask()V

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;

    const v0, 0x7f10006a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/contacts/ContactSelectItem$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem$1;-><init>(Lcom/google/android/voicesearch/contacts/ContactSelectItem;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setContact(Lcom/google/android/speech/contacts/Contact;I)V
    .locals 9
    .param p1    # Lcom/google/android/speech/contacts/Contact;
    .param p2    # I

    const v8, 0x7f10006c

    const v7, 0x7f10006b

    const/16 v6, 0x8

    iput-object p1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->mContact:Lcom/google/android/speech/contacts/Contact;

    const v4, 0x7f100061

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v8}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v7}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const v4, 0x7f10006a

    invoke-virtual {p0, v4}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-nez p2, :cond_3

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    const v4, 0x7f100069

    invoke-virtual {p0, v4}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/QuickContactBadge;

    if-eqz v1, :cond_0

    invoke-direct {p0, v1, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setQuickContactBadgeImage(Landroid/widget/QuickContactBadge;Lcom/google/android/speech/contacts/Contact;)V

    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v8, v3}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v7}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v7, v2}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
