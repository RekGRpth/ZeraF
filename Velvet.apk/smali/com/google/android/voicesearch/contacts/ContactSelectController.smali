.class public Lcom/google/android/voicesearch/contacts/ContactSelectController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "ContactSelectController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;,
        Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;,
        Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

.field private final mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

.field private mContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private mLogDismissInDetach:Z

.field private mMode:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

.field private mQueryText:Ljava/lang/String;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private mUseBailOutAction:Z


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/speech/contacts/ContactLookup;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/speech/contacts/ContactLookup;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mLogDismissInDetach:Z

    iput-object p2, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    iput-object p3, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/contacts/ContactSelectController;)Lcom/google/android/voicesearch/contacts/ContactSelectMode;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mMode:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/contacts/ContactSelectController;)Lcom/google/android/speech/contacts/ContactLookup;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/contacts/ContactSelectController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->showNoCard()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/contacts/ContactSelectController;)Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/contacts/ContactSelectController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->showNoCard()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/contacts/ContactSelectController;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactSelectController;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->selectContact(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method private selectContact(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mQueryText:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContacts:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->showCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    invoke-interface {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;->onShowingDisambiguation()V

    return-void
.end method


# virtual methods
.method public detach()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mLogDismissInDetach:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x2d

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mLogDismissInDetach:Z

    :cond_0
    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->detach()V

    return-void
.end method

.method protected getActionTypeLog()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mMode:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getParentActionType()I

    move-result v0

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x13

    return v0
.end method

.method public handleBailOut()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mUseBailOutAction:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    invoke-interface {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;->onBailOut()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->bailOut()V

    goto :goto_0
.end method

.method public initUi()V
    .locals 6

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContacts:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;

    iget-object v3, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mQueryText:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContacts:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContacts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/android/speech/contacts/Contact;

    invoke-interface {v1, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/speech/contacts/Contact;

    iget-object v4, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mMode:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    iget-object v5, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v5}, Lcom/google/android/voicesearch/settings/Settings;->getTempConfiguration()Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;->getMaximumNumberOfContactsForDisambiguate()I

    move-result v5

    invoke-interface {v0, v3, v1, v4, v5}, Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;->setContacts(Ljava/lang/String;[Lcom/google/android/speech/contacts/Contact;Lcom/google/android/voicesearch/contacts/ContactSelectMode;I)V

    iget-boolean v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mUseBailOutAction:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mMode:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;->setBailOutAction(Lcom/google/android/voicesearch/contacts/ContactSelectMode;)V

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mLogDismissInDetach:Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;->showFindContactAction()V

    goto :goto_1
.end method

.method protected internalBailOut()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getShowContactIntent(Lcom/google/android/speech/contacts/Contact;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V
    .locals 2
    .param p1    # Lcom/google/android/speech/contacts/Contact;
    .param p2    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mLogDismissInDetach:Z

    const/16 v0, 0x2c

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->clearCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;->onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V

    return-void
.end method

.method public pickContact(Ljava/util/List;Lcom/google/android/voicesearch/contacts/ContactSelectMode;ZLjava/lang/String;Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;)V
    .locals 2
    .param p2    # Lcom/google/android/voicesearch/contacts/ContactSelectMode;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;",
            "Lcom/google/android/voicesearch/contacts/ContactSelectMode;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mMode:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    iput-boolean p3, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mUseBailOutAction:Z

    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;

    invoke-direct {v0, p0, p4, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;-><init>(Lcom/google/android/voicesearch/contacts/ContactSelectController;Ljava/lang/String;Ljava/util/List;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
