.class public Lcom/google/android/voicesearch/speechservice/ImageResultData;
.super Ljava/lang/Object;
.source "ImageResultData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageResultData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mHeader:Ljava/lang/String;

.field private final mImageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnlyShowPeanut:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/speechservice/ImageResultData$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/speechservice/ImageResultData$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mHeader:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mOnlyShowPeanut:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mImageList:Ljava/util/ArrayList;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/voicesearch/speechservice/ImageResultData$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/voicesearch/speechservice/ImageResultData$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ImageResultData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/majel/proto/PeanutProtos$Peanut;)V
    .locals 4
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseHeader()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mHeader:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/majel/proto/PeanutProtos$Image;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mImageList:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    invoke-direct {v3, v1}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;-><init>(Lcom/google/majel/proto/PeanutProtos$Image;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getOnlyShowPeanutImageResponse()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mOnlyShowPeanut:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mHeader:Ljava/lang/String;

    return-object v0
.end method

.method public getImageList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mImageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onlyShowPeanut()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mOnlyShowPeanut:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mHeader:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mOnlyShowPeanut:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ImageResultData;->mImageList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
