.class public Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;
.super Ljava/lang/Object;
.source "VoiceSearchConnectionFactory.java"

# interfaces
.implements Lcom/google/android/speech/network/ConnectionFactory;


# instance fields
.field private final mDefaultFactory:Lcom/google/android/speech/network/ConnectionFactory;

.field private final mHttpsOnly:Z

.field private final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mSpdyFactory:Lcom/google/android/speech/network/ConnectionFactory;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/searchcommon/SearchConfig;Z)V
    .locals 0
    .param p1    # Lcom/google/android/speech/network/ConnectionFactory;
    .param p2    # Lcom/google/android/speech/network/ConnectionFactory;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mDefaultFactory:Lcom/google/android/speech/network/ConnectionFactory;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mSpdyFactory:Lcom/google/android/speech/network/ConnectionFactory;

    iput-object p3, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-boolean p4, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mHttpsOnly:Z

    return-void
.end method

.method private isSpdyEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isSpdyEnabled()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Ljava/net/HttpURLConnection;
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/net/URL;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method public openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .param p2    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mHttpsOnly:Z

    if-eqz v0, :cond_0

    const-string v0, "https"

    invoke-virtual {p2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempt to use http with a https only factory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "https"

    invoke-virtual {p2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->isSpdyEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mSpdyFactory:Lcom/google/android/speech/network/ConnectionFactory;

    invoke-interface {v0, p1, p2}, Lcom/google/android/speech/network/ConnectionFactory;->openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mDefaultFactory:Lcom/google/android/speech/network/ConnectionFactory;

    invoke-interface {v0, p1, p2}, Lcom/google/android/speech/network/ConnectionFactory;->openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    goto :goto_0
.end method

.method public openSocket(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;)Ljava/net/Socket;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;->mDefaultFactory:Lcom/google/android/speech/network/ConnectionFactory;

    invoke-interface {v0, p1}, Lcom/google/android/speech/network/ConnectionFactory;->openSocket(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method
