.class public Lcom/google/android/voicesearch/speechservice/ImageParcelable;
.super Ljava/lang/Object;
.source "ImageParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mImage:Lcom/google/majel/proto/PeanutProtos$Image;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/speechservice/ImageParcelable$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/speechservice/ImageParcelable$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    :try_start_0
    new-instance v2, Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-direct {v2}, Lcom/google/majel/proto/PeanutProtos$Image;-><init>()V

    iput-object v2, p0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->mImage:Lcom/google/majel/proto/PeanutProtos$Image;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->mImage:Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/PeanutProtos$Image;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot parse image from bytes "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/voicesearch/speechservice/ImageParcelable$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/voicesearch/speechservice/ImageParcelable$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/majel/proto/PeanutProtos$Image;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->mImage:Lcom/google/majel/proto/PeanutProtos$Image;

    return-void
.end method

.method public static createDefaultInstance()Lcom/google/android/voicesearch/speechservice/ImageParcelable;
    .locals 2

    new-instance v0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Image;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;-><init>(Lcom/google/majel/proto/PeanutProtos$Image;)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getImage()Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->mImage:Lcom/google/majel/proto/PeanutProtos$Image;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->mImage:Lcom/google/majel/proto/PeanutProtos$Image;

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Image;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
