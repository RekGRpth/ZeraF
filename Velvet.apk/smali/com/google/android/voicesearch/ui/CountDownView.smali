.class public Lcom/google/android/voicesearch/ui/CountDownView;
.super Landroid/widget/RelativeLayout;
.source "CountDownView.java"


# instance fields
.field private mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

.field private mAppIcon:Landroid/widget/ImageView;

.field private mAppIconView:Landroid/view/ViewGroup;

.field private mCancelCountdownButton:Landroid/view/View;

.field private mConfirmBar:Landroid/view/View;

.field private mConfirmButton:Landroid/widget/TextView;

.field private mCountDownAnimator:Landroid/animation/ObjectAnimator;

.field private mCountDownBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/CountDownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f0e0047

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/CountDownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ui/CountDownView;)Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/CountDownView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    return-object v0
.end method


# virtual methods
.method public cancelCountDownAnimation()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCancelCountdownButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    const v0, 0x7f100082

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownBar:Landroid/widget/ProgressBar;

    const v0, 0x7f10007f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIconView:Landroid/view/ViewGroup;

    const v0, 0x7f100080

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIcon:Landroid/widget/ImageView;

    const v0, 0x7f10007d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mConfirmBar:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mConfirmBar:Landroid/view/View;

    new-instance v1, Lcom/google/android/voicesearch/ui/CountDownView$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/ui/CountDownView$1;-><init>(Lcom/google/android/voicesearch/ui/CountDownView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100081

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mConfirmButton:Landroid/widget/TextView;

    const v0, 0x7f10007e

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCancelCountdownButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCancelCountdownButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCancelCountdownButton:Landroid/view/View;

    new-instance v1, Lcom/google/android/voicesearch/ui/CountDownView$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/ui/CountDownView$2;-><init>(Lcom/google/android/voicesearch/ui/CountDownView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setActionEditorListener(Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    return-void
.end method

.method public setConfirmIcon(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/CountDownView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIconView:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIcon:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->setConfirmIcon(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setConfirmIcon(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIconView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIconView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setConfirmText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mConfirmButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setConfirmText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mConfirmButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setConfirmationEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mConfirmBar:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setNoConfirmIcon()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mAppIconView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public startCountDownAnimation(J)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/voicesearch/ui/CountDownView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCancelCountdownButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownBar:Landroid/widget/ProgressBar;

    const-class v1, Landroid/widget/ProgressBar;

    const-class v2, Ljava/lang/Integer;

    const-string v3, "progress"

    invoke-static {v1, v2, v3}, Landroid/util/Property;->of(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Landroid/util/Property;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v4, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->mCountDownAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method
