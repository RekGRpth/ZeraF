.class public Lcom/google/android/voicesearch/ui/AppSelectorView;
.super Landroid/widget/LinearLayout;
.source "AppSelectorView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;,
        Lcom/google/android/voicesearch/ui/AppSelectorView$AppSelectorAdapter;
    }
.end annotation


# instance fields
.field private mAppSelectorSpinner:Landroid/widget/Spinner;

.field private mOnAppSelectedListener:Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/AppSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/AppSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ui/AppSelectorView;)Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/AppSelectorView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/AppSelectorView;->mOnAppSelectedListener:Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;

    return-object v0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f1001d8

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/AppSelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/AppSelectorView;->mAppSelectorSpinner:Landroid/widget/Spinner;

    return-void
.end method

.method public setAppSelectorOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnTouchListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/AppSelectorView;->mAppSelectorSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public setOnAppSelectedListener(Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/AppSelectorView;->mOnAppSelectedListener:Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;

    return-void
.end method

.method public setSelectorApps(Ljava/util/List;I)V
    .locals 7
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;I)V"
        }
    .end annotation

    iget-object v6, p0, Lcom/google/android/voicesearch/ui/AppSelectorView;->mAppSelectorSpinner:Landroid/widget/Spinner;

    new-instance v0, Lcom/google/android/voicesearch/ui/AppSelectorView$AppSelectorAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/AppSelectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/AppSelectorView;->getId()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/AppSelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/ui/AppSelectorView$AppSelectorAdapter;-><init>(Lcom/google/android/voicesearch/ui/AppSelectorView;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;)V

    invoke-virtual {v6, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/AppSelectorView;->mAppSelectorSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/AppSelectorView;->mAppSelectorSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/android/voicesearch/ui/AppSelectorView$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/voicesearch/ui/AppSelectorView$1;-><init>(Lcom/google/android/voicesearch/ui/AppSelectorView;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method
