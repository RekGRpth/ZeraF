.class public Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;
.super Landroid/view/View;
.source "BitmapSoundLevelsView.java"


# instance fields
.field private mAnimator:Landroid/animation/TimeAnimator;

.field private final mCenterTranslationX:I

.field private final mCenterTranslationY:I

.field private mCurrentVolume:I

.field private mDestRect:Landroid/graphics/Rect;

.field private final mDisableBackgroundColor:I

.field private final mEmptyPaint:Landroid/graphics/Paint;

.field private final mEnableBackgroundColor:I

.field private mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private final mMinimumLevelSize:I

.field private mPeakLevel:I

.field private mPeakLevelCountDown:I

.field private final mPrimaryLevel:Landroid/graphics/Bitmap;

.field private final mTrailLevel:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mEmptyPaint:Landroid/graphics/Paint;

    sget-object v5, Lcom/google/android/googlequicksearchbox/R$styleable;->BitmapSoundLevelView:[I

    invoke-virtual {p1, p2, v5, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v5, "#66FFFFFF"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v6, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mEnableBackgroundColor:I

    invoke-virtual {v0, v8, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mDisableBackgroundColor:I

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f020190

    invoke-virtual {v0, v9, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    const/4 v2, 0x1

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v0, v10}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    if-eqz v5, :cond_1

    const v5, 0x7f020191

    invoke-virtual {v0, v10, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    const/4 v1, 0x1

    :cond_1
    const/4 v5, 0x5

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCenterTranslationX:I

    const/4 v5, 0x6

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCenterTranslationY:I

    const/4 v5, 0x4

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mMinimumLevelSize:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPrimaryLevel:Landroid/graphics/Bitmap;

    :goto_0
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mTrailLevel:Landroid/graphics/Bitmap;

    :goto_1
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mDestRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    new-instance v5, Lcom/google/android/speech/SpeechLevelSource;

    invoke-direct {v5}, Lcom/google/android/speech/SpeechLevelSource;-><init>()V

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v5, v6}, Lcom/google/android/speech/SpeechLevelSource;->setSpeechLevel(I)V

    new-instance v5, Landroid/animation/TimeAnimator;

    invoke-direct {v5}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v5, v7}, Landroid/animation/TimeAnimator;->setRepeatCount(I)V

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    new-instance v6, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView$1;

    invoke-direct {v6, p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView$1;-><init>(Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;)V

    invoke-virtual {v5, v6}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    return-void

    :cond_2
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPrimaryLevel:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mTrailLevel:Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method private startAnimator()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    :cond_0
    return-void
.end method

.method private stopAnimator()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    return-void
.end method

.method private updateAnimatorState()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->startAnimator()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->stopAnimator()V

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->updateAnimatorState()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->stopAnimator()V

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mEnableBackgroundColor:I

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v4}, Lcom/google/android/speech/SpeechLevelSource;->getSpeechLevel()I

    move-result v2

    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevel:I

    if-le v2, v4, :cond_2

    iput v2, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevel:I

    const/16 v4, 0x19

    iput v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevelCountDown:I

    :goto_0
    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCurrentVolume:I

    if-le v2, v4, :cond_4

    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCurrentVolume:I

    iget v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCurrentVolume:I

    sub-int v5, v2, v5

    div-int/lit8 v5, v5, 0x4

    add-int/2addr v4, v5

    iput v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCurrentVolume:I

    :goto_1
    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCenterTranslationX:I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v0, v4, v5

    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCenterTranslationY:I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v1, v4, v5

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mTrailLevel:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mMinimumLevelSize:I

    sub-int v4, v0, v4

    iget v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevel:I

    mul-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mMinimumLevelSize:I

    add-int v3, v4, v5

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mDestRect:Landroid/graphics/Rect;

    sub-int v5, v0, v3

    sub-int v6, v1, v3

    add-int v7, v0, v3

    add-int v8, v1, v3

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mTrailLevel:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mDestRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v9, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPrimaryLevel:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mMinimumLevelSize:I

    sub-int v4, v0, v4

    iget v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCurrentVolume:I

    mul-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mMinimumLevelSize:I

    add-int v3, v4, v5

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mDestRect:Landroid/graphics/Rect;

    sub-int v5, v0, v3

    sub-int v6, v1, v3

    add-int v7, v0, v3

    add-int v8, v1, v3

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPrimaryLevel:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mDestRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v9, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevelCountDown:I

    if-nez v4, :cond_3

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevel:I

    add-int/lit8 v5, v5, -0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevel:I

    goto/16 :goto_0

    :cond_3
    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevelCountDown:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mPeakLevelCountDown:I

    goto/16 :goto_0

    :cond_4
    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCurrentVolume:I

    int-to-float v4, v4

    const v5, 0x3f733333

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mCurrentVolume:I

    goto/16 :goto_1

    :cond_5
    iget v4, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mDisableBackgroundColor:I

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_2
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->updateAnimatorState()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->stopAnimator()V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->updateAnimatorState()V

    return-void
.end method

.method public setLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    return-void
.end method
