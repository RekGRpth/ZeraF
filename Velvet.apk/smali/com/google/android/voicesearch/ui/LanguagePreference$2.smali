.class Lcom/google/android/voicesearch/ui/LanguagePreference$2;
.super Ljava/lang/Object;
.source "LanguagePreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/ui/LanguagePreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/ui/LanguagePreference;

.field final synthetic val$languages:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/ui/LanguagePreference;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference$2;->this$0:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iput-object p2, p0, Lcom/google/android/voicesearch/ui/LanguagePreference$2;->val$languages:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference$2;->val$languages:[Ljava/lang/String;

    aget-object v0, v1, p2

    const-string v1, "\u2026"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const-string v3, "\u2026"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference$2;->this$0:Lcom/google/android/voicesearch/ui/LanguagePreference;

    # invokes: Lcom/google/android/voicesearch/ui/LanguagePreference;->processLanguageClicked(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->access$100(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
