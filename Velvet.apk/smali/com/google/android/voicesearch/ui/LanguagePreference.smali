.class public Lcom/google/android/voicesearch/ui/LanguagePreference;
.super Landroid/preference/ListPreference;
.source "LanguagePreference.java"


# instance fields
.field private mDialects:[Ljava/lang/String;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/LanguagePreference;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->processDialectClicked(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ui/LanguagePreference;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->processLanguageClicked(Ljava/lang/String;)V

    return-void
.end method

.method private getLanguages()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    const-string v1, "\u2026"

    invoke-static {v0, v1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getLanguageDisplayNames(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private processDialectClicked(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDialectByDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x42

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setValue(Ljava/lang/String;)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private processLanguageClicked(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDialectDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mDialects:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mDialects:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->processDialectClicked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->showDialog(Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 7
    .param p1    # Landroid/app/AlertDialog$Builder;

    const v6, 0x7f040098

    const/4 v5, -0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mDialects:[Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mDialects:[Ljava/lang/String;

    const v3, 0x7f0d03d1

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v6, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    new-instance v3, Lcom/google/android/voicesearch/ui/LanguagePreference$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference$1;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;[Ljava/lang/String;)V

    invoke-virtual {p1, v1, v5, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iput-object v4, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mDialects:[Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v4, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getLanguages()[Ljava/lang/String;

    move-result-object v2

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v6, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    new-instance v3, Lcom/google/android/voicesearch/ui/LanguagePreference$2;

    invoke-direct {v3, p0, v2}, Lcom/google/android/voicesearch/ui/LanguagePreference$2;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;[Ljava/lang/String;)V

    invoke-virtual {p1, v1, v5, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getOnPreferenceChangeListener()Landroid/preference/Preference$OnPreferenceChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getOnPreferenceChangeListener()Landroid/preference/Preference$OnPreferenceChangeListener;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
