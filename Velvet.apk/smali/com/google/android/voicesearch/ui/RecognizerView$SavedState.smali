.class public Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "RecognizerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ui/RecognizerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$State;->valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/ui/RecognizerView$State;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/voicesearch/ui/RecognizerView$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/voicesearch/ui/RecognizerView$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$State;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
