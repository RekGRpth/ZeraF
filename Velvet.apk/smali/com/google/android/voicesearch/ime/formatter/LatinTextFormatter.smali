.class public Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;
.super Ljava/lang/Object;
.source "LatinTextFormatter.java"

# interfaces
.implements Lcom/google/android/voicesearch/ime/formatter/TextFormatter;


# static fields
.field private static DEBUG:Z


# instance fields
.field private mFirstCharUppercase:Z

.field private mFirstCommit:Z

.field private upperCaseChars:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->upperCaseChars:Ljava/util/Set;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->upperCaseChars:Ljava/util/Set;

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->upperCaseChars:Ljava/util/Set;

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->upperCaseChars:Ljava/util/Set;

    const/16 v1, 0x3f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->upperCaseChars:Ljava/util/Set;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private forceFirstCharUppercase(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private formatAlternateSpan(Lcom/google/speech/common/Alternates$AlternateSpan;)Lcom/google/speech/common/Alternates$AlternateSpan;
    .locals 10
    .param p1    # Lcom/google/speech/common/Alternates$AlternateSpan;

    new-instance v9, Lcom/google/speech/common/Alternates$AlternateSpan;

    invoke-direct {v9}, Lcom/google/speech/common/Alternates$AlternateSpan;-><init>()V

    invoke-static {p1, v9}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v5

    check-cast v5, Lcom/google/speech/common/Alternates$AlternateSpan;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/speech/common/Alternates$AlternateSpan;->getAlternatesCount()I

    move-result v9

    if-ge v7, v9, :cond_1

    invoke-virtual {p1, v7}, Lcom/google/speech/common/Alternates$AlternateSpan;->getAlternates(I)Lcom/google/speech/common/Alternates$Alternate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/common/Alternates$Alternate;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->formatPartialResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/google/speech/common/Alternates$Alternate;

    invoke-direct {v9}, Lcom/google/speech/common/Alternates$Alternate;-><init>()V

    invoke-static {v1, v9}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/speech/common/Alternates$Alternate;

    invoke-virtual {v2, v6}, Lcom/google/speech/common/Alternates$Alternate;->setText(Ljava/lang/String;)Lcom/google/speech/common/Alternates$Alternate;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Lcom/google/speech/common/Alternates$AlternateSpan;->clearAlternates()Lcom/google/speech/common/Alternates$AlternateSpan;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$Alternate;

    invoke-virtual {v5, v0}, Lcom/google/speech/common/Alternates$AlternateSpan;->addAlternates(Lcom/google/speech/common/Alternates$Alternate;)Lcom/google/speech/common/Alternates$AlternateSpan;

    goto :goto_1

    :cond_2
    return-object v5
.end method

.method private isAtBeginning(Landroid/view/inputmethod/ExtractedText;)Z
    .locals 2
    .param p1    # Landroid/view/inputmethod/ExtractedText;

    iget v0, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v1, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPrevCharForceUppercase(Landroid/view/inputmethod/ExtractedText;)Z
    .locals 3
    .param p1    # Landroid/view/inputmethod/ExtractedText;

    iget v1, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v2, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v1, v2

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-lez v0, :cond_0

    iget-object v1, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->isSkipChar(C)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->upperCaseChars:Ljava/util/Set;

    iget-object v2, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isPrevTextForceUppercase(Landroid/view/inputmethod/ExtractedText;)Z
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    iget v1, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v3, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v1, v3

    const-class v3, Landroid/text/Annotation;

    invoke-interface {v0, v2, v1, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/Annotation;

    array-length v4, v1

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v5, v1, v3

    invoke-interface {v0, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    iget v7, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v8, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v7, v8

    if-ne v6, v7, :cond_1

    invoke-static {v5}, Lcom/google/android/voicesearch/util/TextUtil;->isForceUppercase(Landroid/text/Annotation;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private isSkipChar(C)Z
    .locals 1
    .param p1    # C

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public formatAlternateSpan(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;"
        }
    .end annotation

    iget-boolean v3, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCharUppercase:Z

    if-eqz v3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v1, p1

    :goto_0
    return-object v1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$AlternateSpan;

    invoke-virtual {v0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getStart()I

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->formatAlternateSpan(Lcom/google/speech/common/Alternates$AlternateSpan;)Lcom/google/speech/common/Alternates$AlternateSpan;

    move-result-object v0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    move-object p1, v1

    goto :goto_1

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method public formatPartialResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCharUppercase:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->forceFirstCharUppercase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public formatResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->formatPartialResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleCommit(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/ExtractedText;)V
    .locals 5
    .param p1    # Landroid/view/inputmethod/InputConnection;
    .param p2    # Landroid/view/inputmethod/ExtractedText;

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCommit:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    iget v1, p2, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    move v0, v1

    iget-object v2, p2, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    iget-object v2, p2, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-nez v2, :cond_3

    sget-boolean v2, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->DEBUG:Z

    if-eqz v2, :cond_2

    const-string v2, "LatinTextFormatter"

    const-string v3, "Add space before"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v2, " "

    invoke-interface {p1, v2, v4}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    :cond_3
    if-lez v1, :cond_5

    iget-object v2, p2, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-nez v2, :cond_5

    sget-boolean v2, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->DEBUG:Z

    if-eqz v2, :cond_4

    const-string v2, "LatinTextFormatter"

    const-string v3, "Add space after"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v2, " "

    const/4 v3, 0x1

    invoke-interface {p1, v2, v3}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    add-int/lit8 v0, v0, 0x1

    :cond_5
    invoke-interface {p1, v0, v0}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    iput-boolean v4, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCommit:Z

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCharUppercase:Z

    return-void
.end method

.method public startDictation(Landroid/view/inputmethod/ExtractedText;)V
    .locals 4
    .param p1    # Landroid/view/inputmethod/ExtractedText;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-boolean v1, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCommit:Z

    iput-boolean v2, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCharUppercase:Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->isAtBeginning(Landroid/view/inputmethod/ExtractedText;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->isPrevCharForceUppercase(Landroid/view/inputmethod/ExtractedText;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->isPrevTextForceUppercase(Landroid/view/inputmethod/ExtractedText;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    :goto_1
    iput-boolean v1, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCharUppercase:Z
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const v1, 0x60452c

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/BugLogger;->record(I)V

    iput-boolean v2, p0, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;->mFirstCharUppercase:Z

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method
