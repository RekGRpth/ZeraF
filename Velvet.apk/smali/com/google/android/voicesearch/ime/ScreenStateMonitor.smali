.class public Lcom/google/android/voicesearch/ime/ScreenStateMonitor;
.super Ljava/lang/Object;
.source "ScreenStateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;
    }
.end annotation


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private mListener:Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;

.field private mRegistered:Z

.field private final mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mRegistered:Z

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/ime/ScreenStateMonitor$1;-><init>(Lcom/google/android/voicesearch/ime/ScreenStateMonitor;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ime/ScreenStateMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/ScreenStateMonitor;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->handleBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private handleBroadcast(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mListener:Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ScreenStateMonitor"

    const-string v1, "#onReceive - screen off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mListener:Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;->onScreenOff()V

    :cond_0
    return-void
.end method


# virtual methods
.method public register(Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mListener:Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mRegistered:Z

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mListener:Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;

    iput-boolean v1, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mRegistered:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public unregister()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mRegistered:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mRegistered:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mListener:Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
