.class public Lcom/google/android/voicesearch/ime/ImePreferences;
.super Landroid/preference/PreferenceActivity;
.source "ImePreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ime/ImePreferences$Settings;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, ":android:no_headers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f0d03cf

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/ime/ImePreferences;->setTitle(I)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getVoiceImeSubtypeUpdater()Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->maybeScheduleUpdate(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    return-void
.end method
