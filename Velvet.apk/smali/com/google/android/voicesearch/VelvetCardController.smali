.class public Lcom/google/android/voicesearch/VelvetCardController;
.super Ljava/lang/Object;
.source "VelvetCardController.java"

# interfaces
.implements Lcom/google/android/voicesearch/CardController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;,
        Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAction:Lcom/google/android/velvet/presenter/Action;

.field private mActionProcessor:Lcom/google/android/voicesearch/speechservice/ActionProcessor;

.field private final mContext:Landroid/content/Context;

.field private mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;

.field private mDefaultCountdownDurationMs:J

.field private final mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

.field private mQuery:Lcom/google/android/velvet/Query;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field private mUnusedActionLoggingProcessor:Lcom/google/android/voicesearch/speechservice/ActionProcessor;

.field private final mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

.field private final mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/VelvetCardController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/voicesearch/VelvetCardController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/DeviceCapabilityManager;Landroid/content/Context;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/DeviceCapabilityManager;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p4    # Lcom/google/android/velvet/VelvetFactory;
    .param p5    # Lcom/google/android/velvet/presenter/QueryState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mDefaultCountdownDurationMs:J

    iput-object p1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/VelvetCardController;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/voicesearch/VelvetCardController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    iput-object p4, p0, Lcom/google/android/voicesearch/VelvetCardController;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p5, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/searchcommon/DeviceCapabilityManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/Action;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/VelvetCardController;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private maybeInitAnswerProcessors()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/VelvetFactory;->createControllerFactory(Lcom/google/android/voicesearch/CardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;

    new-instance v0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;

    new-instance v1, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;-><init>(Lcom/google/android/voicesearch/VelvetCardController;)V

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;-><init>(Lcom/google/android/voicesearch/speechservice/ActionListener;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mActionProcessor:Lcom/google/android/voicesearch/speechservice/ActionProcessor;

    :cond_0
    return-void
.end method


# virtual methods
.method public canPlayLocalTts()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isIntentQuery()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAction()Lcom/google/android/velvet/presenter/Action;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    return-object v0
.end method

.method public getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VelvetCardController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/util/IntentStarter;->getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultCountdownDurationMs()J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mDefaultCountdownDurationMs:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getDefaultActionCountDownMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mDefaultCountdownDurationMs:J

    :cond_0
    iget-wide v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mDefaultCountdownDurationMs:J

    return-wide v0
.end method

.method public getLastAudioUri(Lcom/google/android/speech/audio/AudioUtils$Encoding;)Landroid/net/Uri;
    .locals 2
    .param p1    # Lcom/google/android/speech/audio/AudioUtils$Encoding;

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioStore()Lcom/google/android/speech/audio/AudioStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore;->getLastAudio()[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, v0}, Lcom/google/android/speech/audio/AudioProvider;->insert(Landroid/content/Context;Lcom/google/android/speech/audio/AudioUtils$Encoding;[B)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public getQuery()Lcom/google/android/velvet/Query;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method public getQueryState()Lcom/google/android/velvet/presenter/QueryState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-object v0
.end method

.method public handleAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/Action;

    iput-object p1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p2, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-direct {p0}, Lcom/google/android/voicesearch/VelvetCardController;->maybeInitAnswerProcessors()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mActionProcessor:Lcom/google/android/voicesearch/speechservice/ActionProcessor;

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/helper/AccountHelper;->hasGoogleAccount()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->process(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;Z)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->onHandlingDone()V

    return-void
.end method

.method public isAttached()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public logUnusedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/Action;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mUnusedActionLoggingProcessor:Lcom/google/android/voicesearch/speechservice/ActionProcessor;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;

    new-instance v1, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;-><init>(Lcom/google/android/voicesearch/VelvetCardController;)V

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;-><init>(Lcom/google/android/voicesearch/speechservice/ActionListener;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mUnusedActionLoggingProcessor:Lcom/google/android/voicesearch/speechservice/ActionProcessor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mUnusedActionLoggingProcessor:Lcom/google/android/voicesearch/speechservice/ActionProcessor;

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/helper/AccountHelper;->hasGoogleAccount()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->process(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;Z)Z

    return-void
.end method

.method public restoreActionControllers(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/Action;

    invoke-direct {p0}, Lcom/google/android/voicesearch/VelvetCardController;->maybeInitAnswerProcessors()V

    iput-object p1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p2, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->restoreControllers(Lcom/google/android/voicesearch/fragments/ControllerFactory;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->onHandlingDone()V

    return-void
.end method

.method public setPresenter(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iput-object p1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)Z
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VelvetCardController;->isAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/voicesearch/VelvetCardController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No activity found for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VelvetCardController;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public takeStartCountdown()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isIntentQuery()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isTextSearch()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getAndSetCountdownStarted()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
