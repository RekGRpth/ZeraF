.class public Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
.super Ljava/lang/Object;
.source "AudioTrackSoundManager.java"

# interfaces
.implements Lcom/google/android/speech/audio/SpeechSoundManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;
    }
.end annotation


# static fields
.field private static final INVALID_SOUND:[B


# instance fields
.field private final mAudioData:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[B>;"
        }
    .end annotation
.end field

.field private mAudioStream:I

.field private final mContext:Landroid/content/Context;

.field private final mPlaybackExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->INVALID_SOUND:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mAudioData:Landroid/util/SparseArray;

    iput-object p1, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mAudioStream:I

    iput-object p3, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mPlaybackExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;I)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSound(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(JJJ)J
    .locals 2
    .param p0    # J
    .param p2    # J
    .param p4    # J

    invoke-static/range {p0 .. p5}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->clip(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private static clip(JJJ)J
    .locals 1
    .param p0    # J
    .param p2    # J
    .param p4    # J

    cmp-long v0, p0, p4

    if-lez v0, :cond_0

    :goto_0
    return-wide p4

    :cond_0
    cmp-long v0, p0, p2

    if-gez v0, :cond_1

    move-wide p4, p2

    goto :goto_0

    :cond_1
    move-wide p4, p0

    goto :goto_0
.end method

.method private static closeSilently(Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;

    :try_start_0
    instance-of v0, p0, Landroid/content/res/AssetFileDescriptor;

    if-eqz v0, :cond_1

    check-cast p0, Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {p0}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/io/Closeable;

    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private loadSound(I)[B
    .locals 9
    .param p1    # I

    iget-object v7, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, p1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v5

    const-wide/32 v7, 0x7fffffff

    cmp-long v7, v5, v7

    if-lez v7, :cond_0

    invoke-static {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->closeSilently(Ljava/lang/Object;)V

    sget-object v1, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->INVALID_SOUND:[B

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v7

    long-to-int v4, v7

    new-array v1, v4, [B

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v3

    const/4 v7, 0x0

    invoke-static {v3, v1, v7, v4}, Lcom/google/common/io/ByteStreams;->read(Ljava/io/InputStream;[BII)I

    move-result v7

    if-eq v7, v4, :cond_1

    new-instance v7, Ljava/io/IOException;

    invoke-direct {v7}, Ljava/io/IOException;-><init>()V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v2

    :try_start_1
    sget-object v1, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->INVALID_SOUND:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v3}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->closeSilently(Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->closeSilently(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-static {v3}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->closeSilently(Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->closeSilently(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v7

    invoke-static {v3}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->closeSilently(Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->closeSilently(Ljava/lang/Object;)V

    throw v7
.end method

.method private playSound(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mAudioData:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->loadSound(I)[B

    move-result-object v0

    :cond_0
    sget-object v1, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->INVALID_SOUND:[B

    if-ne v0, v1, :cond_1

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSound([B)I

    move-result v1

    goto :goto_0
.end method

.method private playSoundAsync(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mPlaybackExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$1;-><init>(Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private writeToAudioTrack([BLandroid/media/AudioTrack;I)V
    .locals 3
    .param p1    # [B
    .param p2    # Landroid/media/AudioTrack;
    .param p3    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v1, p3, :cond_0

    sub-int v2, p3, v1

    invoke-virtual {p2, p1, v1, v2}, Landroid/media/AudioTrack;->write([BII)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/2addr v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected createAudioTrack(I)Landroid/media/AudioTrack;
    .locals 7
    .param p1    # I

    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mAudioStream:I

    const/16 v2, 0x3e80

    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v6, 0x1

    move v5, p1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    return-object v0
.end method

.method public playDictationDoneSound()V
    .locals 1

    const v0, 0x7f080009

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSoundAsync(I)V

    return-void
.end method

.method public playErrorSound()V
    .locals 1

    const v0, 0x7f080007

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSoundAsync(I)V

    return-void
.end method

.method public playHandsFreeShutDownSound()V
    .locals 1

    const v0, 0x7f080009

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSoundAsync(I)V

    return-void
.end method

.method public playNoInputSound()V
    .locals 1

    const v0, 0x7f080009

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSoundAsync(I)V

    return-void
.end method

.method public playRecognitionDoneSound()V
    .locals 1

    const v0, 0x7f08000b

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSoundAsync(I)V

    return-void
.end method

.method playSound([B)I
    .locals 5
    .param p1    # [B

    array-length v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->createAudioTrack(I)Landroid/media/AudioTrack;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getState()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1, v2, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->writeToAudioTrack([BLandroid/media/AudioTrack;I)V

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v1

    invoke-virtual {v2}, Landroid/media/AudioTrack;->play()V

    iget-object v3, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mPlaybackExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;

    invoke-direct {v4, v2, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;-><init>(Landroid/media/AudioTrack;I)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public playSpeakNowSound()I
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    const v0, 0x7f08000a

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playSound(I)I

    move-result v0

    return v0
.end method

.method public setAudioStream(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->mAudioStream:I

    return-void
.end method
