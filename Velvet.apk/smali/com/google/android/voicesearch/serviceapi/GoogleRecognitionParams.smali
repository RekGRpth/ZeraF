.class public Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;
.super Ljava/lang/Object;
.source "GoogleRecognitionParams.java"


# instance fields
.field private final mPartialResultsRequested:Z

.field private final mSpokenBcp47Locale:Ljava/lang/String;

.field private final mTriggerApplication:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Intent;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->initTriggerApplication(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->mTriggerApplication:Ljava/lang/String;

    invoke-direct {p0, p2, p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->initSpokenBcp47Locale(Lcom/google/android/voicesearch/settings/Settings;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->mSpokenBcp47Locale:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->initPartialResultsRequested(Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->mPartialResultsRequested:Z

    return-void
.end method

.method private initPartialResultsRequested(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "android.speech.extra.PARTIAL_RESULTS"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private initSpokenBcp47Locale(Lcom/google/android/voicesearch/settings/Settings;Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/settings/Settings;
    .param p2    # Landroid/content/Intent;

    const-string v2, "android.speech.extra.LANGUAGE"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getLanguageDialect(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getSpokenLanguageByJavaLocale(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "GoogleRecognitionParams"

    const-string v3, "The locale should be specified in BCP47"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getBcp47Locale()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private initTriggerApplication(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v1, "calling_package"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getSpokenBcp47Locale()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->mSpokenBcp47Locale:Ljava/lang/String;

    return-object v0
.end method

.method public getTriggerApplication()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->mTriggerApplication:Ljava/lang/String;

    return-object v0
.end method

.method public isPartialResultsRequested()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->mPartialResultsRequested:Z

    return v0
.end method
