.class public Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;
.super Ljava/lang/Object;
.source "GoogleRecognitionServiceImpl.java"


# instance fields
.field private final mLevelsGenerator:Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

.field private final mMainThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mRecognizer:Lcom/google/android/speech/Recognizer;

.field private final mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

.field private final mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

.field private onDoneListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/Recognizer;Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/Recognizer;
    .param p2    # Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;
    .param p3    # Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;
    .param p4    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl$1;-><init>(Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->onDoneListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;

    iput-object p1, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mRecognizer:Lcom/google/android/speech/Recognizer;

    iput-object p3, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mLevelsGenerator:Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iput-object p2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    iput-object p4, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    invoke-direct {p0}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->internalCancel()V

    return-void
.end method

.method private internalCancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mRecognizer:Lcom/google/android/speech/Recognizer;

    invoke-interface {v0}, Lcom/google/android/speech/Recognizer;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mLevelsGenerator:Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->stop()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/16 v0, 0x39

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->internalCancel()V

    return-void
.end method

.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mRecognizer:Lcom/google/android/speech/Recognizer;

    invoke-interface {v0}, Lcom/google/android/speech/Recognizer;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mLevelsGenerator:Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->stop()V

    return-void
.end method

.method public startListening(Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;Landroid/speech/RecognitionService$Callback;)V
    .locals 5
    .param p1    # Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;
    .param p2    # Landroid/speech/RecognitionService$Callback;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const-string v2, "GoogleRecognitionServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "#startListening ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->getSpokenBcp47Locale()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/speech/params/RecognizerParams$Mode;->SERVICE_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v2, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->getSpokenBcp47Locale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-virtual {v2, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setGreco3Mode(Lcom/google/android/speech/embedded/Greco3Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->getTriggerApplication()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setTriggerApplicationId(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v1

    const/16 v2, 0x37

    invoke-virtual {p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->getTriggerApplication()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    new-instance v0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->onDoneListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;->isPartialResultsRequested()Z

    move-result v3

    invoke-direct {v0, p2, v2, v3}, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;-><init>(Landroid/speech/RecognitionService$Callback;Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;Z)V

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mRecognizer:Lcom/google/android/speech/Recognizer;

    iget-object v3, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v2, v1, v0, v3}, Lcom/google/android/speech/Recognizer;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mLevelsGenerator:Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->stop()V

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mLevelsGenerator:Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-virtual {v2, v0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->start(Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const/16 v0, 0x3a

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mRecognizer:Lcom/google/android/speech/Recognizer;

    invoke-interface {v0}, Lcom/google/android/speech/Recognizer;->stopListening()V

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->mLevelsGenerator:Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->stop()V

    return-void
.end method
