.class public Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;
.super Landroid/speech/RecognitionService;
.source "GoogleRecognitionService.java"


# instance fields
.field private mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

.field private mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/speech/RecognitionService;-><init>()V

    return-void
.end method

.method private initGooogleRecognitionImpl()V
    .locals 7

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v1

    new-instance v0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;-><init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/speech/SpeechLevelSource;)V

    new-instance v3, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizerParamsFactory()Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getMainThreadExecutor()Ljava/util/concurrent/Executor;

    move-result-object v6

    invoke-direct {v3, v4, v5, v0, v6}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;-><init>(Lcom/google/android/speech/Recognizer;Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;Ljava/util/concurrent/Executor;)V

    iput-object v3, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    return-void
.end method


# virtual methods
.method protected onCancel(Landroid/speech/RecognitionService$Callback;)V
    .locals 2
    .param p1    # Landroid/speech/RecognitionService$Callback;

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    if-nez v0, :cond_0

    const-string v0, "GoogleRecognitionService"

    const-string v1, "Cancel is called before startListening"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->cancel()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/speech/RecognitionService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->destroy()V

    :cond_0
    invoke-super {p0}, Landroid/speech/RecognitionService;->onDestroy()V

    return-void
.end method

.method protected onStartListening(Landroid/content/Intent;Landroid/speech/RecognitionService$Callback;)V
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/speech/RecognitionService$Callback;

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->initGooogleRecognitionImpl()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    new-instance v1, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {v1, p1, v2}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;-><init>(Landroid/content/Intent;Lcom/google/android/voicesearch/settings/Settings;)V

    invoke-virtual {v0, v1, p2}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->startListening(Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionParams;Landroid/speech/RecognitionService$Callback;)V

    return-void
.end method

.method protected onStopListening(Landroid/speech/RecognitionService$Callback;)V
    .locals 2
    .param p1    # Landroid/speech/RecognitionService$Callback;

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    if-nez v0, :cond_0

    const-string v0, "GoogleRecognitionService"

    const-string v1, "StopListening is called before startListening"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mGoogleRecognitionServiceImpl:Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionServiceImpl;->stopListening()V

    goto :goto_0
.end method
