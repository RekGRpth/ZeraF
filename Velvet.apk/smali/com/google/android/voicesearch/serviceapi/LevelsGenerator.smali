.class public Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;
.super Ljava/lang/Object;
.source "LevelsGenerator.java"


# instance fields
.field private final mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

.field private final mRunnable:Ljava/lang/Runnable;

.field private final mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private final mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p2    # Lcom/google/android/speech/SpeechLevelSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    iput-object p1, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator$1;-><init>(Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mRunnable:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;

    invoke-direct {p0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->notifyRms()V

    return-void
.end method

.method private notifyRms()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v1}, Lcom/google/android/speech/SpeechLevelSource;->getSpeechLevel()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

    invoke-static {v0}, Lcom/google/android/speech/audio/SpeechLevelGenerator;->convertVolumeToRmsDb(I)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->sendRmsValue(F)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->scheduleNotifyRms()V

    goto :goto_0
.end method

.method private scheduleNotifyRms()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method


# virtual methods
.method public start(Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

    invoke-direct {p0}, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->scheduleNotifyRms()V

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/LevelsGenerator;->mListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;

    return-void
.end method
