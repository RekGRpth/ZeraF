.class public Lcom/google/android/voicesearch/SendSmsActivity;
.super Landroid/app/Activity;
.source "SendSmsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;
    }
.end annotation


# instance fields
.field private mStatusReceiver:Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;

.field private mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/SendSmsActivity;Ljava/lang/Exception;I)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/SendSmsActivity;
    .param p1    # Ljava/lang/Exception;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/SendSmsActivity;->fireFailure(Ljava/lang/Exception;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/SendSmsActivity;)Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/SendSmsActivity;

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/SendSmsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/SendSmsActivity;

    invoke-direct {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->fireSuccess()V

    return-void
.end method

.method private fireFailure(Ljava/lang/Exception;I)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .param p2    # I

    if-nez p1, :cond_1

    const-string v0, "SendSmsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failure sending sms, status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;->stop()V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->showDialog(I)V

    return-void

    :cond_1
    const-string v0, "SendSmsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failure sending sms, status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private fireSuccess()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;->stop()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->finish()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 22
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/SendSmsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    const-string v4, "smsto"

    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "SendSmsActivity"

    const-string v7, "unexpected data scheme, requires \'smsto\'"

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/SendSmsActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v2

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v7, "smsto"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    :try_start_0
    const-string v4, "UTF-8"

    move-object/from16 v0, v19

    invoke-static {v0, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    const-string v4, ","

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    const-string v4, "android.intent.extra.TEXT"

    invoke-virtual {v14, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    if-nez v20, :cond_2

    const-string v20, ""

    :cond_2
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v16

    new-instance v4, Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    const v7, 0xea60

    new-instance v21, Lcom/google/android/voicesearch/SendSmsActivity$1;

    invoke-direct/range {v21 .. v22}, Lcom/google/android/voicesearch/SendSmsActivity$1;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;)V

    move-object/from16 v0, v21

    invoke-direct {v4, v7, v0}, Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;-><init>(ILjava/lang/Runnable;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    invoke-virtual {v4}, Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;->start()V

    new-instance v11, Landroid/content/IntentFilter;

    const-string v4, "com.google.android.voicesearch.action.SMS_STATUS"

    invoke-direct {v11, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;

    move-object/from16 v0, v18

    array-length v7, v0

    mul-int v7, v7, v16

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v7}, Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/voicesearch/SendSmsActivity;->mStatusReceiver:Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/voicesearch/SendSmsActivity;->mStatusReceiver:Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, Lcom/google/android/voicesearch/SendSmsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const v4, 0x7f0d03cc

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    move-object/from16 v8, v18

    array-length v15, v8

    const/4 v13, 0x0

    :goto_1
    if-ge v13, v15, :cond_0

    aget-object v3, v8, v13

    new-instance v17, Landroid/content/Intent;

    const-string v4, "com.google.android.voicesearch.action.SMS_STATUS"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/SendSmsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.google.android.voicesearch.extras.SMS_RECIPIENTS"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.google.android.voicesearch.extras.SMS_MESSAGE"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v6, Ljava/util/ArrayList;

    move/from16 v0, v16

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v12, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v12, v0, :cond_3

    const/4 v4, 0x0

    const/high16 v7, 0x40000000

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v4, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :catch_0
    move-exception v10

    const-string v4, "SendSmsActivity"

    const-string v7, "error decoding recipients"

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/google/android/voicesearch/SendSmsActivity;->fireFailure(Ljava/lang/Exception;I)V

    goto/16 :goto_0

    :cond_3
    const/4 v4, 0x0

    const/4 v7, 0x0

    :try_start_1
    invoke-virtual/range {v2 .. v7}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :catch_1
    move-exception v10

    const/4 v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/google/android/voicesearch/SendSmsActivity;->fireFailure(Ljava/lang/Exception;I)V

    goto :goto_3
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0d03cd

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/SendSmsActivity$3;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/SendSmsActivity$3;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/google/android/voicesearch/SendSmsActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/SendSmsActivity$2;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mStatusReceiver:Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mStatusReceiver:Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mStatusReceiver:Lcom/google/android/voicesearch/SendSmsActivity$SmsStatusReceiver;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/watchdog/TimeoutWatchdog;->stop()V

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method
