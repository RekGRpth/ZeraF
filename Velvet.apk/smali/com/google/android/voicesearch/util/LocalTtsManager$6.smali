.class Lcom/google/android/voicesearch/util/LocalTtsManager$6;
.super Ljava/lang/Object;
.source "LocalTtsManager.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/LocalTtsManager;->maybeCreateTtsAndWaitForInit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field final synthetic val$condVar:Landroid/os/ConditionVariable;

.field final synthetic val$ttsInited:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$6;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$6;->val$ttsInited:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$6;->val$condVar:Landroid/os/ConditionVariable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$6;->val$ttsInited:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$6;->val$condVar:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$6;->val$ttsInited:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method
