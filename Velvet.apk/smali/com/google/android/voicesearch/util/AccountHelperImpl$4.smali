.class Lcom/google/android/voicesearch/util/AccountHelperImpl$4;
.super Ljava/lang/Object;
.source "AccountHelperImpl.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/AccountHelperImpl;->getGmailAccounts(Lcom/google/android/speech/callback/SimpleCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<[",
        "Landroid/accounts/Account;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

.field final synthetic val$callback:Lcom/google/android/speech/callback/SimpleCallback;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$4;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$4;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<[",
            "Landroid/accounts/Account;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$4;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    # invokes: Lcom/google/android/voicesearch/util/AccountHelperImpl;->maybeReorderAccounts([Landroid/accounts/Account;)V
    invoke-static {v2, v1}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->access$300(Lcom/google/android/voicesearch/util/AccountHelperImpl;[Landroid/accounts/Account;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$4;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-interface {v2, v1}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "AccountHelperImpl"

    const-string v3, "Retreiving Google accounts failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$4;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "AccountHelperImpl"

    const-string v3, "Retreiving Google accounts failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v2, "AccountHelperImpl"

    const-string v3, "Retreiving Google accounts failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
