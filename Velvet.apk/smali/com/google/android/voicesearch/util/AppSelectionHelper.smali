.class public Lcom/google/android/voicesearch/util/AppSelectionHelper;
.super Ljava/lang/Object;
.source "AppSelectionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    }
.end annotation


# static fields
.field private static final LAUNCHER_INTENT:Landroid/content/Intent;


# instance fields
.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPreferredApplicationsManager:Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->LAUNCHER_INTENT:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/util/PreferredApplicationsManager;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/util/PreferredApplicationsManager;
    .param p2    # Landroid/content/pm/PackageManager;
    .param p3    # Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPreferredApplicationsManager:Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mResources:Landroid/content/res/Resources;

    return-void
.end method

.method private findActivitiesInternal(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v8, 0x0

    invoke-virtual {v7, p2, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v0, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v7}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz p1, :cond_1

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v7, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0, v1, v7, v5}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->createApp(Landroid/content/Intent;Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v2
.end method

.method public static fromContext(Landroid/content/Context;)Lcom/google/android/voicesearch/util/AppSelectionHelper;
    .locals 4
    .param p0    # Landroid/content/Context;

    new-instance v1, Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/voicesearch/util/PreferredApplicationsManager;-><init>(Landroid/content/SharedPreferences;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {v3, v1, v0, v2}, Lcom/google/android/voicesearch/util/AppSelectionHelper;-><init>(Lcom/google/android/voicesearch/util/PreferredApplicationsManager;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    return-object v3
.end method


# virtual methods
.method public appSelected(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPreferredApplicationsManager:Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/util/PreferredApplicationsManager;->setPreferredApplication(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected createApp(Landroid/content/Intent;Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/content/pm/PackageManager;
    .param p3    # Landroid/content/pm/ResolveInfo;

    new-instance v0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    iget-object v1, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v1, p2}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v2, p2}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;-><init>(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public findActivities(Landroid/content/Intent;)Ljava/util/List;
    .locals 1
    .param p1    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivitiesInternal(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public findActivities(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-direct {p0, v2, v1}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivitiesInternal(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/voicesearch/util/AppSelectionHelper;->LAUNCHER_INTENT:Landroid/content/Intent;

    invoke-direct {p0, p1, v2}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivitiesInternal(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method public getAllAppNamesLowerCase()[Ljava/lang/String;
    .locals 8

    iget-object v5, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    sget-object v6, Lcom/google/android/voicesearch/util/AppSelectionHelper;->LAUNCHER_INTENT:Landroid/content/Intent;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v5, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v4, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    return-object v5
.end method

.method public getPlayStoreLink(Landroid/net/Uri;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .locals 5
    .param p1    # Landroid/net/Uri;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.android.vending"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    iget-object v2, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0d03c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f02006e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;-><init>(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-object v1
.end method

.method public getSelectedApp(Ljava/lang/String;Ljava/util/Collection;)I
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;)I"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPreferredApplicationsManager:Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    invoke-virtual {v5, p1}, Lcom/google/android/voicesearch/util/PreferredApplicationsManager;->getPreferredApplication(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isSupported(Landroid/content/Intent;)Z
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method
