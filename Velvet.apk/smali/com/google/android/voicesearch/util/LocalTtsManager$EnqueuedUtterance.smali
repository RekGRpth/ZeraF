.class Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;
.super Ljava/lang/Object;
.source "LocalTtsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/util/LocalTtsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EnqueuedUtterance"
.end annotation


# instance fields
.field final audioStream:I

.field final completionCallback:Ljava/lang/Runnable;

.field final networkTtsFlag:I

.field final utterance:Ljava/lang/String;

.field final utteranceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Runnable;
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->utterance:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->utteranceId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->audioStream:I

    iput-object p4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->completionCallback:Ljava/lang/Runnable;

    iput p5, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->networkTtsFlag:I

    return-void
.end method
