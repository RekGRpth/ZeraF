.class Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder$1;
.super Ljava/lang/Object;
.source "CalendarHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;

.field final synthetic val$calendarId:J


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder$1;->this$1:Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;

    iput-wide p2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder$1;->val$calendarId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder$1;->this$1:Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;

    iget-object v1, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    iget-wide v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder$1;->val$calendarId:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
