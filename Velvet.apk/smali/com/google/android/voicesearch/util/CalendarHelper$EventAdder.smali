.class Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;
.super Ljava/lang/Object;
.source "CalendarHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/util/CalendarHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventAdder"
.end annotation


# instance fields
.field final mAttendees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;",
            ">;"
        }
    .end annotation
.end field

.field final mCalendar:Ljava/lang/String;

.field final mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final mEndTimeMs:J

.field final mLocation:Ljava/lang/String;

.field final mOwner:Ljava/lang/String;

.field final mReminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;",
            ">;"
        }
    .end annotation
.end field

.field final mStartTimeMs:J

.field final mSummary:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/voicesearch/util/CalendarHelper;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/CalendarHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;",
            ">;",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mOwner:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCalendar:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mSummary:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mLocation:Ljava/lang/String;

    iput-wide p6, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mStartTimeMs:J

    iput-wide p8, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mEndTimeMs:J

    iput-object p10, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mAttendees:Ljava/util/List;

    iput-object p11, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mReminders:Ljava/util/List;

    iput-object p12, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;

    if-eqz v2, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mOwner:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mOwner:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCalendar:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCalendar:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mSummary:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mSummary:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mLocation:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mLocation:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mStartTimeMs:J

    iget-wide v4, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mStartTimeMs:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mEndTimeMs:J

    iget-wide v4, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mEndTimeMs:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mAttendees:Ljava/util/List;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mAttendees:Ljava/util/List;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mAttendees:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mAttendees:Ljava/util/List;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mAttendees:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mReminders:Ljava/util/List;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mReminders:Ljava/util/List;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mReminders:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mReminders:Ljava/util/List;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mReminders:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    iget-object v3, v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    if-ne v2, v3, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public run()V
    .locals 11

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mOwner:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCalendar:Ljava/lang/String;

    # invokes: Lcom/google/android/voicesearch/util/CalendarHelper;->getCalendarId(Ljava/lang/String;Ljava/lang/String;)J
    invoke-static {v0, v3, v4}, Lcom/google/android/voicesearch/util/CalendarHelper;->access$000(Lcom/google/android/voicesearch/util/CalendarHelper;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mSummary:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mLocation:Ljava/lang/String;

    iget-wide v5, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mStartTimeMs:J

    iget-wide v7, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mEndTimeMs:J

    iget-object v9, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mAttendees:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mReminders:Ljava/util/List;

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/voicesearch/util/CalendarHelper;->insertEvent(JLjava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    # getter for: Lcom/google/android/voicesearch/util/CalendarHelper;->mUiThreadExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/CalendarHelper;->access$100(Lcom/google/android/voicesearch/util/CalendarHelper;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v3, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder$1;

    invoke-direct {v3, p0, v1, v2}, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder$1;-><init>(Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;J)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method
