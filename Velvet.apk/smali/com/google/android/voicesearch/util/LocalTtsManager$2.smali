.class Lcom/google/android/voicesearch/util/LocalTtsManager$2;
.super Ljava/lang/Object;
.source "LocalTtsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field final synthetic val$audioStream:I

.field final synthetic val$completionCallback:Ljava/lang/Runnable;

.field final synthetic val$networkTtsFlag:I

.field final synthetic val$utterance:Ljava/lang/String;

.field final synthetic val$utteranceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$utterance:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$utteranceId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$audioStream:I

    iput-object p5, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$completionCallback:Ljava/lang/Runnable;

    iput p6, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$networkTtsFlag:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$utterance:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$utteranceId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$audioStream:I

    iget-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$completionCallback:Ljava/lang/Runnable;

    iget v5, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;->val$networkTtsFlag:I

    # invokes: Lcom/google/android/voicesearch/util/LocalTtsManager;->internalEnqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$300(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V

    return-void
.end method
