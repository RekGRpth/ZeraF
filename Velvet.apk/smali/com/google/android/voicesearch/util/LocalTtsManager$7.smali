.class Lcom/google/android/voicesearch/util/LocalTtsManager$7;
.super Ljava/lang/Object;
.source "LocalTtsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/LocalTtsManager;->checkLocaleMatches(Ljava/util/Locale;ZLcom/google/android/speech/callback/SimpleCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field final synthetic val$callback:Lcom/google/android/speech/callback/SimpleCallback;

.field final synthetic val$ignoreCountry:Z

.field final synthetic val$locale:Ljava/util/Locale;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/util/Locale;ZLcom/google/android/speech/callback/SimpleCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->val$locale:Ljava/util/Locale;

    iput-boolean p3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->val$ignoreCountry:Z

    iput-object p4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # invokes: Lcom/google/android/voicesearch/util/LocalTtsManager;->maybeCreateTtsAndWaitForInit()V
    invoke-static {v2}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$600(Lcom/google/android/voicesearch/util/LocalTtsManager;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;
    invoke-static {v2}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$400(Lcom/google/android/voicesearch/util/LocalTtsManager;)Landroid/speech/tts/TextToSpeech;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;
    invoke-static {v2}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$400(Lcom/google/android/voicesearch/util/LocalTtsManager;)Landroid/speech/tts/TextToSpeech;

    move-result-object v2

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->val$locale:Ljava/util/Locale;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->val$locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->val$ignoreCountry:Z

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->val$locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$7;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mUiThread:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$700(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/voicesearch/util/LocalTtsManager$7$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/voicesearch/util/LocalTtsManager$7$1;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager$7;Z)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
