.class public Lcom/google/android/voicesearch/util/PhoneActionUtils;
.super Ljava/lang/Object;
.source "PhoneActionUtils.java"


# static fields
.field public static final MESSAGE_DEFAULT_CONTACT_TYPE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/voicesearch/util/PhoneActionUtils;->MESSAGE_DEFAULT_CONTACT_TYPE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static containsSameContactName(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)Z"
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v3}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_2
    return v5

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    move v5, v4

    goto :goto_2
.end method

.method public static fromPumpkinTaggerResult(Lcom/google/android/speech/embedded/TaggerResult;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;
    .locals 7
    .param p0    # Lcom/google/android/speech/embedded/TaggerResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/speech/exception/ResponseRecognizeException;
        }
    .end annotation

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setPhoneActionExtension(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    const-string v5, "CallContact"

    invoke-virtual {p0}, Lcom/google/android/speech/embedded/TaggerResult;->getActionName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "Contact"

    invoke-virtual {p0, v5}, Lcom/google/android/speech/embedded/TaggerResult;->getArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    const-string v5, "PhoneType"

    invoke-virtual {p0, v5}, Lcom/google/android/speech/embedded/TaggerResult;->getArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;-><init>()V

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->setType(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->addPhone(Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const-string v5, "CallNumber"

    invoke-virtual {p0}, Lcom/google/android/speech/embedded/TaggerResult;->getActionName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;-><init>()V

    const-string v5, "Number"

    invoke-virtual {p0, v5}, Lcom/google/android/speech/embedded/TaggerResult;->getArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->setNumber(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->addPhone(Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    goto :goto_0

    :cond_2
    new-instance v5, Lcom/google/android/speech/exception/ResponseRecognizeException;

    const-string v6, "Can only create PhoneAction from CallContact or CallNumber"

    invoke-direct {v5, v6}, Lcom/google/android/speech/exception/ResponseRecognizeException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public static getActionTypeLog(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)I
    .locals 1
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-static {p0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getActionTypeLog(Z)I

    move-result v0

    return v0
.end method

.method public static getActionTypeLog(Z)I
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    const/16 v0, 0x1c

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static getCallIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "tel"

    const/4 v3, 0x0

    invoke-static {v2, p0, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object v1
.end method

.method public static getContactName(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getParsedName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getContactType(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getPhoneCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getPhone(I)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->phoneTypeStringToAndroidTypeColumn(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getEditContactIntent(Lcom/google/android/speech/contacts/Contact;)Landroid/content/Intent;
    .locals 3
    .param p0    # Lcom/google/android/speech/contacts/Contact;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {p0}, Lcom/google/android/speech/contacts/Contact;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public static getScreenTypeLog(Z)I
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    const/16 v0, 0x20

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xe

    goto :goto_0
.end method

.method public static getSendSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.SENDTO"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p0, :cond_1

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    aput-object p0, v0, v4

    :goto_0
    const-string v3, ","

    invoke-static {v3, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "smsto"

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "sms_body"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v1

    :cond_1
    new-array v0, v4, [Ljava/lang/String;

    goto :goto_0
.end method

.method public static getShowContactIntent(Lcom/google/android/speech/contacts/Contact;)Landroid/content/Intent;
    .locals 3
    .param p0    # Lcom/google/android/speech/contacts/Contact;

    if-eqz p0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/google/android/speech/contacts/Contact;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "android.intent.action.MAIN"

    const-string v1, "android.intent.category.APP_CONTACTS"

    invoke-static {v0, v1}, Landroid/content/Intent;->makeMainSelectorActivity(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static getSpokenNumber(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContact(I)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getPhone(I)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->getNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSpokenNumber(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getContact()Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getPhone(I)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->getNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Z
    .locals 3
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasName()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasParsedName()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getPhoneCount()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getPhone(I)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->hasNumber()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Z
    .locals 2
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContact(I)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public static isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)Z
    .locals 1
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getContact()Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mergePhoneAction(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)V
    .locals 7
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$PhoneAction;
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasName()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasName()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasEmbeddedActionContactExtension()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getEmbeddedActionContactExtension()Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setEmbeddedActionContactExtension(Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    :cond_3
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasEmbeddedActionContactExtension()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getEmbeddedActionContactExtension()Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->setMerged(Z)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    return-void
.end method

.method public static final phoneTypeStringToAndroidTypeColumn(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "home"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "work"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "cell"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "mobile"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, "main"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v0, "other"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static spaceOutDigits(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_4

    aget-char v3, v0, v4

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v2, :cond_0

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/16 v6, 0x20

    if-ne v3, v6, :cond_3

    if-eqz v2, :cond_2

    const-string v6, ","

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
