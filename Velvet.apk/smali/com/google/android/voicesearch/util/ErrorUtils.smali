.class public Lcom/google/android/voicesearch/util/ErrorUtils;
.super Ljava/lang/Object;
.source "ErrorUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static canResendSameAudio(Lcom/google/android/speech/exception/RecognizeException;)Z
    .locals 3
    .param p0    # Lcom/google/android/speech/exception/RecognizeException;

    const/4 v1, 0x0

    const/4 v0, 0x1

    instance-of v2, p0, Lcom/google/android/speech/exception/NetworkRecognizeException;

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$NoMatchesFromEmbeddedRecognizerException;

    if-nez v2, :cond_0

    instance-of v2, p0, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p0, Lcom/google/android/speech/exception/AudioRecognizeException;

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    instance-of v2, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;

    if-nez v2, :cond_0

    instance-of v2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$GrammarCompilationException;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static getErrorImage(Lcom/google/android/speech/exception/RecognizeException;)I
    .locals 2
    .param p0    # Lcom/google/android/speech/exception/RecognizeException;

    const/4 v0, 0x0

    instance-of v1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;

    if-nez v1, :cond_0

    instance-of v1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$NoMatchesFromEmbeddedRecognizerException;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p0, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    if-eqz v1, :cond_0

    const v0, 0x7f02011b

    goto :goto_0
.end method

.method public static getErrorMessage(Lcom/google/android/speech/exception/RecognizeException;)I
    .locals 2
    .param p0    # Lcom/google/android/speech/exception/RecognizeException;

    const v0, 0x7f0d03a9

    instance-of v1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$NoMatchesFromEmbeddedRecognizerException;

    if-eqz v1, :cond_1

    const v0, 0x7f0d046f

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p0, Lcom/google/android/speech/exception/NetworkRecognizeException;

    if-nez v1, :cond_0

    instance-of v1, p0, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    if-eqz v1, :cond_2

    const v0, 0x7f0d03a8

    goto :goto_0

    :cond_2
    instance-of v1, p0, Lcom/google/android/speech/exception/AudioRecognizeException;

    if-eqz v1, :cond_3

    const v0, 0x7f0d03ab

    goto :goto_0

    :cond_3
    instance-of v1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;

    if-nez v1, :cond_0

    instance-of v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$GrammarCompilationException;

    if-eqz v0, :cond_4

    const v0, 0x7f0d03ac

    goto :goto_0

    :cond_4
    const v0, 0x7f0d03aa

    goto :goto_0
.end method

.method public static getRecognizerIntentError(Lcom/google/android/speech/exception/RecognizeException;)I
    .locals 1
    .param p0    # Lcom/google/android/speech/exception/RecognizeException;

    instance-of v0, p0, Lcom/google/android/speech/exception/AudioRecognizeException;

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Lcom/google/android/speech/exception/NetworkRecognizeException;

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static getSpeechRecognizerError(Lcom/google/android/speech/exception/RecognizeException;)I
    .locals 1
    .param p0    # Lcom/google/android/speech/exception/RecognizeException;

    instance-of v0, p0, Lcom/google/android/speech/exception/AudioRecognizeException;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Lcom/google/android/speech/exception/NetworkRecognizeException;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    if-eqz v0, :cond_2

    const/4 v0, 0x7

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_0
.end method
