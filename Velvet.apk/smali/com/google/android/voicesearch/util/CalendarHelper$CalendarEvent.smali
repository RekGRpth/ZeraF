.class public Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;
.super Ljava/lang/Object;
.source "CalendarHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/util/CalendarHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CalendarEvent"
.end annotation


# instance fields
.field private final mEndTimeMs:J

.field private final mId:J

.field private final mLocation:Ljava/lang/String;

.field private final mStartTimeMs:J

.field private final mSummary:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JJ)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .param p7    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mId:J

    iput-object p3, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mSummary:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mLocation:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mStartTimeMs:J

    iput-wide p7, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mEndTimeMs:J

    return-void
.end method


# virtual methods
.method public getEndTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mEndTimeMs:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mId:J

    return-wide v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mStartTimeMs:J

    return-wide v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->mSummary:Ljava/lang/String;

    return-object v0
.end method
