.class Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;
.super Ljava/lang/Object;
.source "QueryCalendarController.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/QueryCalendarController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/util/CalendarTextHelper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/lang/Boolean;)V
    .locals 7
    .param p1    # Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    # getter for: Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->access$400(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)Lcom/google/android/voicesearch/util/LocalTtsManager;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    # getter for: Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->access$300(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)Lcom/google/android/voicesearch/util/CalendarTextHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    # getter for: Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->access$000(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    # getter for: Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalStartTimeMs:J
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->access$100(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    # getter for: Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalEndTimeMs:J
    invoke-static {v4}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->access$200(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->createCalendarQueryTts(Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;JJ)Ljava/lang/String;

    move-result-object v1

    const-string v2, "queryCalendarAnswer"

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;->onResult(Ljava/lang/Boolean;)V

    return-void
.end method
