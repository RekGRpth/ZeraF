.class public interface abstract Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;
.super Ljava/lang/Object;
.source "DictionaryResultController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/DictionaryResultController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setExternalDictionaryLinks(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setMeaningsAndSynonyms(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setPronunciation(Ljava/lang/String;)V
.end method

.method public abstract setWord(Ljava/lang/String;)V
.end method
