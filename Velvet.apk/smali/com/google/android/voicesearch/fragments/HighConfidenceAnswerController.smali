.class public Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "HighConfidenceAnswerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x12

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x14

    return v0
.end method

.method public handleAttributionClick()V
    .locals 3

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->getActionTypeLog()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getSource()Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public handleImageAttributionClick()V
    .locals 3

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->getActionTypeLog()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getImageSource()Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public initUi()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getAnswer()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;->setAnswer(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getAnswerDescription()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;->setAnswerDescription(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getSource()Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;->setSource(Lcom/google/majel/proto/AttributionProtos$Attribution;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getImage()Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getImageSource()Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;->setImageData(Lcom/google/majel/proto/PeanutProtos$Image;Lcom/google/majel/proto/AttributionProtos$Attribution;)V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;

    check-cast p1, Lcom/google/android/voicesearch/speechservice/AnswerData;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/speechservice/AnswerData;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->showCardAndPlayTts()V

    return-void
.end method
