.class public Lcom/google/android/voicesearch/fragments/HelpController$Contact;
.super Ljava/lang/Object;
.source "HelpController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/HelpController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Contact"
.end annotation


# instance fields
.field private final mFirstName:Ljava/lang/String;

.field private final mId:J

.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/HelpController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/HelpController;JLjava/lang/String;)V
    .locals 0
    .param p2    # J
    .param p4    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpController$Contact;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/google/android/voicesearch/fragments/HelpController$Contact;->mId:J

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/HelpController$Contact;->mFirstName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFirstName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$Contact;->mFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$Contact;->mId:J

    return-wide v0
.end method
