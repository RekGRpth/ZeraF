.class public Lcom/google/android/voicesearch/fragments/EmailController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "EmailController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/EmailController$1;,
        Lcom/google/android/voicesearch/fragments/EmailController$Ui;,
        Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/EmailController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

.field private final mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

.field private final mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

.field private mToContact:Lcom/google/android/speech/contacts/Contact;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/EmailSender;Lcom/google/android/voicesearch/contacts/ContactSelectController;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Lcom/google/android/voicesearch/util/EmailSender;
    .param p5    # Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/voicesearch/fragments/EmailController;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/EmailController;
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    return-object p1
.end method

.method private createEmail()Lcom/google/android/voicesearch/util/EmailSender$Email;
    .locals 4

    new-instance v0, Lcom/google/android/voicesearch/util/EmailSender$Email;

    invoke-direct {v0}, Lcom/google/android/voicesearch/util/EmailSender$Email;-><init>()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v3}, Lcom/google/android/speech/contacts/Contact;->toRfc822Token()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, v0, Lcom/google/android/voicesearch/util/EmailSender$Email;->to:[Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/voicesearch/util/EmailSender$Email;->subject:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBody()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/voicesearch/util/EmailSender$Email;->body:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private getToContactName()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getToCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getTo(I)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;->getContactCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getTo(I)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;->getContact(I)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private internalStart(Z)V
    .locals 7
    .param p1    # Z

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getToContactName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->requireShowCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getTo(I)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;->getContactList()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->EMAIL:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->isNonEmpty()Z

    move-result v3

    new-instance v5, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;

    invoke-direct {v5, p0, v4}, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;-><init>(Lcom/google/android/voicesearch/fragments/EmailController;Lcom/google/android/voicesearch/fragments/EmailController$1;)V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->pickContact(Ljava/util/List;Lcom/google/android/voicesearch/contacts/ContactSelectMode;ZLjava/lang/String;Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->showCard()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->showCardAndPlayTts()V

    goto :goto_0
.end method

.method private isNonEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendEmail(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->createEmail()Lcom/google/android/voicesearch/util/EmailSender$Email;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p0}, Lcom/google/android/voicesearch/util/EmailSender;->sendEmail(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V

    return-void
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->isNonEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public initUi()V
    .locals 7

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v3, :cond_0

    move v0, v4

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    invoke-interface {v3, v6}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->setToContact(Lcom/google/android/speech/contacts/Contact;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->setSubject(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBody()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->setBody(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasBody()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getBody()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v4

    :goto_2
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->hasSubject()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    move v2, v4

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->canExecuteAction()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    invoke-interface {v3}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->showSendEmail()V

    :goto_4
    return-void

    :cond_0
    move v0, v5

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->getToCount()I

    move-result v3

    if-lez v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    invoke-interface {v3}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->showContactNotFound()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    invoke-interface {v3}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->hideContactField()V

    goto :goto_1

    :cond_3
    move v1, v5

    goto :goto_2

    :cond_4
    move v2, v5

    goto :goto_3

    :cond_5
    if-nez v0, :cond_6

    if-nez v1, :cond_6

    if-eqz v2, :cond_7

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    invoke-interface {v3}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->showNewEmail()V

    goto :goto_4

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/EmailController$Ui;

    invoke-interface {v3}, Lcom/google/android/voicesearch/fragments/EmailController$Ui;->showEmptyView()V

    goto :goto_4
.end method

.method protected internalBailOut()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/EmailController;->sendEmail(Z)V

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/EmailController;->sendEmail(Z)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/EmailController;->actionComplete()V

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/majel/proto/ActionV2Protos$EmailAction;->parseFrom([B)Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    return-void
.end method

.method public restoreStart()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/EmailController;->internalStart(Z)V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;

    check-cast p1, Lcom/google/android/speech/contacts/Contact;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    return-void
.end method

.method public start(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController;->mAction:Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/EmailController;->internalStart(Z)V

    return-void
.end method
