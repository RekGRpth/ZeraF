.class Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;
.super Ljava/lang/Object;
.source "PhoneCallController.java"

# interfaces
.implements Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/PhoneCallController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactSelectedCallbackImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/fragments/PhoneCallController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/fragments/PhoneCallController;Lcom/google/android/voicesearch/fragments/PhoneCallController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/PhoneCallController;
    .param p2    # Lcom/google/android/voicesearch/fragments/PhoneCallController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;-><init>(Lcom/google/android/voicesearch/fragments/PhoneCallController;)V

    return-void
.end method


# virtual methods
.method public onBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->bailOut()V

    return-void
.end method

.method public onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V
    .locals 2
    .param p1    # Lcom/google/android/speech/contacts/Contact;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    # setter for: Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->access$102(Lcom/google/android/voicesearch/fragments/PhoneCallController;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    # getter for: Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->access$100(Lcom/google/android/voicesearch/fragments/PhoneCallController;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->executeAction(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->showCard()V

    goto :goto_0
.end method

.method public onNoContactFound()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->showCard()V

    return-void
.end method

.method public onShowingDisambiguation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->showNoCard()V

    return-void
.end method
