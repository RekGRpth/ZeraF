.class final Lcom/google/android/voicesearch/fragments/UberRecognizerController$3;
.super Ljava/lang/Object;
.source "UberRecognizerController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setFinalRecognizedText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    return-void
.end method

.method public showInitializing()V
    .locals 0

    return-void
.end method

.method public showInitializingMic()V
    .locals 0

    return-void
.end method

.method public showListening()V
    .locals 0

    return-void
.end method

.method public showNotListening()V
    .locals 0

    return-void
.end method

.method public showRecognizing()V
    .locals 0

    return-void
.end method

.method public showRecording()V
    .locals 0

    return-void
.end method

.method public showSoundSearchPromotedQuery()V
    .locals 0

    return-void
.end method

.method public showTapToSpeak()V
    .locals 0

    return-void
.end method

.method public updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method
