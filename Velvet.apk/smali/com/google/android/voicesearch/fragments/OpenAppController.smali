.class public Lcom/google/android/voicesearch/fragments/OpenAppController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "OpenAppController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/OpenAppController$1;,
        Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;,
        Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

.field private mAppName:Ljava/lang/String;

.field private final mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/OpenAppController;)Lcom/google/android/voicesearch/util/AppSelectionHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/OpenAppController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/OpenAppController;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/OpenAppController;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppController;->onAppResults(Ljava/util/List;)V

    return-void
.end method

.method private internalStart()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->requireShowCard()V

    new-instance v0, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;-><init>(Lcom/google/android/voicesearch/fragments/OpenAppController;Lcom/google/android/voicesearch/fragments/OpenAppController$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    const-string v0, "OpenAppController"

    const-string v1, "No app name"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onAppResults(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "OpenAppController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "App not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->clearCard()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->showCardAndPlayTts()V

    goto :goto_0
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "app_name"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;->setName(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->uiReady()V

    return-void
.end method

.method protected internalBailOut()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->internalExecuteAction()V

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public restoreStart()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->internalStart()V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "app_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppName:Ljava/lang/String;

    return-void
.end method

.method public start(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/OpenAppController;->internalStart()V

    return-void
.end method
