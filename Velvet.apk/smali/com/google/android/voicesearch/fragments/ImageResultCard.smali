.class public Lcom/google/android/voicesearch/fragments/ImageResultCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "ImageResultCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/ImageResultController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;"
    }
.end annotation


# instance fields
.field private mColumnCount:I

.field private mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private mContext:Landroid/content/Context;

.field private mImageClickListener:Landroid/view/View$OnClickListener;

.field private mImageGrid:Landroid/widget/TableLayout;

.field private mImageGridLayout:Landroid/view/ViewGroup;

.field private mImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMarginPixelSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public clearImages()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGrid:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    return-void
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mInflater:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04005a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGridLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGridLayout:Landroid/view/ViewGroup;

    const v2, 0x7f100134

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableLayout;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGrid:Landroid/widget/TableLayout;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mMarginPixelSize:I

    const v1, 0x7f0b0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mColumnCount:I

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGridLayout:Landroid/view/ViewGroup;

    return-object v1
.end method

.method public setHeader(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGridLayout:Landroid/view/ViewGroup;

    const v2, 0x7f100133

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setImage(Lcom/google/android/voicesearch/speechservice/ImageParcelable;)V
    .locals 9
    .param p1    # Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    const/4 v8, 0x0

    new-instance v4, Landroid/widget/TableRow;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f04005b

    invoke-virtual {v6, v7, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v6, 0x7f100135

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->getImage()Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/velvet/ui/WebImageView;->setTag(Ljava/lang/Object;)V

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageClickListener:Landroid/view/View$OnClickListener;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v6}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    new-instance v5, Landroid/widget/TableLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow$LayoutParams;

    invoke-virtual {v4, v2, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGrid:Landroid/widget/TableLayout;

    invoke-virtual {v6, v4, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setImageClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setImageList(Ljava/util/ArrayList;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImages:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImages:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v8

    int-to-double v0, v8

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mColumnCount:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v21, v0

    div-double v19, v19, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->floor(D)D

    move-result-wide v19

    move-wide/from16 v0, v19

    double-to-int v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/searchcommon/SearchConfig;->getMaxImageCardResultRows()I

    move-result v15

    move/from16 v0, v16

    if-le v0, v15, :cond_0

    move/from16 v16, v15

    :cond_0
    const/4 v9, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v9, v0, :cond_7

    new-instance v17, Landroid/widget/TableRow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    new-instance v18, Landroid/widget/TableLayout$LayoutParams;

    const/16 v19, -0x1

    const/16 v20, -0x2

    invoke-direct/range {v18 .. v20}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    add-int/lit8 v19, v16, -0x1

    move/from16 v0, v19

    if-ne v9, v0, :cond_3

    const/16 v19, 0x0

    :goto_1
    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    const/4 v14, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mColumnCount:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v14, v0, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mColumnCount:I

    move/from16 v19, v0

    mul-int v19, v19, v9

    add-int v13, v19, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v19, v0

    const v20, 0x7f04005b

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v17

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    if-ge v13, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImages:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mColumnCount:I

    move/from16 v20, v0

    mul-int v20, v20, v9

    add-int v20, v20, v14

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    const v19, 0x7f100135

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/velvet/ui/WebImageView;

    const/4 v5, 0x0

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->getImage()Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbData()Z

    move-result v19

    if-eqz v19, :cond_1

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->getImage()Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v6

    const/16 v19, 0x0

    array-length v0, v6

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v6, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v5

    :cond_1
    if-eqz v5, :cond_4

    invoke-virtual {v12, v5}, Lcom/google/android/velvet/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_3
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/google/android/velvet/ui/WebImageView;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/TableRow$LayoutParams;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mColumnCount:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    if-ne v14, v0, :cond_5

    const/16 v19, 0x0

    :goto_4
    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v19

    move/from16 v3, v22

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v11, v7}, Landroid/widget/TableRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mMarginPixelSize:I

    move/from16 v19, v0

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->getImage()Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbUrl()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mMarginPixelSize:I

    move/from16 v19, v0

    goto :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/ImageResultCard;->mImageGrid:Landroid/widget/TableLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    :cond_7
    return-void
.end method
