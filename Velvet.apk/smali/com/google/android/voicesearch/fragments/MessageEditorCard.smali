.class public Lcom/google/android/voicesearch/fragments/MessageEditorCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "MessageEditorCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/MessageEditorController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;"
    }
.end annotation


# instance fields
.field private isContactSet:Z

.field private isMessageBodySet:Z

.field private mContactNameView:Landroid/widget/TextView;

.field private mContactNotFoundView:Landroid/widget/TextView;

.field private mContactPhoneNumberView:Landroid/widget/TextView;

.field private mContactPhoneTypeView:Landroid/widget/TextView;

.field private mContactPictureView:Landroid/widget/ImageView;

.field private mContactWrapper:Landroid/view/View;

.field private mMessageField:Landroid/widget/TextView;

.field private mNumberOnlyView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/voicesearch/fragments/MessageEditorCard;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/MessageEditorCard;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->isContactSet:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/MessageEditorCard;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/MessageEditorCard;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->checkUiReady()V

    return-void
.end method

.method private checkUiReady()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->isContactSet:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->isMessageBodySet:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->uiReady()V

    :cond_0
    return-void
.end method

.method private setNormalContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 7
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactNameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    new-instance v0, Lcom/google/android/voicesearch/fragments/MessageEditorCard$1;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPictureView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v3, v4}, Lcom/google/android/voicesearch/fragments/MessageEditorCard$1;-><init>(Lcom/google/android/voicesearch/fragments/MessageEditorCard;Landroid/widget/ImageView;Lcom/google/android/voicesearch/ui/ActionEditorView;)V

    new-array v1, v1, [Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/MessageEditorCard$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->isContactSet:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->checkUiReady()V

    goto :goto_1
.end method

.method private setNumberOnlyContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mNumberOnlyView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->isContactSet:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->checkUiReady()V

    return-void
.end method

.method private showFieldAndHideOthers(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mNumberOnlyView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mNumberOnlyView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactWrapper:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactWrapper:Landroid/view/View;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactNotFoundView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactNotFoundView:Landroid/widget/TextView;

    if-ne p1, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public hideContactField()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->showFieldAndHideOthers(Landroid/view/View;)V

    return-void
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f040072

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmIcon(I)V

    const v1, 0x7f100061

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactNameView:Landroid/widget/TextView;

    const v1, 0x7f100049

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactNotFoundView:Landroid/widget/TextView;

    const v1, 0x7f100062

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    const v1, 0x7f100063

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    const v1, 0x7f10004a

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPictureView:Landroid/widget/ImageView;

    const v1, 0x7f10016e

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mNumberOnlyView:Landroid/widget/TextView;

    const v1, 0x7f1000a7

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mMessageField:Landroid/widget/TextView;

    const v1, 0x7f1000a5

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactWrapper:Landroid/view/View;

    const/4 v1, 0x4

    new-array v1, v1, [Landroid/widget/TextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactNameView:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mMessageField:Landroid/widget/TextView;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->clearTextViews([Landroid/widget/TextView;)V

    return-object v0
.end method

.method public setMessageBody(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mMessageField:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mMessageField:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->isMessageBodySet:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->checkUiReady()V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setToContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->setNumberOnlyContact(Lcom/google/android/speech/contacts/Contact;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->setNormalContact(Lcom/google/android/speech/contacts/Contact;)V

    goto :goto_0
.end method

.method public showContactField()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactWrapper:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->showFieldAndHideOthers(Landroid/view/View;)V

    return-void
.end method

.method public showContactNotFound()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mContactNotFoundView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->showFieldAndHideOthers(Landroid/view/View;)V

    return-void
.end method

.method public showEmptyView()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mMessageField:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mMessageField:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0396

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->showNewMessage()V

    return-void
.end method

.method public showNewMessage()V
    .locals 1

    const v0, 0x7f020051

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->setConfirmIcon(I)V

    const v0, 0x7f0d0448

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->setConfirmText(I)V

    return-void
.end method

.method public showNumberOnlyField()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->mNumberOnlyView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->showFieldAndHideOthers(Landroid/view/View;)V

    return-void
.end method

.method public showSendMessage()V
    .locals 1

    const v0, 0x7f02007a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->setConfirmIcon(I)V

    const v0, 0x7f0d0449

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;->setConfirmText(I)V

    return-void
.end method
