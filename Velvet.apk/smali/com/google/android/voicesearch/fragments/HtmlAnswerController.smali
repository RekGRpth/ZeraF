.class public Lcom/google/android/voicesearch/fragments/HtmlAnswerController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "HtmlAnswerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/HtmlAnswerController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/HtmlAnswerController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mHtml:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/VelvetFactory;
    .param p2    # Lcom/google/android/voicesearch/CardController;
    .param p3    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p4    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "HtmlAnswerController state:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUrl: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mUrl:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x13

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "html"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mHtml:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x15

    return v0
.end method

.method public initUi()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/HtmlAnswerController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->getCardController()Lcom/google/android/voicesearch/CardController;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/velvet/VelvetFactory;->createJavascriptExtensionsForSearchResults(Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;

    move-result-object v0

    const-string v2, "agsa_ext"

    invoke-interface {v1, v0, v2}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController$Ui;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mHtml:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController$Ui;->setHtml(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public openUrl(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "about:blank"

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->getActionTypeLog()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;)Lcom/google/android/velvet/Query;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/velvet/Query;->fromWebView()Lcom/google/android/velvet/Query;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v0, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->startActivity(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "html"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mHtml:Ljava/lang/String;

    return-void
.end method

.method public start(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mUrl:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->mHtml:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->showCardAndPlayTts()V

    return-void
.end method
