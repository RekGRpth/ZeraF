.class public interface abstract Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
.super Ljava/lang/Object;
.source "UberRecognizerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setFinalRecognizedText(Ljava/lang/String;)V
.end method

.method public abstract setLanguage(Ljava/lang/String;)V
.end method

.method public abstract setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
.end method

.method public abstract showInitializing()V
.end method

.method public abstract showInitializingMic()V
.end method

.method public abstract showListening()V
.end method

.method public abstract showNotListening()V
.end method

.method public abstract showRecognizing()V
.end method

.method public abstract showRecording()V
.end method

.method public abstract showSoundSearchPromotedQuery()V
.end method

.method public abstract showTapToSpeak()V
.end method

.method public abstract updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V
.end method
