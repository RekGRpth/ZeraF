.class public Lcom/google/android/voicesearch/fragments/SelfNoteController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "SelfNoteController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAudioUri:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mNote:Ljava/lang/String;

.field private final mNoteToSelfEmailSubject:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/EmailSender;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Lcom/google/android/voicesearch/util/EmailSender;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNoteToSelfEmailSubject:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private saveNote(Z)V
    .locals 4
    .param p1    # Z

    new-instance v1, Lcom/google/android/voicesearch/util/EmailSender$Email;

    invoke-direct {v1}, Lcom/google/android/voicesearch/util/EmailSender$Email;-><init>()V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNoteToSelfEmailSubject:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/voicesearch/util/EmailSender$Email;->subject:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNote:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/voicesearch/util/EmailSender$Email;->body:Ljava/lang/CharSequence;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mAudioUri:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, v1, Lcom/google/android/voicesearch/util/EmailSender$Email;->attachment:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    invoke-virtual {v2, v1, p1, p0}, Lcom/google/android/voicesearch/util/EmailSender;->sendEmailToSelf(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V

    return-void

    :catch_0
    move-exception v0

    const-string v2, "SelfNoteController"

    const-string v3, "Unable to attach the audio"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "SelfNoteController"

    const-string v3, "Unable to attach the audio"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNote:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "note_text"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNote:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x11

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->canExecuteAction()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;->showSaveNote()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNote:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;->setNoteText(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;->showNewNote()V

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->saveNote(Z)V

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->saveNote(Z)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->actionComplete()V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "note_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNote:Ljava/lang/String;

    return-void
.end method

.method public start(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mNote:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SelfNoteController$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController$1;-><init>(Lcom/google/android/voicesearch/fragments/SelfNoteController;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController;->mAudioUri:Ljava/util/concurrent/Future;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->showCardAndPlayTts()V

    return-void
.end method
