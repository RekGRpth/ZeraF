.class Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard$1;
.super Ljava/lang/Object;
.source "MultipleLocalResultsCard.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard$1;->this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard$1;->this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard$1;->this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;

    # getter for: Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->access$000(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;)Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getSelectedTransportationMethod()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->setTransportationMethod(I)V

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
