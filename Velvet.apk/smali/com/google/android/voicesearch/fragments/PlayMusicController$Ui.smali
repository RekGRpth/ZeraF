.class public interface abstract Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;
.super Ljava/lang/Object;
.source "PlayMusicController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/PlayMusicController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setImageOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
.end method

.method public abstract showAlbum(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract showArtist(Ljava/lang/String;)V
.end method

.method public abstract showIsExplicit(Z)V
.end method

.method public abstract showMisc(Ljava/lang/String;)V
.end method

.method public abstract showSong(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method
