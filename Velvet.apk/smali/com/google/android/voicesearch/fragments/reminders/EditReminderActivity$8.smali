.class Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;
.super Ljava/lang/Object;
.source "EditReminderActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10009a

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->saveReminder(Lcom/google/android/speech/callback/SimpleCallback;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->finish(Z)V
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$400(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;Z)V

    goto :goto_0
.end method
