.class public final enum Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;
.super Ljava/lang/Enum;
.source "SymbolicTime.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

.field public static final enum AFTERNOON:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

.field public static final enum EVENING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

.field public static final enum MORNING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

.field public static final enum NIGHT:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

.field public static final enum TIME_UNSPECIFIED:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

.field public static final enum WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;


# instance fields
.field public final actionV2Symbol:I

.field public final defaultHour:I

.field public final defaultResId:I

.field public final sameDayResId:I

.field public final sameWeekResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const-string v1, "MORNING"

    const v3, 0x7f0d0400

    const v4, 0x7f0d0404

    const v5, 0x7f0d0408

    const/16 v6, 0x9

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->MORNING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    new-instance v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const-string v4, "AFTERNOON"

    const v6, 0x7f0d0401

    const v7, 0x7f0d0405

    const v8, 0x7f0d0409

    const/16 v9, 0xd

    move v5, v11

    move v10, v11

    invoke-direct/range {v3 .. v10}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->AFTERNOON:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    new-instance v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const-string v4, "EVENING"

    const v6, 0x7f0d0402

    const v7, 0x7f0d0406

    const v8, 0x7f0d040a

    const/16 v9, 0x12

    move v5, v12

    move v10, v12

    invoke-direct/range {v3 .. v10}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->EVENING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    new-instance v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const-string v4, "NIGHT"

    const v6, 0x7f0d0403

    const v7, 0x7f0d0407

    const v8, 0x7f0d040b

    const/16 v9, 0x14

    move v5, v13

    move v10, v13

    invoke-direct/range {v3 .. v10}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->NIGHT:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    new-instance v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const-string v4, "TIME_UNSPECIFIED"

    const/16 v9, 0x9

    move v5, v14

    move v6, v2

    move v7, v2

    move v8, v2

    move v10, v14

    invoke-direct/range {v3 .. v10}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->TIME_UNSPECIFIED:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    new-instance v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const-string v4, "WEEKEND"

    const/4 v5, 0x5

    const/16 v9, 0x9

    const/4 v10, 0x5

    move v6, v2

    move v7, v2

    move v8, v2

    invoke-direct/range {v3 .. v10}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    sget-object v1, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->MORNING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->AFTERNOON:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->EVENING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->NIGHT:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    aput-object v1, v0, v13

    sget-object v1, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->TIME_UNSPECIFIED:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    aput-object v1, v0, v14

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->$VALUES:[Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->sameDayResId:I

    iput p4, p0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->sameWeekResId:I

    iput p5, p0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultResId:I

    iput p6, p0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultHour:I

    iput p7, p0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->actionV2Symbol:I

    return-void
.end method

.method public static fromActionV2Symbol(I)Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;
    .locals 5
    .param p0    # I

    invoke-static {}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->values()[Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    iget v4, v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->actionV2Symbol:I

    if-ne v4, p0, :cond_0

    :goto_1
    return-object v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->$VALUES:[Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    return-object v0
.end method
