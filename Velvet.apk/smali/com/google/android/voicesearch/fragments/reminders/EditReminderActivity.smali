.class public Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;
.super Landroid/app/Activity;
.source "EditReminderActivity.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$9;
    }
.end annotation


# instance fields
.field private final mActionBarClickListener:Landroid/view/View$OnClickListener;

.field private mDateSpinner:Landroid/widget/Spinner;

.field private mDateSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

.field private mLabelView:Landroid/widget/EditText;

.field private final mLocationAliasListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mLocationAliasSpinner:Landroid/widget/Spinner;

.field private final mLocationTriggerTypeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mLocationTriggerTypeSpinner:Landroid/widget/Spinner;

.field private mLocationTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

.field private final mOnDateSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final mOnDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private final mOnTimeSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final mOnTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

.field private mTimeSpinner:Landroid/widget/Spinner;

.field private mTimeSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

.field private mTimeTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

.field private final mTriggerToggleListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$1;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnDateSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$2;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnTimeSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$3;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$4;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$5;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTriggerToggleListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$6;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$6;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerTypeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$7;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$7;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mActionBarClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/velvet/ui/CheckableImageView;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    return-object v0
.end method

.method static synthetic access$200(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getActionV2LocationTriggerType(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getActionV2LocationAlias(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->finish(Z)V

    return-void
.end method

.method private finish(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->finish()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getActionV2LocationAlias(I)I
    .locals 1
    .param p0    # I

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static getActionV2LocationTriggerType(I)I
    .locals 1
    .param p0    # I

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private setUpFromIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "action"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;-><init>()V

    const-string v3, "action"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    const-string v3, "preferredTriggerType"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "preferredTriggerType"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    :goto_0
    invoke-virtual {v4, v0, v3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setUpFromAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;I)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "EditReminderActivity"

    const-string v4, "Could not read EXTRA_ACTION"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private showToast(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const v6, 0x7f04002f

    new-instance v2, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetFactory;->createReminderSaver()Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;Landroid/content/Context;Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    const v2, 0x7f04002e

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v2, 0x7f04002d

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f100098

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mActionBarClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f10009a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mActionBarClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f10009b

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLabelView:Landroid/widget/EditText;

    const v2, 0x7f10009c

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/CheckableImageView;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    const v2, 0x7f10009d

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/CheckableImageView;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    const v2, 0x7f10009f

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    const v2, 0x7f1000a0

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    const v2, 0x7f1000a1

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerTypeSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerTypeSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerTypeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v2, 0x7f1000a2

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v2, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f003d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v6, v3}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnDateSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v2, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f003e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v6, v3}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnTimeSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerTypeSpinner:Landroid/widget/Spinner;

    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasSpinner:Landroid/widget/Spinner;

    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0040

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTriggerToggleListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ui/CheckableImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTriggerToggleListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ui/CheckableImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    invoke-virtual {v2, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->restoreInstanceState(Landroid/os/Bundle;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->setUpFromIntent(Landroid/content/Intent;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setUpWithDefaults()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->setUpFromIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public pickDate(III)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->showCustomDate()V

    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    move-object v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    return-void
.end method

.method public pickTime(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->showCustomTime()V

    new-instance v0, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mOnTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    const/4 v5, 0x1

    move-object v1, p0

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    invoke-virtual {v0}, Landroid/app/TimePickerDialog;->show()V

    return-void
.end method

.method public setCustomDate(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->setCustomValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setCustomTime(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->setCustomValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLabelView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLabelView:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public setLocationAlias(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasSpinner:Landroid/widget/Spinner;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setLocationTriggerType(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasSpinner:Landroid/widget/Spinner;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const/4 v3, 0x2

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    sget-object v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-ne p1, v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setVisibility(I)V

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$9;->$SwitchMap$com$google$android$voicesearch$fragments$reminders$SymbolicTime:[I

    invoke-virtual {p1}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public setTriggerType(I)V
    .locals 5
    .param p1    # I

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-ne p1, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/ui/CheckableImageView;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/CheckableImageView;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/ui/CheckableImageView;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/CheckableImageView;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mLocationAliasSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1
.end method

.method public showCustomDate()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->getCustomValuePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public showCustomTime()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeSpinnerAdapter:Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->getCustomValuePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public showDateToday()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public showDateTomorrow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mDateSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public showSaveError()V
    .locals 1

    const v0, 0x7f0d03fc

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->showToast(I)V

    return-void
.end method

.method public showSaveSuccess()V
    .locals 1

    const v0, 0x7f0d03fb

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->showToast(I)V

    return-void
.end method
