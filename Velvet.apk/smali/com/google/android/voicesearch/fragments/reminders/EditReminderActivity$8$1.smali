.class Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;
.super Ljava/lang/Object;
.source "EditReminderActivity.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;->this$1:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;->this$1:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->showSaveSuccess()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;->this$1:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    # invokes: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->finish(Z)V
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$400(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;->this$1:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->showSaveError()V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$8$1;->onResult(Ljava/lang/Boolean;)V

    return-void
.end method
