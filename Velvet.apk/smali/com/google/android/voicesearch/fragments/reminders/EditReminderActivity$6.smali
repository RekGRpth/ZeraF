.class Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$6;
.super Ljava/lang/Object;
.source "EditReminderActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$6;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$6;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    move-result-object v0

    # invokes: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->getActionV2LocationTriggerType(I)I
    invoke-static {p3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$200(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLocationTriggerType(I)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
