.class Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;
.super Ljava/lang/Object;
.source "ShowContactInformationCard.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 5
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->access$000(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->access$000(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0459

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mCopyToClipboardMenuItemListener:Landroid/view/MenuItem$OnMenuItemClickListener;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->access$100(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method
