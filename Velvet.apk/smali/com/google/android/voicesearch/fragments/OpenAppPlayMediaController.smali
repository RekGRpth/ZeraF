.class public Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;
.super Lcom/google/android/voicesearch/fragments/PlayMediaController;
.source "OpenAppPlayMediaController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/PlayMediaController",
        "<",
        "Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/voicesearch/fragments/PlayMediaController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    return-void
.end method

.method private updateUi(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .param p2    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->isPlayStoreLink(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getAppItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;->setTitle(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->setImage(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;->setTitle(Ljava/lang/String;)V

    const v1, 0x7f0d03be

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;->setAppLabel(I)V

    goto :goto_0
.end method


# virtual methods
.method public appSelected(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-super {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->appSelected(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->updateUi(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    return-void
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected getGoogleContentAppIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLocalApps(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Lcom/google/common/collect/ImmutableList;
    .locals 4
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;",
            ")",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Sets;->newLinkedHashSet()Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {v2, v1}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivities(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getAppItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getAppItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$AppItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivities(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_1
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    return-object v2
.end method

.method protected getMimeType()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getOpenFromSearchActionString()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPreviewIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->initUi()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->getSelectedApp()Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->updateUi(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->uiReady()V

    return-void
.end method

.method protected setImage(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->getSelectedApp()Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->isPlayStoreLink(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->setImage(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController$Ui;->showImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
