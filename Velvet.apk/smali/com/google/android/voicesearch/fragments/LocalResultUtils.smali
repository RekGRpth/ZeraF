.class public Lcom/google/android/voicesearch/fragments/LocalResultUtils;
.super Ljava/lang/Object;
.source "LocalResultUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createIntentForAction(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Landroid/content/Intent;
    .locals 5
    .param p0    # I
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .param p2    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .param p3    # I

    const/4 v2, 0x4

    if-ne p0, v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "tel"

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_0
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/voicesearch/util/MapUtil;->getMapsIntent(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static getActionIconImageResource(I)I
    .locals 1
    .param p0    # I

    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    :cond_0
    packed-switch p0, :pswitch_data_0

    const v0, 0x7f02006b

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f02004b

    goto :goto_0

    :pswitch_1
    const v0, 0x7f02004e

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020064

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static getActionLabelStringId(I)I
    .locals 1
    .param p0    # I

    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    :cond_0
    packed-switch p0, :pswitch_data_0

    const v0, 0x7f0d0004

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0d0003

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0d0006

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0d0005

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static getActionTypeLog(I)I
    .locals 1
    .param p0    # I

    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    :cond_0
    packed-switch p0, :pswitch_data_0

    const/16 v0, 0xe

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xd

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xf

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
