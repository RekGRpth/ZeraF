.class public Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "PlainTitleAndTextController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mContent:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x25

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "content"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mContent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x2a

    return v0
.end method

.method public initUi()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mContent:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController$Ui;->setText(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mTitle:Ljava/lang/String;

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mContent:Ljava/lang/String;

    return-void
.end method

.method public start(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mTitle:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->mContent:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->showCard()V

    return-void
.end method
