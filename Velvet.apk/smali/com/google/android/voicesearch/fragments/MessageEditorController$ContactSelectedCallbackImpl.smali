.class Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;
.super Ljava/lang/Object;
.source "MessageEditorController.java"

# interfaces
.implements Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/MessageEditorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactSelectedCallbackImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/fragments/MessageEditorController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/fragments/MessageEditorController;Lcom/google/android/voicesearch/fragments/MessageEditorController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/MessageEditorController;
    .param p2    # Lcom/google/android/voicesearch/fragments/MessageEditorController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;-><init>(Lcom/google/android/voicesearch/fragments/MessageEditorController;)V

    return-void
.end method


# virtual methods
.method public onBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->bailOut()V

    return-void
.end method

.method public onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    # setter for: Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->access$102(Lcom/google/android/voicesearch/fragments/MessageEditorController;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    # invokes: Lcom/google/android/voicesearch/fragments/MessageEditorController;->hasMessageBody()Z
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->access$200(Lcom/google/android/voicesearch/fragments/MessageEditorController;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->bailOut()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->showCard()V

    goto :goto_0
.end method

.method public onNoContactFound()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->showCard()V

    return-void
.end method

.method public onShowingDisambiguation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->showNoCard()V

    return-void
.end method
