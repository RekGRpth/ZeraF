.class public Lcom/google/android/voicesearch/fragments/MessageEditorController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "MessageEditorController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;,
        Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

.field private final mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

.field private mHasRecipient:Z

.field private mSelectedContact:Lcom/google/android/speech/contacts/Contact;

.field private final mSendResultCallback:Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/contacts/ContactSelectController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/contacts/ContactSelectController;
    .param p3    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p4    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    new-instance v0, Lcom/google/android/voicesearch/fragments/MessageEditorController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController$1;-><init>(Lcom/google/android/voicesearch/fragments/MessageEditorController;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSendResultCallback:Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/voicesearch/fragments/MessageEditorController;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/MessageEditorController;
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/MessageEditorController;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->hasMessageBody()Z

    move-result v0

    return v0
.end method

.method private hasMessageBody()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getMessageBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private internalStart()V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->showCard()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getSpokenNumber(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/speech/contacts/Contact;->newPhoneNumberOnlyContact(Ljava/lang/String;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->showCard()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getRecipientCount()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getRecipient(I)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;->getContactCount()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mHasRecipient:Z

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mHasRecipient:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->requireShowCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getRecipient(I)Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContactGroup;->getContactList()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->SMS:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->hasMessageBody()Z

    move-result v3

    sget-object v4, Lcom/google/android/voicesearch/util/PhoneActionUtils;->MESSAGE_DEFAULT_CONTACT_TYPE:Ljava/lang/String;

    new-instance v5, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;

    const/4 v7, 0x0

    invoke-direct {v5, p0, v7}, Lcom/google/android/voicesearch/fragments/MessageEditorController$ContactSelectedCallbackImpl;-><init>(Lcom/google/android/voicesearch/fragments/MessageEditorController;Lcom/google/android/voicesearch/fragments/MessageEditorController$1;)V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->pickContact(Ljava/util/List;Lcom/google/android/voicesearch/contacts/ContactSelectMode;ZLjava/lang/String;Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->showCard()V

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->hasMessageBody()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method public initUi()V
    .locals 5

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v2, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-interface {v2, v4}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->setToContact(Lcom/google/android/speech/contacts/Contact;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v2}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->showNumberOnlyField()V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->setMessageBody(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->hasMessageBody()Z

    move-result v2

    if-nez v2, :cond_4

    :goto_2
    if-nez v0, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->showEmptyView()V

    :goto_3
    return-void

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->showContactField()V

    goto :goto_1

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mHasRecipient:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->showContactNotFound()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->hideContactField()V

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_6

    if-eqz v1, :cond_7

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->showNewMessage()V

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getMessageBody()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->setMessageBody(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;->showSendMessage()V

    goto :goto_3
.end method

.method protected internalBailOut()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getSendSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->startActivity(Landroid/content/Intent;)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected internalExecuteAction()V
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.google.android.apps.googlevoice.action.AUTO_SEND"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v3}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    :goto_0
    const-string v3, ","

    invoke-static {v3, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "smsto"

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v3, "android.intent.extra.TEXT"

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSendResultCallback:Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;

    invoke-virtual {p0, v1, v3}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z

    return-void

    :cond_0
    new-array v0, v4, [Ljava/lang/String;

    goto :goto_0
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/ActionV2Protos$SMSAction;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    return-void
.end method

.method public restoreStart()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->internalStart()V

    return-void
.end method

.method public start(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageEditorController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->internalStart()V

    return-void
.end method
