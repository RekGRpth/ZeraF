.class public interface abstract Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;
.super Ljava/lang/Object;
.source "GogglesGenericController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/GogglesGenericController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract hideImage()V
.end method

.method public abstract setActions(Ljava/util/List;Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/GogglesGenericAction;",
            ">;",
            "Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract setFifeImageUrl(Ljava/lang/String;)V
.end method

.method public abstract setSessionId(Ljava/lang/String;)V
.end method

.method public abstract setSubtitle(Ljava/lang/String;)V
.end method

.method public abstract setTitle(Ljava/lang/String;)V
.end method
