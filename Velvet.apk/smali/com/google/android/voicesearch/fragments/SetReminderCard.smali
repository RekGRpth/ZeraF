.class public Lcom/google/android/voicesearch/fragments/SetReminderCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "SetReminderCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/SetReminderController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;"
    }
.end annotation


# instance fields
.field private mAddTriggerBubble:Landroid/view/View;

.field private mLabelView:Landroid/widget/TextView;

.field private mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

.field private mSetPlaceButton:Landroid/widget/TextView;

.field private mSetTimeButton:Landroid/widget/TextView;

.field private mTriggerView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private setTriggerText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mTriggerView:Landroid/widget/TextView;

    invoke-super {p0, v0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->showTextIfNonEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showToast(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v0, 0x7f0400b2

    invoke-virtual {p0, p2, p3, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100216

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mAddTriggerBubble:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100214

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mLabelView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100215

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mTriggerView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100217

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mSetTimeButton:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100218

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mSetPlaceButton:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mSetTimeButton:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SetReminderCard$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard$1;-><init>(Lcom/google/android/voicesearch/fragments/SetReminderCard;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mSetPlaceButton:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SetReminderCard$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard$2;-><init>(Lcom/google/android/voicesearch/fragments/SetReminderCard;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f02007d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setConfirmIcon(I)V

    const v0, 0x7f0d03f3

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setConfirmText(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    return-object v0
.end method

.method public setAbsoluteTimeTrigger(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setTriggerText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setArrivingTrigger(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    packed-switch p1, :pswitch_data_0

    invoke-virtual {p0, p2}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setArrivingTrigger(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    const v0, 0x7f0d03ec

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setTriggerText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0d03ee

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setArrivingTrigger(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d03ea

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setTriggerText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mLabelView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d03f2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0d03f4

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setConfirmText(I)V

    const v0, 0x7f020051

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setConfirmIcon(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mLabelView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setLeavingTrigger(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    packed-switch p1, :pswitch_data_0

    invoke-virtual {p0, p2}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setLeavingTrigger(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    const v0, 0x7f0d03ed

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setTriggerText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0d03ef

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setLeavingTrigger(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d03eb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->setTriggerText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setNoTrigger(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mTriggerView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mAddTriggerBubble:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setContentClickable(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmationEnabled(Z)V

    :cond_0
    return-void
.end method

.method public showSaveError()V
    .locals 1

    const v0, 0x7f0d03fc

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->showToast(I)V

    return-void
.end method

.method public showSaveSuccess()V
    .locals 1

    const v0, 0x7f0d03fb

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderCard;->showToast(I)V

    return-void
.end method
