.class public Lcom/google/android/voicesearch/fragments/DictionaryResultController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "DictionaryResultController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method


# virtual methods
.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->toByteArray()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x1a

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x1d

    return v0
.end method

.method public goToDictionary(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->getActionTypeLog()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public initUi()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getDictionaryWord()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;->setWord(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPronunciation()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;->setPronunciation(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getPartOfSpeechMeaningList()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSynonymsHeader()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getSynonymList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;->setMeaningsAndSynonyms(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->getExternalDictionaryLinkList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;->setExternalDictionaryLinks(Ljava/util/List;)V

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;->parseFrom([B)Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    :cond_0
    return-void
.end method

.method public start(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->showCardAndPlayTts()V

    return-void
.end method
