.class public Lcom/google/android/voicesearch/fragments/SportsResultController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "SportsResultController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/SportsResultController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/SportsResultController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x1b

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x1e

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SportsResultController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/SportsResultController$Ui;->setMatchData(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getInfoUrlTitle()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getInfoUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getInfoUrlTitle()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/SportsResultController$Ui;->setInfoLink(I)V

    :cond_0
    return-void
.end method

.method onInfoClick()V
    .locals 3

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultController;->getActionTypeLog()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getInfoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SportsResultController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;

    check-cast p1, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SportsResultController;->mMatchData:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultController;->showCardAndPlayTts()V

    return-void
.end method
