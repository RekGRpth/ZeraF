.class Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;
.super Ljava/lang/Object;
.source "HtmlAnswerCard.java"

# interfaces
.implements Lcom/google/android/velvet/ActivityLifecycleObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;->this$0:Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;->this$0:Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->access$000(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;->this$0:Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->access$000(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    :cond_0
    return-void
.end method

.method public onActivityStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;->this$0:Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->access$000(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;->this$0:Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->access$000(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    :cond_0
    return-void
.end method
