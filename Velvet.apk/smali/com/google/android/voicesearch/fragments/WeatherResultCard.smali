.class public Lcom/google/android/voicesearch/fragments/WeatherResultCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "WeatherResultCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/WeatherResultController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/WeatherResultController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/WeatherResultController$Ui;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContent:Lcom/google/android/velvet/cards/WeatherCard;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/fragments/WeatherResultCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/voicesearch/fragments/WeatherResultCard;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    new-instance v0, Lcom/google/android/velvet/cards/WeatherCard;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/cards/WeatherCard;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/WeatherResultCard;->mContent:Lcom/google/android/velvet/cards/WeatherCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/WeatherResultCard;->mContent:Lcom/google/android/velvet/cards/WeatherCard;

    return-object v0
.end method

.method public setData(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V
    .locals 29
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasLocation()Z

    move-result v26

    if-eqz v26, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getLocation()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;->hasFormattedAddress()Z

    move-result v26

    if-eqz v26, :cond_8

    invoke-virtual/range {v22 .. v22}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;->getFormattedAddress()Ljava/lang/String;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasCurrent()Z

    move-result v26

    if-eqz v26, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getCurrent()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->hasTemp()Z

    move-result v26

    if-eqz v26, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getCurrent()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    move-result-object v8

    new-instance v6, Lcom/google/android/velvet/cards/WeatherCard$Builder;

    invoke-virtual {v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->getTemp()I

    move-result v26

    move/from16 v0, v26

    invoke-direct {v6, v4, v0}, Lcom/google/android/velvet/cards/WeatherCard$Builder;-><init>(Ljava/lang/String;I)V

    move-object v5, v6

    invoke-virtual {v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->hasCondition()Z

    move-result v26

    if-eqz v26, :cond_2

    invoke-virtual {v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->getCondition()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;->hasText()Z

    move-result v26

    if-eqz v26, :cond_1

    invoke-virtual {v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;->getText()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setConditions(Ljava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    :cond_1
    invoke-virtual {v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;->hasImageUrl()Z

    move-result v26

    if-eqz v26, :cond_2

    invoke-virtual {v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setIcon(Landroid/net/Uri;)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    :cond_2
    invoke-virtual {v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->hasWindSpeed()Z

    move-result v26

    if-eqz v26, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getInMetricUnits()Z

    move-result v26

    if-eqz v26, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/fragments/WeatherResultCard;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0d013a

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    :goto_1
    invoke-virtual {v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->getWindSpeed()I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v6, v0, v1}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setWindSpeed(ILjava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    :cond_3
    invoke-virtual {v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->hasChanceOfPrecipitation()Z

    move-result v26

    if-eqz v26, :cond_4

    invoke-virtual {v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;->getChanceOfPrecipitation()I

    move-result v26

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->setChanceOfPrecipitation(I)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    :cond_4
    :goto_2
    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasForecastStartDate()Z

    move-result v26

    if-eqz v26, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getForecastStartDate()Ljava/lang/String;

    move-result-object v10

    new-instance v17, Ljava/text/SimpleDateFormat;

    const-string v26, "yyyy-MM-dd"

    sget-object v27, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    :try_start_0
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v9

    new-instance v12, Ljava/util/GregorianCalendar;

    invoke-direct {v12}, Ljava/util/GregorianCalendar;-><init>()V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v12, v9}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v12, v0}, Ljava/util/Calendar;->setLenient(Z)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v11, v12

    :cond_5
    :goto_3
    const/16 v19, 0x0

    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getDailyForecastCount()I

    move-result v26

    move/from16 v0, v19

    move/from16 v1, v26

    if-ge v0, v1, :cond_b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getDailyForecast(I)Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;

    move-result-object v16

    const/16 v21, 0x0

    if-eqz v11, :cond_6

    const/16 v26, 0x7

    move/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/util/Calendar;->get(I)I

    move-result v14

    const/16 v26, 0x14

    move/from16 v0, v26

    invoke-static {v14, v0}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v21

    const/16 v26, 0x7

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v11, v0, v1}, Ljava/util/Calendar;->add(II)V

    :cond_6
    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;->hasCondition()Z

    move-result v26

    if-eqz v26, :cond_7

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;->getCondition()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;->hasImageUrl()Z

    move-result v26

    if-eqz v26, :cond_7

    invoke-virtual {v13}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    :cond_7
    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;->getLowTemp()I

    move-result v23

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;->getHighTemp()I

    move-result v18

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move/from16 v2, v23

    move/from16 v3, v18

    invoke-virtual {v5, v0, v1, v2, v3}, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->addForecast(Ljava/lang/CharSequence;Landroid/net/Uri;II)Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;

    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    :cond_8
    invoke-virtual/range {v22 .. v22}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;->hasCity()Z

    move-result v26

    if-eqz v26, :cond_0

    invoke-virtual/range {v22 .. v22}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;->getCity()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/fragments/WeatherResultCard;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0d0138

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_1

    :cond_a
    new-instance v5, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;

    invoke-direct {v5, v4}, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_0
    move-exception v15

    :goto_5
    sget-object v26, Lcom/google/android/voicesearch/fragments/WeatherResultCard;->TAG:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Unrecognized date value: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/WeatherResultCard;->mContent:Lcom/google/android/velvet/cards/WeatherCard;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->update(Lcom/google/android/velvet/cards/WeatherCard;)V

    return-void

    :catch_1
    move-exception v15

    move-object v11, v12

    goto :goto_5
.end method
