.class public interface abstract Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;
.super Ljava/lang/Object;
.source "ShowContactInformationController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/ShowContactInformationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setContact(Lcom/google/android/speech/contacts/Contact;)V
.end method

.method public abstract showContactDetailsNotFound()V
.end method

.method public abstract showContactNotFound()V
.end method

.method public abstract showEmailAddressNotFound()V
.end method

.method public abstract showEmailAddresses(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract showPhoneNumberNotFound()V
.end method

.method public abstract showPhoneNumbers(Ljava/util/List;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract showPostalAddressNotFound()V
.end method

.method public abstract showPostalAddresses(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation
.end method
