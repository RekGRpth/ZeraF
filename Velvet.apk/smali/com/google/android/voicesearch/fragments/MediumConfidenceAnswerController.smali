.class public Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "MediumConfidenceAnswerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

.field private mShouldCardShowSources:Z


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x12

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "medium_confidence.answer_data"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "medium_confidence.showing_sources"

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;->isShowingSources()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x14

    return v0
.end method

.method public handleSourceClick(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getAnswer()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;->setAnswer(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getDisclaimer()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;->setDisclaimer(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->getSourceList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;->setSourceList(Ljava/util/List;)V

    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mShouldCardShowSources:Z

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;->setShowingSources(Z)V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "medium_confidence.answer_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/speechservice/AnswerData;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    const-string v1, "medium_confidence.showing_sources"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mShouldCardShowSources:Z

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/speechservice/AnswerData;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mAnswer:Lcom/google/android/voicesearch/speechservice/AnswerData;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->mShouldCardShowSources:Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->showCardAndPlayTts()V

    return-void
.end method
