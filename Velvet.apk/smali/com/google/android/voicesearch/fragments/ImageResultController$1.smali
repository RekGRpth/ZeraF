.class Lcom/google/android/voicesearch/fragments/ImageResultController$1;
.super Ljava/lang/Object;
.source "ImageResultController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/ImageResultController;->createImageClickListener(Z)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/ImageResultController;

.field final synthetic val$fetchMore:Z


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/ImageResultController;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ImageResultController$1;->this$0:Lcom/google/android/voicesearch/fragments/ImageResultController;

    iput-boolean p2, p0, Lcom/google/android/voicesearch/fragments/ImageResultController$1;->val$fetchMore:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ImageResultController$1;->this$0:Lcom/google/android/voicesearch/fragments/ImageResultController;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/fragments/ImageResultController;->getActionTypeLog()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ImageResultController$1;->this$0:Lcom/google/android/voicesearch/fragments/ImageResultController;

    # getter for: Lcom/google/android/voicesearch/fragments/ImageResultController;->mPackageName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ImageResultController;->access$000(Lcom/google/android/voicesearch/fragments/ImageResultController;)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/voicesearch/fragments/ImageResultController$1;->val$fetchMore:Z

    invoke-static {v2, v0, v3}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->createPhotoViewIntent(Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ImageResultController$1;->this$0:Lcom/google/android/voicesearch/fragments/ImageResultController;

    invoke-virtual {v2, v1}, Lcom/google/android/voicesearch/fragments/ImageResultController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method
