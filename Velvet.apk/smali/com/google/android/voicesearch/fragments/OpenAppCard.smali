.class public Lcom/google/android/voicesearch/fragments/OpenAppCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "OpenAppCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/OpenAppController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/OpenAppController$Ui;"
    }
.end annotation


# instance fields
.field private mIcon:Landroid/widget/ImageView;

.field private mNameLabel:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f040084

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/OpenAppCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f10019d

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenAppCard;->mNameLabel:Landroid/widget/TextView;

    const v1, 0x7f10019c

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenAppCard;->mIcon:Landroid/widget/ImageView;

    const v1, 0x7f0d03be

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmText(I)V

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setNoConfirmIcon()V

    return-object v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenAppCard;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenAppCard;->mNameLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
