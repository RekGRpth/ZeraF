.class public Lcom/google/android/voicesearch/settings/SettingsUtils;
.super Ljava/lang/Object;
.source "SettingsUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {p0, p1}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getDebugServer(Ljava/lang/String;Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    move-result-object v0

    return-object v0
.end method

.method private static getDebugServer(Ljava/lang/String;Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/voicesearch/settings/Settings;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug()Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "VS.SettingsUtils"

    const-string v5, "Debug info section not found"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;->getDebugServerList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->hasLabel()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_2
    const-string v4, "VS.SettingsUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid or missing override: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0
.end method

.method public static getDefaultSpokenLanguageSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/Settings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/settings/Settings;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/settings/SettingsUtils$8;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/settings/SettingsUtils$8;-><init>(Lcom/google/android/voicesearch/settings/Settings;)V

    return-object v0
.end method

.method public static getMaxBluetoothScoVolumeSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/Settings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/settings/Settings;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/settings/SettingsUtils$9;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/settings/SettingsUtils$9;-><init>(Lcom/google/android/voicesearch/settings/Settings;)V

    return-object v0
.end method

.method public static getPairHttpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/Settings;
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/settings/Settings;",
            "Lcom/google/android/searchcommon/SearchConfig;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/settings/SettingsUtils$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/settings/SettingsUtils$2;-><init>(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)V

    return-object v0
.end method

.method public static getSingleHttpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/Settings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/settings/Settings;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/settings/SettingsUtils$4;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/settings/SettingsUtils$4;-><init>(Lcom/google/android/voicesearch/settings/Settings;)V

    return-object v0
.end method

.method public static getSpokenLocaleBcp47Supplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/Settings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/settings/Settings;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/settings/SettingsUtils$5;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/settings/SettingsUtils$5;-><init>(Lcom/google/android/voicesearch/settings/Settings;)V

    return-object v0
.end method

.method public static getTcpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/Settings;
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/settings/Settings;",
            "Lcom/google/android/searchcommon/SearchConfig;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/settings/SettingsUtils$1;-><init>(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)V

    return-object v0
.end method
