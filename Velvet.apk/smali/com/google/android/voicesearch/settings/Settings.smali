.class public Lcom/google/android/voicesearch/settings/Settings;
.super Ljava/lang/Object;
.source "Settings.java"

# interfaces
.implements Lcom/google/android/speech/SpeechSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mTempConfiguration:Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p4    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    new-instance v4, Lcom/google/android/searchcommon/GserviceWrapper;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/searchcommon/GserviceWrapper;-><init>(Landroid/content/ContentResolver;)V

    move-object v0, p0

    move-object v1, p2

    move-object v5, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/voicesearch/settings/Settings;-><init>(Lcom/google/android/searchcommon/GsaPreferenceController;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/GserviceWrapper;Landroid/content/Context;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/SearchConfig;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/GsaPreferenceController;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/GserviceWrapper;Landroid/content/Context;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Ljava/util/concurrent/ExecutorService;
    .param p4    # Lcom/google/android/searchcommon/GserviceWrapper;
    .param p5    # Landroid/content/Context;
    .param p6    # Lcom/google/android/searchcommon/SearchSettings;
    .param p7    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/settings/Settings;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    new-instance v0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;-><init>(Lcom/google/android/searchcommon/GsaPreferenceController;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/GserviceWrapper;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    new-instance v0, Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;

    invoke-direct {v0}, Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mTempConfiguration:Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;

    iput-object p5, p0, Lcom/google/android/voicesearch/settings/Settings;->mContext:Landroid/content/Context;

    iput-object p6, p0, Lcom/google/android/voicesearch/settings/Settings;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p7, p0, Lcom/google/android/voicesearch/settings/Settings;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    new-instance v1, Lcom/google/android/voicesearch/settings/Settings$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/settings/Settings$1;-><init>(Lcom/google/android/voicesearch/settings/Settings;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->addListener(Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/settings/Settings;
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/settings/Settings;->verifySpokenLocaleBcp47(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    return-void
.end method

.method private getPrefs()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method private setDefaultSpokenLocaleBcp47()Ljava/lang/String;
    .locals 4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDefaultMainSpokenLanguageBcp47(Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spoken-language-bcp-47"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spoken-language-default"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-object v0
.end method

.method private verifySpokenLocaleBcp47(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 6
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v4, "spoken-language-bcp-47"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v3, :cond_1

    invoke-static {p1, v3}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->isSupportedBcp47Locale(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v1, p1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDefaultMainSpokenLanguageBcp47(Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v4, "spoken-language-default"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eq v0, v4, :cond_0

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "spoken-language-default"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->setDefaultSpokenLocaleBcp47()Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public addConfigurationListener(Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->addListener(Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;)V

    return-void
.end method

.method public asyncLoad()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->asyncLoad()V

    return-void
.end method

.method public getAuthTokenLastRefreshTimestamp()J
    .locals 4

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "authTokenLastRefreshTimestamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public getConfigurationTimestamp()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getTimestamp()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultActionCountDownMs()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getVoiceSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;->getActionCountDownMsec()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getInstallId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getVoiceSearchInstallId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOverrideConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getOverrideConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getSpokenLocaleBcp47()Ljava/lang/String;
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "spoken-language-bcp-47"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->setDefaultSpokenLocaleBcp47()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getTempConfiguration()Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mTempConfiguration:Lcom/google/android/voicesearch/settings/TempGStaticConfiguration;

    return-object v0
.end method

.method public getTtsMode()Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ttsMode"

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/Settings;->mContext:Landroid/content/Context;

    const v3, 0x7f0d03db

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBluetoothHeadsetEnabled()Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "bluetoothHeadset"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isDebugAudioLoggingEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isDebugAudioLoggingEnabled()Z

    move-result v0

    return v0
.end method

.method public isDefaultSpokenLanguage()Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "spoken-language-default"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isHotwordDetectorEnabled()Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "hotwordDetector"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isPersonalizationEnabled()Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "personalizedResults"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isProfanityFilterEnabled()Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "profanityFilter"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isSingleRequestArchitectureEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    return v0
.end method

.method public resetAuthTokenLastRefreshTimestamp()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "authTokenLastRefreshTimestamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setOverrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->setOverrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    return-void
.end method

.method public declared-synchronized setSpokenLanguageBcp47(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "spoken-language-bcp-47"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spoken-language-bcp-47"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spoken-language-default"

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public updateAuthTokenLastRefreshTimestamp()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/Settings;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "authTokenLastRefreshTimestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public updateStaticConfiguration()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/Settings;->mGStaticConfiguration:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    iget-object v1, p0, Lcom/google/android/voicesearch/settings/Settings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->update(Lcom/google/android/searchcommon/util/HttpHelper;)V

    return-void
.end method
