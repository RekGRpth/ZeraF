.class final Lcom/google/android/voicesearch/settings/SettingsUtils$5;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/settings/SettingsUtils;->getSpokenLocaleBcp47Supplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$settings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$5;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/settings/SettingsUtils$5;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$5;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
