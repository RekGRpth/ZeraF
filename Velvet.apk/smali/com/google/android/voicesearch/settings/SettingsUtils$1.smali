.class final Lcom/google/android/voicesearch/settings/SettingsUtils$1;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/settings/SettingsUtils;->getTcpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)Lcom/google/common/base/Supplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$searchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field final synthetic val$settings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p2, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->val$searchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .locals 4

    iget-object v3, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->val$searchConfig:Lcom/google/android/searchcommon/SearchConfig;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->val$searchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getS3ServerOverrideForSingleRequest()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->isSingleRequestArchitectureEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    # invokes: Lcom/google/android/voicesearch/settings/SettingsUtils;->getDebugServer(Ljava/lang/String;Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;
    invoke-static {v2, v3}, Lcom/google/android/voicesearch/settings/SettingsUtils;->access$000(Ljava/lang/String;Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/settings/SettingsUtils$1;->get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    move-result-object v0

    return-object v0
.end method
