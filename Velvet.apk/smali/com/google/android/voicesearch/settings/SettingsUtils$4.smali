.class final Lcom/google/android/voicesearch/settings/SettingsUtils$4;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/settings/SettingsUtils;->getSingleHttpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$settings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$4;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/SettingsUtils$4;->val$settings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/settings/SettingsUtils$4;->get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v0

    return-object v0
.end method
