.class public Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;
.super Ljava/lang/Object;
.source "VelvetCardController.java"

# interfaces
.implements Lcom/google/android/voicesearch/speechservice/ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/VelvetCardController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UnusedActionListenerLogger"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/VelvetCardController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/VelvetCardController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private log(I)V
    .locals 2
    .param p1    # I

    const/16 v0, 0x6c

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onAddCalendarEventAction(Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;

    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onAtHomePowerControlAction(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/16 v0, 0x1d

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onDictionaryResults(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onEmailAction(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onFlightResults(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onGogglesAction()V
    .locals 0

    return-void
.end method

.method public onGogglesGenericResult(Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    return-void
.end method

.method public onHelpAction(Lcom/google/majel/proto/ActionV2Protos$HelpAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    const/16 v0, 0x25

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onHighConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/AnswerData;

    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onHtmlAnswer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onImageResults(Lcom/google/android/voicesearch/speechservice/ImageResultData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/ImageResultData;

    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onMediumConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/AnswerData;

    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onMultipleLocalResults(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getActionType()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->getActionTypeLog(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onOpenAppPlayMediaAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onOpenApplicationAction(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onOpenBookAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onOpenURLAction(Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onPhoneAction(Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;->getAction()Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getActionTypeLog(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onPlayMovieAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onPlayMusicAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onPuntAction(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/PuntController$Data;

    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onQueryCalendarAction()V
    .locals 1

    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onRecognizedContact(Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    return-void
.end method

.method public onSearchResults(Lcom/google/majel/proto/PeanutProtos$Url;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Url;

    return-void
.end method

.method public onSelfNoteAction(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onSetAlarmAction(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onSetReminderAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onShowContactInformationAction(Ljava/util/List;I)V
    .locals 1
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;I)V"
        }
    .end annotation

    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onSingleLocalResult(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getActionType()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->getActionTypeLog(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onSmsAction(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onSoundSearchAction()V
    .locals 0

    return-void
.end method

.method public onSportsResult(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    const/16 v0, 0x1b

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onUnsupportedAction(Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;

    return-void
.end method

.method public onUpdateSocialNetwork(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method

.method public onWeatherResults(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$UnusedActionListenerLogger;->log(I)V

    return-void
.end method
