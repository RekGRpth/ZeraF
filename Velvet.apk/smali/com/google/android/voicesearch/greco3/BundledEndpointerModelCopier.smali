.class public Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;
.super Ljava/lang/Object;
.source "BundledEndpointerModelCopier.java"

# interfaces
.implements Lcom/google/android/speech/embedded/EndpointerModelCopier;


# static fields
.field private static final BUNDLED_EP_FILE_NAMES:[Ljava/lang/String;

.field private static final BUNDLED_EP_RESOURCES:[I


# instance fields
.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x4

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->BUNDLED_EP_RESOURCES:[I

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ep_acoustic_model"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "endpointer_dictation.config"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "endpointer_voicesearch.config"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "metadata"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->BUNDLED_EP_FILE_NAMES:[Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f080006
        0x7f080004
        0x7f080005
        0x7f080008
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->mResources:Landroid/content/res/Resources;

    return-void
.end method

.method private static hasEndpointerResources(Lcom/google/android/speech/embedded/Greco3DataManager;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->ENDPOINTER_VOICESEARCH:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->ENDPOINTER_DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public copyEndpointerModels(Lcom/google/common/base/Supplier;Lcom/google/android/speech/embedded/Greco3DataManager;)Z
    .locals 1
    .param p2    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/google/android/speech/embedded/Greco3DataManager;",
            ")Z"
        }
    .end annotation

    const-string v0, "en-US"

    invoke-static {p2, v0}, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->hasEndpointerResources(Lcom/google/android/speech/embedded/Greco3DataManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->doCopyModels(Lcom/google/common/base/Supplier;)Z

    move-result v0

    goto :goto_0
.end method

.method doCopyModels(Lcom/google/common/base/Supplier;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/io/File;",
            ">;)Z"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-interface {p1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    new-instance v4, Ljava/io/File;

    const-string v7, "en-US"

    invoke-direct {v4, v0, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "VS.EPModelCopier"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to create model dir: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v6

    :cond_0
    const/4 v1, 0x0

    :goto_1
    :try_start_0
    sget-object v7, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->BUNDLED_EP_RESOURCES:[I

    array-length v7, v7

    if-ge v1, v7, :cond_1

    iget-object v7, p0, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->mResources:Landroid/content/res/Resources;

    sget-object v8, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->BUNDLED_EP_RESOURCES:[I

    aget v8, v8, v1

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v7, Ljava/io/File;

    sget-object v8, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;->BUNDLED_EP_FILE_NAMES:[Ljava/lang/String;

    aget-object v8, v8, v1

    invoke-direct {v7, v4, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-static {v3, v5}, Lcom/google/common/io/ByteStreams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v7, "VS.EPModelCopier"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error copying EP models: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
