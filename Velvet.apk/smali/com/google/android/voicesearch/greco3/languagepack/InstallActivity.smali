.class public Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;
.super Landroid/app/Activity;
.source "InstallActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$FragmentPagerAdapter;,
        Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;
    }
.end annotation


# instance fields
.field private mTabsAdapter:Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;

.field private mViewPager:Lvedroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$200(II)Ljava/lang/String;
    .locals 1
    .param p0    # I
    .param p1    # I

    invoke-static {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->makeFragmentName(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static makeFragmentName(II)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android:switcher:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static withType(I)Landroid/os/Bundle;
    .locals 2
    .param p0    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "type"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040065

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->setContentView(I)V

    const v0, 0x7f10014d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const v1, 0x7f0d0436

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    new-instance v1, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;-><init>(Landroid/app/Activity;Lvedroid/support/v4/view/ViewPager;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->mTabsAdapter:Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->mTabsAdapter:Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f0d043a

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-class v3, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    invoke-static {v4}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->withType(I)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;->addTab(Landroid/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->mTabsAdapter:Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    const v2, 0x7f0d0439

    invoke-virtual {v0, v2}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    const-class v2, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->withType(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity$TabsAdapter;->addTab(Landroid/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
