.class Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;
.super Ljava/lang/Object;
.source "HybridRecognitionEngine.java"

# interfaces
.implements Lcom/google/android/speech/audio/EndpointerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->wrapWithMergerCallbacks(Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/audio/EndpointerListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private speechDetected:Z

.field final synthetic this$0:Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;

.field final synthetic val$listener:Lcom/google/android/speech/audio/EndpointerListener;

.field final synthetic val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/voicesearch/greco3/ResultsMerger;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->this$0:Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;

    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$listener:Lcom/google/android/speech/audio/EndpointerListener;

    iput-object p3, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEndOfSpeech()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->startWaitingForPreferredSource()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$listener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v0}, Lcom/google/android/speech/audio/EndpointerListener;->onEndOfSpeech()V

    return-void
.end method

.method public onMusicDetected()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->speechDetected:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$listener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v0}, Lcom/google/android/speech/audio/EndpointerListener;->onMusicDetected()V

    :cond_0
    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$listener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v0}, Lcom/google/android/speech/audio/EndpointerListener;->onNoSpeechDetected()V

    return-void
.end method

.method public onStartOfSpeech(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->this$0:Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;

    # invokes: Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->shouldStopMusicDetectorOnStartOfSpeech()Z
    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->access$000(Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->speechDetected:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->this$0:Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;

    # getter for: Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mOriginalMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;
    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->access$100(Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;)Lcom/google/android/speech/engine/RecognitionEngine;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/speech/engine/RecognitionEngine;->close()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->speechDetected:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;->val$listener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/speech/audio/EndpointerListener;->onStartOfSpeech(J)V

    return-void
.end method
