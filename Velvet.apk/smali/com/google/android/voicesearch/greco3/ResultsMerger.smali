.class public Lcom/google/android/voicesearch/greco3/ResultsMerger;
.super Ljava/lang/Object;
.source "ResultsMerger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/greco3/ResultsMerger$State;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation
.end field

.field private mFallbackException:Lcom/google/android/speech/exception/RecognizeException;

.field private final mFallbackResponseQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mMergeEnabled:Z

.field private final mPhoneActionsMerger:Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;

.field private final mPreferredSource:I

.field private final mSourceTimeoutRunnable:Ljava/lang/Runnable;

.field private final mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<",
            "Lcom/google/android/voicesearch/greco3/ResultsMerger$State;",
            ">;"
        }
    .end annotation
.end field

.field private final mSwitchTimeoutMs:J

.field private final mTimeoutExecutor:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/speech/callback/Callback;JLcom/google/android/voicesearch/greco3/ResultsMergerStrategy;)V
    .locals 5
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
    .param p3    # J
    .param p5    # Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;J",
            "Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "VS.ResultsMerger"

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->newBuilder(Ljava/lang/String;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    sget-object v2, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_EMBEDDED:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    sget-object v2, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setStrictMode(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setDebug(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->build()Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mFallbackResponseQueue:Ljava/util/List;

    new-instance v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/greco3/ResultsMerger$1;-><init>(Lcom/google/android/voicesearch/greco3/ResultsMerger;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mSourceTimeoutRunnable:Ljava/lang/Runnable;

    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;->PREFER_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    if-ne p5, v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPreferredSource:I

    iput-boolean v3, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mMergeEnabled:Z

    :goto_0
    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mCallback:Lcom/google/android/speech/callback/Callback;

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mTimeoutExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iput-wide p3, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mSwitchTimeoutMs:J

    new-instance v0, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;

    invoke-direct {v0}, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPhoneActionsMerger:Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;->EMBEDDED_IGNORE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    if-ne p5, v0, :cond_1

    iput v3, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPreferredSource:I

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_EMBEDDED:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iput-boolean v4, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mMergeEnabled:Z

    goto :goto_0

    :cond_1
    iput v3, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPreferredSource:I

    iput-boolean v3, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mMergeEnabled:Z

    goto :goto_0
.end method

.method private static asState(I)Lcom/google/android/voicesearch/greco3/ResultsMerger$State;
    .locals 1
    .param p0    # I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_EMBEDDED:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    goto :goto_0
.end method

.method private static getSource(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)I
    .locals 3
    .param p0    # Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_EMBEDDED:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    if-ne p0, v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string v0, "VS.ResultsMerger"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bizarre call to getSource: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getSourceClientEventType()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_EMBEDDED:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x46

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x47

    goto :goto_0
.end method

.method private switchTo(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)V
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    invoke-virtual {v2, p1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->getSourceClientEventType()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget v2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPreferredSource:I

    invoke-static {p1}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->getSource(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)I

    move-result v3

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mFallbackException:Lcom/google/android/speech/exception/RecognizeException;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mCallback:Lcom/google/android/speech/callback/Callback;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mFallbackException:Lcom/google/android/speech/exception/RecognizeException;

    invoke-interface {v2, v3}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mFallbackResponseQueue:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/RecognitionResponse;

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v2, v1}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized handleSourceTimeout()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPreferredSource:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->switchTo(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_EMBEDDED:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->switchTo(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onError(Lcom/google/android/speech/exception/RecognizeException;I)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPreferredSource:I

    if-ne p2, v0, :cond_2

    const-string v0, "VS.ResultsMerger"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switching source from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->asState(I)Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_NETWORK:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->switchTo(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->USE_EMBEDDED:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->switchTo(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mFallbackException:Lcom/google/android/speech/exception/RecognizeException;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    invoke-static {p2}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->asState(I)Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized onResult(Lcom/google/android/speech/RecognitionResponse;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/RecognitionResponse;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/speech/RecognitionResponse;->getEngine()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPreferredSource:I

    if-ne v0, v1, :cond_0

    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->asState(I)Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->switchTo(Lcom/google/android/voicesearch/greco3/ResultsMerger$State;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->asState(I)Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mMergeEnabled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPhoneActionsMerger:Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;

    invoke-virtual {p1}, Lcom/google/android/speech/RecognitionResponse;->getS3Response()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->mergeWithEmbeddedResponses(Lcom/google/speech/s3/S3$S3Response;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v1, p1}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mFallbackResponseQueue:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mPhoneActionsMerger:Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;

    invoke-virtual {p1}, Lcom/google/android/speech/RecognitionResponse;->getS3Response()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->process(Lcom/google/speech/s3/S3$S3Response;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized startWaitingForPreferredSource()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/voicesearch/greco3/ResultsMerger$State;->WAITING:Lcom/google/android/voicesearch/greco3/ResultsMerger$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mTimeoutExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mSourceTimeoutRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/voicesearch/greco3/ResultsMerger;->mSwitchTimeoutMs:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
